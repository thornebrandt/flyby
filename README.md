# FlyBy
## Audiovisual Sequencer in Unity 

This is currently being refactored on scratch, live on [stream](https://twitch.tv/thornebrandt) with a Test-Driven Development approach.  Examples of the project can be see on my [instagram](https://instagram.com/thornebrandt)




### In Progress/Punchlist

* re-test why instanced puppetry as not working. 
* allow activeShader to receive colors, vector3s, and floats ( test in person ) 
* make sure all knob methods are mapping to the new knobValue from the activeTrigger. 
* RangeActiveBehaviour ( particles - ) --- ? what does this mean?
* move json to private resources so it's saved in version history. 
* move lists of roles into an array of paths, like components. 
* allow presets for roles. 
* investigate how kinect's "initialPosition" might slowly drift. 
* figure out "feedback" shader.
* investigate why audioController does not have a checkbox.
* kinect - send any activeTrigger intro within activeKinect children. 
* messaging in createObjFromSelection, showing activeTrigger that you have added.
* knobs for cameraAngle ( quick setup ) 
* need better way to default a knob value for composition ( triggerProperty ? ) 
* consolidate scripts to use additiveActiveBehaviour.


### Backlog


* --------------------------------------- UI 
* -
* make better display of active triggers and vectors in editor.  (customPropertyDrawers);
* make custom editor window w/ dropdowns for the values that you want to use. odin? ( possibly a search field ) 
* midi listen mode / key listen mode - define triggers rapidly.
* -
* ---------------------------------- design 
* remove nested "emission" from Cols and triggerColor() and just use HDR colors. 
* way to auto-setup auto triggers from obj - (  makes sense for kinect bodies - but could also follow presets ) - step towards UI.  ( simple, XY, ) 
* -UI boolean for unique vs common triggers. ![Alt text]
* move preloadVariants in activeSprite and activeParticles to assetController. 
* FEATURE: live coding file that is similar to quick reset but manually adds objs scene additively from a file. 
* allow expressions in vec variables. `z: 0.5*${global.obj_index}` 
* consider changing triggerUI class to Button. 
* consider changing triggerProperty.start to triggerProperty.defaultKnob or moving default into knob. 
* better way to handle chat messages.
* an option for activeTrigger presets to copy child activeTriggers ( off by default ) 
* activeAnimator design - allow reverse animations somehow.
* make color respond to HEX values. 
* for vecs, make a way for relative to "loop",  so we can iterate over looping animation states
* Do we want a childIndex2 for random children? or randomize group?
* bug: activeVirtualCameras defaults to cameraAngles you aren't using - think of a way to fix this. 
* add a third position, besides pivot and position, that does offset.  (why?)
* function for copying transform elements into the clipboard as "triggerScale: value, etc." 
* activeAnimator - think of way to send a "preWarm" animations ( start in the middle )
* turn on/off controllers, such as twitch, audio, sockets, via config file. 
* attempt to send and retrieve assetBundles from a server. 
* parts/objs are not "hidden" on change, some might need a "constant or remove proprety", or "constant" parts that always listen except song change?
* transition to "relative knobs" so that there aren't jumps every time they are used. 
* 2D sprite system - presets for edges ( pop along bottom )    should position be normalized ?  so -0.5 is the edges? 
* list of assets instead of asset.
* convert alwaysFront/alwaysBack to a "zType"  - including a random Z. 
* allow groups to be nullified after presets. 

* -
* ---------------------------- chores/bugs
* investigate why custom activeTriggers need hack during update. to setup stuff.  ( see ActiveAvatar ) 
* BUG: memory leak: scene with a lot of lights starts to freeze after "re-loaded"
* BUG: memory leak:  mudbun curve spawns slow down app a lot  
* refactor triggerSourceControllers. Do we need active? - just use enabled.   we need to set up supplemtary controllers based on config. before set.
* bug - one single array instance with hideOnComplete only fires onece. 
* activeAnimator - handle noStartTime for intro animations. 
* take colorProperty out of tests, move to config file. 
* multiple assetBundles
* make the rest of the blend modes for color mixing.
* make the rest of the HierarchyTypes ( what are they ? ) 
* utility function for vector3.sign to help with next function. ( include zero ) 
* remove pooled midi notes from application pause.  that get fired before a certain frame. 
* make tests for midi double triggers with groups. ( fixed manually ) 
* active animator:  need a way to use default animation time with speed 1
* common multipliers - like origin - especially for scale. 
* 4k RENDER TASK add "skip to value" for timeMapping. or new script?  ( y ? ) 
* 4k RENDER TASK pull from an entire json file for the triggers for timeMapper ( to prevent typos ) 
* 4k RENDER TASK - don't accept timed triggers from time mapper. ( or at least give warning. )  
* presets for cameraAngles. 
* turning controllers on and off w/ config.  
* make triggerColor work with activeRamp ( only intro works now )  
* fov property for cameraAngle ( animated ) 
* stress test alwaysFront videos. 
* triggerScale needs to check for zeros
* ability to disable cameraTriggers.
* make tests for nonstatic traversal of obj hierarchy that use hierarchyTypes. 
* -
* ----------------------- more features:

* look up gestures for canvas. 
* investigate spriteShapes.
* knobStartTimeTrigger, knobEndTimeTrigger ( uses startTime and startTime2 )  - knob will control intro time. 
* cinemachine specific controls / rotation, etc. 
* hue is important on instanceArrays. 
* rainbowFeedback post processing. 
* alembic speed triggers.  
* alembic knobs.
* actual imageSequences
* osc controller.
* knobs for cameraTrigger rotate and speed. 
* note value, calculated by triggerProperty range and calculated as the trigger & intro value ( noteMultiplier ) 
* implement "waitTime/bullpen" if bonePhone ever becomes larger. 
* ----------------------- optimization:
* -
* do activeType.All need to share tweens?
* think about ways to restructure triggers after definition - possibly a narrowed down dictionary of strings of eventNames per part -  narrow down "used" triggers as the list of triggers for part change for optimization. 



### Tips 

* remapLocalPosition can not be nested under a gameObject when used with fullIK.
* default knob position is usually zero. 
* now that we are triggering objs from other objs, the end obj needs the default knob starting position. 
* midi knob OUT will easily create loops. Avoid similar channels.  
* in normal hierarchy, prefabs are offset by their included properties. ( to update from 2021 information, zero out the prefab. )  In flat hierarchy, they are offset by the information of the obj. 
* If something isn't popping back to current location correctly, it's in the behaviours TriggerOff function. 
* Components from different parts of a song might show up if there is an error to their data structure. ( it's not a problem with parts or prerender )
* Don't use "on" in "WaitForSecondsOrTrue", use the property that you are testing. 
* 0.1f is too short of time to use for bulk tests. minimum is 0.15f.
* if tests are becoming too slow, restart unity.  (35s to 3s compile time) 
* Dont use prerender in tests if you have to edit the triggers manually.
* If propertyAts is null, setupActiveTriggers is not setup correctly.


### Completed
* ~~fast group setup ( only on behaviours )~~
* ~~lineRenderer~~
* ~~punch/shake trigger on virtual cameras~~
* ~~can get virtual cameras by name~~
* ~~can download and populate avatars from the discord~~
* ~~TriggerUI - discord buttons~~
* ~~LFO controller~~ 
* ~~knobLerpTime is a triggerProperty~~
* ~~collission triggers for midiOUT ( custom scripts )~~ 
* ~~kinectController~~
* ~~activeLayer~~
* ~~activeBehaviours have ids to separate calls to multiple custom behaviours:  "behaviour"~~
* ~~custom displacement ramp shader~~ 
* ~~can change resolution on build with config~~
* ~~socketController listens to discord messages~~ 
* ~~display for MIDI out messages~~
* ~~need a way to clear MIDIOut notes "onDisable"  midi.~~
* ~~velocity value added to activeTrigger~~
* ~~behaviourType for filtering specific multiple custom activeTriggers~~
* ~~randomized colors and vectors can now be seeded~~
* ~~groups now work with array~~
* ~~dynamic geometry~~
* ~~socketController~~
* ~~quick one button set/song/part/component creation~~
* ~~arrays for single and multiple object triggers~~
* ~~activePivot to help with arrays~~
* ~~curves are part of activeTrigger now - used in bone rotation~~
* ~~activeBone: handles dynamicBone / rigidBodies and manual rotation ~~ 
* ~~control sprite sequences with knobs~~
* ~~sound controller -all/bass/mid/treble frequencies.~~
* ~~stacks should work with all hierarchyTypes with speed and position~~
* ~~animated intensity and cone angle for spotLights~~
* ~~timeTrigger "timeOff" to simulate sustain~~
* ~~fade out knob~~
* ~~solo mode for objs~~
* ~~can disabled objs and activeTriggers~~
* ~~easing parameters in cameraAngle~~
* ~~easing parameters in activeTrigger. ~~
* ~~startDelay for animator~~
* ~~timeTriggers for recordings, custom json paths~~
* ~~activeRamp shader~~
* ~~alwaysFront and alwaysBack modes~~
* ~~TriggerGroups in both objs~~
* ~~pooling of render textures for multiple timed videos~~
* ~~can parent to camera~~
* ~~transition between orthographic and perspective.~~
* ~~parenting for cameraAngle, animated changing of parents.~~ 
* ~~twitch command controller.~~
* ~~sprites maintain widths with strange image ratios~~
* ~~sprite variants~~
* ~~cameraTriggers for rotation~~
* ~~animatedSprite with hideOnComplete~~
* ~~made compilation time faster~~
* ~~animated sprites~~
* ~~sequential animator~~
* ~~relative values for activePosition and activeRotation~~
* ~~parenting~~
* ~~manual additive scenes~~
* ~~particles~~
* ~~vfx~~
* ~~shaders~~
* ~~blendshapes~~ 
* ~~storing sequences in activetriggers for additive secondary triggers.~~
* ~~data driven active triggers for custom behaviour~~
* ~~trigger lights.~~
* ~~trigger animator custom active behaviour.~~
* ~~fixed recursion/serialization problem for active triggers.~~ 
* ~~add additional hierarchy so custom child instance has no transforms.~~
* ~~combine active speed and position manually.~~
* ~~activeType.Next and activeType.All working great.~~
* ~~made a blend mode property of color component~~
* ~~make setup of flybytests work without json.~~ 
* ~~fixed knobs working for activeNext for colors.~~
* ~~activeBehaviours have their own killing solution.~~
* ~~test for endDelay~~
* ~~test for startDelay~~
* ~~custom kill commands for each activeBehaviour.~~
* ~~implementing "value" as part of activeTrigger.~~
* ~~seperate rotate and rotation activeBehaviours~~
* ~~camera angles~~
* ~~Objs can copy objects and overwrite with default booleans~~
* ~~Objs inherit bundle from parts which inherit from Songs~~
* ~~preRender~~
* ~~start knob value~~
* ~~asset bundles~~
* ~~relative knob~~
* ~~obj specific triggerProperty for knob reversal and remapping~~
* ~~debounce/throlle midi knobs~~
* ~~multiple knobs control same color~~
* ~~knob controls main color~~
* ~~parts can be triggered~~
* ~~songs can be triggered~~
* ~~color can animated~~
* ~~triggers can override activeType~~
* ~~presetSystem -includes presets from presets, objs and activeTriggers~~
* ~~activePosition~~
* ~~refactored activeTriggers~~
* ~~sustained triggers~~
* ~~active behaviours are cached within objs~~
* ~~activeTriggerController~~
* ~~triggerObjs~~
* ~~activeInstances~~
* ~~common TriggerSource~~
* ~~midi interface~~
* ~~setup keyboard interface~~
* ~~instance pooling~~
* ~~controllers~~
* ~~core classes~~
* ~~json parsing~~  


---

Questions? contact me on [twitter.com/thornebrandt](twitter.com/thornebrandt)

[thornebrandt.com](thornebrandt.com)
