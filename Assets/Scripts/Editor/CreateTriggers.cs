using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Reflection;
using flyby.Controllers;
using Newtonsoft.Json;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using flyby.Sets;
using flyby.Core;
using flyby.UI;

namespace flyby
{
    public class CreateTriggers : BaseEditorWindow
    {
        private Set set;
        private Trigger trigger;
        private Config config;
        //private string configPath = "config.json"; //need way to save this. ( make an input? ) 
        private List<Trigger> triggers;
        private List<string> triggerNames;
        private int selectedTriggerIndex;
        private int lastSelectedTriggerIndex;
        //private string buttonLabel = "Add Trigger";
        private JsonSerializerSettings JSONSettings;
        private string state = "";
        private string listenState = "";
        private bool usingKeyboard;
        private string pressedKey;
        private bool usingMIDINote;
        private bool usingCC;
        private bool usingTime;
        private bool usingChat;


        [MenuItem("FlyBy/Create Triggers")]
        static void ShowWindow()
        {
            CreateTriggers window = (CreateTriggers)EditorWindow.GetWindow(typeof(CreateTriggers));
            window.config = window.loadConfig();
            window.jsonPath = window.config.jsonPath;
            window.triggers = new List<Trigger>();
            window.triggerNames = new List<string>();
            window.set = window.loadSet(window.config.set);
            window.Show();
            window.JSONSettings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        private void OnEnable()
        {
            MidiController.OnMidiNote += MidiNoteHandler;
        }


        // Unsubscribe when the editor window is disabled
        private void OnDisable()
        {
            MidiController.OnMidiNote -= MidiNoteHandler;
        }

        private void MidiNoteHandler(int channel, int note, float velocity)
        {
            Debug.Log("received midi note: " + channel + " " + note + " " + velocity);
        }

        private void OnGUI()
        {
            loadStyles();
            EditorGUILayout.Separator();
            showTriggerDropDowns();
            showListenOverlay();
        }

        private void showCreateTrigger()
        {
            //temp values on.

            DrawHeading("Create Trigger", "Create a definition for how information comes in");

            DrawDashedLine(Color.white);

            trigger.id = EditorGUILayout.TextField("Description:", trigger.id);
            trigger.eventName = EditorGUILayout.TextField("Event Name:", trigger.eventName);
            trigger.group = EditorGUILayout.TextField("Group: ", trigger.group);

            DrawDashedLine(Color.white);

            if (this.usingMIDINote)
            {
                trigger.note = EditorGUILayout.IntField("Note: ", trigger.note);
                //TODO - make a UI to signify a range. 
                trigger.note2 = EditorGUILayout.IntField("Note2: ", trigger.note2);
                trigger.channel = EditorGUILayout.IntField("Channel: ", trigger.channel);
                if (GUILayout.Button("Ignore MIDI Notes"))
                {
                    this.usingMIDINote = false;
                }
            }
            else
            {
                if (GUILayout.Button("Use MIDI Note Triggers"))
                {
                    this.usingMIDINote = true;
                }
            }

            DrawDashedLine(Color.white);

            if (this.usingCC)
            {
                trigger.knob = EditorGUILayout.IntField("Knob: ", trigger.knob);
                trigger.channel = EditorGUILayout.IntField("Channel: ", trigger.channel);
                if (GUILayout.Button("Ignore CC Values"))
                {
                    this.usingCC = false;
                }
            }
            else
            {
                if (GUILayout.Button("Use CC Values"))
                {
                    this.usingCC = true;
                }
            }

            DrawDashedLine(Color.white);

            if (this.usingKeyboard)
            {
                trigger.key = EditorGUILayout.TextField("Key: ", trigger.key);
                if (GUILayout.Button("Listen"))
                {
                    this.listenState = "keyboard";
                }
                if (GUILayout.Button("Ignore Keyboard Keys"))
                {
                    this.usingKeyboard = false;
                }
            }
            else
            {
                if (GUILayout.Button("Use Keyboard presses"))
                {
                    this.usingKeyboard = true;
                }
            }

            DrawDashedLine(Color.white);

            if (this.usingTime)
            {
                trigger.time = EditorGUILayout.FloatField("Time: ", trigger.time);
                trigger.noteOff = EditorGUILayout.Toggle("Time Off: ", trigger.noteOff);
                trigger.repeatInterval = EditorGUILayout.FloatField("Repeat Interval: ", trigger.repeatInterval);
                trigger.repeatInterval2 = EditorGUILayout.FloatField("Repeat Interval 2: ", trigger.repeatInterval2);
                trigger.numRepeats = EditorGUILayout.FloatField("NumRepeats: ", trigger.numRepeats);
                if (GUILayout.Button("Ignore Time"))
                {
                    this.usingKeyboard = false;
                }
            }
            else
            {
                if (GUILayout.Button("Use timed trigger"))
                {
                    this.usingTime = true;
                }
            }

            DrawDashedLine(Color.white);
            if (this.usingChat)
            {
                trigger.user = EditorGUILayout.TextField("User: ", trigger.user);
                trigger.chat = EditorGUILayout.TextField("Chat: ", trigger.chat);
                if (GUILayout.Button("Ignore Chat"))
                {
                    this.usingKeyboard = false;
                }
            }
            else
            {
                if (GUILayout.Button("Listen to chat"))
                {
                    this.usingChat = true;
                }
            }
        }


        private void showTriggerDropDowns()
        {
            switch (this.state)
            {
                case "createTrigger":
                    showCreateTrigger();

                    break;
                case "editTrigger":
                    DrawHeading("Edit Trigger");
                    //TODO - get list of triggers here to select one. 
                    //-change state to editing trigger. 
                    break;
                case "":
                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Create Trigger"))
                    {
                        this.trigger = new Trigger();
                        this.state = "createTrigger";
                    }
                    if (GUILayout.Button("Edit Trigger"))
                    {
                        this.state = "editTrigger";
                    }
                    GUILayout.EndHorizontal();
                    break;
            }
        }

        private void showListenOverlay()
        {
            if (!string.IsNullOrEmpty(this.listenState))
            {
                GUI.color = new Color(0.5f, 0, 0, 0.2f);
                GUI.DrawTexture(new Rect(0, 0, position.width, position.height), EditorGUIUtility.whiteTexture);
                GUI.color = Color.white;
                GUIStyle headingStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 24,
                    fontStyle = FontStyle.Bold,
                    alignment = TextAnchor.MiddleCenter,
                    normal = new GUIStyleState
                    {
                        textColor = Color.white // Set text color to white
                    }
                };
                GUI.Label(new Rect(0, position.height / 2 - 12, position.width, 50), "Listening for keyboard", headingStyle);
                Event e = Event.current;
                if (e.type == EventType.KeyDown)
                {
                    this.listenState = "";
                    trigger.key = e.keyCode.ToString();
                    Debug.Log(e.keyCode.ToString() + " pressed");
                }
                Event.current.Use();
            }
        }

        private Set loadSet(string setPath)
        {
            //set nests all child functions, and child functions respectively. 
            string setJSON = Json.getJSON(jsonPath + setPath);
            this.set = JsonConvert.DeserializeObject<Set>(setJSON, this.JSONSettings);
            if (set.triggers != null && set.triggers.Count > 0)
            {
                this.triggerNames = set.triggers;
            }

            return set;
        }

    }
}
#endif