#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

namespace flyby {
    public class BuildAssetBundles : MonoBehaviour {
        [MenuItem("FlyBy/Build Asset Bundles")]
        static void BuildABs() {
            BuildPipeline.BuildAssetBundles("AssetBundles", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
        }
    }
}

#endif