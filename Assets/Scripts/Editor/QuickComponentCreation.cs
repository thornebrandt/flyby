using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
using System;
using System.Collections.Generic;
using flyby.Controllers;
using Newtonsoft.Json;
using flyby.Tools;

namespace flyby
{
    public class QuickComponentCreation : EditorWindow
    {
        private string fileName = "new_component.json";
        private Config config;
        private string jsonPath = "/";
        private string setsPath = "sets.json";
        private string configPath = "config.json"; //need way to save this. ( make an input? ) 

        [MenuItem("FlyBy/Quick Component Creation")]

        static void OnEnable()
        {
            QuickComponentCreation window = (QuickComponentCreation)EditorWindow.GetWindow(typeof(QuickComponentCreation));
            window.config = window.loadConfig();
            window.Show();
        }

        private Config loadConfig()
        {
            if (Config.instance == null)
            {
                string configJSON = Json.getJSON(configPath);
                this.config = JsonConvert.DeserializeObject<Config>(configJSON);
                this.jsonPath = this.config.jsonPath;
                return this.config;
            }
            else
            {
                return Config.instance;
            }
        }

        private void OnGUI()
        {
            EditorGUILayout.HelpBox("Create a folder and basee name, such as 'songs/new_song' Will create set, song, part, and component, saving to " + this.jsonPath, MessageType.Info);
            EditorGUILayout.LabelField("Base File Name:");

            string tempFileName = "";
            if (GUILayout.Button("Browse Files"))
            {
                tempFileName = EditorUtility.OpenFilePanel("Browse Set Files", jsonPath, "");
            }

            fileName = EditorGUILayout.TextField(fileName);

            if (GUILayout.Button("Create JSON Files"))
            {
                CreateAllJson();
            }
        }

        private void CreateAllJson()
        {
            fileName = Path.ChangeExtension(fileName, null);
            string filePath = Path.Combine(jsonPath, fileName);
            string[] folders = filePath.Split(new char[] { Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar });
            string currentPath = "";
            try
            {
                //check for folders. 
                for (int i = 0; i < folders.Length - 1; i++)
                {
                    string folder = folders[i];
                    currentPath = Path.Combine(currentPath, folder);
                    if (!Directory.Exists(currentPath))
                    {
                        Directory.CreateDirectory(currentPath);
                    }
                    else
                    {
                        Debug.Log("everything returning true: " + currentPath);
                    }
                }

                string componentName = fileName + ".json";
                string fullComponentFilePath = Path.Combine(jsonPath, componentName);
                if (!File.Exists(componentName))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(filePath)); //shouldn't cause problems?
                    File.WriteAllText(fullComponentFilePath, "{}");
                    Debug.Log("component created at: " + componentName);
                }
                else
                {
                    Debug.Log("component " + componentName + " already exists.");
                }
                string partName = fileName + "_part.json";
                if (!File.Exists(partName))
                {
                    string partText = " { \"components\": [\"" + componentName + "\"] }";
                    File.WriteAllText(Path.Combine(jsonPath, partName), partText);
                    Debug.Log("part created at: " + partName);
                }
                else
                {
                    Debug.Log("part " + partName + " already exists.");
                }

                string songName = fileName + "_song.json";
                if (!File.Exists(songName))
                {
                    string songText = " { \"parts\": [\"" + songName + "\"] }";
                    File.WriteAllText(Path.Combine(jsonPath, songName), songText);
                    Debug.Log("song created at: " + songName);
                }
                else
                {
                    Debug.Log("song " + songName + " already exists.");
                }

                string setName = fileName + "_set.json";
                if (!File.Exists(setName))
                {
                    string setText = " { \"songs\": [\"" + songName + "\"] }";
                    File.WriteAllText(Path.Combine(jsonPath, setName), setText);
                    Debug.Log("set created at: " + setName);
                }
                else
                {
                    Debug.Log("set " + songName + " already exists.");
                }

                updateConfig(config, setName);
                updateSetNames(config, setName);
                Close();
            }
            catch (Exception e)
            {
                Debug.Log("there was a problem creating a json file: " + e);
            }
        }

        private void updateConfig(Config config, string setName)
        {
            config.set = setName;
            string saveConfig = JsonConvert.SerializeObject(config, Formatting.Indented);
            File.WriteAllText("config.json", saveConfig);
            Debug.Log("Updated config");
        }

        private List<string> pruneSetNames(List<string> sets)
        {
            List<string> prunedSets = new List<string>();
            foreach (string set in sets)
            {
                if (File.Exists(Path.Combine(jsonPath, set)))
                {
                    prunedSets.Add(set);
                }
            }
            return prunedSets;
        }

        private void updateSetNames(Config config, string setName)
        {
            string fullSetsPath = Path.Combine(jsonPath, setsPath);
            string originalSetsJSON = Json.getJSON(fullSetsPath);
            List<string> sets = JsonConvert.DeserializeObject<List<string>>(originalSetsJSON);
            sets = pruneSetNames(sets);
            sets.Add(setName);
            string newSetsJSON = JsonConvert.SerializeObject(sets, Formatting.Indented);
            File.WriteAllText(fullSetsPath, newSetsJSON);
            Debug.Log("updated " + fullSetsPath);
        }
    }
}
#endif
