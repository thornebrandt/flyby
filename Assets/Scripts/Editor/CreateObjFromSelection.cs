using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Reflection;
using flyby.Controllers;
using Newtonsoft.Json;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using flyby.Sets;
using flyby.Core;
using flyby.UI;

namespace flyby
{
    public class CreateObjFromSelection : BaseEditorWindow
    {
        private GameObject gameObject;
        private Obj o; //the abstract representation of the gameObject that is constructed during runtime. 
        private Set set;
        private Song song;
        private Part part;
        private Config config;
        //private string configPath = "config.json"; //need way to save this. ( make an input? ) 
        private string setsPath = "sets.json";
        private string prefabName = "newObj";
        private string prefabPath; //saves to tempPrefabs at first.
        private int lastSelectedSetIndex = -1;
        private int selectedSetIndex = 0;
        private string newSetName = "";
        private string newSongName = "";
        private string newPartName = "";
        private string newComponentName = "";
        private List<string> setNames = new List<string>();
        private List<string> setNamesDisplay = new List<string>();
        private List<string> songNamesDisplay = new List<string>();
        private List<string> partNamesDisplay = new List<string>();
        private List<string> componentNamesDisplay = new List<string>();
        private int selectedSongIndex = 0;
        private int lastSelectedSongIndex = -1;
        private int selectedPartIndex = 0;
        private int lastSelectedPartIndex = -1;
        private int selectedComponentIndex = 0;
        private int lastSelectedComponentIndex = -1;
        private string partHeading = "No Part Selected";
        private List<Obj> component;
        private string loadComponentFile;
        private string newComponentFile;
        private string activeTriggerSearchInput = "";
        private string triggerSearchInput = "";
        private int activeTriggerSuggestionIndex = 0;
        private int triggerSuggestionIndex = 0;
        private int selectedActiveTriggerIndex;
        private int lastSelectedActiveTriggerIndex;
        private List<ActiveTriggerUI> activeTriggers;
        private List<Trigger> triggers;
        private int selectedTriggerIndex;
        private int lastSelectedTriggerIndex;
        private string buttonLabel = "Add Trigger";
        private string state = "gameObjectSelection";
        private string subState = ""; //for hiding/showing within states. 
        private JsonSerializerSettings JSONSettings;

        private List<ActiveTriggerUI> activeTriggerSuggestions = new List<ActiveTriggerUI>{
            new ActiveTriggerUI{
                name="Color",
                type="triggerColor"
            },
            new ActiveTriggerUI{
                name="Pivot",
                type="triggerPivot"
            },
            new ActiveTriggerUI{
                name="Parent",
                type="triggerParent"
            },
            new ActiveTriggerUI{
                name="Position",
                type="triggerPosition"
            },
            new ActiveTriggerUI{
                name="Rotation",
                type="triggerRotation"
            },
            new ActiveTriggerUI{
                name="Speed",
                type="triggerSpeed"
            },
            new ActiveTriggerUI{
                name="Scale",
                type="triggerScale"
            },
            new ActiveTriggerUI{
                name="Sprite",
                type="triggerSprite"
            },
            new ActiveTriggerUI{
                name="Custom",
                type="triggerTrigger"
            }
        };


        [MenuItem("FlyBy/Create Obj From Selection")]

        static void OnEnable()
        {
            CreateObjFromSelection window = (CreateObjFromSelection)EditorWindow.GetWindow(typeof(CreateObjFromSelection));
            window.config = window.loadConfig();
            window.jsonPath = window.config.jsonPath;
            window.setNames = window.loadSetNames(window.setsPath);
            if (window.setNames == null)
            {
                window.setNames = new List<string>();
            }
            window.setNamesDisplay = window.prepareFilePathsToDisplayStrings(new List<string>(window.setNames));
            window.setNamesDisplay.Add("**FIND SET**");
            window.setNamesDisplay.Add("**NEW SET**");
            window.selectedSetIndex = window.getStringIndex(window.setNames, window.config.set); //default set index
            window.activeTriggers = new List<ActiveTriggerUI>();
            window.triggers = new List<Trigger>();
            if (Selection.activeGameObject != null)
            {
                window.gameObject = Selection.activeGameObject;
                window.prefabName = Selection.activeGameObject.name;
                window.prefabPath = window.prefabName + ".prefab";
            }
            window.Show();
            window.o = new Obj();
            window.loadSetFromSetNames(window.setNames);
            window.JSONSettings = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore
            };
        }

        private List<string> loadSetNames(string setsPath)
        {
            string setsJSON = Json.getJSON(Path.Combine(jsonPath, setsPath));
            if (string.IsNullOrEmpty(setsJSON))
            {
                return null;
            }
            setNames = JsonConvert.DeserializeObject<List<string>>(setsJSON, this.JSONSettings);
            return setNames;
        }

        private int getStringIndex(List<string> setNames, string setName)
        {
            int setIndex = setNames.IndexOf(setName);
            if (setIndex != -1)
            {
                return setIndex;
            }
            else
            {
                return 0;
            }
        }

        private Set loadSet(string setPath)
        {
            //set nests all child functions, and child functions respectively. 
            string setJSON = Json.getJSON(jsonPath + setPath);
            Set set = JsonConvert.DeserializeObject<Set>(setJSON, this.JSONSettings);
            if (set.songs != null && set.songs.Count > 0)
            {
                songNamesDisplay = prepareFilePathsToDisplayStrings(new List<string>(set.songs));
                songNamesDisplay.Add("**FIND SONG**");
                songNamesDisplay.Add("**NEW SONG**");
                this.song = loadSong(set.songs[selectedSongIndex]);
            }
            else
            {
                this.song = null;
                this.part = null;
            }
            return set;
        }

        private Song loadSong(string songPath)
        {
            string songJSON = Json.getJSON(jsonPath + songPath);
            Song song = JsonConvert.DeserializeObject<Song>(songJSON, this.JSONSettings);
            if (song.parts != null && song.parts.Count > 0)
            {
                partNamesDisplay = prepareFilePathsToDisplayStrings(new List<string>(song.parts));
                partNamesDisplay.Add("**FIND PART**");
                partNamesDisplay.Add("**NEW PART**");
                this.part = loadPart(song.parts[selectedPartIndex]);
            }
            else
            {
                this.part = null;
            }
            return song;
        }

        private Part loadPart(string partPath)
        {
            string partJSON = Json.getJSON(jsonPath + partPath);
            Part part = JsonConvert.DeserializeObject<Part>(partJSON, this.JSONSettings);

            //HACK - dealing with weird part structure of default cameraAngles. 
            if (part.cameraAngles.Count == 1 && part.cameraAngles[0].id == "default_camera_angle")
            {
                part.cameraAngles = null;
                //part.currentCameraAngle = null;
            }

            if (part.components != null && part.components.Count > 0)
            {
                componentNamesDisplay = prepareFilePathsToDisplayStrings(new List<string>(part.components));
                componentNamesDisplay.Add("**FIND COMPONENT**");
                componentNamesDisplay.Add("**NEW COMPONENT**");
                this.component = loadComponent(part.components[selectedComponentIndex]);
            }
            else
            {
                this.component = null;
            }
            return part;
        }

        private List<Obj> loadComponent(string componentPath)
        {
            string componentJSON = Json.getJSON(jsonPath + componentPath);
            List<Obj> component = JsonConvert.DeserializeObject<List<Obj>>(componentJSON, this.JSONSettings);
            return component;
        }

        private void showGameObject()
        {
            DrawHeading("Prefab Object");
            if (this.state == "gameObjectSelection")
            {
                gameObject = EditorGUILayout.ObjectField("GameObject:", gameObject, typeof(GameObject), true) as GameObject;
            }
            else
            {
                if (GUILayout.Button("Select a Different GameObject"))
                {
                    this.state = "gameObjectSelection";
                }
            }
        }

        private void loadSetFromSetNames(List<string> setNames)
        {
            if (setNames != null && setNames.Count > 0)
            {
                this.set = loadSet(setNames[selectedSetIndex]);
            }
        }

        private void showPartDropDowns()
        {
            partHeading = checkForSetSubHeading();
            if (this.state == "partSelection")
            {
                DrawHeading("Part Selection", partHeading);
                switch (this.subState)
                {
                    case "newSet":
                        GUILayout.BeginHorizontal();
                        newSetName = EditorGUILayout.TextField("Create New Set", newSetName);
                        if (GUILayout.Button("Create Set"))
                        {
                            set = new Set();
                            string fileName = Path.ChangeExtension(newSetName, ".json");
                            string filePath = Path.Combine(jsonPath, fileName);
                            try
                            {
                                File.WriteAllText(filePath, "{}");
                                saveSetToSets(fileName);
                                selectedSetIndex = setNames.Count - 1;
                            }
                            catch (Exception e)
                            {
                                Debug.Log("there was a problem creating a set: " + e);
                            }
                            this.subState = "";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        GUILayout.EndHorizontal();
                        break;
                    case "findSet":
                        newSetName = "";
                        newSetName = EditorUtility.OpenFilePanel("Select Set", jsonPath, "json");
                        newSetName = getRelativePath(newSetName, jsonPath);
                        if (!string.IsNullOrEmpty(newSetName))
                        {
                            this.subState = "foundSet";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        break;
                    case "foundSet":
                        GUILayout.BeginHorizontal();
                        EditorGUILayout.LabelField("Add \"" + newSetName + "\" to sets registry?");
                        if (GUILayout.Button("Confirm"))
                        {
                            saveSetToSets(newSetName);
                            this.subState = "";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        GUILayout.EndHorizontal();
                        break;
                    case "":
                        if (setNames != null && setNames.Count > 0)
                        {
                            EditorGUILayout.Space();
                            selectedSetIndex = EditorGUILayout.Popup("Set:", selectedSetIndex, setNamesDisplay.ToArray());
                            if (selectedSetIndex == setNames.Count)
                            {
                                this.subState = "findSet";
                                selectedSetIndex = lastSelectedSetIndex; // don't trigger repaint. 
                            }

                            if (selectedSetIndex == setNames.Count + 1)
                            {
                                this.subState = "newSet";
                                selectedSetIndex = lastSelectedSetIndex; // don't trigger repaint.
                            }


                            if (selectedSetIndex != lastSelectedSetIndex)
                            {
                                this.set = loadSet(setNames[selectedSetIndex]);
                                if (set.songs != null)
                                {
                                    lastSelectedSongIndex = -1;
                                    lastSelectedPartIndex = -1;
                                    lastSelectedComponentIndex = -1;
                                    lastSelectedSetIndex = selectedSetIndex;
                                }
                                if (set.triggers != null)
                                {
                                    this.triggers.Clear();
                                    foreach (string triggerName in set.triggers)
                                    {
                                        string fullTriggerPath = Path.Combine(jsonPath, triggerName);
                                        if (File.Exists(fullTriggerPath))
                                        {
                                            string triggerJSON = Json.getJSON(fullTriggerPath);
                                            List<Trigger> newTriggers = JsonConvert.DeserializeObject<List<Trigger>>(triggerJSON, JSONSettings);
                                            this.triggers.AddRange(newTriggers);
                                        }
                                        else
                                        {
                                            Debug.Log("trigger file: " + triggerName + " does not exist");
                                        }
                                    }
                                }
                            }
                        }
                        break;
                }

                switch (this.subState)
                {
                    case "newSong":
                        GUILayout.BeginHorizontal();
                        newSongName = EditorGUILayout.TextField("Create New Song", newSongName);
                        if (GUILayout.Button("Create Song"))
                        {
                            song = new Song();
                            string fileName = Path.ChangeExtension(newSongName, ".json");
                            string filePath = Path.Combine(jsonPath, fileName);
                            try
                            {
                                File.WriteAllText(filePath, "{}");
                                saveSongToSet(this.set, fileName);
                                selectedSongIndex = set.songs.Count - 1;
                            }
                            catch (Exception e)
                            {
                                Debug.Log("there was a problem creating a song: " + e);
                            }
                            this.subState = "";
                        }
                        GUILayout.EndHorizontal();
                        break;
                    case "findSong":
                        newSongName = "";
                        newSongName = EditorUtility.OpenFilePanel("Select Song", jsonPath, "json");
                        newSongName = getRelativePath(newSongName, jsonPath);
                        if (!string.IsNullOrEmpty(newSongName))
                        {
                            this.subState = "foundSong";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        break;
                    case "foundSong":
                        GUILayout.BeginHorizontal();
                        string setName = setNames[selectedSetIndex];
                        EditorGUILayout.LabelField("Add \"" + newSongName + "\" to sets " + setName + "?");
                        if (GUILayout.Button("Confirm"))
                        {
                            saveSongToSet(this.set, newSongName);
                            this.subState = "";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        GUILayout.EndHorizontal();
                        break;
                    case "":
                        if (this.set != null && set.songs != null && set.songs.Count > 0)
                        {
                            EditorGUILayout.Space();

                            selectedSongIndex = EditorGUILayout.Popup("Song:", selectedSongIndex, songNamesDisplay.ToArray());

                            if (selectedSongIndex == set.songs.Count)
                            {
                                this.subState = "findSong";
                                selectedSongIndex = lastSelectedSongIndex > -1 ? lastSelectedSongIndex : 0; // don't trigger repaint. 
                            }

                            if (selectedSongIndex == set.songs.Count + 1)
                            {
                                this.subState = "newSong";
                                selectedSongIndex = lastSelectedSongIndex > -1 ? lastSelectedSongIndex : 0; // don't trigger repaint.
                            }

                            if (selectedSongIndex != lastSelectedSongIndex)
                            {
                                this.song = loadSong(set.songs[selectedSongIndex]);
                                //reset nested part selections. 
                                lastSelectedComponentIndex = -1;
                                lastSelectedPartIndex = selectedPartIndex;
                            }
                        }
                        break;
                }

                switch (this.subState)
                {
                    case "newPart":
                        GUILayout.BeginHorizontal();
                        newPartName = EditorGUILayout.TextField("Create New Part", newPartName);
                        if (GUILayout.Button("Create Part"))
                        {
                            part = new Part();
                            string fileName = Path.ChangeExtension(newPartName, ".json");
                            string filePath = Path.Combine(jsonPath, fileName);
                            try
                            {
                                File.WriteAllText(filePath, "{}");
                                savePartToSong(this.song, fileName);
                                selectedPartIndex = song.parts.Count - 1;
                            }
                            catch (Exception e)
                            {
                                Debug.Log("there was a problem creating a part: " + e);
                            }
                            this.subState = "";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        GUILayout.EndHorizontal();
                        break;
                    case "findPart":
                        newPartName = "";
                        newPartName = EditorUtility.OpenFilePanel("Select Part", jsonPath, "json");
                        newPartName = getRelativePath(newPartName, jsonPath);
                        if (!string.IsNullOrEmpty(newPartName))
                        {
                            this.subState = "foundPart";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        break;
                    case "foundPart":
                        GUILayout.BeginHorizontal();
                        string songName = set.songs[selectedSongIndex];
                        EditorGUILayout.LabelField("Add \"" + newPartName + "\" to song " + songName + "?");
                        if (GUILayout.Button("Confirm"))
                        {
                            savePartToSong(this.song, newPartName);
                            this.subState = "";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        GUILayout.EndHorizontal();
                        break;
                    case "":
                        if (this.song != null && this.song.parts != null && this.song.parts.Count > 0)
                        {
                            EditorGUILayout.Space();

                            selectedPartIndex = EditorGUILayout.Popup("Part:", selectedPartIndex, partNamesDisplay.ToArray());

                            if (selectedPartIndex == song.parts.Count)
                            {
                                this.subState = "findPart";
                                selectedPartIndex = lastSelectedPartIndex > -1 ? lastSelectedPartIndex : 0; // don't trigger repaint and don't assign -1
                            }

                            if (selectedPartIndex == song.parts.Count + 1)
                            {
                                this.subState = "newPart";
                                selectedPartIndex = lastSelectedPartIndex > -1 ? lastSelectedPartIndex : 0; // don't trigger repaint and don't assign -1
                            }

                            if (selectedPartIndex != lastSelectedPartIndex)
                            {
                                this.part = loadPart(song.parts[selectedPartIndex]);
                                //reset nested componet selections. 
                                lastSelectedComponentIndex = -1;
                                lastSelectedComponentIndex = selectedComponentIndex;
                            }
                        }
                        break;
                }

                switch (this.subState)
                {
                    case "newComponent":
                        GUILayout.BeginHorizontal();
                        newComponentName = EditorGUILayout.TextField("Create New Component", newComponentName);
                        if (GUILayout.Button("Create Component"))
                        {
                            component = new List<Obj>();
                            string fileName = Path.ChangeExtension(newComponentName, ".json");
                            string filePath = Path.Combine(jsonPath, fileName);
                            try
                            {
                                File.WriteAllText(filePath, "[]");
                                saveComponentToPart(this.part, fileName);
                                selectedComponentIndex = part.components.Count - 1;
                            }
                            catch (Exception e)
                            {
                                Debug.Log("there was a problem creating a component: " + e);
                            }
                            this.subState = "";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        GUILayout.EndHorizontal();
                        break;
                    case "findComponent":
                        newComponentName = "";
                        string rawNameFromPanel;
                        rawNameFromPanel = EditorUtility.OpenFilePanel("Select Component", jsonPath, "json");
                        if (string.IsNullOrEmpty(rawNameFromPanel))
                        {
                            this.subState = "";
                        }
                        else
                        {
                            newComponentName = getRelativePath(rawNameFromPanel, jsonPath);
                        }
                        if (!string.IsNullOrEmpty(newComponentName))
                        {
                            Debug.Log("but what is the newComponentName? " + newComponentName);
                            this.subState = "foundComponent";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        break;
                    case "foundComponent":
                        GUILayout.BeginHorizontal();
                        string partName = this.song.parts[selectedPartIndex];
                        EditorGUILayout.LabelField("Add \"" + newComponentName + "\" to part " + partName + "?");
                        if (GUILayout.Button("Confirm"))
                        {
                            saveComponentToPart(this.part, newComponentName);
                            this.subState = "";
                        }
                        if (GUILayout.Button("Cancel"))
                        {
                            this.subState = "";
                        }
                        GUILayout.EndHorizontal();
                        break;
                    case "":
                        if (this.part != null && this.part.components != null && this.part.components.Count > 0)
                        {
                            EditorGUILayout.Space();

                            selectedComponentIndex = EditorGUILayout.Popup("Component:", selectedComponentIndex, componentNamesDisplay.ToArray());

                            if (selectedComponentIndex == part.components.Count)
                            {
                                this.subState = "findComponent";
                                selectedComponentIndex = lastSelectedComponentIndex > -1 ? lastSelectedComponentIndex : 0;
                            }

                            if (selectedComponentIndex == part.components.Count + 1)
                            {
                                this.subState = "newComponent";
                                selectedPartIndex = lastSelectedPartIndex > -1 ? lastSelectedPartIndex : 0; // don't trigger repaint and don't assign -1
                                selectedComponentIndex = lastSelectedComponentIndex > -1 ? lastSelectedComponentIndex : 0;
                            }

                            if (selectedComponentIndex != lastSelectedComponentIndex)
                            {
                                this.component = loadComponent(part.components[selectedComponentIndex]);
                                //reset nested componet selections. 
                                lastSelectedComponentIndex = selectedComponentIndex;
                            }
                        }
                        break;
                }
            }
            else
            {
                DrawDisabledHeading("Part Selection", partHeading);
                if (GUILayout.Button("Assign To Parts"))
                {
                    this.state = "partSelection";
                }
            }
        }

        private List<string> prepareFilePathsToDisplayStrings(List<string> fileNames)
        {
            List<string> fileNamesToDisplay = new List<string>();
            foreach (string fileName in fileNames)
            {
                fileNamesToDisplay.Add(fileName.Replace("/", " \u2215 "));
            }
            return fileNamesToDisplay;
        }

        private void saveSetToSets(string setFileName)
        {
            setNames.Add(setFileName);
            string setJson = JsonConvert.SerializeObject(setNames, Formatting.Indented, this.JSONSettings);
            File.WriteAllText(Path.Combine(jsonPath, setsPath), setJson);
            //prune sets here. 
            this.setNamesDisplay = new List<string>(this.setNames);
            this.setNamesDisplay = prepareFilePathsToDisplayStrings(this.setNamesDisplay);
            this.setNamesDisplay.Add("**FIND SET**");
            this.setNamesDisplay.Add("**NEW SET**");
            selectedSetIndex = getStringIndex(this.setNamesDisplay, setFileName);
        }

        private void saveSongToSet(Set set, string songFileName)
        {
            if (set != null)
            {
                if (set.songs == null)
                {
                    set.songs = new List<string>();
                }
                set.songs.Add(songFileName);
                string setJson = JsonConvert.SerializeObject(set, Formatting.Indented, this.JSONSettings);
                string setPath = setNames[selectedSetIndex];
                File.WriteAllText(Path.Combine(jsonPath, setPath), setJson);
                this.songNamesDisplay = prepareFilePathsToDisplayStrings(new List<string>(set.songs));
                this.songNamesDisplay.Add("**FIND SONG**");
                this.songNamesDisplay.Add("**NEW SONG**");
                selectedSongIndex = getStringIndex(set.songs, songFileName);
            }
        }

        private void savePartToSong(Song song, string partFileName)
        {
            if (song != null)
            {
                if (song.parts == null)
                {
                    song.parts = new List<string>();
                }
                song.parts.Add(partFileName);
                string songJson = JsonConvert.SerializeObject(song, Formatting.Indented, this.JSONSettings);
                string songPath = set.songs[selectedSongIndex];
                File.WriteAllText(Path.Combine(jsonPath, songPath), songJson);
                this.partNamesDisplay = prepareFilePathsToDisplayStrings(new List<string>(song.parts));
                this.partNamesDisplay.Add("**FIND PART**");
                this.partNamesDisplay.Add("**NEW PART**");
                selectedPartIndex = getStringIndex(song.parts, partFileName);
            }
            else
            {
                Debug.Log("can't save part to song that doesn't exist.");
            }
        }

        private void saveComponentToPart(Part part, string componentFileName)
        {
            if (part != null)
            {
                if (part.components == null)
                {
                    part.components = new List<string>();
                }

                part.components.Add(componentFileName);
                try
                {
                    string partJson = JsonConvert.SerializeObject(part, Formatting.Indented, this.JSONSettings);
                    string partPath = song.parts[selectedPartIndex];
                    File.WriteAllText(Path.Combine(jsonPath, partPath), partJson);
                    this.componentNamesDisplay = prepareFilePathsToDisplayStrings(new List<string>(part.components));
                    this.componentNamesDisplay.Add("**FIND COMPONENT**");
                    this.componentNamesDisplay.Add("**NEW COMPONENT**");
                    selectedComponentIndex = getStringIndex(part.components, componentFileName);
                }
                catch (Exception e)
                {
                    Debug.Log("there was a problem saving component to the part: " + e);
                }
            }
            else
            {
                Debug.Log("tried to save component " + componentFileName + " to part, but it was null.");
            }
        }

        private void savePart(Part part, string partPath)
        {
            try
            {
                string partJson = JsonConvert.SerializeObject(part, Formatting.Indented);
                File.WriteAllText(Path.Combine(jsonPath, setsPath), partJson);
            }
            catch (Exception e)
            {
                Debug.Log("there was a problem saving the part: " + e);
            }
        }

        private string checkForSetSubHeading()
        {
            partHeading = "";
            if (setNames.Count > 0)
            {
                partHeading = "will save a reference to this obj to: " + setNames[selectedSetIndex] + " - ";
            }

            if (set != null && set.songs != null && set.songs.Count > 0)
            {
                partHeading += set.songs[selectedSongIndex] + " - ";
            }
            else
            {
                partHeading += " no set ( check your json path in config ) ";
            }

            if (song != null && song.parts != null && song.parts.Count > 0)
            {
                partHeading += song.parts[selectedPartIndex] + " - ";
            }
            else
            {
                partHeading += " no song - ";
            }

            if (part != null && part.components != null && part.components.Count > 0)
            {
                partHeading += part.components[selectedComponentIndex];
            }
            else
            {
                partHeading += " no part";
            }
            return partHeading;
        }

        private void showTriggers()
        {
            if (this.state == "objTriggerSelection")
            {
                DrawHeading("Instantiation Trigger", "Select the trigger that will instantiate your object.");
                triggerSearchInput = EditorGUILayout.TextField("Search Triggers:", triggerSearchInput);
                List<Trigger> startsWithTriggerSuggestions = this.triggers
                    .Where(s => s.eventName.ToLower().StartsWith(triggerSearchInput.ToLower())).ToList();
                List<Trigger> containsTriggerSuggestions = this.triggers
                    .Where(s => s.eventName.ToLower().Contains(triggerSearchInput.ToLower())).Except(startsWithTriggerSuggestions).ToList();
                List<Trigger> filteredTriggerSuggestions = startsWithTriggerSuggestions
                    .Concat(containsTriggerSuggestions).ToList();

                string[] triggerSuggestionArray = filteredTriggerSuggestions
                    .Select(triggerSuggestion => triggerSuggestion.eventName).ToArray();

                if (selectedTriggerIndex >= 0 && selectedTriggerIndex < triggerSuggestionArray.Length)
                {
                    triggerSuggestionIndex = selectedTriggerIndex;
                }
                else
                {
                    //show all if there is no match. 
                    triggerSuggestionArray = this.triggers.Select(triggerSuggestion => triggerSuggestion.eventName).ToArray();
                    triggerSuggestionIndex = 0;
                    EditorGUILayout.HelpBox("No search results.", MessageType.Info);
                }

                selectedTriggerIndex = EditorGUILayout.Popup(triggerSuggestionIndex, triggerSuggestionArray);
                if (selectedTriggerIndex != lastSelectedTriggerIndex)
                {
                    Trigger selectedTrigger = this.triggers[selectedTriggerIndex];
                    o.trigger = selectedTrigger.eventName;
                    Debug.Log("o.trigger: " + o.trigger);
                    lastSelectedTriggerIndex = selectedTriggerIndex;
                }
            }
            else
            {
                DrawDisabledHeading("Instantiation Trigger", "Select the trigger that will instantiate your object.");
                if (GUILayout.Button("Assign Instantiation Triggers"))
                {
                    this.state = "objTriggerSelection";
                }
            }
        }

        private void showActiveTriggers()
        {
            if (this.state == "activeTriggerSelection")
            {
                DrawHeading("Triggered Behaviours", "Assign common object properties which are triggered");
                activeTriggerSearchInput = EditorGUILayout.TextField("Search Active:", activeTriggerSearchInput);
                // Calculate and display autocomplete activeTriggerSuggestions
                // prefer startsWith and then fallback to "contains"
                List<ActiveTriggerUI> startsWithactiveTriggerSuggestions = activeTriggerSuggestions
                    .Where(s => s.name.ToLower().StartsWith(activeTriggerSearchInput.ToLower())).ToList();
                List<ActiveTriggerUI> containsactiveTriggerSuggestions = activeTriggerSuggestions
                    .Where(s => s.name.ToLower().Contains(activeTriggerSearchInput.ToLower())).Except(startsWithactiveTriggerSuggestions).ToList();
                List<ActiveTriggerUI> filteredActiveTriggerSuggestions = startsWithactiveTriggerSuggestions
                    .Concat(containsactiveTriggerSuggestions).ToList();

                string[] activeTriggerSuggestionArray = filteredActiveTriggerSuggestions
                    .Select(activeTriggerSuggestion => activeTriggerSuggestion.name).ToArray();

                if (selectedActiveTriggerIndex >= 0 && selectedActiveTriggerIndex < activeTriggerSuggestionArray.Length)
                {
                    activeTriggerSuggestionIndex = selectedActiveTriggerIndex;
                }
                else
                {
                    // show all if there is no match.
                    // UI decision.
                    // show disclaimer ? 
                    activeTriggerSuggestionArray = activeTriggerSuggestions.Select(activeTriggerSuggestion => activeTriggerSuggestion.name).ToArray();
                    activeTriggerSuggestionIndex = 0;
                    EditorGUILayout.HelpBox("No search results.", MessageType.Info);
                }

                selectedActiveTriggerIndex = EditorGUILayout.Popup(activeTriggerSuggestionIndex, activeTriggerSuggestionArray);

                if (selectedActiveTriggerIndex != lastSelectedActiveTriggerIndex)
                {
                    lastSelectedActiveTriggerIndex = selectedActiveTriggerIndex;
                    //handle dropdown selection. 
                }

                bool foundActiveTriggers = filteredActiveTriggerSuggestions.Count > 0;
                if (foundActiveTriggers)
                {
                    buttonLabel = "Add " + filteredActiveTriggerSuggestions[activeTriggerSuggestionIndex].name;
                }
                else
                {
                    buttonLabel = "Add...";
                    GUI.enabled = false; //------
                }

                if (GUILayout.Button(buttonLabel))
                {
                    ActiveTriggerUI a = this.activeTriggerSuggestions[selectedActiveTriggerIndex];
                    this.activeTriggers.Add(a);
                    this.activeTriggerSuggestions.Remove(a);
                }

                GUI.enabled = true; //-----

                foreach (ActiveTriggerUI at in activeTriggers)
                {
                    EditorGUILayout.Space();

                    EditorGUILayout.BeginVertical(style_whiteBox);

                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField("x:", style_letterLabel, GUILayout.Width(20));
                    at.value.x = EditorGUILayout.FloatField(at.value.x);

                    EditorGUILayout.LabelField("y:", style_letterLabel, GUILayout.Width(20));
                    at.value.y = EditorGUILayout.FloatField(at.value.y);

                    EditorGUILayout.LabelField("z:", style_letterLabel, GUILayout.Width(20));
                    at.value.z = EditorGUILayout.FloatField(at.value.z);

                    EditorGUILayout.EndHorizontal();

                    if (at.usingValue2)
                    {

                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("x2:", style_letterLabel, GUILayout.Width(20));
                        at.value2.x = EditorGUILayout.FloatField(at.value2.x);

                        EditorGUILayout.LabelField("y2:", style_letterLabel, GUILayout.Width(20));
                        at.value2.y = EditorGUILayout.FloatField(at.value2.y);

                        EditorGUILayout.LabelField("z2:", style_letterLabel, GUILayout.Width(20));
                        at.value2.z = EditorGUILayout.FloatField(at.value2.z);

                        EditorGUILayout.EndHorizontal();

                        if (GUILayout.Button("Remove Second Vector"))
                        {
                            at.usingValue2 = false;
                        }
                    }
                    else
                    {
                        if (GUILayout.Button("Add Second Vector"))
                        {
                            at.usingValue2 = true;
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
            }
            else
            {
                DrawDisabledHeading("Triggered Behaviours", "Assign common object properties which are triggered");
                if (GUILayout.Button("Add Triggered Behaviours"))
                {
                    this.state = "activeTriggerSelection";
                }
            }
        }

        private string getRelativePath(string absolutePath, string baseDirectory)
        {
            // Unity prefers forward slashes. 
            absolutePath = absolutePath.Replace("\\", "/");
            baseDirectory = baseDirectory.Replace("\\", "/");

            // Use Path.GetRelativePath to calculate the relative path
            string relativePath = Path.GetRelativePath(baseDirectory, absolutePath);

            // Ensure the relative path uses forward slashes
            relativePath = relativePath.Replace("\\", "/");

            return relativePath;
        }

        private string getTempPrefabPath(string prefabPath)
        {
            //taking the name, removing the folders, adding only one .prefab.
            string tempPrefabRoot = "Assets/TempPrefabs/";
            string rawFileName = Path.GetFileNameWithoutExtension(prefabPath);
            string tempPrefabName = Path.ChangeExtension(rawFileName, "prefab");
            return tempPrefabRoot + tempPrefabName;
        }

        private string getDestinationPrefabPath(string prefabPath)
        {
            //display version for heading.  eventually needs a create function. 
            string prefabRoot = "Assets/Resources/Private/"; //does this need to be in config file ? 
            string rawFileName = Path.GetFileNameWithoutExtension(prefabPath);
            string destinationPrefabName = Path.ChangeExtension(rawFileName, "prefab");
            string destinationPath = Path.GetDirectoryName(prefabPath);
            return prefabRoot + destinationPath + destinationPrefabName;
        }

        private void showCreateObjButton()
        {

            string tempPrefabPath = getTempPrefabPath(this.prefabPath);
            string destinationPrefabPath = getDestinationPrefabPath(this.prefabPath);

            //string prefabRoot = Assets/;" //if moving to resources doesn't work. 
            if (this.state == "saveObj")
            {
                if (string.IsNullOrEmpty(this.prefabPath))
                {
                    // root is actually "Assets/Resources" before it's an assetBundle. 
                    // need something like this for assets going forward.  
                    // MyPrefabData data = ScriptableObject.CreateInstance<MyPrefabData>();
                    // AssetDatabase.CreateAsset(data, "Assets/Resources/MyPrefabData.asset");
                    // MyPrefabData data = Resources.Load<MyPrefabData>("MyPrefabData");
                    prefabPath = prefabName + ".prefab";
                }
                //don't create it yet. 
                DrawHeading("Save Obj", "Save prefab to " + destinationPrefabPath);
                this.prefabPath = EditorGUILayout.TextField("Prefab Path:", prefabPath);
                EditorGUILayout.LabelField("Temporary path for prefab within Resources folder for testing. Eventually you should save this prefab into an asset bundle.", style_subheading);
                if (GUILayout.Button("Save Obj"))
                {
                    foreach (ActiveTriggerUI aui in activeTriggers)
                    {
                        FieldInfo field = o.GetType().GetField(aui.type);
                        ActiveTrigger a = new ActiveTrigger();
                        a.value = new Vec(aui.value.x, aui.value.y, aui.value.z, aui.value2.x, aui.value2.y, aui.value2.z);
                        field.SetValue(o, a);
                    }
                    //------------creation
                    Directory.CreateDirectory(Path.GetDirectoryName(destinationPrefabPath));
                    PrefabUtility.SaveAsPrefabAsset(gameObject, tempPrefabPath);
                    Debug.Log("Obj Prefab created at: " + tempPrefabPath + " now moving to: " + destinationPrefabPath);
                    AssetDatabase.MoveAsset(tempPrefabPath, destinationPrefabPath);
                }
                if (GUILayout.Button("Cancel"))
                {
                    this.state = "";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(this.prefabPath))
                {
                    //root is actually "Assets/Resources" before it's an assetBundle. 
                    prefabPath = prefabName + ".prefab";
                }
                DrawDisabledHeading("Save Obj", "Save obj prefab to " + destinationPrefabPath);
                if (GUILayout.Button("Save Obj"))
                {
                    this.state = "saveObj";
                }
            }
        }

        private void OnGUI()
        {
            loadStyles();
            EditorGUILayout.Separator();
            showGameObject();
            DrawDashedLine(Color.white);
            EditorGUI.BeginDisabledGroup(gameObject == null);
            showPartDropDowns();
            DrawDashedLine(Color.white);
            showTriggers();
            DrawDashedLine(Color.white);
            showActiveTriggers();
            DrawDashedLine(Color.white);
            showCreateObjButton();
            EditorGUI.EndDisabledGroup();
        }
    }
}
#endif