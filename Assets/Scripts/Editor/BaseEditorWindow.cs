using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System;
using System.Reflection;
using flyby.Controllers;
using Newtonsoft.Json;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using flyby.Sets;
using flyby.Core;
using flyby.UI;

namespace flyby
{
    public class BaseEditorWindow : EditorWindow
    {
        //check
        private Config config;
        private string configPath = "config.json"; //need way to save this. ( make an input? ) 
        private JsonSerializerSettings JSONSettings;
        protected string jsonPath = "/";
        protected GUIStyle style_helpText;
        protected GUIStyle style_text;
        protected GUIStyle style_whiteBox;
        protected GUIStyle style_letterLabel;
        protected GUIStyle style_heading;
        protected GUIStyle style_heading_disabled;
        protected GUIStyle style_box;
        protected GUIStyle style_subheading;

        public virtual void loadStyles()
        {
            //TODO - put this in a base editor class. 
            style_text = new GUIStyle(EditorStyles.label);
            style_text.normal.textColor = Color.black;
            style_text.alignment = TextAnchor.MiddleCenter;

            style_helpText = new GUIStyle(EditorStyles.helpBox);
            style_helpText.normal.textColor = Color.red;

            style_whiteBox = new GUIStyle(GUI.skin.box);
            style_whiteBox.normal.background = EditorGUIUtility.whiteTexture;
            style_whiteBox.normal.textColor = Color.black;
            style_whiteBox.alignment = TextAnchor.MiddleCenter;

            style_letterLabel = new GUIStyle(GUI.skin.label);
            style_letterLabel.alignment = TextAnchor.MiddleRight;
            style_letterLabel.normal.textColor = Color.black;

            style_heading = new GUIStyle(EditorStyles.label);
            style_heading.fontSize = 15;
            style_heading.padding.bottom = -8;
            style_heading.padding.top = -8;
            style_heading.margin.bottom = 8;
            style_heading.margin.top = 8;
            style_heading.fontStyle = FontStyle.Bold;
            style_heading.alignment = TextAnchor.MiddleLeft;

            style_heading_disabled = new GUIStyle(EditorStyles.label);
            style_heading_disabled.normal.textColor = new Color(0.38f, 0.4f, 0.4f);
            style_heading_disabled.fontSize = 15;
            style_heading_disabled.padding.bottom = -8;
            style_heading_disabled.padding.top = -8;
            style_heading_disabled.margin.bottom = 8;
            style_heading_disabled.margin.top = 8;
            style_heading_disabled.fontStyle = FontStyle.Bold;
            style_heading_disabled.alignment = TextAnchor.MiddleLeft;

            style_subheading = new GUIStyle(EditorStyles.label);
            style_subheading.fontSize = 10;
            style_subheading.wordWrap = true;
            style_heading.alignment = TextAnchor.MiddleLeft;

            style_box = new GUIStyle(GUI.skin.box);
            style_box.padding = new RectOffset(5, 10, 5, 10);
        }

        public virtual void DrawHeading(string headingText, string subHeading = "")
        {
            //TODO - put this in a base editor class. 
            GUILayout.BeginVertical(style_box);
            EditorGUILayout.LabelField(headingText, style_heading);
            if (!string.IsNullOrEmpty(subHeading))
            {
                EditorGUILayout.LabelField(subHeading, style_subheading);
            }
            GUILayout.EndVertical();
        }

        public virtual void DrawDisabledHeading(string headingText, string subHeading = "")
        {
            //TODO - put this in a base editor class. 
            GUILayout.BeginVertical(style_box);
            EditorGUILayout.LabelField(headingText, style_heading_disabled);
            if (!string.IsNullOrEmpty(subHeading))
            {
                EditorGUILayout.LabelField(subHeading, style_subheading);
            }
            GUILayout.EndVertical();
        }

        public virtual Config loadConfig()
        {
            //TODO - make this part of base editor class. 
            if (Config.instance == null)
            {
                string configJSON = Json.getJSON(configPath);
                this.config = JsonConvert.DeserializeObject<Config>(configJSON, this.JSONSettings);
                this.jsonPath = this.config.jsonPath;
                return this.config;
            }
            else
            {
                return Config.instance;
            }
        }

        public virtual void DrawDashedLine(Color color, float width = 1f, float dashSize = 4f, float gapSize = 2f)
        {
            //TODO - put this in a base editor class. 
            EditorGUILayout.Space();
            Rect rect = GUILayoutUtility.GetLastRect();
            Vector2 startPoint = new Vector2(rect.x, rect.y + rect.height / 2);
            Vector2 endPoint = new Vector2(rect.x + rect.width, rect.y + rect.height / 2);

            Handles.BeginGUI();
            Handles.color = color;

            Vector2 dashDirection = (endPoint - startPoint).normalized * dashSize;
            float distance = Vector2.Distance(startPoint, endPoint);
            Vector2 currentPoint = startPoint;

            while (currentPoint.x < endPoint.x)
            {
                Vector2 nextPoint = currentPoint + dashDirection;
                Handles.DrawLine(currentPoint, nextPoint);
                currentPoint += dashDirection + new Vector2(gapSize, 0f);
            }
            Handles.EndGUI();
        }
    }
}
#endif