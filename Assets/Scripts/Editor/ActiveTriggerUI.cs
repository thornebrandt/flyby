using UnityEngine;
using flyby.Active;

namespace flyby {
    namespace UI {
        public class ActiveTriggerUI {
            public string name;
            public string type;
            public ActiveTrigger a;
            public bool usingValue;
            public bool usingValue2;
            public Vector3 value;
            public Vector3 value2;

            public ActiveTriggerUI() {
                this.value = new Vector3();
                this.value2 = new Vector3();
            }
        }
    }
}