using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System;

namespace flyby {
    public class TimeButton : EditorWindow {
        private bool buttonClicked = false;
        private DateTime buttonClickTime;
        private string buttonLabel = "Click Me";

        [MenuItem("FlyBy/TimeButton")]

        static void Init() {
            TimeButton window = (TimeButton)EditorWindow.GetWindow(typeof(TimeButton));
        }

        private void OnGUI() {
            if (GUILayout.Button(buttonLabel)) {
                buttonClicked = true;
                buttonClickTime = DateTime.Now;
            }

            if (buttonClicked) {
                TimeSpan elapsedTime = DateTime.Now - buttonClickTime;
                buttonLabel = "Seconds Elapsed: " + elapsedTime.TotalSeconds.ToString("F2");
            }
        }
    }
}
#endif