using System;
using System.Collections.Generic;
using UnityEngine;

public class AssignDynamicBones : MonoBehaviour {
    //TODO - rename dynamic bones controller - 
    // assign bones from 3dflyby

    //NOTE if this isn't working - change Edit > Project Settings > Script Execution Order and make Dynamic Bones a negative number
    public List<Transform> roots; //line up to the json
    public DynamicBoneColliderBase[] externalColliders;
    private List<DynamicBoneColliderBase> allColliders;
    private List<DynamicBone> dbs;

    public List<BoneChain> boneChains;

    void Start() {
        this.setup();
    }

    void setup() {
        setupChain(boneChains);
        List<DynamicBoneColliderBase> allColliders = attachDynamicColliders(boneChains);
        assignDynamicBones(allColliders);
    }

    void setupChain(List<BoneChain> boneChains) {
        dbs = new List<DynamicBone>();
        int length = Mathf.Min(boneChains.Count, roots.Count);
        for (int i = 0; i < length; i++) {
            BoneChain chain = boneChains[i];
            chain.root = roots[i]; //needs to be assinged in the editor;
            Transform[] bones = chain.root.GetComponentsInChildren<Transform>();
            chain.bones = bones;
        }
    }

    void assignDynamicBones(List<DynamicBoneColliderBase> allColliders) {
        foreach (BoneChain chain in boneChains) {
            List<DynamicBoneColliderBase> collidersMasked = getMaskedColliders(chain);
            DynamicBone new_db = gameObject.AddComponent<DynamicBone>();
            Debug.Log("assigning dynamic bone to " + new_db.gameObject.name);
            new_db.m_Root = chain.root;
            new_db.m_Colliders = collidersMasked;
            new_db.m_Damping = chain.damping;
            new_db.m_DampingDistrib = chain.dampingDistrib;
            new_db.m_Elasticity = chain.elasticity;
            new_db.m_ElasticityDistrib = chain.elasticityDistrib;
            new_db.m_Stiffness = chain.stiffness;
            new_db.m_StiffnessDistrib = chain.stiffnessDistrib;
            new_db.m_Force = chain.force;
            new_db.m_Radius = chain.colliderRadius;
            new_db.m_UpdateRate = 45;
            new_db.m_DistantDisable = true;
            dbs.Add(new_db);
            chain.db = new_db;
        }
    }

    public void addMoreColliders(List<DynamicBoneColliderBase> moreColliders) {
        List<DynamicBoneColliderBase> collidersMasked = new List<DynamicBoneColliderBase>(moreColliders);
        foreach (BoneChain chain in boneChains) {
            List<DynamicBoneColliderBase> thisColliders = chain.colliders;
            foreach (DynamicBoneColliderBase collider in allColliders) {
                foreach (DynamicBoneColliderBase thisCollider in thisColliders) {
                    if (collider == thisCollider) {
                        collidersMasked.Remove(collider);
                    }
                }
            }
        }

        DynamicBone[] dbs = gameObject.GetComponents<DynamicBone>();
        foreach (DynamicBone db in dbs) {
            db.m_Colliders.AddRange(collidersMasked);
        }
    }

    List<DynamicBoneColliderBase> getMaskedColliders(BoneChain chain) {
        List<DynamicBoneColliderBase> thisColliders = chain.colliders;
        List<DynamicBoneColliderBase> collidersMasked = new List<DynamicBoneColliderBase>(allColliders);

        foreach (DynamicBoneColliderBase collider in allColliders) {
            foreach (DynamicBoneColliderBase thisCollider in thisColliders) {
                if (collider == thisCollider) {
                    collidersMasked.Remove(collider);
                }
            }
        }
        if (chain.addExternalColliders) {
            collidersMasked.AddRange(externalColliders);

        }
        return collidersMasked;
    }

    List<DynamicBoneColliderBase> attachDynamicColliders(List<BoneChain> boneChains) {
        allColliders = new List<DynamicBoneColliderBase>();
        foreach (BoneChain chain in boneChains) {
            chain.colliders = new List<DynamicBoneColliderBase>();
            for (int i = 0; i < chain.bones.Length; i++) {
                if (i % chain.colliderSpacing == 0) {
                    Transform bone = chain.bones[i];
                    DynamicBoneCollider collider = bone.gameObject.AddComponent<DynamicBoneCollider>();
                    collider.m_Radius = chain.colliderRadius;
                    chain.colliders.Add(collider);
                    allColliders.Add(collider);
                }
            }
        }

        allColliders.AddRange(externalColliders);

        return allColliders;
    }
}

public class BoneChainArray {
    public BoneChain global;
    public List<BoneChain> boneChains;
    public BoneChainArray() {
        this.global = new BoneChain();
        this.boneChains = new List<BoneChain>();
    }
}

[Serializable]
public class BoneChain {
    public string label;
    public Transform root;
    public float colliderRadius;
    public float colliderSpacing;
    public float damping = 0.8f;
    public AnimationCurve dampingDistrib;
    public float elasticity;
    public AnimationCurve elasticityDistrib;
    public float stiffness;
    public AnimationCurve stiffnessDistrib;
    [HideInInspector]
    public List<DynamicBoneColliderBase> colliders;
    public bool addExternalColliders;
    [HideInInspector]
    public Transform[] bones;
    public Quaternion[] originalRotations;
    public Quaternion[] currentRotations;
    public Quaternion[] targetRotations;
    public Vector3 force;
    public int smoothness; //distributes curves over multiple joints
    [HideInInspector]
    public DynamicBone db;

    public BoneChain() {
        Keyframe[] ks = new Keyframe[2];
        ks[0] = new Keyframe(0, 0.2f);
        ks[1] = new Keyframe(1, 1);
        this.dampingDistrib = new AnimationCurve(ks);
        this.colliderRadius = 0.003f;
        this.colliderSpacing = 2;
        this.damping = 0.9f;
        this.elasticity = 0.4f;
        this.stiffness = 0.07f;
        this.smoothness = 1; //bone distribution, should probably delete soon 
        this.colliders = new List<DynamicBoneColliderBase>();
        this.addExternalColliders = true;
    }

    public BoneChain(BoneChain chain) {
        this.label = chain.label;
        this.dampingDistrib = chain.dampingDistrib;
        this.colliderRadius = chain.colliderRadius;
        this.colliderSpacing = chain.colliderSpacing;
        this.damping = chain.damping;
        this.elasticity = chain.elasticity;
        this.stiffness = chain.stiffness;
        this.smoothness = chain.smoothness;
        this.colliders = chain.colliders;
        this.addExternalColliders = chain.addExternalColliders;
    }
}