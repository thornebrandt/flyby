using UnityEngine;
using System;

namespace flyby
{
    namespace Tools
    {
        public static class Rand
        {
            public static float Between(float num1, float num2, int seed = -1)
            {
                if (seed > 0)
                {
                    UnityEngine.Random.InitState(seed + (int)(Time.time * 1000));
                }

                if (num2 == -999)
                {
                    if (num1 == -999)
                    {
                        return 0;
                    }
                    return num1;

                }
                if (num1 == num2)
                {
                    return num1;
                }
                return UnityEngine.Random.Range(num1, num2);
            }
        }
    }
}