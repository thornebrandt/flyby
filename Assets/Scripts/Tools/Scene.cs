using UnityEngine;

namespace flyby
{
    namespace Tools
    {
        public static class Scene
        {
            public static void setLayer(GameObject parent, int layer)
            {
                parent.layer = layer;
                foreach (Transform child in parent.transform)
                {
                    GameObject go = child.gameObject;
                    if (go != null)
                    {
                        go.layer = layer;
                        setLayer(go, layer);
                    }
                }
            }

            public static string getLayerName(GameObject go)
            {
                int layerIndex = go.layer;
                string layerName = LayerMask.LayerToName(layerIndex);
                return layerName;
            }

            public static void setLayer(GameObject parent, string layerName)
            {
                if (string.IsNullOrEmpty(layerName))
                {
                    layerName = "Default";
                }
                int layerIndex = LayerMask.NameToLayer(layerName);
                if (layerIndex == -1)
                {
                    Debug.Log("Could not find layer: " + layerName);
                    layerIndex = 0;
                }
                setLayer(parent, layerIndex);
            }
        }
    }
}



