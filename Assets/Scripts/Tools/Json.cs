using System.IO;
using UnityEngine;

namespace flyby {
    namespace Tools {
        public static class Json {
            public static string getJSON(string path) {
                string filePath = path;
                if (File.Exists(filePath)) {
                    using (StreamReader sr = new StreamReader(filePath)) {
                        string line = "";
                        string json = "";
                        int i = 0;
                        while ((line = sr.ReadLine()) != null) {
                            json += line;
                            i++;
                        }
                        return json;
                    }
                }
                Debug.Log("JSON PATH MISSING: could not find " + filePath);
                return "";
            }
        }
    }
}