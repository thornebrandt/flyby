using System;
using flyby.Core;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    namespace Tools
    {
        public static class Math
        {
            public static List<Vec> SortVecsByX(List<Vec> vecs)
            {
                vecs.Sort((a, b) => a.x.CompareTo(b.x));
                return vecs;
            }

            public static float Round(float value, int decimalPoints)
            {
                float mult = Mathf.Pow(10.0f, (float)decimalPoints);
                return Mathf.Round(value * mult) / mult;
            }

            public static bool RoughEquals(Color color1, Color color2)
            {
                return RoughEquals(color1, color2, true);
            }

            public static bool RoughEquals(Color color1, Color color2, bool verbose)
            {
                int decimalPoints = 3;
                bool equals = (
                    Round(color1.r, decimalPoints) == Round(color2.r, decimalPoints) &&
                    Round(color1.g, decimalPoints) == Round(color2.g, decimalPoints) &&
                    Round(color1.b, decimalPoints) == Round(color2.b, decimalPoints) &&
                    Round(color1.a, decimalPoints) == Round(color2.a, decimalPoints)
                );
                if (!equals && verbose)
                {
                    Debug.Log(color1 + " does not equal " + color2 + " exactly...");
                }
                return equals;
            }


            public static bool RoughEquals(float float1, float float2)
            {
                return RoughEquals(float1, float2, true);
            }

            public static bool RoughEquals(float f1, float f2, bool verbose)
            {
                int decimalPoints = 2;
                bool equals = Round(f1, decimalPoints) == Round(f2, decimalPoints);
                if (!equals && verbose)
                {
                    Debug.Log(f1 + " does not equal " + f2 + " exactly...");
                }
                return equals;
            }

            public static bool RoughEquals(Vector3 v1, Vector3 v2)
            {
                return RoughEquals(v1, v2, true);
            }

            public static bool RoughEquals(Vector3 v1, Vector3 v2, bool verbose)
            {
                int decimalPoints = 2;
                bool equals =
                (
                    Round(v1.x, decimalPoints) == Round(v2.x, decimalPoints) &&
                    Round(v1.y, decimalPoints) == Round(v2.y, decimalPoints) &&
                    Round(v1.z, decimalPoints) == Round(v2.z, decimalPoints)
                );
                if (!equals && verbose)
                {
                    string v1_string = "(" + v1.x + ", " + v1.y + ", " + v1.z + ")";
                    string v2_string = "(" + v2.x + ", " + v2.y + ", " + v2.z + ")";
                    Debug.Log(v1_string + " does not equal " + v2_string + " exactly...");
                }
                return equals;
            }

            public static bool RoughEquals(Quaternion rotation1, Quaternion rotation2)
            {
                return RoughEquals(rotation1, rotation2, true);
            }

            public static bool RoughEquals(Quaternion rotation1, Quaternion rotation2, bool verbose)
            {
                float acceptableRange = 0.00002f;
                bool equals = 1 - Mathf.Abs(Quaternion.Dot(rotation1, rotation2)) < acceptableRange;
                if (!equals && verbose)
                {
                    Debug.Log(rotation1 + ", (" + rotation1.eulerAngles + ") != " + rotation2 + ", (" + rotation2.eulerAngles + ")");
                }
                return equals;
            }

            public static float Lerp(float num1, float num2, float t)
            {
                if (num2 == -999)
                {
                    if (num1 == -999)
                    {
                        return 0;
                    }
                    return num1;
                }
                if (num1 == num2)
                {
                    return num1;
                }
                return Mathf.Lerp(num1, num2, t);
            }

            public static Vector3 Clamp(Vector3 value, float min, float max)
            {
                return new Vector3(
                    Mathf.Clamp(value.x, min, max),
                    Mathf.Clamp(value.y, min, max),
                    Mathf.Clamp(value.z, min, max)
                );
            }

            public static float Add(float num1, float num2)
            {
                //adding if some numbers are left out. 
                //used in arrays in vecs - after parsing but before instantiation. 
                if (num1 == -999)
                {
                    if (num2 == -999)
                    {
                        return -999;
                    }
                    else
                    {
                        return num2;
                    }
                }
                else
                {
                    if (num2 == -999)
                    {
                        return num1;
                    }
                    else
                    {
                        return num1 + num2;
                    }
                }
            }

            public static float Remap(float x1, float x2, float y1, float y2, float value)
            {
                float normalizedValue = Mathf.InverseLerp(x1, x2, value);
                return Mathf.Lerp(y1, y2, normalizedValue);
            }

            public static Matrix4x4 MatrixLerp(Matrix4x4 from, Matrix4x4 to, float t)
            {
                Matrix4x4 transitionMatrix = new Matrix4x4();
                for (int i = 0; i < 16; i++)
                {
                    transitionMatrix[i] = Mathf.Lerp(from[i], to[i], t);
                }
                return transitionMatrix;
            }

            public static float getNearestMultiple(float _value, float multiple)
            {
                if (multiple <= 0) return _value;
                double value = _value / (double)multiple; //lol -- thing?
                float roundedResult = (float)System.Math.Round(value) * multiple;
                return roundedResult;
            }

            public static Vector3 NormalizeVector3(Vector3 original, Vector3 min, Vector3 max)
            {
                //thing of this as a three-dimensional inverse lerp.
                float normalizedX = (max.x != min.x) ? (original.x - min.x) / (max.x - min.x) : 0f;
                float normalizedY = (max.y != min.y) ? (original.y - min.y) / (max.y - min.y) : 0f;
                float normalizedZ = (max.z != min.z) ? (original.z - min.z) / (max.z - min.z) : 0f;

                return new Vector3(normalizedX, normalizedY, normalizedZ);
            }


            public static float NormalizeAngle(float angle, float minAngle, float maxAngle)
            {
                //an inverse lerp for two degree angles.  ( such as 270 and 90 , which looks around 360. ) 
                //note: this operates clockwise. This can't be reversed.

                //looped through 360 here. 
                float loopedAngle = angle % 360;
                if (loopedAngle < 0) loopedAngle += 360;

                float loopedMinAngle = minAngle % 360;
                if (loopedMinAngle < 0) loopedMinAngle += 360;

                float loopedMaxAngle = maxAngle % 360;
                // if (loopedMaxAngle < 0) loopedMaxAngle += 360;

                float rangeStart = loopedMinAngle;
                float rangeEnd = loopedMaxAngle + 360;

                if (rangeEnd <= rangeStart) rangeEnd += 360;

                if (loopedAngle < rangeStart) //should be able to loop around the same angle. 
                {
                    loopedAngle += 360;
                }

                float normalizedValue = (loopedAngle - rangeStart) / (rangeEnd - rangeStart);

                float clampedValue = Mathf.Clamp01(normalizedValue);
                if (clampedValue == 1 || clampedValue == 0)
                {
                    loopedAngle = loopedAngle % 360;
                    if (loopedMaxAngle < loopedMinAngle)
                    {
                        loopedMinAngle = loopedMinAngle % 360;
                        loopedMaxAngle = loopedMaxAngle % 360;
                    }
                    float distanceToMin = Mathf.Abs(loopedAngle - loopedMinAngle) % 360;
                    float distanceToMax = Mathf.Abs(loopedAngle - loopedMaxAngle) % 360;
                    clampedValue = distanceToMin <= distanceToMax ? 0 : 1;
                }

                return clampedValue;
            }

            public static float GetAngle(Vector2 from, Vector2 to)
            {
                Vector2 direction = to - from;
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
                return angle < 0 ? 360 + angle : angle;
            }
        }
    }
}