using UnityEngine;


namespace flyby {
    namespace Tools {
        public static class Space {
            public static Vector3 getSize(GameObject parentGO) {
                Renderer[] rr = parentGO.GetComponentsInChildren<Renderer>();
                if (rr.Length == 0) {
                    return Vector3.one;
                }
                Bounds b = rr[0].bounds;
                foreach (Renderer r in rr) {
                    b.Encapsulate(r.bounds);
                }
                Vector3 size = new Vector3(
                    Mathf.Max(0.1f, b.size.x),
                    Mathf.Max(0.1f, b.size.y),
                    Mathf.Max(0.1f, b.size.z)
                );
                return size;
            }

            public static Vector3 getCenterHierarchy(GameObject parentGO) {
                //center of geometry with many children. 
                Renderer[] rr = parentGO.GetComponentsInChildren<Renderer>();
                if (rr.Length == 0) {
                    return Vector3.zero;
                }
                Bounds b = rr[0].bounds;
                foreach (Renderer r in rr) {
                    b.Encapsulate(r.bounds);
                }
                return b.center;
            }

            public static Vector3 getCenter(GameObject go) {
                //fast center of geometry.
                Renderer r = go.GetComponent<Renderer>();
                if (r != null) {
                    return r.bounds.center;
                }
                return Vector3.zero;
            }
        }
    }
}



