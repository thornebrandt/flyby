using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorColorBus : MonoBehaviour {
    // this script circumvents the unity bug mentioned here. 
    // https://forum.unity.com/threads/bug-animated-material-color-always-returns-default-value.465494/

    // in which the material property animated within mecanin is not accessible.  

    // a workaround is to animate this property instead, and assign it.  

    public Color animatedColor;
    public bool animating;
    private Renderer _renderer;
    public string materialProperty = "_BaseColor";
    private Material material;

    void Awake() {
        if (this.enabled) {
            _renderer = gameObject.GetComponent<Renderer>();
            material = _renderer.material;
        }
    }

    void Update() {
        if (animating) {
            material.SetColor(materialProperty, animatedColor);
        }
    }
}
