using com.rfilkov.components;
using flyby.Active;

namespace flyby
{
    namespace Core
    {
        public class Body
        {
            public string id; //name of body.
            public ulong kinectUserId; //syncing with kinect userId.
            public int kinectBodyIndex; //kinect's body index. strange behavior. needs to update avatarController.
            public int i;
            public float lostTrackingTime; //time since last tracking.
            public string startTrigger; //start and end if you use sustain. 
            public string xTrigger; //x position of hips, normalized. 
            public string yTrigger; //y position of hips, normalized.
            public string zTrigger; //z position of hips, normalized.
            public string rightHandXTrigger; //relative to hips, normalized. ( -0.7 to 0.7 )
            public string rightHandYTrigger; //relative to hips, normailized.  
            public string rightHandZTrigger; //relative to hips, normalized
            public string rightHandSpeed; //experimental. 
            public string leftHandXTrigger; //relative to hips, normalized
            public string leftHandYTrigger; //relative to hips, normalized
            public string leftHandZTrigger;
            public string leftHandSpeed; //experimental.
            public string leftFootXTrigger; //relative to hips, normalized
            public string leftFootYTrigger; //relative to hips, normalized
            public string leftFootZTrigger;
            public string rightFootXTrigger; //relative to hips, normalized
            public string rightFootYTrigger; //relative to hips, noramlized
            public string rightFootZTrigger;
            public string handsZTriggerTrigger; //distance between hands, normalized.
            public string wingspanTrigger; //distance between hands
            public string feetSpreadTrigger; //distance between feet
            public string tallnessTrigger; //distance between hands and feet.
            public string handAngleTrigger; //z-angle between hands ( airplane position ) 
            public string headTiltTrigger; //z-angle - night at the roxburry style. 
            public string headNodTrigger; //x-angle nodding movement. 
            public string headShakeTrigger; //y-angle shaking movement.
            public string leanTrigger; //similar to surfboard . 
            public bool disabled; //experimental.


            public bool active; //not implemented yet? turned on during intro. turns off when sustain is released.
            public AvatarController avatarController; //will link the object to the instance.
            public ActiveKinect activeKinect; //will link the object to the instance.
            //TODO - parentBody. 
            public Body()
            {
                kinectUserId = 999;
            }
        }
    }
}
