using System;
using System.Reflection;
using flyby.Tools;
using UnityEngine;
using UnityEngine.Video;
using System.Collections.Generic;

namespace flyby
{
    namespace Core
    {
        [Serializable]
        public class Media
        {
            public int oi; //object index, for debugging. 
            public string id;
            public string path;
            public string url; // manually setting up for now - needs optimization can re-use path.. 
            public int repeat; //-1 is a loop. 0 means play once. ( hopefully ) 
            public int cols; //for spriteSheets. 
            public int rows; //for spriteSheets. 
            public int totalFrames; //for spriteSheets, if cells is different than cols vs rows. 
            public int stops; //animation term, inverse of fps. negative stops will freeze animation. 
            public int stops2; //to randomize stops.
            public float fps; //frames per second. ( used for activeParticle. ) TODO - add some functions. 
            public float fps2; //to randomize fps.
            public bool pingpong; //loop back and forth if repeat is greater than 0.
            public int numVariants; //automatically gets an array of media/clips 
            public int variant_i; //private
            public int initialVariant; //offset for variant loop. 
            public string _path; //private, generated path.
            public float width; //private
            public float height; //private
            public float ratio; //private
            public int timesRepeated; //private times repeated.
            public int currentFrame; //private
            public int startFrame; //base 0, starting frame index. 
            public int startFrame2; //base 0, to randomize start frame. 
            public int endFrame; //base 0, end frame index. 
            public int endFrame2; //base 0, to randomize end frame. 
            public int frame; //private frame index. 
            public bool hideOnComplete;
            public bool ended; //private
            public bool fullWidth; //not
            public bool fullHeight;
            public Texture2D loadedTexture; //private for loaded texture. 
            public List<Texture2D> loadedTextures; //for array of variants.
            public VideoClip loadedVideoClip;
            public List<VideoClip> loadedVideoClips;
            public int sortingOrder; //renderOrder
            public bool sortByZ;
            public bool alwaysFront; //uses songs saved sortingOrder parameter. ( new - uses song ) 

            public Media()
            {
                this.stops = 3;
                this.stops2 = -999;
                this.fps = 20;
                this.fps2 = -999;
                this.repeat = -1; //-1 means loop. 0 means play once. ( hopefully ) 
                this.cols = 1;
                this.rows = 1;
                this.ratio = 1;
                this.startFrame2 = -999;
                this.endFrame = -1;
                this.endFrame2 = -999;
                //this.sortByZ = true; //default behaviour when making 2d sprites that don't zfight. 
            }

            public Media(Media media)
            {
                foreach (FieldInfo field in media.GetType().GetFields())
                {
                    var type = Nullable.GetUnderlyingType(field.FieldType) ?? field.FieldType;
                    field.SetValue(this, field.GetValue(media));
                }
            }

            public override bool Equals(System.Object obj)
            {
                if (obj is null)
                {
                    return false;
                }
                Media media = obj as Media;
                return this.id == media.id &&
                this.url == media.url &&
                this.path == media.path &&
                this.repeat == media.repeat &&
                this.cols == media.cols &&
                this.rows == media.rows &&
                this.totalFrames == media.totalFrames &&
                this.stops == media.stops &&
                this.stops2 == media.stops2 &&
                this.fps == media.fps &&
                this.fps2 == media.fps2 &&
                this.pingpong == media.pingpong &&
                this.numVariants == media.numVariants &&
                this.startFrame == media.startFrame &&
                this.startFrame2 == media.startFrame2 &&
                this.endFrame == media.endFrame &&
                this.endFrame2 == media.endFrame2 &&
                this.hideOnComplete == media.hideOnComplete &&
                this.sortingOrder == media.sortingOrder &&
                this.sortByZ == media.sortByZ &&
                this.alwaysFront == media.alwaysFront;
            }


            public static bool operator ==(Media lhs, Media rhs)
            {
                if (lhs is null)
                {
                    if (rhs is null)
                    {
                        return true;
                    }
                    return false;
                }
                return lhs.Equals(rhs);
            }

            public static bool operator !=(Media lhs, Media rhs) => !(lhs == rhs);

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }
    }
}