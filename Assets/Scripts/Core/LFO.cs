using System;
using UnityEngine;

namespace flyby
{
    namespace Core
    {
        public class LFO
        {
            //note LFO returns normalized value between 0 and 1, amplitude should be multiplied by this value externally. 
            public enum Waveform
            {
                Sine,
                Saw,
                Triangle,
                Square,
                Custom
            }

            public string trigger;
            public int customCurveIndex;
            public float offset;
            private Waveform _waveform;
            private float _frequency;
            private AnimationCurve _customCurve;

            public float frequency
            {
                get => _frequency;
                set
                {
                    if (value <= 0) throw new ArgumentException("Frequency must be greater than 0.");
                    _frequency = value;
                }
            }

            public Waveform waveform
            {
                get => _waveform;
                set => _waveform = value;
            }

            public LFO()
            {
                waveform = Waveform.Sine;
                frequency = 1f; // Duration of one cycle in seconds
                offset = 0f; //offset is normalized.  ( put 0.25 against another axis make a circle ) 
            }

            public float GetValue(float time)
            {
                //value is normalized between 0 and 1. 
                float phase = (time / frequency + offset) % 1f;

                switch (waveform)
                {
                    case Waveform.Sine:
                        return 0.5f * (1 + Mathf.Sin(2 * Mathf.PI * phase));
                    case Waveform.Saw:
                        return phase;
                    case Waveform.Triangle:
                        return 1 - Mathf.Abs(1 - 2 * phase);
                    case Waveform.Square:
                        return phase < 0.5f ? 1 : 0;
                    case Waveform.Custom:
                        return _customCurve.Evaluate(phase);
                    default:
                        return 0.5f * (1 + Mathf.Sin(2 * Mathf.PI * phase));
                }
            }
        }
    }
}