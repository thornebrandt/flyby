using System;
using UnityEngine;
using flyby.Triggers;
using System.Collections.Generic;

namespace flyby
{
    namespace Core
    {
        [System.Serializable]
        public class Role
        {
            //roles are set up on parts. They are abstractly meant to connect specific groups of triggers to specific users
            // for consistent manipulation of objects.
            //roles are setup for sockets and we are attempting to connect them to kinect body indexes. 

            public string id; //name of role.
            public string description; //this is sent back to the client and displayed to describe their function. 
            public bool disabled; //not implemented. 
            public bool changed; //any change has happened. 

            public string startTrigger; //this is a sustain trigger. //TODO - make introTrigger. 
            public string xTrigger;  //might need HUGE refactoring to use vector3s as the values we send. 
            public string yTrigger;

            //gamepad
            public string upTrigger;
            public string rightTrigger;
            public string downTrigger;
            public string leftTrigger;

            //triggers are base 1.  a and b are also trigger1 and trigger2. 

            //todo - replace trigger1 etc eith this array of triggerUIs.
            public List<TriggerUI> triggers;

            public string trigger1; //the actual trigger eventName, for impulsing for existing objects. 
            public string trigger1Display; //display name for trigger1. Such as "Strum"

            public string trigger2;
            public string trigger2Display;

            //TODO rest of triggers. 
            //TODO - possibly move into "interface" class.  ( interface is a reserved word. ) 
            public string interfaceType; //interface is a reserved word. 

            [HideInInspector]
            public int i;
            [HideInInspector]
            public bool active;
            public bool primary; //role needs to be filled before child roles.   if there are child roles when this player exits.  they need to be re-assigned to secondary roles. ( so there isn't juggling of parent roles ) 
            public bool secondary; //tertiary - a lot of users preseent. allowing infinite instances, don't make inactive when user leaves. ( not implemented yet )
            public string parentRoleName; //the primary role needs to exist, otherwise go to next role. 
            [HideInInspector]
            [System.NonSerialized]
            public Role parentRole; //asigned after constructor with the string parentRoleName.  
            public Role()
            { }
            public Role(Role r)
            {
                this.id = r.id;
                this.description = r.description;
                this.disabled = r.disabled;
                this.trigger1Display = r.trigger1Display;
                this.trigger1 = r.trigger1;
                this.trigger2Display = r.trigger2Display;
                this.trigger2 = r.trigger2;
                this.upTrigger = r.upTrigger;
                this.rightTrigger = r.rightTrigger;
                this.downTrigger = r.downTrigger;
                this.leftTrigger = r.leftTrigger;
                this.startTrigger = r.startTrigger;
                this.xTrigger = r.xTrigger;
                this.yTrigger = r.yTrigger;
                this.active = r.active;
                this.primary = r.primary;
                this.secondary = r.secondary;
                this.parentRoleName = r.parentRoleName;
                this.interfaceType = r.interfaceType;
            }
        }
    }
}
