using System;
using System.Reflection;
using flyby.Tools;
using UnityEngine;
using System.Collections.Generic;

namespace flyby {
    namespace Core {
        [Serializable]
        public class Curve {
            public List<ShapeKey> shapeKeys;
            private AnimationCurve animationCurve;
            public float Evaluate(float _value) {
                this.animationCurve = getAnimationCurve();
                return this.animationCurve.Evaluate(_value);
            }
            public AnimationCurve getAnimationCurve() {
                // this is an emergency hacked class because 
                // json serialization was causing serialization loop in build. 
                // hoping the translation between activeTrigger and secondaryActiveTrigger works. 
                if (this.animationCurve == null) {
                    Keyframe[] keyframes = new Keyframe[shapeKeys.Count];
                    for (int i = 0; i < shapeKeys.Count; i++) {
                        ShapeKey k = shapeKeys[i];
                        keyframes[i] = new Keyframe(k.time, k.value);
                    }
                    this.animationCurve = new AnimationCurve(keyframes);
                }
                return this.animationCurve;
            }
        }

        public struct ShapeKey {
            public float time;
            public float value;
        }
    }
}
