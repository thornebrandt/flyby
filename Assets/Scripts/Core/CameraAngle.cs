namespace flyby
{
    namespace Core
    {
        public class CameraAngle
        {
            public string id;
            public bool disabled;
            public string trigger;
            public string nextTrigger;
            public string prevTrigger;
            public string parent; //can parent to an animated obj.
            public Vec origin; //center. 
            public Vec offset; //distance from center that it rotates around. 
            public Vec rotation;
            public Vec directionalIntensity; //not sure I want this here. 
            public Vec directionalAngle;
            public float animationTime; //should probably rename to startTime for consistency. 
            public float orthographicSize;
            public bool orthographic;
            public float fov;  //probably just use this instead of orthographic size. 
            public bool physical; //use physical properties - ( fix blocky shadows )
            public float near; //near plane - tweak to fix blocky shadows.
            public float far; //far plane - tweak to fix blocky shadows. 

            public CameraAngle()
            {
                this.origin = new Vec();
                this.offset = new Vec(0, 0, -5);
                this.rotation = new Vec();
                this.nextTrigger = "next_camera_angle";
                this.prevTrigger = "prev_camera_angle";
                this.animationTime = 4;
                this.orthographicSize = 5;
                this.fov = 70;
                this.physical = false;
                this.near = 0.1f;
                this.far = 1000f;
            }
        }
    }
}