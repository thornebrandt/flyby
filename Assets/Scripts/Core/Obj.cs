using System;
using System.Collections.Generic;
using System.Reflection;
using flyby;
using flyby.Active;
using flyby.Controllers;
using flyby.Tools;
using flyby.Sets;
using flyby.Triggers;
using Newtonsoft.Json;
using UnityEngine;

namespace flyby
{
    namespace Core
    {
        public class Obj
        {
            public string id;
            public bool killMe; //experimental - kill and re-instantiate ( for physics and the like )  
            public bool disabled;
            public string asset;
            public string bundle;
            public string trigger;
            public bool preRender;
            public Vec origin; //quick origin, no interaction.
            public Vec scale; //quick scale, no interaction. 
            public Vec rotation; // quick rotation, no interaction. 
            [HideInInspector]
            public Vector3 size; //private, calculated bounds. on parsing. 
            public int numInstances;
            public int _i;
            public int activeI;
            public bool current; //wheather or not it is part of the current part/set. 
            public string preset;
            //public string layer;
            public bool alwaysFront;
            public bool alwaysBack;
            public List<GameObject> instances;
            public GameObject prefab; //reference to root prefab. 
            public GameObject instance; //parent level?
            public ActiveType activeType; //activeType determines which instances are affected by activeTriggers
            //public ActiveType lastActiveType; //not sure what this is for. 
            public HierarchyType hierarchyType;
            public List<GameObject> activeInstances;
            public List<int> activeInstanceIndexes;
            public int timesTriggered; //stats on times triggered. 
            public Vector3 nullVector = new Vector3(-100, -100, -100);
            public ActiveTrigger triggerSprite;
            public ActiveTrigger triggerColor;
            public ActiveTrigger triggerPivot;
            public ActiveTrigger triggerPosition;
            public ActiveTrigger triggerSpeed;
            public ActiveTrigger triggerRotation;
            public ActiveTrigger triggerRotate;
            public ActiveTrigger triggerScale;
            public ActiveTrigger triggerParent;
            public ActiveTrigger activeTrigger; //custom generic trigger - assigned manually via custom active behaviours. 
            public StackArray stack; //for continuous backgrounds.  ( might eventually be replaced by array ) 
            public InstanceArray array; //for multiple instances with varying parameters with same trigger. 
            private List<ActiveTrigger> activeTriggers;
            private List<ActiveTrigger> allActiveTriggers; //for assigning group indexes. 
            private List<List<ActiveBehaviour>> activeBehaviours;
            private List<ActiveBehaviour> allActiveBehaviours;
            private Dictionary<int, List<ActiveBehaviour>> sustainedActiveBehaviours;
            private CameraController cameraController;
            //experimental
            public string group;
            public int groupIndex;
            public Obj originalO;
            public int arrayIndex;

            public void instantiate(TriggerObj triggerObj)
            {
                if (instances.Count > 0)
                {
                    this.timesTriggered++;
                    instance = instances[this._i];

                    if (instance != null)
                    {
                        resetTransforms(instance);
                        this.activeInstanceIndexes = getActiveInstanceIndexes(this._i, activeType, instances);
                        instance.SetActive(true); //just moved this to before activeBehaviourIntro...
                        triggerActiveBehaviourIntros(triggerObj, this._i);
                    }
                    this.activeI = this._i;
                    this._i++;
                    this._i = (int)Mathf.Repeat(this._i, numInstances);
                }
            }

            private void resetTransforms(GameObject _instance)
            {
                resetRigidBody(_instance);
                resetParent(_instance);
                if (this.hierarchyType != HierarchyType.KeepChildren)
                {
                    removeChildren(getPrefabInstance(_instance), prefab);
                }
                moveToOrigin(_instance); //origin seems to be the only transform that is dependent on json.
                applyScale(getRotateScaleInstance(_instance));
                applyRotation(getRotateScaleInstance(_instance));
                _instance.transform.rotation = Quaternion.identity;
                _instance.transform.localScale = Vector3.one;

            }

            private void removeChildren(GameObject _instance, GameObject _prefab)
            {
                if (_instance.transform.childCount == 0)
                {
                    //no children to remove. 
                    return;
                }
                if (_prefab.transform.childCount == 0)
                {
                    //no children in prefab.
                    removeAllChildren(_instance);
                    return;
                }
                //otherwise, remove a number of newer children that aren't in the original prefab.
                for (int i = _instance.transform.childCount - 1; i > 0; i--)
                {
                    if (i > _prefab.transform.childCount - 1)
                    {

                        removeChild(_instance.transform.GetChild(i));
                    }
                }
            }

            private void removeChild(Transform child)
            {
                //this is meant to restart manipulated parents. 
                child.SetParent(null);
                child.transform.position = this.nullVector;
            }

            private void removeAllChildren(GameObject _instance)
            {
                foreach (Transform child in _instance.transform)
                {
                    removeChild(child);
                }
            }

            private void resetParent(GameObject _instance)
            {
                //takes care of resetting hierarchy.
                _instance.transform.SetParent(null);
            }

            public void preRenderObj()
            {
                instantiate(new TriggerObj());
            }

            public void TriggerEventHandler(TriggerObj triggerObj)
            {
                if (this.current && !this.disabled)
                {
                    //TODO - do we also need to narrow down potential triggers on part change? 
                    bool triggeredGroup = false;


                    if (triggerObj.eventName == trigger)
                    {
                        bool passingFilters = true;
                        if (!string.IsNullOrEmpty(this.group))
                        {
                            triggeredGroup = true;
                            int groupIndex = FlyByController.instance.currentPart.getCurrentGroupIndex(this.group);
                            if (this.groupIndex != groupIndex)
                            {
                                passingFilters = false;
                                triggeredGroup = false;
                            }
                        }

                        if (passingFilters)
                        {
                            if (triggerObj.noteOff || triggerObj.triggerEnd)
                            {
                                //noteOff is from MIDI, triggerOff is set manually, sent in activebhevirs. 
                                releaseTrigger(triggerObj);
                            }
                            else
                            {
                                instantiate(triggerObj);
                                FlyByController.instance.currentPart.assignIncrementToGroupIndex(this.group);
                            }
                        }
                    }

                    if (!triggeredGroup)
                    {
                        this.checkActiveTriggers(this.activeInstanceIndexes, this.activeBehaviours, triggerObj);
                    }

                }
            }

            private void printActiveInstanceIndexes()
            {
                string str = "Active indexes triggered from " + this.id + ": [";
                foreach (int index in this.activeInstanceIndexes)
                {
                    str += index + ",";
                }
                str += "]";
                Debug.Log(str);
            }

            public void checkActiveTriggers(
                List<int> activeInstanceIndexes,
                List<List<ActiveBehaviour>> activeBehaviours,
                TriggerObj triggerObj)
            {
                activeInstanceIndexes = getActiveInstanceIndexesByTrigger(triggerObj, activeTriggers);
                foreach (int activeI in activeInstanceIndexes)
                {
                    foreach (ActiveBehaviour activeBehaviour in activeBehaviours[activeI])
                    {
                        activeBehaviour.TriggerEventHandler(triggerObj, this);
                    }
                }
            }

            public void setupInstances()
            {
                activeBehaviours = new List<List<ActiveBehaviour>>();
                sustainedActiveBehaviours = new Dictionary<int, List<ActiveBehaviour>>();
                allActiveBehaviours = new List<ActiveBehaviour>();
                allActiveTriggers = new List<ActiveTrigger>();

                if (this.stack != null && this.stack.instanceDimensions != null)
                {
                    //positions are handled by the stack. 
                    Vector3 dim = this.stack.instanceDimensions;
                    dim.x = dim.x <= 0 ? 1 : dim.x;
                    dim.y = dim.y <= 0 ? 1 : dim.y;
                    dim.z = dim.z <= 0 ? 1 : dim.z;
                    this.numInstances = (int)(dim.x * dim.y * dim.z);
                    this.activeType = ActiveType.All;
                }

                //make sure this is last before any array multipliers. 
                if (this.disabled)
                {
                    this.numInstances = 0;
                }

                for (int i = 0; i < this.numInstances; i++)
                {
                    GameObject instance = createInstance(prefab, i);
                    this.instances.Add(instance);
                }
                this.activeTriggers = setupActiveTriggers();
                this.allActiveTriggers = getAllActiveTriggers();
                this.activeInstances = setupInitialActiveInstances(activeType, instances);
                this.setupGroup();
            }

            public void setupGroup()
            {

                if (FlyByController.instance != null && FlyByController.instance.currentPart != null)
                {
                    if (!string.IsNullOrEmpty(this.group))
                    {
                        if (this.originalO == null || this.arrayIndex == this.originalO.arrayIndex)
                        {
                            this.groupIndex = FlyByController.instance.currentPart.assignGroupIndex(this.group);
                        }
                        else
                        {
                            if (this.originalO.array.shareGroup)
                            {
                                this.groupIndex = this.originalO.groupIndex;
                            }
                            else
                            {
                                this.groupIndex = FlyByController.instance.currentPart.assignGroupIndex(this.group);

                            }
                        }
                    }

                    List<int> groupIndexes = new List<int>();

                    // setup group indexes for activeTriggers
                    foreach (ActiveTrigger _a in this.activeTriggers)
                    {
                        if (!string.IsNullOrEmpty(_a.group))
                        {
                            _a.groupIndex = FlyByController.instance.currentPart.assignGroupIndex(_a.group);
                            groupIndexes.Add(_a.groupIndex);
                        }
                        else
                        {
                            groupIndexes.Add(-1);
                        }
                    }

                    int numActiveTriggers = this.activeTriggers.Count;

                    //no copy these indexes for the rest of the instances.
                    for (int i = 1; i < numInstances; i++)
                    {
                        for (int j = 0; j < numActiveTriggers; j++)
                        {
                            int _at_i = (i * numActiveTriggers) + j;
                            ActiveTrigger _at = this.allActiveTriggers[_at_i];
                            _at.groupIndex = groupIndexes[j];
                        }
                    }
                }
            }

            public GameObject createInstance(GameObject prefab, int i)
            {
                // this function positions the built in transform properties so they don't conflict with each other. 
                // default is constant speed allowing for additional "popup" and "pop-to" positions
                // as well as rotation and and scale on the lowest level so they don't affect position.

                GameObject prefab_go = GameObject.Instantiate(prefab) as GameObject;
                string baseName = getBaseName(prefab.name);
                prefab_go.name = baseName + "_" + i + "_prefab";
                GameObject root = prefab_go; //give activeBehaviour this information for custom children. 

                //TODO - replace with witch statement when there are multiple hierarchies. 
                //TODO - possible optimization - should the hierarchys be different if the active transforms don't exist. ? 
                // -- ( finding children at runtime will be more difficult. )
                GameObject instance = prefab_go;

                if (this.hierarchyType == HierarchyType.None)
                {
                    // if hierarchy is none, the prefab itself handles the hierarchy. 
                    // activeBehaviours are collected from the prefab via setupInstanceBehaviours in the controller. 
                    instance = prefab_go;
                }
                else
                {
                    // default hierarchy, as mentioned above.                
                    // base instance, in case there are additional custom transformations. 
                    // lowest hierarchy takes care of both rotation and scale. 
                    GameObject rotateScale_go = new GameObject();
                    rotateScale_go.name = baseName + "_" + i + "_rotateScale";
                    prefab_go.transform.SetParent(rotateScale_go.transform);
                    GameObject popup_go = new GameObject(); //child for relative "popping up" positions. 
                    popup_go.name = baseName + "_" + i + "_popup";
                    rotateScale_go.transform.SetParent(popup_go.transform);
                    GameObject speed_go = new GameObject(); //top level, for "speed." //also origin. 
                    speed_go.name = baseName + "_" + i;
                    popup_go.transform.SetParent(speed_go.transform); //instance parent. 
                    instance = speed_go;
                }

                //Tools.Scene.setLayer(instance, this.layer);
                this.size = Tools.Space.getSize(prefab);
                setupActiveBehaviours(root, instance, i);
                instance.transform.position = nullVector;
                instance.SetActive(false);
                return instance;
            }

            private string getBaseName(string prefabName)
            {
                if (!string.IsNullOrEmpty(this.id))
                {
                    return this.id;
                }
                if (!string.IsNullOrEmpty(prefabName))
                {
                    return prefabName;
                }
                return "instance";
            }

            public void addEventListeners()
            {
                if (!this.disabled)
                {
                    TriggerSourceController.OnTriggerEvent += TriggerEventHandler;
                }
            }

            public void removeEventListeners()
            {
                TriggerSourceController.OnTriggerEvent -= TriggerEventHandler;
            }

            public void kill()
            {
                this._i = 0;
                removeEventListeners();
                killAllActiveBehaviours();
                killInstances();
                this.instances.Clear();
            }

            private void killInstances()
            {
                foreach (GameObject instance in this.instances)
                {
                    if (Application.isPlaying)
                    {
                        UnityEngine.Object.Destroy(instance);
                    }
                    else
                    {
                        UnityEngine.Object.DestroyImmediate(instance);
                    }
                }
            }

            public void hide()
            {
                foreach (GameObject instance in this.instances)
                {
                    instance.transform.position = nullVector;
                    instance.SetActive(false);
                    resetRigidBody(instance);
                }
            }

            public void resetRigidBody(GameObject instance)
            {
                Rigidbody rb = instance.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.gameObject.SetActive(false);
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                    rb.transform.localPosition = Vector3.zero;
                    rb.gameObject.SetActive(true);
                }
            }


            public List<ActiveBehaviour> getActiveBehaviours(int i)
            {
                return activeBehaviours[i];
            }

            public void killActiveBehaviours(int i)
            {
                foreach (ActiveBehaviour activeBehaviour in activeBehaviours[i])
                {
                    activeBehaviour.Kill();
                }
            }

            public void killActiveTweens(int i)
            {
                foreach (ActiveBehaviour activeBehaviour in activeBehaviours[i])
                {
                    activeBehaviour.killTweens();
                }
            }

            public void introActiveBehaviours(int i)
            {
                //not usable. 
                foreach (ActiveBehaviour activeBehaviour in activeBehaviours[i])
                {
                    activeBehaviour.Intro(new TriggerObj(), this);
                }
            }

            private void killAllActiveBehaviours()
            {
                if (allActiveBehaviours != null)
                {
                    foreach (ActiveBehaviour ab in allActiveBehaviours)
                    {
                        ab.Kill();
                    }
                }
            }

            public void releaseTrigger(TriggerObj triggerObj)
            {
                //midi
                if (triggerObj.noteOff)
                {
                    if (sustainedActiveBehaviours != null)
                    {
                        bool keyExists = sustainedActiveBehaviours.ContainsKey(triggerObj.note);
                        if (keyExists)
                        {
                            foreach (ActiveBehaviour activeBehaviour in sustainedActiveBehaviours[triggerObj.note])
                            {
                                activeBehaviour.Release();
                            }
                        }
                    }
                }
                //manual
                if (triggerObj.triggerEnd)
                {
                    foreach (int activeI in activeInstanceIndexes)
                    {
                        foreach (ActiveBehaviour ab in activeBehaviours[activeI])
                        {
                            ab.EndTrigger(triggerObj);
                        }
                    }
                }
            }

            private void setupActiveBehaviours(GameObject root, GameObject instance, int i)
            {
                ActiveBehaviourController activeBehaviourController = ActiveBehaviourController.instance;
                if (activeBehaviourController != null)
                {
                    List<ActiveBehaviour> instanceBehaviours = activeBehaviourController.setupInstanceBehaviours(this, root, instance, i);
                    this.activeBehaviours.Add(instanceBehaviours);
                    this.allActiveBehaviours.AddRange(instanceBehaviours);
                }
                else
                {
                    Debug.Log("Warning: No ActiveBehaviourController found.");
                }
            }

            private List<ActiveTrigger> getAllActiveTriggers()
            {
                foreach (ActiveBehaviour ab in this.allActiveBehaviours)
                {
                    this.allActiveTriggers.AddRange(ab.triggers);
                }
                return this.allActiveTriggers;
            }

            private List<ActiveTrigger> setupActiveTriggers()
            {
                // this caches a list of activeTriggers which have a different active type than this obj. 
                ActiveBehaviourController activeBehaviourController = ActiveBehaviourController.instance;
                if (activeBehaviourController != null && this.activeBehaviours.Count > 0)
                {
                    return activeBehaviourController.setupActiveTriggerList(this, this.activeBehaviours[0]);
                }
                return new List<ActiveTrigger>();
            }

            public void triggerActiveBehaviourIntros(TriggerObj triggerObj, int i)
            {
                int ab_i = 0;
                foreach (ActiveBehaviour activeBehaviour in activeBehaviours[i])
                {
                    ab_i++;
                    activeBehaviour.Intro(triggerObj, this);
                }
                if (triggerObj.sustain)
                {
                    sustainedActiveBehaviours[triggerObj.note] = activeBehaviours[i];
                }
            }

            public List<GameObject> setupInitialActiveInstances(ActiveType activeType, List<GameObject> instances)
            {
                if (instances != null && instances.Count > 0)
                {
                    switch (activeType)
                    {
                        case ActiveType.Next:
                            activeInstances.Add(this.instances[0]);
                            activeInstanceIndexes.Add(0);
                            break;
                        case ActiveType.All:
                            activeInstances.AddRange(this.instances);
                            activeInstanceIndexes = getAllIndexes();
                            break;
                    }
                }
                return activeInstances;
            }

            private List<int> getAllIndexes()
            {
                for (int i = 0; i < this.numInstances; i++)
                {
                    activeInstanceIndexes.Add(i);
                }
                return activeInstanceIndexes;
            }


            private List<int> getActiveInstanceIndexesByTrigger(TriggerObj triggerObj, List<ActiveTrigger> activeTriggers)
            {
                //TODO set dynamicActiveType to true while setting up activeTrigger list. 
                if (this.activeTriggers != null && activeTriggers.Count > 0)
                {
                    //slower. 
                    foreach (ActiveTrigger a in this.activeTriggers)
                    {
                        if (a.trigger == triggerObj.eventName)
                        {
                            if (a.activeType == ActiveType.Null)
                            {
                                a.activeType = activeType;
                            }
                            if (a.activeType != activeType)
                            {
                                activeInstanceIndexes = getActiveInstanceIndexes(activeI, a.activeType, instances);
                            }
                        }
                    }
                }
                return activeInstanceIndexes;
            }

            public List<int> getActiveInstanceIndexes(int i, ActiveType activeType, List<GameObject> instances)
            {
                if (activeType != ActiveType.All)
                {
                    activeInstanceIndexes.Clear();
                }

                switch (activeType)
                {
                    case ActiveType.All:
                        if (activeInstanceIndexes.Count != numInstances)
                        {
                            activeInstanceIndexes.Clear();
                            activeInstanceIndexes = getAllIndexes();
                        }
                        break;
                    case ActiveType.Next:
                        activeInstanceIndexes.Add(getIndex(i + 1));
                        break;
                    case ActiveType.Second:
                        if (timesTriggered <= 2)
                        {
                            activeInstanceIndexes.Add(0);
                        }
                        else
                        {
                            activeInstanceIndexes.Add(getIndex(i - 1));
                        }
                        break;
                    case ActiveType.Random:
                        int maxIndex = (int)Mathf.Min(timesTriggered, numInstances);
                        int randomI = (int)Tools.Rand.Between(0, maxIndex);
                        activeInstanceIndexes.Add(randomI);
                        break;
                    case ActiveType.Middle:
                        int middleI = timesTriggered / 2;
                        if (timesTriggered > numInstances)
                        {
                            middleI = getIndex(i + Mathf.CeilToInt(numInstances / 2.0f));
                        }
                        activeInstanceIndexes.Add(middleI);
                        break;
                    case ActiveType.Closest:
                        float minLength = Mathf.Infinity;
                        int closestIndex = i;
                        int loopedIndex;

                        for (int j = 0; j < numInstances; j++)
                        {
                            loopedIndex = getIndex(i - j); //subtract to move backwards through the array. 
                            GameObject loopedInstance = getInstance(loopedIndex);
                            //TODO: should eventually be subtracted from camera position. 
                            float thisLength = Vector3.Distance(loopedInstance.transform.position, Vector3.zero);
                            if (thisLength < minLength)
                            {
                                closestIndex = loopedIndex;
                                minLength = thisLength;
                            }

                        }
                        activeInstanceIndexes.Add(getIndex(closestIndex));
                        break;
                    case ActiveType.Default:
                    case ActiveType.Last:
                        activeInstanceIndexes.Add(i);
                        break;
                }
                return activeInstanceIndexes;
            }


            public List<GameObject> getActiveInstances(int i, ActiveType activeType, List<GameObject> instances)
            {
                //at this point this is only used for tests.
                getActiveInstanceIndexes(i, activeType, instances);
                activeInstances.Clear();
                foreach (int index in activeInstanceIndexes)
                {
                    activeInstances.Add(instances[index]);
                }
                return activeInstances;
            }

            private void applyScale(GameObject rotateScaleGO)
            {
                // quickly applies initialize scale. 
                // use triggerScale for more interactions. 
                if (this.scale != null)
                {
                    rotateScaleGO.transform.localScale = this.scale.getVector3();
                }
            }

            private void applyRotation(GameObject rotateScaleGO)
            {
                // quickly applies initialize rotation. 
                // use triggerRotation or triggerRotate for more interactions. 
                if (this.rotation != null)
                {
                    rotateScaleGO.transform.localRotation = Quaternion.Euler(this.rotation.getVector3());
                }
            }

            private void moveToOrigin(GameObject go)
            {
                // quickly applies position. 
                // use triggerRotation or triggerRotate for more interactions. 

                go.transform.localPosition = this.origin.getVector3();
                //TODO - should these add the current position ? 

                if (this.alwaysFront)
                {
                    float alwaysFrontValue = getAlwaysFrontValue();
                    go.transform.localPosition = new Vector3(
                        go.transform.localPosition.x,
                        go.transform.localPosition.y,
                        alwaysFrontValue
                    );
                }

                if (this.alwaysBack)
                {
                    float alwaysBackValue = getAlwaysBackValue();
                    go.transform.localPosition = new Vector3(
                        go.transform.localPosition.x,
                        go.transform.localPosition.y,
                        alwaysBackValue
                    );
                }

            }

            private float getAlwaysFrontValue()
            {
                if (this.cameraController == null)
                {
                    this.cameraController = CameraController.instance;
                }
                if (this.cameraController == null)
                {
                    Debug.Log("Could not connect camera controller, sending 0 for front position");
                    return 0;
                }
                return this.cameraController.getNextAlwaysFrontZ();
            }

            private float getAlwaysBackValue()
            {
                if (this.cameraController == null)
                {
                    this.cameraController = CameraController.instance;
                }
                if (this.cameraController == null)
                {
                    Debug.Log("Could not connect camera controller, sending 0 for back position");
                    return 0;
                }
                return this.cameraController.getNextAlwaysBackZ();
            }


            public Vector3 calculateScale()
            {
                // multiplies bounds of asset by activeScale.
                Vector3 multiplier = Vector3.one;
                if (this.triggerScale != null && this.triggerScale.value != null)
                {
                    multiplier = this.triggerScale.value.getVector3();
                }
                return Vector3.Scale(this.size, multiplier);
            }

            public Obj getObjFromJSON(string JSONPath)
            {
                string json = Json.getJSON("Assets/Tests/EditorTests/json/obj/" + JSONPath);
                return JsonConvert.DeserializeObject<Obj>(json);
            }

            public static GameObject getPopupInstance(GameObject instance)
            {
                //getting child at one degree down for a secondary animation. 
                //shouldn't be static, because it's dependent on hierarchy. ( hierarchy could be passed. ) 
                if (instance.transform.childCount > 0)
                {
                    return instance.transform.GetChild(0).gameObject;
                }
                return instance; //on flat hierarchy. 
            }

            public static GameObject getRotateScaleInstance(GameObject instance)
            {
                //getting one level up from root child. 
                //shouldn't be static, because it's dependent on hierarchy. ( hierarchy could be passed. ) 
                GameObject popupInstance = getPopupInstance(instance);
                if (popupInstance.transform.childCount > 0)
                {
                    return popupInstance.transform.GetChild(0).gameObject;
                }
                return popupInstance; //bubbling up on flat hierarchy. 
            }

            public static GameObject getPrefabInstance(GameObject instance)
            {
                //getting the root child. 
                //shouldn't be static, because it's dependent on hierarchy. ( hierarchy could be passed. ) 
                GameObject rotateScaleInstance = getRotateScaleInstance(instance);
                if (rotateScaleInstance.transform.childCount > 0)
                {
                    return rotateScaleInstance.transform.GetChild(0).gameObject;
                }
                return rotateScaleInstance;
            }

            public GameObject getInstanceRoot(GameObject instance)
            {
                if (this.hierarchyType == HierarchyType.None)
                {
                    return instance;
                }
                else
                {
                    GameObject rotateScaleInstance = getRotateScaleInstance(instance);
                    if (rotateScaleInstance.transform.childCount > 0)
                    {
                        return rotateScaleInstance.transform.GetChild(0).gameObject;
                    }
                    return rotateScaleInstance;
                }
            }


            private int getIndex(int _i)
            {
                int index = (int)Mathf.Repeat(_i, numInstances);
                return index;
            }

            public GameObject getInstance(int _instanceI)
            {
                int instanceI = getIndex(_instanceI);
                if (this.instances.Count > instanceI)
                {
                    return this.instances[instanceI];
                }
                else
                {
                    return null;
                }
            }

            public GameObject getPreviousInstance(int i)
            {
                return this.getInstance(i - 1);
            }

            public void overwrite(NullObj nullObj)
            {
                //these all need to be overwritten via deep copy. 
                this.id = string.IsNullOrEmpty(nullObj.id) ? this.id : nullObj.id;
                this.disabled = string.IsNullOrEmpty(nullObj.disabled) ? this.disabled : nullObj.getBoolean(nullObj.disabled);
                this.asset = string.IsNullOrEmpty(nullObj.asset) ? this.asset : nullObj.asset;
                this.bundle = string.IsNullOrEmpty(nullObj.bundle) ? this.bundle : nullObj.bundle;
                this.trigger = string.IsNullOrEmpty(nullObj.trigger) ? this.trigger : nullObj.trigger;
                this.preRender = string.IsNullOrEmpty(nullObj.preRender) ? this.preRender : nullObj.getBoolean(nullObj.preRender);
                this.origin = nullObj.origin == null ? this.origin : nullObj.origin;
                this.scale = nullObj.scale == null ? this.scale : nullObj.scale;
                this.rotation = nullObj.rotation == null ? this.rotation : nullObj.rotation;
                this.activeTrigger = nullObj.activeTrigger == null ? this.activeTrigger : nullObj.activeTrigger;
                this.numInstances = nullObj.numInstances == -999 ? this.numInstances : nullObj.numInstances;
                this.activeType = nullObj.activeType == ActiveType.Null ? this.activeType : nullObj.activeType;
                this.hierarchyType = nullObj.hierarchyType == HierarchyType.Null ? this.hierarchyType : nullObj.hierarchyType;
                this.triggerSprite = nullObj.triggerSprite == null ? this.triggerSprite : nullObj.triggerSprite;
                this.triggerColor = nullObj.triggerColor == null ? this.triggerColor : nullObj.triggerColor;
                this.triggerPosition = nullObj.triggerPosition == null ? this.triggerPosition : nullObj.triggerPosition;
                this.triggerPivot = nullObj.triggerPivot == null ? this.triggerPivot : nullObj.triggerPivot;
                this.triggerRotation = nullObj.triggerRotation == null ? this.triggerRotation : nullObj.triggerRotation;
                this.triggerRotate = nullObj.triggerRotate == null ? this.triggerRotate : nullObj.triggerRotate;
                this.triggerSpeed = nullObj.triggerSpeed == null ? this.triggerSpeed : nullObj.triggerSpeed;
                this.triggerScale = nullObj.triggerScale == null ? this.triggerScale : nullObj.triggerScale;
                this.triggerParent = nullObj.triggerParent == null ? this.triggerParent : nullObj.triggerParent;
                this.stack = nullObj.stack == null ? this.stack : new StackArray(nullObj.stack);
                this.array = nullObj.array == null ? this.array : new InstanceArray(nullObj.array);
                //this.layer = nullObj.layer == null ? this.layer : nullObj.layer;
                this.alwaysFront = string.IsNullOrEmpty(nullObj.alwaysFront) ? this.alwaysFront : nullObj.getBoolean(nullObj.alwaysFront);
                this.alwaysBack = string.IsNullOrEmpty(nullObj.alwaysBack) ? this.alwaysBack : nullObj.getBoolean(nullObj.alwaysBack);
                this.group = string.IsNullOrEmpty(nullObj.group) ? this.group : nullObj.group;
                this.groupIndex = nullObj.groupIndex == -999 ? this.groupIndex : nullObj.groupIndex;
                this.killMe = string.IsNullOrEmpty(nullObj.killMe) ? this.killMe : nullObj.getBoolean(nullObj.killMe);
            }


            public Obj()
            {
                this.origin = new Vec();
                this.scale = new Vec(1);
                this.numInstances = 1;
                this.instances = new List<GameObject>();
                this.activeInstances = new List<GameObject>();
                this.activeInstanceIndexes = new List<int>();
                this.activeType = ActiveType.Default;
            }

            public Obj(Obj obj) : this()
            {
                if (obj != null)
                {
                    foreach (FieldInfo field in obj.GetType().GetFields())
                    {
                        var type = Nullable.GetUnderlyingType(field.FieldType) ?? field.FieldType;
                        if (type.Name == "ActiveTrigger")
                        {
                            ActiveTrigger at = field.GetValue(obj) as ActiveTrigger;
                            if (at != null)
                            {
                                field.SetValue(this, new ActiveTrigger(at));
                            }
                        }
                        else
                        {
                            field.SetValue(this, field.GetValue(obj));

                        }
                    }
                    if (obj.stack != null)
                    {
                        this.stack = new StackArray(obj.stack);
                    }
                    if (obj.array != null)
                    {
                        this.array = new InstanceArray(obj.array);
                    }


                    // if (obj.triggerRotation != null) {
                    //     //debugging arrays. 
                    //     this.triggerRotation = new ActiveTrigger(obj.triggerRotation);
                    // }

                    //shot in the dark. 
                    this.instances = new List<GameObject>(obj.instances);
                }
            }

            public Obj(NullObj nullObj) : this()
            {
                this.overwrite(nullObj);
            }

            public override bool Equals(System.Object o)
            {
                //TODO - finish this. 
                if (o is null)
                {
                    return false;
                }
                Obj obj = o as Obj;
                return this.asset == obj.asset &&
                    this.activeType == obj.activeType &&
                    this.origin == obj.origin &&
                    this.numInstances == obj.numInstances &&
                    this.triggerColor == obj.triggerColor;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }
    }
}