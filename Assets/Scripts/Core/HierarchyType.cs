namespace flyby
{
    namespace Core
    {
        public enum HierarchyType
        {
            Null, //so can be replaced. 
            Default, //Speed -> LocalPosition -> Rotation/Scale -> Prefab
            KeepChildren, //We are manipulating children at initialization ( see splines ) and want to keep them,
            None, //only prefab. No additional parents added. 
        }
    }
}