namespace flyby {
    namespace Core {
        public enum ActiveType {
            Null, //so can be replaced. 
            Default, //affects last instance triggered
            Last, //same as default, for overwriting,
            Second, //affects second to last instance triggered ( used for slow fade ins )
            Next, //affects instance before it is triggered. 
            All, //affects all instances
            Random, //affects a random visible instance 
            Closest, //closest to camera's focus point,
            Middle, //for scrolling backgrounds, less calculation than closest,
        }
    }
}