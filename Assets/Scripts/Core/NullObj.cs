using UnityEngine;
using System;
using flyby.Active;
using System.Reflection;

namespace flyby
{
    namespace Core
    {
        public class NullObj
        {

            // this app is meant for a lot of generative and evolving self-cloning. 
            // this class is the same as obj, but it's used during construction, so default values can copy the obj.
            // for non-nullable types such as booleans. 

            public string id;
            public string disabled; //consider changing to bypass ? 
            public string solo; //disable all other objs except for this one ( during comp - but perhaps has an 'activeDisable' )
            public string preset;
            public string asset;
            public string bundle;
            public string trigger;
            public string preRender;
            public Vec origin; //quick position - no interaction. 
            public Vec scale; //quick scale - no interaction. 
            public Vec rotation; //quick rotation - no interaction. 
            public int numInstances;
            public ActiveType activeType; //activeType determines which instances are affected by activeTriggers. defaults to
            public HierarchyType hierarchyType; //hierarchy type. Default adds parents for non-conflicting transform. None leaves prefabs alone.
            public ActiveTrigger triggerSprite;
            public ActiveTrigger triggerColor;
            public ActiveTrigger triggerPosition; //position after rotation.
            public ActiveTrigger triggerPivot; //position before rotation. 
            public ActiveTrigger triggerSpeed;
            public ActiveTrigger triggerRotation;
            public ActiveTrigger triggerRotate;
            public ActiveTrigger triggerScale;
            public ActiveTrigger triggerParent;
            public ActiveTrigger activeTrigger; //custom generic trigger. is overriden by manual trigger list. 
            public StackArray stack;
            public InstanceArray array;
            public string layer;
            public string alwaysFront;
            public string alwaysBack;
            public string group;
            public int groupIndex;
            public int arrayIndex;
            public string killMe; //experimental. 

            public NullObj()
            {
                this.numInstances = -999;
                this.arrayIndex = -999;
                this.groupIndex = -999; //experimental.
                this.activeType = ActiveType.Null;
            }

            public NullObj(NullObj nullObj)
            {
                foreach (FieldInfo field in nullObj.GetType().GetFields())
                {
                    var type = Nullable.GetUnderlyingType(field.FieldType) ?? field.FieldType;
                    field.SetValue(this, field.GetValue(nullObj));
                }
            }

            public void overwrite(NullObj nullObj)
            {
                //these all need to be overwritten via deep copy. 
                this.id = string.IsNullOrEmpty(nullObj.id) ? this.id : nullObj.id;
                this.disabled = string.IsNullOrEmpty(nullObj.disabled) ? this.disabled : nullObj.disabled;
                this.asset = string.IsNullOrEmpty(nullObj.asset) ? this.asset : nullObj.asset;
                this.bundle = string.IsNullOrEmpty(nullObj.bundle) ? this.bundle : nullObj.bundle;
                this.trigger = string.IsNullOrEmpty(nullObj.trigger) ? this.trigger : nullObj.trigger;
                this.preRender = string.IsNullOrEmpty(nullObj.preRender) ? this.preRender : nullObj.preRender;
                this.origin = nullObj.origin == null ? this.origin : nullObj.origin;
                this.scale = nullObj.scale == null ? this.scale : nullObj.scale;
                this.rotation = nullObj.rotation == null ? this.rotation : nullObj.rotation;
                this.activeTrigger = nullObj.activeTrigger == null ? this.activeTrigger : nullObj.activeTrigger;
                this.numInstances = nullObj.numInstances == -999 ? this.numInstances : nullObj.numInstances;
                this.activeType = nullObj.activeType == ActiveType.Null ? this.activeType : nullObj.activeType;
                this.hierarchyType = nullObj.hierarchyType == HierarchyType.Null ? this.hierarchyType : nullObj.hierarchyType;
                this.triggerSprite = nullObj.triggerSprite == null ? this.triggerSprite : nullObj.triggerSprite;
                this.triggerColor = nullObj.triggerColor == null ? this.triggerColor : nullObj.triggerColor;
                this.triggerPosition = nullObj.triggerPosition == null ? this.triggerPosition : nullObj.triggerPosition;
                this.triggerPivot = nullObj.triggerPivot == null ? this.triggerPivot : nullObj.triggerPivot;
                this.triggerRotation = nullObj.triggerRotation == null ? this.triggerRotation : nullObj.triggerRotation;
                this.triggerRotate = nullObj.triggerRotate == null ? this.triggerRotate : nullObj.triggerRotate;
                this.triggerSpeed = nullObj.triggerSpeed == null ? this.triggerSpeed : nullObj.triggerSpeed;
                this.triggerScale = nullObj.triggerScale == null ? this.triggerScale : nullObj.triggerScale;
                this.triggerParent = nullObj.triggerParent == null ? this.triggerParent : nullObj.triggerParent;
                this.stack = nullObj.stack == null ? this.stack : new StackArray(nullObj.stack);
                this.array = nullObj.array == null ? this.array : new InstanceArray(nullObj.array);
                this.layer = nullObj.layer == null ? this.layer : nullObj.layer;
                this.alwaysFront = string.IsNullOrEmpty(nullObj.alwaysFront) ? this.alwaysFront : nullObj.alwaysFront;
                this.alwaysBack = string.IsNullOrEmpty(nullObj.alwaysBack) ? this.alwaysBack : nullObj.alwaysBack;
                this.group = string.IsNullOrEmpty(nullObj.group) ? this.group : nullObj.group;
                this.groupIndex = nullObj.groupIndex == -999 ? this.groupIndex : nullObj.groupIndex;
                this.arrayIndex = nullObj.arrayIndex == -999 ? this.arrayIndex : nullObj.arrayIndex; //experimental.
                this.killMe = string.IsNullOrEmpty(nullObj.killMe) ? this.killMe : nullObj.killMe;
            }


            public override bool Equals(System.Object o)
            {
                if (o is null)
                {
                    return false;
                }
                NullObj nullObj = o as NullObj;
                return this.asset == nullObj.asset &&
                    this.disabled == nullObj.disabled &&
                    this.solo == nullObj.solo &&
                    this.preRender == nullObj.preRender &&
                    this.preset == nullObj.preset &&
                    this.activeType == nullObj.activeType &&
                    this.hierarchyType == nullObj.hierarchyType &&
                    this.origin == nullObj.origin &&
                    this.scale == nullObj.scale &&
                    this.rotation == nullObj.rotation &&
                    this.numInstances == nullObj.numInstances &&
                    this.triggerSprite == nullObj.triggerSprite &&
                    this.triggerColor == nullObj.triggerColor &&
                    this.triggerPosition == nullObj.triggerPosition &&
                    this.triggerPivot == nullObj.triggerPivot &&
                    this.triggerSpeed == nullObj.triggerSpeed &&
                    this.triggerRotation == nullObj.triggerRotation &&
                    this.triggerRotate == nullObj.triggerRotate &&
                    this.triggerScale == nullObj.triggerScale &&
                    this.triggerParent == nullObj.triggerParent &&
                    this.activeTrigger == nullObj.activeTrigger &&
                    this.stack == nullObj.stack &&
                    this.array == nullObj.array &&
                    this.layer == nullObj.layer &&
                    this.alwaysFront == nullObj.alwaysFront &&
                    this.alwaysBack == nullObj.alwaysBack &&
                    this.group == nullObj.group &&
                    this.groupIndex == nullObj.groupIndex &&
                    this.arrayIndex == nullObj.arrayIndex &&
                    this.killMe == nullObj.killMe;
            }

            public bool getBoolean(string str)
            {
                if (string.IsNullOrEmpty(str) || str == "false" || str == "0")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }


            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

        }
    }
}