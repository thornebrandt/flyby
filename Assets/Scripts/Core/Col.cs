using System;
using System.Reflection;
using flyby.Tools;
using UnityEngine;

namespace flyby
{
    namespace Core
    {

        [Serializable]
        public enum BlendMode
        {
            Default, //adds two colors,
            Colorize, //adds colors, then subtracts everything by difference over one,
            Null //so can be replaced. 
        }
        [Serializable]
        public class Col
        {
            public float gray;
            public float gray2;
            public float _gray;
            public float alpha;
            public float alpha2;
            public float _alpha;
            public bool uniform;
            public float r;
            public float r2;
            public float _r;
            public float g;
            public float g2;
            public float _g;
            public float b;
            public float b2;
            public float _b;
            public float sat;
            public float sat2;
            public float _sat;
            public float brightness;
            public float brightness2;
            public float _brightness;
            public float hue;
            public float hue2;
            public float _hue;
            public string color; //for presets. 
            public BlendMode blendMode;
            public int seed; //used to get consistant random colors between arrays. ( + time ) 
            private int timesTriggered;

            public Color getColor()
            {
                setPresets(this.color);
                setGray();
                setColors();
                setAlpha();
                setAdvancedPresets(this.color);
                Color RGBColor = new Color(this._r, this._g, this._b, this._alpha);
                Color HSVColor = applyHSV(RGBColor);
                this.timesTriggered++;
                return HSVColor;
            }

            public void setBaseColors()
            {
                Debug.Log("setting presets: " + this.r + " , " + this.g + " , " + this.b);
                setPresets(this.color);
                setGray();
                setColors();
                setAlpha();
            }


            public static Color AddColors(Color color1, Color color2)
            {
                return Col.AddColors(color1, color2, BlendMode.Default);
            }

            public void Add(Col c2)
            {
                this.setBaseColors();
                this.gray = Tools.Math.Add(this.gray, c2.gray);
                this.gray2 = Tools.Math.Add(this.gray2, c2.gray2);


                //initially set settings to gray so we can subtract. 
                this.r = this.r == -999 ? this.gray : this.r;
                this.r2 = this.r2 == -999 ? this.gray2 : this.r2;
                this.g = this.g == -999 ? this.gray : this.g;
                this.g2 = this.g2 == -999 ? this.gray2 : this.g2;
                this.b = this.b == -999 ? this.gray : this.b;
                this.b2 = this.b2 == -999 ? this.gray2 : this.b2;


                this.r = Tools.Math.Add(this.r, c2.r);
                this.r2 = Tools.Math.Add(this.r2, c2.r2);
                this.g = Tools.Math.Add(this.g, c2.g);
                this.g2 = Tools.Math.Add(this.g2, c2.g2);
                this.b = Tools.Math.Add(this.b, c2.b);
                this.b2 = Tools.Math.Add(this.b2, c2.b2);
                this.sat = Tools.Math.Add(this.sat, c2.sat);
                this.sat2 = Tools.Math.Add(this.sat2, c2.sat2);
                this.brightness = Tools.Math.Add(this.brightness, c2.brightness);
                this.brightness2 = Tools.Math.Add(this.brightness2, c2.brightness2);
                this.hue = Tools.Math.Add(this.hue, c2.hue);
                this.hue2 = Tools.Math.Add(this.hue2, c2.hue2);
            }


            public static Color AddColors(Color color1, Color color2, BlendMode blendMode)
            {
                Color addedColor = color1 + color2;
                switch (blendMode)
                {
                    case BlendMode.Colorize:
                        float max = 0;
                        //adds all colors, then subtracts everything so max is only 1.
                        max = Mathf.Max(addedColor.r, max);
                        max = Mathf.Max(addedColor.g, max);
                        max = Mathf.Max(addedColor.b, max);
                        float diff = max - 1.0f;
                        if (max > 1)
                        {
                            addedColor.r = addedColor.r - diff;
                            addedColor.g = addedColor.g - diff;
                            addedColor.b = addedColor.b - diff;
                        }
                        addedColor.a = Mathf.Clamp(addedColor.a, 0, 1);
                        break;
                    case BlendMode.Default:
                    default:
                        addedColor.a = Mathf.Min(addedColor.a, 1);
                        break;
                }
                return addedColor;
            }


            private void setPresets(string preset)
            {
                switch (preset)
                {
                    case "white":
                        this.gray = 1;
                        break;
                    case "clear":
                        this.alpha = 0;
                        break;
                    case "gray":
                        this.gray = 0.5f;
                        break;
                    case "red":
                        this.r = 1;
                        break;
                    case "green":
                        this.g = 1;
                        break;
                    case "blue":
                        this.b = 1;
                        break;
                    case "orange":
                        this.r = 1;
                        this.g = 0.5f;
                        break;
                    case "rgb":
                        //sequentially return red, green, then blue. 
                        int rgb_thirds = this.timesTriggered % 3;
                        switch (rgb_thirds)
                        {
                            case 0:
                                this.r = 1;
                                this.g = 0;
                                this.b = 0;
                                break;
                            case 1:
                                this.r = 0;
                                this.g = 1;
                                this.b = 0;
                                break;
                            case 2:
                                this.r = 0;
                                this.g = 0;
                                this.b = 1;
                                break;
                        }
                        break;
                    case "cyan":
                        this.b = 1;
                        this.g = 1;
                        this.r = 0;
                        break;
                    case "random":
                        this.gray = 0;
                        this.gray2 = 1;
                        this.uniform = false;
                        break;
                    case "random_gray":
                        this.gray = 0;
                        this.gray2 = 1;
                        this.uniform = true;
                        break;
                    case "random_red":
                        this.r = 0.8f;
                        this.r2 = 1;
                        this.g = 0;
                        this.g2 = 0.2f;
                        this.b = 0;
                        this.b2 = 0.2f;
                        break;
                    case "pastels":
                        this.gray = 0;
                        this.gray = 1;
                        this.uniform = false;
                        this.sat = 0.2f;
                        this.brightness = 1;
                        break;
                    case "random_green":
                        this.r = 0;
                        this.r2 = 0.2f;
                        this.g = 0.8f;
                        this.g2 = 1;
                        this.b = 0;
                        this.b2 = 0.2f;
                        break;
                    case "random_blue":
                        this.r = 0;
                        this.r2 = 0.2f;
                        this.g = 0;
                        this.g2 = 0.2f;
                        this.b = 0.8f;
                        this.b2 = 1;
                        break;
                    case "random_purple":
                        this.r = 0.8f;
                        this.r2 = 1;
                        this.g = 0;
                        this.g2 = 0.2f;
                        this.b = 0.8f;
                        this.b2 = 1;
                        break;
                    case "random_brown":
                        this.r = 0.2f;
                        this.r2 = 1;
                        this.g = 0.2f;
                        this.g2 = 1;
                        this.b = 0.1f;
                        this.b2 = 1;
                        this.uniform = false;
                        break;
                    case "cyan_red":
                        if (this.timesTriggered % 2 == 0)
                        {
                            this.r = 0;
                            this.g = 1;
                            this.b = 1;
                        }
                        else
                        {
                            this.r = 1;
                            this.g = 0;
                            this.b = 0;
                        }
                        break;
                    case "cyan_red_white":
                        if (this.timesTriggered % 3 == 0)
                        {
                            this.r = 0;
                            this.g = 1;
                            this.b = 1;
                        }
                        else if (this.timesTriggered % 3 == 1)
                        {
                            this.r = 1;
                            this.g = 0;
                            this.b = 0;
                        }
                        else
                        {
                            this.r = 1;
                            this.g = 1;
                            this.b = 1;
                        }
                        break;
                    case "black":
                        break;
                    case "blackwhite":
                        if (this.timesTriggered % 2 == 0)
                        {
                            this.gray = 0;
                        }
                        else
                        {
                            this.gray = 1;
                        }
                        break;
                    case "hypercolor":
                        this._sat = 1;
                        this.hue = 0;
                        this.hue2 = 1;
                        break;
                    case "rainbow":
                        int rgb_sixths = this.timesTriggered % 6;
                        switch (rgb_sixths)
                        {
                            case 0:
                                this.r = 1;
                                this.g = 0;
                                this.b = 0;
                                break;
                            case 1:
                                this.r = 1;
                                this.g = 1;
                                this.b = 0;
                                break;
                            case 2:
                                this.r = 0;
                                this.g = 1;
                                this.b = 0;
                                break;
                            case 3:
                                this.r = 0;
                                this.g = 1;
                                this.b = 1;
                                break;
                            case 4:
                                this.r = 0;
                                this.g = 0;
                                this.b = 1;
                                break;
                            case 5:
                                this.r = 1;
                                this.g = 0;
                                this.b = 1;
                                break;
                        }
                        break;
                    case "bisexual":
                        //pink and blue 
                        if (this.timesTriggered % 2 == 0)
                        {
                            this.r = 0.97f;
                            this.g = 0.47f;
                            this.b = 1;
                        }
                        else
                        {
                            this.r = 0.47f;
                            this.g = 0.99f;
                            this.b = 1;
                        }
                        break;
                    case "redgreen":
                    case "christmas":
                        if (this.timesTriggered % 2 == 0)
                        {
                            this.r = 1;
                            this.g = 0;
                            this.b = 0;
                        }
                        else
                        {
                            this.r = 0;
                            this.g = 1;
                            this.b = 0;
                        }
                        break;
                    case "yellow_blue":
                        if (this.timesTriggered % 2 == 0)
                        {
                            this.r = 1;
                            this.g = 1;
                            this.b = 0;
                        }
                        else
                        {
                            this.r = 0;
                            this.g = 0;
                            this.b = 1;
                        }
                        break;
                    case "purple_green":
                        if (this.timesTriggered % 2 == 0)
                        {
                            this.r = 1;
                            this.g = 0;
                            this.b = 1;
                        }
                        else
                        {
                            this.r = 0;
                            this.g = 1;
                            this.b = 0;
                        }
                        break;
                    case "apple_logo":
                    case "apple":
                        int apple_sixths = this.timesTriggered % 6;
                        switch (apple_sixths)
                        {
                            case 0:
                                this.r = 0.38f;
                                this.g = 0.79f;
                                this.b = 0.39f;
                                break;
                            case 1:
                                this.r = 0.98f;
                                this.g = 0.85f;
                                this.b = 0.37f;
                                break;
                            case 2:
                                this.r = 0.99f;
                                this.g = 0.56f;
                                this.b = 0.28f;
                                break;
                            case 3:
                                this.r = 0.91f;
                                this.g = 0.32f;
                                this.b = 0.29f;
                                break;
                            case 4:
                                this.r = 0.56f;
                                this.g = 0.28f;
                                this.b = 0.68f;
                                break;
                            case 5:
                                this.r = 0.27f;
                                this.g = 0.45f;
                                this.b = 0.85f;
                                break;
                        }
                        break;
                    default:
                        if (!string.IsNullOrEmpty(preset))
                        {
                            Debug.Log("color preset not found: " + preset);
                        }
                        break;
                }
            }

            public void Multiply(float n)
            {
                this.gray = this.gray == -999 ? -999 : this.gray * n;
                this.gray2 = this.gray2 == -999 ? -999 : this.gray2 * n;
                this.r = this.r == -999 ? -999 : this.r * n;
                this.r2 = this.r2 == -999 ? -999 : this.r2 * n;
                this.g = this.g == -999 ? -999 : this.g * n;
                this.g2 = this.g2 == -999 ? -999 : this.g2 * n;
                this.b = this.b == -999 ? -999 : this.b * n;
                this.b2 = this.b2 == -999 ? -999 : this.b2 * n;
                this.alpha = this.alpha == 1 ? 1 : this.alpha * n;
                this.alpha2 = this.alpha2 == -999 ? -999 : this.alpha2 * n;

            }

            private void setGray()
            {
                this._gray = this.gray >= 0 ? Rand.Between(this.gray, this.gray2, this.seed) : 0;
                if (this.uniform)
                {
                    this._r = this._gray;
                    this._g = this._gray;
                    this._b = this._gray;
                }
                else
                {
                    this._r = this._gray;
                    this._g = Rand.Between(this.gray, this.gray2, this.seed);
                    this._b = Rand.Between(this.gray, this.gray2, this.seed);
                }
            }

            private void setColors()
            {
                if (this.r > -999)
                {
                    this._r = Rand.Between(this.r, this.r2, this.seed);
                }
                else
                {
                    if (this.r2 > -999)
                    {
                        this._r = Rand.Between(this._r, this.r2, this.seed);
                    }
                }

                if (this.g > -999)
                {
                    this._g = Rand.Between(this.g, this.g2, this.seed + 1);
                }
                else
                {
                    if (this.g2 > -999)
                    {
                        this._g = Rand.Between(this._g, this.g2, this.seed + 1);
                    }
                }

                if (this.b > -999)
                {
                    this._b = Rand.Between(this.b, this.b2, this.seed + 2);
                }
                else
                {
                    if (this.b2 > -999)
                    {
                        this._b = Rand.Between(this._b, this.b2, this.seed + 2);
                    }
                }
            }

            private void setAdvancedPresets(string preset)
            {
                //post - 

                switch (preset)
                {
                    case "random_brown":
                        // make sure red is the greatest. 
                        // will be white/yellow often. 
                        if (_g > _r + 0.1f)
                        {
                            _g = _r;
                        }
                        if (_b > _r - 0.05f)
                        {
                            _b = _r;
                        }
                        break;
                }
            }

            private void setAlpha()
            {
                this._alpha = Rand.Between(this.alpha, this.alpha2, this.seed + 3);
            }

            private Color applyHSV(Color RGBColor)
            {
                float currentHue;
                float currentSat;
                float currentBrightness;
                Color.RGBToHSV(RGBColor, out currentHue, out currentSat, out currentBrightness);
                this._sat = this.sat >= 0 ? Rand.Between(this.sat, this.sat2, this.seed + 4) : currentSat;
                this._brightness = this.brightness >= 0 ? Rand.Between(this.brightness, this.brightness2, this.seed) : currentBrightness;
                this._hue = this.hue >= 0 ? Rand.Between(this.hue, this.hue2, this.seed + 5) : currentHue;

                if (this._sat == currentSat && this._brightness == currentBrightness)
                {
                    return RGBColor;
                }

                return Color.HSVToRGB(this._hue, this._sat, this._brightness);
            }

            public Col()
            {
                this.gray = -999;
                this.gray2 = -999;
                this.r = -999;
                this.r2 = -999;
                this.g = -999;
                this.g2 = -999;
                this.b = -999;
                this.b2 = -999;
                this.sat = -999;
                this.sat2 = -999;
                this.brightness = -999;
                this.brightness2 = -999;
                this.hue = -999;
                this.hue2 = -999;
                this.alpha = 1; //is this messing things up? 
                this.alpha2 = -999;
                this.uniform = true;
                this.seed = -999;
            }

            public Col(float gray) : this()
            {
                this.gray = gray;
            }

            public Col(float r, float g, float b) : this()
            {
                this.r = r;
                this.b = b;
                this.g = g;
            }

            public Col(string colorPreset) : this()
            {
                this.color = colorPreset;
            }

            public Col(Color color) : this()
            {
                this.r = color.r;
                this.g = color.g;
                this.b = color.b;
                this.alpha = color.a;
            }

            public override bool Equals(System.Object obj)
            {
                if (obj is null)
                {
                    return false;
                }
                Col col = obj as Col;
                return this.gray == col.gray &&
                    this.gray2 == col.gray2 &&
                    this.alpha == col.alpha &&
                    this.alpha2 == col.alpha2 &&
                    this.uniform == col.uniform &&
                    this.r == col.r &&
                    this.r2 == col.r2 &&
                    this.g == col.g &&
                    this.g2 == col.g2 &&
                    this.b == col.b &&
                    this.b2 == col.b2 &&
                    this.sat == col.sat &&
                    this.sat2 == col.sat2 &&
                    this.brightness == col.brightness &&
                    this.brightness2 == col.brightness2 &&
                    this.hue == col.hue &&
                    this.hue2 == col.hue2;
            }

            public static bool operator ==(Col lhs, Col rhs)
            {
                if (lhs is null)
                {
                    if (rhs is null)
                    {
                        return true;
                    }
                    return false;
                }
                return lhs.Equals(rhs);
            }

            public static bool operator !=(Col lhs, Col rhs) => !(lhs == rhs);

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public Color lerpColor(float lerpAmount)
            {
                //TODO - check for presence of r2 but not r. 
                this._gray = this.gray > -999 ? Tools.Math.Lerp(this.gray, this.gray2, lerpAmount) : 0;
                this._r = this._gray;
                this._g = this._gray;
                this._b = this._gray;
                if (this.r > -999)
                {
                    this._r = Tools.Math.Lerp(this.r, this.r2, lerpAmount);
                }
                else
                {
                    if (this.r2 > -999)
                    {
                        this._r = Tools.Math.Lerp(this._r, this.r2, lerpAmount);
                    }
                }

                if (this.g > -999)
                {
                    this._g = Tools.Math.Lerp(this.g, this.g2, lerpAmount);
                }
                else
                {
                    if (this.g2 > -999)
                    {
                        this._g = Tools.Math.Lerp(this._g, this.g2, lerpAmount);
                    }
                }

                if (this.b > -999)
                {
                    this._b = Tools.Math.Lerp(this.b, this.b2, lerpAmount);
                }
                else
                {
                    if (this.b2 > -999)
                    {
                        this._b = Tools.Math.Lerp(this._b, this.b2, lerpAmount);
                    }
                }

                this._alpha = this.alpha > -999 ? Tools.Math.Lerp(this.alpha, this.alpha2, lerpAmount) : this._alpha;
                return new Color(
                    this._r,
                    this._g,
                    this._b,
                    this._alpha
                );
            }

            public Col(Col col)
            {
                if (col != null)
                {
                    foreach (FieldInfo field in col.GetType().GetFields())
                    {
                        var type = Nullable.GetUnderlyingType(field.FieldType) ?? field.FieldType;
                        field.SetValue(this, field.GetValue(col));
                    }
                }
            }




        }
    }
}