using System;
using flyby.Tools;
using UnityEngine;

namespace flyby
{
    namespace Core
    {
        [Serializable]
        public class Vec
        {
            public float x;
            public float x2;
            private float _x;
            public float y;
            public float y2;
            private float _y;
            public float z;
            public float z2;
            private float _z;
            public float a;
            public float a2;
            private float _a;
            public bool uniform;
            public bool relative; //use as additive position. 
            public bool absolute; //use by controllers for absolute position. 
            public Vector3 multiple;
            public Vector3 max; //experimental.
            public Vector3 min; //experimental.
            public int seed; //used to get consistant random numbers between arrays. ( + time ) 

            public Vector3 getVector3()
            {
                setAll();
                setIndividual();
                clampValues();
                return new Vector3(this._x, this._y, this._z);
            }

            public Vector3 lerpVector3(float lerpAmount)
            {
                //mostly used for knobs 
                this._a = Tools.Math.Lerp(this.a, this.a2, lerpAmount);

                float x = this.x != -999 ? this.x : this.a;
                float x2 = this.x2 != -999 ? this.x2 : this.a2;
                float y = this.y != -999 ? this.y : this.a;
                float y2 = this.y2 != -999 ? this.y2 : this.a2;
                float z = this.z != -999 ? this.z : this.a;
                float z2 = this.z2 != -999 ? this.z2 : this.a2;

                this._x = Tools.Math.Lerp(x, x2, lerpAmount);
                this._y = Tools.Math.Lerp(y, y2, lerpAmount);
                this._z = Tools.Math.Lerp(z, z2, lerpAmount);

                return new Vector3(
                    this._x,
                    this._y,
                    this._z
                );
            }

            public Vec getSanitized()
            {
                //returns a cloned sanitized vec. 
                Vec vec = new Vec();
                //set all.
                vec.x = this.x == -999 ? this.a : this.x; //a will also be -999
                vec.x2 = this.x2 == -999 ? this.a2 : this.x2;
                vec.y = this.y == -999 ? this.a : this.y;
                vec.y2 = this.y2 == -999 ? this.a2 : this.y2;
                vec.z = this.z == -999 ? this.a : this.z;
                vec.z2 = this.z2 == -999 ? this.a2 : this.z2;

                //set second axis.
                vec.x = this.x == -999 ? 0 : this.x;
                vec.x2 = this.x2 == -999 ? vec.x : this.x2;
                vec.y = this.y == -999 ? 0 : this.y;
                vec.y2 = this.y2 == -999 ? vec.y : this.y2;
                vec.z = this.z == -999 ? 0 : this.z;
                vec.z2 = this.z2 == -999 ? vec.z : this.z2;

                return vec;

            }

            public Vector3 lerpVector3(Vector3 lerpVector)
            {
                float x = this.x != -999 ? this.x : this.a;
                float x2 = this.x2 != -999 ? this.x2 : this.a2;
                float y = this.y != -999 ? this.y : this.a;
                float y2 = this.y2 != -999 ? this.y2 : this.a2;
                float z = this.z != -999 ? this.z : this.a;
                float z2 = this.z2 != -999 ? this.z2 : this.a2;

                this._x = Tools.Math.Lerp(x, x2, lerpVector.x);
                this._y = Tools.Math.Lerp(y, y2, lerpVector.y);
                this._z = Tools.Math.Lerp(z, z2, lerpVector.z);

                return new Vector3(
                    this._x,
                    this._y,
                    this._z
                );
            }


            public static Vector3 lerpVector3(Vec vec, float lerpAmount)
            {
                if (vec == null)
                {
                    return Vector3.one;
                }
                return vec.lerpVector3(lerpAmount);
            }

            public static Vector3 Add(Vector3 vec1, Vector3 vec2)
            {
                return vec1 + vec2;
            }

            public void Add(Vec v2)
            {
                //used in arrays - after parsing but before instantiation. 
                this.x = Tools.Math.Add(this.x, v2.x);
                this.x2 = Tools.Math.Add(this.x2, v2.x2);
                this.y = Tools.Math.Add(this.y, v2.y);
                this.y2 = Tools.Math.Add(this.y2, v2.y2);
                this.z = Tools.Math.Add(this.z, v2.z);
                this.z2 = Tools.Math.Add(this.z2, v2.z2);
                this.a = Tools.Math.Add(this.a, v2.a);
                this.a2 = Tools.Math.Add(this.a2, v2.a2);
            }

            public static Vector3 getNearestMultiple(Vector3 valueVec, Vector3 multipleVec)
            {
                return new Vector3(
                    Tools.Math.getNearestMultiple(valueVec.x, multipleVec.x),
                    Tools.Math.getNearestMultiple(valueVec.y, multipleVec.y),
                    Tools.Math.getNearestMultiple(valueVec.z, multipleVec.z)
                );
            }

            private void setAll()
            {
                this._a = Rand.Between(this.a, this.a2, this.seed);
                this._x = this._a;
                this._y = this.uniform ? this._a : Rand.Between(this.a, this.a2, this.seed + 1);
                this._z = this.uniform ? this._a : Rand.Between(this.a, this.a2, this.seed + 2);
            }

            private void setIndividual()
            {
                this._x = this.x != -999 ? Rand.Between(this.x, this.x2, this.seed) : this._x;
                this._y = this.y != -999 ? Rand.Between(this.y, this.y2, this.seed + 1) : this._y;
                this._z = this.z != -999 ? Rand.Between(this.z, this.z2, this.seed + 2) : this._z;
            }

            private void clampValues()
            {
                if (this.max != null)
                {

                    this._x = this.max.x != -999 ? Mathf.Min(this._x, this.max.x) : this._x;
                    this._y = this.max.y != -999 ? Mathf.Min(this._y, this.max.y) : this._y;
                    this._z = this.max.z != -999 ? Mathf.Min(this._z, this.max.z) : this._z;
                }

                if (this.min != null)
                {
                    this._x = this.min.x == -999 ? this._x : Mathf.Max(this._x, this.min.x);
                    this._y = this.min.y == -999 ? this._y : Mathf.Max(this._y, this.min.y);
                    this._z = this.min.z == -999 ? this._z : Mathf.Max(this._z, this.min.z);
                }
            }

            public override bool Equals(System.Object obj)
            {
                if (obj is null)
                {
                    return false;
                }
                Vec vec = obj as Vec;
                if (vec is null)
                {
                    //not sure why we'd ever end up here. but here we are. 
                    return false;
                }
                return this.x == vec.x &&
                    this.x2 == vec.x2 &&
                    this.y == vec.y &&
                    this.y2 == vec.y2 &&
                    this.z == vec.z &&
                    this.z2 == vec.z2 &&
                    this.a == vec.a &&
                    this.a2 == vec.a2 &&
                    this.uniform == vec.uniform;
            }

            public static bool operator ==(Vec lhs, Vec rhs)
            {
                if (lhs is null)
                {
                    if (rhs is null)
                    {
                        return true;
                    }
                    return false;
                }
                return lhs.Equals(rhs);
            }

            public static bool operator !=(Vec lhs, Vec rhs) => !(lhs == rhs);

            public Quaternion getQuaternion()
            {
                return UnityEngine.Quaternion.Euler(this.getVector3());
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

            public void Multiply(float n)
            {
                //used in arrays - after parsing but before instantiation. 
                this.x = this.x == -999 ? -999 : this.x * n;
                this.x2 = this.x2 == -999 ? -999 : this.x2 * n;
                this.y = this.y == -999 ? -999 : this.y * n;
                this.y2 = this.y2 == -999 ? -999 : this.y2 * n;
                this.z = this.z == -999 ? -999 : this.z * n;
                this.z2 = this.z2 == -999 ? -999 : this.z2 * n;
                this.a = this.a == -999 ? -999 : this.a * n;
                this.a2 = this.a2 == -999 ? -999 : this.a2 * n;
            }

            public Vec()
            {
                this.x = -999;
                this.x2 = -999;
                this.y = -999;
                this.y2 = -999;
                this.z = -999;
                this.z2 = -999;
                this.a = -999;
                this.a2 = -999;
                this.seed = -999;
                this.min = new Vector3(-999, -999, -999);
                this.max = new Vector3(-999, -999, -999);
            }

            public Vec(float _a) : this()
            {
                this.a = _a;
                this.x = _a;
                this.y = _a;
                this.z = _a;
            }

            public Vec(float _x, float _y, float _z) : this()
            {
                this.x = _x;
                this.y = _y;
                this.z = _z;
            }

            public Vec(Vector3 vec3) : this()
            {
                this.x = vec3.x;
                this.y = vec3.y;
                this.z = vec3.z;
            }

            public Vec(Vector3 vec3_1, Vector3 vec3_2) : this()
            {
                this.x = vec3_1.x;
                this.y = vec3_1.y;
                this.z = vec3_1.z;
                this.x2 = vec3_2.x;
                this.y2 = vec3_2.y;
                this.z2 = vec3_2.z;
            }

            public Vec(float _x, float _x2, float _y, float _y2, float _z, float _z2) : this()
            //TODO - this initialization might be confusiont, might want to do xyz, x2y2z2 instead. 
            {
                this.x = _x;
                this.x2 = _x2;
                this.y = _y;
                this.y2 = _y2;
                this.z = _z;
                this.z2 = _z2;
            }

            public Vec(Vec vec) : this()
            {
                if (vec != null)
                {
                    this.x = vec.x;
                    this.x2 = vec.x2;
                    this.y = vec.y;
                    this.y2 = vec.y2;
                    this.z = vec.z;
                    this.z2 = vec.z2;
                    this.a = vec.a;
                    this.a2 = vec.a2;
                    this.uniform = vec.uniform;
                    this.relative = vec.relative;
                    this.absolute = vec.absolute;
                    this.multiple = vec.multiple;
                    this.seed = vec.seed;
                }
            }
        }

    }

}