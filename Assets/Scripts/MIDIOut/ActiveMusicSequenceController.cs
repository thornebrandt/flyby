using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Active;
using flyby.Triggers;
using flyby.Music;
using System.Collections;
using System.Collections.Generic;


namespace flyby
{
    namespace Active
    {
        public class ActiveMusicSequenceController : ActiveBehaviour
        {
            public List<SequentialMusicObject> sequentialMusicObjects;
            public string eventName_for_random; //the event for randomizing the patterns. 

            //make a list of a list of integers. 
            public List<PatternPair> patternPairs;

            public int startPatternIndex = -1;

            private Dictionary<string, List<int>> sequences = new Dictionary<string, List<int>>();  //not sure if this is needed, we can just sync the index. 

            private bool setup; //hack - for some reason intro is being called before some other kind of initialzation.

            public override void setupActiveTriggers(Obj o)
            {
                // useful for generic and dynamic but custom behaviours. 
                // to quickly make variations via json. 
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);
                    }
                    else
                    {
                        //experimental. 
                        this.assignSecondaryTriggers(new ActiveTrigger());
                    }
                }
            }


            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                //this is a hack. 
                //unsure why initalPatternSetup is not working here. 
                base.Intro(triggerObj, o);
            }

            private void initialPatternSetup()
            {
                if (this.startPatternIndex != -1 && this.patternPairs.Count > this.startPatternIndex)
                {
                    string patternString = this.patternPairs[this.startPatternIndex].pattern.ToString();
                    this.setPattern(this.patternPairs[this.startPatternIndex].pattern);
                }
                else
                {
                    Debug.Log(" did not pass");
                    //set random
                    this.randomizePattern();
                }

            }

            public override void Update()
            {
                if (!this.setup)
                {
                    //really messy but not sure why no other way works. 
                    this.setup = true;
                    this.initialPatternSetup();  //hack doing this in update. 
                }

                base.Update();
            }

            public override void TriggerEventHandler(TriggerObj triggerObj, Obj o)
            {
                if (!string.IsNullOrEmpty(this.eventName_for_random) && triggerObj.eventName == this.eventName_for_random)
                {
                    this.randomizePattern();
                    return;
                }

                foreach (PatternPair pp in this.patternPairs)
                {
                    if (triggerObj.eventName == pp.eventName)
                    {
                        this.setPattern(pp.pattern);
                        return;
                    }
                }
            }

            private void setPattern(List<int> pattern)
            {
                if (pattern == null || pattern.Count == 0)
                {
                    return;
                }

                for (int i = 0; i < this.sequentialMusicObjects.Count; i++)
                {
                    //loop . 
                    int patternIndex = i % pattern.Count;
                    int collision = pattern[patternIndex];
                    this.sequentialMusicObjects[i].setCollision(collision);
                }
            }

            private void randomizePattern()
            {
                Debug.Log("randomizing pattern for " + this.gameObject.name);
                foreach (SequentialMusicObject smo in this.sequentialMusicObjects)
                {
                    smo.setRandomCollision();
                }
            }
        }

        [System.Serializable]
        public class PatternPair
        {
            public string eventName;
            public List<int> pattern;
        }

    }
}
