using System.Collections.Generic;
using UnityEngine;
using flyby.Active;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Music
    {
        //this is the most vore-centric class.  Sends a value based on how many objects have been in it's belly. 

        public class AccumulativeMusicObject : MusicObject
        {
            private Dictionary<GameObject, float> insideObjects = new Dictionary<GameObject, float>();
            //objects that are inside of this object, have not left. 
            public ActiveColor activeColor;

            public int maxCollisions = 3; //just realized that this could mean non-user objects. 
            private int currentCollisions = 0;
            public float lerpSpeed = 2f;
            public float changeThreshold = 0.01f;

            public int channel = 0;
            //public int note = 50;
            //public float vel = 0.9f; //velocity of note.
            public float note_length = 300f; //length of note in seconds - should be a long note. 

            private int note = 36; //storing for note-off. 
            public int knob = 23;  //sends a knob value based on the number of collisions.   23 for frequency. 
            public int knobChannel = 15; //15 for this demo. 

            private float currentKnobValue = 0f;
            private float targetKnobValue = 0f;
            private float lastTargetKnobValue = 0f;

            // Inactivity timer to reset when a new collision occurs
            private float inactivityTimer = 0f;
            public float maxInactivityTime = 90f; // Time to wait without new collisions

            public override void Start()
            {
                this.setupColor();
                base.Start();
            }

            public override void Update()
            {
                inactivityTimer += Time.deltaTime;
                if (inactivityTimer >= maxInactivityTime)
                {
                    ClearAllColliders();
                }

                checkInsideObjectTransforms(); //was missing this before. 
                checkIfKnobHasChanged(this.targetKnobValue);
            }

            private void checkIfKnobHasChanged(float targetKnobValue)
            {
                //lerping the KnobValue jknob.
                if (lastTargetKnobValue != targetKnobValue)
                {
                    this.SendMidiKnob(this.knobChannel, this.knob, targetKnobValue);
                }

                lastTargetKnobValue = targetKnobValue;
            }

            public override void OnCollision(MusicCollision musicCollision, MusicObject other)
            {
                if (!insideObjects.ContainsKey(other.gameObject))
                {
                    insideObjects.Add(other.gameObject, Time.time);
                    this.currentCollisions = insideObjects.Count;
                    UpdateTargetKnobValue();
                    if (this.currentCollisions == 1)
                    {
                        this.TriggerColor(this.mc.targetColor, 30);
                        this.TriggerLongNote();
                    }
                }

                // Reset the inactivity timer on any new collision event
                inactivityTimer = 0f;
            }

            private void setupColor()
            {
                if (this.activeColor == null)
                {
                    this.activeColor = this.GetComponent<ActiveColor>();
                }
                if (this.activeColor != null)
                {
                    this.activeColor.material = this.GetComponent<Renderer>().material;
                    this.activeColor.material.EnableKeyword("_EMISSION");
                    this.activeColor.instance = this.gameObject;
                    this.activeColor.usingEmission = true;
                    this.TriggerColor(this.mc.startColor, 0.1f);
                }
            }

            private void TriggerColor(Color _targetColor, float _duration)
            {
                if (this.activeColor != null)
                {
                    this.activeColor.setBaseColors(this.mc.startColor); //sets the color to come back to. 

                    TriggerObj t = new TriggerObj();
                    ActiveTrigger a = new ActiveTrigger();
                    a.targetColor = new Col(
                        _targetColor.r,
                        _targetColor.g,
                        _targetColor.b
                    );
                    a.onTime = _duration;
                    a.offTime = -1;
                    this.activeColor.Trigger(t, a, null);
                }
            }

            private void checkInsideObjectTransforms()
            {
                List<GameObject> objectsToRemove = new List<GameObject>();

                if (insideObjects == null) return;

                foreach (var entry in insideObjects)
                {
                    GameObject obj = entry.Key;
                    if (obj == null)
                    {
                        objectsToRemove.Add(obj);
                        continue;
                    }

                    if (!obj.activeInHierarchy)
                    {
                        objectsToRemove.Add(obj);
                        continue;
                    }

                    if (obj.transform.localScale.magnitude < 0.01f)
                    {
                        objectsToRemove.Add(obj);
                    }
                }

                foreach (var obj in objectsToRemove)
                {
                    insideObjects.Remove(obj);
                    this.HandleRemoval();
                }
            }


            public override void OnMusicCollision(MusicCollision musicCollision, MusicObject other)
            {
                //don't collide.
                return;
            }

            public override void OnMusicCollisionExit(GameObject other)
            {
                if (insideObjects.ContainsKey(other.gameObject))
                {
                    insideObjects.Remove(other.gameObject);
                    this.HandleRemoval();
                }
            }

            private void HandleRemoval()
            {
                this.currentCollisions = insideObjects.Count;
                UpdateTargetKnobValue(); //update knob first. 

                if (this.currentCollisions == 0)
                {
                    this.TriggerColor(mc.startColor, 2);
                    this.EndLongNote(); ;
                }
            }


            private void TriggerLongNote()
            {
                MusicCollision musicCollision = this.musicCollisions[0];
                this.note = CalculateNote(this.mc, null);
                float vel = CalculateVelocity(this.mc, null);
                this.SendMidiNote(this.channel, note, vel, this.note_length);
                this.UpdateTargetKnobValue();
            }

            private void EndLongNote()
            {
                this.SendMidiNoteOff(this.channel, this.note);
            }

            // consider different name: Update the target Knob value based on the current number of objects
            void UpdateTargetKnobValue()
            {
                targetKnobValue = Mathf.Clamp01((float)this.currentCollisions / maxCollisions);
            }

            // Method to clear all colliders from the dictionary and reset the timer
            void ClearAllColliders()
            {
                insideObjects.Clear();
                UpdateTargetKnobValue();
                inactivityTimer = 0f; // Reset the timer
            }
        }
    }
}