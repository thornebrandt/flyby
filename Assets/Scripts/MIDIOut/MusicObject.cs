using UnityEngine;
using System.Collections.Generic;
using flyby.Controllers;
using flyby.Tools;

namespace flyby
{
    namespace Music
    {
        public class MusicObject : MonoBehaviour
        {
            public string id; //identifier for type of object. ( ball, pad, string, etc.)
            public List<string> tags; //experimental. not used yet. 
            public int priority; //if the object hits itself. 
            [HideInInspector]
            public float randomPriority; //regularly updating and compared. 
            public List<MusicCollision> musicCollisions;

            [HideInInspector]
            public MusicCollision mc; //quick referernece fo calculations. 
            private MidiController midiController;
            private uint midiEndPoint;

            private Rigidbody rb; //not sure if needed. 

            public virtual void Start()
            {
                this.midiController = MidiController.instance;
                this.randomPriority = Random.Range(0.0f, 1.0f);
                this.rb = GetComponent<Rigidbody>(); //not sure if needed.
                if (this.musicCollisions != null && this.musicCollisions.Count > 0)
                {
                    this.mc = this.musicCollisions[0];
                }
                else
                {
                    this.mc = new MusicCollision();
                }
            }

            public virtual void Update()
            {
            }

            public virtual void OnCollision(MusicCollision musicCollision, MusicObject other)
            {
                //generic function for handling collisions.
                //non-music effects that happen on each collision. musicCollision can be null.
            }

            public virtual void OnMusicCollision(MusicCollision musicCollision, MusicObject other)
            {
                //TODO - consider some kind of pre CC effect.

                bool shouldFire = this.CheckShouldFire(musicCollision, other);
                if (shouldFire)
                {
                    float vel = this.CalculateVelocity(musicCollision, other);
                    int note = this.CalculateNote(musicCollision, other);
                    int channel = this.CalculateChannel(musicCollision, other);
                    float length = this.CalculateLength(musicCollision, other);

                    //clamping out too quiet values. 
                    if (vel > 0.2f)
                    {
                        this.SendMidiNote(channel, note, vel, length);
                    }
                }
            }

            public virtual float CalculateVelocity(MusicCollision musicCollision, MusicObject other)
            {
                //this is a generic velocity calculator. 
                return Random.Range(0.75f, 1.0f);
            }

            public virtual int CalculateNote(MusicCollision musicCollision, MusicObject other)
            {
                //this is a generic note calculator. 
                return (int)Rand.Between(musicCollision.note, musicCollision.note2);
            }

            public virtual int CalculateChannel(MusicCollision musicCollision, MusicObject other)
            {
                //this is a generic channel calculator. 
                return (int)Rand.Between(musicCollision.channel, musicCollision.channel2);
            }

            public virtual float CalculateLength(MusicCollision musicCollision, MusicObject other)
            {
                //this is a generic length calculator. 
                return 0.2f;
            }

            public bool CheckShouldFire(MusicCollision musicCollision, MusicObject other)
            {
                //TODO - if musicCollision.randomPriority, check against randomPriority. ( randomPriority should update )
                if (musicCollision.always) return true;
                if (this.priority > other.priority) return true;
                if (this.priority < other.priority) return false; //important step was missing.

                //priorities are equal. determine based on position.
                GameObject otherGameObject = other.gameObject;
                Vector3 thisPos = this.transform.position;
                Vector3 otherPos = other.transform.position;
                if (thisPos.y > otherPos.y) return true;
                if (thisPos.y == otherPos.y)
                {
                    if (thisPos.x > otherPos.x) return true;
                }
                return false;
            }

            void OnTriggerEnter(Collider other)
            {
                if (this.checkIfColliderIsTooSmall(other))
                {
                    return;
                }


                HandleCollision(other.gameObject);
            }

            private bool checkIfColliderIsTooSmall(Collider other)
            {
                float epsilon = 0.01f;
                Vector3 boundsSize = other.bounds.size;
                bool isTooSmall = false;
                if (boundsSize.x < epsilon || boundsSize.y < epsilon || boundsSize.z < epsilon)
                {
                    isTooSmall = true;
                }
                return isTooSmall;
            }

            void OnTriggerExit(Collider other)
            {
                this.HandleCollisionExit(other.gameObject);
            }

            void OnCollisionEnter(Collision other)
            {
                Debug.Log("onCollision called instead of onTrigger - unexpected.");
                if (this.checkIfColliderIsTooSmall(other.collider))
                {
                    return;
                }
                HandleCollision(other.gameObject);
            }

            public bool HasTag(string tag)
            {
                if (tags == null) return false;
                return tags.Contains(tag);
            }

            private void HandleCollisionExit(GameObject other)
            {
                //usually doing nothing. 
                this.OnMusicCollisionExit(other);
            }

            public virtual void OnMusicCollisionExit(GameObject other)
            {
            }

            private void HandleCollision(GameObject other)
            {
                //this is a generic collision handler
                //note, if you need specific positional information, you might need to use OnCollisionEnter specifically. 
                //this is only going to fire on the first musicalCollision hit.

                if (other.gameObject.GetComponent<MusicObject>() != null)
                {
                    MusicObject otherMusicObject = other.gameObject.GetComponent<MusicObject>();
                    MusicCollision musicCollision = null;
                    string otherID = otherMusicObject.id;
                    foreach (MusicCollision _musicCollision in musicCollisions)
                    {
                        if (_musicCollision.otherID == otherID || _musicCollision.any)
                        {
                            this.OnMusicCollision(_musicCollision, otherMusicObject);
                            musicCollision = _musicCollision;
                            break;
                        }
                    }
                    this.OnCollision(musicCollision, otherMusicObject); //visual or logic effects. musicCollision can be null. 
                }
            }

            public void SendMidiNote(int channel, int note, float vel, float length)
            {
                if (midiController != null)
                {
                    midiController.triggerMidiNote(channel, note, vel, length);
                }
            }

            public void SendMidiNoteOff(int channel, int note)
            {
                if (midiController != null)
                {
                    midiController.triggerMidiNoteOff(channel, note);
                }
            }

            public void SendMidiKnob(int channel, int knob, float value)
            {
                if (midiController != null)
                {
                    midiController.triggerMidiKnob(channel, knob, value);
                }
            }
        }
    }
}