using System;
using flyby.Core;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    namespace Music
    {
        [Serializable]
        public class MusicCollision
        {
            //this is a relational class to organize collisions between two musicObjects.
            public string description;
            public string otherID; //the id of the object that is colliding with this object.
            public int note; //single note - 
            public int note2;  //if present - this is a range - fills up notes.
            public List<int> notes; //actual object of notes. 
            public float velocity;
            public float velocity2;
            public int channel;
            public int channel2;
            public float length; //length of MIDI off. 
            public bool always; //if true, both objects might fire.
            public bool any; //returns for any body id. still requires musicObject on other collider. 
            public Color startColor;
            public Color targetColor;
            public Vector3 value; //arbitary value to apply to behaviours. 

            public MusicCollision()
            {
                this.note = 36; //preservting for translation. 
                this.length = 0.2f;
                this.velocity = 0.9f;
                this.velocity2 = -999;
                this.channel2 = -999;
            }
        }
    }
}