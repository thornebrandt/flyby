using System.Collections.Generic;
using UnityEngine;
using flyby.Active;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Music
    {
        public class LineMusicObject : MusicObject
        {
            public ActiveLine activeLine;

            public Color startColor;
            public Color targetColor;

            public float emissionIntensity = 3.0f;

            private Material material;

            private ActiveTrigger at;

            public override void Start()
            {
                this.setupLine();
                base.Start();
            }


            private void setupLine()
            {
                if (this.activeLine == null)
                {
                    this.activeLine = this.GetComponent<ActiveLine>();
                }
                if (this.activeLine != null)
                {
                    this.at = new ActiveTrigger();
                    at.color = new Col(this.startColor);
                    at.targetColor = new Col(
                        this.targetColor.r * this.emissionIntensity,
                        this.targetColor.g * this.emissionIntensity,
                        this.targetColor.b * this.emissionIntensity
                    );
                    at.currentColorValue = this.startColor;
                    at.baseColorValue = this.startColor;
                    at.onTime = 0.08f;
                    at.offTime = 0.2f;
                    at.id = "collision";
                    this.activeLine.triggers.Add(at);  //hopefully this is in the right order. 
                    this.material = this.GetComponent<Renderer>().material;
                    this.material.EnableKeyword("_EMISSION");
                    this.activeLine.instance = this.gameObject;
                    this.triggerColor();
                }
            }

            public override void OnCollision(MusicCollision musicCollision, MusicObject other)
            {
                this.triggerColor();
            }

            private void triggerColor()
            {
                if (this.activeLine != null)
                {
                    TriggerObj t = new TriggerObj();
                    ActiveTrigger a = new ActiveTrigger();
                    this.activeLine.Trigger(t, this.at, null);
                }
            }
        }

    }
}