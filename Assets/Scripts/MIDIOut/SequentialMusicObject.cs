using System.Collections.Generic;
using UnityEngine;
using flyby.Active;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Music
    {
        public class SequentialMusicObject : MusicObject
        {
            //seems to handle a series of notes, colors, and hiding. 

            private int startSeqIndex;
            private int currentSeqIndex;
            private MusicCollision currentSeqCollision;
            public List<MusicCollision> sequentialCollisions; //different from the map of collisions - keeps track of colors, notes. channels. 
            public ActiveColor activeColor;
            public ActiveScale activeScale;
            public Color startColor;
            public Color targetColor;
            public bool hidenOnStart; //not hooked up yet, not sure why things are hidden on start.
            public float growBackTime = 10f;
            public float stayHiddenTime = 10f;
            //TODO - list of ids that will trigger the sequence. 

            public override void Start()
            {

                this.setupSequence();
                this.setupColor();
                this.setupScale();
                base.Start();
            }

            private void setupSequence()
            {
                this.startSeqIndex = Random.Range(0, this.sequentialCollisions.Count);
                this.currentSeqIndex = this.startSeqIndex;
                if (this.sequentialCollisions.Count > 0)
                {
                    this.currentSeqCollision = this.sequentialCollisions[this.currentSeqIndex];
                    if (this.currentSeqCollision != null)
                    {
                        this.startColor = this.currentSeqCollision != null ? this.currentSeqCollision.startColor : this.startColor;
                        this.targetColor = this.currentSeqCollision.targetColor != null ? this.currentSeqCollision.targetColor : this.targetColor;
                    }
                }
            }

            private void setupColor()
            {
                if (this.activeColor == null)
                {
                    this.activeColor = this.GetComponent<ActiveColor>();
                }
                if (this.activeColor != null)
                {
                    this.activeColor.material = this.GetComponent<Renderer>().material;
                    this.activeColor.material.EnableKeyword("_EMISSION");
                    this.activeColor.instance = this.gameObject;
                    this.activeColor.usingEmission = true;
                    this.triggerColor();
                }
            }

            private void setupScale()
            {
                if (this.activeScale == null)
                {
                    this.activeScale = this.GetComponent<ActiveScale>();
                }

                if (this.activeScale != null)
                {
                    this.activeScale.instance = this.gameObject;
                }
            }

            public void hide()
            {
                if (this.activeScale != null)
                {
                    TriggerObj t = new TriggerObj();
                    ActiveTrigger a = new ActiveTrigger();
                    a.start = new Vec(1, 1, 1);
                    a.value = new Vec(1, 1, 1);
                    a.target = new Vec(0, 0, 0);
                    a.onTime = 0.8f;
                    a.offTime = this.growBackTime;
                    a.offDelay = this.stayHiddenTime;
                    this.activeScale.Trigger(t, a, null);
                }
            }

            private void triggerColor()
            {
                if (this.activeColor != null && this.currentSeqCollision != null)
                {
                    this.startColor = this.currentSeqCollision.startColor;
                    this.targetColor = this.currentSeqCollision.targetColor;
                    this.activeColor.setBaseColors(this.startColor); //sets the color to come back to. 

                    TriggerObj t = new TriggerObj();
                    ActiveTrigger a = new ActiveTrigger();
                    a.targetColor = new Col(
                        targetColor.r,
                        targetColor.g,
                        targetColor.b
                    );
                    a.onTime = 0.08f;
                    a.offTime = 0.3f;
                    this.activeColor.Trigger(t, a, null);
                }
            }

            public void setNextCollision()
            {
                this.currentSeqIndex++;
                this.setCollision(this.currentSeqIndex); //module is included within set. 
            }

            public void setRandomCollision()
            {
                int randomInteger = Random.Range(0, this.sequentialCollisions.Count);
                this.setCollision(this.currentSeqIndex);
                this.triggerColor();
            }

            public void setCollision(int _index)
            {
                //experimental.
                int index = 0;
                switch (_index)
                {
                    case -1:
                        index = Random.Range(0, this.sequentialCollisions.Count);
                        break;
                    case -2:
                        this.hide();
                        break;
                    default:
                        index = _index;
                        break;
                }

                this.currentSeqIndex = index;
                this.currentSeqIndex = (int)Mathf.Repeat(this.currentSeqIndex, this.sequentialCollisions.Count);
                this.currentSeqCollision = this.sequentialCollisions[this.currentSeqIndex];
                this.triggerColor();
            }

            public override void OnMusicCollision(MusicCollision musicCollision, MusicObject other)
            {
                if (other.HasTag("changer"))
                {
                    this.setNextCollision(); //should have a side effect. 
                }
                bool shouldFire = this.CheckShouldFire(musicCollision, other);
                MusicCollision coll = this.currentSeqCollision != null ? this.currentSeqCollision : musicCollision;
                if (shouldFire)
                {
                    float vel = this.CalculateVelocity(coll, other);
                    int note = this.CalculateNote(coll, other);
                    int channel = this.CalculateChannel(musicCollision, other); //note - this is using the musicCollision for channel.
                    float length = this.CalculateLength(coll, other);

                    //clamping out too quiet values. 
                    if (vel > 0.2f)
                    {
                        this.SendMidiNote(channel, note, vel, length);
                    }
                }
                //base.OnMusicCollision(musicCollision, other);
            }

            public override void OnCollision(MusicCollision musicCollision, MusicObject other)
            {
                //visual or logic effects that happen on each collision. musicCollision can be null.
                base.OnCollision(musicCollision, other);
                this.triggerColor();
                if (other.HasTag("eraser"))
                {
                    this.hide();
                }
            }
        }
    }
}