using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using MidiJack;

public class TestMidiOut : MonoBehaviour
{
    // just for debug purposes. 
    // 0 is usually the wavetable synth. 
    // 1 is loopMIDI.  
    public int midiOutDeviceIndex = 1;
    private uint midiEndPoint;


    void Start()
    {
        testMidiOut();
    }

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            testMidiOut();
        }
    }

    private void testMidiOut()
    {
        midiEndPoint = GetSendEndpointIdAtIndex(midiOutDeviceIndex);
        int note = Random.Range(20, 120);
        MidiMaster.SendNoteOn(midiEndPoint, MidiJack.MidiChannel.Ch1, note, 0.8f);
        StartCoroutine(sendNoteOff(note));
    }


    private IEnumerator sendNoteOff(int _note)
    {
        yield return new WaitForSeconds(0.2f);
        MidiMaster.SendNoteOff(midiEndPoint, MidiJack.MidiChannel.Ch1, _note, 0.8f);
    }


    #region Native Plugin Interface

    // MIDI OUT
    [DllImport("MidiJackPlugin", EntryPoint = "MidiJackCountSendEndpoints")]
    static extern int CountSendEndpoints();

    [DllImport("MidiJackPlugin", EntryPoint = "MidiJackGetSendEndpointIDAtIndex")]
    static extern uint GetSendEndpointIdAtIndex(int index);

    [DllImport("MidiJackPlugin")]
    static extern System.IntPtr MidiJackGetSendEndpointName(uint id);

    static string GetSendEndpointName(uint id)
    {
        return Marshal.PtrToStringAnsi(MidiJackGetSendEndpointName(id));
    }

    #endregion

}
