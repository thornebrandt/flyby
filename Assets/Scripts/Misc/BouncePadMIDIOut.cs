
using UnityEngine;
using flyby.Controllers;
using flyby.Active;
using flyby.Core;
using flyby.Triggers;

public class BouncePadMIDIOut : MonoBehaviour
{
    //deprecated - use inheritance of MusicObject. 

    private Rigidbody rb;
    public int channel;
    public int lowNote = 55;
    public int highNote = 85;
    public Color targetColor;
    public Color startingColor; // use startingColor. 
    public ActiveColor activeColor;
    public bool random;
    private MidiController midiController;
    private uint midiEndPoint;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        midiController = MidiController.instance;
        setupColor();
    }

    private void setupColor()
    {
        if (this.activeColor == null)
        {
            this.activeColor = this.GetComponent<ActiveColor>();
        }
        if (this.activeColor != null)
        {
            this.activeColor.material = this.GetComponent<Renderer>().material;
            this.activeColor.material.EnableKeyword("_EMISSION");
            this.activeColor.instance = this.gameObject;
            this.activeColor.usingEmission = true;
            this.activeColor.setBaseColors(startingColor);
        }
    }

    private void triggerColor()
    {
        if (this.activeColor != null)
        {
            TriggerObj t = new TriggerObj();
            ActiveTrigger a = new ActiveTrigger();
            a.targetColor = new Col(
                targetColor.r,
                targetColor.g,
                targetColor.b
            );
            a.onTime = 0.08f;
            a.offTime = 0.3f;
            this.activeColor.Trigger(t, a, null);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        float vel = Random.Range(0.5f, 1.0f);
        int note = Random.Range(lowNote, highNote);
        float length = 0.2f;
        this.sendMidiNote(channel, note, vel, length);
    }

    void OnCollisionEnter(Collision collision)
    {
        Vector3 collisionForce = collision.impulse / Time.fixedDeltaTime;
        float impactForce = collision.relativeVelocity.magnitude;

        float normalizedForce = Mathf.InverseLerp(2.0f, 8.0f, impactForce);
        float vel = Mathf.Lerp(0.1f, 1.0f, normalizedForce);
        if (random)
        {
            vel = Random.Range(0.3f, 0.5f);
        }

        float normalizedDirection = Mathf.InverseLerp(-5.0f, 5.0f, collision.impulse.x);
        int note = (int)Mathf.Lerp(lowNote, highNote, normalizedDirection);
        if (random)
        {
            note = Random.Range(lowNote, highNote);
        }

        float length = 0.2f;

        if (vel > 0.2f)
        {
            this.triggerColor();
            sendMidiNote(channel, note, vel, length);
        }
    }

    private void sendMidiNote(int channel, int note, float vel, float length)
    {
        if (midiController != null)
        {
            midiController.triggerMidiNote(channel, note, vel, length);
        }
    }
}
