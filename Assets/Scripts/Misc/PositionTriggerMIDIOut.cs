using UnityEngine;
using flyby.Controllers;
using flyby.Active;
using flyby.Core;
using flyby.Triggers;

public class PositionTriggerMIDIOut : MonoBehaviour
{
    public string collissionType = "";
    public int midiOutDeviceIndex = 0; //0 on legion.  1 on desktop. 
    public int channel = 0;
    public int minNote = 28;
    public int maxNote = 100;
    public float minPosition = -4;
    public float maxPosition = 4;
    private MidiController midiController;
    //private bool random = true; //not used yet.
    public ActiveColor activeColor;
    public Color targetColor;
    public Color startingColor; // use startingColor. 


    void Start()
    {
        midiController = MidiController.instance;
    }

    private void triggerColor()
    {
        if (this.activeColor != null)
        {
            TriggerObj t = new TriggerObj();
            ActiveTrigger a = new ActiveTrigger();
            a.targetColor = new Col(
                targetColor.r,
                targetColor.g,
                targetColor.b
            );
            a.onTime = 0.08f;
            a.offTime = 0.3f;
            this.activeColor.Trigger(t, a, null);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        bool shouldFire = true;

        if (other.gameObject.GetComponent<PositionTriggerMIDIOut>() != null)
        {
            //prevent double hits. 
            shouldFire = false;
            Vector3 thisPos = this.transform.position;
            Vector3 colPos = other.transform.position;
            if (thisPos.y > colPos.y)
            {
                shouldFire = true;
            }
            if (thisPos.y == colPos.y)
            {
                if (thisPos.x > colPos.x)
                {
                    shouldFire = true;
                }
            }
        }

        ActiveDiscord colliderDiscord = other.gameObject.GetComponent<ActiveDiscord>();
        int channel = this.channel;
        if (colliderDiscord != null)
        {
            if (colliderDiscord.collisionMIDIChannel > -1)
            {
                channel = colliderDiscord.collisionMIDIChannel;
            }
        }

        if (shouldFire)
        {
            //send the midi note.
            float hitPoint = other.transform.position.y;
            float normalizedValue = Mathf.InverseLerp(this.minPosition, this.maxPosition, hitPoint);
            float length = 0.26f;
            int note = (int)Mathf.Lerp(this.minNote, this.maxNote, normalizedValue);
            float vel = Random.Range(0.3f, 0.5f);
            this.sendMidiNote(channel, note, vel, length);
        }
        //fire color either way. 
        this.triggerColor();
    }

    private void sendMidiNote(int channel, int note, float vel, float length)
    {
        if (midiController != null)
        {
            midiController.triggerMidiNote(channel, note, vel, length);
        }
    }

}