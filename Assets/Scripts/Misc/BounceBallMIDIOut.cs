using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using MidiJack;
using flyby.Controllers;

public class BounceBallMIDIOut : MonoBehaviour
{
    private Rigidbody rb;
    public int channel;
    public int lowNote = 55;
    public int highNote = 65;
    public bool random;
    public int midiOutDeviceIndex = 1; // TODO -  get from midi controller. (HARDCODED FOR SKETCH)
    private MidiController midiController;
    private uint midiEndPoint;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        midiController = MidiController.instance;
    }

    private int ExtractNumberFromName(string name)
    {
        string numberString = System.Text.RegularExpressions.Regex.Match(name, @"\d+").Value;
        return int.TryParse(numberString, out int number) ? number : 0;
    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<BounceBallMIDIOut>() != null)
        {
            bool shouldFire = false;
            Vector3 thisPos = this.transform.position;
            Vector3 colPos = collision.transform.position;
            if (thisPos.y > colPos.y)
            {
                shouldFire = true;
            }
            if (thisPos.y == colPos.y)
            {
                if (thisPos.x > colPos.x)
                {
                    shouldFire = true;
                }
            }

            if (shouldFire)
            {
                Vector3 collisionForce = collision.impulse / Time.fixedDeltaTime;
                float impactForce = collision.relativeVelocity.magnitude;

                float normalizedForce = Mathf.InverseLerp(2.0f, 8.0f, impactForce);
                float vel = Mathf.Lerp(0.1f, 1.0f, normalizedForce);
                if (random)
                {
                    vel = Random.Range(0.3f, 0.5f);
                }

                float normalizedDirection = Mathf.InverseLerp(-5.0f, 5.0f, collision.impulse.x);
                int note = (int)Mathf.Lerp(lowNote, highNote, normalizedDirection);
                if (random)
                {
                    note = Random.Range(lowNote, highNote);
                }

                float length = 0.2f;
                sendMidiNote(channel, note, vel, length);
            }
        }
    }

    private void sendMidiNote(int channel, int note, float vel, float length)
    {
        if (midiController != null)
        {
            midiController.triggerMidiNote(channel, note, vel, length);
        }
    }
}
