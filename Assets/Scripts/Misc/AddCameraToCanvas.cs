using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using flyby.Active;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;

public class AddCameraToCanvas : ActiveBehaviour {
    private Canvas canvas;
    // simple utility to add main camera to canvas to objects appear in front. 
    // listens for a "toggle_camera" event. 
    // good example of how to make a barebones imple activebehaviour that listens to one event to do one task. 

    public void assignCamera() {
        canvas = GetComponent<Canvas>();
        if (canvas != null && CameraController.instance != null) {
            //will need to adjust if switching to spout camera. 
            canvas.worldCamera = CameraController.instance.cam;
        }
    }

    public override void Intro(TriggerObj triggerObj, Obj o) {
        assignCamera();
    }

    public override void TriggerEventHandler(TriggerObj triggerObj, Obj o) {
        if (triggerObj.eventName == "toggle_spout") {
            assignCamera();
        }
    }

}
