using UnityEngine;
using System.Collections;

public class ReloadUtility : MonoBehaviour
{
    public GameObject targetObject;
    public float timeout = 0.5f; //defaults to half a second. 

    void Start()
    {
        StartCoroutine(ToggleObject());
    }

    IEnumerator ToggleObject()
    {
        Debug.Log("setting timeout");
        targetObject.SetActive(false);
        yield return new WaitForSeconds(timeout);
        Debug.Log("turning back on");
        targetObject.SetActive(true);

    }

}