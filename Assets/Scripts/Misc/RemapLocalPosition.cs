using UnityEngine;

public class RemapLocalPosition : MonoBehaviour
{
    //very simple utility. 
    // note: there seems to be a bug, when used with fullIK, that freezes the position of this object if it is nested.


    // TODO - eventually can fade this in within the activeConstraint behavior. 

    // and can allow spawning of children. 
    public Transform nestedChild; // Reference to the nested child
    private Vector3 initialOffset; //set the child up on what you want to be zero, then apply additional transforms with activePosition. 

    void Start()
    {
        initialOffset = nestedChild.localPosition;
    }

    void Update()
    {
        // Update the nested child's position based on the first-level child's position
        nestedChild.localPosition = this.transform.localPosition + initialOffset;
    }
}