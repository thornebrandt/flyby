using UnityEngine;
//using Windows.Kinect;

using System;
using System.Collections.Generic;
using com.rfilkov.kinect;

//TODO - if position/rotations are hosed, manually set offsetNode to the parent. 

namespace flyby
{
    [RequireComponent(typeof(Animator))]
    public class KinectAvatar : MonoBehaviour
    {

        // userId of the player
        // not using index - 
        [NonSerialized]
        public ulong playerId = 0;

        [Tooltip("Whether the avatar is facing the player or not.")]
        public bool mirroredMovement = true;

        [Tooltip("Whether the avatar is allowed to move vertically or not.")]
        public bool verticalMovement = true;

        [Tooltip("Whether the avatar is allowed to move horizontally or not.")]
        public bool horizontalMovement = true;

        [Tooltip("Whether the avatar's root motion is applied by other component or script.")]
        public bool externalRootMotion = false;

        [Tooltip("Whether the head rotation is controlled externally (e.g. by VR-headset).")]
        public bool externalHeadRotation = false;

        [Tooltip("Whether the hand and finger rotations are controlled externally (e.g. by LeapMotion controller)")]
        public bool externalHandRotations = false;

        [Tooltip("Whether the finger orientations are allowed or not.")]
        public bool fingerOrientations = false;

        [Tooltip("Rate at which the avatar will move through the scene.")]
        public float moveRate = 1f;

        [Tooltip("Smooth factor used for avatar movements and joint rotations.")]
        public float smoothFactor = 10f;

        [Tooltip("Whether to update the avatar in LateUpdate(), instead of in Update(). Needed for Mecanim animation blending.")]
        public bool lateUpdateAvatar = false;

        [Tooltip("Game object this transform is relative to (optional).")]
        public Transform offsetNode;

        [Tooltip("If enabled, makes the avatar position relative to this camera to be the same as the player's position to the sensor.")]
        public Camera posRelativeToCamera;

        [Tooltip("Whether the avatar's position should match the color image (in Pos-rel-to-camera mode only).")]
        public bool posRelOverlayColor = false;

        [Tooltip("Whether z-axis movement needs to be inverted (Pos-Relative mode only).")]
        public bool posRelInvertedZ = false;

        [Tooltip("Whether the avatar's feet must stick to the ground.")]
        public bool groundedFeet = false;

        [Tooltip("Whether to apply the humanoid model's muscle limits or not.")]
        public bool applyMuscleLimits = false;

        [Tooltip("Whether to flip left and right, relative to the sensor.")]
        public bool flipLeftRight = false;

        [Tooltip("Horizontal offset of the avatar with respect to the position of user's spine-base.")]
        [Range(-0.5f, 0.5f)]
        public float horizontalOffset = 0f;

        [Tooltip("Vertical offset of the avatar with respect to the position of user's spine-base.")]
        [Range(-0.5f, 0.5f)]
        public float verticalOffset = 0f;

        [Tooltip("Forward offset of the avatar with respect to the position of user's spine-base.")]
        [Range(-0.5f, 0.5f)]
        public float forwardOffset = 0f;

        // suggested and implemented by Ruben Gonzalez
        [Tooltip("Whether to use unscaled or normal (scaled) time.")]
        public bool useUnscaledTime = false;

        [Tooltip("Radius of the joint sphere and bone capsule colliders, in meters. You can set it to 0.02 to try it out. 0 means no collider.")]
        [Range(0f, 0.1f)]
        public float boneColliderRadius = 0f;  // 0.02f
        protected Transform bodyRoot;
        protected float hipCenterDist = 0f;
        protected Transform[] bones;
        //protected Transform[] fingerBones;
        protected CapsuleCollider[] boneColliders;
        protected Transform[] boneColTrans;
        protected Transform[] boneColJoint;
        protected Transform[] boneColParent;
        protected Quaternion[] initialRotations;
        protected Quaternion[] localRotations;
        protected bool[] isBoneDisabled;
        protected Dictionary<HumanBodyBones, Quaternion> fingerBoneLocalRotations = new Dictionary<HumanBodyBones, Quaternion>();
        protected Dictionary<HumanBodyBones, Vector3> fingerBoneLocalAxes = new Dictionary<HumanBodyBones, Vector3>();
        protected Vector3 initialPosition;
        protected Quaternion initialRotation;
        protected Vector3 initialHipsPosition;
        protected Quaternion initialHipsRotation;
        protected Vector3 initialUpVector;

        protected Vector3 bodyRootPosition;

        [NonSerialized]
        public bool offsetCalibrated = false;
        protected Vector3 offsetPos = Vector3.zero;

        protected bool poseApplied = false;
        protected Quaternion pelvisRotation = Quaternion.identity;


        // sharp rotation angle
        protected const float sharpRotAngle = 90f;  // 90 degrees

        private Animator animatorComponent = null;
        private HumanPoseHandler humanPoseHandler = null;
        private HumanPose humanPose = new HumanPose();

        protected KinectManager kinectManager;
        //// last hand events
        //private InteractionManager.HandEventType lastLeftHandEvent = InteractionManager.HandEventType.Release;
        //private InteractionManager.HandEventType lastRightHandEvent = InteractionManager.HandEventType.Release;

        //// fist states
        //private bool bLeftFistDone = false;
        //private bool bRightFistDone = false;
        private const int raycastLayers = ~2;  // Ignore Raycast
        private const float maxFootDistanceGround = 0.02f;  // maximum distance from lower foot to the ground
        private const float maxFootDistanceTime = 0.2f; // 1.0f;  // maximum allowed time, the lower foot to be distant from the ground
        private Transform leftFoot, rightFoot;
        private Vector3 leftFootPos, rightFootPos;

        private float fFootDistanceInitial = 0f;
        //private float fFootDistance = 0f;
        //private float fFootDistanceTime = 0f;
        private Vector3 vFootCorrection = Vector3.zero;

        private Transform _transformCache;


        public void Awake()
        {
            if (this.bones != null)
            {
                return;
            }
            if (!this.gameObject.activeInHierarchy)
            {
                return;
            }
            bones = new Transform[25];
            animatorComponent = GetComponent<Animator>();
            MapBones();

            // get distance to hip center
            Vector3 bodyRootPos = bodyRoot != null ? bodyRoot.localPosition : transform.localPosition;
            Vector3 hipCenterPos = bodyRoot != null ? bodyRoot.localPosition : (bones != null && bones.Length > 0 && bones[0] != null ? bones[0].localPosition : transform.localPosition);
            hipCenterDist = (hipCenterPos - bodyRootPos).magnitude;

            // Initial rotations and directions of the bones.
            initialRotations = new Quaternion[bones.Length];
            localRotations = new Quaternion[bones.Length];
            isBoneDisabled = new bool[bones.Length];

            // Get initial bone rotations
            GetInitialRotations();

            // get initial distance to ground
            fFootDistanceInitial = GetCorrDistanceToGround();
            // fFootDistance = 0f;
            // fFootDistanceTime = 0f;

            if (animatorComponent && animatorComponent.avatar && animatorComponent.avatar.isHuman)
            {
                //Transform hipsTransform = animator.GetBoneTransform(HumanBodyBones.Hips);
                //Transform rootTransform = hipsTransform.parent;
                Transform rootTransform = transform;

                humanPoseHandler = new HumanPoseHandler(animatorComponent.avatar, rootTransform);
                humanPoseHandler.GetHumanPose(ref humanPose);

                initialHipsPosition = (humanPose.bodyPosition - rootTransform.localPosition);  // hipsTransform.position
                initialHipsRotation = humanPose.bodyRotation;
                //Debug.Log("initial hips pos: " + initialHipsPosition + ", rot: " + initialHipsRotation.eulerAngles);
            }
            CreateBoneColliders();
        }

        public void Update()
        {
            this.kinectManager = this.checkForKinectManger();

            //we are only setting userID, not messing with index in this flybyscript.
            if (this.kinectManager.GetUserIndexById(playerId) != -1)
            {
                if (!lateUpdateAvatar && this.playerId != 0)
                {
                    UpdateAvatar(this.playerId);
                }
                //user exists in the kinectManager. 
            }
            else
            {
                this.playerId = 0;
                //send something back about missing the avatar?
            }
        }

        public KinectManager checkForKinectManger()
        {
            if (kinectManager == null)
            {
                kinectManager = KinectManager.Instance;
                if (kinectManager = null)
                {
                    Debug.LogWarning("KinectManager not found.");
                    return null;
                }
            }
            return kinectManager;
        }

        public void LateUpdate()
        {
            if (lateUpdateAvatar && this.playerId != 0)
            {
                this.kinectManager = this.checkForKinectManger();
                UpdateAvatar(this.playerId);
            }
        }

        private void CheckMuscleLimits()
        {
            //not using atm
        }

        public void UpdateAvatar(ulong userId)
        {
            if (!gameObject.activeInHierarchy)
            {
                return;
            }
            //in avatarController there are some blocks about hand events here.
            float pelvisAngle = getPelvisAngle(userId, false);
        }

        // returns the angle between the last and current pelvis orientations (in degrees 0-180), or -1 if anything goes wrong
        protected float getPelvisAngle(ulong userId, bool flip)
        {
            int iJoint = (int)KinectInterop.JointType.Pelvis;
            if (kinectManager == null || !kinectManager.IsJointTracked(userId, iJoint))
                return -1f;

            // get Kinect joint orientation
            Quaternion jointRotation = kinectManager.GetJointOrientation(userId, iJoint, flip);
            if (jointRotation == Quaternion.identity)
                return -1f;

            float angle = Quaternion.Angle(pelvisRotation, jointRotation);

            return angle;
        }

        protected void CreateBoneColliders()
        {
            if (boneColliderRadius <= 0f)
                return;

            boneColliders = new CapsuleCollider[bones.Length];
            boneColTrans = new Transform[bones.Length];
            boneColJoint = new Transform[bones.Length];
            boneColParent = new Transform[bones.Length];

            for (int i = 0; i < bones.Length; i++)
            {
                if (bones[i] == null)
                    continue;

                SphereCollider jCollider = bones[i].gameObject.AddComponent<SphereCollider>();
                jCollider.radius = boneColliderRadius;

                if (i > 0)
                {
                    GameObject objBoneCollider = new GameObject("BoneCollider" + i);
                    objBoneCollider.transform.parent = bones[i];
                    boneColTrans[i] = objBoneCollider.transform;

                    CapsuleCollider bCollider = objBoneCollider.AddComponent<CapsuleCollider>();
                    bCollider.radius = boneColliderRadius;
                    bCollider.height = 0f;

                    boneColliders[i] = bCollider;
                }
            }

            for (int i = 0; i < bones.Length; i++)
            {
                if (boneColliders[i] == null)
                    continue;

                boneColJoint[i] = bones[i];
                Transform parentTrans = boneColJoint[i].parent;

                while (parentTrans != null)
                {
                    if (parentTrans.GetComponent<SphereCollider>() != null)
                        break;
                    parentTrans = parentTrans.parent;
                }

                if (parentTrans != null)
                    boneColParent[i] = parentTrans;
                else
                    boneColliders[i] = null;
            }
        }

        protected void GetInitialRotations()
        {
            initialPosition = transform.localPosition;
            initialRotation = transform.localRotation;
            initialUpVector = transform.up;

            transform.rotation = Quaternion.identity;

            // save the body root initial position
            if (bodyRoot != null)
            {
                bodyRootPosition = bodyRoot.localPosition;
            }
            else
            {
                bodyRootPosition = transform.localPosition;
            }

            if (offsetNode != null)
            {
                bodyRootPosition = bodyRootPosition - offsetNode.localPosition;
            }

            // save the initial bone rotations
            for (int i = 0; i < bones.Length; i++)
            {
                if (bones[i] != null)
                {
                    initialRotations[i] = bones[i].localRotation;
                    localRotations[i] = bones[i].localRotation;
                }
            }

            // get finger bones' local rotations
            foreach (int boneIndex in boneIndex2MultiBoneMap.Keys)
            {
                List<HumanBodyBones> alBones = boneIndex2MultiBoneMap[boneIndex];

                for (int b = 0; b < alBones.Count; b++)
                {
                    HumanBodyBones bone = alBones[b];
                    Transform boneTransform = animatorComponent ? animatorComponent.GetBoneTransform(bone) : null;

                    // get the finger's 1st transform
                    Transform fingerBaseTransform = animatorComponent ? animatorComponent.GetBoneTransform(alBones[b - (b % 3)]) : null;

                    // get the finger's 2nd transform
                    Transform baseChildTransform = fingerBaseTransform && fingerBaseTransform.childCount > 0 ? fingerBaseTransform.GetChild(0) : null;
                    Vector3 vBoneDirChild = baseChildTransform && fingerBaseTransform ? (baseChildTransform.localPosition - fingerBaseTransform.localPosition).normalized : Vector3.zero;
                    Vector3 vOrthoDirChild = Vector3.Cross(vBoneDirChild, Vector3.up).normalized;

                    if (boneTransform)
                    {
                        fingerBoneLocalRotations[bone] = boneTransform.localRotation;

                        if (vBoneDirChild != Vector3.zero)
                        {
                            fingerBoneLocalAxes[bone] = boneTransform.InverseTransformDirection(vOrthoDirChild).normalized;
                        }
                        else
                        {
                            fingerBoneLocalAxes[bone] = Vector3.zero;
                        }
                    }
                }
            }

            // Restore the initial rotation
            transform.localRotation = initialRotation;
        }

        // If the bones to be mapped have been declared, map that bone to the model.
        protected virtual void MapBones()
        {
            for (int boneIndex = 0; boneIndex < bones.Length; boneIndex++)
            {
                if (!boneIndex2MecanimMap.ContainsKey(boneIndex))
                    continue;

                bones[boneIndex] = animatorComponent ? animatorComponent.GetBoneTransform(boneIndex2MecanimMap[boneIndex]) : null;
            }

            // ---TODO test finger bones. 

            //fingerBones = new Transform[fingerIndex2MecanimMap.Count];
            //for (int boneIndex = 0; boneIndex < fingerBones.Length; boneIndex++)
            //{
            //    if (!fingerIndex2MecanimMap.ContainsKey(boneIndex))
            //        continue;

            //    fingerBones[boneIndex] = animatorComponent ? animatorComponent.GetBoneTransform(fingerIndex2MecanimMap[boneIndex]) : null;
            //}
        }

        protected virtual float GetCorrDistanceToGround(Transform trans, Vector3 initialPos, bool isRightJoint)
        {
            if (!trans)
                return 0f;

            Vector3 deltaDir = trans.localPosition - initialPos;
            Vector3 vTrans = new Vector3(deltaDir.x * initialUpVector.x, deltaDir.y * initialUpVector.y, deltaDir.z * initialUpVector.z);
            float fSign = Vector3.Dot(deltaDir, initialUpVector) < 0f ? 1f : -1f;  // change the sign, because it's a correction
            float deltaDist = fSign * vTrans.magnitude;
            return deltaDist;
        }

        protected virtual float GetCorrDistanceToGround()
        {
            if (leftFoot == null && rightFoot == null)
            {
                leftFoot = GetBoneTransform(GetBoneIndexByJoint(KinectInterop.JointType.FootLeft, false));
                rightFoot = GetBoneTransform(GetBoneIndexByJoint(KinectInterop.JointType.FootRight, false));

                if (leftFoot == null || rightFoot == null)
                {
                    leftFoot = GetBoneTransform(GetBoneIndexByJoint(KinectInterop.JointType.AnkleLeft, false));
                    rightFoot = GetBoneTransform(GetBoneIndexByJoint(KinectInterop.JointType.AnkleRight, false));
                }

                leftFootPos = leftFoot != null ? leftFoot.localPosition : Vector3.zero;
                rightFootPos = rightFoot != null ? rightFoot.localPosition : Vector3.zero;
            }

            float fDistMin = 1000f;
            float fDistLeft = leftFoot ? GetCorrDistanceToGround(leftFoot, leftFootPos, false) : fDistMin;
            float fDistRight = rightFoot ? GetCorrDistanceToGround(rightFoot, rightFootPos, true) : fDistMin;
            fDistMin = Mathf.Abs(fDistLeft) < Mathf.Abs(fDistRight) ? fDistLeft : fDistRight;

            if (fDistMin == 1000f)
            {
                fDistMin = 0f;
            }
            return fDistMin;
        }

        public int GetBoneTransformCount()
        {
            return bones != null ? bones.Length : 0;
        }

        public Transform GetBoneTransform(int index)
        {
            if (index >= 0 && bones != null && index < bones.Length)
            {
                return bones[index];
            }

            return null;
        }

        public void DisableBone(int index, bool resetBone)
        {
            if (index >= 0 && index < bones.Length)
            {
                isBoneDisabled[index] = true;

                if (resetBone && bones[index] != null)
                {
                    bones[index].localRotation = localRotations[index];
                }
            }
        }

        public void EnableBone(int index)
        {
            if (index >= 0 && index < bones.Length)
            {
                isBoneDisabled[index] = false;
            }
        }

        public bool IsBoneEnabled(int index)
        {
            if (index >= 0 && index < bones.Length)
            {
                return !isBoneDisabled[index];
            }
            return false;
        }

        public int GetBoneIndexByJoint(KinectInterop.JointType joint, bool bMirrored)
        {
            int boneIndex = -1;
            if (jointMap2boneIndex.ContainsKey(joint))
            {
                boneIndex = !bMirrored ? jointMap2boneIndex[joint] : mirrorJointMap2boneIndex[joint];
            }
            return boneIndex;
        }

        public List<HumanBodyBones> GetMecanimBones()
        {
            List<HumanBodyBones> alMecanimBones = new List<HumanBodyBones>();
            for (int boneIndex = 0; boneIndex < bones.Length; boneIndex++)
            {
                if (!boneIndex2MecanimMap.ContainsKey(boneIndex) || boneIndex >= 21)
                    continue;

                alMecanimBones.Add(boneIndex2MecanimMap[boneIndex]);
            }
            return alMecanimBones;
        }

        public new Transform transform
        {
            get
            {
                if (!_transformCache)
                {
                    _transformCache = base.transform;
                }
                return _transformCache;
            }
        }


        protected static readonly Dictionary<KinectInterop.JointType, int> jointMap2boneIndex = new Dictionary<KinectInterop.JointType, int>
        {
            {KinectInterop.JointType.Pelvis, 0},
            {KinectInterop.JointType.SpineNaval, 1},
            {KinectInterop.JointType.SpineChest, 2},
            {KinectInterop.JointType.Neck, 3},
            {KinectInterop.JointType.Head, 4},

            {KinectInterop.JointType.ClavicleLeft, 5},
            {KinectInterop.JointType.ShoulderLeft, 6},
            {KinectInterop.JointType.ElbowLeft, 7},
            {KinectInterop.JointType.WristLeft, 8},

            {KinectInterop.JointType.ClavicleRight, 9},
            {KinectInterop.JointType.ShoulderRight, 10},
            {KinectInterop.JointType.ElbowRight, 11},
            {KinectInterop.JointType.WristRight, 12},

            {KinectInterop.JointType.HipLeft, 13},
            {KinectInterop.JointType.KneeLeft, 14},
            {KinectInterop.JointType.AnkleLeft, 15},
            {KinectInterop.JointType.FootLeft, 16},

            {KinectInterop.JointType.HipRight, 17},
            {KinectInterop.JointType.KneeRight, 18},
            {KinectInterop.JointType.AnkleRight, 19},
            {KinectInterop.JointType.FootRight, 20},
        };

        protected static readonly Dictionary<KinectInterop.JointType, int> mirrorJointMap2boneIndex = new Dictionary<KinectInterop.JointType, int>
        {
            {KinectInterop.JointType.Pelvis, 0},
            {KinectInterop.JointType.SpineNaval, 1},
            {KinectInterop.JointType.SpineChest, 2},
            {KinectInterop.JointType.Neck, 3},
            {KinectInterop.JointType.Head, 4},

            {KinectInterop.JointType.ClavicleRight, 5},
            {KinectInterop.JointType.ShoulderRight, 6},
            {KinectInterop.JointType.ElbowRight, 7},
            {KinectInterop.JointType.WristRight, 8},

            {KinectInterop.JointType.ClavicleLeft, 9},
            {KinectInterop.JointType.ShoulderLeft, 10},
            {KinectInterop.JointType.ElbowLeft, 11},
            {KinectInterop.JointType.WristLeft, 12},

            {KinectInterop.JointType.HipRight, 13},
            {KinectInterop.JointType.KneeRight, 14},
            {KinectInterop.JointType.AnkleRight, 15},
            {KinectInterop.JointType.FootRight, 16},

            {KinectInterop.JointType.HipLeft, 17},
            {KinectInterop.JointType.KneeLeft, 18},
            {KinectInterop.JointType.AnkleLeft, 19},
            {KinectInterop.JointType.FootLeft, 20},
        };

        protected static readonly Dictionary<int, HumanBodyBones> boneIndex2MecanimMap = new Dictionary<int, HumanBodyBones>
        {
            {0, HumanBodyBones.Hips},
            {1, HumanBodyBones.Spine},
            {2, HumanBodyBones.Chest},
            {3, HumanBodyBones.Neck},
            {4, HumanBodyBones.Head},

            {5, HumanBodyBones.LeftShoulder},
            {6, HumanBodyBones.LeftUpperArm},
            {7, HumanBodyBones.LeftLowerArm},
            {8, HumanBodyBones.LeftHand},

            {9, HumanBodyBones.RightShoulder},
            {10, HumanBodyBones.RightUpperArm},
            {11, HumanBodyBones.RightLowerArm},
            {12, HumanBodyBones.RightHand},

            {13, HumanBodyBones.LeftUpperLeg},
            {14, HumanBodyBones.LeftLowerLeg},
            {15, HumanBodyBones.LeftFoot},
            {16, HumanBodyBones.LeftToes},

            {17, HumanBodyBones.RightUpperLeg},
            {18, HumanBodyBones.RightLowerLeg},
            {19, HumanBodyBones.RightFoot},
            {20, HumanBodyBones.RightToes},

            {21, HumanBodyBones.LeftIndexProximal},
            {22, HumanBodyBones.LeftThumbProximal},
            {23, HumanBodyBones.RightIndexProximal},
            {24, HumanBodyBones.RightThumbProximal},
        };


        protected static readonly Dictionary<int, List<HumanBodyBones>> boneIndex2MultiBoneMap = new Dictionary<int, List<HumanBodyBones>>
        {
            {21, new List<HumanBodyBones> {  // left fingers
				    HumanBodyBones.LeftIndexProximal,
                    HumanBodyBones.LeftIndexIntermediate,
                    HumanBodyBones.LeftIndexDistal,
                    HumanBodyBones.LeftMiddleProximal,
                    HumanBodyBones.LeftMiddleIntermediate,
                    HumanBodyBones.LeftMiddleDistal,
                    HumanBodyBones.LeftRingProximal,
                    HumanBodyBones.LeftRingIntermediate,
                    HumanBodyBones.LeftRingDistal,
                    HumanBodyBones.LeftLittleProximal,
                    HumanBodyBones.LeftLittleIntermediate,
                    HumanBodyBones.LeftLittleDistal,
                }},
            {22, new List<HumanBodyBones> {  // left thumb
				    HumanBodyBones.LeftThumbProximal,
                    HumanBodyBones.LeftThumbIntermediate,
                    HumanBodyBones.LeftThumbDistal,
                }},
            {23, new List<HumanBodyBones> {  // right fingers
				    HumanBodyBones.RightIndexProximal,
                    HumanBodyBones.RightIndexIntermediate,
                    HumanBodyBones.RightIndexDistal,
                    HumanBodyBones.RightMiddleProximal,
                    HumanBodyBones.RightMiddleIntermediate,
                    HumanBodyBones.RightMiddleDistal,
                    HumanBodyBones.RightRingProximal,
                    HumanBodyBones.RightRingIntermediate,
                    HumanBodyBones.RightRingDistal,
                    HumanBodyBones.RightLittleProximal,
                    HumanBodyBones.RightLittleIntermediate,
                    HumanBodyBones.RightLittleDistal,
                }},
            {24, new List<HumanBodyBones> {  // right thumb
				    HumanBodyBones.RightThumbProximal,
                    HumanBodyBones.RightThumbIntermediate,
                    HumanBodyBones.RightThumbDistal,
                }},
        };


    }
}
