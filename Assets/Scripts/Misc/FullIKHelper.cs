using RootMotion.FinalIK;
using UnityEngine;
using System.Collections.Generic;
using flyby.Triggers;
using flyby.Core;


namespace flyby
{
    namespace Active
    {
        public class FullIKHelper : ActiveBehaviour
        {
            public FullBodyBipedIK ik;

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                Debug.Log("SOLVING!: " + ik.transform.name);
                ik.solver.Initiate(ik.transform);
                Debug.Log("SOLVED: " + ik.solver);
            }
        }
    }
}
