using UnityEngine;

public class ForceAttractor : MonoBehaviour
{
    //experimental utility function that will attract a rigidbody to this object. ( both position and rotation ) 
    //NOTE - this works best with high drag ( over 3 ) 

    public Rigidbody targetRigidbody;  // The Rigidbody of the object that will be attracted
    private float attractionStrength = 10f;  // Strength of the attraction
    private float dampingFactor = 0.6f;  // Damping factor to reduce tangential velocity when near the attractor
    private float closeDistanceThreshold = 0.5f;  // Distance within which additional damping is applied
    private float maxForce = 100f;  // Cap on the maximum force applied to avoid excessive acceleration

    public float rotationStrength = 5f;  // Strength of the torque applied to align rotation
    public float angularDampingFactor = 0.5f;  // Damping factor to reduce angular velocity near target rotation

    private Transform targetOrientation;

    void FixedUpdate()
    {
        if (targetRigidbody != null)
        {
            //position
            Vector3 direction = transform.position - targetRigidbody.position;
            float distance = direction.magnitude;
            if (distance > 0.01f)
            {
                Vector3 force = direction.normalized * Mathf.Min(attractionStrength * distance, maxForce);
                targetRigidbody.AddForce(force, ForceMode.Acceleration);
                if (distance < closeDistanceThreshold)
                {
                    targetRigidbody.velocity *= (1 - dampingFactor * Time.fixedDeltaTime);
                }
            }

            targetOrientation = this.transform;
            // Rotational Alignment with Shortest Path
            if (targetOrientation != null)
            {
                Quaternion targetRotation = targetOrientation.rotation;  // Use world rotation directly
                Quaternion currentRotation = targetRigidbody.rotation;
                Quaternion rotationDelta = targetRotation * Quaternion.Inverse(currentRotation);
                Vector3 torque = new Vector3(rotationDelta.x, rotationDelta.y, rotationDelta.z) * rotationStrength;
                targetRigidbody.AddTorque(torque, ForceMode.Acceleration);
                if (Quaternion.Angle(currentRotation, targetRotation) < 10f)
                {
                    targetRigidbody.angularVelocity *= (1 - angularDampingFactor * Time.fixedDeltaTime);
                }
            }
        }
    }
}