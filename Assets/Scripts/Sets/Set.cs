using System.Collections.Generic;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Controllers;
using Newtonsoft.Json;
using UnityEngine;

namespace flyby
{
    namespace Sets
    {
        public class Set
        {
            public string label;
            public string jsonPath;
            public List<string> songs;
            public List<string> triggers;
            public List<string> presets; // list of objects that are accessible as presets, but are not assigned to a part.
            public List<Trigger> loadedTriggers;
            public List<Song> preloadedSongs; //for adding event listeners to trigger song.
            public List<NullObj> preloadedObjs; //a list of all bare boned objs, so presets can be copied. 
            public Song currentSong;
            public int currentSongIndex;

            public delegate void SongChanged(Song song);
            public static event SongChanged OnSongChanged;

            public Set getSetFromJSON(string path)
            {
                string setJSON = Json.getJSON(jsonPath + path);
                Set set = JsonConvert.DeserializeObject<Set>(setJSON);

                if (set == null)
                {
#if UNITY_EDITOR
                    Debug.Log("no set found at " + jsonPath + path + ", so we're quitting!");
                    UnityEditor.EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif
                }

                //takes a while to quit, so we still do a null check. 
                if (set != null)
                {

                    this.presets = set.presets;
                    set.jsonPath = jsonPath;
                    set.preloadedSongs = preloadSongs(set);
                    set.preloadedObjs = preloadObjs(set.preloadedSongs);
                    this.preloadedObjs = set.preloadedObjs;
                    this.preloadedSongs = set.preloadedSongs;
                    this.songs = set.songs;
                    this.triggers = set.triggers;
                    this.loadedTriggers = set.loadedTriggers;
                    this.currentSong = set.currentSong;
                    this.currentSongIndex = 0;
                    this.label = set.label;
                }

                return this;
            }

            private List<Song> preloadSongs(Set set)
            {
                List<Song> preloadedSongs = new List<Song>();
                foreach (string songPath in set.songs)
                {
                    preloadedSongs.Add(preloadSong(songPath));
                }
                return preloadedSongs;
            }

            private List<NullObj> preloadObjs(List<Song> preloadedSongs)
            {
                List<NullObj> preloadedObjs = preloadPresets();
                foreach (Song song in preloadedSongs)
                {
                    song.jsonPath = jsonPath;
                    List<Part> parts = song.preloadParts(song.parts);
                    foreach (Part part in parts)
                    {
                        part.jsonPath = jsonPath;
                        List<NullObj> objList = part.parseNullObjs();
                        preloadedObjs.AddRange(objList);
                    }
                }
                return preloadedObjs;
            }

            private List<NullObj> preloadPresets()
            {
                List<NullObj> preloadedObjs = new List<NullObj>();
                if (this.presets != null)
                {
                    foreach (string presetPath in this.presets)
                    {
                        preloadedObjs.AddRange(getNullObjsFromJSON(jsonPath + presetPath));
                    }
                }
                return preloadedObjs;
            }

            public List<NullObj> getNullObjsFromJSON(string path)
            {
                string componentJSON = Json.getJSON(path);
                List<NullObj> componentObjs = JsonConvert.DeserializeObject<List<NullObj>>(componentJSON);
                return componentObjs;
            }

            private Song preloadSong(string songPath)
            {
                Song song = Song.fromJSON(this.jsonPath + songPath);
                return song;
            }

            public void eventHandler(string eventName)
            {
                for (int i = 0; i < preloadedSongs.Count; i++)
                {
                    Song song = preloadedSongs[i];
                    if (song.trigger == eventName)
                    {
                        this.currentSong = loadSong(i);
                        break;
                    }

                    if (this.currentSong.nextTrigger == eventName)
                    {
                        int nextSong = currentSongIndex + 1;
                        this.currentSong = loadSong(nextSong);
                        return;
                    }

                    if (this.currentSong.prevTrigger == eventName)
                    {
                        int prevSong = currentSongIndex - 1;
                        this.currentSong = loadSong(prevSong);
                        return;
                    }
                }
            }

            public Song loadSong(int songInt)
            {
                Song song = new Song();
                song.jsonPath = this.jsonPath;


                if (this.preloadedSongs != null && this.preloadedSongs.Count > 0)
                {
                    songInt = (int)Mathf.Repeat(songInt, this.preloadedSongs.Count);
                    if (songInt != currentSongIndex || this.currentSong == null)
                    {
                        this.currentSongIndex = songInt;
                        string songPath = this.songs[songInt];
                        this.currentSong = song.getSongWithPartsFromJSON(this.jsonPath + songPath, preloadedObjs);
                        this.currentSong.ResetSortingOrder();
                        OnSongChanged.Invoke(this.currentSong);
                    }
                    return this.currentSong;
                }
                else
                {
                    Debug.Log("no songs.");
                    return song;
                }
            }

            public List<Trigger> loadTriggers()
            {
                this.loadedTriggers = new List<Trigger>();
                foreach (string triggerPath in this.triggers)
                {
                    string triggersJSON = Json.getJSON(jsonPath + triggerPath);
                    if (!string.IsNullOrEmpty(triggersJSON))
                    {
                        List<Trigger> triggerList = JsonConvert.DeserializeObject<List<Trigger>>(triggersJSON);
                        this.loadedTriggers.AddRange(triggerList);
                    }
                }
                //also loads app triggers. 
                if (Config.instance != null)
                {
                    string appTriggersJSON = "";
                    if (!string.IsNullOrEmpty(Config.instance.appTriggersPath))
                    {
                        appTriggersJSON = Json.getJSON(Config.instance.appTriggersPath);
                    }

                    if (!string.IsNullOrEmpty(appTriggersJSON))
                    {
                        List<Trigger> appTriggers = JsonConvert.DeserializeObject<List<Trigger>>(appTriggersJSON);
                        this.loadedTriggers.AddRange(appTriggers);
                    }
                    else
                    {
                        this.loadedTriggers.AddRange(getDefaultAppTriggers());
                    }
                }

                return this.loadedTriggers;
            }

            private List<Trigger> getDefaultAppTriggers()
            {
                List<Trigger> appTriggers = new List<Trigger>();
                appTriggers.Add(
                    new Trigger
                    {
                        eventName = "reset",
                        key = "space"
                    }
                );
                appTriggers.Add(
                    new Trigger
                    {
                        eventName = "triggerDebug",
                        key = "m"
                    }
                );
                appTriggers.Add(
                    new Trigger
                    {
                        eventName = "next_part",
                        key = "right"
                    }
                );
                appTriggers.Add(
                    new Trigger
                    {
                        eventName = "prev_part",
                        key = "left"
                    }
                );
                appTriggers.Add(
                    new Trigger
                    {
                        eventName = "next_song",
                        key = "up"
                    }
                );
                appTriggers.Add(
                    new Trigger
                    {
                        eventName = "prev_song",
                        key = "down"
                    }
                );
                appTriggers.Add(
                    new Trigger
                    {
                        eventName = "next_camera_angle",
                        key = "r"
                    }
                );
                Debug.Log("Added " + appTriggers.Count + " default app triggers");
                return appTriggers;
            }

            public Set()
            {
                this.triggers = new List<string>();
                this.loadedTriggers = new List<Trigger>();
                this.preloadedSongs = new List<Song>();
            }
        }
    }
}