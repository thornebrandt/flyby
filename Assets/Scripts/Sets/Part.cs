using System.Collections;
using System.Collections.Generic;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Active;
using flyby.Triggers;
using Newtonsoft.Json;
using UnityEngine;
using System;


namespace flyby
{
    namespace Sets
    {
        public class Part
        {
            public string id;
            public string bundle;
            public string trigger;
            public string nextTrigger;
            public string prevTrigger;
            public string jsonPath;
            public string scene; //loading additive scene
            public List<string> components;
            public AssetController assetController;
            public List<Obj> objs;
            public List<Obj> preRenderedObjs; //objs loaded when parts are changed. 
            public List<NullObj> nullObjs;
            public List<CameraAngle> cameraAngles; //linear progression of camera-specific properties.
            public Obj cameraTriggers; //for temporary triggers between cameraAngles, like knobRotation and punch. 
            public List<GameObject> sceneGameObjects; //to delete when part changes. 
            public List<ActiveBehaviour> sceneActiveBehaviours; //for event listeners. 
            public List<Role> roles; //used in socketController - sets up socket role and associated triggers.
            public List<Role> chatRoles; //used in socketController - for discord. 
            public List<Body> bodies; //used in kinectController - sets up kinect body and associated triggers.
            public List<LFO> lfos; //used in lfoController - sets up lfo and associated triggers.

            //private 
            public Dictionary<string, int> groupIndexes;
            public Dictionary<string, int> groupLengths;
            public Dictionary<string, int> startGroupIndexes; //to get back on zero.  
            public List<string> groupsToIncrement; //for lateUpdate.


            [HideInInspector]
            private CameraController cameraController;
            [HideInInspector]
            private SocketController socketController;

            public Part getPartFromJSON(string path, List<NullObj> preloadedObjs)
            {
                string partJSON = Json.getJSON(path);

                // these JSONSettings makes the json overwrite an object instead of appending to the default list. 
                // never knew that this happened until writing unit tests.
                var JSONSettings = new JsonSerializerSettings
                {
                    ObjectCreationHandling = ObjectCreationHandling.Replace
                };

                Part part = JsonConvert.DeserializeObject<Part>(partJSON, JSONSettings);
                this.components = part.components;
                this.nextTrigger = part.nextTrigger;
                this.prevTrigger = part.prevTrigger;
                this.cameraAngles = part.cameraAngles;
                this.cameraTriggers = part.cameraTriggers;
                this.roles = part.roles;
                this.chatRoles = part.chatRoles;
                this.bodies = part.bodies;
                this.lfos = part.lfos;
                this.objs = new List<Obj>();
                this.assetController = setupAssetController();
                this.nullObjs = parseNullObjs();
                this.id = part.id;
                this.trigger = part.trigger;
                this.bundle = part.bundle;
                this.objs = parseObjs(this.nullObjs, preloadedObjs);
                this.scene = part.scene;
                this.groupIndexes = part.groupIndexes;
                this.groupLengths = part.groupLengths;
                this.startGroupIndexes = part.startGroupIndexes;
                return this;
            }

            public Part getPartFromJSON(string path)
            {
                return getPartFromJSON(path, new List<NullObj>());
            }

            public List<GameObject> FindSceneObjects(string sceneName)
            {
                List<GameObject> gos = new List<GameObject>();
                foreach (GameObject go in UnityEngine.Object.FindObjectsOfType(typeof(GameObject)))
                {
                    if (go.scene.name.CompareTo(sceneName) == 0)
                    {
                        //turning off sameras. 
                        if (go.GetComponent<Camera>() != null)
                        {
                            go.SetActive(false);
                        }
                        gos.Add(go);
                    }
                }
                return gos;
            }

            public List<ActiveBehaviour> FindSceneActiveBehaviours(string sceneName)
            {
                List<ActiveBehaviour> abs = new List<ActiveBehaviour>();
                TriggerObj t = new TriggerObj();
                foreach (ActiveBehaviour ab in UnityEngine.Object.FindObjectsOfType<ActiveBehaviour>())
                {
                    if (ab.gameObject.scene.name.CompareTo(sceneName) == 0)
                    {
                        ab.instantiated = true;
                        ab.instance = ab.gameObject;
                        ab.Intro(t, null);
                        abs.Add(ab);
                    }
                }
                return abs;
            }



            public Part preloadPartFromJSON(string path)
            {
                string partJSON = Json.getJSON(path);
                Part part = JsonConvert.DeserializeObject<Part>(partJSON);
                return part;
            }


            public List<NullObj> parseNullObjs()
            {
                List<NullObj> nullObjsList = new List<NullObj>();
                if (this.components != null)
                {
                    foreach (string componentPath in this.components)
                    {
                        nullObjsList.AddRange(getNullObjsFromJSON(jsonPath + componentPath));
                    }
                }
                return nullObjsList;
            }

            public List<Obj> parseObjs(List<NullObj> nullObjs, List<NullObj> preloadedObjs)
            {
                this.objs = new List<Obj>();
                this.preRenderedObjs = new List<Obj>();

                //checking for solo mode. 
                bool soloMode = false;
                foreach (NullObj no in nullObjs)
                {
                    if (!string.IsNullOrEmpty(no.solo))
                    {
                        soloMode = true;
                        break;
                    }
                }

                if (soloMode)
                {
                    foreach (NullObj o in nullObjs)
                    {
                        if (string.IsNullOrEmpty(o.solo))
                        {
                            o.disabled = "true";
                        }
                    }
                }
                //---------

                foreach (NullObj nullObj in nullObjs)
                {
                    Obj obj = parseObj(nullObj, preloadedObjs);
                    if (obj.array == null)
                    {
                        this.objs.Add(obj);
                    }
                    else
                    {
                        //makeArray can add a single obj with manipulated activeTriggers. 
                        List<Obj> arrayObjs = obj.array.makeArray(obj);
                        this.objs.AddRange(arrayObjs);
                    }
                }

                foreach (Obj o in objs)
                {
                    if (o.preRender)
                    {
                        this.preRenderedObjs.Add(o);
                    }
                }
                this.objs = setupObjs(this.objs);
                return this.objs;
            }

            public List<Obj> parseObjs(List<NullObj> nullObjs)
            {
                return this.parseObjs(nullObjs, null);
            }

            public List<Obj> setupObjs(List<Obj> _objs)
            {
                loadAssets(_objs);
                addEventListeners(_objs);
                return _objs;
            }

            private Obj parseObj(NullObj nullObj, List<NullObj> preloadedObjs)
            {
                Obj obj = new Obj();
                if (!string.IsNullOrEmpty(nullObj.preset))
                {
                    obj = findPreset(nullObj, preloadedObjs);
                }
                obj.overwrite(nullObj);
                return obj;
            }

            private Obj findPreset(NullObj nullObj, List<NullObj> preloadedObjs)
            {
                if (preloadedObjs != null)
                {
                    foreach (NullObj preloadedObj in preloadedObjs)
                    {
                        if (preloadedObj.id == nullObj.preset)
                        {
                            if (!string.IsNullOrEmpty(preloadedObj.preset))
                            {
                                NullObj presetPreset = findNullPreset(preloadedObj, preloadedObjs);
                                return new Obj(presetPreset);
                            }
                            return new Obj(preloadedObj);
                        }
                    }
                }
                return new Obj(nullObj);
            }

            private NullObj findNullPreset(NullObj nullObj, List<NullObj> preloadedObjs)
            {
                if (preloadedObjs != null)
                {
                    foreach (NullObj preloadedObj in preloadedObjs)
                    {
                        if (preloadedObj.id == nullObj.preset)
                        {
                            NullObj newPreset = new NullObj(preloadedObj);
                            newPreset.overwrite(nullObj);
                            return newPreset;
                        }
                    }
                }
                return nullObj;
            }

            public AssetController setupAssetController()
            {
                if (AssetController.instance == null || AssetController.instance.defaultPrefab == null)
                {
                    AssetController assetController = new AssetController();
                    assetController.setup();
                }
                this.assetController = AssetController.instance;
                return this.assetController;
            }

            public List<NullObj> getNullObjsFromJSON(string path)
            {
                string componentJSON;
                List<NullObj> componentObjs;

                try
                {
                    componentJSON = Json.getJSON(path);
                    componentObjs = JsonConvert.DeserializeObject<List<NullObj>>(componentJSON);

                }
                catch (Exception e)
                {
                    Debug.Log("something went wrong fetching " + path + " " + e);
                    componentObjs = new List<NullObj>();
                }

                return componentObjs;
            }

            public void loadAssets(List<Obj> objs)
            {
                foreach (Obj obj in objs)
                {
                    string bundleName = getBundleName(obj);
                    if (obj.disabled)
                    {
                        Debug.Log(obj.asset + " is disabled.");
                        continue;
                    }
                    obj.prefab = this.assetController.loadPrefab(obj.asset, bundleName);
                }
            }

            public string getBundleName(Obj obj)
            {
                if (!string.IsNullOrEmpty(obj.asset) && string.IsNullOrEmpty(obj.bundle) && !string.IsNullOrEmpty(this.bundle))
                {
                    return this.bundle;
                }
                return obj.bundle;
            }

            public void addEventListeners(List<Obj> objs)
            {
                //does this belong here? perhaps listeners should be added by parent controller. 
                //and this controller should only be responsible for sending events. 
                foreach (Obj obj in objs)
                {
                    obj.addEventListeners();
                }
            }

            public void kill()
            {
                if (this.objs != null)
                {
                    foreach (Obj o in this.objs)
                    {
                        o.kill();
                    }
                }
                if (this.preRenderedObjs != null)
                {
                    foreach (Obj p in this.preRenderedObjs)
                    {
                        p.kill();
                    }
                }
                this.preRenderedObjs = null;
                this.killAdditiveScene();
                this.assetController.kill();
            }

            public void killAdditiveScene()
            {
                this.sceneActiveBehaviours = new List<ActiveBehaviour>();
                if (this.sceneGameObjects != null)
                {
                    foreach (GameObject go in this.sceneGameObjects)
                    {
                        if (Application.isPlaying)
                        {
                            UnityEngine.Object.Destroy(go);
                        }
                        else
                        {
                            UnityEngine.Object.DestroyImmediate(go);
                        }
                    }
                }
            }

            public void activateObjs()
            {
                if (this.objs != null)
                {
                    foreach (Obj o in this.objs)
                    {
                        o.current = true;
                    }

                    if (this.preRenderedObjs != null)
                    {
                        foreach (Obj o in this.preRenderedObjs)
                        {
                            o.preRenderObj();
                        }
                    }
                }
            }

            public void deactivateObjs()
            {
                if (this.objs != null)
                {
                    foreach (Obj o in this.objs)
                    {
                        //unless has "persistant.
                        o.current = false;
                        o.hide();
                    }
                }
            }

            public void setupInstances()
            {
                List<GameObject> gos = new List<GameObject>();
                foreach (Obj o in this.objs)
                {
                    o.setupInstances();
                }
            }

            public void setupGroups()
            {
                this.groupLengths = new Dictionary<string, int>();
                this.groupIndexes = new Dictionary<string, int>();
                this.startGroupIndexes = new Dictionary<string, int>();

                foreach (Obj o in this.objs)
                {
                    o.setupGroup();
                }
            }

            public int getCurrentGroupIndex(string groupName)
            {
                //get group index frm the list of groupNames. 
                //try to get from groupIndexes 
                int groupIndex;
                if (this.groupIndexes != null && this.groupIndexes.TryGetValue(groupName, out groupIndex))
                {
                    return this.groupIndexes[groupName];
                }
                else
                {
                    //this should mean there are no groupIndexes for this group.
                    return -999;
                }
            }

            public int getGroupLength(string _group)
            {
                //mostly for testing. 
                int groupLength;
                if (groupLengths.TryGetValue(_group, out groupLength))
                {
                    return groupLengths[_group];
                }
                else
                {
                    return 0;
                }
            }

            private int increaseGroupIndex(string _group)
            {
                if (String.IsNullOrEmpty(_group))
                {
                    return -999;
                }
                int groupIndex;
                if (groupIndexes != null && groupIndexes.TryGetValue(_group, out groupIndex))
                {
                    int groupLength = groupLengths[_group];
                    groupIndex = groupIndex + 1;
                    groupIndex = (int)Mathf.Repeat(groupIndex, groupLength);
                    groupIndexes[_group] = groupIndex;
                    return groupIndex;
                }
                else
                {
                    return assignGroupIndex(_group);
                }
            }

            public void assignIncrementToGroupIndex(string _group)
            {
                //store groups to incrememnt for LateUpdate().
                if (String.IsNullOrEmpty(_group))
                {
                    return;
                }
                if (this.groupsToIncrement == null)
                {
                    this.groupsToIncrement = new List<string>();
                }
                this.groupsToIncrement.Add(_group);
            }

            public void lateUpdate()
            {
                //note: not a monobehavior. flybyController will call this. 
                if (groupsToIncrement != null && groupsToIncrement.Count > 0)
                {
                    foreach (string group in groupsToIncrement)
                    {
                        increaseGroupIndex(group);
                    }
                    groupsToIncrement.Clear();
                }
            }

            public int assignGroupIndex(string _group)
            {
                if (String.IsNullOrEmpty(_group))
                {
                    //usually doesn't actually return an int anyways. 
                    return -999;
                }

                //each time a group is added - 
                int groupLength;
                if (groupLengths != null && groupLengths.TryGetValue(_group, out groupLength))
                {
                    groupLength = groupLength + 1;
                    this.groupLengths[_group] = groupLength;
                }
                else
                {
                    if (this.groupIndexes == null)
                    {
                        this.groupIndexes = new Dictionary<string, int>();
                    }
                    this.groupIndexes[_group] = 0;
                    if (this.groupLengths == null)
                    {
                        this.groupLengths = new Dictionary<string, int>();
                    }
                    this.groupLengths[_group] = 1;
                }
                //experimental.
                if (this.startGroupIndexes == null)
                {
                    this.startGroupIndexes = new Dictionary<string, int>();
                }

                this.startGroupIndexes[_group] = groupLengths[_group] - 1;
                //----  not sure what this is doing. 

                int groupIndex = groupLengths[_group] - 1;
                return groupIndex;
            }

            public void eventHandler(TriggerObj triggerObj)
            {
                string eventName = triggerObj.eventName;
                if (this.cameraController != null && !triggerObj.noteOff)
                {
                    this.cameraController.eventHandler(triggerObj);
                }
                if (this.sceneActiveBehaviours != null)
                {
                    foreach (ActiveBehaviour activeBehaviour in this.sceneActiveBehaviours)
                    {
                        activeBehaviour.TriggerEventHandler(triggerObj, null);
                    }
                }
            }

            public void setupCameraController(CameraController cameraController)
            {
                this.cameraController = cameraController;
                cameraController.setup(this.cameraAngles, this.cameraTriggers);
            }

            public Part()
            {
                //*-------- this population is pretty problamatic - think about moving to setup function.  
                this.cameraAngles = new List<CameraAngle>(){
                    new CameraAngle()
                };
                this.cameraAngles[0].id = "default_camera_angle";
                this.nextTrigger = "next_part";
                this.prevTrigger = "prev_part";

                //this.currentCameraAngle = this.cameraAngles[0];
                //***----problematic - think about moving to setup function 
            }
        }
    }
}