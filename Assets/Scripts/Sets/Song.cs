using System.Collections;
using System.Collections.Generic;
using flyby.Core;
using flyby.Tools;
using flyby.Controllers;
using Newtonsoft.Json;
using UnityEngine;

namespace flyby
{
    namespace Sets
    {
        public class Song
        {
            public string id;
            public string jsonPath;
            public string trigger;
            public string nextTrigger;
            public string prevTrigger;
            public string bundle; //objs inherits bundle from paths, which inherit from song, which inherit from config. 
            public string assetPath; //additional place to look for assets. 
            public List<string> parts;
            public List<Part> loadedParts;
            public Part currentPart;
            public int currentPartIndex;
            public int partI;
            private int currentSortingOrder = 0;

            private List<NullObj> preloadedObjs;

            public delegate void PartChanged(Part part);
            public static event PartChanged OnPartChanged;

            public Song getSongWithPartsFromJSON(string path, List<NullObj> preloadedObjs)
            {
                this.preloadedObjs = preloadedObjs;
                string songJSON = Json.getJSON(path);
                Song song = JsonConvert.DeserializeObject<Song>(songJSON);
                this.parts = song.parts;
                this.id = song.id;
                this.trigger = song.trigger;
                this.bundle = song.bundle;
                this.assetPath = song.assetPath;
                this.loadedParts = loadParts(parts, preloadedObjs);
                this.currentPartIndex = 0;
                this.currentPart = setCurrentPart(currentPartIndex);
                return this;
            }

            public Song getSongWithPartsFromJSON(string path)
            {
                return this.getSongWithPartsFromJSON(path, new List<NullObj>());
            }


            public int GetNextSortingOrder()
            {
                return this.currentSortingOrder++;
            }

            public void ResetSortingOrder()
            {
                this.currentSortingOrder = 0;
            }

            public static Song fromJSON(string path)
            {
                string songJSON = Json.getJSON(path);
                Song song = JsonConvert.DeserializeObject<Song>(songJSON);
                return song;
            }

            public List<Part> loadParts(List<string> partPaths, List<NullObj> preloadedObjs)
            {
                List<Part> loadedParts = new List<Part>();
                foreach (string partPath in partPaths)
                {
                    Part loadedPart = new Part();
                    loadedPart.jsonPath = jsonPath;
                    loadedPart = loadedPart.getPartFromJSON(jsonPath + partPath, preloadedObjs);
                    loadedPart.bundle = assignBundle(loadedPart);
                    loadedPart.setupInstances();
                    loadedParts.Add(loadedPart);
                }
                return loadedParts;
            }

            private string assignBundle(Part part)
            {
                if (string.IsNullOrEmpty(part.bundle) && !string.IsNullOrEmpty(this.bundle))
                {
                    part.bundle = this.bundle;
                }
                return part.bundle;
            }

            public List<Part> preloadParts(List<string> partPaths)
            {
                List<Part> loadedParts = new List<Part>();
                foreach (string partPath in partPaths)
                {
                    Part loadedPart = new Part();
                    loadedPart.jsonPath = jsonPath;
                    loadedPart = loadedPart.preloadPartFromJSON(jsonPath + partPath);
                    loadedParts.Add(loadedPart);
                }
                return loadedParts;
            }

            public Part setCurrentPart(int partIndex)
            {
                if (this.loadedParts != null && this.loadedParts.Count > 0)
                {
                    this.currentPart = this.loadedParts[partIndex];
                }
                else
                {
                    this.currentPart = new Part();
                }
                this.currentPartIndex = partIndex;
                //current part needs to be set before this.
                if (Application.isPlaying)
                {
                    FlyByController.instance.prerenderPart = this.currentPart;
                }
                this.currentPart.activateObjs();
                return this.currentPart;
            }

            private void deactivateAllParts()
            {
                foreach (Part part in loadedParts)
                {
                    part.deactivateObjs();
                }
            }

            private void loadPartByIndex(int i)
            {
                Debug.Log("loadParts.Count: " + loadedParts.Count);
                if (this.loadedParts != null && this.loadedParts.Count > 0)
                {
                    i = (int)Mathf.Repeat(i, loadedParts.Count);
                    //first check if there is just one part.
                    Debug.Log("loading part: " + i + " and: " + currentPartIndex);
                    if (i != currentPartIndex)
                    {
                        deactivateAllParts();
                        setCurrentPart(i);
                        OnPartChanged.Invoke(this.currentPart);
                    }
                }
            }

            public void eventHandler(string eventName)
            {
                for (int i = 0; i < loadedParts.Count; i++)
                {
                    Part part = loadedParts[i];
                    if (part.trigger == eventName)
                    {
                        deactivateAllParts();
                        setCurrentPart(i);
                        OnPartChanged.Invoke(this.currentPart);
                        break;
                    }
                }

                if (currentPart.nextTrigger == eventName)
                {
                    Debug.Log("currentPart nextTrigger is being loaded : " + currentPart.nextTrigger);
                    int nextPart = currentPartIndex + 1;
                    loadPartByIndex(nextPart);
                    return;
                }

                if (currentPart.prevTrigger == eventName)
                {
                    int prevPart = currentPartIndex - 1;
                    loadPartByIndex(prevPart);
                    return;
                }
            }

            public void kill()
            {
                if (this.parts != null)
                {
                    foreach (Part part in this.loadedParts)
                    {
                        part.kill();
                    }
                }
            }

            public Song()
            {
                this.nextTrigger = "next_song"; //default next trigger.
                this.prevTrigger = "prev_song"; //default prev trigger.
            }
        }
    }
}