using System;
using System.Reflection;
using flyby.Core;
using flyby.Triggers;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    [Serializable]
    // TODO: extend parent "InstanceArray" class.
    // this one is meant for continuous scrolling backgrounds. 

    // NOTES:------
    // this usually needs "ActiveType: All"  for desired speed effects. 
    // (it is currently being forced)
    // if there is an undersired overlap on intro - it usually means the startTime of a speed transform needs to be zero. 

    public class StackArray
    {
        public Vector3 normalized; // 1 should stack exactly. 
        public Vec exact; // in case normalized stacking is not working. 
        public Vector3 instanceDimensions; //dimensions are multiplied to create the obj.numInstances. for examples ( 2, 3, 1) creates 6 instances. 
        public Vec boundaries; //make rectangular bounds with vec2. 
        [HideInInspector]
        public Vector3 cachedStack; //private for storing stack used - either instance bounds or custom offset. 
        [HideInInspector]
        public Vector3 cachedStackDirection;
        [HideInInspector]
        public float cachedStackDistance; //linear value of the distance of the stack. 
        [HideInInspector]
        public Vector3 fullStack; //stack times instances. 
        [HideInInspector]
        public Vec cachedOrigin;
        [HideInInspector]
        public Vector3 cachedOriginVector3;
        public bool introduced;
        public bool centered;
        private TriggerObj t;
        private List<Vector3> previousPositions;
        private GameObject prevInstance;
        private List<GameObject> instances;

        public Vector3 getStack(Obj o)
        {
            //stack can either be instance bounds from calculate scale, or custom offset. 
            if (this.cachedStack != null && this.cachedStack != Vector3.zero)
            {
                //return the cache. 
                return this.cachedStack;
            }
            cachedStack = o.nullVector;
            if (normalized == null && exact == null)
            {
                this.cachedStack = o.nullVector;
            }
            Vector3 scale = o.calculateScale();
            if (this.normalized != null)
            {
                //one scale on an axis should suffice. 
                this.cachedStack = Vector3.Scale(this.normalized, scale);
            }
            if (this.exact != null)
            {
                //if the scaled precision isn't cutting it. 
                this.cachedStack = this.exact.getVector3();
            }
            return this.cachedStack;
        }

        public void kill()
        {
            this.cachedStack = Vector3.zero;
            this.cachedStackDirection = Vector3.zero;
            this.cachedStackDistance = 0;
            this.introduced = false;
        }

        public Vector3 getStackDirection(Obj o)
        {
            //re-investigate. 
            if (this.cachedStackDirection != null && this.cachedStackDirection != Vector3.zero)
            {
                return this.cachedStackDirection;
            }

            Vector3 speed = Vector3.one;
            Vector3 stack = this.getStack(o);

            this.cachedStackDirection = new Vector3(
                Mathf.Abs(stack.x) * Mathf.Sign(speed.x),
                Mathf.Abs(stack.y) * Mathf.Sign(speed.y),
                Mathf.Abs(stack.z) * Mathf.Sign(speed.z)
            );

            return this.cachedStackDirection;
        }


        public float getStackDistance(Obj o)
        {
            if (this.cachedStackDistance != 0)
            {
                return this.cachedStackDistance;
            }
            this.cachedStackDistance = Vector3.Distance(this.cachedStackDirection, Vector3.zero);
            return cachedStackDistance;
        }

        public void initializeStack(Obj o)
        {
            if (!o.disabled)
            {
                t = new TriggerObj
                {
                    source = "stack"
                };

                this.cachedStack = this.getStack(o);
                this.cachedStackDirection = this.getStackDirection(o);
                this.cachedStackDistance = this.getStackDistance(o); //for single dimension and precision. 
                this.preRenderStack(o);
                this.previousPositions = getPreviousPositions(o);
                this.introduced = true;
            }
        }

        private List<Vector3> getPreviousPositions(Obj o)
        {
            this.previousPositions = new List<Vector3>();
            foreach (GameObject instance in o.instances)
            {
                this.previousPositions.Add(instance.transform.localPosition);  //fallback to absolute if this is weird. 
            }
            return this.previousPositions;
        }

        public void preRenderStack(Obj o)
        {
            this.centered = true; //can't think of a use case for not centering. 
            //fix instance dimensions so that there aren't any zero dimensions.
            //TODO - try using this for scale triggers. 
            this.instanceDimensions = Vector3.Max(Vector3.one, this.instanceDimensions);
            this.fullStack = Vector3.Scale(this.cachedStack, this.instanceDimensions);
            Vector3 newOrigin = Vector3.zero;
            Vector3 offset = o.origin.getVector3();
            if (this.centered)
            {
                offset = o.origin.getVector3() + Vector3.Scale(this.instanceDimensions, this.cachedStack) * -0.5f + (this.cachedStack * 0.5f);
            }

            for (int i_x = (int)this.instanceDimensions.x; i_x > 0; i_x--)
            {
                for (int i_y = (int)this.instanceDimensions.y; i_y > 0; i_y--)
                {
                    for (int i_z = (int)this.instanceDimensions.z; i_z > 0; i_z--)
                    {
                        newOrigin.x = this.cachedStack.x * (i_x - 1) + offset.x;
                        newOrigin.y = this.cachedStack.y * (i_y - 1) + offset.y;
                        newOrigin.z = this.cachedStack.z * (i_z - 1) + offset.z;
                        o.origin = new Vec(newOrigin);
                        o.instantiate(t);
                        GameObject instance = o.getPreviousInstance(o._i);
                    }
                }
            }

            o._i = 0;
        }

        private Vector3 getCachedOrigin(Obj o)
        {
            if (this.cachedOrigin != null)
            {
                return this.cachedOriginVector3;
            }
            this.cachedOrigin = o.origin;
            this.cachedOriginVector3 = this.cachedOrigin.getVector3();
            return this.cachedOriginVector3;
        }

        private Vector3 getPreciseOrigin(Obj o, Vector3 resetDirection)
        {
            //adds some seamless precision
            this.prevInstance = o.getPreviousInstance(o._i);
            //prevInstance.GetComponent<Renderer>().material.color = Color.red;
            Vector3 prevPos = this.prevInstance.transform.localPosition;
            float preciseX = prevPos.x + (this.cachedStack.x * resetDirection.x);
            float preciseY = prevPos.y + (this.cachedStack.y * resetDirection.y);
            float preciseZ = prevPos.z + (this.cachedStack.z * resetDirection.z);
            return new Vector3(
                preciseX,
                preciseY,
                preciseZ
            );
        }

        public void checkStack(Obj o)
        {
            //less efficient, but we can move in any direction. 
            for (int i = 0; i < o.instances.Count; i++)
            {

                GameObject instance;
                Vector3 localPosition;
                GameObject parentInstance = o.instances[i];
                Vector3 parentPosition = parentInstance.transform.localPosition;

                if (o.hierarchyType == HierarchyType.None)
                {
                    instance = parentInstance;
                    localPosition = parentPosition;
                }
                else
                {
                    instance = Obj.getPopupInstance(o.instances[i]);
                    localPosition = parentPosition + instance.transform.localPosition;
                }

                Vector3 newOffset = localPosition;
                Vector3 resetDirection = Vector3.zero;
                bool resetInstance = false;
                //+x
                if (localPosition.x < this.boundaries.x && localPosition.x < this.previousPositions[i].x)
                {
                    newOffset.x = localPosition.x + fullStack.x;
                    resetInstance = true;
                    resetDirection.x = 1;
                }

                //-x
                if (localPosition.x > this.boundaries.x2 && localPosition.x > this.previousPositions[i].x)
                {
                    newOffset.x = localPosition.x - fullStack.x;
                    resetInstance = true;
                    resetDirection.x = -1;
                }

                //+y
                if (localPosition.y < this.boundaries.y && localPosition.y < this.previousPositions[i].y)
                {
                    newOffset.y = localPosition.y + fullStack.y;
                    resetInstance = true;
                    resetDirection.y = 1;
                }

                //-y
                if (localPosition.y > this.boundaries.y2 && localPosition.y > this.previousPositions[i].y)
                {
                    newOffset.y = localPosition.y - fullStack.y;
                    resetInstance = true;
                    resetDirection.y = -1;
                }

                //+z
                if (localPosition.z < this.boundaries.z && localPosition.z < this.previousPositions[i].z)
                {
                    newOffset.z = localPosition.z + fullStack.z;
                    resetInstance = true;
                    resetDirection.z = 1;
                }

                //-z
                if (localPosition.z > this.boundaries.z2 && localPosition.z > this.previousPositions[i].z)
                {
                    newOffset.z = localPosition.z - fullStack.z;
                    resetInstance = true;
                    resetDirection.z = -1;
                }

                if (resetInstance)
                {
                    o._i = i;
                    //o.origin = new Vec(newOffset);

                    if (o.hierarchyType == HierarchyType.None)
                    {
                        instance.transform.localPosition = newOffset;
                    }
                    else
                    {
                        instance.transform.localPosition = newOffset - parentPosition;
                    }

                    //number of times attempting to fix this problem: 6  
                    //instance.transform.localPosition = getPreciseOrigin(o, resetDirection);
                }

                if (o.hierarchyType == HierarchyType.None)
                {
                    this.previousPositions[i] = instance.transform.localPosition;
                }
                else
                {
                    this.previousPositions[i] = instance.transform.localPosition + parentPosition;
                }

            }
        }

        public StackArray()
        {
            this.normalized = Vector3.one;
            this.boundaries = new Vec(
                -15,
                15,
                -15,
                15,
                -2,
                20
            );
            this.instanceDimensions = new Vector3(
                5,
                1,
                1
            );
        }

        public StackArray(StackArray obj)
        {
            foreach (FieldInfo field in obj.GetType().GetFields())
            {
                var type = Nullable.GetUnderlyingType(field.FieldType) ?? field.FieldType;
                field.SetValue(this, field.GetValue(obj));
            }
        }
    }
}