using System;
using System.Reflection;
using flyby.Core;
using flyby.Active;
using flyby.Triggers;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    public class InstanceArray
    {
        public int numInstances;
        public bool singleObj;
        public ActiveTrigger triggerSprite; //TODO - need to test propertyStrings. 
        public ActiveTrigger triggerPivot;
        public ActiveTrigger triggerPosition;
        public ActiveTrigger triggerSpeed;
        public ActiveTrigger triggerRotation;
        public ActiveTrigger triggerRotate;
        public ActiveTrigger triggerScale;
        public ActiveTrigger triggerParent; //TODO - test 3 children populating to 3 parents. 
        public ActiveTrigger triggerColor;
        public ActiveTrigger activeTrigger;
        public bool introduced;
        public bool shareGroup; //if true, groups fire all instances of array together.
        public string trigger; //should be int ( usually 0 or 1 )
        public string asset;  //should be number ( usually 0 or 1 )

        public InstanceArray()
        {
            this.numInstances = 1;
        }

        public InstanceArray(InstanceArray obj)
        {
            foreach (FieldInfo field in obj.GetType().GetFields())
            {
                var type = Nullable.GetUnderlyingType(field.FieldType) ?? field.FieldType;
                field.SetValue(this, field.GetValue(obj));
            }
        }

        public float getOffsetOffTime(float originalOffTime, float copyOffTime, int i)
        {
            if (originalOffTime == -1)
            {
                return -1;
            }
            if (copyOffTime == -1)
            {
                return -1;
            }
            return originalOffTime + (copyOffTime * i);
        }

        private bool checkUsingColors(ActiveTrigger oa, ActiveTrigger ta)
        {
            return (oa != null && (oa.startColor != null || oa.color != null || oa.targetColor != null || oa.endColor != null || oa.knobColor != null)
             || (ta != null && (ta.startColor != null || ta.color != null || ta.targetColor != null || ta.endColor != null || ta.knobColor != null)));
        }

        public ActiveTrigger multiplyActiveTrigger(ActiveTrigger oa, ActiveTrigger ta, ActiveTrigger ca, int i)
        {
            // TODO - explain why original and copy is needed if copy copies original. 
            // ( test removing original calculation. ) 

            //oa = original obj active trigger.
            //ta= this active trigger.  
            //ca = copy active trigger. 
            ca = ca == null ? new ActiveTrigger() : ca;
            oa = oa == null ? new ActiveTrigger() : oa;
            bool usingColors = checkUsingColors(oa, ta);

            ca.trigger = getIncrementalString(oa.trigger, ta.trigger, i);
            ca.propertyName = getIncrementalString(oa.propertyName, ta.propertyName, i);

            //childIndex. 
            if (ca.childIndex > -1 && ta.childIndex > -1)
            {
                int offsetChildIndex = ta.childIndex * i;
                ca.childIndex = ca.childIndex + offsetChildIndex;
            }

            //start
            Vec offsetStart = new Vec(ta.start);
            ca.start = new Vec(oa.start);
            offsetStart.Multiply(i);
            ca.start.Add(offsetStart);

            float offsetStartDelay = ta.startDelay * i;
            ca.startDelay = ca.startDelay + offsetStartDelay;
            float offsetStartTime = ta.startTime * i;
            ca.startTime = ca.startTime + offsetStartTime;

            //end 
            Vec offsetEnd = new Vec(ta.end);
            ca.end = new Vec(oa.end);
            offsetEnd.Multiply(i);
            ca.end.Add(offsetEnd);

            float offsetEndDelay = ta.endDelay * i;
            ca.endDelay = ca.endDelay + offsetEndDelay;
            float offsetEndTime = ta.endTime * i;
            ca.endTime = ca.endTime + offsetEndTime;

            //startColor 
            if (usingColors)
            {
                Col offsetStartCol = new Col(ta.startColor);
                ca.startColor = new Col(oa.startColor);
                offsetStartCol.Multiply(i);
                ca.startColor.Add(offsetStartCol);
            }

            //value
            Vec offsetValue = new Vec(ta.value);
            ca.value = new Vec(oa.value);
            offsetValue.Multiply(i);
            ca.value.Add(offsetValue);

            //value color 
            if (usingColors)
            {
                Col offsetCol = new Col(ta.color);
                ca.color = new Col(oa.color);
                offsetCol.Multiply(i);
                ca.color.Add(offsetCol);
            }

            //target vec 
            if (oa.target != null)
            {
                Vec offsetTarget;
                float offsetOnDelay = ta.onDelay * i;
                float offsetOnTime = ta.onTime * i;
                float offsetOffDelay = ta.offDelay * i;
                if (ta.target != null)
                {
                    offsetTarget = new Vec(ta.target);
                }
                else
                {
                    offsetTarget = new Vec();
                }

                ca.target = new Vec(oa.target);
                ca.onDelay = oa.onDelay + offsetOnDelay;
                ca.onTime = oa.onTime + offsetOnTime;
                ca.offDelay = oa.offDelay + offsetOffDelay;
                ca.offTime = this.getOffsetOffTime(oa.offTime, ta.offTime, i);
                //remember, add needs to be after multiply. 
                offsetTarget.Multiply(i);
                ca.target.Add(offsetTarget);
            }

            //knob vec 
            if (oa.knob != null)
            {
                Vec offsetKnob;
                if (ta.knob != null)
                {
                    offsetKnob = new Vec(ta.knob);
                }
                else
                {
                    offsetKnob = new Vec();
                }
                ca.knob = new Vec(oa.knob);
                ca.trigger = oa.trigger;
                offsetKnob.Multiply(i);
                ca.knob.Add(offsetKnob);
            }

            //knob vec 
            if (oa.knobColor != null)
            {
                Col offsetKnobColor;
                if (ta.knobColor != null)
                {
                    offsetKnobColor = new Col(ta.knobColor);
                }
                else
                {
                    offsetKnobColor = new Col();
                }
                ca.knobColor = new Col(oa.knobColor);
                offsetKnobColor.Multiply(i);
                ca.knobColor.Add(offsetKnobColor);
            }

            //target color
            if (usingColors && oa.targetColor != null)
            {
                Col offsetTargetCol = new Col(ta.targetColor);
                float offsetOnDelay = ta.onDelay * i;
                float offsetOnTime = ta.onTime * i;
                float offsetOffDelay = ta.offDelay * i;
                if (ta.targetColor != null)
                {
                    offsetTargetCol = new Col(ta.targetColor);
                }
                else
                {
                    offsetTargetCol = new Col();
                }

                ca.targetColor = new Col(oa.targetColor);
                ca.onDelay = oa.onDelay + offsetOnDelay;
                ca.onTime = oa.onTime + offsetOnTime;
                ca.offDelay = oa.offDelay + offsetOffDelay;
                ca.offTime = this.getOffsetOffTime(oa.offTime, ta.offTime, i);
                offsetTargetCol.Multiply(i);
                ca.targetColor.Add(offsetTargetCol);
            }


            //nested activeTriggers. 
            if (oa.triggers != null && ta.triggers != null)
            {
                int maxCount = (int)Mathf.Max(oa.triggers.Count, ta.triggers.Count);
                ca.triggers = new List<SecondaryActiveTrigger>();
                for (int j = 0; j < maxCount; j++)
                {
                    ActiveTrigger nestedOA = oa.triggers[j] == null ? null : new ActiveTrigger(oa.triggers[j]);
                    ActiveTrigger nestedTA = ta.triggers[j] == null ? null : new ActiveTrigger(ta.triggers[j]);
                    ActiveTrigger nestedCA = multiplyActiveTrigger(nestedOA, nestedTA, new ActiveTrigger(), i);
                    ca.triggers.Add(new SecondaryActiveTrigger(nestedCA));
                }
            }

            //copied active trigger is the one that is important. 
            return ca;
        }

        private string getIncrementalString(string oString, string startingI, int i)
        {
            if (string.IsNullOrEmpty(startingI))
            {
                return oString;
            }
            int currentI = i + int.Parse(startingI);
            return oString + currentI;
        }

        private Obj makeActiveTriggerArray(Obj o)
        {
            // TODO --
            // here we will loop through activeTriggers and add nestedActiveTriggers
            Obj newObj = new Obj(o);
            for (int i = 0; i < this.numInstances; i++)
            {
                FieldInfo[] thisFields = this.GetType().GetFields();
                foreach (FieldInfo thisField in thisFields)
                {
                    if (typeof(ActiveTrigger).IsAssignableFrom(thisField.FieldType))
                    {
                        ActiveTrigger thisA = thisField != null ? (ActiveTrigger)thisField.GetValue(this) : null;
                        FieldInfo originalField = o.GetType().GetField(thisField.Name);
                        ActiveTrigger originalA = originalField != null ? (ActiveTrigger)originalField.GetValue(o) : null;
                        FieldInfo newField = newObj.GetType().GetField(thisField.Name);
                        ActiveTrigger newObjA = newField != null ? (ActiveTrigger)newField.GetValue(newObj) : null;
                        if (thisA != null)
                        {
                            ActiveTrigger newA = new ActiveTrigger(newObjA);
                            newA = multiplyActiveTrigger(originalA, thisA, newA, i);
                            if (newObjA != null)
                            {
                                if (newObjA.triggers == null)
                                {
                                    newObjA.triggers = new List<SecondaryActiveTrigger>();
                                }
                                newObjA.triggers.Add(new SecondaryActiveTrigger(newA));
                            }
                        }
                    }
                }
            }

            return newObj;
        }

        public List<Obj> makeArray(Obj o)
        {
            List<Obj> objArray = new List<Obj>();
            if (this.singleObj)
            {
                Obj updatedObj = makeActiveTriggerArray(o);
                objArray.Add(updatedObj);
            }
            else
            {
                for (int i = 0; i < this.numInstances; i++)
                {
                    Obj copy = new Obj(o);
                    copy.id = o.id + "_copy_" + i;
                    copy.arrayIndex = i;
                    copy.trigger = getIncrementalString(o.trigger, this.trigger, i);
                    copy.asset = getIncrementalString(o.asset, this.asset, i);
                    FieldInfo[] thisFields = this.GetType().GetFields();
                    //the loop should be the fields in the array itself. 
                    foreach (FieldInfo thisField in thisFields)
                    {
                        if (typeof(ActiveTrigger).IsAssignableFrom(thisField.FieldType))
                        {
                            FieldInfo copyField = copy.GetType().GetField(thisField.Name);
                            FieldInfo originalField = o.GetType().GetField(thisField.Name);
                            ActiveTrigger originalA = originalField != null ? (ActiveTrigger)originalField.GetValue(o) : null;
                            ActiveTrigger copyA = copyField != null ? (ActiveTrigger)copyField.GetValue(copy) : null;
                            ActiveTrigger thisA = thisField != null ? (ActiveTrigger)thisField.GetValue(this) : null;
                            if (thisA != null)
                            {
                                copyA = multiplyActiveTrigger(originalA, thisA, copyA, i);
                                copyField.SetValue(copy, copyA);
                            }
                        }
                    }
                    objArray.Add(copy);
                }

                //assign reference to original. 
                foreach (Obj obj in objArray)
                {
                    obj.originalO = objArray[0];
                }

            }
            return objArray;
        }
    }
}
