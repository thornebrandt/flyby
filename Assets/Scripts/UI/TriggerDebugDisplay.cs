using System.Collections.Generic;
using UnityEngine;
using flyby.Controllers;
using flyby.Triggers;

namespace flyby
{
    namespace UI
    {
        public class TriggerDebugDisplay : MonoBehaviour
        {
            private bool display = false;
            private int messageI = 0;
            private List<string> messages = new List<string>();
            private Dictionary<int, string> knobMessages = new Dictionary<int, string>();

            private void OnEnable()
            {
                TriggerSourceController.OnTriggerEvent += TriggerEventHandler;
                MidiController.OnMidiNote += MidiNoteHandler;
                MidiController.OnMidiCC += MidiCCHandler;
                MidiController.OnNoteOut += MidiNoteOutHandler;
            }

            private void OnDisable()
            {
                TriggerSourceController.OnTriggerEvent -= TriggerEventHandler;
                MidiController.OnMidiNote -= MidiNoteHandler;
                MidiController.OnMidiCC -= MidiCCHandler;
            }

            public void toggleDisplay()
            {
                display = !display;
                if (!display)
                {
                    messageI = 0;
                    messages = new List<string>();
                }
            }

            void TriggerEventHandler(TriggerObj triggerObj)
            {
                switch (triggerObj.eventName)
                {
                    case "triggerDebug":
                        toggleDisplay();
                        break;
                    default:
                        break;
                }
            }

            void MidiCCHandler(int channel, int knob, float value)
            {
                CreateMidiKnobMessage(channel, knob, value);
            }

            void MidiNoteHandler(int channel, int note, float velocity)
            {
                CreateMidiNoteMessage(channel, note, velocity);
            }

            void MidiNoteOutHandler(int channel, int note, float velocity)
            {
                CreateMidiNoteOutMessage(channel, note, velocity);
            }

            void CreateMidiNoteOutMessage(int channel, int note, float velocity)
            {
                if (display)
                {
                    messageI++;
                    messages.Add(messageI + ": --OUT-- " + note + " - CHANNEL: " + channel + " - " + velocity);
                    if (messages.Count > 10)
                    {
                        messages.RemoveAt(0);
                    }
                }
            }

            void CreateMidiKnobMessage(int channel, int knob, float value)
            {
                if (display)
                {
                    string knobMessage = "KNOB: " + knob + " - CHANNEL: " + channel + " - " + value;
                    if (knobMessages.TryGetValue(knob, out string foundKnobMessage))
                    {
                        knobMessages[knob] = knobMessage;
                    }
                    else
                    {
                        knobMessages.Add(knob, knobMessage);
                    }
                }
            }

            void CreateMidiNoteMessage(int channel, int note, float velocity)
            {
                if (display)
                {
                    messageI++;
                    messages.Add(messageI + ": NOTE: " + note + " - CHANNEL: " + channel + " - " + velocity);
                    if (messages.Count > 10)
                    {
                        messages.RemoveAt(0);
                    }
                }
            }

            void OnGUI()
            {
                if (display)
                {
                    string text = "";

                    foreach (int knob in knobMessages.Keys)
                    {
                        text += knobMessages[knob] + "\n";
                    }

                    for (int i = messages.Count; i-- > 0;)
                    {
                        text += messages[i] + "\n";

                    }

                    if (text == "")
                    {
                        text = "LISTENING FOR MIDI";
                    }
                    int w = Screen.width, h = Screen.height;
                    GUIStyle style = new GUIStyle();
                    Rect rect = new Rect(0, 0, w, h * 2 / 30);
                    style.alignment = TextAnchor.UpperLeft;
                    style.fontSize = h * 2 / 60;
                    style.normal.textColor = new Color(0.0f, 1.0f, 0.5f, 1.0f);
                    GUI.Label(rect, text, style);
                }
            }
        }
    }
}
