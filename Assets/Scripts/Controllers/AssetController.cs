using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using UnityEngine.Networking;
using System.Collections;
using flyby.Core;

namespace flyby
{
    namespace Controllers
    {
        public class AssetController
        {
            public static AssetController instance { get; private set; } //singleton
            public Dictionary<string, GameObject> prefabCache;
            public Dictionary<string, AssetBundle> bundleLibrary;
            public Dictionary<string, Texture2D> textureLibrary;
            public GameObject defaultPrefab;
            public string defaultBundlePath = "TestPrefabs";
            public Config config;
            // responsible for textures 
            // responsible for loading and compiling assetBundles. 
            // responsible for resource management for tests. 

            public void setup()
            {
                AssetController.instance = this;
                this.prefabCache = new Dictionary<string, GameObject>();
                this.bundleLibrary = new Dictionary<string, AssetBundle>();
                this.textureLibrary = new Dictionary<string, Texture2D>();
                this.defaultPrefab = loadPrefab("Default", "TestPrefabs");
                this.config = Config.instance;

            }

            public void kill()
            {
                foreach (var bundleRef in this.bundleLibrary)
                {
                    AssetBundle assetBundle = bundleRef.Value;
                    assetBundle.Unload(true);
                    unloadTextures();
                }
                this.bundleLibrary.Clear();
            }

            public bool IsTextureLoaded(string name)
            {
                if (textureLibrary.ContainsKey(name))
                {
                    return true;
                }
                return false;
            }

            public void CacheTexture(string name, Texture2D texture)
            {
                if (textureLibrary.ContainsKey(name))
                {
                    return;
                }
                textureLibrary.Add(name, texture);
            }


            public Texture2D LoadTexture(string name)
            {
                if (name == null)
                {
                    Debug.Log("WARNING: missing texture");
                    return null;
                }

                Texture2D texture;
                if (!textureLibrary.TryGetValue(name, out texture))
                {
                    string path = FlyByController.instance.config.assetPath + name;
                    texture = LoadImage(path);
                    if (texture == null)
                    {
                        Debug.Log("WARNING: could not find texture: " + path);
                    }
                    else
                    {
                        texture.name = name;
                        textureLibrary.Add(name, texture);
                    }
                }
                return texture;

            }

            public static Texture2D LoadImage(string filePath)
            {
                Texture2D tex = null;
                byte[] fileData;
                if (File.Exists(filePath))
                {
                    fileData = File.ReadAllBytes(filePath);
                    tex = new Texture2D(2, 2);
                    tex.LoadImage(fileData);
                }
                return tex;
            }

            public Media PreloadTextureVariants(Media media)
            {
                //this might eventually need to be a IEnumerator. 
                if (media.numVariants > 1)
                {
                    List<Texture2D> textures = new List<Texture2D>();
                    string name = System.IO.Path.GetFileNameWithoutExtension(media.path);
                    string extension = System.IO.Path.GetExtension(media.path);
                    string path = System.IO.Path.GetDirectoryName(media.path);
                    for (int i = 0; i < media.numVariants; i++)
                    {
                        string fullTexturePath = path + "/" + name + i + extension;
                        textures.Add(this.LoadTexture(fullTexturePath));
                        media.loadedTextures = textures;
                    }
                    media.loadedTexture = media.loadedTextures[0];
                }
                else
                {
                    media.loadedTexture = this.LoadTexture(media.path);
                }
                return media;
            }


            public static VideoClip LoadVideoClip(string filePath)
            {
                VideoClip clip = null;
                byte[] fileData;
                if (File.Exists(filePath))
                {
                    fileData = File.ReadAllBytes(filePath);
                }
                //videoclip has no constructor , investigating how to store video. 
                return clip;
            }

            void unloadTextures()
            {
                //on song changed. 
                foreach (var textureRef in textureLibrary)
                {
                    Texture2D texture = textureRef.Value;
                    UnityEngine.Object.Destroy(texture);
                }
                textureLibrary.Clear();
            }

            public GameObject loadPrefab(string asset, string bundle)
            {
                //placeholder <-- what is a placeholder?
                config = Config.instance;
                GameObject foundPrefab = loadAssetFromResources(bundle, asset);
                if (foundPrefab == null)
                {
                    foundPrefab = LoadAssetFromBundles(bundle, asset);
                }
                if (foundPrefab == null)
                {
                    if (this.config != null && !this.config.testing)
                    {
                        Debug.Log("bundle: " + bundle + ":" + asset + " reverting to defaultPrefab");
                    }
                    return this.defaultPrefab;
                }
                return foundPrefab;
            }

            public GameObject LoadAssetFromBundles(string bundleName, string assetName)
            {
                if (string.IsNullOrEmpty(bundleName) || string.IsNullOrEmpty(assetName))
                {
                    return null;
                }
                if (Config.instance == null)
                {
                    return null;
                }
                string bundlePath = Config.instance.assetPath + "Bundles/" + bundleName;
                GameObject foundAsset = null;
                AssetBundle bundle = LoadBundle(bundlePath);
                if (bundle != null)
                {
                    try
                    {
                        foundAsset = bundle.LoadAsset(assetName) as GameObject;
                    }
                    catch
                    {
                        Debug.Log("WARNING: could not find " + assetName + " in " + bundlePath);
                    }
                    if (foundAsset != null)
                    {
                        return foundAsset;
                    }
                    else
                    {
                        Debug.Log("WARNING: could not find " + assetName + " in " + bundle.name);
                    }
                }
                return foundAsset;
            }

            private AssetBundle LoadBundle(string bundlePath)
            {
                AssetBundle bundle = null;
                if (!this.bundleLibrary.TryGetValue(bundlePath, out bundle))
                {
                    try
                    {
                        bundle = AssetBundle.LoadFromFile(bundlePath);
                        if (bundle != null)
                        {
                            this.bundleLibrary.Add(bundlePath, bundle);
                        }

                    }
                    catch
                    {
                        Debug.Log("Warning: could not find bundle " + bundlePath);
                        return null;
                    }
                }
                return bundle;
            }

            public GameObject loadAssetFromResources(string bundlePath, string assetPath)
            {
                string name;

                if (string.IsNullOrEmpty(assetPath))
                {
                    return null;
                }

                if (string.IsNullOrEmpty(bundlePath))
                {
                    bundlePath = defaultBundlePath;
                }

                name = bundlePath + "/" + assetPath;

                GameObject cachedPrefab;
                GameObject foundObject = null;
                if (!prefabCache.TryGetValue(name, out cachedPrefab))
                {
                    foundObject = Resources.Load(name, typeof(GameObject)) as GameObject;
                    if (foundObject != null)
                    {
                        prefabCache.Add(name, foundObject);
                    }
                    else
                    {
                        //Don't think we need an error her since we are moving through different methods of loading.
                        //Debug.Log("something went wrong loading " + name + " from Resources folder");
                    }
                }
                else
                {
                    foundObject = prefabCache[name];
                }
                return foundObject;
            }
        }
    }
}