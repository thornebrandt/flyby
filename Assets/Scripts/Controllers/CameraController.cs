using UnityEngine;
using System.Collections.Generic;
using flyby.Core;
using flyby.Active;
using flyby.Triggers;
using DG.Tweening;


namespace flyby
{
    namespace Controllers
    {
        public class CameraController : MonoBehaviour
        {
            [HideInInspector]
            public static CameraController instance { get; private set; } //makes singleton
            public Camera cam; //reference to which cam is being used. 
            private FlyByController flyByController;
            public GameObject camObj;
            private GameObject camTarget;  //cam will always be looking at this target. used to keep track of y rotation. 
            private GameObject camPivot; //used to keep track of x rotation. 
            private GameObject camOffset; //direct parent of cam. 
            private GameObject camGO; //actual camera object. used for positioning;
            private Camera unityCamera; //unity camera
            private Camera spoutCamera; //camera that just passes. 
            bool usingSpout;
            public List<CameraAngle> cameraAngles;
            public Obj cameraO; //for temporary triggers between angles - like knobRotation and punch.  
            public CameraAngle currentCameraAngle;
            private int currentCameraAngleIndex = 0;
            private Sequence _sequence; //general sequence. 
            private float _lerpValue;
            Matrix4x4 originalMatrix;
            Matrix4x4 orthoMatrix;
            Matrix4x4 perspectiveMatrix;
            Matrix4x4 targetMatrix; //becomes one of the above. 
            Matrix4x4 currentMatrix; //for transitioning between ortho and perspective. 
            private float aspectRatio;
            private bool _animating;

            private Sequence rotateSequence;
            private float rotateLerpValue;
            private List<ActiveTrigger> rotateTriggers;
            //private Vector3 knobRotate;  //TODO - add knobRotate! 
            private Vector3 currentRotate;
            private Vector3 targetRotate;
            private bool rotating; //specifically for constant rotation. 

            private Sequence positionSequence;
            private float positionLerpValue;
            private List<ActiveTrigger> positionTriggers;
            private Vector3 currentPosition;
            private Vector3 targetPosition;
            private bool moving; //for punch/constant speeds. 

            private float rotationLerpValue;
            private float currentOrthographicSize;

            //alwaysFront orthographic for sprites / videos 
            private float alwaysFrontStartZ = 100;
            private float alwaysFrontIncrement = 0.01f;
            public float alwaysFrontCurrentZ;
            public float alwaysBackCurrentZ;

            void Awake()
            {
                if (this.enabled)
                {
                    setupInstance();
                    this.usingSpout = checkUsingSpout();
                    this.assignCamera(); //needed for switching - but also breaks tests.
                }

                if (this.cam != null)
                {
                    this.originalMatrix = this.cam.projectionMatrix;
                }
            }

            void OnDisable()
            {
                if (this.cam != null)
                {
                    this.cam.projectionMatrix = originalMatrix;
                }
            }

            public void setupInstance()
            {
                //makes cameraController into a singleton.
                if (instance == null)
                {
                    instance = this;
                }
            }

            private void Update()
            {
                if (this._animating)
                {
                    if (this.currentCameraAngle.orthographicSize != this.cam.orthographicSize)
                    {
                        this.cam.orthographicSize = Mathf.Lerp(
                            this.currentOrthographicSize,
                            this.currentCameraAngle.orthographicSize,
                            _lerpValue
                        );
                    }
                    //this.targetMatrix = this.currentCameraAngle.orthographic ? orthoMatrix : perspectiveMatrix;
                    if (this.targetMatrix != this.cam.projectionMatrix)
                    {
                        this.cam.projectionMatrix = Tools.Math.MatrixLerp(currentMatrix, targetMatrix, _lerpValue);
                    }
                }
                if (this.rotating)
                {
                    Vector3 animatedRotate = Vector3.Lerp(currentRotate, targetRotate, rotateLerpValue);
                    rotateCamera(animatedRotate);
                }
                if (this.moving)
                {
                    Vector3 animatedPosition = Vector3.Lerp(currentPosition, targetPosition, positionLerpValue);
                    positionCamera(animatedPosition);
                }
            }

            private void rotateCamera(Vector3 animatedRotate)
            {
                Vector3 pivotAngle = new Vector3(animatedRotate.x, 0, 0);
                Vector3 camTargetAngle = new Vector3(0, animatedRotate.y, animatedRotate.z);
                this.camPivot.transform.Rotate(pivotAngle * Time.deltaTime * 180f, Space.Self);
                this.camTarget.transform.Rotate(camTargetAngle * Time.deltaTime * 180f, Space.Self);
            }

            private void positionCamera(Vector3 animatedPosition)
            {
                this.camGO.transform.localPosition = animatedPosition;
            }

            private void triggerRotateCamera(TriggerObj triggerObj, ActiveTrigger a)
            {
                if (a.target != null)
                {
                    this.targetRotate = a.target.getVector3();
                }
                if (rotateSequence != null)
                {
                    rotateSequence.Kill();
                }

                if (_sequence != null)
                {
                    _sequence.Kill();
                    // need to break this up or set IDS of tweens for rotation and only kill those.  
                }

                rotateSequence = DOTween.Sequence();
                rotateSequence.Append(
                    DOTween.To(() => rotateLerpValue, x => rotateLerpValue = x, 1.0f, a.onTime)
                    .SetDelay(a.onDelay)
                    .OnStart(() =>
                    {
                        this.rotating = true;
                    })
                    .OnComplete(() =>
                    {
                        Update();
                    })
                );
            }


            private void triggerPositionCamera(TriggerObj triggerObj, ActiveTrigger a)
            {
                if (a.target != null)
                {
                    this.targetPosition = a.target.getVector3();
                    // if (a.target.relative) {
                    //     this.targetPosition = this.targetPosition + cam.transform.localPosition;
                    // }
                }
                if (positionSequence != null)
                {
                    positionSequence.Kill();
                }
                positionSequence = DOTween.Sequence();
                positionSequence.Append(
                    DOTween.To(() => positionLerpValue, x => positionLerpValue = x, 1.0f, a.onTime)
                    .SetDelay(a.onDelay)
                    .OnStart(() =>
                    {
                        this.moving = true;
                    })
                    .SetEase(a.onEase)
                    .OnComplete(() =>
                    {
                        Update();
                    })
                );
                if (!triggerObj.sustain && a.offTime >= 0)
                {
                    TriggerOffPositionSequence(a);
                }

            }

            private void TriggerOffPositionSequence(ActiveTrigger a)
            {
                positionSequence.Append(
                    DOTween.To(() => positionLerpValue, x => positionLerpValue = x, 0.0f, a.offTime)
                    .SetDelay(a.offDelay)
                    .SetEase(a.offEase)
                    .OnComplete(() =>
                    {
                        Update();
                        this.moving = false;
                    })
                );
            }

            private void handleRotate(TriggerObj t)
            {
                //this is distinct from the rotate offset of a camera... ( should it be ? ) 
                if (rotateTriggers != null)
                {
                    foreach (ActiveTrigger rotateA in rotateTriggers)
                    {
                        if (t.eventName == rotateA.trigger)
                        {
                            triggerRotateCamera(t, rotateA);
                        }
                    }
                }
            }

            private void handlePosition(TriggerObj t)
            {
                //this is the distinct from the angle offset of a camera... ( should it be? ) 
                if (positionTriggers != null)
                {
                    foreach (ActiveTrigger positionA in positionTriggers)
                    {
                        if (t.eventName == positionA.trigger)
                        {
                            triggerPositionCamera(t, positionA);
                        }
                    }
                }
            }

            private void handleSpoutToggle(TriggerObj t)
            {
                if (t.eventName == "toggle_spout")
                {
                    usingSpout = !usingSpout;
                    switchToSpoutCamera(usingSpout);
                }
            }

            private void switchToSpoutCamera(bool isSpout)
            {
                unityCamera.enabled = !isSpout;
                spoutCamera.enabled = isSpout;
                this.cam = isSpout ? spoutCamera : unityCamera;
            }

            private Matrix4x4 setPerspectiveMatrix(float _fov, float _near, float _far)
            {
                return Matrix4x4.Perspective(_fov, this.aspectRatio, _near, _far);
            }

            private Matrix4x4 setOrthographicMatrix(float _orthoSize, float _near, float _far)
            {
                return Matrix4x4.Ortho(-_orthoSize * this.aspectRatio, _orthoSize * aspectRatio, -_orthoSize, _orthoSize, _near, _far);
            }

            private bool checkUsingSpout()
            {
                if (this.flyByController != null && flyByController.config != null)
                {
                    return this.flyByController.config.spout;
                }

                if (Config.instance != null)
                {
                    return Config.instance.spout;
                }

                return false;
            }

            private void setResolution()
            {
                if (Config.instance != null)
                {

                    Screen.SetResolution(
                        Config.instance.screenWidth,
                        Config.instance.screenHeight,
                        Config.instance.fullScreen
                    );

                    if (spoutCamera != null && spoutCamera.targetTexture != null)
                    {
                        spoutCamera.targetTexture.Release();
                        spoutCamera.targetTexture.width = Config.instance.screenWidth;
                        spoutCamera.targetTexture.height = Config.instance.screenHeight;
                        spoutCamera.targetTexture.Create();
                    }
                }
            }

            public void setup(List<CameraAngle> unityCameraAngles, Obj unityCameraO)
            {
                //TODO - figure out why this is called 1600 time in the flyby test suite. 
                this.flyByController = getFlyByController();
                this.usingSpout = checkUsingSpout();
                this.assignCamera();
                this.setResolution();
                Debug.Log("setting up camera: using spout: " + usingSpout + " this.cam: " + this.cam.gameObject.name);
                switchToSpoutCamera(this.usingSpout);
                if (unityCameraAngles != null)
                {
                    unityCameraAngles.RemoveAll((c) => c.disabled); //removing disabled cameraAngles
                }
                this.cameraAngles = unityCameraAngles;
                float tempOrthoSize = 5;
                float tempFOV = 60;
                float tempNear = 0.1f;
                float tempFar = 1000f;
                this.aspectRatio = (float)Screen.width / (float)Screen.height; //important
                this.orthoMatrix = setOrthographicMatrix(tempOrthoSize, tempNear, tempFar);
                this.perspectiveMatrix = setPerspectiveMatrix(tempFOV, tempNear, tempFar);
                this.cameraO = unityCameraO;
                if (unityCameraAngles == null || unityCameraAngles.Count == 0)
                {
                    this.cameraAngles = new List<CameraAngle>{
                        new CameraAngle()
                    };
                }
                if (unityCameraO == null)
                {
                    this.cameraO = new Obj();
                }
                this.currentCameraAngle = this.cameraAngles[0];
                this.cutToCameraAngle(this.currentCameraAngle);
                setupCameraTriggers(cameraO);
            }


            private FlyByController getFlyByController()
            {
                //TODO - move this to base class. 

                if (this.flyByController != null)
                {
                    return flyByController;
                }
                if (FlyByController.instance != null)
                {
                    return FlyByController.instance;
                }
                return null;
            }

            public void setupCameraTriggers(Obj cameraO)
            {
                this.cameraO = cameraO;
                if (cameraO != null)
                {
                    if (cameraO.triggerRotate != null)
                    {
                        rotateTriggers = assignSecondaryTriggers(cameraO.triggerRotate);
                    }
                    if (cameraO.triggerPosition != null)
                    {
                        positionTriggers = assignSecondaryTriggers(cameraO.triggerPosition);

                    }
                }
            }

            private List<ActiveTrigger> assignSecondaryTriggers(ActiveTrigger _a)
            {
                List<ActiveTrigger> triggers = new List<ActiveTrigger>();
                ActiveTrigger a = new ActiveTrigger(_a);
                triggers.Add(a);
                a._i = 0;
                int i = 1;
                if (a.triggers != null)
                {
                    foreach (SecondaryActiveTrigger sat in a.triggers)
                    {
                        ActiveTrigger at = new ActiveTrigger(sat);
                        at._i = i;
                        triggers.Add(at);
                        i++;
                    }
                }
                return triggers;
            }


            public void assignCamera()
            {
                if (this.camObj == null)
                {
                    if (FlyByController.instance != null && FlyByController.instance.camObj != null)
                    {
                        this.camObj = FlyByController.instance.camObj;
                    }
                }

                if (this.camObj != null)
                {
                    this.camTarget = this.camObj.transform.GetChild(0).gameObject;
                    this.camPivot = this.camTarget.transform.GetChild(0).gameObject;
                    this.camOffset = this.camPivot.transform.GetChild(0).gameObject;
                    this.camGO = this.camOffset.transform.GetChild(0).gameObject;
                    this.unityCamera = this.camGO.GetComponent<Camera>();
                    this.cam = this.usingSpout ? this.spoutCamera : this.unityCamera;
                    this.spoutCamera = this.camGO.transform.GetChild(0).GetComponent<Camera>();
                }
            }

            public GameObject getCam()
            {
                if (this.camGO == null)
                {
                    this.assignCamera();
                    if (this.camGO == null)
                    {
                        Debug.Log("Something went wrong getting cam.");
                        return null;
                    }
                }
                return this.camGO;
            }

            private void resetAnimations()
            {
                if (this.cam != null)
                {
                    this.currentOrthographicSize = this.cam.orthographicSize;
                    this.currentMatrix = this.cam.projectionMatrix;
                }
                this.currentRotate = Vector3.zero;
                //this.currentPosition = this.currentCameraAngle.offset.getVector3();
                this.currentPosition = Vector3.zero;
                this.rotating = false;
                this.moving = false;
                _lerpValue = 0;
            }

            public void reset()
            {
                this.camGO.transform.position = Vector3.zero;
                this.camGO.transform.rotation = Quaternion.identity;
            }

            public void transitionToCameraAngle(CameraAngle c)
            {
                // this is a legacy camera angle system.
                // these are meant to override constant animations like rotate, speed, etc.

                // cam is the actual cam obj. ) feel free to add another layer at some point. ) 
                // hierarchy is: 
                // scene -> camObj -> camParent -> camPivot -> cam

                CameraAngle _nextCameraAngle = c;
                setParent(c);
                Vector3 origin = c.offset.getVector3();
                Vector3 targetOffset = c.offset.getVector3();
                Vector3 cameraRotation = c.rotation.getVector3();
                Vector3 pivotAngle = new Vector3(cameraRotation.x, 0, cameraRotation.z);
                Vector3 camTargetAngle = new Vector3(0, cameraRotation.y, 0);

                if (_sequence != null)
                {
                    _sequence.Kill();
                }

                resetAnimations();
                //TODO - put an OR here. 
                if (c.animationTime > 0)
                {

                    _sequence = DOTween.Sequence();
                    _sequence.Append(
                        this.camOffset.transform.DOLocalMove(c.offset.getVector3(), c.animationTime)
                    )

                    .OnStart(() =>
                    {
                        this._animating = true;
                        currentRotate = Vector3.zero;
                        this.targetMatrix = c.orthographic ? setOrthographicMatrix(c.orthographicSize, c.near, c.far) : setPerspectiveMatrix(c.fov, c.near, c.far);
                    })
                    .OnComplete(() =>
                    {
                        this._animating = false;
                        this.cam.usePhysicalProperties = c.physical;
                        this.perspectiveMatrix = setPerspectiveMatrix(c.fov, c.near, c.far);
                        this.orthoMatrix = setOrthographicMatrix(c.orthographicSize, c.near, c.far);
                        this.cam.orthographicSize = c.orthographicSize;
                        this.cam.projectionMatrix = c.orthographic ? orthoMatrix : perspectiveMatrix;
                        this.cam.farClipPlane = c.far;
                        this.cam.nearClipPlane = c.near;
                    })
                    ;
                    _sequence.Insert(0,
                        this.camObj.transform.DOLocalMove(c.origin.getVector3(), c.animationTime)
                    );
                    _sequence.Insert(0,
                        this.camPivot.transform.DOLocalRotate(pivotAngle, c.animationTime)
                    );
                    _sequence.Insert(0,
                        this.camTarget.transform.DOLocalRotate(camTargetAngle, c.animationTime)
                    );
                    //this one stabalizes rotation in between parent shifts. 
                    _sequence.Insert(0,
                        this.camObj.transform.DOLocalRotate(Vector3.zero, c.animationTime)
                    );
                    //misc. 
                    _sequence.Insert(0,
                        DOTween.To(() => _lerpValue, x => _lerpValue = x, 1.0f, c.animationTime)
                    );


                }
                else
                {
                    cutToCameraAngle(c);
                }
            }

            public void cutToCameraAngle(CameraAngle c)
            {
                resetAnimations();
                setParent(c);
                Vector3 cameraRotation = c.rotation.getVector3();
                Vector3 pivotAngle = new Vector3(cameraRotation.x, 0, cameraRotation.z);
                Vector3 camTargetAngle = new Vector3(0, cameraRotation.y, 0);
                this.camGO.transform.localPosition = Vector3.zero;
                this.camOffset.transform.localPosition = c.offset.getVector3();
                this.camObj.transform.localPosition = c.origin.getVector3();
                this.camObj.transform.localRotation = Quaternion.identity; //should not ever change
                this.camPivot.transform.localRotation = Quaternion.Euler(pivotAngle);
                this.camTarget.transform.localRotation = Quaternion.Euler(camTargetAngle);
                this.perspectiveMatrix = setPerspectiveMatrix(c.fov, c.near, c.far);
                this.orthoMatrix = setOrthographicMatrix(c.orthographicSize, c.near, c.far);
                this.cam.orthographicSize = c.orthographicSize;
                this.cam.projectionMatrix = c.orthographic ? orthoMatrix : perspectiveMatrix;
                this.cam.farClipPlane = c.far;
                this.cam.nearClipPlane = c.near;
                //experimental
                this.cam.usePhysicalProperties = c.physical;
                if (c.physical)
                {
                    Debug.Log("setting field of view to : " + c.fov);
                    this.cam.fieldOfView = c.fov;
                    Debug.Log(this.cam.fieldOfView);
                }
                //experimental. 
                resetAlwaysFrontZ(); //experimental. 
                resetAlwaysBackZ();
            }

            private void resetAlwaysFrontZ()
            {
                this.alwaysFrontCurrentZ = this.alwaysFrontStartZ;
            }

            private void resetAlwaysBackZ()
            {
                //using the same as alwaysFront. 
                this.alwaysBackCurrentZ = this.alwaysFrontStartZ;
            }

            public float getNextAlwaysFrontZ()
            {
                this.alwaysFrontCurrentZ = this.alwaysFrontCurrentZ - this.alwaysFrontIncrement;
                return this.alwaysFrontCurrentZ;
            }

            public float getNextAlwaysBackZ()
            {
                this.alwaysBackCurrentZ = this.alwaysBackCurrentZ + this.alwaysFrontIncrement;
                return this.alwaysBackCurrentZ;
            }

            private void setParent(CameraAngle c)
            {
                //this function uses prefabs during testing.  
                if (Application.isPlaying)
                {
                    if (string.IsNullOrEmpty(c.parent))
                    {
                        if (Config.instance != null && !Config.instance.testing)
                        {
                            this.camObj.transform.parent = null;
                        }
                        else
                        {
                            Debug.Log("not setting camObj in test");
                        }
                    }
                    else
                    {
                        this.camObj.transform.parent = getParent(c.parent);
                    }
                }
            }

            private Transform getParent(string parentName)
            {
                if (this.flyByController != null)
                {
                    GameObject instance = this.flyByController.findActiveInstanceById(parentName);
                    if (instance != null)
                    {
                        return Obj.getPrefabInstance(instance).transform;
                    }
                    else
                    {
                        Debug.Log("cameraAngle: could not find parent " + parentName + " in " + this.flyByController.currentPart);
                    }
                }
                return null;
            }

            public void eventHandler(TriggerObj t)
            {
                string eventName = t.eventName;
                handleRotate(t);
                handlePosition(t);
                handleSpoutToggle(t);
                for (int i = 0; i < cameraAngles.Count; i++)
                {
                    CameraAngle c = cameraAngles[i];
                    if (c.trigger == eventName)
                    {
                        currentCameraAngleIndex = i;
                        currentCameraAngle = c;
                        transitionToCameraAngle(currentCameraAngle);
                    }
                }
                if (currentCameraAngle.nextTrigger == eventName)
                {
                    currentCameraAngleIndex = currentCameraAngleIndex + 1;
                    currentCameraAngleIndex = (int)Mathf.Repeat(currentCameraAngleIndex, cameraAngles.Count);
                    currentCameraAngle = this.cameraAngles[currentCameraAngleIndex];
                    transitionToCameraAngle(currentCameraAngle);
                }
                if (currentCameraAngle.prevTrigger == eventName)
                {
                    currentCameraAngleIndex = currentCameraAngleIndex - 1;
                    currentCameraAngleIndex = (int)Mathf.Repeat(currentCameraAngleIndex, cameraAngles.Count);
                    currentCameraAngle = this.cameraAngles[currentCameraAngleIndex];
                    transitionToCameraAngle(currentCameraAngle);
                }
            }

            public void kill()
            {
                this.cameraAngles = null;
                killSequences();
                rotating = false;
                moving = false;
                currentCameraAngleIndex = 0;
            }

            private void killSequences()
            {
                if (_sequence != null)
                {
                    _sequence.Kill();
                }
                if (rotateSequence != null)
                {
                    rotateSequence.Kill();
                }
                if (positionSequence != null)
                {
                    positionSequence.Kill();
                }
            }
        }
    }
}