using System.Collections.Generic;
using flyby.Active;
using flyby.Core;
using flyby.Sets;
using flyby.Triggers;
using UnityEngine;
using System;
using System.Reflection;

namespace flyby
{
    namespace Controllers
    {
        public class ActiveBehaviourController : MonoBehaviour
        {
            [HideInInspector]
            public static ActiveBehaviourController instance { get; private set; } //makes singleton
            [HideInInspector]
            public ActiveBehaviour[] activeBehaviours; //variable list of active behaviours
            public Dictionary<string, ActiveTrigger> presets; //if any activeTrigger has an id, put it here.  
            private Dictionary<string, ActiveBehaviour> abs;
            private Dictionary<string, ActiveTrigger> ats;
            private Dictionary<string, string> abs_instances;

            void Awake()
            {
                if (this.enabled)
                {
                    setup();
                }
            }

            public void setup()
            {
                if (instance == null)
                {
                    instance = this;
                }
                this.setupHierarchyDictionary();
                this.setupActiveTriggerPresets();
            }

            public List<ActiveTrigger> setupActiveTriggerList(Obj o, List<ActiveBehaviour> activeBehaviours)
            {
                //we're taking the list of custom behaviours from one of the instances of the obj.
                List<ActiveTrigger> activeTriggerList = new List<ActiveTrigger>();
                foreach (ActiveBehaviour activeBehaviour in activeBehaviours)
                {
                    if (activeBehaviour.triggers != null && activeBehaviour.triggers.Count > 0)
                    {
                        if (activeBehaviour.triggers[0].activeType != o.activeType)
                        {
                            activeTriggerList.AddRange(activeBehaviour.triggers);
                        }
                    }
                }

                return activeTriggerList;
            }

            private void setupActiveTriggerPresets()
            {
                this.presets = new Dictionary<string, ActiveTrigger>();
            }

            public void addToActiveTriggerPresets(ActiveTrigger at)
            {
                this.presets[at.id] = at;
            }

            public ActiveTrigger findActiveTriggerPreset(string preset)
            {
                ActiveTrigger at;
                //debugPresets(this.presets);
                if (this.presets.TryGetValue(preset, out at))
                {
                    return new ActiveTrigger(at);
                }
                return null;
            }

            private void debugPresets(Dictionary<string, ActiveTrigger> presets)
            {
                foreach (KeyValuePair<string, ActiveTrigger> preset in presets)
                {
                    Debug.Log("preset: " + preset.Key);
                }
            }

            private void setupHierarchyDictionary()
            {
                if (this.abs == null)
                {
                    GameObject nullObject = new GameObject("cachedPreset");
                    this.abs = new Dictionary<string, ActiveBehaviour>();
                    this.abs.Add("triggerSprite", nullObject.AddComponent<ActiveSprite>());
                    this.abs.Add("triggerColor", nullObject.AddComponent<ActiveColor>());
                    this.abs.Add("triggerPosition", nullObject.AddComponent<ActivePosition>());
                    this.abs.Add("triggerPivot", nullObject.AddComponent<ActivePosition>()); //might cause some conflicts. 
                    this.abs.Add("triggerSpeed", nullObject.AddComponent<ActiveSpeed>());
                    this.abs.Add("triggerRotation", nullObject.AddComponent<ActiveRotation>());
                    this.abs.Add("triggerRotate", nullObject.AddComponent<ActiveRotate>());
                    this.abs.Add("triggerScale", nullObject.AddComponent<ActiveScale>());
                    this.abs.Add("triggerParent", nullObject.AddComponent<ActiveParent>());
                }

                if (this.abs_instances == null)
                {
                    //this is a dictionary of hierarchies. 
                    this.abs_instances = new Dictionary<string, string>();
                    this.abs_instances.Add("triggerParent", "instance");
                    this.abs_instances.Add("triggerSpeed", "instance");
                    this.abs_instances.Add("triggerSprite", "prefab");
                    this.abs_instances.Add("triggerColor", "prefab");
                    this.abs_instances.Add("triggerPosition", "popup");
                    this.abs_instances.Add("triggerPivot", "prefab");
                    this.abs_instances.Add("triggerRotation", "rotateScale");
                    this.abs_instances.Add("triggerRotate", "rotateScale");
                    this.abs_instances.Add("triggerScale", "rotateScale");
                }
            }

            public GameObject getInstanceForBehaviour(GameObject instance, Obj o, string abName)
            {
                switch (o.hierarchyType)
                {
                    case HierarchyType.None:
                        return instance;
                    default:
                        //all others besides none. 
                        switch (abs_instances[abName])
                        {
                            case "instance":
                                return instance;
                            case "prefab":
                                return Obj.getPrefabInstance(instance);
                            case "popup":
                                return Obj.getPopupInstance(instance);
                            case "rotateScale":
                                return Obj.getRotateScaleInstance(instance);
                            default:
                                return instance;
                        }
                }
            }


            public List<ActiveBehaviour> setupInstanceBehaviours(Obj o, GameObject root, GameObject instance, int i)
            {
                List<ActiveBehaviour> instanceBehaviours = new List<ActiveBehaviour>();
                //custom behaviours ( all powered by activeTrigger ) 

                //experimental: just removed a conditional for hierarchyType.None

                instanceBehaviours.AddRange(instance.GetComponentsInChildren<ActiveBehaviour>());
                List<ActiveBehaviour> newBehaviours = new List<ActiveBehaviour>();

                foreach (ActiveBehaviour ab in instanceBehaviours)
                {
                    // behaviours that exist in the prefab
                    ab.instance = ab.gameObject;
                    ab.root = ab.gameObject;
                    if (o.activeTrigger != null)
                    {
                        ab.assignSecondaryTriggers(o.activeTrigger);
                        newBehaviours.AddRange(ab.assignChildBehaviours());
                    }
                }
                instanceBehaviours.AddRange(newBehaviours);

                foreach (var property in o.GetType().GetFields())
                {
                    foreach (KeyValuePair<string, ActiveBehaviour> ab in this.abs)
                    {
                        if (ab.Key == property.Name)
                        {
                            ActiveTrigger at = property.GetValue(o) as ActiveTrigger;
                            if (at != null && !at.disabled)
                            {
                                GameObject _instance = getInstanceForBehaviour(instance, o, ab.Key);
                                var behaviour = _instance.AddComponent(ab.Value.GetType()) as ActiveBehaviour;
                                behaviour.instance = _instance;
                                behaviour.root = root;
                                behaviour._i = i;
                                behaviour.o = o;
                                behaviour.assignSecondaryTriggers(at);
                                if (at.childIndex < 0)
                                {
                                    instanceBehaviours.Add(behaviour);
                                }
                                instanceBehaviours.AddRange(behaviour.assignChildBehaviours());
                            }
                        }
                    }
                }
                setupActiveTriggers(o, instanceBehaviours, i);
                return instanceBehaviours;
            }

            public GameObject assignInstance(ActiveTrigger at, GameObject instance, GameObject root)
            {
                if (root == null)
                {
                    return instance;
                }

                if (at.childIndex > -1 && root.transform.childCount > at.childIndex)
                {
                    return root.transform.GetChild(at.childIndex).gameObject;
                }
                else
                {
                    return instance;
                }
            }

            private void setupActiveTriggers(Obj o, List<ActiveBehaviour> activeBehaviours, int i)
            {
                foreach (ActiveBehaviour ab in activeBehaviours)
                {
                    // virtual function used by inherited custom behaviours.
                    // TODO: need to design a way to turn off for transforms. 
                    ab._i = i;
                    ab.setupActiveTriggers(o);
                }
            }

        }
    }
}