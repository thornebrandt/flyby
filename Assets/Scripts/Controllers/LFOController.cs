using System.Collections.Generic;
using flyby.Core;
using flyby.Sets;
using flyby.Triggers;
using UnityEngine;

namespace flyby
{
    namespace Controllers
    {
        public class LFOController : TriggerSourceController
        {
            //attached to parts. 
            private List<LFO> lfos;
            private List<LFO> usedLFOS;
            private List<Trigger> triggers;
            public List<Trigger> LFOTriggers; //public for debug. 
            private TriggerObj t; //reusable triggerObj. 

            public override void setup(List<Trigger> triggers)
            {
                this.triggers = triggers;
                this.t = new TriggerObj();
                t.created = Time.time;
                t.source = "lfo";
                t.knob = 0;
            }

            public void Update()
            {
                this.sendLFOTrigers(this.lfos);
            }

            private void sendLFOTrigers(List<LFO> lfos)
            {
                if (lfos != null)
                {
                    foreach (LFO lfo in lfos)
                    {
                        //other values are set on setup. 
                        t.eventName = lfo.trigger;
                        t.value = lfo.GetValue(Time.time);
                        t.created = Time.time;
                        this.trigger(t);
                        this.cacheTriggered(t);
                    }
                }
            }

            private void prepareLFOSFromPart(List<LFO> partLFOS)
            {
                this.lfos = new List<LFO>();
                if (partLFOS == null)
                {
                    return;
                }
                foreach (LFO lfo in partLFOS)
                {
                    this.lfos.Add(lfo);
                }
            }

            public void PartChangedHandler(Part part)
            {
                prepareLFOSFromPart(part.lfos);
                Debug.Log("we have these lfos: " + this.lfos.Count);
            }
        }
    }
}