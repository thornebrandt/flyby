namespace flyby {
    namespace Controllers {
        public class PrivateVars {
            public string twitch_auth_code; //private auth code. 
            public string twitch_channel; //private channel string.
            public string twitch_username; //usually same as channel, unless you are using a bot. 
            public string socket_server; //
        }
    }
}