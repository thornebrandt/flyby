using System.Collections.Generic;
using flyby.Triggers;
using flyby.Controllers;
using Lasp;
using UnityEngine;

namespace flyby
{
    namespace Controllers
    {
        public class AudioController : TriggerSourceController
        {
            private bool isSetup;
            public List<Trigger> triggers;
            public AudioLevelTracker audioLevelTracker;
            public AudioLevelTracker bassLevelTracker;
            public AudioLevelTracker midLevelTracker;
            public AudioLevelTracker trebleLevelTracker;

            private int deviceI;
            private IEnumerable<DeviceDescriptor> deviceIDs;

            private float audioLevel;
            private float bassAudioLevel;
            private float midAudioLevel;
            private float trebleAudioLevel;

            private float lastAudioLevel;
            private float lastBassAudioLevel;
            private float lastBassHitTime;
            private float lastMidAudioLevel;
            private float lastMidHitTime;
            private float lastTrebleAudioLevel;
            private float lastTrebleHitTime;

            //to control during performance. 
            private bool audioEnabled = true;
            private float audioWeight = 1;
            //floors are meant to override high volumes ( with lower audio weight. ) 
            private float audioFloor; //min audio value - use as override.
            private float audioBassFloor; //min bass value - use as override 
            private float audioMidFloor; //min bass value. - use as override. 
            private float audioTrebleFloor; //min treble value. - use as override


            private TriggerObj t;
            private TriggerObj audioTriggerObj;
            private TriggerObj bassAudioTriggerObj;
            private TriggerObj midAudioTriggerObj;
            private TriggerObj trebleAudioTriggerObj;
            private TriggerObj audioHitTriggerObj; //experimental - a NOTE ON for impacts. 

            private float lastTimeHandled;



            private int deviceLength;

            public float AudioLevel
            {
                get
                {
                    return audioLevel;
                }
                set
                {
                    if (audioEnabled)
                    {
                        audioLevel = value;
                        HandleAudio();
                    }
                }
            }

            public float BassAudioLevel
            {
                //set all on audio level?
                get
                {
                    return bassAudioLevel;
                }
                set
                {
                    bassAudioLevel = value;
                }
            }

            public float MidAudioLevel
            {
                //set all on audio level?
                get
                {
                    return midAudioLevel;
                }
                set
                {
                    midAudioLevel = value;
                }
            }

            public float TrebleAudioLevel
            {
                //set all on audio level?
                get
                {
                    return trebleAudioLevel;
                }
                set
                {
                    trebleAudioLevel = value;
                }
            }

            public override void setup(List<Trigger> triggers)
            {
                if (this.enabled && this.active)
                //TODO - add this to base class in wrapper function. 
                {
                    this.triggers = triggers;
                    this.deviceI = Config.instance.audioDevice;
                    this.deviceIDs = Lasp.AudioSystem.InputDevices;
                    int i = 0;
                    t = new TriggerObj(); //placeholder. 
                                          //caching trigger objs.
                    audioTriggerObj = new TriggerObj();
                    bassAudioTriggerObj = new TriggerObj();
                    midAudioTriggerObj = new TriggerObj();
                    trebleAudioTriggerObj = new TriggerObj();
                    audioHitTriggerObj = new TriggerObj();
                    if (!isSetup)
                    {
                        foreach (var device in deviceIDs)
                        {
                            if (i == deviceI)
                            {
                                if (Config.instance.debug)
                                {
                                    Debug.Log("Found and Assigning audio input to " + device.Name + " i: " + i);
                                }
                                assignDeviceID(device.ID);
                            }
                            else
                            {
                                if (Config.instance.debug)
                                {
                                    Debug.Log("Found AudioDevice: " + device.Name + " set audioDevice to " + i + " in config to use");
                                }
                            }

                            i++;
                        }
                    }
                    deviceLength = i + 1;
                    isSetup = true;
                }
            }

            private void assignDeviceID(string device_id)
            {
                audioLevelTracker.enabled = false;
                audioLevelTracker.deviceID = device_id;
                audioLevelTracker.enabled = true;
                bassLevelTracker.enabled = false;
                bassLevelTracker.deviceID = device_id;
                bassLevelTracker.enabled = true;
                midLevelTracker.enabled = false;
                midLevelTracker.deviceID = device_id;
                midLevelTracker.enabled = true;
                trebleLevelTracker.enabled = false;
                trebleLevelTracker.deviceID = device_id;
                trebleLevelTracker.enabled = true;
            }


            public void toggleAudio()
            {
                audioEnabled = !audioEnabled;
            }

            public void eventHandler(TriggerObj t)
            {
                // eventually handling things such as 
                // fallRate - frequencies, etc.

                switch (t.eventName)
                {
                    case "audio_weight":
                        this.audioWeight = t.value;
                        break;
                    case "audio_bass_floor":
                        this.audioBassFloor = t.value;
                        break;
                    case "audio_mid_floor":
                        this.audioMidFloor = t.value;
                        break;
                    case "audio_treble_floor":
                        this.audioTrebleFloor = t.value;
                        break;
                }
            }

            public void HandleAudio()
            {
                float hitThreshold = 0.45f;
                bool audioHit = false;
                bool bassAudioHit = false;
                bool midAudioHit = false;
                bool trebleAudioHit = false;



                if (audioLevel > lastAudioLevel + hitThreshold)
                {
                    //volume is raising a lot. 
                    audioHit = true;
                }

                if (audioLevel == 1 && lastAudioLevel < 0.98f)
                {
                    //it just started peaking. 
                    audioHit = true;
                }


                if (bassAudioLevel == 1 && lastBassAudioLevel < 0.94f)
                {
                    if (Time.time - lastBassHitTime > 0.18f)
                    {
                        bassAudioHit = true;
                    }
                    lastBassHitTime = Time.time;
                }

                if (bassAudioLevel > lastBassAudioLevel + hitThreshold)
                {
                    if (Time.time - lastBassHitTime > 0.18f)
                    {
                        bassAudioHit = true;
                    }
                    lastBassHitTime = Time.time;
                }

                if (midAudioLevel > lastMidAudioLevel + hitThreshold)
                {
                    if (Time.time - lastMidHitTime > 0.18f)
                    {
                        midAudioHit = true;
                    }
                    lastMidHitTime = Time.time;
                }

                if (trebleAudioLevel > lastTrebleAudioLevel + hitThreshold)
                {
                    if (Time.time - lastTrebleHitTime > 0.18f)
                    {
                        trebleAudioHit = true;
                    }
                    lastTrebleHitTime = Time.time;
                }


                foreach (Trigger trigger in triggers)
                {
                    bool audioTriggered = false;
                    bool audioHitTriggered = false;
                    t = audioTriggerObj;
                    switch (trigger.eventName)
                    {
                        case "audio":
                            audioTriggered = true;
                            t.value = audioLevel * audioWeight;
                            break;
                        case "audio_bass":
                            audioTriggered = true;
                            t = bassAudioTriggerObj;
                            t.value = (bassAudioLevel * audioWeight) + audioBassFloor;
                            break;
                        case "audio_mid":
                            audioTriggered = true;
                            t = midAudioTriggerObj;
                            t.value = (midAudioLevel * audioWeight) + audioMidFloor;
                            break;
                        case "audio_treble":
                            audioTriggered = true;
                            t = trebleAudioTriggerObj;
                            t.value = (trebleAudioLevel * audioWeight) + audioTrebleFloor;
                            break;
                        case "audio_hit":
                            if (audioHit)
                            {
                                audioHitTriggered = true;
                                t = audioHitTriggerObj;
                            }
                            break;
                        case "audio_bass_hit":
                            if (bassAudioHit)
                            {
                                audioHitTriggered = true;
                                t = audioHitTriggerObj;
                            }
                            break;
                        case "audio_mid_hit":
                            if (midAudioHit)
                            {
                                audioHitTriggered = true;
                                t = audioHitTriggerObj;
                            }
                            break;
                        case "audio_treble_hit":
                            if (trebleAudioHit)
                            {
                                audioHitTriggered = true;
                                t = audioHitTriggerObj;
                            }
                            break;
                        default:
                            break;
                    }

                    if (audioTriggered || audioHitTriggered)
                    {
                        t.trigger = trigger;
                        t.eventName = trigger.eventName;
                        t.created = Time.time;
                        t.source = audioTriggered ? "audio" : "audioHit";
                        this.trigger(t);
                        this.cacheTriggered(t);
                    }
                }

                //probably shouldn't be affected by audioWeight. 
                lastAudioLevel = audioLevel;
                lastBassAudioLevel = bassAudioLevel;
                lastMidAudioLevel = midAudioLevel;
                lastTrebleAudioLevel = trebleAudioLevel;
                lastTimeHandled = Time.time;
            }

            private void fadeOutAudioLevels()
            {
                float cacheLastTimeHandled = lastTimeHandled;
                float lerpTime = 0.01f;
                audioLevel = Mathf.Lerp(audioLevel, 0, lerpTime);
                bassAudioLevel = Mathf.Lerp(bassAudioLevel, 0, lerpTime);
                midAudioLevel = Mathf.Lerp(midAudioLevel, 0, lerpTime);
                trebleAudioLevel = Mathf.Lerp(trebleAudioLevel, 0, lerpTime);
                HandleAudio();
                lastTimeHandled = cacheLastTimeHandled; //because handleAudio resets it. 
            }

        }
    }
}
