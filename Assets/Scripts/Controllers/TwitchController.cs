using System;
using System.Collections.Generic;
using flyby.Triggers;
using System.Net.Sockets;
using System.IO;
using UnityEngine;

namespace flyby
{
    namespace Controllers
    {
        public class TwitchController : TriggerSourceController
        {
            public List<Trigger> twitchTriggers;
            private Config config;

            TcpClient Twitch;
            StreamReader Reader;
            StreamWriter Writer;

            float pingCounter;

            const string URL = "irc.chat.twitch.tv";
            const int PORT = 6667;

            public override void setup(List<Trigger> triggers)
            {
                this.twitchTriggers = filterTriggers(triggers);
                if (this.twitchTriggers.Count > 0)
                {
                    this.config = Config.instance;
                    if (config.vars != null &&
                        !string.IsNullOrEmpty(config.vars.twitch_auth_code) &&
                        !string.IsNullOrEmpty(config.vars.twitch_channel)
                    )
                    {
                        ConnectToTwitch();
                    }
                }
            }

            private List<Trigger> filterTriggers(List<Trigger> _triggers)
            {
                this.twitchTriggers = new List<Trigger>();
                foreach (Trigger trigger in _triggers)
                {
                    if (!string.IsNullOrEmpty(trigger.chat) || !string.IsNullOrEmpty(trigger.user))
                    {
                        this.twitchTriggers.Add(trigger);
                    }
                }
                return this.twitchTriggers;
            }

            void Update()
            {
                checkForTwitchUpdates();
            }

            private void checkForTwitchUpdates()
            {
                if (Twitch != null)
                {
                    pingCounter += Time.deltaTime;
                    if (pingCounter > 60)
                    {
                        Writer.WriteLine("PING " + URL);
                        Writer.Flush();
                        pingCounter = 0;
                    }

                    if (!Twitch.Connected)
                    {
                        ConnectToTwitch();
                    }

                    if (Twitch.Available > 0)
                    {
                        string message = Reader.ReadLine();
                        if (message.Contains("PRIVMSG"))
                        {
                            int splitPoint = message.IndexOf("!");
                            string chatter = message.Substring(1, splitPoint - 1);
                            splitPoint = message.IndexOf(":", 1);
                            string msg = message.Substring(splitPoint + 1);
                            onChat(chatter, msg);
                        }
                    }
                }
            }

            private void onChat(string chatter, string message)
            {
                foreach (Trigger trigger in this.twitchTriggers)
                {
                    if (message.Contains(trigger.chat))
                    {
                        TriggerObj triggerObj = new TriggerObj();
                        triggerObj.eventName = trigger.eventName;
                        triggerObj.source = "twitch";
                        triggerObj.message = message;
                        triggerObj.user = chatter;
                        //triggerObj.group = trigger.group;
                        this.trigger(triggerObj);
                    }
                }
                Debug.Log(chatter + " says " + message);
            }

            private void ConnectToTwitch()
            {
                try
                {
                    Twitch = new TcpClient(URL, PORT);
                    Reader = new StreamReader(Twitch.GetStream());
                    Writer = new StreamWriter(Twitch.GetStream());
                    Writer.WriteLine("PASS " + config.vars.twitch_auth_code);
                    Writer.WriteLine("NICK " + config.vars.twitch_username.ToLower());
                    Writer.WriteLine("JOIN #" + config.vars.twitch_channel.ToLower());
                    Writer.Flush();
                }
                catch (Exception e)
                {
                    Debug.Log("TWITCH: " + e.Message);
                }
            }




        }
    }
}