
using System.Collections;
using System.Collections.Generic;
using flyby.Triggers;
using MidiJack;
using UnityEngine;
using System.Runtime.InteropServices;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace flyby
{
    namespace Controllers
    {

        public class MidiController : TriggerSourceController
        {
            public int midiOutDeviceIndex = 1; //this is overriden by config.
            private uint midiEndPoint;

            [HideInInspector]
            public static MidiController instance { get; private set; } //makes singleton
            public List<Trigger> triggers;
            private MidiChannel[] channels;
            [HideInInspector]
            private Dictionary<MidiChannel, int> channelIndexes;

            //primarily used for debug messages. 
            public delegate void MidiCC(int channel, int knob, float value);
            public static event MidiCC OnMidiCC;
            public delegate void MidiNote(int channel, int note, float velocity);
            public static event MidiNote OnMidiNote;
            public delegate void NoteOut(int channel, int note, float velocity);
            public static event NoteOut OnNoteOut;

            private List<TriggerObj> midiNoteEventQueue;
            private List<TriggerObj> midiKnobEventQueue;

            private List<NoteChannel> noteOutCache; //this is to send note off when the program is shot off between notes. 

            private Dictionary<string, TriggerObj> midiKnobs;
            private float knobDelay = 0.0001f;
            private float relativeKnobDelay = 30; //seconds for relative knob to reset. 

            public override void Awake()
            {
                if (this.enabled)
                {
                    setupInstance();
                }
                base.Awake();
            }

            void OnDestroy()
            {
                clearNoteCache();
            }

            public void OnApplicationQuit()
            {
                //TODO - add equivalent function for editor closing. 
                clearNoteCache();
            }

            private void clearNoteCache()
            {
                if (noteOutCache != null)
                {
                    foreach (NoteChannel n in noteOutCache)
                    {

                        //triggerMidiNoteOff(n.channel, n.note, 0, 0);  //this was an IEnumerator, so that's wrong. 
                        MidiMaster.SendNoteOff(midiEndPoint, this.channels[n.channel], n.note, 0); //sending note off immediatly. 
                    }
                    noteOutCache.Clear();
                }
            }

            public void setupInstance()
            {
                //makes cameraController into a singleton.
                if (instance == null)
                {
                    instance = this;

                }
            }

            private void OnEnable()
            {
                enableMidi();
                enableEditor();
                enableMidiOut();
            }

            private void enableEditor()
            {
#if UNITY_EDITOR
                EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
#endif
            }

#if UNITY_EDITOR
            private void OnPlayModeStateChanged(PlayModeStateChange state)
            {
                if (state == PlayModeStateChange.ExitingPlayMode)
                {
                    clearNoteCache();
                }
            }
#endif

            private void enableMidiOut()
            {
                midiEndPoint = GetSendEndpointIdAtIndex(midiOutDeviceIndex);
            }

            private void onDisable()
            {
                disableMidi();
            }

            private void enableMidi()
            {
                MidiMaster.noteOnDelegate += NoteOnHandler;
                MidiMaster.noteOffDelegate += NoteOffHandler;
                MidiMaster.knobDelegate += KnobHandler;
            }

            private void disableMidi()
            {
                MidiMaster.noteOnDelegate -= NoteOnHandler;
                MidiMaster.noteOffDelegate -= NoteOffHandler;
                MidiMaster.knobDelegate -= KnobHandler;
            }

            public override void setup(List<Trigger> triggers)
            {
                this.triggers = triggers;
                midiNoteEventQueue = new List<TriggerObj>();
                midiKnobEventQueue = new List<TriggerObj>();
                midiKnobs = new Dictionary<string, TriggerObj>();
                noteOutCache = new List<NoteChannel>(); //storing note out for noteOff events. 
                this.midiOutDeviceIndex = Config.instance.midiOutDevice; //possible failure point. 
                SetupMidiChannels();
            }

            private void Update()
            {
                sendMidiNotesFromQueue();
                sendThrottledMidiKnobsFromDictionary();
            }

            public void triggerMidiNote(int chan, int note, float velocity, float length)
            {
                //CREATES A MIDI NOTE --OUT-OF-UNITY-- 
                //note is 0 - 128
                //chan is 0-15
                //velocity is a float 0-1
                //length is seconds. 
                MidiMaster.SendNoteOn(midiEndPoint, this.channels[chan], note, velocity);
                NoteChannel nc = new NoteChannel();  //constructor might not be working for some reaon. 
                nc.note = note;
                nc.channel = chan;
                noteOutCache.Add(nc);
                StartCoroutine(delayedTriggerMidiNoteOff(nc, velocity, length));
                OnNoteOut.Invoke(chan, note, velocity);
            }

            public void triggerMidiNoteOff(int chan, int note)
            {
                //IMMEDIATE PUBLIC EVENT TO NOTE OFF. 
                NoteChannel nc = noteOutCache.FindLast(n => n.note == note && n.channel == chan);
                MidiMaster.SendNoteOff(midiEndPoint, this.channels[chan], note, 0);
                noteOutCache.Remove(nc);
            }

            private IEnumerator delayedTriggerMidiNoteOff(NoteChannel nc, float velocity, float delay)
            {
                //CREATES A MIDI NOTE OFF  --OUT-OF-UNITY--
                yield return new WaitForSeconds(delay);
                NoteChannel mostRecentNote = noteOutCache.FindLast(n => n.note == nc.note && n.channel == nc.channel);
                if (mostRecentNote != null && mostRecentNote != nc)
                {
                    // A more recent note exists, suppress this note_off
                    yield break;
                }

                //if notes are hanging, (I wanted to witness first ) try this 

                // Send multiple `note_off` events just in case
                // for (int i = 0; i < 3; i++)  // Send 3 note_offs to ensure the note stops
                // {
                //     MidiMaster.SendNoteOff(midiEndPoint, this.channels[nc.channel], nc.note, velocity);
                // }

                MidiMaster.SendNoteOff(midiEndPoint, this.channels[nc.channel], nc.note, velocity);
                noteOutCache.Remove(nc);
            }

            public void triggerMidiKnob(int chan, int knob, float value)
            {
                //CREATES A MIDI KNOB --OUT-OF-UNITY-- 
                //knob is 0 - 127
                //chan is 0-15
                //value is a float 0-1
                MidiMaster.SendCC(midiEndPoint, this.channels[chan], knob, value);
            }

            private void sendMidiNotesFromQueue()
            {
                //meant to protect automatic chords from canceling each other.
                int lastNote = -1;
                int lastChannel = -1;
                bool lastNoteOff = false;
                string lastEventName = "noEventName";
                for (int i = midiNoteEventQueue.Count - 1; i >= 0; i--)
                {
                    TriggerObj t = midiNoteEventQueue[i];
                    if (!(lastNote == t.note && lastChannel == t.channel
                        && lastEventName == t.eventName && lastNoteOff == t.noteOff))
                    {
                        lastNote = t.note;
                        lastChannel = t.channel;
                        //lastGroup = t.group ?? "noGroup";
                        lastEventName = t.eventName ?? "noEventName";
                        HandleMidiNote(t);
                    }
                    //else - two notes are firing at exactly the same time. 
                };
                midiNoteEventQueue.Clear();
            }

            private void sendThrottledMidiKnobsFromDictionary()
            {
                //placeholder to see if stuff still works.
                foreach (KeyValuePair<string, TriggerObj> knob in midiKnobs)
                {
                    TriggerObj t = knob.Value;
                    string key = knob.Key;
                    if (t.changed != Mathf.Infinity)
                    {
                        //throttling behaviour. 
                        //checks if first event, or last event since delay has been hit. 
                        //reset change timestamp to infinity to prevent refiring.
                        if (t.changed == t.created || (Time.time - t.changed) > knobDelay)
                        {
                            OnMidiKnob(t);
                            t.changed = Mathf.Infinity;
                        }
                    }
                }
            }

            private void NoteOnHandler(MidiChannel channel, int note, float velocity)
            {
                if (Application.isPlaying)
                {
                    OnMidiNote.Invoke(channelIndexes[channel], note, velocity);
                    foreach (Trigger trigger in triggers)
                    {
                        for (int chan = 0; chan < 16; chan++)
                        {
                            if (channels[chan] == channel)
                            {
                                if (CheckMidiTrigger(chan, note, trigger))
                                {
                                    AddToMidiNoteEventQueue(trigger, note, velocity);
                                }
                            }
                        }
                    }
                }
            }

            private void NoteOffHandler(MidiChannel channel, int note)
            {
                foreach (Trigger trigger in triggers)
                {
                    for (int chan = 0; chan < 16; chan++)
                    {
                        if (channels[chan] == channel)
                        {
                            if (CheckMidiTrigger(chan, note, trigger))
                            {
                                //using negative one to represent note off. 
                                AddToMidiNoteEventQueue(trigger, note, -1);
                            }
                        }
                    }
                }
            }

            private void AddToMidiNoteEventQueue(Trigger trigger, int note, float velocity)
            {
                TriggerObj triggerObj = new TriggerObj();
                triggerObj.channel = trigger.channel;
                triggerObj.eventName = trigger.eventName;
                triggerObj.trigger = trigger;
                triggerObj.created = Time.time;
                triggerObj.changed = Time.time;
                triggerObj.note = note;
                triggerObj.sustain = trigger.sustain;
                //triggerObj.group = trigger.group;
                if (velocity == -1 || velocity == 0)
                {
                    triggerObj.source = "midiNoteOff";
                    triggerObj.noteOff = true;
                }
                else
                {
                    if (velocity > 0.2)
                    {
                        triggerObj.source = "midiNoteOn";
                        triggerObj.velocity = velocity;
                    }
                }
                midiNoteEventQueue.Add(triggerObj);
            }

            private void AddToMidiKnobEventQueue(Trigger trigger, int knob, float value)
            {
                string key = trigger.eventName + ":" + trigger.channel + ":" + knob;
                TriggerObj t;
                if (midiKnobs.TryGetValue(key, out t))
                {
                    t.value = value;
                    t.changed = Time.time;
                    getRelativeValue(value, t);
                    if ((t.changed - t.created) > knobDelay)
                    {
                        //throttling behaviour. 
                        //if knob hasn't been hit in a while, reset created timeStamp, which causes first knob hit to fire.
                        t.created = t.changed;
                    }
                }
                else
                {
                    t = new TriggerObj();
                    midiKnobs.Add(key, t);
                    t.value = value;
                    t.created = Time.time;
                    t.changed = Time.time;
                    t.trigger = trigger;
                    t.eventName = trigger.eventName;
                    t.knob = knob;
                    t.relativeValue = 0;
                    t.lastRelativeValue = value;
                }
                t.value = value;
                t.source = "midiKnob";
                midiKnobEventQueue.Add(t);
            }

            private void getRelativeValue(float value, TriggerObj t)
            {
                float diff;
                if (t.changed - t.created > relativeKnobDelay)
                {
                    diff = 0;
                }
                else
                {
                    diff = value - t.lastRelativeValue;
                    float amountOver;
                    if (Mathf.Abs(diff) >= .5f)
                    {
                        if (diff >= .5f)
                        {
                            amountOver = .5f - diff;
                        }
                        else
                        {
                            amountOver = -.5f - diff;
                        }
                        t.lastRelativeValue = t.lastRelativeValue - amountOver;
                        diff = diff - amountOver;
                    }
                }
                t.relativeValue = Mathf.Clamp(diff * 2, -1, 1); //TODO - might not need clamp - test.
            }

            private void KnobHandler(MidiChannel channel, int knob, float value)
            {
                OnMidiCC.Invoke(channelIndexes[channel], knob, value);
                foreach (Trigger trigger in triggers)
                {
                    for (int chan = 0; chan < 16; chan++)
                    {
                        if (channels[chan] == channel)
                        {
                            if (knob == trigger.knob && chan == trigger.channel)
                            {
                                AddToMidiKnobEventQueue(trigger, knob, value);
                                // this return was the second knob from being fired.  
                                //return;
                            }
                        }
                    }
                }
            }

            private bool CheckMidiTrigger(int channel, int note, Trigger trigger)
            {
                if (channel == trigger.channel)
                {
                    return checkMidiNote(note, trigger);
                }
                return false;
            }

            private bool checkMidiNote(int note, Trigger trigger)
            {
                if (trigger.note2 > trigger.note)
                {
                    return note >= trigger.note && note <= trigger.note2;
                }
                else
                {
                    return trigger.note == note;
                }
            }


            private bool CheckMidiKnob(int channel, int knob, Trigger trigger)
            {
                if (channel == trigger.channel && knob == trigger.knob)
                {
                    return true;
                }
                return false;
            }



            public void HandleMidiNote(TriggerObj triggerObj)
            {
                this.trigger(triggerObj);
            }

            public void OnMidiNoteOn(int channel, int note, float velocity)
            {
                //mostly used for tests.
                foreach (Trigger trigger in triggers)
                {
                    if (CheckMidiTrigger(channel, note, trigger))
                    {
                        TriggerObj triggerObj = new TriggerObj();
                        triggerObj.trigger = trigger;
                        triggerObj.eventName = trigger.eventName;
                        triggerObj.note = note;
                        triggerObj.velocity = velocity;
                        triggerObj.created = Time.time;
                        triggerObj.source = "midiNoteOn";
                        triggerObj.sustain = trigger.sustain;
                        this.trigger(triggerObj);
                    }
                }
            }

            public void OnMidiKnob(TriggerObj triggerObj)
            {
                this.trigger(triggerObj);
                this.cacheTriggered(triggerObj);
            }

            public void OnMidiKnob(int channel, int knob, float value)
            {
                foreach (Trigger trigger in triggers)
                {
                    if (CheckMidiKnob(channel, knob, trigger))
                    {
                        TriggerObj triggerObj = new TriggerObj();
                        triggerObj.trigger = trigger;
                        triggerObj.eventName = trigger.eventName;
                        triggerObj.knob = knob;
                        triggerObj.value = value;
                        triggerObj.created = Time.time;
                        triggerObj.source = "midiKnob";
                        this.trigger(triggerObj);
                        this.cacheTriggered(triggerObj);
                    }
                }
            }

            public MidiChannel getChannel(int chan)
            {
                return this.channels[chan];
            }

            private void SetupMidiChannels()
            {
                channels = new MidiChannel[17];
                channels[0] = MidiChannel.Ch1;
                channels[1] = MidiChannel.Ch2;
                channels[2] = MidiChannel.Ch3;
                channels[3] = MidiChannel.Ch4;
                channels[4] = MidiChannel.Ch5;
                channels[5] = MidiChannel.Ch6;
                channels[6] = MidiChannel.Ch7;
                channels[7] = MidiChannel.Ch8;
                channels[8] = MidiChannel.Ch9;
                channels[9] = MidiChannel.Ch10;
                channels[10] = MidiChannel.Ch11;
                channels[11] = MidiChannel.Ch12;
                channels[12] = MidiChannel.Ch13;
                channels[13] = MidiChannel.Ch14;
                channels[14] = MidiChannel.Ch15;
                channels[15] = MidiChannel.Ch16;
                channels[16] = MidiChannel.All;

                channelIndexes = new Dictionary<MidiChannel, int>();
                for (int i = 0; i < channels.Length; i++)
                {
                    MidiChannel channel = channels[i];
                    channelIndexes[channel] = i;
                }
            }

            public class NoteChannel
            {
                public int note;
                public int channel;
                //public float velocity;

                public NoteChannel(int note, int channel)
                {
                    this.note = note;
                    this.channel = channel;
                }
                public NoteChannel()
                {
                }
            }

            #region Native Plugin Interface

            [DllImport("MidiJackPlugin", EntryPoint = "MidiJackGetSendEndpointIDAtIndex")]
            static extern uint GetSendEndpointIdAtIndex(int index);

            #endregion


        }
    }
}