using System.Collections.Generic;
using flyby.Triggers;
using flyby.Sockets; //do we need a namespace for sockets? 
using flyby.Sets;
using flyby.Core;
using Firesplash.GameDevAssets.SocketIO;
using Newtonsoft.Json;
using UnityEngine;

namespace flyby
{
    namespace Controllers
    {
        public class SocketController : TriggerSourceController
        {
            //this class takes care of both socket_interface webui and discord chat. 

            public SocketIOCommunicator sioCom;
            [HideInInspector]
            public bool connected;
            private Config config;
            private List<SocketUser> users;
            private List<SocketUser> idleUsers; //garbage collecting idle users. 
            private int userIndex;
            private float idleTime = 300; //5 minutes - take down to 30 seconds when demoing. 
            private int maxUsers = 1; // take down when developing. 
            public float lerpAmount = 15;
            [HideInInspector]
            public List<Role> primaryRoles;  //TODO - fix behavior for primary roles - look at bonePhone.unity
            [HideInInspector]
            public List<Role> secondaryRoles; //we're using the part to populate roles. 
            public List<Role> roles;
            public List<Role> chatRoles;
            private List<Trigger> discordTriggers;

            private int roleIndex = 0; //adds one. 
            private int lastRoleIndex; //might not need if we're only doing chronological. 
            private TriggerObj t; //reusable triggerobj since there are a lot. 
            private JsonSerializerSettings JSONSettings;


            public override void setup(List<Trigger> triggers)
            {

                if (!Config.instance.usingSockets) return;

                this.JSONSettings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DefaultValueHandling = DefaultValueHandling.Ignore,
                    NullValueHandling = NullValueHandling.Ignore
                };

                if (this.enabled && this.active)
                {
                    this.sioCom.enabled = true;
                    this.config = Config.instance;
                    this.users = new List<SocketUser>();
                    this.idleUsers = new List<SocketUser>();

                    if (this.config.vars != null && !string.IsNullOrEmpty(this.config.vars.socket_server))
                    {
                        this.discordTriggers = filterDiscordTriggers(triggers); //deprecated.
                        //this.roles = setupRoles(); --now being handled by part changed handler. 

                        if (!connected)
                        {
                            connectToSocketServer();
                        }
                        //TODO - check if triggers is greater than zero. 
                    }
                    else
                    {
                        this.sioCom.enabled = false;
                        Debug.Log("no socket server in config.");
                    }
                }
            }

            void OnDestroy()
            {
                DisconnectSocket();
            }

            private void DisconnectSocket()
            {
                if (this.sioCom.Instance != null && this.sioCom.Instance.IsConnected())
                {
                    sioCom.Instance.Close();
                    Debug.Log("Socket disconnected cleanly.");
                }
            }


            private void HandleLog(string logString, string stackTrace, LogType type)
            {
                //experimental.
                if (type == LogType.Error || type == LogType.Exception)
                {
                    this.DisconnectSocket();
                }
            }

            private bool userIsMoving(SocketUser user)
            {
                float epsilon = 0.005f;
                return Mathf.Abs(user.current_x - user.x) > epsilon || Mathf.Abs(user.current_y - user.y) > epsilon;
            }

            public void Update()
            {
                if (!Config.instance.usingSockets) return;

                if (this.connected)
                {

                    idleUsers = new List<SocketUser>();

                    foreach (SocketUser user in this.users)
                    {
                        if (user.roleObj != null && user.roleObj.active)
                        {
                            user.current_x = Mathf.Lerp(user.current_x, user.x, Time.deltaTime * lerpAmount);
                            user.current_y = Mathf.Lerp(user.current_y, user.y, Time.deltaTime * lerpAmount);

                            if (userIsMoving(user))
                            {
                                //not sure if we should be sending the avatar each time.
                                //let's attempt to save some memory. 

                                //trigger x
                                t = new TriggerObj();
                                t.eventName = user.roleObj.xTrigger;
                                t.knob = 99; //user.i might have caused show-stopping bug ( for chan:0 , knob:0 ) 
                                t.value = user.current_x; //TODO - replace with current x. 
                                t.created = Time.time;
                                t.source = "socketKnob";
                                t.avatar = user.avatar;
                                this.trigger(t);
                                this.cacheTriggered(t);

                                //trigger y 
                                t = new TriggerObj();
                                t.eventName = user.roleObj.yTrigger;
                                t.knob = 99;
                                t.value = 1.0f - user.current_y; //TODO - replace with current lerped y.   upside down y ( top is 0 on browser ) 
                                t.created = Time.time;
                                t.avatar = user.avatar;
                                t.source = "socketKnob";
                                this.trigger(t);
                                this.cacheTriggered(t);
                            }
                        }


                        //check for idle. 
                        if (Time.time - user.lastTimeUsed > idleTime)
                        {
                            user.idle = true;
                            idleUsers.Add(user);
                        }
                    }
                }

                if (idleUsers != null && idleUsers.Count > 0)
                {
                    foreach (SocketUser user in idleUsers)
                    {
                        Debug.Log("removing user: " + user.id + " for being idle... " + idleUsers.Count + " idleusers and " + users.Count + " actual users");
                        removeUser(user);
                    }
                }
            }

            private List<Trigger> filterDiscordTriggers(List<Trigger> _triggers)
            {
                this.discordTriggers = new List<Trigger>();
                foreach (Trigger trigger in _triggers)
                {
                    if (!string.IsNullOrEmpty(trigger.chat) || !string.IsNullOrEmpty(trigger.user))
                    {
                        this.discordTriggers.Add(trigger);
                    }
                }
                return this.discordTriggers;
            }

            private List<Role> setupRoles()
            {
                //socket webUI interface roles. 
                this.roles = new List<Role>();
                this.roles.AddRange(this.primaryRoles);
                int totalRoles = this.primaryRoles.Count + this.secondaryRoles.Count;

                if (totalRoles > this.maxUsers)
                {
                    this.maxUsers = totalRoles;
                }

                int secondary_i = 0;

                //these two functions even out the roles and maxUsers.  

                //TODO - getNextRole still needs logic to sort through primary roles first. 
                while (this.roles.Count < this.maxUsers)
                {
                    this.secondaryRoles[secondary_i].secondary = true;
                    this.roles.Add(
                        new Role(this.secondaryRoles[secondary_i])
                    );
                    secondary_i = (int)Mathf.Repeat(secondary_i + 1, this.secondaryRoles.Count);
                }

                for (int i = 0; i < this.roles.Count; i++)
                {
                    //role indexes determine which triggers eventNames, which controls distinct behavior. 
                    this.roles[i].i = i;
                    this.roles[i].startTrigger = string.IsNullOrEmpty(this.roles[i].startTrigger) ? "socket" + i : this.roles[i].startTrigger;
                    this.roles[i].trigger1 = string.IsNullOrEmpty(this.roles[i].trigger1) ? "socket_trigger1_" + i : this.roles[i].trigger1;
                    this.roles[i].xTrigger = string.IsNullOrEmpty(this.roles[i].xTrigger) ? "socket_x_" + i : this.roles[i].xTrigger;
                    this.roles[i].yTrigger = string.IsNullOrEmpty(this.roles[i].yTrigger) ? "socket_y_" + i : this.roles[i].yTrigger;
                }
                return this.roles;
            }

            private void prepareRolesFromPart(List<Role> partRoles)
            {
                this.primaryRoles = new List<Role>();
                this.secondaryRoles = new List<Role>();
                foreach (Role role in partRoles)
                {
                    if (!role.disabled)
                    {
                        if (role.primary)
                        {
                            this.primaryRoles.Add(role);
                        }
                        else
                        {
                            this.secondaryRoles.Add(role);
                        }
                    }
                }
            }

            private void prepareChatRolesFromPart(List<Role> chatRoles)
            {
                this.chatRoles = new List<Role>();
                if (chatRoles != null && chatRoles.Count > 0)
                {
                    foreach (Role chatRole in chatRoles)
                    {
                        if (!chatRole.disabled)
                        {
                            this.chatRoles.Add(chatRole);
                        }
                    }
                }
            }

            private void connectToSocketServer()
            {
                sioCom.Instance.On("connect", (string data) =>
                {
                    //starting a handshake. 
                    Debug.Log("successfully connected to socket server.");
                    this.connected = true;
                });

                //When the conversation is done, the server will close our connection after we said Goodbye
                sioCom.Instance.On("disconnect", (string payload) =>
                {
                    if (payload.Equals("io server disconnect"))
                    {
                        Debug.Log("Disconnected from server.");
                    }
                    else
                    {
                        Debug.LogWarning("We have been unexpecteldy disconnected. This will cause an automatic reconnect. Reason: " + payload);
                    }
                });

                sioCom.Instance.On("client_disconnected", (string payload) =>
                {
                    if (this.roles != null && this.roles.Count > 0)
                    {
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        SocketUser user = findUserById(socketPayload.id);
                        removeUser(user);
                    }
                });

                sioCom.Instance.On("introduction", (string payload) =>
                {
                    // TODO - test - is this done every part change? 
                    // do we get messed up if we start a part and there are no roles connected?

                    if (this.roles != null && this.roles.Count > 0)
                    {
                        SocketUser user = JsonConvert.DeserializeObject<SocketUser>(payload);

                        if (user.presenter)
                        {
                            sioCom.Instance.Emit("presenter_connected", JsonConvert.SerializeObject(user), false);
                            this.PartChangedHandler(FlyByController.instance.currentPart); //experimental.
                        }
                        else
                        {
                            this.addUser(user, "introduction");
                            user = this.AssignNewRole(user);
                            sioCom.Instance.Emit("role_assigned", JsonConvert.SerializeObject(user), false);
                        }
                    }
                });

                sioCom.Instance.On("new_role_handler", (string payload) =>
                {
                    // this is a button handler - 
                    //TODO -  this will replace the user currently on the role.
                    //need to send a signal that the role has been replaced.  

                    //large button at the top of web interface that requests a new identity for the player.  
                    if (this.roles != null && this.roles.Count > 0)
                    {
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        SocketUser user = this.findUserById(socketPayload.id);

                        if (user != null)
                        {
                            user.lastTimeUsed = Time.time;
                            user.avatar = string.IsNullOrEmpty(user.avatar) ? socketPayload.avatar : user.avatar;
                            user = this.AssignNewRole(user);
                            sioCom.Instance.Emit("role_assigned", JsonConvert.SerializeObject(user), false);
                        }
                        else
                        {
                            Debug.Log("New role handler for null user.");
                        }
                    }
                });

                sioCom.Instance.On("xy_handler", (string payload) =>
                {
                    if (this.roles != null && this.roles.Count > 0)
                    {

                        //mouse movement on web handler. 
                        Vector3 position = JsonConvert.DeserializeObject<Vector3>(payload);
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        SocketUser user = findUserById(socketPayload.id);

                        if (user != null)
                        {
                            user.lastTimeUsed = Time.time;
                            user.x = position.x;
                            user.y = position.y;
                            user.avatar = string.IsNullOrEmpty(user.avatar) ? socketPayload.avatar : user.avatar;
                        }
                        else
                        {
                            user = JsonConvert.DeserializeObject<SocketUser>(payload);
                            this.addUser(user, "xy");
                            user = this.AssignNewRole(user);
                            user.x = position.x;
                            user.y = position.y;
                            user.avatar = string.IsNullOrEmpty(user.avatar) ? socketPayload.avatar : user.avatar; //maybe this was all you needed. 
                            user.lastTimeUsed = Time.time;
                            sioCom.Instance.Emit("role_assigned", JsonConvert.SerializeObject(user), false);
                        }
                    }

                });

                sioCom.Instance.On("trigger_handler", (string payload) =>
                {
                    // this is a generic trigger impulse from a web UI button.  ( non game UI ) 
                    if (this.roles != null && this.roles.Count > 0)
                    {
                        Vector3 position = JsonConvert.DeserializeObject<Vector3>(payload); //this is an impulse but spatial data is saved. 
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        SocketUser user = findUserById(socketPayload.id);

                        // TODO - how to send user info through the trigger? 
                        // triggerObjs are not sacred, they can take different forms that include identifying information to a custom script. 
                        if (user == null)
                        {
                            Debug.Log("trigger handler for user that hasn't been introduced. Assigning next role to user now:  avatar: " + socketPayload.avatar);
                            //experimental. 
                            user = JsonConvert.DeserializeObject<SocketUser>(payload);
                            this.addUser(user, "trigger_handler");
                            user = this.AssignNewRole(user);
                            user.lastTimeUsed = Time.time;
                        }

                        if (user != null)
                        {
                            user.lastTimeUsed = Time.time;
                            user.avatar = string.IsNullOrEmpty(user.avatar) ? socketPayload.avatar : user.avatar;
                            user.user = socketPayload.user;
                            t = new TriggerObj();

                            //can override the custom socketTrigger event name. 
                            if (!string.IsNullOrEmpty(socketPayload.eventName))
                            {
                                t.eventName = socketPayload.eventName;
                            }
                            else
                            {
                                t.eventName = user.roleObj.trigger1;
                            }
                            t.source = "socketOn";
                            t.avatar = user.avatar;
                            t.user = user.user;
                            t.position = new Vector3(position.x, position.y, 0);
                            t.velocity = 128;
                            this.trigger(t);
                        }
                    }
                });

                sioCom.Instance.On("buttonOn_handler", (string payload) =>
                {
                    //gamepad ui - making sustain trigger. 
                    if (this.roles != null && this.roles.Count > 0)
                    {
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        SocketUser user = findUserById(socketPayload.id);


                        if (user == null || user.roleObj == null)
                        {
                            Debug.Log("buttonOn handler for user that hasn't been introduced. Assigning next role to user now.");
                            //experimental. 
                            user = JsonConvert.DeserializeObject<SocketUser>(payload);
                            this.addUser(user, "buttonOn_handler");
                            user = this.AssignNewRole(user);
                            user.lastTimeUsed = Time.time;
                        }

                        if (user != null && user.roleObj != null)
                        {
                            user.lastTimeUsed = Time.time;
                            t = new TriggerObj();
                            if (!string.IsNullOrEmpty(socketPayload.eventName))
                            {
                                t.eventName = socketPayload.eventName;
                            }
                            else
                            {
                                switch (socketPayload.triggerID)
                                {
                                    case 1:
                                        t.eventName = user.roleObj.trigger1;
                                        break;
                                    case 2:
                                        t.eventName = user.roleObj.trigger2;
                                        break;
                                    default:
                                        t.eventName = user.roleObj.trigger1;
                                        break;
                                }
                            }
                            t.source = "socketOn";
                            t.avatar = user.avatar;
                            t.user = user.user; //user.user is username. 
                            t.sustain = true;
                            t.velocity = 128;
                            this.trigger(t);
                        }
                    }
                });

                sioCom.Instance.On("buttonOff_handler", (string payload) =>
                {
                    //gamepad ui - making sustain trigger. 
                    if (this.roles != null && this.roles.Count > 0)
                    {
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        SocketUser user = findUserById(socketPayload.id);
                        if (user != null && user.roleObj != null)
                        {
                            user.lastTimeUsed = Time.time;
                            t = new TriggerObj();
                            if (!string.IsNullOrEmpty(socketPayload.eventName))
                            {
                                t.eventName = socketPayload.eventName;
                            }
                            else
                            {
                                switch (socketPayload.triggerID)
                                {
                                    case 1:
                                        t.eventName = user.roleObj.trigger1;
                                        break;
                                    case 2:
                                        t.eventName = user.roleObj.trigger2;
                                        break;
                                    default:
                                        t.eventName = user.roleObj.trigger1;
                                        break;
                                }
                            }
                            t.source = "socketOff";
                            t.noteOff = true;
                            t.sustain = true;
                            this.trigger(t);
                        }
                        {
                            user.lastTimeUsed = Time.time;
                            t = new TriggerObj();
                            if (!string.IsNullOrEmpty(socketPayload.eventName))
                            {
                                t.eventName = socketPayload.eventName;
                            }
                            else
                            {
                                switch (socketPayload.triggerID)
                                {
                                    case 1:
                                        t.eventName = user.roleObj.trigger1;
                                        break;
                                    case 2:
                                        t.eventName = user.roleObj.trigger2;
                                        break;
                                    default:
                                        t.eventName = user.roleObj.trigger1;
                                        break;
                                }
                            }
                            t.source = "socketOff";
                            t.sustain = true;
                            t.noteOff = true;
                            this.trigger(t);
                        }
                    }
                });

                sioCom.Instance.On("dpadOn_handler", (string payload) =>
                {
                    //gamepad ui for dpad.
                    if (this.roles != null && this.roles.Count > 0)
                    {
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        SocketUser user = findUserById(socketPayload.id);

                        if (user == null || user.roleObj == null)
                        {
                            Debug.Log("dpad is being used while user is null. creating a new one.");
                            user = JsonConvert.DeserializeObject<SocketUser>(payload);
                            this.addUser(user, "dpadOn_handler");
                        }

                        if (user != null && user.roleObj != null)
                        {
                            user.lastTimeUsed = Time.time;
                            t = new TriggerObj();
                            if (!string.IsNullOrEmpty(socketPayload.eventName))
                            {
                                t.eventName = socketPayload.eventName;
                            }

                            switch (socketPayload.triggerID)
                            {
                                case 1:
                                    t.eventName = user.roleObj.upTrigger;
                                    break;
                                case 2:
                                    t.eventName = user.roleObj.rightTrigger;
                                    break;
                                case 3:
                                    t.eventName = user.roleObj.downTrigger;
                                    break;
                                case 4:
                                    t.eventName = user.roleObj.leftTrigger;
                                    break;
                                default:
                                    t.eventName = user.roleObj.upTrigger;
                                    break;
                            }
                            t.sustain = true;
                            t.velocity = 128;
                            t.source = "socketOn";
                            this.trigger(t);
                        }

                    }
                });

                sioCom.Instance.On("dpadOff_handler", (string payload) =>
                {
                    if (this.roles != null && this.roles.Count > 0)
                    {
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        SocketUser user = findUserById(socketPayload.id);

                        //dont create user for note off. 

                        if (user != null && user.roleObj != null)
                        {
                            user.lastTimeUsed = Time.time;
                            t = new TriggerObj();
                            switch (socketPayload.triggerID)
                            {
                                case 1:
                                    t.eventName = user.roleObj.upTrigger;
                                    break;
                                case 2:
                                    t.eventName = user.roleObj.rightTrigger;
                                    break;
                                case 3:
                                    t.eventName = user.roleObj.downTrigger;
                                    break;
                                case 4:
                                    t.eventName = user.roleObj.leftTrigger;
                                    break;
                                default:
                                    t.eventName = user.roleObj.upTrigger;
                                    break;
                            }
                            t.source = "socketOff";
                            t.noteOff = true;
                            t.sustain = true;
                            this.trigger(t);
                        }
                    }
                });


                sioCom.Instance.On("discord_trigger", (string payload) =>
                {
                    //this is distinct from discord message because it is from the discord UI. 

                    //chat roles from the part are mostly being setup on the server side to organize the buttons. 
                    SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                    //TODO - think about to getting avatar link. discordUser = findDiscordUserByID(socketPayload.id);
                    Debug.Log("we have an avatar: " + socketPayload.avatar + " and will trigger: " + socketPayload.eventName);
                    t = new TriggerObj();
                    t.eventName = socketPayload.eventName;
                    Debug.Log("triggering: " + t.eventName);
                    t.avatar = socketPayload.avatar;
                    t.user = socketPayload.user;
                    t.source = "discord";
                    this.trigger(t);

                });

                sioCom.Instance.On("discord_message", (string payload) =>
                {
                    // onChat -  this is setup toreceive and parse any chat.
                    // this could possibly cause an overload under heavay chat. 
                    // TODO - not sure why we were checking for roles here.  
                    if (this.discordTriggers != null)
                    {
                        SocketPayload socketPayload = JsonConvert.DeserializeObject<SocketPayload>(payload);
                        Debug.Log("received discord message: " + socketPayload.message);
                        SocketUser user = findUserById(socketPayload.id);

                        foreach (Trigger trigger in this.discordTriggers)
                        {
                            if (socketPayload.message.Contains(trigger.chat))
                            {
                                t = new TriggerObj();
                                t.eventName = trigger.eventName;
                                t.source = "discord";
                                t.message = socketPayload.message;
                                t.avatar = socketPayload.avatar;
                                t.user = socketPayload.user;
                                if (user != null)
                                {
                                    t.position = new Vector3(user.x, user.y, 0);
                                }
                                this.trigger(t);
                            }
                        }
                    }
                });

                SIOAuthPayload auth = new SIOAuthPayload();
                auth.AddElement("presenter", true);
                sioCom.Instance.Connect(config.vars.socket_server, true, auth);
            }

            private SocketUser AssignNewRole(SocketUser user)
            {
                //used for obj_xy:start_event  startEvents

                string user_debug = JsonConvert.SerializeObject(user, Formatting.Indented);

                if (user != null)
                {
                    //sending a noteOff to last role, which might have had a sustained note. 
                    Role lastRole = user.roleObj;
                    this.triggerOffEvents(user, lastRole);

                    user = assignNextRoleToUser(user);

                    user_debug = JsonConvert.SerializeObject(user, Formatting.Indented);
                    //TODO - remove from a list of sustained objs. 
                    t = new TriggerObj();
                    t.eventName = user.roleObj.startTrigger;
                    t.source = "socketOn";
                    //t.user - maybe soon.
                    t.note = user.i;
                    t.velocity = 128;
                    t.avatar = user.avatar;
                    t.user = user.user;
                    t.sustain = true;
                    this.trigger(t);
                }
                else
                {
                    Debug.Log("trying to assign a role to a null user");
                }

                return user;
            }

            private SocketUser assignNextRoleToUser(SocketUser user)
            {
                //only doing chronological for now. ( stay in secondary ) 
                Role nextRole = getNextRole();
                user.roleObj = nextRole;
                user.roleObj.active = true;
                this.lastRoleIndex = nextRole.i;
                return user;
            }

            private Role getNextRole()
            {
                if (this.roles == null || this.roles.Count == 0)
                {
                    Debug.Log("no roles to assign.");
                    return null;
                }
                this.roleIndex = (int)Mathf.Repeat(this.roleIndex + 1, this.roles.Count);
                return this.roles[this.roleIndex];
            }

            private void triggerOffEvents(SocketUser user, Role role)
            {
                //used for obj_xy: events - send note off for knob ( or zero to knobs ) 
                if (role == null) return;

                if (!string.IsNullOrEmpty(role.startTrigger))
                {
                    t = new TriggerObj();
                    t.eventName = role.startTrigger;
                    t.note = user.i;
                    t.source = "socketOff";
                    t.noteOff = true;
                    this.trigger(t);
                }

                if (!string.IsNullOrEmpty(role.xTrigger))
                {
                    t = new TriggerObj();
                    t.eventName = role.xTrigger;
                    t.knob = 99;
                    t.value = 0; //TODO this is meant to clear the value.  find default value from active Trigger somehow.   (0.5 might be bad for filters ) 
                    t.source = "socketKnob";
                    t.created = Time.time;
                    this.trigger(t);
                }

                if (!string.IsNullOrEmpty(role.yTrigger))
                {
                    t = new TriggerObj();
                    t.eventName = role.yTrigger;
                    t.knob = 99;
                    t.value = 0; //TODO this is mean to clear the value.  - find default value from active Trigger somehow. ( 0.5 might be bad for filters )
                    t.source = "socketKnob";
                    t.created = Time.time;
                    this.trigger(t);
                }
            }

            private void removeUser(SocketUser user)
            {
                if (user != null && user.roleObj != null)
                {
                    user.roleObj.active = false;
                    triggerOffEvents(user, user.roleObj);
                }
                this.users.Remove(user);
            }


            public void addUser(SocketUser user, string eventSource)
            {
                //TODO - assign the role
                Debug.Log(eventSource + ": adding user " + user.id + " with avatar: " + user.avatar);
                SocketUser existingUser = findUserById(user.id);
                string existingAvatar = "";

                if (existingUser != null)
                {
                    existingAvatar = existingUser.avatar;
                    if (string.IsNullOrEmpty(user.avatar))
                    {
                        Debug.Log(eventSource + ": newUser id: " + user.id + " is keeping old avatar bc wtf: " + existingAvatar + " from existing user " + existingUser.id);
                        user.avatar = existingAvatar;
                    }

                }

                if (user != null && !string.IsNullOrEmpty(user.id)) //experimental. 
                {
                    user.i = getNextIndex();
                    this.users.Add(user);
                }
                else
                {
                    Debug.Log("trying to add a null user");
                }
            }

            private void AddAllUsers()
            {
                if (this.users != null)
                {
                    foreach (SocketUser user in this.users)
                    {
                        this.AssignNewRole(user);
                    }
                }
            }

            private int getNextIndex()
            {
                userIndex++;
                return userIndex;
            }


            public SocketUser findUserById(string user_id)
            {
                foreach (SocketUser user in this.users)
                {
                    if (user.id == user_id)
                    {
                        return user;
                    }
                }
                Debug.Log("something went wrong, cannot find user: " + user_id);
                return null;
            }

            public void PartChangedHandler(Part part)
            {
                this.removeAllUsers();
                if (part.roles != null && part.roles.Count > 0)
                {
                    this.userIndex = -1;
                    this.roleIndex = -1;
                    this.prepareRolesFromPart(part.roles);
                    this.prepareChatRolesFromPart(part.chatRoles);
                    this.setupRoles();
                    this.AddAllUsers();
                    if (this.connected)
                    {
                        StatePayload state_payload = new StatePayload();
                        state_payload.users = this.users;
                        state_payload.chatRoles = this.chatRoles;
                        sioCom.Instance.Emit("state_changed", JsonConvert.SerializeObject(state_payload, this.JSONSettings), false);
                    }
                }
            }

            private void checkIdleUsers()
            {
                idleUsers = new List<SocketUser>();
                foreach (SocketUser user in this.users)
                {
                    if (Time.time - user.lastTimeUsed > idleTime)
                    {
                        user.idle = true;
                        idleUsers.Add(user);
                    }
                }

                foreach (SocketUser user in idleUsers)
                {
                    this.removeUser(user);
                }
            }

            private void removeAllUsers()
            {
                if (this.users != null && this.users.Count > 0)
                {
                    List<SocketUser> usersToRemove = new List<SocketUser>(this.users);
                    foreach (SocketUser user in usersToRemove)
                    {
                        this.removeUser(user);
                    }
                    this.users.Clear();
                }
            }
        }
    }

    namespace Sockets
    {
        struct StatePayload
        {
            public List<SocketUser> users;
            public List<Role> chatRoles;
        }

        struct SocketPayload
        {
            public string id;
            public string message;
            public string user; //not used yet. could potentially cause crash.
            public string avatar; //hardcoded full url of avatar ( hard-coded for now - will possibly change to id for safety ) 
            public string room;
            public int triggerID; //base one on actual buttons, if it's zero, it's inactive. 
            public string eventName; // can override default socket trigger.s 
        }

        struct DiscordRole
        {
            public string id;
            public string description;
            public List<TriggerUI> triggers;
        }

        public class SocketUser
        {
            //TODO - add avatar, username. 
            public string id; //socket id. 
            public string user; //username - index to re-use socketPayload. 
            public string avatar;  //hardcoded full url of avatar ( hard-coded for now - will possibly change to id for safety )
            public int i;
            public float lastTimeUsed;
            public string message; //extra information. 
            public Role roleObj;
            public bool presenter; //the unity connection. 
            public bool active; //in use. 
            public bool idle;
            public float x; //target x. 
            public float y; //target y
            public float current_x; //stored lerped x. 
            public float current_y; //stored lerped y.
        }
    }
}


