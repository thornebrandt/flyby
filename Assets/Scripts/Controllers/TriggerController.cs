using System.Collections.Generic;
using flyby.Triggers;
using flyby.Active;
using flyby.Sets;
using UnityEngine;


namespace flyby
{
    namespace Controllers
    {
        public class TriggerController : MonoBehaviour
        {
            [HideInInspector]
            public static TriggerController instance { get; private set; } //makes singleton
            public List<Trigger> triggers; //list of definitions of triggers
            [HideInInspector]
            public TriggerSourceController[] triggerSources; //variable list of trigger sources.
            public Dictionary<string, TriggerObj> triggered; //storing continuous trigger values like knobs.  

            // public Dictionary<string, int> groupLengths;
            // public Dictionary<string, int> groupIndexes;
            // public Dictionary<string, int> startGroupIndexes; //to get back on zero.  
            //should triggers be in scriptable object?

            void Awake()
            {
                if (instance == null)
                {
                    instance = this;
                }
            }

            public void setup(List<Trigger> _triggers, TriggerSourceController[] triggerSources)
            {
                this.triggerSources = triggerSources;
                this.triggers = _triggers;
                this.triggered = new Dictionary<string, TriggerObj>();
                registerTriggers(this.triggers);
            }


            public void registerTriggers(List<Trigger> _triggers)
            {
                foreach (var triggerSource in triggerSources)
                {
                    if (triggerSource.enabled && triggerSource.active)
                    {
                        triggerSource.setup(_triggers);
                    }
                }
            }

            public void kill()
            {
                this.triggered.Clear();
            }

            public TriggerObj getCachedKnob(TriggerObj t)
            {
                if (this.triggered.Count == 0)
                {
                    return t;
                }
                else
                {
                    if (!this.triggered.TryGetValue(t.eventName, out t))
                    {
                        return t;
                    }
                }
                return t; //null
            }

            private TriggerObj createKnobPlaceHolder(ActiveTrigger a)
            {
                TriggerObj t = new TriggerObj();
                t.eventName = a.trigger;
                //t.knob = -1; //not sure this is correct with the below. 
                if (a.triggerProperty != null)
                {
                    t.value = a.triggerProperty.start;
                }
                return t;
            }

            public TriggerObj getCachedKnob(ActiveTrigger a)
            {
                TriggerObj t;
                if (this.triggered.Count == 0)
                {
                    t = createKnobPlaceHolder(a);
                    return t;
                }
                else
                {
                    if (!this.triggered.TryGetValue(a.trigger, out t))
                    {
                        return t;
                    }
                }
                return t;
            }

            public void cacheKnob(ActiveTrigger a)
            {
                TriggerObj t = new TriggerObj();
                t.eventName = a.trigger;
                t.value = a.knobValue;
                this.triggered[a.trigger] = t;
            }
        }
    }
}