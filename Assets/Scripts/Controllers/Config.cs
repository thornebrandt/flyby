using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    namespace Controllers
    {
        public class Config
        {
            public static Config instance { get; set; }
            public string jsonPath; // path for sets. 
            public string assetPath; //for image and movie files. 
            public string appTriggersPath;
            public List<string> assetBundlePaths;  //TODO 
            public string set; //set -> song -> part -> components -> obs, cameras and scenes. 
            public int audioDevice;
            public int midiOutDevice;
            public bool testing; //flag 
            public bool debug;
            public bool spout; //using spout camera instead of main camera ( not implemented yet )
            public bool usingKinect;
            public bool usingSockets;
            public string spoutSender; //spout output name. 
            public string privateVarsPath; //private configPath;
            public int screenWidth;
            public int screenHeight;
            public bool fullScreen;
            public PrivateVars vars; //loaded privateConfig. 
            public Vector3 kinectBoundsMin;
            public Vector3 kinectBoundsMax;
            public Vector3 kinectSensorOffset;
            public Config()
            {
                screenWidth = 1920;
                screenHeight = 1080;
                fullScreen = true;
                midiOutDevice = 1;
                kinectBoundsMin = new Vector3(-1.0f, 0.0f, -2.0f);
                kinectBoundsMax = new Vector3(1.0f, 2.0f, 2.0f);
                kinectSensorOffset = new Vector3(0.0f, 2.0f, 0.0f);
            }
        }
    }
}