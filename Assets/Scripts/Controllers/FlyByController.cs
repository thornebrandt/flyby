
using flyby.Sets;
using flyby.Tools;
using flyby.Active;
using flyby.Triggers;
using flyby.UI;
using flyby.Core;
using Newtonsoft.Json;
using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
using System.Collections.Generic;

namespace flyby
{
    namespace Controllers
    {
        public class FlyByController : MonoBehaviour
        {
            public string configPath;
            public GameObject camObj;
            public Volume volume;
            public static FlyByController instance { get; private set; } //makes singleton
            [HideInInspector]
            public string jsonPath;
            [HideInInspector]
            public TriggerSourceController[] triggerSources;
            public Set currentSet;
            public Song currentSong;
            public Part currentPart;
            public Part prerenderPart; //hack. 
            [HideInInspector]
            public Vector3 nullVector = new Vector3(-999, -999, -999);
            [HideInInspector]
            public TriggerController triggerController;
            [HideInInspector]
            public StackController stackController; //for any directional looping movement.
            [HideInInspector]
            public ActiveBehaviourController activeBehaviourController; //compiles and assigns active behaviors. 
            [HideInInspector]
            public CameraController cameraController;
            public SocketController socketController;
            public KinectController kinectController;
            public LFOController lfoController;
            public RuntimeAnimatorController animatorController;
            public Config config;
            private AudioController audioController;  //audio controller listens to bass/mid/treble frequences and sends knob events.

            void Start()
            {
                if (currentSet == null)
                {
                    initialize();
                }
            }

            void LateUpdate()
            {
                if (this.currentPart != null)
                {
                    this.currentPart.lateUpdate();
                }
            }

            public void initialize()
            {
                setupDOTween();
                turnOnPhysics();
                //turnOffPhysics();

                configPath = string.IsNullOrEmpty(configPath) ? "config.sample.json" : configPath;
                if (instance == null)
                {
                    instance = this;
                }
                config = loadConfigFromJSON();
                Config.instance = config;
                if (config != null)
                {
                    jsonPath = config.jsonPath;
                    setup();
                }
                else
                {
                    Debug.LogError(configPath + " could not be found");
                }
            }

            public void quickSetup(List<NullObj> _nullObjs)
            {
                //mostly for testing - assumes triggers have already been established. 
                killAll();
                Part tempPart = new Part();
                tempPart.setupAssetController();
                tempPart.parseObjs(_nullObjs, _nullObjs);
                tempPart.setupInstances();

                currentPart = tempPart;
                prerenderPart = currentPart;
                currentPart.setupGroups();
                currentPart.activateObjs();

                if (this.socketController != null)
                {
                    //TODO - this is too hard coupled. make a part changed handler for all triggerSourceControllers
                    this.socketController.PartChangedHandler(tempPart);
                }
                if (this.kinectController != null)
                {
                    //same note as above. 
                    this.kinectController.PartChangedHandler(tempPart);
                }
                if (this.lfoController != null)
                {
                    this.lfoController.PartChangedHandler(tempPart);
                }
                stackController.PartChangedHandler(tempPart);
                prerenderPart = null;
            }

            private void setupDOTween()
            {
                DOTween.SetTweensCapacity(10000, 10000);
                DOTween.defaultRecyclable = true; //experimental
                                                  //DOTween.logB
                                                  //DOTween.logBehaviour = LogBehaviour.Verbose;
            }

            private void turnOffPhysics()
            {
                Physics.autoSimulation = false;
            }

            private void turnOnPhysics()
            {
                Physics.autoSimulation = true;
            }

            public void setup()
            {
                activeBehaviourController = setupActiveBehaviourController();
                cameraController = setupCameraController();
                setupEventListeners();
                loadSet(config.set);
                triggerController = setupTriggerController();
                stackController = setupStackController();
                setupMidiDebugDisplay();
                setupAudioController();

                //TODO - investigate why we are not not calling songChangedHandler? 
                //deleteMeSetSecondaryDisplay();
            }

            // private void deleteMeSetSecondaryDisplay(){
            //     PlayerPrefs.SetInt("UnitySelectMonitor", 1);
            // }

            private void setupMidiDebugDisplay()
            {
                TriggerDebugDisplay midiDebugDisplay = gameObject.GetComponent<TriggerDebugDisplay>();
                if (midiDebugDisplay == null)
                {
                    midiDebugDisplay = gameObject.AddComponent<TriggerDebugDisplay>();
                }
            }

            private void setupAudioController()
            {
                //audio controller listens to frequencies and sends knob triggers. 
                this.audioController = this.GetComponent<AudioController>();
            }

            private void toggleAudio()
            {
                if (this.audioController == null)
                {
                    this.audioController = this.GetComponent<AudioController>();
                }
                if (audioController != null)
                {
                    audioController.toggleAudio();
                }
            }


            private void setupEventListeners()
            {
                TriggerSourceController.OnTriggerEvent += TriggerEventHandler;
                Set.OnSongChanged += SongChangedHandler;
                Song.OnPartChanged += PartChangedHandler;
            }

            private void removeEventListeners()
            {
                TriggerSourceController.OnTriggerEvent -= TriggerEventHandler;
                Set.OnSongChanged -= SongChangedHandler;
                Song.OnPartChanged -= PartChangedHandler;
            }

            private void TriggerEventHandler(TriggerObj triggerObj)
            {
                if (triggerObj.eventName != null)
                {
                    switch (triggerObj.eventName)
                    {
                        case "reset":
                            //same scene unload. 
                            killAll();
                            StartCoroutine(checkForScene(this.currentPart.scene, ""));
                            setup();
                            break;
                        case "toggle_audio":
                            toggleAudio();
                            break;
                        case "":
                            break;
                        default:
                            if (this.audioController != null)
                            {
                                this.audioController.eventHandler(triggerObj);
                            }
                            if (!triggerObj.noteOff)
                            {
                                currentSet.eventHandler(triggerObj.eventName);
                                currentSong.eventHandler(triggerObj.eventName);
                            }
                            currentPart.eventHandler(triggerObj);
                            break;
                    }
                }
            }

            public void killAll()
            {
                removeEventListeners();
                if (this.currentSong != null)
                {
                    this.currentSong.kill();
                }

                // if (this.currentPart != null) {
                //     this.currentPart.kill();
                // }
                if (this.triggerController != null)
                {
                    this.triggerController.kill();
                }
                if (this.stackController != null)
                {
                    this.stackController.kill();
                }

                if (this.cameraController != null)
                {
                    this.cameraController.kill();
                }
            }

            public Config loadConfigFromJSON()
            {
                string configJSON = Json.getJSON(configPath);
                this.config = JsonConvert.DeserializeObject<Config>(configJSON);
                if (!string.IsNullOrEmpty(this.config.privateVarsPath))
                {
                    string varsJSON = Json.getJSON(this.config.privateVarsPath);
                    this.config.vars = JsonConvert.DeserializeObject<PrivateVars>(varsJSON);
                }
                return this.config;
            }

            public Set loadSet(string path)
            {
                currentSet = new Set();
                currentSet.jsonPath = jsonPath;
                currentSet = currentSet.getSetFromJSON(path);
                currentSong = currentSet.loadSong(0);
                return currentSet;
            }

            public void SongChangedHandler(Song song)
            {
                if (this.currentPart != null)
                {
                    this.currentPart.kill();
                }
                this.currentSong = song;
                this.currentPart = currentSong.currentPart;
                this.currentPart.setupCameraController(cameraController);
                this.currentPart.setupGroups();

                if (this.stackController != null)
                {
                    stackController.PartChangedHandler(this.currentPart); //should be solution. 
                }
                if (this.socketController != null)
                {
                    socketController.PartChangedHandler(this.currentPart);
                }
                if (this.kinectController != null)
                {
                    //same note as above. 
                    this.kinectController.PartChangedHandler(this.currentPart);
                }
                if (this.lfoController != null)
                {
                    this.lfoController.PartChangedHandler(this.currentPart);
                }

            }

            public void PartChangedHandler(Part part)
            {
                Part lastPart = this.currentPart;
                this.currentPart.killAdditiveScene(); //dont kill o's just the custom additive scenes. 
                this.currentPart = part;
                this.currentPart.setupCameraController(cameraController);
                this.prerenderPart = null; //clear this once we have an actual part.
                StartCoroutine(checkForScene(lastPart.scene, this.currentPart.scene));
                this.currentPart.setupGroups();
                if (this.socketController != null)
                {
                    //TODO - refactor so that this is part of the trigger source controller. 
                    socketController.PartChangedHandler(part);
                }
                if (kinectController != null)
                {
                    //same note as above. 
                    kinectController.PartChangedHandler(part);
                }
            }

            private IEnumerator checkForScene(string _lastScene, string _scene)
            {
                if (!string.IsNullOrEmpty(_lastScene))
                {
                    AsyncOperation async = SceneManager.UnloadSceneAsync(_lastScene);
                    yield return async;
                }

                if (!string.IsNullOrEmpty(_scene))
                {
                    AsyncOperation async = SceneManager.LoadSceneAsync(_scene, LoadSceneMode.Additive);
                    yield return async;
                    this.currentPart.sceneGameObjects = currentPart.FindSceneObjects(_scene);
                    currentPart.sceneActiveBehaviours = currentPart.FindSceneActiveBehaviours(_scene);
                }
                else
                {
                    yield return 0;
                }
            }

            public TriggerController setupTriggerController()
            {
                this.triggerSources = gameObject.GetComponents<TriggerSourceController>();
                this.triggerController = gameObject.GetComponent<TriggerController>();
                if (this.triggerController == null)
                {
                    if (TriggerController.instance != null)
                    {
                        this.triggerController = TriggerController.instance;
                    }
                }
                if (this.triggerController == null)
                {
                    //trigger controller has not been setup yet.
                    this.triggerController = gameObject.AddComponent<TriggerController>();
                }
                this.triggerController.setup(currentSet.loadTriggers(), triggerSources);
                this.currentPart.setupGroups();
                return this.triggerController;
            }

            public void registerTriggers(List<Trigger> _triggers)
            {
                this.triggerController.registerTriggers(_triggers);
            }

            public ActiveBehaviourController setupActiveBehaviourController()
            {
                activeBehaviourController = gameObject.GetComponent<ActiveBehaviourController>();
                if (activeBehaviourController == null)
                {
                    activeBehaviourController = gameObject.AddComponent<ActiveBehaviourController>();
                }
                return activeBehaviourController;
            }

            public CameraController setupCameraController()
            {
                cameraController = gameObject.GetComponent<CameraController>();
                if (cameraController == null)
                {
                    cameraController = gameObject.AddComponent<CameraController>();
                }
                cameraController.camObj = camObj;
                cameraController.assignCamera();
                return cameraController;
            }

            public StackController setupStackController()
            {
                this.stackController = gameObject.GetComponent<StackController>();
                if (stackController == null)
                {
                    stackController = gameObject.AddComponent<StackController>();
                }
                stackController.setup(this.currentPart);
                return stackController;
            }

            public Obj findObjById(string id)
            {
                //prefer prerendered part. 
                Part _currentPart = this.prerenderPart;
                _currentPart = _currentPart == null ? this.currentPart : this.prerenderPart;
                if (_currentPart != null)
                {
                    foreach (Obj o in _currentPart.objs)
                    {
                        if (o.id == id)
                        {
                            return o;
                        }
                    }
                }
                return null;
            }

            public GameObject findPrefabById(string id)
            {
                // does not matter if active. 
                // used by activeGrass to make a copy of mesh. 
                Obj o = findObjById(id);
                if (o != null && o.instances.Count > 0)
                {
                    return Obj.getPrefabInstance(o.instances[0]);
                }
                return null;
            }


            public GameObject findActiveInstanceById(string id)
            {
                Obj o = findObjById(id);
                if (o != null && o.activeInstanceIndexes.Count > 0)
                {
                    return o.instances[o.activeInstanceIndexes[0]];
                }

                return null;
            }

        }
    }
}