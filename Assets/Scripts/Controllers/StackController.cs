using System.Collections.Generic;
using flyby.Core;
using flyby.Sets;
using UnityEngine;


namespace flyby {
    namespace Controllers {
        public class StackController : MonoBehaviour {


            // takes care of triggering constant stacks. 
            // for stuff like scrolling backgrounds.             
            [HideInInspector]
            public static StackController instance { get; private set; } //makes singleton

            [HideInInspector]
            public List<Obj> stackedObjs;
            private Part currentPart;


            public void setup(Part part) {
                if (this.currentPart == null) {
                    setupEventListeners();
                }
                this.currentPart = part;
                stackedObjs = getStackedObjs(this.currentPart);
            }


            public void LateUpdate() {
                if (this.stackedObjs != null) {
                    foreach (Obj stackedO in this.stackedObjs) {
                        checkStack(stackedO);
                    }
                }
            }

            private void checkStack(Obj o) {
                if (!o.stack.introduced) {
                    o.stack.initializeStack(o);
                } else {
                    o.stack.checkStack(o);
                }
            }

            public void PartChangedHandler(Part part) {
                this.currentPart = part;
                stackedObjs = getStackedObjs(this.currentPart);
            }

            private List<Obj> getStackedObjs(Part part) {
                this.stackedObjs = new List<Obj>();
                foreach (Obj o in part.objs) {
                    if (o.stack != null && o.size != null) {
                        this.stackedObjs.Add(o);
                    }
                }
                return this.stackedObjs;
            }

            private void setupEventListeners() {
                Song.OnPartChanged += PartChangedHandler;
            }

            private void removeEventListeners() {
                Song.OnPartChanged -= PartChangedHandler;
            }

            public void kill() {
                this.currentPart = null;
                removeEventListeners();
            }
        }
    }
}