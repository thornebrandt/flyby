using System.Collections.Generic;
using flyby.Triggers;
using flyby.Sets;
using flyby.Core;
using Newtonsoft.Json;
using System.Linq;
using UnityEngine;
using com.rfilkov.kinect;
using com.rfilkov.components;
using flyby.Tools;

namespace flyby
{
    namespace Controllers
    {
        public class KinectController : TriggerSourceController
        {
            public bool debug; //text on screen. 
            public bool kinectEnabled;
            public KinectManager kinectManager;
            private KinectUserManager kinectUserManager;
            public static KinectController instance { get; private set; } //makes singleton
            public Vector3 kinectSensorOffset = Vector3.zero;
            private Config config;
            public List<Trigger> triggers;
            public List<Body> bodies;
            private int bodyIndex; //for looping through local bodies. 
            private ulong maxKinectUserID; //checking if a new user is actually new. 
            private int newUserIndex; //will always go up. 
            private TriggerObj t; //reusable triggerObj. 
            private int debounceBodiesThreshold = 5;

            //kinect
            private List<ulong> allUsers;
            private List<ulong> kinectUsers;
            private Dictionary<ulong, Body> userBodyDictionary = new Dictionary<ulong, Body>(); //maps avatarController to userId & userIndex.          
            [HideInInspector]
            public Body currentBody; //sloppy - caching a "most recent body".  


            [Tooltip("Humanoid model used for avatar instatiation.")]
            public GameObject avatarModel;

            [Tooltip("Transform to parent avatars to ( for mudbun renderer")]
            public Transform avatarParent;

            [Tooltip("Smooth factor used by the avatar controller.")]
            public float smoothFactor = 10f;

            [Tooltip("If enabled, makes the avatar position relative to this camera to be the same as the player's position to the sensor.")]
            public Camera posRelativeToCamera;

            [Tooltip("Whether the avatar is facing the player or not.")]
            public bool mirroredMovement = true;

            [Tooltip("Whether the avatar is allowed to move vertically or not.")]
            public bool verticalMovement = true;

            [Tooltip("Whether the avatar is allowed to move horizontally or not.")]
            public bool horizontalMovement = true;

            [Tooltip("Whether the avatar's feet must stick to the ground.")]
            public bool groundedFeet = false;

            [Tooltip("Whether to apply the humanoid model's muscle limits or not.")]
            public bool applyMuscleLimits = false;


            public override void Awake()
            {
                base.Awake();
                if (instance == null)
                {
                    instance = this;
                }
            }

            public override void setup(List<Trigger> triggers)
            {
                this.config = Config.instance;
                this.kinectSensorOffset = config.kinectSensorOffset;

                if (config.usingKinect) //experimental - first time using kinect. 
                {
                    this.enabled = true;
                    this.active = true;
                }
                else
                {
                    this.enabled = false;
                    this.active = false;
                }

                if (this.enabled && this.active)
                {
                    this.kinectManager.gameObject.SetActive(true); //experimental.
                    this.kinectManager.enabled = true; //experimental.
                    if (this.bodies == null)
                    {
                        this.bodies = new List<Body>();
                    }
                    this.allUsers = new List<ulong>();
                    this.kinectUsers = new List<ulong>();
                }
                else
                {
                    if (this.kinectManager != null)
                    {
                        this.kinectManager.enabled = false; //experimental.
                        this.kinectManager.gameObject.SetActive(false); //experimental.
                    }
                }

                if (!kinectEnabled)
                {
                    setupKinect();
                }
                if (!kinectEnabled)
                {
                    Debug.Log("kinect is not enabled");
                }
            }

            public void Update()
            {
                if (this.kinectEnabled)
                {
                    CheckStaleBodies();
                    UpdateBodyIndexesForAvatar();
                    UpdateBodyKnobs();
                }
            }

            public void PartChangedHandler(Part part)
            {
                if (part.bodies != null)
                {
                    this.prepareBodiesFromPart(part.bodies);
                    this.maxKinectUserID = 0;
                    this.bodyIndex = -1;
                    //TODO - all existing users that exist need an outro signal, and then body needs to be reset. 
                    this.allUsers = new List<ulong>();
                    //clearing allUsers should take care of the intros for the next part. 
                }
            }

            private void prepareBodiesFromPart(List<Body> bodies)
            {
                // bodies are the data that stores identifying triggers. 
                // instances will not be stored in this controller, but instead respond to events that are in bodies. .
                this.bodies = parseOutDisabledBodies(bodies);
                for (int i = 0; i < this.bodies.Count; i++)
                {
                    this.bodies[i].i = i;
                }
            }

            private List<Body> parseOutDisabledBodies(List<Body> bodies)
            {
                List<Body> enabledBodies = new List<Body>();
                foreach (Body body in bodies)
                {
                    if (!body.disabled)
                    {
                        enabledBodies.Add(body);
                    }
                }
                return enabledBodies;
            }


            private void UpdateBodyKnobs()
            {
                foreach (ulong userId in this.userBodyDictionary.Keys)
                {
                    //TODO - should we make these more effic cient. 
                    Vector3 bodyPosition = kinectManager.GetUserPosition(userId);  //hip position is bodyPosition. 

                    //experimental - returning if we're not getting tracking - don't send knobs. 
                    if (bodyPosition == Vector3.zero)
                    {
                        return;
                    }

                    Vector3 leftHandPosition = kinectManager.GetJointPosition(userId, KinectInterop.JointType.HandLeft);
                    Vector3 rightHandPosition = kinectManager.GetJointPosition(userId, KinectInterop.JointType.HandRight);
                    Vector3 rightHandSpeed = kinectManager.GetJointVelocity(userId, KinectInterop.JointType.HandRight);
                    Vector3 leftFootPosition = kinectManager.GetJointPosition(userId, KinectInterop.JointType.FootLeft);
                    Vector3 rightFootPosition = kinectManager.GetJointPosition(userId, KinectInterop.JointType.FootRight);
                    Vector3 headPosition = kinectManager.GetJointPosition(userId, KinectInterop.JointType.Head);
                    Quaternion headRotation = kinectManager.GetJointOrientation(userId, KinectInterop.JointType.Head, false);

                    this.CheckBounds(bodyPosition); //adjusts min and max of bounds for normalized values. 
                    Vector3 normalizedPosition = Math.NormalizeVector3(bodyPosition, config.kinectBoundsMin, config.kinectBoundsMax);

                    //hand distance relative to hips. 
                    Vector3 rightHandDistance = rightHandPosition - bodyPosition;
                    Vector3 leftHandDistance = leftHandPosition - bodyPosition;
                    Vector3 handMin = new Vector3(-1.1f, -0.7f, -1.1f);
                    Vector3 handMax = new Vector3(1.1f, 1.5f, 1.1f);

                    //leg distance. 
                    Vector3 rightFootDistance = rightFootPosition - bodyPosition;
                    Vector3 leftFootDistance = leftFootPosition - bodyPosition;
                    Vector3 footMin = new Vector3(-1.1f, -1.1f, -1.0f);
                    Vector3 footMax = new Vector3(1.1f, 0.1f, 1.0f);
                    Vector3 normalizedLeftFootPosition = Math.NormalizeVector3(leftFootDistance, footMin, footMax);
                    Vector3 normalizedRightFootPosition = Math.NormalizeVector3(rightFootDistance, footMin, footMax);

                    Vector3 normalizedRightHandPosition = Math.NormalizeVector3(rightHandDistance, handMin, handMax);
                    Vector3 normalizedLeftHandPosition = Math.NormalizeVector3(leftHandDistance, handMin, handMax);

                    Body body = this.userBodyDictionary[userId];

                    if (!string.IsNullOrEmpty(body.xTrigger))
                    {
                        this.SendKnob(body, body.xTrigger, normalizedPosition.x);
                    }

                    if (!string.IsNullOrEmpty(body.yTrigger))
                    {
                        this.SendKnob(body, body.yTrigger, normalizedPosition.y);
                    }

                    if (!string.IsNullOrEmpty(body.zTrigger))
                    {
                        this.SendKnob(body, body.zTrigger, normalizedPosition.z);
                    }

                    if (!string.IsNullOrEmpty(body.rightHandXTrigger))
                    {
                        this.SendKnob(body, body.rightHandXTrigger, normalizedRightHandPosition.x);
                    }

                    if (!string.IsNullOrEmpty(body.rightHandYTrigger))
                    {
                        this.SendKnob(body, body.rightHandYTrigger, normalizedRightHandPosition.y);
                    }

                    if (!string.IsNullOrEmpty(body.leftHandXTrigger))
                    {
                        this.SendKnob(body, body.leftHandXTrigger, normalizedLeftHandPosition.x);
                    }

                    if (!string.IsNullOrEmpty(body.leftHandYTrigger))
                    {
                        this.SendKnob(body, body.leftHandYTrigger, normalizedLeftHandPosition.y);
                    }

                    if (!string.IsNullOrEmpty(body.leftFootXTrigger))
                    {
                        this.SendKnob(body, body.leftFootXTrigger, leftFootDistance.x);
                    }

                    if (!string.IsNullOrEmpty(body.wingspanTrigger))
                    {
                        float handDistance = Vector3.Distance(leftHandPosition, rightHandPosition); //wingspan. 
                        float normalizedWingSpan = Mathf.InverseLerp(0, 1.9f, handDistance);
                        this.SendKnob(body, body.wingspanTrigger, normalizedWingSpan);
                    }

                    if (!string.IsNullOrEmpty(body.feetSpreadTrigger))
                    {
                        float footDistance = Vector3.Distance(leftFootPosition, rightFootPosition); //footspread. 
                        float normalizedFootDistance = Mathf.InverseLerp(0.2f, 0.8f, footDistance);
                        this.SendKnob(body, body.feetSpreadTrigger, normalizedFootDistance);
                    }

                    if (!string.IsNullOrEmpty(body.handAngleTrigger))
                    {
                        float angle = Math.GetAngle(new Vector2(rightHandPosition.x, rightHandPosition.y), new Vector2(leftHandPosition.x, leftHandPosition.y));
                        float normalizedHandAngle = 1.0f - Math.NormalizeAngle(angle, 45, -45);
                        this.SendKnob(body, body.handAngleTrigger, normalizedHandAngle);
                    }

                    if (!string.IsNullOrEmpty(body.headTiltTrigger))
                    {
                        //night of the roxberry  head angle. 
                        float normalizedHeadTilt = 1.0f - Math.NormalizeAngle(headRotation.eulerAngles.z, -70, 70);
                        this.SendKnob(body, body.headTiltTrigger, normalizedHeadTilt);
                    }

                    if (!string.IsNullOrEmpty(body.headNodTrigger))
                    {
                        //up down head angle. 
                        float headNod = 1.0f - Math.NormalizeAngle(headRotation.eulerAngles.x, -60, 70);
                        this.SendKnob(body, body.headNodTrigger, headNod);
                    }

                    if (!string.IsNullOrEmpty(body.headShakeTrigger))
                    {
                        //left and right head angle. 
                        float normalizedHeadShake = (Math.NormalizeAngle(headRotation.eulerAngles.y, -80, 80));
                        this.SendKnob(body, body.headShakeTrigger, normalizedHeadShake);
                    }

                    if (!string.IsNullOrEmpty(body.tallnessTrigger))
                    {
                        float averageFootHeight = (leftFootDistance.y + rightFootDistance.y) / 2;
                        float averageHandHeight = (leftHandDistance.y + rightHandDistance.y) / 2;
                        float tallness = averageHandHeight - averageFootHeight;
                        float normalizedTallness = Mathf.InverseLerp(0.2f, 1.5f, tallness);
                        this.SendKnob(body, body.tallnessTrigger, normalizedTallness);
                    }

                    if (!string.IsNullOrEmpty(body.leanTrigger))
                    {
                        float lean = getLean(userId, bodyPosition);
                        float normalizedLean = 1.0f - Mathf.InverseLerp(-0.38f, 0.38f, lean);
                        this.SendKnob(body, body.leanTrigger, normalizedLean);
                    }
                }
                //TODO - cycle through and check if any knobTriggers need to be sent. 
            }


            private float getBoneWeight(int boneIndex)
            {
                //see KinectInterop.cs for index references. 
                //for isolating weights to check they are behaving correctly. 
                //set every other weight to zero. 
                //for example, only move head.
                float boneWeight = 0;
                float headWeight = 2.5f;
                float coreWeight = 0.75f;
                float limbWeight = 0.25f;

                switch (boneIndex)
                {
                    case 1: //spine1
                    case 2: //spine2
                    case 3: //neck
                    case 4:
                        boneWeight = headWeight;
                        break;
                    case 15: //hipLeft
                    case 19: //hipRight
                        boneWeight = coreWeight;
                        break;
                    case 9: //handleft
                    case 14: //handRight
                        boneWeight = limbWeight;
                        break;
                    case 18: //footleft
                    case 22: //footrighjt
                        boneWeight = limbWeight;
                        break;
                    default:
                        break;
                }
                return boneWeight;
            }

            private float getLean(ulong userId, Vector3 hipPosition)
            {
                float weightedSum = 0f;
                int avgLength = 0;
                int bonesLength = 23;
                for (int i = 0; i < bonesLength; i++)
                {
                    Vector3 bonePosition = kinectManager.GetJointPosition(userId, (KinectInterop.JointType)i);
                    float weight = getBoneWeight(i);
                    if (weight != 0)
                    {
                        weightedSum += (bonePosition.x - hipPosition.x) * weight;
                        avgLength++;
                    }
                }
                return weightedSum / avgLength;
            }

            private bool AreIdentical(List<ulong> list1, List<ulong> list2)
            {
                if (list1.Count != list2.Count)
                {
                    return false;
                }

                // Compare each element
                for (int i = 0; i < list1.Count; i++)
                {
                    if (list1[i] != list2[i])
                    {
                        return false;
                    }
                }

                return true;
            }


            private void SendKnob(Body body, string knobName, float value)
            {
                //TODO - send knob to all triggers that are listening. 
                t = new TriggerObj();
                t.eventName = knobName;
                t.knob = body.i; //not sure why sending this. 
                t.created = Time.time;
                t.source = "kinectKnob";
                t.value = value;
                this.trigger(t);
                this.cacheTriggered(t);
            }

            private void CheckBounds(Vector3 bodyPosition)
            {
                if (bodyPosition.x < config.kinectBoundsMin.x)
                {
                    config.kinectBoundsMin.x = bodyPosition.x;
                }

                if (bodyPosition.x > config.kinectBoundsMax.x)
                {
                    config.kinectBoundsMax.x = bodyPosition.x;
                }

                if (bodyPosition.y < config.kinectBoundsMin.y)
                {
                    config.kinectBoundsMin.y = bodyPosition.y;
                }

                if (bodyPosition.y > config.kinectBoundsMax.y)
                {
                    config.kinectBoundsMax.y = bodyPosition.y;
                }

                if (bodyPosition.z < config.kinectBoundsMin.z)
                {
                    config.kinectBoundsMin.z = bodyPosition.z;
                }

                if (bodyPosition.z > config.kinectBoundsMax.z)
                {
                    config.kinectBoundsMax.z = bodyPosition.z;
                }
            }

            private void UpdateBodyIndexesForAvatar()
            {
                // For some reason the avatarController takes the index instead of the id. 
                //TODO - make your own avatarController and cut out this step ( it only needs the userId ).
                foreach (ulong userId in this.userBodyDictionary.Keys)
                {
                    Body body = this.userBodyDictionary[userId];
                    int bodyIndex = kinectManager.GetUserIndexById(userId);
                    if (body.kinectBodyIndex != bodyIndex && body.avatarController != null)
                    {
                        body.kinectBodyIndex = bodyIndex;
                        body.avatarController.playerIndex = bodyIndex;
                    }
                }
            }

            private void CheckStaleBodies()
            {
                if (this.bodies != null && this.bodies.Count > 0 && this.kinectEnabled)
                {
                    //loop through userBodyDictionary and add to bodiesToRemove and remove all at once. 
                    List<Body> bodiesToRemove = new List<Body>();
                    foreach (ulong userId in this.userBodyDictionary.Keys)
                    {
                        Body body = this.userBodyDictionary[userId];
                        if (Time.time - body.lostTrackingTime > debounceBodiesThreshold)
                        {
                            bodiesToRemove.Add(body);
                        }
                    }

                    foreach (Body body in bodiesToRemove)
                    {
                        this.RemoveUser(body);
                    }

                }
            }

            private string debugUsers(string listName, List<ulong> users)
            {
                //just prints out a string of a list of ulongs. 
                string usersString = listName + ": [";
                foreach (ulong user in users)
                {
                    usersString += user + ", ";
                }
                usersString += "]";
                return usersString;
            }


            private void RemoveUserHandler(ulong removedUserId, int bodyIndex)
            {
                if (userBodyDictionary.ContainsKey(removedUserId))
                {
                    Body body = userBodyDictionary[removedUserId];
                    body.lostTrackingTime = Time.time;
                }
            }

            private void RemoveUser(Body body)
            {
                ulong removedUserID = body.kinectUserId;
                if (!string.IsNullOrEmpty(body.startTrigger))
                {
                    t = new TriggerObj();
                    t.eventName = body.startTrigger;
                    t.note = body.i;
                    t.source = "kinectOff";
                    t.noteOff = true;
                    this.trigger(t);

                    //cleanup even though it's being destroyed. 
                    body.kinectBodyIndex = -1;
                    body.kinectUserId = 999;
                    if (body.avatarController != null)
                    {
                        body.avatarController.playerId = 999;
                        body.avatarController.playerIndex = -1;
                    }
                }

                userBodyDictionary.Remove(removedUserID);

            }

            private void AddUserHandler(ulong kinectUserID, int bodyIndex)
            {
                if (kinectUserID > this.maxKinectUserID || !userBodyDictionary.ContainsKey(kinectUserID))
                {
                    Body body = this.getNextBody();
                    if (!userBodyDictionary.ContainsKey(kinectUserID))
                    {
                        userBodyDictionary.Add(kinectUserID, body);
                        body.kinectUserId = kinectUserID;
                        body.kinectBodyIndex = bodyIndex;
                    }
                    else
                    {
                        userBodyDictionary[kinectUserID] = body;
                    }

                    body.i = this.bodyIndex;
                    body.lostTrackingTime = float.PositiveInfinity;
                    body.kinectUserId = kinectUserID;
                    body.kinectBodyIndex = bodyIndex;
                    this.currentBody = body;

                    if (!string.IsNullOrEmpty(body.startTrigger))
                    {

                        t = new TriggerObj();
                        t.eventName = body.startTrigger;
                        t.source = "kinectOn";
                        t.note = body.i;
                        t.velocity = 128;
                        t.sustain = true;
                        this.trigger(t); //should immediately trigger the setupAvatarController from the activeKinect.
                    }
                    this.maxKinectUserID = kinectUserID;
                }
                else
                {
                    Body body = userBodyDictionary[kinectUserID];
                    body.lostTrackingTime = float.PositiveInfinity;
                }
            }

            private Body getNextBody()
            {
                if (this.bodies == null || this.bodies.Count == 0)
                {
                    Debug.Log("no bodies to assign.");
                    return null;
                }
                this.bodyIndex = (int)Mathf.Repeat(this.bodyIndex + 1, this.bodies.Count);
                return this.bodies[this.bodyIndex];
            }

            public void setupKinect()
            {
                if (kinectManager == null)
                {
                    //attempting to find instance. 
                    kinectManager = KinectManager.Instance;
                }

                if (this.kinectManager == null)
                {
                    Debug.Log("KinectManager not found.");
                }

                this.setupEventListeners();
                this.kinectEnabled = true;
            }

            private void setupEventListeners()
            {
                this.kinectUserManager = this.kinectManager.userManager;
                if (this.kinectUserManager != null)
                {
                    this.kinectUserManager.OnUserAdded.AddListener(this.AddUserHandler);
                    this.kinectUserManager.OnUserRemoved.AddListener(this.RemoveUserHandler);
                }
            }

            private void OnDestroy()
            {
                if (this.kinectUserManager != null)
                {
                    this.kinectUserManager.OnUserAdded.RemoveListener(this.AddUserHandler);
                    this.kinectUserManager.OnUserRemoved.RemoveListener(this.RemoveUserHandler);
                }
            }

            public Body SetupBody(AvatarController avatarController)
            {
                // public function for syncing the avatarController. 
                // if it exists. Sometimes we might only want certain roles. 
                if (avatarController != null && this.currentBody != null)
                {
                    this.currentBody.avatarController = avatarController;
                    avatarController.playerIndex = this.currentBody.kinectBodyIndex; //used by current avatarController. 
                    avatarController.playerId = this.currentBody.kinectUserId;
                    // TODO - if there is mirroredMovement do a 180 on something. 
                    //  if there is a reason to override these, this is where to change it. 
                    avatarController.smoothFactor = smoothFactor;
                    avatarController.posRelativeToCamera = posRelativeToCamera;
                    avatarController.mirroredMovement = mirroredMovement;
                    avatarController.verticalMovement = verticalMovement;
                    avatarController.horizontalMovement = horizontalMovement;
                    avatarController.groundedFeet = groundedFeet;
                    avatarController.applyMuscleLimits = applyMuscleLimits;
                }
                return this.currentBody;
            }

            public void OnGUI()
            {
                if (this.debug)
                {
                    string text = debugUsers("bodies", this.allUsers);
                    int w = Screen.width, h = Screen.height;
                    GUIStyle style = new GUIStyle();
                    Rect rect = new Rect(0, 0, w, h * 2 / 30);
                    style.alignment = TextAnchor.UpperLeft;
                    style.fontSize = h * 4 / 60;
                    style.normal.textColor = new Color(0.0f, 1.0f, 0.5f, 1.0f);
                    GUI.Label(rect, text, style);
                }
            }
        }
    }
}
