using System;
using System.Collections.Generic;
using flyby.Triggers;
using UnityEngine;

namespace flyby
{
    namespace Controllers
    {
        public class KeyboardController : TriggerSourceController
        {
            public List<Trigger> triggers;
            public List<TriggerObj> sustainedKeys;

            void Update()
            {
                checkKeyUpSustainRelease();
                checkKeyboardInput();
            }

            private void checkKeyboardInput()
            {
                if (Input.anyKeyDown)
                {
                    string lastKey = "noDoubles";
                    foreach (Trigger trigger in triggers)
                    {
                        if (!string.IsNullOrEmpty(trigger.key) && Input.GetKeyDown(trigger.key) &&
                        lastKey != trigger.key)
                        {
                            OnKeyPress(trigger.key); //there is another loop inside of here that does the actual test.
                            lastKey = trigger.key;
                            // lastKey prevents double firing in case there are many triggers with same key. 
                            // such as sustain keys.
                            // we used to break, but this prevents two keys at once. 
                        }
                    }
                }
            }

            void checkKeyUpSustainRelease()
            {
                List<TriggerObj> removedKeys = new List<TriggerObj>();
                int i = 0;
                foreach (TriggerObj sustainedTrigger in sustainedKeys)
                {
                    string key = sustainedTrigger.key;
                    if (Input.GetKeyUp(sustainedTrigger.key))
                    {
                        removedKeys.Add(sustainedTrigger);
                        TriggerObj sustainedTriggerObj = sustainedKeys[i];
                        sustainedTriggerObj.noteOff = true;
                        sustainedTriggerObj.source = "keyUp";
                        this.trigger(sustainedTriggerObj);
                    }
                    i++;
                }
                foreach (TriggerObj removedKey in removedKeys)
                {
                    sustainedKeys.Remove(removedKey);
                }
            }


            public override void setup(List<Trigger> triggers)
            {
                this.triggers = triggers;
                sustainedKeys = new List<TriggerObj>();
            }


            public void OnKeyPress(string key)
            {
                foreach (Trigger trigger in triggers)
                {
                    if (trigger.key == key)
                    {
                        TriggerObj triggerObj = new TriggerObj();
                        triggerObj.trigger = trigger;
                        triggerObj.key = key;
                        triggerObj.eventName = trigger.eventName;
                        triggerObj.velocity = 1;  //changed from 128. 
                        triggerObj.note = getNoteFromKey(key);
                        triggerObj.sustain = trigger.sustain;
                        triggerObj.source = "keyDown";
                        //triggerObj.group = trigger.group;
                        if (triggerObj.sustain)
                        {
                            addToSustained(trigger, triggerObj);
                        }
                        this.trigger(triggerObj);
                    }
                }
            }

            public void addToSustained(Trigger trigger, TriggerObj triggerObj)
            {
                triggerObj.created = Time.time;
                sustainedKeys.Add(triggerObj);
            }

            public int getNoteFromKey(string key)
            {
                int note = 1;
                //converting to int is sloppy - just assinging numbers here. 
                //TODO - to manages random values between two notes, as well as sustain. 
                Dictionary<string, int> keys = new Dictionary<string, int>();
                keys.Add("1", 1);
                keys.Add("2", 2);
                keys.Add("3", 3);
                keys.Add("4", 4);
                keys.Add("5", 5);
                keys.Add("6", 6);
                keys.Add("7", 7);
                keys.Add("8", 8);
                keys.Add("9", 9);
                keys.Add("0", 10);
                keys.Add("q", 11);
                keys.Add("w", 12);
                keys.Add("e", 13);
                keys.Add("t", 15);
                keys.Add("y", 16);
                keys.Add("z", 69);
                if (keys.ContainsKey(key))
                {
                    return keys[key];
                }
                return note;
            }
        }
    }
}