using System;
using System.Collections.Generic;
using flyby.Triggers;
using System.Net.Sockets;
using System.IO;
using UnityEngine;

namespace flyby
{
    namespace Controllers
    {
        public class TimeTriggerController : TriggerSourceController
        {
            public List<Trigger> timeTriggers;
            private Config config;
            private float timeStarted;
            private float time;

            public override void setup(List<Trigger> triggers)
            {
                this.timeTriggers = filterTriggers(triggers);
                timeStarted = Time.time;
                if (this.timeTriggers.Count > 0)
                {
                    //start time here. 
                }
            }

            private List<Trigger> filterTriggers(List<Trigger> _triggers)
            {
                this.timeTriggers = new List<Trigger>();
                foreach (Trigger trigger in _triggers)
                {
                    if (trigger.time > -1)
                    {
                        this.timeTriggers.Add(trigger);
                    }
                }
                return this.timeTriggers;
            }

            void Update()
            {
                this.time = setTime();
                checkForTimedTriggers();
            }

            private float setTime()
            {
                this.time = Time.time - timeStarted;
                return this.time;
            }

            private bool triggersLeft(Trigger trigger)
            {
                if (trigger.numRepeats == -1)
                {
                    //default value - repeat indefinitely. 
                    return true;
                }
                if (trigger.numRepeats > 0)
                {
                    if (trigger.timesTriggered < trigger.numRepeats)
                    {
                        return true;
                    }
                }
                trigger.time = float.PositiveInfinity;
                return false;
            }

            private void checkForTimedTriggers()
            {
                if (this.timeTriggers != null)
                {
                    foreach (Trigger trigger in this.timeTriggers)
                    {
                        if (this.time > trigger.time && triggersLeft(trigger))
                        {
                            TriggerObj triggerObj = new TriggerObj();
                            triggerObj.eventName = trigger.eventName;
                            triggerObj.source = "time";
                            triggerObj.sustain = trigger.sustain;
                            triggerObj.noteOff = trigger.noteOff;
                            //triggerObj.group = trigger.group;
                            this.trigger(triggerObj);
                            if (trigger.repeatInterval > 0)
                            {
                                trigger.time = this.time + Tools.Rand.Between(trigger.repeatInterval, trigger.repeatInterval2);
                            }
                            else
                            {
                                trigger.time = float.PositiveInfinity;
                            }
                            trigger.timesTriggered++;
                        }
                    }
                }
            }
        }
    }
}