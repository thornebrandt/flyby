using System;
using System.Collections.Generic;
using flyby.Triggers;
using UnityEngine;

namespace flyby
{
    namespace Controllers
    {
        public class TriggerSourceController : MonoBehaviour
        {

            public delegate void TriggerEvent(TriggerObj triggerObj);
            public static event TriggerEvent OnTriggerEvent;
            public bool active = true;

            public virtual void Awake()
            {
            }

            public virtual void setup(List<Trigger> triggers)
            {
                //TODO - wrap in another function to check for active source before setting up.
            }

            public virtual void trigger(TriggerObj triggerObj)
            {
                if (string.IsNullOrEmpty(triggerObj.eventName))
                {
                    Debug.Log("trigger event name is empty");
                    return;
                }

                if (TriggerController.instance != null)
                {
                    //triggerObj.groupIndex = FlyByController.instance.currentPart.getCurrentGroupIndex();
                    //triggerObj.groupIndex = TriggerController.instance.getNextGroupIndex(triggerObj.eventName);  //not the correct place to get the groupIndex. 
                    //Debug.Log("the group index is: " + triggerObj.groupIndex + " from eventname: " + triggerObj.eventName);
                }

                OnTriggerEvent.Invoke(triggerObj);
            }

            public virtual void cacheTriggered(TriggerObj triggerObj)
            {
                //used for references of instances to get continuous values from knobs. 
                //could be included in this.trigger but don't want to clog this up until I need it. 
                //this is useful for behaviours that don't actively communicate but can keep track of knob position.
                if (TriggerController.instance != null)
                {
                    TriggerController triggerController = TriggerController.instance;
                    triggerController.triggered[triggerObj.eventName] = triggerObj;
                }
            }

        }
    }
}