Shader "Hidden/Shader/VHSRamp"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _VideoTex("Video Texture", 2D) = "black" {}
    }
    HLSLINCLUDE

    #pragma target 4.5
    #pragma only_renderers d3d11 ps4 xboxone vulkan metal switch

    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/FXAA.hlsl"
    #include "../../Resources/TestPrefabs/TestShaders/rampFunction.hlsl"

    struct Attributes
    {
        uint vertexID : SV_VertexID;
		float3 vertex : POSITION;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
        float2 texcoord   : TEXCOORD0;
		float2 texcoordStereo   : TEXCOORD1;
        UNITY_VERTEX_OUTPUT_STEREO
    };

#pragma shader_feature ALPHA_CHANNEL

    TEXTURE2D_X(_MainTex);
    TEXTURE2D(_VideoTex);
	SAMPLER(_Pattern);
    SAMPLER(sampler_MainTex);
    SAMPLER(sampler_linear_repeat);
    #pragma shader_feature FOCUS_CENTER
    float displacementAmount = 0.5;
    float noiseAmount = 0.5;


    Varyings Vert(Attributes input)
    {
        Varyings output;
        UNITY_SETUP_INSTANCE_ID(input);
        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
        output.positionCS = GetFullScreenTriangleVertexPosition(input.vertexID);
        output.texcoord = GetFullScreenTriangleTexCoord(input.vertexID);
        return output;    }


    float3 hash( float3 p )
    {
        p = float3( dot(p,float3(127.1,311.7, 74.7)),
                dot(p,float3(269.5,183.3,246.1)),
                dot(p,float3(113.5,271.9,124.6)));

        return frac(sin(p)*43758.5453123);
    }

    float4 GetRampColor(uint index)
    {
        return _Ramps[index].color;
    }

    float noise(float2 uv)
    {
        float scale = 0.8;
        float2 seed = uv * float2(0.00007 * scale, 0.05 * scale);
        seed.y = (sin(seed.y + _Time.z * 0.0001) * 0.5 + 0.5);

        // Generate random value
        float x = sin(dot(seed.xy, float2(12.9898, 78.244))) * 43758.5453;
        float random = x - floor(x); //fract

        // Quantize into levels to create banding    
        random = floor(random * 200.0) / 400;
        return random;
    }

    float3 HSBtoRGB(float3 c)
    {
        float3 rgb = clamp(abs(fmod(c.x * 6.0 + float3(0.0, 4.0, 2.0), 6.0) - 3.0) - 1.0, 0.0, 1.0);
        rgb = rgb * rgb * (3.0 - 2.0 * rgb);
        return c.z * lerp(float3(1.0, 1.0, 1.0), rgb, c.y);
    }

    float2 RotateUV(float2 uv, float angle)
    {
        float cosAngle = cos(angle);
        float sinAngle = sin(angle);

        // Rotation matrix
        float2x2 rotationMatrix = float2x2(
            cosAngle, -sinAngle,
            sinAngle, cosAngle
        );

        // Apply rotation
        return mul(uv, rotationMatrix);
    }


    float3 RGBtoHSB(float3 rgb)
    {
        float r = rgb.r;
        float g = rgb.g;
        float b = rgb.b;

        float maxC = max(r, max(g, b));
        float minC = min(r, min(g, b));
        float delta = maxC - minC;

        float h = 0.0;
        float s = 0.0;
        float v = maxC; // Brightness

        // Calculate Hue
        if (delta > 0.00001)
        {
            if (maxC == r)
            {
                h = (g - b) / delta;
                if (g < b) h += 6.0;
            }
            else if (maxC == g)
            {
                h = 2.0 + (b - r) / delta;
            }
            else
            {
                h = 4.0 + (r - g) / delta;
            }
            h /= 6.0;
        }

        
        // Calculate Saturation
        if (maxC > 0.0)
        {
            s = delta / maxC;
        }

        return float3(h, s, v);
    }


    float4 CustomPostProcess(Varyings input) : SV_Target
    {
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
        float2 uv = input.texcoord;
        float2 rotated_uv = RotateUV(input.texcoord, 40);


        rotated_uv.x += _Time.x * 8.0;
        float4 black = float4(0.0, 0.0, 0.0, 0.0);
        float4 rampsColor = float4(0, 0, 0, 0); //about to change rampsColro in next "out" line. 
        float4 value = rampFunction_float4_white(uv.y, black, rampsColor);
        //retains rampsColors to apply animated effects.
        float4 rainbow = float4(HSBtoRGB(float3(rotated_uv.x * 0.7, 0.8, 1)), 1.0);
        rainbow = saturate(rainbow + (float4(1.0, 0.7, 1.0, 0.5) - rainbow) * clamp(0.75 - rampsColor.b, -10, 1)) * 2;
        float mask = rampMask(uv.y, hash(uv.y * _Time.z) * 0.05);
        float4 mask4 = float4(mask, mask, mask, 1);
        float2 bar = float2(value.x, value.y * 0.5);
        float2 displacement = float2(bar.r * (displacementAmount * rampsColor.r), 0.0);
        float2 uvs = uv + displacement;
        float4 color =  SAMPLE_TEXTURE2D_X(_MainTex, sampler_MainTex, uvs);

        //vidoe texture. 
        float2 centerScaledUV = uv - 0.5;
        centerScaledUV *= 0.96;
        centerScaledUV += 0.5;
        float4 videoColor = SAMPLE_TEXTURE2D(_VideoTex, sampler_linear_repeat, centerScaledUV) * rainbow;

        float noiseValue = noise(uv);
        float4 noise4 = float4(noiseValue, noiseValue, noiseValue, rampsColor.g) * rainbow * noiseAmount * rampsColor.g; //experimental. 
        float4 darkenedVideoColor = noise4 * videoColor;
        float4 lerpedNoise = lerp(noise4, darkenedVideoColor, 0.6);
        color = clamp(color, 0.0, 1.0);       
        float4 maskedNoise = clamp(lerpedNoise * mask, 0.0, 1.0);
        float4 mixedNoise = clamp(max(maskedNoise, color), 0.0, 1.0);
        float4 mix = clamp((color * (1.0 - mask4)) + mixedNoise, 0.0, 1.0);
        return mix;
    }

    
    ENDHLSL

    SubShader
    {
        Pass
        {
            Name "#Distortion_7#"
            ZWrite Off
            ZTest Always
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off

            HLSLPROGRAM
                #pragma fragment CustomPostProcess
                #pragma vertex Vert
            ENDHLSL
        }
    }
    Fallback Off
}