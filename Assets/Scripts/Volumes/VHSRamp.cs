using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;


//guide for XYZ/RGB

//TODO - these values might need to be adjusted to be more intuitive.

// x -> position of ramp ( left - right ) 
// y -> width of ramp ( 1 is full width, it exxtends from center.  )  0.1 is default. 
// z -> softness of ramp ( eventually )  0 is default. 

//an example of a subtle VHS bottom ramp 
// "knob": {
//     "x": 0.09,
//     "y": 0.8,
//     "z": 0.2
// },
// "knobColor": {
//     "r": 0.3,
//     "g": 0.5,
//     "b": 0.3
// }


// to get full height.  
// "value": {
//     "x": 0.4999,
//     "y": 1,
//     "z": 0,
// }


//R - horizontal displacement
//G - white/static/noise
//B - rainbow  




namespace flyby
{
    [Serializable]
    public struct Ramp
    {
        public float position;
        public float width;
        public float blur;
        public Vector4 color;
    }

    [Serializable]
    public class RampsParameter : VolumeParameter<Ramp[]>
    {
        public RampsParameter(Ramp[] value, bool overrideState = false)
            : base(value, overrideState) { }

        public override void Interp(Ramp[] from, Ramp[] to, float t)
        {
            value = t < 0.5f ? from : to;
        }
    }

    [Serializable, VolumeComponentMenu("Post-processing/Custom/VHSRamp")]
    public sealed class VHSRamp : CustomPostProcessVolumeComponent, IPostProcessComponent
    {

        public TextureParameter videoTexture = new TextureParameter(null);

        public BoolParameter quickTest = new BoolParameter(false, false);

        public ClampedFloatParameter noiseAmount = new ClampedFloatParameter(0f, 0f, 1f, true);
        public ClampedFloatParameter displaceAmount = new ClampedFloatParameter(0.5f, 0f, 1f, true);

        public ClampedFloatParameter position = new ClampedFloatParameter(0.5f, 0f, 1f, true);

        private ComputeBuffer rampBuffer;
        private int numRamps = 32;
        //private int rampIndex = 0;
        public RampsParameter ramps = new RampsParameter(new Ramp[32], true); //triggers represented in floats for shader. 

        Material m_Material;

        public bool IsActive() => m_Material != null && displaceAmount.value > 0f;

        // Do not forget to add this post process in the Custom Post Process Orders list (Project Settings > Graphics > HDRP Global Settings).
        public override CustomPostProcessInjectionPoint injectionPoint => CustomPostProcessInjectionPoint.AfterPostProcess;

        const string kShaderName = "Hidden/Shader/VHSRamp";
        static readonly int _Mask = Shader.PropertyToID("_Mask");
        static readonly int _FadeMultiplier = Shader.PropertyToID("_FadeMultiplier");

        public void setRamps(Ramp[] ramps)
        {
            this.ramps.value = ramps;
        }


        public override void Setup()
        {
            if (Shader.Find(kShaderName) != null)
            {
                m_Material = new Material(Shader.Find(kShaderName));
                setupRamps();
            }
            else
            {
                Debug.LogError($"Unable to find shader '{kShaderName}'.");
            }
        }

        private void setupRamps()
        {
            Ramp[] _ramps = new Ramp[numRamps];
            this.ramps.value = _ramps;
            int bufferSize = sizeof(float) * 7; //3 floats, then assuming that vector 4 is equivalent 4 floats. 
            this.rampBuffer = new ComputeBuffer(numRamps, bufferSize);
            for (int i = 0; i < numRamps; i++)
            {
                ramps.value[i] = new Ramp();
                this.ramps.value[i].position = 0;
                this.ramps.value[i].color = new Vector4(1, 1, 1, 1);
                this.ramps.value[i].width = 0;
                this.ramps.value[i].blur = 0;
            }
        }

        public override void Render(CommandBuffer cmd, HDCamera camera, RTHandle source, RTHandle destination)
        {
            if (m_Material == null)
                return;

            m_Material.SetFloat("displacementAmount", displaceAmount.value);
            m_Material.SetTexture("_VideoTex", videoTexture.value);
            m_Material.SetFloat("noiseAmount", noiseAmount.value);
            m_Material.SetFloat("position", position.value);

            if (this.ramps.value.Length > 0)
            {
                rampBuffer.SetData(ramps.value);
                if (this.quickTest.value)
                {
                    for (int i = 0; i < this.numRamps; i++)
                    {
                        ramps.value[i].position += 0.0001f;
                    }
                }
                m_Material.SetBuffer("_Ramps", rampBuffer);
            }

            cmd.Blit(source, destination, m_Material, 0);
        }

        public override void Cleanup()
        {
            CoreUtils.Destroy(m_Material);
            rampBuffer.Dispose();
        }
    }
}