using UnityEngine;
using RootMotion.Dynamics;

//custom utility function that should reset a puppet if it ever goes crazy. 
public class PuppetMasterResetter : MonoBehaviour
{
    public PuppetMaster puppetMaster;
    public Transform animatedRoot; //this should usually be the hips directly under puppetMaster. 
    public Vector3 spawnPosition;
    public Quaternion spawnRotation;
    public float outOfBoundsThresholdY = -30f;
    public float maxDistanceFromCenter = 90f;

    void Update()
    {
        if (!gameObject.activeInHierarchy)
            return;

        CheckOutOfBounds();
    }

    private void CheckOutOfBounds()
    {
        //more common, just falling. 
        if (animatedRoot.position.y < outOfBoundsThresholdY)
        {
            RespawnPuppet();
        }

        //launched into space very hard. 
        float distanceFromCenter = Vector3.Distance(animatedRoot.position, spawnPosition);
        if (distanceFromCenter > maxDistanceFromCenter)
        {
            RespawnPuppet();
        }

    }

    private void RespawnPuppet()
    {
        puppetMaster.Freeze();
        animatedRoot.position = spawnPosition;
        animatedRoot.rotation = spawnRotation;
        puppetMaster.Kill();
        puppetMaster.Teleport(spawnPosition, spawnRotation, false);
        ResetRigidbodyVelocities();
        puppetMaster.Rebuild();
        puppetMaster.Resurrect();
    }

    private void ResetRigidbodyVelocities()
    {
        foreach (var muscle in puppetMaster.muscles)
        {
            Rigidbody rb = muscle.joint.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }
        }
    }
}