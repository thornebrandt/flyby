// much more simple version of trigger.
// used for describing a trigger. eventually will make a tree of triggers. 
using System.Collections.Generic;

//TODO - possibly confusion between triggers and this. 
//consider renaming to Button. 
namespace flyby
{
    namespace Triggers
    {
        public class TriggerUI
        {
            public int i;
            public int role_i; //role index of parent role. 
            public string display;
            public string style;
            public string eventName;
            public string endEventName; //when the role has been navigated away from - it will send this event.
            public string response; //shows after button click ( will appear over sub menus for type UI ) 
            public string type; //[trigger, ui]  whether it is just changing the UI or sending a trigger. 
            public string emoji;
            public bool ephemeral; //whether to show to user only. 
            public bool hide; //hide buttons after click ( show the response. )
            public List<TriggerUI> triggers; //sub
            public string id; //deprecated - possibly use for preset. 
        }
    }
}