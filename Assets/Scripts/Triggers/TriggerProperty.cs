using UnityEngine;
using flyby.Tools;

namespace flyby
{
    namespace Triggers
    {
        public class TriggerProperty
        {
            public float clampMin; //clamps and holds values. 
            public float clampMax;
            public float rangeMin; //like clamp, but scales range within clamp. 
            public float rangeMax;
            public float max;
            public float min;
            public float start; //default value when started when given zero. 
            public bool started; //determines if it should show start.
            public bool invert;

            public float currentLerpedValue; //experimental.
            public float lerpSpeed;

            public float map(float value)
            {
                float startValue = getStartValue(value);
                float invertedValue = this.invert ? 1.0f - startValue : startValue;
                float mappedValue = Math.Lerp(min, max, invertedValue);
                float rangedValue = getRangedValue(mappedValue);
                this.currentLerpedValue = getLerpedValue(rangedValue);

                float finalValue = this.currentLerpedValue;
                if (clampMin != -999 && finalValue <= clampMin)
                {
                    return clampMin;
                }
                if (clampMax != -999 && finalValue >= clampMax)
                {
                    return clampMax;
                }
                return finalValue;
            }

            private float getLerpedValue(float value)
            {
                if (this.lerpSpeed == -999)
                {
                    return value;
                }
                return Mathf.Lerp(this.currentLerpedValue, value, this.lerpSpeed * Time.deltaTime);
            }

            private float getStartValue(float value)
            {
                // questionably over-engineered.
                // default of value coming in is zero.
                // if this triggerProperty receives a map other than zero, its considered 'started'
                // otherwise, it can be overridden with a start value.
                // maps apply to start value - so think about it as "on or off to start."
                if (started)
                {
                    return value;
                }
                else
                {
                    this.currentLerpedValue = this.start;
                }
                if (value != this.min)
                {
                    started = true;
                    return value;
                }
                return this.start;
            }

            private float getRangedValue(float value)
            {
                if (value <= this.rangeMin)
                {
                    return this.min;
                }
                if (value >= this.rangeMax)
                {
                    return this.max;
                }
                float range = this.rangeMax - this.rangeMin;
                float rangedValue = (value - this.rangeMin) / range;
                return rangedValue;
            }

            public TriggerProperty()
            {
                this.clampMin = -999;
                this.clampMax = -999;
                this.rangeMin = 0;
                this.rangeMax = 1;
                this.min = 0;
                this.max = 1;
                this.start = 0;
                this.lerpSpeed = -999; //no lerp as default. 
            }
        }
    }
}


