using System.Collections;
using System.Collections.Generic;
using flyby.Core;
using flyby.Tools;
using Newtonsoft.Json;
using UnityEngine;

namespace flyby
{
    namespace Triggers
    {
        public class TriggerObj
        {

            //triggerObj is the data from the event. 
            //trigger is a dictionary of calls for parsing from json. 

            public string eventName;
            public string source;
            public int note;
            public string key;
            public int knob;
            public int channel;
            public string message; //full message ( twitch ) 
            public string user; // ( twitch chatter )
            public string avatar; // ( twitch chatter avatar ) //experimental. 
            public float velocity; // ( midi velocity ) 
            public float value;  // ( knob value )
            public float relativeValue; //delta of value.
            public float lastRelativeValue; //storing expansive relative value. 
            public bool sustain;
            public bool noteOff; //for sustained midi notes. 
            public bool triggerEnd; //for manual "off/end" on current obj for -1 startTimes, triggers release for those.
            public float created; //timestamp;
            public float changed; //timestamp for keeping track of throttling.
            public Trigger trigger;  //to check for complex things such as trigger property. 
            public ActiveType activeType; //can override an obj activeType. 
            public int groupIndex; //private. 
            //experimental
            public Vector3 position; //meant for xy pads, bodytracking, etc. 
        }
    }
}


