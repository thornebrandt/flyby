using System.Collections;
using System.Collections.Generic;
using flyby.Core;
using flyby.Tools;
using Newtonsoft.Json;
using UnityEngine;

namespace flyby
{
    namespace Triggers
    {
        public class Trigger
        {
            //trigger is a dictionary of calls. 
            //triggerObj is the data from the event. 
            //triggerUI definies interfaces 
            public string id;
            public string key;
            public int note;
            public int note2; //to make range. 
            public int channel;
            public string eventName;
            public bool sustain;
            public int knob;
            public string user;
            public string chat;
            //experimental - needs debugging. 
            public string group;
            public float time;
            public bool noteOff; //experimental - noteOff for time. 
            public float repeatInterval;
            public float repeatInterval2;
            public float numRepeats; //TODO - convert this into an int.
            public float timesTriggered; //TODO - why are these not ints? should it always fire ?  make sure it rests on load. 
            //TODO - eventually need "timeOff" 

            public Trigger()
            {
                this.note = -1;
                this.note2 = -2;
                this.knob = -1;
                this.time = -1;
                this.numRepeats = -1;
                this.repeatInterval = -1;
                this.repeatInterval2 = -999;
            }

            public override bool Equals(System.Object obj)
            {
                if (obj is null)
                {
                    return false;
                }
                Trigger trigger = obj as Trigger;
                return this.key == trigger.key &&
                    this.eventName == trigger.eventName;
            }

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }

        }

    }
}