using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using UnityEngine.UI;

namespace flyby
{
    namespace Active
    {
        public class ActiveFader : ActiveBehaviour
        {
            //this is fullscreen fadeout overlay. 
            private CanvasGroup canvasGroup;
            //this is for alpha on a UI. 
            // first example of additive tweens.  
            public override void setupActiveTriggers(Obj o)
            {
                // - use triggerColor for colors - this one is for multiple floats. 
                // ( you could hack 4 floats to make colors if you really need to )
                // useful for generic and dynamic but custom behaviours. 
                // to quickly test variations via json. 
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    // this one will probably use a bunch of duplicates of this script. 
                    // probably need find a way to add an array of them with copies of the component. 
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);
                        this.instance = this.gameObject;
                    }
                }
                setupCanvasGroup();
            }

            private void setupCanvasGroup()
            {
                this.canvasGroup = GetComponent<CanvasGroup>();
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                if (this.canvasGroup != null)
                {
                    Kill();
                    triggerController = base.getTriggerController();
                    this.checkForCachedKnobs(this.triggers[0]);
                    foreach (ActiveTrigger at in this.triggers)
                    {
                        at.lerpValue = 0;
                        if (at.end != null)
                        {
                            at.endValue = at.end.getVector3();
                        }
                        if (at.value != null)
                        {
                            at.baseValue = at.value.getVector3();
                        }
                        else
                        {
                            at.baseValue = new Vector3(canvasGroup.alpha, 0);
                        }
                        if (at.start != null)
                        {
                            at.startValue = at.start.getVector3();
                        }
                        else
                        {
                            at.startValue = Vector3.zero;
                        }

                        if (at.startTime <= 0)
                        {
                            at.startValue = at.baseValue;
                        }

                        at.currentValue = at.startValue;
                        canvasGroup.alpha = at.currentValue.x;
                        bool needsSequence = at.startTime > 0 || at.endTime > 0;
                        if (needsSequence)
                        {
                            if (at.start == at.value)
                            {
                                at.startTime = 0;
                            }
                            if (at.sequence != null)
                            {
                                at.sequence.Kill();
                            }
                            at.sequence = DOTween.Sequence();
                            AdditiveIntroSequence(at);
                            if (!triggerObj.sustain && at.endTime > 0)
                            {
                                AdditiveEndSequence(at);
                            }
                        }
                    }
                }
                base.Intro(triggerObj, o);
            }

            public override void Update()
            {
                updateCanvasGroup();
            }

            private void updateCanvasGroup()
            {
                if (this.canvasGroup != null && this.triggers != null)
                {
                    for (int i = 0; i < this.triggers.Count; i++)
                    {
                        ActiveTrigger at = this.triggers[i];

                        if (at.starting)
                        {
                            at.currentValue = Vector3.Lerp(at.startValue, at.baseValue, at.lerpValue);
                            canvasGroup.alpha = at.currentValue.x;
                        }
                        if (at.ending)
                        {
                            at.currentValue = Vector3.Lerp(at.baseValue, at.endValue, 1.0f - at.lerpValue);
                            canvasGroup.alpha = at.currentValue.x;
                        }

                        if (at.target != null && at.animatingTrigger)
                        {
                            //seems wrong we should get rid of currentAnimatedValue and replace with value. 
                            at.animatedValue = Vector3.Lerp(at.currentAnimatedValue, at.targetValue, at.triggerLerpValue);
                            canvasGroup.alpha = at.animatedValue.x;
                        }

                        if (at.knob != null && at.animatingKnob)
                        {
                            float mappedValue = Tools.Math.Lerp(at.knob.x, at.knob.x2, at.knobValue);
                            //double map here - 
                            mappedValue = at.triggerProperty != null ? at.triggerProperty.map(mappedValue) : mappedValue;
                            canvasGroup.alpha = mappedValue;
                            at.animatingKnob = false;
                        }
                    }
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (Application.isPlaying && a != null && canvasGroup != null && a.target != null)
                {

                    Vector3 newTargetValue = a.target.getVector3();

                    if (a.triggerSequence != null)
                    {
                        a.triggerSequence.Kill();
                    }

                    a.targetValue = newTargetValue;
                    a.triggerSequence = DOTween.Sequence();
                    AdditiveTriggerSequence(a, triggerObj.sustain);
                    a.lastTargetValue = newTargetValue;
                    base.Trigger(triggerObj, a, o);
                    if (!triggerObj.sustain)
                    {
                        AdditiveTriggerOffSequence(a);
                    }
                }
            }

            public override void Release()
            {
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.value != null && at.sequence != null && at.end != null)
                    {
                        at.sequence.Kill();
                        this.AdditiveEndSequence(at);
                    }
                }
            }


            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (canvasGroup != null)
                {
                    if (a.offTime < 0)
                    {
                        //it aint coming back. 
                        a.currentAnimatedValue = new Vector3(canvasGroup.alpha, 0, 0);
                    }
                    else
                    {
                        a.currentAnimatedValue = Vector3.zero;
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //handing sustain off
                if (a.triggerSequence != null)
                {
                    a.triggerSequence.Kill();
                }
                a.triggerSequence = DOTween.Sequence();
                a.currentAnimatedValue = Vector3.zero;  //not sure if this should be zero. 
                AdditiveTriggerOffSequence(a);
            }


            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                //looking similar in betweed fader and shader. 
                animatingKnob = true;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        if (at.trigger == knobEvent.eventName)
                        {
                            at.knobValue = knobEvent.value;
                            at.animatingKnob = true;
                        }
                    }
                }
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                //looking similar in betweed fader and shader. 
                this.triggerController = getTriggerController();
                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }

                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        TriggerObj t = checkForCachedKnob(at);
                        float cachedValue = t != null ? t.value : 0;
                        //need to figure out if we want to normalize in the data. it would make this algorithm easier. 
                        at.knobValue = t != null ? t.value : at.knobValue;
                        at.animatingKnob = true;
                    }
                }
            }

        }
    }
}