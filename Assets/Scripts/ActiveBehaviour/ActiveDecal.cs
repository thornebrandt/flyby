using DG.Tweening;
using UnityEngine;
using flyby.Core;
using System.Collections.Generic;
using flyby.Triggers;
using UnityEngine.Rendering.HighDefinition;
using flyby.Controllers;

namespace flyby {
    namespace Active {
        public class ActiveDecal : ActiveBehaviour {
            private FlyByController flyByController;
            private AssetController assetController;
            private DecalProjector decal;
            private Material material;
            private string materialProperty = "_BaseColor";

            private Color originalColor; //original color before knob changes it. 
            private Color startColor; //color to fade in from. 
            private Color baseColor; //color in rest state.
            private Color endColor; //color to fade out to. 
            private Color targetColor; //color for triggers.
            private Color currentColor; //before targetColor lerps over it. 
            private Color animatedColor; //animating between triggers. 
            private Color knobColor; //addition of all knob colors. 
            private Color color; //final color
            private Color killedColor = Color.clear; //color after animation is complete and killed.  
            private Color lastTargetColor; //caching target color for transition.


            // TODO - use value for opacity if colors need to stay the same. 
            // private Vector3 originalValue;
            // private Vector3 value;
            // private Vector3 baseValue;
            // private Vector3 startValue;
            // private Vector3 endValue;
            // private Vector3 currentValue;
            // private Vector3 targetValue;
            // private Vector3 lastTargetValue;
            // private Vector3 animatedValue;
            // private Vector3 knobValue;

            private void OnEnable() {
                this.instance = this.gameObject;
                setupDecal();
            }

            public override void setupActiveTriggers(Obj o) {
                this.o = o;
                this.assetController = getAssetController();

                if (this.triggers == null || this.triggers.Count == 0) {
                    if (o.activeTrigger != null) {
                        this.assignSecondaryTriggers(o.activeTrigger);
                    }
                }

            }

            private void setupDecal() {
                if (this.decal == null) {
                    this.decal = GetComponent<DecalProjector>();
                    //testing decal. 
                }

                if (this.decal != null) {
                    this.material = new Material(this.decal.material);
                    this.decal.material = this.material;
                    this.originalColor = getOriginalMaterialColor(); //cache original color before knob changes it.
                } else {
                    Debug.Log("could not find decal.");
                }
            }

            private Color getOriginalMaterialColor() {
                if (this.material != null && this.originalColor == null) {
                    this.originalColor = this.material.GetColor(materialProperty);
                }

                return this.originalColor;
            }

            public override void Intro(TriggerObj triggerObj, Obj o) {
                if (this.decal != null && this.triggers != null && this.triggers.Count > 0) {
                    Kill();
                    triggerController = base.getTriggerController();
                    ActiveTrigger a = this.triggers[0];
                    lerpValue = 0;
                    this.checkForCachedKnobs(a);
                    this.animatingKnob = true;


                    if (a.endColor != null) {
                        this.endColor = a.endColor.getColor();
                    }

                    if (a.color != null) {
                        this.baseColor = a.color.getColor();
                    } else {
                        this.baseColor = getOriginalMaterialColor();
                    }

                    if (a.startColor != null) {
                        this.startColor = a.startColor.getColor();
                    } else {
                        this.startColor = Color.black;
                    }

                    if (a.startTime <= 0) {
                        this.startColor = this.baseColor;
                    }


                    this.currentColor = this.startColor;
                    this.material.color = this.startColor;

                    if (_sequence != null) {
                        _sequence.Kill();
                    }

                    _sequence = DOTween.Sequence();
                    base.IntroSequence(_sequence, a);

                    if (!triggerObj.sustain && a.endColor != null && a.endTime > 0) {
                        base.EndSequence(_sequence, a);
                    }
                }
            }

            public override void Update() {
                if (animating || animatingTrigger || animatingKnob) {
                    updateDecal();
                    animatingKnob = false;
                }
            }

            private void updateDecal() {
                if (this.material != null) {
                    if (starting) {
                        currentColor = Color.Lerp(startColor, baseColor, lerpValue);
                    }
                    if (ending) {
                        currentColor = Color.Lerp(baseColor, endColor, 1.0f - lerpValue);
                    }

                    animatedColor = currentColor;

                    if (animatingTrigger) {
                        if (targetColor != null) {
                            animatedColor = Color.Lerp(currentColor, targetColor, triggerLerpValue);
                        }
                    }

                    color = Col.AddColors(animatedColor, knobColor);
                    this.material.color = color;
                }
            }

            public override void Release() {
                //handles endColor on sustain for intro trigger. 
                if (_sequence != null) {
                    _sequence.Kill();
                }

                if (this.triggers.Count > 0) {
                    ActiveTrigger a = this.triggers[0];
                    if (a != null && a.endColor != null) {
                        _sequence = DOTween.Sequence();
                        base.EndSequence(_sequence, a);
                    }
                }
            }

            public override void updateKnobColor(Color addedKnobsColor) {
                //base knob calls this after running through cached knobs.
                knobColor = addedKnobsColor;
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o) {
                if (a != null && this.material != null) {
                    if (a.targetColor != null && this.material != null) {
                        Color newTargetColor = a.targetColor.getColor();
                        if (_triggerSequence != null) {
                            _triggerSequence.Kill();
                        }

                        if (a.offTime <= 0 || (lastTrigger != null && lastTrigger != a)) {
                            triggerLerpValue = 0;
                            currentColor = material.GetColor(this.materialProperty);
                        }

                        if (animatingTrigger) {
                            if (lastTargetColor != newTargetColor) {
                                //retrigger
                                starting = false;
                                triggerLerpValue = 0;
                                currentColor = material.GetColor(this.materialProperty);

                            }
                        }

                        _triggerSequence = DOTween.Sequence();
                        targetColor = newTargetColor;
                        lastTargetColor = newTargetColor;

                        TriggerSequence(_triggerSequence, a);

                        if (!triggerObj.sustain) {
                            TriggerOffSequence(_triggerSequence, a);
                        }

                    }
                }
            }

            public override void OnTriggerComplete(ActiveTrigger a) {
                if (instance != null && material != null) {
                    if (a.offTime < 0) {
                        //its not coming back. this is our new home.  
                        currentColor = material.GetColor(materialProperty);
                    } else {
                        //it is going back, come back to home.
                        currentColor = baseColor;
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o) {
                //handling sustain off - move currentColor back to baseColor. 
                if (_triggerSequence != null) {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                currentColor = baseColor;
                TriggerOffSequence(_triggerSequence, a);
            }


            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o) {
                animatingKnob = true;
                knobColor = Color.black;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers) {
                    if (at.knobColor != null) {
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue) : at.knobValue;
                        knobColor = Col.AddColors(knobColor, at.knobColor.lerpColor(mappedValue), BlendMode.Colorize);
                    }
                }
            }

            private AssetController getAssetController() {
                if (this.assetController != null) {
                    return this.assetController;
                }
                if (AssetController.instance != null) {
                    return AssetController.instance;
                }
                return null;
            }


            public override void Kill() {
                if (material != null) {
                    color = killedColor;
                    //obsolete
                    material.SetColor(materialProperty, killedColor);
                    if (this.triggers != null && this.triggers.Count > 0) {
                        if (this.triggers[0].hideOnComplete) {
                            //transform.parent.parent.parent.gameObject.SetActive(false);
                        }
                    }
                }
                base.Kill();
            }


        }

    }
}