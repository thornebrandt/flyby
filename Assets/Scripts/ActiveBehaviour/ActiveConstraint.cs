using DG.Tweening;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using UnityEngine;
using UnityEngine.Animations;
using System.Collections;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveConstraint : AdditiveActiveBehaviour
        {
            public IConstraint constraint;

            //TODO - should probably have two targets to lerp between.
            // after confirmed working. 

            public Transform constraintTarget;
            private string lastTargetName;
            private FlyByController flyByController;

            public override void setupComponent()
            {
                this.flyByController = getFlyByController();

                if (this.constraint == null)
                {
                    this.constraint = GetComponent<IConstraint>();
                }

                if (this.constraint != null && this.flyByController != null)
                {
                    this.hasComponent = true;
                }

                this.constraint.weight = 0;
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {

                //TODO - some way to handle "startString" 
                //meant for fading in between targets? 


                if (this.hasComponent)
                {

                    //setting up target. 
                    string targetName = "";

                    if (!string.IsNullOrEmpty(at.valueString))
                    {
                        targetName = at.valueString;
                    }

                    if (!string.IsNullOrEmpty(at.targetString))
                    {
                        targetName = at.targetString;
                    }

                    if (!string.IsNullOrEmpty(targetName) && targetName != this.lastTargetName)
                    {
                        GameObject instance = this.flyByController.findActiveInstanceById(targetName);
                        if (instance != null)
                        {
                            this.constraintTarget = instance.transform;
                        }
                        else
                        {
                            Debug.Log("activeConstraint canot find: " + targetName);
                        }
                    }

                    if (this.constraintTarget != null)
                    {
                        this.constraint.AddSource(new ConstraintSource()
                        {
                            sourceTransform = this.constraintTarget,
                            weight = 1
                        });
                    }

                }

            }


            private FlyByController getFlyByController()
            {
                //TODO - move this to base class. 

                if (this.flyByController != null)
                {
                    return flyByController;
                }
                if (FlyByController.instance != null)
                {
                    return FlyByController.instance;
                }
                return null;
            }


        }
    }
}