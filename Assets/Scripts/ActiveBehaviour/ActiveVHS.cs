using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using UnityEngine.Rendering;
using System.Collections.Generic;
using UnityEngine.Rendering.HighDefinition;
using VladStorm;

namespace flyby
{
    namespace Active
    {
        public class ActiveVHS : AdditiveActiveBehaviour
        {
            private Volume volume;
            private VHSPro vhs;

            public override void setupComponent()
            {
                volume = GetComponent<Volume>();
                volume.profile.TryGet(out vhs);
                if (vhs == null)
                {
                    Debug.Log("could not find vhs.");
                    return;
                }
                else
                {
                    this.hasComponent = true;
                }
            }

            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                if (this.hasComponent && volume != null && vhs != null)
                {
                    switch (at.propertyName)
                    {
                        case "weight":
                        case "":
                            return new Vector3(volume.weight, 0, 0);
                        case "resolution":
                            return new Vector3(vhs.screenWidth.value, vhs.screenHeight.value, 0);
                    }
                }
                return base.getCurrentValue(at);
            }

            public override void calculateValue(ActiveTrigger at)
            {
                switch (at.propertyName)
                {
                    case "weight":
                    case "resolution":
                    case "":
                        this.multiplyValues(at);
                        break;
                    default:
                        this.addValues(at);
                        break;

                }
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent && this.volume != null && this.vhs != null)
                {
                    switch (at.propertyName)
                    {
                        case "weight":
                        case "":
                            volume.weight = _value.x;
                            break;
                        case "resolution":
                            vhs.screenWidth.value = (int)_value.x;
                            vhs.screenHeight.value = (int)_value.y;
                            break;
                    }
                }
            }
        }
    }
}
