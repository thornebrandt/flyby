using DG.Tweening;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveScale : ActiveBehaviour
        {
            private Vector3 scale; //final calculated scale. 
            private Vector3 baseScale; //value of 
            private Vector3 startScale;
            private Vector3 endScale;
            private Vector3 currentScale; //lerped value between start, base, and end. 
            private Vector3 targetScale; //temporary or sustained scale from triggers.
            private Vector3 animatedScale; //lerped scale value between currentScale and targetScale.
            private Vector3 targetKnobScale; //saved multiplied knob scale.  
            private Vector3 currentKnobScale; //lerped value between start, base, and end.
            private Vector3 lastTargetScale;
            private float knobLerpSpeed = 5;


            public override void setupActiveTriggers(Obj o)
            {
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o.triggerScale != null)
                    {
                        this.assignSecondaryTriggers(o.triggerScale);
                    }
                    this.instance = this.gameObject;
                }
                if (Config.instance.testing)
                {
                    knobLerpSpeed = Mathf.Infinity;  //lots of tests failing because of the lerp speed. 
                }
            }

            public override void Update()
            {
                if (instance != null)
                {
                    if (animating || animatingTrigger || animatingKnob)
                    {
                        updateScale();
                        animatingKnob = false;
                    }
                }
            }

            public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                ActiveScale _activeScale = _instance.GetComponent<ActiveScale>();
                if (_activeScale == null)
                {
                    _activeScale = _instance.AddComponent<ActiveScale>();
                    _activeScale.triggers = new List<ActiveTrigger>();
                    _activeScale.instance = _instance;
                    _activeScale.root = this.root;
                    _behaviours.Add(_activeScale);
                }
                _activeScale.triggers.Add(at);
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                Kill();
                triggerController = base.getTriggerController();
                if (this.triggers == null)
                {
                    this.assignSecondaryTriggers(o.triggerScale);
                }

                ActiveTrigger a = this.triggers.Count > 0 ? this.triggers[0] : new ActiveTrigger();

                if (a != null && !a.disabled)
                {
                    this.multiplier = getMultiplier(a);
                    lerpValue = 0;
                    baseScale = o.scale.getVector3() * multiplier;
                    this.checkForCachedKnobs(a);
                    if (a.value != null)
                    {
                        baseScale = a.value.getVector3();
                    }
                    if (a.start != null)
                    {
                        startScale = a.start.getVector3();
                    }
                    else
                    {
                        startScale = baseScale;
                    }

                    if (a.end != null)
                    {
                        endScale = a.end.getVector3();
                    }

                    currentScale = startScale;
                    if (_sequence != null)
                    {
                        _sequence.Kill(true);
                        if (instance != null)
                        {
                            DOTween.Kill(instance);
                        }
                    }
                    _sequence = DOTween.Sequence();
                    _sequence.SetId("scale" + _i); //can we delete this? seems to be artifact from debugging. 
                    if (a.start == a.value)
                    {
                        a.startTime = 0;
                    }
                    base.IntroSequence(_sequence, a);
                    if (!triggerObj.sustain && a.end != null && a.endTime > 0)
                    {
                        base.EndSequence(_sequence, a);
                    }
                    updateScale(); //prevent first frame popping.
                }
                base.Intro(triggerObj, o);
            }

            public void setBaseScale(Vector3 scale)
            {
                //forcing scale for external tweens. 
                this.instance.transform.localScale = scale;
                this.baseScale = scale;
                this.startScale = scale;
                this.currentScale = scale;
                this.animatedScale = scale;
            }

            public void updateScale()
            {
                if (starting)
                {
                    currentScale = Vector3.Lerp(startScale, baseScale, lerpValue);
                }
                if (ending)
                {
                    currentScale = Vector3.Lerp(baseScale, endScale, 1.0f - lerpValue);
                }
                if (animatingTrigger && targetScale != null)
                {
                    animatedScale = Vector3.Lerp(currentScale, targetScale, triggerLerpValue);
                }
                else
                {
                    animatedScale = currentScale;
                }
                if (this.usingKnobs)
                {
                    currentKnobScale = Vector3.Lerp(currentKnobScale, targetKnobScale, knobLerpSpeed * Time.deltaTime);
                }
                else
                {
                    currentKnobScale = Vector3.one;
                }
                scale = Vector3.Scale(currentKnobScale, animatedScale);
                instance.transform.localScale = scale;
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (a.target != null)
                {
                    Vector3 newTargetScale = a.target.getVector3();
                    if (_triggerSequence != null)
                    {
                        _triggerSequence.Kill();
                    }

                    //fluidity among mulitple triggers.
                    if (a.offTime <= 0 || (lastTrigger != null && lastTrigger != a))
                    {
                        triggerLerpValue = 0;
                        currentScale = instance.transform.localScale;
                    }

                    //check for self randomized position.
                    if (animatingTrigger && lastTargetScale != newTargetScale)
                    {
                        starting = false;
                        triggerLerpValue = 0;
                        currentScale = instance.transform.localScale;
                    }

                    _triggerSequence = DOTween.Sequence();
                    targetScale = a.target.getVector3();
                    TriggerSequence(_triggerSequence, a);
                    lastTargetScale = newTargetScale;
                    base.Trigger(triggerObj, a, o);
                    if (!triggerObj.sustain)
                    {
                        TriggerOffSequence(_triggerSequence, a);
                    }
                }
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                usingKnobs = true;
                animatingKnob = true;
                targetKnobScale = Vector3.one;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue) : at.knobValue;
                        targetKnobScale = Vector3.Scale(targetKnobScale, at.knob.lerpVector3(mappedValue));
                    }
                }
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                //override for quaternion multiplication. 

                this.triggerController = getTriggerController();
                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }

                this.targetKnobScale = Vector3.one;
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        this.usingKnobs = true;
                        TriggerObj t = checkForCachedKnob(at);
                        at.knobValue = t != null ? t.value : 1.0f; //set to one instead of zero. 
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue) : at.knobValue;
                        this.targetKnobScale = Vector3.Scale(this.targetKnobScale, at.knob.lerpVector3(mappedValue));
                    }
                }
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (instance != null)
                {
                    if (a.offTime < 0)
                    {
                        //its not coming back. this is our new home.
                        currentScale = instance.transform.localScale;
                    }
                    else
                    {
                        currentScale = baseScale;
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //CAN THIS BE MOVED TO PARENT? 
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                currentScale = baseScale;
                TriggerOffSequence(_triggerSequence, a);
            }
        }
    }
}