using UnityEngine;
using com.rfilkov.kinect;
using com.rfilkov.components;
using flyby.Controllers;
using flyby.Triggers;
using flyby.Active;
using flyby.Core;
using UnityEngine.UI;
using TMPro;

namespace flyby
{
    namespace Active
    {
        public class ActiveKinect : AdditiveActiveBehaviour
        {
            // TODO - don't require "activeTrigger: {}" to work?  ( use default scale ) 
            public AvatarController avatarController;  //use for a skinned mesh. 

            public TextMeshProUGUI debugText;
            public GameObject debugFollow;

            //scale ( quick MVP for intro/outro. 
            public Vector3 defaultJointScale = new Vector3(0.2f, 0.2f, 0.2f);
            public Vector3 currentJointScale;

            // TODO can we use a map of "childIndex" for intro/activeTrigger effects ? 

            public GameObject anyJointPrefab; //use on all joints. 
            public GameObject pelvisPrefab;
            public GameObject headPrefab;
            public GameObject spinePrefab;
            public GameObject leftHandPrefab;
            public GameObject rightHandPrefab;
            public GameObject leftFootPrefab;
            public GameObject rightFootPrefab;


            public string anyJointPrefabName; //experimental for returning an obj. 
            private GameObject[] jointGOs;
            private float lerpSpeed = 8;
            private bool usingJointGOs; //if any of the joints are being populated. 
            private int numJoints = 23;

            private KinectController kinectController;
            private KinectManager kinectManager;
            private Vector3 kinectSensorOffset;
            private bool kinectEnabled;
            private Body body;

            public override void setupComponent()
            {
                this.setupKinect();
                this.setupKinectAvatar();
                this.setupJointPrefabs(); //populate bones with joint prefabs. 
                this.hasComponent = this.kinectEnabled;
            }

            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                Vector3 value = new Vector3();
                if (this.hasComponent)
                {
                    switch (at.propertyName)
                    {
                        case "jointScale":
                        default:
                            value = this.currentJointScale;
                            break;
                    }
                }
                return value;
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {

                if (this.hasComponent)
                {
                    this.currentJointScale = _value;
                    switch (at.propertyName)
                    {
                        case "jointScale":
                        default:
                            this.currentJointScale = _value;
                            break;
                    }
                }
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                if (!this.hasComponent)
                {
                    return;
                }

                this.Kill(); //experimental - trying to debug lost bodies. 
                this.body = kinectController.SetupBody(this.avatarController);
                this.body.activeKinect = this;
                this.kinectSensorOffset = this.kinectController.kinectSensorOffset;

                foreach (GameObject jointGO in jointGOs)
                {
                    if (jointGO != null)
                    {
                        jointGO.SetActive(true);
                    }
                }

                this.ShowDebug();
                base.Intro(triggerObj, o);
            }

            public override void OnEndComplete(ActiveTrigger a)
            {
                switch (a.propertyName)
                {
                    case "jointScale":
                    default:
                        this.HideJoints();
                        this.HideDebug();
                        break;
                }
            }

            private void HideJoints()
            {
                foreach (GameObject jointGO in jointGOs)
                {
                    if (jointGO != null)
                    {
                        jointGO.SetActive(false);
                    }
                }
            }

            public override void Update()
            {
                if (this.usingJointGOs && this.kinectEnabled)
                {
                    this.updateJointGOs();
                }
                this.updateDebugUI();
                base.Update();
            }

            private void setupKinectAvatar()
            {
                if (this.kinectController != null && this.avatarController != null)
                {
                    //this.avatarController.offsetNode = kinectManager.transform;
                    //this.avatarController.offsetNode = null;//might not need thist after fixing the avatarController offsetNode. 
                }
            }

            public void setupKinect()
            {
                kinectManager = KinectManager.Instance;

                if (kinectManager == null)
                {
                    Debug.Log("KinectManager not found.");
                    return;
                }
                if (avatarController == null)
                {
                    Debug.Log("AvatarController not found.");
                    return;
                }

                this.kinectController = KinectController.instance;
                this.kinectSensorOffset = this.kinectController.kinectSensorOffset;
                this.kinectEnabled = true;
            }

            private void setupJointPrefabs()
            {
                this.jointGOs = new GameObject[this.numJoints];
                for (int i = 0; i < this.numJoints; i++)
                {
                    bool jointSet = false;
                    KinectInterop.JointType jointType = (KinectInterop.JointType)i;
                    switch (jointType)
                    {
                        case KinectInterop.JointType.Pelvis:
                            if (this.pelvisPrefab != null)
                            {
                                this.jointGOs[i] = CreateJointPrefab(this.pelvisPrefab);
                                jointSet = true;
                            }
                            break;
                        case KinectInterop.JointType.Head:
                            if (this.headPrefab != null)
                            {
                                this.jointGOs[i] = CreateJointPrefab(this.headPrefab);
                                jointSet = true;
                            }
                            break;
                        case KinectInterop.JointType.SpineChest:
                            if (this.spinePrefab != null)
                            {
                                this.jointGOs[i] = CreateJointPrefab(this.spinePrefab);
                                jointSet = true;
                            }
                            break;
                        case KinectInterop.JointType.HandLeft:
                            if (this.leftHandPrefab != null)
                            {
                                this.jointGOs[i] = CreateJointPrefab(this.leftHandPrefab);
                                jointSet = true;
                            }
                            break;
                        case KinectInterop.JointType.HandRight:
                            if (this.rightHandPrefab != null)
                            {
                                this.jointGOs[i] = CreateJointPrefab(this.rightHandPrefab);
                                jointSet = true;
                            }
                            break;
                        case KinectInterop.JointType.FootLeft:
                            if (this.leftFootPrefab != null)
                            {
                                this.jointGOs[i] = CreateJointPrefab(this.leftFootPrefab);
                                jointSet = true;
                            }
                            break;
                        case KinectInterop.JointType.FootRight:
                            if (this.rightFootPrefab != null)
                            {
                                this.jointGOs[i] = CreateJointPrefab(this.rightFootPrefab);
                                jointSet = true;
                            }
                            break;
                    }

                    //if joint is not set and anyJointPrefab is set, fill in the joint. 
                    if (!jointSet && this.anyJointPrefab != null)
                    {
                        this.usingJointGOs = true;
                        this.jointGOs[i] = Instantiate(this.anyJointPrefab) as GameObject;
                        this.jointGOs[i].transform.parent = this.gameObject.transform;
                        this.jointGOs[i].transform.localScale = this.defaultJointScale;
                        this.jointGOs[i].SetActive(false);
                    }
                }
            }

            private GameObject CreateJointPrefab(GameObject prefab)
            {
                this.usingJointGOs = true;
                GameObject go = Instantiate(prefab) as GameObject;
                go.transform.parent = this.gameObject.transform;
                go.transform.localScale = this.defaultJointScale;
                go.SetActive(false);
                return go;
            }

            private void updateJointGOs()
            {
                //kinectUserId seems to become out of sync, so we will try to adjust.
                Vector3 bodyPosition = kinectManager.GetUserPosition(this.body.kinectUserId);
                bool bodyIsFound = kinectManager.IsUserTracked(this.body.kinectUserId);

                for (int i = 0; i < this.numJoints; i++)
                {
                    if (this.jointGOs[i] != null)
                    {
                        //continue to transition intro and outro if body is lost. 
                        GameObject jointGO = jointGOs[i];
                        jointGO.transform.localScale = Vector3.Lerp(jointGO.transform.localScale, this.currentJointScale, Time.deltaTime * lerpSpeed);

                        if (bodyIsFound)
                        {
                            KinectInterop.JointType jointType = (KinectInterop.JointType)i;
                            Vector3 targetJointPosition = bodyPosition + kinectManager.GetJointPosition(this.body.kinectUserId, jointType) - kinectSensorOffset;
                            Quaternion targetJointRotation = Quaternion.Euler(0, 180, 0) * kinectManager.GetJointOrientation(this.body.kinectUserId, jointType, false);
                            jointGO.transform.localPosition = Vector3.Lerp(jointGO.transform.localPosition, targetJointPosition, Time.deltaTime * lerpSpeed);
                            jointGO.transform.localRotation = Quaternion.Lerp(jointGO.transform.localRotation, targetJointRotation, Time.deltaTime * lerpSpeed);

                            if (this.debugFollow != null)
                            {
                                if (jointType == KinectInterop.JointType.Pelvis)
                                {
                                    this.debugFollow.transform.localPosition = jointGO.transform.localPosition;
                                }
                            }

                        }
                    }
                }
            }

            private void ShowDebug()
            {
                if (this.debugFollow != null)
                {
                    this.debugFollow.SetActive(true);
                }
            }

            private void HideDebug()
            {
                if (this.debugFollow != null)
                {
                    this.debugFollow.SetActive(false);
                }
            }

            private void updateDebugUI()
            {
                //display ulong kinect bodyUI 
                if (this.debugText != null)
                {
                    if (this.body != null)
                    {
                        this.debugText.text = "ID: " + this.body.kinectUserId + ", i: " + this.body.kinectBodyIndex + ", b: " + this.body.i;
                    }
                    else
                    {
                        this.debugText.text = "nobody";
                    }
                }
            }

            void ChangeColorOfAllChildren(Transform parent, Color color)
            {
                foreach (Transform child in parent)
                {
                    // Check if the child has a MeshRenderer component
                    MeshRenderer meshRenderer = child.GetComponent<MeshRenderer>();
                    if (meshRenderer != null)
                    {
                        // Change the material color
                        foreach (var material in meshRenderer.materials)
                        {
                            material.color = color;
                        }
                    }

                    // Recursively call this method for the child's children
                    if (child.childCount > 0)
                    {
                        ChangeColorOfAllChildren(child, color);
                    }
                }
            }

        }
    }
}