using DG.Tweening;
using UnityEngine;
using UnityEngine.Rendering;
using System;
using UnityEngine.Formats.Alembic.Importer;
using flyby.Core;
using flyby.Triggers;

namespace flyby {
    namespace Active {
        public class ActiveAlembic : ActiveBehaviour {
            private AlembicStreamPlayer abc;
            public float speed = 1.0f;
            private float endTime;
            private float startTime;
            private float lastTime;
            private bool looping;

            public override void setupActiveTriggers(Obj o) {
                if (this.triggers == null || this.triggers.Count == 0) {
                    if (o.activeTrigger != null) {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);

                    }
                }
                this.instance = this.gameObject;
                setupAlembic();
            }

            public override void Intro(TriggerObj triggerObj, Obj o) {
                if (this.abc != null) {
                    Kill();
                    if (o.activeTrigger != null) {
                        ActiveTrigger a = o.activeTrigger;
                        if (a.startTime > 0) {
                            this.speed = this.endTime / a.startTime;
                        }
                    }
                }
            }


            private void setupAlembic() {
                this.abc = GetComponent<AlembicStreamPlayer>();
                if (this.abc != null) {
                    this.startTime = this.abc.StartTime;
                    this.endTime = this.abc.EndTime;
                }
            }

            public override void Update() {
                updateAlembic();
            }

            private void updateAlembic() {
                if (this.abc != null) {
                    float increment = Time.deltaTime * speed;
                    float nextTime = this.abc.CurrentTime + increment;
                    this.abc.CurrentTime = nextTime;
                    if (this.abc.CurrentTime > 0 && lastTime == this.abc.CurrentTime) {
                        //hack to get looping working. 
                        this.abc.CurrentTime = 0;
                    }
                    lastTime = this.abc.CurrentTime;
                }
            }

            public override void Kill() {
                if (this.abc != null) {
                    this.abc.CurrentTime = startTime;
                }
            }

        }
    }
}