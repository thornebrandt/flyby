using UnityEngine;
using System.Collections.Generic;
using flyby.Triggers;
using flyby.Core;
using Drawing;
using DG.Tweening;

namespace flyby
{

    [System.Serializable]
    public class Branch
    {
        public int i;
        public List<Vector3> positions;
        public List<Vector3> targets;
        public List<Vector3> currents;
        public List<Vector3> original; //pre-corrupted. 
        public Branch parent; //not sure we need this as a branch or just a point. 
        public int parentIndex; //the index of the parent's point. 
        public bool mirrored;

        //add setter for last point
        public Vector3 getLastPoint()
        {
            if (this.positions.Count == 0)
            {
                Debug.Log("no points in branch.");
                return new Vector3();
            }

            if (this.positions.Count == 1)
            {
                return this.positions[0];
            }

            return this.positions[this.positions.Count - 1];
        }


        public void setLastPoint(Vector3 value)
        {
            this.positions[this.positions.Count - 1] = value;
        }

        public Vector3 getLastTarget()
        {
            if (this.targets.Count == 0)
            {
                Debug.Log("no targets in branch.");
                return new Vector3();
            }

            if (this.targets.Count == 1)
            {
                return this.targets[0];
            }

            return this.targets[this.targets.Count - 1];
        }

        public void setLastTarget(Vector3 value)
        {
            this.targets[this.targets.Count - 1] = value;
        }

        public Vector3 getLastCurrent()
        {
            if (this.currents.Count == 0)
            {
                Debug.Log("no currents in branch.");
                return new Vector3();
            }

            if (this.currents.Count == 1)
            {
                return this.currents[0];
            }

            return this.currents[this.currents.Count - 1];
        }

        public void setLastCurrent(Vector3 value)
        {
            this.currents[this.currents.Count - 1] = value;
        }
        //constructor for branch
        public Branch()
        {
            this.positions = new List<Vector3>();
            this.targets = new List<Vector3>();
            this.currents = new List<Vector3>();
        }

        public void AddPoint(Vector3 point)
        {
            this.positions.Add(new Vector3(point.x, point.y, point.z));
            this.targets.Add(new Vector3(point.x, point.y, point.z));
            this.currents.Add(new Vector3(point.x, point.y, point.z));
        }

        public void LerpAll(float lerpValue)
        {
            for (int i = 0; i < this.positions.Count; i++)
            {
                this.positions[i] = Vector3.Lerp(this.currents[i], this.targets[i], lerpValue);
            }
        }

        public Vector3 LerpIndex(int index, float lerpValue)
        {
            this.positions[index] = Vector3.Lerp(this.currents[index], this.targets[index], lerpValue);
            return this.positions[index];
        }

        public void LerpLast(float lerpValue)
        {
            Vector3 lastPoint = this.LerpIndex(this.positions.Count - 1, lerpValue);
            Vector3 lastTarget = this.targets[this.targets.Count - 1];
            Vector3 lastCurrent = this.currents[this.currents.Count - 1];
        }

    }

    namespace Active
    {
        public class ActiveLine : AdditiveActiveBehaviour
        {

            private bool lineClassic = true;
            private Material lineMaterial;

            //public List<List<Vector3>> branches; //list of lists of points to represent branches. 
            public List<Branch> branches; //list of lists of points to represent branches.
            public int branchCount = 1; //its created on initialize.


            public List<LineRenderer> lineRenderers;
            Dictionary<int, int> branchMap = new Dictionary<int, int>();

            //colors
            private Color currentColor = new Color(1, 1, 1);
            private Color targetColor;

            //width
            private float currentWidth = 4f;
            private float targetWidth; //not hooked up. 

            private bool hasBranched; //once we have branched, we need to use relative values. 

            private ActiveTrigger currentTrigger; //needs to be killed and replaced.
            private Vector3 lastTargetValue; //for reacting to the last value. 


            //TODO - use struct with 'targetPoint' and parent. 

            public Material GetMaterial()
            {
                return this.lineMaterial;
            }

            public void SetBaseColors(Color _color)
            {
                this.currentColor = _color;
                this.updateComponent();
            }

            private void setupBranches()
            {
                //required to draw. 
                this.branches = new List<Branch>();
                List<Vector3> positions = new List<Vector3>();
                Branch newBranch = new Branch();
                newBranch.positions = positions;
                newBranch.AddPoint(new Vector3());
                this.branches.Add(newBranch);
                this.hasComponent = true;
                LineRenderer lr = this.gameObject.GetComponent<LineRenderer>();
                if (lr != null)
                {
                    //prevent strobing. 
                    lr.enabled = false;
                }

                if (this.lineClassic)
                {
                    //removing previous line renderer.
                    this.initializeLineRendererClassic();
                }
            }

            public override void setupComponent()
            {
                this.hasComponent = true; //experimental
                //this.setupBranches();
            }


            private void initializeLineRendererClassic()
            {
                //starting with just one line renderer. 
                this.lineRenderers = new List<LineRenderer>();
                LineRenderer originalLineRenderer = this.gameObject.GetComponent<LineRenderer>();
                if (originalLineRenderer == null)
                {
                    Debug.Log("there is no line renderer on this object.");
                    //lr = this.gameObject.AddComponent<LineRenderer>();
                }
                else
                {
                    originalLineRenderer.enabled = false; //prevent flickering - we do not show the original line renderer ( this is for creating branches. ) 
                    this.lineMaterial = originalLineRenderer.material; //need to add this to other line renderers going forward. 
                    LineRenderer lr = this.CloneLineRenderer(originalLineRenderer, this.gameObject, 0);
                    this.lineRenderers.Add(lr);
                    lr.SetPositions(this.branches[0].positions.ToArray());
                }
            }

            public override void Update()
            {
                if (this.lineClassic)
                {
                    this.updateLineRendererClassic();
                }
                else
                {
                    using (Draw.ingame.WithLineWidth(this.currentWidth, false))
                    {
                        //Draw.ingame.Line(new Vector3(0, 0, 0), new Vector3(3, 3, 3), this.currentColor);
                        foreach (Branch branch in this.branches)
                        {
                            Draw.ingame.Polyline(branch.positions.ToArray(), this.currentColor);
                        }
                    }
                }
                base.Update();
            }

            private Branch createBranchFrom(Branch lastBranch)
            {
                Vector3 lastPoint = lastBranch.getLastPoint();
                Branch newBranch = new Branch();
                newBranch.parent = lastBranch;
                newBranch.parentIndex = lastBranch.positions.Count - 1;
                newBranch.AddPoint(lastPoint);
                newBranch.i = this.branchCount - 1;
                this.branchCount++;
                return newBranch;
            }

            private void addTargetPointToAllBranches(ActiveTrigger at)
            {
                //TODO - can make a second function that takes a custom list of branches ( such as all odd branches or second half of branches ) 
                for (int i = 0; i < this.branches.Count; i++)
                {
                    Branch branch = this.branches[i];
                    Vector3 lastPoint = branch.getLastPoint(); //the existing point on the branch.
                    branch.AddPoint(lastPoint);
                    Vector3 target = this.getBranchTarget(branch, at, i); //could also be next target ??
                    branch.setLastTarget(target); //last target means the end of the chain. 
                    branch.i = i;
                }

                if (this.lineClassic)
                {
                    this.addBranchesToLineRendererClassic();
                }
            }

            public override void IntroSetup()
            {
                this.setupBranches();
            }


            private void createTargetsForExistingPoints(ActiveTrigger at, List<Branch> branches, bool useParentStructure = false)
            {
                //shift without adding any new points.
                //does not use parent/child relationships. 

                int branchI = 0;
                foreach (Branch branch in branches)
                {

                    if (branch.original == null)
                    {
                        branch.original = new List<Vector3>(branch.positions);
                    }

                    for (int i = 0; i < branch.positions.Count; i++)
                    {

                        if (useParentStructure && i == 0 && branchI == 0) continue; //skip the first point.

                        if (useParentStructure && i <= 1 && branchI == 0)
                        {
                            branch.targets[i] = branch.positions[i]; //freeze the points where they are instead of hard absolute extreme target. 
                        }

                        if (i == 0 && useParentStructure && branch.parent != null && branch.parent.positions.Count > branch.parentIndex)
                        {
                            //parent point. 
                            branch.targets[i] = branch.parent.targets[branch.parentIndex]; //experimental - trying to freeze. 
                        }
                        else
                        {
                            branch.targets[i] = this.getPointTarget(branch.positions[i], at);
                        }
                    }
                    branchI++;

                }



            }

            private void resetToOriginal()
            {

                //loop through and get the relative difference of the points to their previous point. 
                foreach (Branch branch in this.branches)
                {
                    for (int i = 0; i < branch.positions.Count; i++)
                    {
                        if (branch.original != null && branch.original.Count > i)
                        {
                            branch.targets[i] = branch.original[i];

                        }
                        else
                        {
                            //parent exists and it has an original. 
                            if (i == 0 && branch.parent != null && branch.parent.original != null && branch.parent.original.Count > branch.parentIndex)
                            {
                                branch.targets[i] = branch.parent.original[branch.parentIndex];
                                branch.original = new List<Vector3>();
                                branch.original.Add(branch.targets[i]);
                                //TODO - set original at the same time as target? 
                            }
                            else
                            {
                                if (i > 0)
                                {
                                    Vector3 positionDifference = branch.positions[i] - branch.positions[i - 1];
                                    if (branch.original != null && branch.original.Count > i - 1)
                                    {
                                        branch.targets[i] = branch.original[i - 1] + positionDifference;
                                        branch.original.Add(branch.targets[i]);
                                    }
                                    else
                                    {
                                        Debug.Log("no reference to apply position difference of " + positionDifference + " to ");
                                        branch.targets[i] = this.getRandomVector3();
                                    }
                                }
                                else
                                {
                                    //have to do something about getting the parent. possibly in the above block. \
                                    branch.targets[i] = this.getRandomVector3();  //TEMP TO make sure we are not getting stuck.
                                }
                            }
                        }
                    }
                }
            }

            private Vector3 getRandomVector3()
            {
                return new Vector3(Random.Range(-1.5f, 1.5f), Random.Range(0f, 2f), Random.Range(-1.2f, 1.5f));
            }

            private void resetCurrents(List<Branch> branches)
            {
                foreach (Branch branch in branches)
                {
                    for (int i = 0; i < branch.positions.Count; i++)
                    {
                        branch.currents[i] = branch.positions[i];
                    }
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger at, Obj o)
            {
                //good example of why the base.Trigger should be called first ( it gets relative at.targetValue ) 

                if (at.target != null && this.currentTrigger != null && this.currentTrigger.triggerSequence != null && this.currentTrigger.target != null)
                {
                    //interrupting slow draws ( but only for position based. ) 
                    this.currentTrigger.triggerSequence.Kill();
                    this.currentTrigger.lerpValue = 0;
                    this.currentTrigger.animatingTrigger = false;
                    this.resetCurrents(this.branches);
                }

                if (at.target != null)
                {
                    Branch newBranch;

                    switch (at.propertyName)
                    {
                        case "branch_single":
                            //the first branch gets a new point.
                            //Branch lastBranch = this.branches[^1];
                            //get last branch 
                            Branch lastBranch = this.branches[this.branches.Count - 1];
                            newBranch = this.createBranchFrom(lastBranch);
                            this.branches.Add(newBranch);
                            this.addTargetPointToAllBranches(at);
                            break;
                        case "branch":
                        case "branch_mirror_x":
                        case "branch_mirror_y":
                        case "branch_mirror_z":
                            List<Branch> newBranches = createNewBranchesFrom(this.branches);
                            this.branches = this.shuffleInNewBranches(newBranches);
                            this.addTargetPointToAllBranches(at);
                            break;
                        case "center_branch_mirror_x":
                            List<Branch> newBranches2 = createNewBranchesFrom(this.branches);
                            this.branches.AddRange(newBranches2);
                            this.addTargetPointToAllBranches(at);
                            break;
                        case "break":
                            this.createTargetsForExistingPoints(at, this.branches);
                            break;
                        case "shift":
                            bool useParentStructure = true;
                            this.createTargetsForExistingPoints(at, this.branches, useParentStructure);
                            break;
                        case "reset":
                        case "return":
                            this.resetToOriginal();
                            break;
                        // covered by default. 
                        // case "grow":
                        // case "grow_mirror_x":
                        // case "point_mirror_x":
                        // case "newPoint":
                        // case "center_point_mirror_x":
                        // case "absolute":
                        // case "absolute_point":
                        default:
                            //main task is adding lines to the branches. 
                            this.addTargetPointToAllBranches(at);
                            break;
                    }
                    this.currentTrigger = at;
                }

                if (at.targetColor != null)
                {
                    this.targetColor = at.targetColor.getColor();
                }

                base.Trigger(triggerObj, at, o);

            }

            private List<Branch> createNewBranchesFrom(List<Branch> branches)
            {
                List<Branch> newBranches = new List<Branch>();
                foreach (Branch branch in this.branches)
                {
                    Branch newBranch = this.createBranchFrom(branch);
                    newBranches.Add(newBranch);
                }
                //this.branches.AddRange(newBranches);  // this mirrors the entire thing. 
                return newBranches;
            }

            private List<Branch> shuffleInNewBranches(List<Branch> newBranches)
            {
                //this is experimental. 
                List<Branch> shuffledBranches = new List<Branch>();
                for (int i = 0; i < this.branches.Count; i++)
                {
                    shuffledBranches.Add(this.branches[i]);
                    if (i < newBranches.Count)
                    {
                        shuffledBranches.Add(newBranches[i]);
                    }
                }
                return shuffledBranches;
            }


            private void updateLineRendererClassic()
            {
                if (this.lineMaterial != null)
                {
                    this.lineMaterial.color = this.currentColor;
                    this.lineMaterial.SetColor("_UnlitColor", this.currentColor);
                    this.lineMaterial.SetColor("_EmissiveColor", this.currentColor);
                    //this should cover all the materials. 
                    for (int i = 0; i < this.branches.Count; i++)
                    {
                        Branch branch = this.branches[i];
                        LineRenderer lr = this.lineRenderers[i];
                        if (lr != null)
                        {
                            lr.startWidth = this.currentWidth * 0.003f;
                            lr.endWidth = this.currentWidth * 0.003f;
                            lr.positionCount = branch.positions.Count;
                            if (lr.positionCount > 1)
                            {
                                lr.SetPositions(branch.positions.ToArray());
                            }
                        }
                        else
                        {
                            Debug.Log("line renderer " + i + " is null.");
                        }
                    }
                }
            }

            private void addBranchesToLineRendererClassic()
            {
                for (int i = 0; i < this.branches.Count; i++)
                {
                    LineRenderer lr;
                    if (i > 0 && i > this.lineRenderers.Count - 1)
                    {
                        //time to create a new line renderer. 
                        lr = this.CloneLineRenderer(this.lineRenderers[0], this.gameObject, i);
                        lr.material = this.lineMaterial;
                        this.lineRenderers.Add(lr);
                    }
                    else
                    {
                        lr = this.lineRenderers[i];
                    }
                    if (lr != null)
                    {
                        lr.positionCount = this.branches[i].positions.Count;
                    }
                }
            }

            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                //really only makes sense before branches. 
                return this.branches[0].positions[this.branches[0].positions.Count - 1];
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent && this.branches.Count > 0)
                {
                    int i = 0;
                    switch (at.propertyName)
                    {
                        case "break":
                        case "shift":
                        case "reset":
                        case "return":
                            //not growing any points, just animating all of them.
                            foreach (Branch branch in this.branches)
                            {
                                branch.LerpAll(at.triggerLerpValue);
                            }
                            break;
                        default:
                            //usually only growing the last point of each branch. 
                            foreach (Branch branch in this.branches)
                            {
                                if (branch.positions.Count > 1)
                                {
                                    branch.LerpLast(at.triggerLerpValue);
                                }
                                i++;
                            }

                            break;
                    }
                }
            }

            public override void updateColorValue(ActiveTrigger at, Color _color)
            {
                this.currentColor = _color;
            }

            private Vector3 getBranchTarget(Branch branch, ActiveTrigger at, int i)
            {
                Vector3 lastPoint = branch.getLastPoint();
                Vector3 branchTargetValue = at.target.getVector3(); //need to possibly add seed to this. 
                Debug.Log("gettin branch target: " + branchTargetValue);

                switch (at.propertyName)
                {
                    case "absolute":
                    case "absolute_point":
                        //TODO - also liten for absolute property of vec. 
                        if (at.target.x == -999) branchTargetValue.x = lastPoint.x;
                        if (at.target.y == -999) branchTargetValue.y = lastPoint.y;
                        if (at.target.z == -999) branchTargetValue.z = lastPoint.z;

                        return branchTargetValue; //experimental. 
                    case "branch_mirror_x":
                    case "center_branch_mirror_x":
                    case "point_mirror_x":
                    case "grow_mirror_x":
                        if (i % 2 != 0) //isIodd
                        {
                            if (this.lastTargetValue.x != 0)
                            {
                                branchTargetValue = new Vector3(-this.lastTargetValue.x, this.lastTargetValue.y, this.lastTargetValue.z);
                            }
                        }
                        break;
                    case "center_point_mirror_x":
                        bool isLeft = lastPoint.x < 0;
                        if (isLeft)
                        {
                            branchTargetValue = new Vector3(-branchTargetValue.x, branchTargetValue.y, branchTargetValue.z);
                        }
                        break;
                    case "branch_mirror_y":
                        if (i % 2 != 0) //isOdd
                        {
                            //odd. 
                            if (this.lastTargetValue.y != 0)
                            {
                                branchTargetValue = new Vector3(this.lastTargetValue.x, -this.lastTargetValue.y, this.lastTargetValue.z);
                            }
                        }
                        break;
                    case "branch_mirror_z":
                        if (i % 2 != 0) //isOdd
                        {
                            //odd. 
                            if (this.lastTargetValue.z != 0)
                            {
                                branchTargetValue = new Vector3(this.lastTargetValue.x, this.lastTargetValue.y, -this.lastTargetValue.z);
                            }
                        }
                        break;
                    case "rotate_y":
                        //rotating by the y axis. 
                        //x - amount to rotate. 
                        //y - y distance. 
                        //z - scaling of the distance. 

                        float scale = at.target.z > 0 ? at.target.z : 1;  //don't want to cause this to be zero. 
                        Quaternion rotation = Quaternion.Euler(0, at.target.x, 0);
                        Vector3 rotatingPoint = rotation * (lastPoint * scale);
                        rotatingPoint.y += at.target.y;
                        return rotatingPoint;
                }





                if (at.target.relative || branch.positions.Count > 1)
                {
                    //we have to switch to relative. 
                    this.lastTargetValue = branchTargetValue; //side effect for storing. 
                    return lastPoint + branchTargetValue;
                }
                else
                {
                    this.lastTargetValue = at.targetValue;
                    return at.targetValue; //original
                }

            }


            private Vector3 getPointTarget(Vector3 point, ActiveTrigger at)
            {
                Vector3 pointTargetValue = at.target.getVector3(); //need to possibly add seed to this. 

                if (at.target.relative)
                {
                    return point + pointTargetValue;
                }
                else
                {
                    return at.targetValue; //original
                }
            }

            public LineRenderer CloneLineRenderer(LineRenderer source, GameObject parent, int index)
            {
                // Copy general settings
                GameObject child = new GameObject();
                child.name = "from" + parent.name + "_LineRenderer_" + index;
                child.transform.SetParent(parent.transform, false);
                LineRenderer new_lr = child.AddComponent<LineRenderer>();
                if (source == null)
                {
                    Debug.Log("line renderer to clone from is null.");
                    return new_lr;
                }
                new_lr.useWorldSpace = source.useWorldSpace;
                new_lr.startWidth = source.startWidth;
                new_lr.endWidth = source.endWidth;
                new_lr.numCapVertices = source.numCapVertices;
                new_lr.numCornerVertices = source.numCornerVertices;
                new_lr.alignment = source.alignment;
                new_lr.textureMode = source.textureMode;
                new_lr.material = source.material;
                return new_lr;
            }

            public override void Kill()
            {
                if (this.lineClassic)
                {

                    foreach (LineRenderer lr in this.lineRenderers)
                    {
                        if (lr != null)
                        {
                            if (lr.gameObject != this.gameObject)
                            {
                                GameObject.Destroy(lr.gameObject);
                            }
                            else
                            {
                                Destroy(lr);
                            }
                        }
                    }
                }

                this.lineRenderers = new List<LineRenderer>();
                this.branches = new List<Branch>();
                base.Kill();
            }
        }
    }
}