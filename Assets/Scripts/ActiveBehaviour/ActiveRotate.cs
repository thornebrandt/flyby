using DG.Tweening;
using flyby.Core;
using flyby.Triggers;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveRotate : ActiveBehaviour
        {
            //rotate is different from rotation because rotate is a continiuous motion.
            //rotation is a distinct quaternion value. 
            //rotation vs rotate is similar to the difference between position and speed. 

            public Vector3 rotate; //final calculated rotate
            private Vector3 baseRotate;
            private Vector3 startRotate;
            private Vector3 endRotate;
            private Vector3 currentRotate;
            private Vector3 targetRotate;
            private Vector3 animatedRotate;
            private Vector3 knobRotate;
            private Vector3 lastTargetRotate;
            private List<TriggerObj> knobs;
            private Vector3 center;
            private bool rotateAroundCenter;

            public override void Update()
            {
                if (instantiated)
                {
                    updateRotate();
                    animatingKnob = false;
                }
            }

            public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                ActiveRotate _activeRotate = _instance.GetComponent<ActiveRotate>();
                if (_activeRotate == null)
                {
                    _activeRotate = _instance.AddComponent<ActiveRotate>();
                    _activeRotate.triggers = new List<ActiveTrigger>();
                    _activeRotate.instance = _instance;
                    _activeRotate.root = this.root;
                    _behaviours.Add(_activeRotate);
                }
                _activeRotate.triggers.Add(at);
            }


            public override void updateKnobValue(Vector3 addedKnobsValue)
            {
                //base class calls this after running through cached knobs. 
                knobRotate = addedKnobsValue;
            }



            private Vector3 getCenter()
            {
                //obselete, if we're gonna use this it has to be every frame.


                if (this.center != null && this.center != Vector3.zero)
                {
                    return this.center;
                }
                if (this.instance != null)
                {
                    this.center = Tools.Space.getCenter(instance);
                    if (this.o.triggerScale != null)
                    {
                        this.center = Vector3.Scale(this.center, this.o.triggerScale.value.getVector3());
                    }
                }
                return this.center;
            }


            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                this.o = o;
                Kill();
                triggerController = base.getTriggerController();
                if (this.triggers == null)
                {
                    this.assignSecondaryTriggers(o.triggerRotate);
                }
                ActiveTrigger a = this.triggers.Count > 0 ? this.triggers[0] : new ActiveTrigger();
                if (a != null)
                {
                    this.multiplier = getMultiplier(a);
                    lerpValue = 0;
                    this.checkForCachedKnobs(a);

                    if (a.value != null)
                    {
                        baseRotate = a.value.getVector3() * multiplier;
                        currentRotate = baseRotate;
                    }
                    if (a.start != null)
                    {
                        startRotate = a.start.getVector3() * multiplier;
                        currentRotate = startRotate;
                        this.rotateAroundCenter = (a.startString == "center");
                    }
                    else
                    {
                        startRotate = Vector3.zero;
                        currentRotate = startRotate;
                    }
                    if (a.end != null)
                    {
                        endRotate = a.end.getVector3() * multiplier;
                    }
                    if (_sequence != null)
                    {
                        _sequence.Kill(true);
                    }
                    _sequence = DOTween.Sequence();
                    _sequence.SetId("rotate" + _i);
                    if (a.start == a.value)
                    {
                        a.startTime = 0;
                    }
                    base.IntroSequence(_sequence, a);
                    if (!triggerObj.sustain && a.end != null && a.endTime > 0)
                    {
                        base.EndSequence(_sequence, a);
                    }

                    updateRotate(); //prevent first frame popping.
                }
                base.Intro(triggerObj, o);
            }



            public void updateRotate()
            {
                if (starting)
                {
                    currentRotate = Vector3.Lerp(startRotate, baseRotate, lerpValue);
                }
                if (ending)
                {
                    currentRotate = Vector3.Lerp(baseRotate, endRotate, 1.0f - lerpValue);
                }
                if (animatingTrigger && targetRotate != null)
                {
                    animatedRotate = Vector3.Lerp(currentRotate, targetRotate, triggerLerpValue);
                }
                else
                {
                    animatedRotate = currentRotate;
                }
                rotate = knobRotate + animatedRotate;
                if (rotateAroundCenter)
                {
                    center = Tools.Space.getCenter(instance);
                    instance.transform.RotateAround(center, rotate, Time.deltaTime * 90 * multiplier);
                }
                else
                {
                    instance.transform.Rotate(rotate * Time.deltaTime * 180.0f);
                }
            }
            public override void OnIntroComplete(ActiveTrigger a)
            {
                this.rotateAroundCenter = (a.valueString == "center");
            }

            public override void OnEnd(ActiveTrigger a)
            {
                this.rotateAroundCenter = (a.endString == "center");
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (a.target != null)
                {
                    this.multiplier = getMultiplier(a);
                    Vector3 newTargetRotate = a.target.getVector3() * multiplier;
                    if (_triggerSequence != null)
                    {
                        _triggerSequence.Kill();
                    }

                    if (a.offTime < 0)
                    {
                        //not coming back.
                        triggerLerpValue = 0;
                        currentRotate = animatedRotate;
                    }

                    if (animatingTrigger && lastTargetRotate != newTargetRotate)
                    {
                        starting = false;
                        triggerLerpValue = 0;
                        currentRotate = animatedRotate;
                        //should set to 
                    }
                    _triggerSequence = DOTween.Sequence();
                    targetRotate = newTargetRotate;
                    TriggerSequence(_triggerSequence, a, triggerObj.sustain);
                    lastTargetRotate = newTargetRotate;
                    base.Trigger(triggerObj, a, o);
                    if (!triggerObj.sustain && a.offTime >= 0)
                    {
                        //normal one hit fire without sustain or hold. 
                        TriggerOffSequence(_triggerSequence, a);
                    }
                    this.rotateAroundCenter = (a.targetString == "center");
                }
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true;
                knobRotate = Vector3.zero;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue) : at.knobValue;
                        knobRotate += at.knob.lerpVector3(mappedValue) * multiplier;
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //handling sustain off - move currentColor back to baseColor. 
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                currentRotate = baseRotate;
                TriggerOffSequence(_triggerSequence, a);
            }


            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (instance != null)
                {
                    if (a.offTime < 0)
                    {
                        this.currentRotate = this.targetRotate;
                        this.rotateAroundCenter = (a.targetString == "center");
                    }
                    else
                    {
                        currentRotate = baseRotate;
                    }
                }

            }

            public override void Kill()
            {
                //clear out values. 
                rotate = Vector3.zero;
                startRotate = Vector3.zero;
                endRotate = Vector3.zero;
                currentRotate = Vector3.zero;
                targetRotate = Vector3.zero;
                animatedRotate = Vector3.zero;
                //knobSpeed = Vector3.zero; //we want to cache knobs, actually. 
                lastTargetRotate = Vector3.zero;
                base.Kill();
            }
        }
    }
}