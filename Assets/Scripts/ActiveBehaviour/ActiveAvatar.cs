using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;

//not to be confused with bodytracking kinect avatar - this is for image - discord user avatar.
//listens for avatar as part of triggerObj. 
namespace flyby
{
    namespace Active
    {
        public class ActiveAvatar : ActiveBehaviour
        {
            public Renderer rend;
            public Texture2D defaultTexture; //TODO - make an array. 
            private MaterialPropertyBlock mpb;
            public string propertyName = "_UnlitColorMap";
            private AssetController assetController;
            private string avatarToLoad = ""; //turn this on. 
            private bool loaded, downloading;

            // trying to turn this off - not sure what works and what doesn;'t...
            public override void setupActiveTriggers(Obj o)
            {
                this.assetController = this.getAssetController();
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                base.Intro(triggerObj, o);

                //clear textures  in case there is a new user. 
                this.avatarToLoad = "";
                this.loaded = false;
                this.downloading = false;

                if (rend == null)
                {
                    rend = GetComponent<Renderer>();
                }

                if (triggerObj.avatar != null && rend != null)
                {
                    this.avatarToLoad = triggerObj.avatar;
                    setDefaultTexture();
                }
            }

            private void setDefaultTexture()
            {
                if (this.defaultTexture != null)
                {
                    this.SetTexture(this.defaultTexture);
                }
                else
                {
                    rend.gameObject.SetActive(false);
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (!loaded && triggerObj.avatar != null)
                {
                    this.avatarToLoad = triggerObj.avatar;
                }
                base.Trigger(triggerObj, a, o);
            }

            public override void TriggerKnob(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (!loaded && triggerObj.avatar != null)
                {
                    this.avatarToLoad = triggerObj.avatar;
                }
                base.TriggerKnob(triggerObj, a, o);
            }

            public override void Update()
            {
                if (!this.loaded)
                {
                    if (!this.downloading && !string.IsNullOrEmpty(this.avatarToLoad))
                    {
                        if (this.gameObject.activeInHierarchy && this.assetController != null)
                        {
                            this.StartDownloadingAvatar();
                        }
                    }
                }
                base.Update();
            }

            private void SetTexture(Texture2D texture)
            {
                if (rend != null)
                {
                    rend.material.SetTexture(this.propertyName, texture);
                }
            }

            private void StartDownloadingAvatar()
            {
                this.downloading = true;
                if (this.assetController.IsTextureLoaded(this.avatarToLoad))
                {
                    this.loaded = true;
                    this.downloading = false;
                    rend.gameObject.SetActive(true);
                    this.SetTexture(this.assetController.textureLibrary[this.avatarToLoad]);
                    return;
                }


                StartCoroutine(this.DownloadAvatar(this.avatarToLoad, (Texture2D texture) =>
                {
                    this.loaded = true;
                    this.downloading = false;
                    rend.gameObject.SetActive(false);
                    this.SetTexture(texture);
                    this.assetController.CacheTexture(this.avatarToLoad, texture);
                    this.SetTexture(texture);
                }));
            }

            public IEnumerator DownloadAvatar(string filePath, System.Action<Texture2D> onComplete)
            {
                Texture2D tex = null;

                if (filePath.StartsWith("http://") || filePath.StartsWith("https://"))
                {
                    using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(filePath))
                    {
                        yield return request.SendWebRequest();

                        if (request.result != UnityWebRequest.Result.Success)
                        {
                            Debug.Log("error loading avatar: " + request.error);
                            onError(this.avatarToLoad);
                        }
                        else
                        {
                            tex = ((DownloadHandlerTexture)request.downloadHandler).texture;
                            onComplete(tex);
                        }
                    }
                }
                else
                {
                    onError(this.avatarToLoad);
                    //onComplete(null);
                }
            }

            private void onError(string avatarToLoad)
            {
                Debug.Log("avatar failed to load: " + avatarToLoad);
                this.rend.gameObject.SetActive(false); //hide the damn avatar texture. 
            }

            private AssetController getAssetController()
            {
                //TODO - put in utility function. 
                if (this.assetController != null)
                {
                    return this.assetController;
                }
                if (AssetController.instance != null)
                {
                    return AssetController.instance;
                }
                return null;
            }
        }

    }
}