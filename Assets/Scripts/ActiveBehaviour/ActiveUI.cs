using System.Collections;
using System.Collections.Generic;
using flyby.Active;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ActiveUI : AdditiveActiveBehaviour
{

    public string suffix;
    public string prefix;

    public Text text;
    public TextMeshProUGUI textPro;
    public string numberFormatting = "F0";
    private float targetTextNumber;
    private float currentTextNumber;
    private string currentText;


    public RectTransform rectTransform;
    //until we can rewrite this to use floats. ( after january probably ) 
    private Vector3 targetRectPosition;
    private Vector3 currentRectPosition; //for lerping knobs. 
    private float knobLerpSpeed = 5;
    private bool hasText;

    public override void Update()
    {
        base.Update();
        if (this.rectTransform != null)
        {
            updateRectTransform();
        }

        if (this.hasText)
        {
            updateText();
        }
    }

    private void updateRectTransform()
    {
        currentRectPosition = Vector3.Lerp(currentRectPosition, targetRectPosition, Time.deltaTime * knobLerpSpeed);
        rectTransform.anchoredPosition = currentRectPosition;
    }

    private void updateText()
    {
        currentTextNumber = Mathf.Lerp(currentTextNumber, targetTextNumber, Time.deltaTime * knobLerpSpeed);

        if (numberFormatting.Contains("F"))
        {
            currentText = prefix + currentTextNumber.ToString(numberFormatting) + suffix;
        }

        if (numberFormatting.Contains("D"))
        {
            currentText = prefix + ((int)currentTextNumber).ToString(numberFormatting) + suffix;
        }


        if (this.text != null)
        {
            this.text.text = currentText;
        }
        if (this.textPro != null)
        {
            this.textPro.text = currentText;
        }
    }

    public override Vector3 getCurrentValue(ActiveTrigger at)
    {
        if (this.hasComponent)
        {
            switch (at.propertyName)
            {
                case "number":
                case "":
                    return new Vector3(currentTextNumber, 0, 0);
                case "position":
                    return currentRectPosition;
            }
        }
        return currentRectPosition;
    }

    public override void setupComponent()
    {
        //setting this up manual for now. 

        if (this.text != null)
        {
            this.hasText = true;
            this.hasComponent = true;
        }

        if (this.textPro != null)
        {
            this.hasText = true;
            this.hasComponent = true;
        }


        if (this.rectTransform != null)
        {
            this.hasComponent = true;
        }
    }

    public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
    {
        ActiveUI activeUI = _instance.GetComponent<ActiveUI>();
        if (activeUI == null)
        {
            activeUI = _instance.AddComponent<ActiveUI>();
        }
        activeUI.triggers = new List<ActiveTrigger>();
        activeUI.instance = _instance;
        activeUI.root = this.root;
        _behaviours.Add(activeUI);
        activeUI.triggers.Add(at);
    }

    public override void updateValue(ActiveTrigger at, Vector3 _value)
    {
        if (hasText)
        {
            switch (at.propertyName)
            {
                case "number":
                case "":
                    this.targetTextNumber = _value.x;
                    break;
            }
        }

        if (this.rectTransform != null)
        {
            switch (at.propertyName)
            {
                case "position":
                    this.targetRectPosition = _value;
                    break;
            }
        }
    }

}
