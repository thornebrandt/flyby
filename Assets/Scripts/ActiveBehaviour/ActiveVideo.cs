using DG.Tweening;
using UnityEngine;
using UnityEngine.Video;
using flyby.Core;
using System.Collections.Generic;
using flyby.Triggers;
using flyby.Controllers;

namespace flyby {
    namespace Active {
        public class ActiveVideo : ActiveBehaviour {
            private FlyByController flyByController;
            private Renderer rend;
            private VideoPlayer videoPlayer;
            private AssetController assetController;
            public VideoClip startVideoClip;
            public VideoClip videoClip;
            public VideoClip endVideoClip;
            public List<RenderTexture> renderTextures; //pool of render textures.
            public VideoClip targetVideoClip;
            private VideoClip originalVideoClip;
            private VideoClip currentVideoClip;
            private VideoClip lastTargetVideoClip;
            private Material material;
            private bool playing;

            private void OnEnable() {
                this.instance = this.gameObject;
                setupVideoPlayer();
                this.videoPlayer.loopPointReached += handleVideoEndFrame;

            }

            private void OnDisable() {
                videoPlayer.loopPointReached -= handleVideoEndFrame;
            }

            public override void setupActiveTriggers(Obj o) {
                this.o = o;
                rend = GetComponent<Renderer>();
                this.assetController = getAssetController();

                if (this.triggers == null || this.triggers.Count == 0) {
                    if (o.activeTrigger != null) {
                        this.assignSecondaryTriggers(o.activeTrigger);
                    }
                }
            }


            private void setupVideoPlayer() {
                if (this.videoPlayer == null) {
                    this.videoPlayer = GetComponent<VideoPlayer>();
                }
                if (this.videoPlayer == null && this.instance != null) {
                    this.videoPlayer = this.instance.AddComponent<VideoPlayer>();
                }
                this.videoPlayer.renderMode = VideoRenderMode.RenderTexture;
                this.videoPlayer.targetTexture = this.renderTextures[0];
                this.videoPlayer.isLooping = true;
                this.material = this.GetComponent<Renderer>().material;
            }

            private void handleVideoEndFrame(VideoPlayer vp) {
                ActiveTrigger a = this.triggers[0];
                if (starting) {
                    animating = false;
                    introduced = true;
                    starting = false;
                    fresh = false;
                    OnIntroComplete(a);
                }
            }

            private RenderTexture getNextPooledRenderTexture(Obj o) {
                //Debug.Log("renderTextures: " + this.renderTextures.Count);
                int texture_i = (int)Mathf.Repeat(o._i, this.renderTextures.Count);
                return this.renderTextures[texture_i];
            }

            public override void Update() {
                if (this.videoPlayer != null) {
                    if (this.videoPlayer.isPlaying) {
                        instance.transform.localScale = Vector3.one; //hack to prevent popping video.
                    }
                }
            }

            public override void Intro(TriggerObj triggerObj, Obj o) {
                //TODO - needs a media controller for storing textures and videos. 
                this.o = o;
                Kill();
                this.triggerController = base.getTriggerController();
                this.flyByController = getFlyByController();
                this.videoPlayer.targetTexture = getNextPooledRenderTexture(o);
                this.material.SetTexture("_UnlitColorMap", this.videoPlayer.targetTexture);
                this.instance.transform.localScale = Vector3.zero; //hack to prevent popping video. 

                ActiveTrigger a = this.triggers[0];
                if (a != null && videoPlayer != null) {
                    lerpValue = 0;
                    if (this.videoClip != null) {
                        currentVideoClip = this.videoClip;
                    }

                    if (this.startVideoClip != null && a.startTime > 0) {
                        currentVideoClip = this.startVideoClip;
                    }

                    if (a.startMedia != null && a.startMedia.repeat > 0) {
                        currentVideoClip = this.startVideoClip;
                        starting = true;
                    }

                    if (_sequence != null) {
                        _sequence.Kill();
                    }

                    _sequence = DOTween.Sequence();

                    playVideoClip(currentVideoClip);

                    base.IntroSequence(_sequence, a);
                    if (!triggerObj.sustain && endVideoClip != null && a.endTime > 0) {
                        base.EndSequence(_sequence, a);
                    }
                    base.Intro(triggerObj, o);
                }
            }

            private void playVideoClip(VideoClip _clip) {
                if (this.videoPlayer != null) {
                    this.videoPlayer.clip = _clip;
                    this.videoPlayer.Play();
                }
            }

            public override void OnIntroComplete(ActiveTrigger a) {
                if (instance != null && videoPlayer != null) {
                    if (videoClip != null) {
                        videoPlayer.clip = videoClip;
                        currentVideoClip = videoClip;
                        playVideoClip(currentVideoClip);
                    }
                }
            }

            public override void OnEndComplete(ActiveTrigger a) {
                if (instance != null && videoPlayer != null) {
                    //endVideo. 
                    videoPlayer.clip = endVideoClip;
                    currentVideoClip = endVideoClip;
                    playVideoClip(currentVideoClip);
                }
            }

            public override void Release() {
                //first time this is being added to parent activeBehaviour.
                //check for doubles in activeColor. 

                if (_sequence != null) {
                    _sequence.Kill();
                }

                if (this.triggers[0] != null && this.triggers[0].endMedia != null) {
                    _sequence = DOTween.Sequence();
                    EndSequence(_sequence, this.triggers[0]);
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o) {
                if (this.targetVideoClip != null) {
                    videoPlayer.clip = this.targetVideoClip;

                    if (_triggerSequence != null) {
                        _triggerSequence.Kill();
                    }

                    _triggerSequence = DOTween.Sequence();
                    TriggerSequence(_triggerSequence, a);

                    if (!triggerObj.sustain && a.offTime >= 0) {
                        TriggerOffSequence(_triggerSequence, a);
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o) {
                //TODO - this should probably be the generic, probaly just need global methods such as .
                //OnTriggerStart and OnTriggerOff
                if (_triggerSequence != null) {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                TriggerOffSequence(_triggerSequence, a);
            }

            public override void OnTriggerComplete(ActiveTrigger a) {
                //we can't really transition in between frames so this is just a timer. 
                if (a.targetMedia != null && a.targetMedia.loadedTexture != null) {
                    //a.targetMedia.loadedVideo = getNextVideoVariant(a.targetMedia);
                    //videoPlayer.videoClip = AssetController.instance.LoadVideo(a.targetMedia.loadedVideo);
                }

            }

            public override void OnTriggerOffComplete(ActiveTrigger a) {
                //this value should always exist. 
                if (instance != null && rend != null) {
                    videoPlayer.clip = currentVideoClip;
                }
            }

            private FlyByController getFlyByController() {
                //TODO - move this to base class. 
                if (this.flyByController != null) {
                    return this.flyByController;
                }
                if (FlyByController.instance != null) {
                    return FlyByController.instance;
                }
                return null;
            }

            private AssetController getAssetController() {
                if (this.assetController != null) {
                    return this.assetController;
                }
                if (AssetController.instance != null) {
                    return AssetController.instance;
                }
                return null;
            }
        }
    }
}