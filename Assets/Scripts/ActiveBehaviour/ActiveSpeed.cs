using DG.Tweening;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveSpeed : AdditiveActiveBehaviour
        {
            private Vector3 speed = new Vector3(); //final calculated speed
            private Vector3[] triggerValues; //speeds from triggers.

            public override void setupComponent()
            {
                //only needs a transform. 
                this.hasComponent = true;
                triggerValues = new Vector3[triggers.Count];
            }

            public override void setupActiveTriggers(Obj o)
            {
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o != null && o.triggerPosition != null)
                    {
                        this.assignSecondaryTriggers(o.triggerSpeed);
                    }
                    this.instance = this.gameObject;
                }
                this.setupComponent();
            }

            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                if (this.hasComponent && at._i < this.triggerValues.Length)
                {
                    return this.triggerValues[at._i];
                }
                return Vector3.zero;
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent && at._i < this.triggerValues.Length)
                {
                    this.triggerValues[at._i] = _value;
                    this.speed = getTotalSpeed(this.triggerValues);
                }
            }

            public override void Update()
            {
                base.Update();
                if (instantiated && this.triggers != null)
                {
                    //debugging speed: 
                    //print(Time.time + " here we are: " + speed.x + " " + this.gameObject.name + " -- " + this.gameObject.transform.position.x);
                    instance.transform.Translate(this.speed * Time.deltaTime);
                }
            }

            private Vector3 getTotalSpeed(Vector3[] speeds)
            {
                Vector3 totalSpeed = new Vector3();
                foreach (Vector3 s in speeds)
                {
                    totalSpeed += s;
                }
                return totalSpeed;
            }
        }
    }
}