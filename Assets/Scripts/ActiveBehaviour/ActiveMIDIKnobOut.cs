using UnityEngine;
using flyby.Controllers;
using flyby.Active;
using flyby.Core;
using flyby.Triggers;

//this class translates an arbitary "knob" value to a CC message OUT 
// example call:   this.midiController.triggerMidiKnob(this.channel, this.knob, value);
public class ActiveMIDIKnobOut : AdditiveActiveBehaviour
{
    public int channel = 0;
    public int knob = 1;
    private MidiController midiController;

    private float knobValue; //stored value so we can animate to it. 
    private float lastKnobValue; //TODO - if we start changing channels or making a dictionary this will not work. 

    public override void setupComponent()
    {
        this.midiController = MidiController.instance;
        this.hasComponent = this.midiController != null;
    }

    public override Vector3 getCurrentValue(ActiveTrigger at)
    {
        if (this.hasComponent)
        {
            return new Vector3(this.knobValue, this.channel, this.knob);
        }
        return Vector3.zero;
    }

    // public override void Trigger(TriggerObj triggerObj, ActiveTrigger at, Obj o)
    // {
    //     Debug.Log("triggerin: " + triggerObj.eventName);
    //     base.Trigger(triggerObj, at, o);
    // }

    public override void updateValue(ActiveTrigger at, Vector3 _value)
    {
        if (this.hasComponent)
        {
            switch (at.propertyName)
            {
                //reset the channel. 
                case "channel":
                    this.channel = (int)_value.x;
                    break;
                case "knob":
                    this.knob = (int)_value.x;
                    break;
                default:
                    this.knobValue = _value.x; //not to be confused with the knob of the activeTrigger.
                    if (knobValue != lastKnobValue)
                    {
                        this.midiController.triggerMidiKnob(this.channel, this.knob, _value.x); //experimental. need to refactor if using multiple channels. 
                    }
                    lastKnobValue = knobValue;
                    break;
            }

            if (at.propertyIndex > 0)
            {
                this.knob = at.propertyIndex;
            }
        }
    }

    public override void checkForCachedKnobs(ActiveTrigger parentA)
    {
        //skip this for now. 
    }

}
