using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

namespace flyby
{
    namespace Active
    {
        public class ActiveVolume : AdditiveActiveBehaviour
        {
            private Volume volume;
            private Bloom bloom;
            private GradientSky gradientSky;


            // first example of multiple overlapping tweens. 
            public override void setupActiveTriggers(Obj o)
            {
                // useful for generic and dynamic but custom behaviours. 
                // to quickly make variations via json. 
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    // this one will probably use a bunch of duplicates of this script. 
                    // probably need find a way to add an array of them with copies of the component. 
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);
                        this.instance = this.gameObject;
                    }
                }
                setupVolume();

            }

            private void setupVolume()
            {
                //eventually will use arrays of volumes. 
                //this.volume = rend.volume;
                volume = GetComponent<Volume>();
                volume.profile.TryGet(out gradientSky);
                if (gradientSky == null)
                {
                    Debug.Log("could not set up gradient sky");
                }
                volume.profile.TryGet(out bloom);
                if (bloom == null)
                {
                    Debug.Log("could not set up bloom");
                    return;
                }
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                if (Application.isPlaying)
                {
                    if (this.volume != null && this.triggers != null && this.triggers.Count > 0)
                    {
                        Kill();
                        triggerController = base.getTriggerController();
                        //this.checkForCachedKnobs(this.triggers[0]); dont think you need caches for volumes. 
                        foreach (ActiveTrigger at in this.triggers)
                        {
                            at.lerpValue = 0;

                            if (at.end != null)
                            {
                                at.endValue = at.end.getVector3();
                            }

                            if (at.endColor != null)
                            {
                                at.endColorValue = at.endColor.getColor();
                            }

                            if (at.value != null)
                            {
                                at.baseValue = at.value.getVector3();
                            }
                            else
                            {
                                //at.baseValue = getDefaultValue(propertyName);
                            }

                            if (at.color != null)
                            {
                                at.baseColorValue = at.color.getColor();
                            }
                            else
                            {
                                //at.baseColor = getDefaultColor(propertyName);
                            }


                            if (at.start != null)
                            {
                                at.startValue = at.start.getVector3();
                            }
                            else
                            {
                                at.startValue = at.baseValue;
                            }

                            if (at.startColor != null)
                            {
                                at.startColorValue = at.startColor.getColor();
                            }
                            else
                            {
                                at.startColorValue = Color.black;
                            }

                            if (at.startTime <= 0)
                            {
                                at.startValue = at.baseValue;
                                at.startColorValue = at.baseColorValue;
                            }

                            at.currentValue = at.startValue;
                            at.currentColorValue = at.startColorValue;

                            updateValue(at, at.currentValue);

                            if (at.hasColor())
                            {
                                updateColor(at, at.currentColorValue);
                            }


                            bool needsSequence = at.startTime > 0 || at.endTime > 0;
                            if (needsSequence)
                            {

                                if (at.hasColor())
                                {
                                    if (at.startColor == at.color)
                                    {
                                        at.startTime = 0;
                                    }
                                }
                                else
                                {
                                    if (at.start == at.value)
                                    {
                                        at.startTime = 0;
                                    }
                                }

                                if (at.sequence != null)
                                {
                                    at.sequence.Kill();
                                }
                                at.sequence = DOTween.Sequence();
                                AdditiveIntroSequence(at);
                                if (!triggerObj.sustain && at.endTime > 0)
                                {
                                    AdditiveEndSequence(at);
                                }
                            }
                        }
                    }
                }
                base.Intro(triggerObj, o);
            }

            public override void Update()
            {
                updateVolume();
            }


            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                switch (at.propertyName)
                {
                    case "weight":
                        volume.weight = _value.x;
                        break;
                    case "bloom":
                        bloom.intensity.overrideState = true;
                        bloom.intensity.value = _value.x;
                        break;
                    case "bloom_threshold":
                        bloom.threshold.value = _value.x;
                        break;
                    case "scatter":
                        bloom.scatter.value = _value.x;
                        break;
                    case "dirtIntensity":
                        bloom.dirtIntensity.value = _value.x;
                        break;
                }
            }

            private void updateColor(ActiveTrigger at, Color _color)
            {
                switch (at.propertyName)
                {
                    case "backgroundColor":
                        //all three parts of gradient sky here. 
                        gradientSky.top.value = _color;
                        gradientSky.middle.value = _color;
                        gradientSky.bottom.value = _color;
                        break;
                    case "topColor":
                        gradientSky.top.value = _color;
                        break;
                    case "middleColor":
                        gradientSky.middle.value = _color;
                        break;
                    case "bottomColor":
                        gradientSky.middle.value = _color;
                        break;
                }
            }


            private void updateVolume()
            {
                if (volume != null && this.triggers != null)
                {
                    for (int i = 0; i < this.triggers.Count; i++)
                    {
                        ActiveTrigger at = this.triggers[i];

                        if (at.starting)
                        {
                            if (at.value != null)
                            {
                                at.currentValue = Vector3.Lerp(at.startValue, at.baseValue, at.lerpValue);
                                updateValue(at, at.currentValue);
                            }
                            if (at.color != null)
                            {
                                at.currentColorValue = Color.Lerp(at.startColorValue, at.baseColorValue, at.lerpValue);
                                updateColor(at, at.currentColorValue);
                            }
                        }

                        if (at.ending)
                        {
                            at.currentValue = Vector3.Lerp(at.baseValue, at.endValue, 1.0f - at.lerpValue);
                            updateValue(at, at.currentValue);
                            if (at.hasColor())
                            {
                                updateColor(at, at.currentColorValue);
                            }
                        }

                        if (at.target != null && at.animatingTrigger)
                        {
                            at.animatedValue = Vector3.Lerp(at.currentAnimatedValue, at.targetValue, at.triggerLerpValue);
                            updateValue(at, at.animatedValue);
                            if (at.hasColor())
                            {
                                updateColor(at, at.animatedColorValue);
                            }
                        }

                        if (at.knob != null && at.animatingKnob)
                        {
                            float mappedValue = Tools.Math.Lerp(at.knob.x, at.knob.x2, at.knobValue);
                            updateValue(at, new Vector3(mappedValue, 0, 0));
                            if (at.hasColor())
                            {
                                Color mappedColor = at.knobColor.lerpColor(at.knobValue);
                                //not sure if you want the actual color or not - and this posible should be added. 
                                updateColor(at, mappedColor);
                            }
                            at.animatingKnob = false;
                        }
                    }
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {

                if (Application.isPlaying && a != null && volume != null && a.target != null)
                {

                    Vector3 newTargetValue = a.target.getVector3();

                    if (a.triggerSequence != null)
                    {
                        a.triggerSequence.Kill();
                    }

                    a.targetValue = newTargetValue; //usually just one.
                    a.triggerSequence = DOTween.Sequence();



                    AdditiveTriggerSequence(a, triggerObj.sustain);

                    a.lastTargetValue = newTargetValue;
                    base.Trigger(triggerObj, a, o);
                    if (!triggerObj.sustain)
                    {
                        AdditiveTriggerOffSequence(a);
                    }
                }
            }

            public override void Release()
            {
                //TODO: can we get the actual activeTrigger that is being released?
                foreach (ActiveTrigger at in this.triggers)
                {
                    if ((at.value != null || at.color != null) && at.sequence != null && at.end != null)
                    {
                        at.sequence.Kill();
                        this.AdditiveEndSequence(at);
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //handing sustain off
                if (a.triggerSequence != null)
                {
                    a.triggerSequence.Kill();
                }
                a.triggerSequence = DOTween.Sequence();
                a.currentAnimatedValue = Vector3.zero;  //not sure if this should be zero. 
                AdditiveTriggerOffSequence(a);
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null || at.knobColor != null)
                    {
                        if (at.trigger == knobEvent.eventName)
                        {
                            at.knobValue = knobEvent.value;
                            at.animatingKnob = true;
                        }
                    }
                }
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                this.triggerController = getTriggerController();
                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }

                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null || at.knobColor != null)
                    {
                        TriggerObj t = checkForCachedKnob(at);
                        if (t != null)
                        {
                            at.animatingKnob = true;
                        }
                        float cachedValue = t != null ? t.value : 0;
                        //need to figure out if we want to normalize in the data. it would make this algorithm easier. 
                        at.knobValue = t != null ? t.value : at.knobValue;
                    }
                }
            }
        }
    }
}

