using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveParticles : AdditiveActiveBehaviour
        {
            public ParticleSystem particles;
            private ParticleSystemRenderer rend;
            private ParticleSystem.MainModule main;
            private AssetController assetController;
            private Material material; //particle material. 

            private float scaleX = 1;
            private float scaleY = 1;
            private float scaleMultiplier = 1;
            private Vec currentScale; //sanitized, additive ( mutliplied )
            private Vector3 scale;
            private Vector3 scale2;
            private Vector3 knobScale = new Vector3(1, 1, 1);

            private Vec currentRotation; //sanitized, additive ( mutliplied )
            private Vector3 rotation;
            private Vector3 knobRotation = new Vector3(0, 0, 0);
            private bool using3DSize = false;
            private Mesh shapeMesh;

            private string propertyName = "_MainTex";  //try _BaseColorMap if this doesn't work. 


            public override void setupComponent()
            {
                if (this.particles == null)
                {
                    this.particles = GetComponent<ParticleSystem>();
                }

                if (this.particles != null)
                {
                    this.rend = this.particles.GetComponent<ParticleSystemRenderer>();
                    this.material = rend.material;
                    this.main = this.particles.main;
                    //setup 3d size 
                    //this.setupShapeMesh(); //not verified as working. . 
                    this.setupInitialTextures(this.triggers);
                    this.hasComponent = true;
                }
                else
                {
                    Debug.Log("could not find particles");
                }
            }


            private void setupInitialTextures(List<ActiveTrigger> triggers)
            {
                //doing a quick setup first. 
                foreach (ActiveTrigger at in this.triggers)
                {
                    //should be last, doing a quick test first. 
                    //no variants yet. 
                    if (at.startMedia != null)
                    {
                        preloadVariants(at.startMedia);
                    }

                    if (at.endMedia != null)
                    {
                        preloadVariants(at.endMedia);
                    }
                    if (at.targetMedia != null)
                    {
                        preloadVariants(at.targetMedia);
                    }

                    if (at.media == null && at.valueString != null)
                    {
                        at.media = getMediaFromString(at.valueString);
                    }

                    this.preloadVariants(at.media);

                    if (at.media != null && at.media.loadedTexture != null)
                    {

                        this.setParticleTexture(at.media);
                    }
                }
            }

            private void preloadVariants(Media media)
            {
                this.assetController = getAssetController();
                if (this.assetController != null && media != null)
                {
                    media = this.assetController.PreloadTextureVariants(media);
                }
            }

            private AssetController getAssetController()
            {
                //TODO - put in utility function.
                if (this.assetController != null)
                {
                    return this.assetController;
                }
                if (AssetController.instance != null)
                {
                    return AssetController.instance;
                }
                return null;
            }

            private Media getMediaFromString(string mediaString)
            {
                Media media = new Media();
                media.path = mediaString;
                return media;
            }

            private void setParticleTexture(Media media)
            {
                if (media != null && media.loadedTexture != null && this.material != null)
                {
                    Texture2D texture = media.loadedTexture;
                    this.material.mainTexture = texture;
                    this.material.SetTexture(this.propertyName, texture);

                    var textureSheetAnimation = this.particles.textureSheetAnimation;
                    if (media.cols > 1 || media.rows > 1)
                    {
                        textureSheetAnimation.enabled = true;
                        textureSheetAnimation.numTilesX = media.cols;
                        textureSheetAnimation.numTilesY = media.rows;
                        textureSheetAnimation.animation = ParticleSystemAnimationType.WholeSheet;
                        int startFrame2 = media.startFrame2 > -999 ? media.startFrame2 : media.startFrame;
                        textureSheetAnimation.startFrame = new ParticleSystem.MinMaxCurve(media.startFrame, startFrame2);
                        textureSheetAnimation.timeMode = ParticleSystemAnimationTimeMode.FPS;
                        textureSheetAnimation.fps = Tools.Rand.Between(media.fps, media.fps2); //in case we're using fps. 
                    }
                    else
                    {
                        textureSheetAnimation.enabled = false;
                    }
                }
            }

            private void setupShapeMesh(ParticleSystem _particles, ActiveTrigger _a)
            {
                //not verified as working yet. 
                if (!string.IsNullOrEmpty(_a.propertyName))
                {
                    GameObject shapeGO = FlyByController.instance.findPrefabById(_a.valueString);
                    if (shapeGO != null)
                    {
                        this.shapeMesh = getShapeMesh(shapeGO);
                        var shapeModule = _particles.shape;
                        shapeModule.mesh = this.shapeMesh;
                        shapeModule.shapeType = ParticleSystemShapeType.Mesh;
                        // this.particles.shape = shapeModule;
                    }
                    else
                    {
                        Debug.Log("could not find: " + _a.valueString);
                    }
                }
            }

            private Mesh getShapeMesh(GameObject go)
            {
                if (go.GetComponent<SkinnedMeshRenderer>() != null)
                {
                    return go.GetComponent<SkinnedMeshRenderer>().sharedMesh;
                }
                if (go.GetComponent<MeshFilter>() != null)
                {
                    return go.GetComponent<MeshFilter>().sharedMesh;
                }
                Debug.Log("Had trouble finding a shape mesh mesh for go: " + go.name);
                return null;
            }

            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                Vector3 value = new Vector3();
                if (this.hasComponent && this.material != null)
                {
                    switch (at.propertyName)
                    {
                        case "spawnRate":
                        case "rateOverTime":
                        default:
                            var emission = this.particles.emission;
                            value.x = emission.rateOverTime.constant;
                            break;
                        case "speed":
                            value.x = this.main.startSpeed.constant;
                            break;
                        case "scale":
                            value = this.currentScale.getVector3();
                            break;
                        case "rotation":
                            value = this.currentRotation.getVector3();
                            break;
                    }
                }
                return value;
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                //redesign - maybe use min max ? 
                ParticleSystem.MinMaxCurve curve = new ParticleSystem.MinMaxCurve(0, 0);

                switch (at.propertyName)
                {
                    case "spawnRate":
                    case "rateOverTime":
                    default:
                        var emission = this.particles.emission;
                        emission.rateOverTime = _value.x;
                        break;
                    case "speed":
                        this.main.startSpeed = curve;
                        break;
                    case "scale":
                        //TODO - make scalable with multiple knobs. 
                        //can probably use the multiplier for immediate needs. 
                        if (at.knob == null)
                        {
                            this.scale = _value;
                            this.updateScale();
                        }
                        else
                        {
                            this.knobScale = _value;
                        }
                        break;
                    case "rotation":
                        //determine if knob or trigger.
                        if (at.knob == null)
                        {
                            this.rotation = _value;
                            this.updateRotation();
                        }
                        break;
                }
            }

            private void apply3DSize()
            {
                this.main.startSize3D = true;
            }

            private void updateScale()
            {
                //apply minMax to scale 
                this.currentScale = new Vec(this.scale, this.scale2);

                if (this.currentScale.x != this.currentScale.y)
                {
                    this.apply3DSize();

                    this.main.startSizeX = new ParticleSystem.MinMaxCurve(this.currentScale.x, this.currentScale.x2);
                    this.main.startSizeY = new ParticleSystem.MinMaxCurve(this.currentScale.y, this.currentScale.y2);
                    this.main.startSizeZ = new ParticleSystem.MinMaxCurve(this.currentScale.z, this.currentScale.z2);
                }
                else
                {
                    this.main.startSize = new ParticleSystem.MinMaxCurve(this.currentScale.x, this.currentScale.x2);
                }
            }

            private void updateRotation()
            {
                this.currentRotation = new Vec(this.rotation);
                this.main.startRotationX = new ParticleSystem.MinMaxCurve(this.currentRotation.x, this.currentRotation.x2);
                this.main.startRotationY = new ParticleSystem.MinMaxCurve(this.currentRotation.y, this.currentRotation.y2);
                this.main.startRotationZ = new ParticleSystem.MinMaxCurve(this.currentRotation.z, this.currentRotation.z2);
            }
        }
    }
}