
using DG.Tweening;
using flyby.Core;
using flyby.Triggers;
using UnityEngine;
using System.Collections.Generic;
namespace flyby
{
    namespace Active
    {
        public class HelloWorld : ActiveBehaviour
        {
            [HideInInspector]
            public int numTimesIntroduced;
            public int numTimesTriggered;
            public float knobValue;

            public override void setupActiveTriggers(Obj o)
            {
                // useful for generic and dynamic but custom behaviours. 
                // to quickly make variations via json. 
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);
                    }
                }
            }


            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                numTimesIntroduced = numTimesIntroduced + 1;
                Debug.Log("hello world intro: " + numTimesIntroduced);
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //NOTE - this is called after the trigger has passed the filters. 
                //if you want to listen to anny trigger event, use TriggerEVentHandler. 

                numTimesTriggered = numTimesTriggered + 1;
                Debug.Log("hello world trigger: " + numTimesTriggered);
            }

            public override void TriggerKnob(TriggerObj triggerObj, ActiveTrigger _a, Obj o)
            {
                Debug.Log("we are trigger knob: " + triggerObj.value);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        this.knobValue = at.knob.lerpVector3(triggerObj.value).x;
                        Debug.Log(" we got it: " + this.knobValue);
                    }
                }
            }

            // public override void TriggerEventHandler(TriggerObj triggerObj, Obj o)
            // {
            //     //this will listen to all triggers.
            //     base.TriggerEventHandler(triggerObj, o);
            // }
        }
    }
}