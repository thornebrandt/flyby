using DG.Tweening;
using UnityEngine;
using MudBun;
using Dreamteck.Splines;
using System.Collections.Generic;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Active
    {
        public class ActiveMudSpline : AdditiveActiveBehaviour
        // NOTE - use hierarchy type "KeepChildren" for best results. 
        // this class makes a worm like shape go down a spline path.  It facilitates the setup of the mud points.  
        {
            public MudCurveFull mudCurve;
            public SplineComputer splineComputer;
            public GameObject prefab; //for debugging purposes. 
            public int numPoints = 5;
            public float radius = 0.25f;
            public float slope = 0.2f;
            public float pointDistance = 0.01f;
            private float splineValue;
            private List<float> splinePositions;
            private List<SplinePositioner> splinePositioners;
            private List<Transform> curveTransforms; //final transforms after manipulation. 
            private List<MudBun.MudCurveFull.Point> points;

            public override void Update()
            {
                base.Update();
                if (this.splinePositioners != null && this.splinePositioners.Count > 0)
                {
                    updateSplinePositioners();
                    if (this.mudCurve != null && this.mudCurve.Points.Count > 0)
                    {
                        updateMudRadius();
                    }
                }
            }

            private void updateSplinePositioners()
            {
                for (int i = 0; i < this.splinePositioners.Count; i++)
                {
                    SplinePositioner splinePositioner = this.splinePositioners[i];
                    if (splinePositioner != null)
                    {
                        splinePositioner.SetPercent(this.splinePositions[i]);
                        Transform splineTransform = splinePositioner.transform;
                        if (this.curveTransforms != null && this.curveTransforms.Count > i)
                        {
                            this.curveTransforms[i].position = new Vector3(
                                splineTransform.position.x,
                                splineTransform.position.y,
                                splineTransform.position.z + (i * 0.0005f) //to prevent disappearing shapes.  
                            );
                        }
                    }

                }
            }

            private void updateMudRadius()
            {
                for (int i = 0; i < this.splinePositions.Count; i++)
                {
                    float rMultiplier = this.quadratic_curve(this.splinePositions[i], slope) * this.quadratic_curve(splineValue, 0.005f);
                    mudCurve.Points[i].Radius = radius * rMultiplier;
                }
            }

            public override void setupComponent()
            {
                if (this.mudCurve != null && this.splineComputer != null)
                {
                    points = new List<MudBun.MudCurveFull.Point>();
                    float offset = pointDistance;
                    this.splinePositioners = new List<SplinePositioner>();
                    this.splinePositions = new List<float>();
                    this.curveTransforms = new List<Transform>();
                    for (int i = 0; i < this.numPoints; i++)
                    {
                        //GameObject splinePoint = Instantiate(prefab) as GameObject;
                        GameObject splinePoint = new GameObject(); //empty gameObject that follows spline.
                        //GameObject curvePoint = Instantiate(prefab) as GameObject;
                        GameObject curvePoint = new GameObject(); //another layer of points that we will manipulate. 
                        splinePoint.name = this.gameObject.name + "_spline_point_" + i;
                        splinePoint.transform.SetParent(this.gameObject.transform);
                        curvePoint.name = this.gameObject.name + "_curve_point_" + i;
                        curvePoint.transform.SetParent(this.gameObject.transform);
                        this.curveTransforms.Add(curvePoint.transform);
                        SplinePositioner splinePositioner = splinePoint.AddComponent<SplinePositioner>();
                        splinePositioner.spline = splineComputer;
                        splinePositioner.SetPercent(offset);
                        this.splinePositions.Add(offset);
                        this.splinePositioners.Add(splinePositioner);
                        curvePoint.transform.position = splinePoint.transform.position;
                        MudBun.MudCurveFull.Point point = new MudBun.MudCurveFull.Point
                        {
                            Transform = curvePoint?.transform,
                            Radius = this.radius
                        };
                        point.Radius = 0;
                        points.Add(point);
                        offset = offset + pointDistance;
                    }
                    mudCurve.Points = points;
                    this.hasComponent = true;
                }
                else
                {
                    Debug.Log("mud curve or computer not setup on activeMudSpline");
                }
            }

            float quadratic_curve(float x, float width)
            {
                width = Mathf.Clamp(width, 0f, 0.5f);

                // If width is 0, return 1 for all x
                if (Mathf.Approximately(width, 0))
                {
                    return 1;
                }

                // Calculate the lower and upper bounds
                float lowerBound = width;
                float upperBound = 1f - width;

                // Interpolate based on the region of x
                if (x < lowerBound)
                {
                    return Mathf.InverseLerp(0f, lowerBound, x);
                }
                else if (x > upperBound)
                {
                    return Mathf.InverseLerp(1f, upperBound, x);
                }
                else
                {
                    return 1;
                }
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                //TODO - this might cause a problem if we get to the max value. 

                if (this.hasComponent)
                {
                    switch (at.propertyName)
                    {
                        case "position":
                        case "":
                            updateSplinePositionValues(_value.x);
                            break;
                    }
                }
            }

            private void updateSplinePositionValues(float _value)
            {
                float offset = 0;
                this.splineValue = _value;
                float normalizedValue = (1 + (pointDistance * (numPoints - 1))) * _value;
                for (int i = 0; i < this.splinePositions.Count; i++)
                {
                    this.splinePositions[i] = normalizedValue + offset - (pointDistance * (numPoints - 1));
                    offset = offset + pointDistance;
                }
            }

            public override void Kill()
            {
                if (this.points != null & this.points.Count > 0)
                {
                    for (int i = 0; i < this.points.Count; i++)
                    {
                        this.curveTransforms[i].position = Vector3.zero;
                        this.points[i].Radius = 0;
                    }
                }
                base.Kill();
            }

            public override void Hide()
            {
                if (this.points != null & this.points.Count > 0)
                {
                    for (int i = 0; i < this.points.Count; i++)
                    {
                        if (this.curveTransforms[i] != null)
                        {
                            this.curveTransforms[i].position = this.o.nullVector;
                        }
                        this.points[i].Radius = 0;
                    }
                }
                base.Hide();
            }
        }
    }
}