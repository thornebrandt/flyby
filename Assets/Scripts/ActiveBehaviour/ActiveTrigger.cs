using System;
using System.Collections.Generic;
using flyby.Core;
using DG.Tweening;
using flyby.Tools;
using flyby.Triggers;
using UnityEngine;
using Newtonsoft.Json;

//this class is meant to encapsulate "active behaviour triggers"
//to prevent objs from becoming too monolithic. 

namespace flyby
{
    namespace Active
    {
        [Serializable]
        public class ActiveTrigger
        {
            //TODO - think about consolidating vec and color
            public int oi; //the objs instance i ( for debugging purposes ) 
            public string id; //representation of activeTrigger.
            public string preset;
            public string trigger; //event name
            public int propertyIndex; //material or blendshape index.
            public string propertyName; //example: color name, also used for prefs.
            public string behaviour; //id of behaviour when there are multiple activeTriggers. ( leave blank for all of them )  
            public float startTime; //example: time to fade in 
            public float startTime2; //adding a range to the startTime.  TODO - do we need knobStartTimeTrigger?
            public float endTime; //triggered on exit
            public float endTime2; //adding range to endTime;
            public float onTime; //on animation for trigger. 
            public float onTime2; //adding range to onTime. 
            public float offTime; //off animation for trigger. 
            public float offTime2; //to add range to offTime. 
            public Vec value;
            public Vec start; //intro position, rotation, etc. 
            public Vec target; //triggered target position, rotation, etc. 
            public Vec knob; //meant to interpolate between two vectors with a knob. 
            public Vec velocity; //meant to interpolate between two vectors and velocity. 
            public Vec end; //ending position, rotation, etc.
            public Col color; //resting color.
            public Col startColor; //fading in color, fades to obj.color
            public Col targetColor; //triggered target color
            public Col knobColor;
            public Col endColor; //fadeOut color 
            public float multiplier; //quickly affects all vectors.
            public float multiplier2; //quickly affects. 
            public string startString;
            public string valueString;
            public string targetString;
            public string endString;
            public Media startMedia;
            public Media media;
            public Media targetMedia;
            public Media endMedia;
            [HideInInspector]
            public bool disabled; //toggle off for testing. 
            [HideInInspector]
            public bool hideOnComplete; //making hide different than kill.
            public bool hideOnTarget; //hides on trigger reaches destination, can be zero, meant for seamlessly swapping objs.
            public float onDelay; //delay for trigger. 
            public float onDelay2; //range for ondelay. 
            public float offDelay; //sustain without sustain 
            public float offDelay2; //range for offDelay. 
            public float startDelay;
            public float startDelay2; //range for startDelay. 
            public float endDelay; //formerly fadeout delay
            public float endDelay2; //range for endDelay. 
            public int timesTriggered; //private, incremented when triggered to target. 
            public int groupIndex; //set by the constrtuctor to determine trigger order. 
            public string group;

            //stashing for show - needs some work materials are not behaving correctly. 
            public int childIndex; //tells activeBehavior to target a first order child with the script instead of parent. 
            public Ease startEase;
            public Ease endEase;
            public Ease onEase;
            public Ease offEase;
            public string activeBehaviour; //experimental - not implemented yet - for mulitple generic triggers. 
            public Curve curve; //experimental - used for bone rotations.  ??? DELETE THIS I THINK. 

            //---------------------------------------for additive/multiplying triggers. 
            public bool instantiated; //intro has been fired.
            [HideInInspector]
            public bool fresh; //brief moment before any animation.
            [HideInInspector]
            public bool introduced; //should it be called ended?  
            [HideInInspector]
            public bool ended;
            [HideInInspector]
            public bool triggered; //a trigger has completed. 
            [HideInInspector]
            public bool animating;
            [HideInInspector]
            public bool animatingTrigger;
            [HideInInspector]
            public bool animatingTriggerOff;
            [HideInInspector]
            public bool animatingKnob;
            [HideInInspector]
            public bool starting;
            [HideInInspector]
            public bool ending;
            [HideInInspector]
            public float lerpValue;
            [HideInInspector]
            public float triggerLerpValue;
            [HideInInspector]
            public bool on; //active trigger animation is completed
            [HideInInspector]
            public int _i; //trigger index
            [HideInInspector]
            public Sequence sequence;
            [HideInInspector]
            public Sequence triggerSequence;
            [HideInInspector]
            public Vector3 baseValue; //when not starting or ending. 
            [HideInInspector]
            public Vector3 startValue; //private cached start value. 
            [HideInInspector]
            public Vector3 endValue;
            [HideInInspector]
            public Vector3 currentValue; //current value in start/end envelope. 
            [HideInInspector]
            public Vector3 currentAnimatedValue; //current value when triggers begin. 
            [HideInInspector]
            public Vector3 targetValue; //goal for triggers. 
            [HideInInspector]
            public Vector3 lastTargetValue; //cached goal for triggers.
            [HideInInspector]
            public Vector3 animatedValue; //final calculated animatedValue. 
            [HideInInspector]
            public Vector3 lastValue; //cached final value. 
            [HideInInspector]
            public Color baseColorValue;
            [HideInInspector]
            public Color startColorValue;
            [HideInInspector]
            public Color endColorValue; //when not starting or ending. 
            [HideInInspector]
            public Color currentColorValue; //current value in start/end envelope. 
            [HideInInspector]
            public Color currentAnimatedColor; //current value for triggers. 
            [HideInInspector]
            public Color targetColorValue; //goal for triggers. 
            [HideInInspector]
            public Color lastTargetColorValue; //cached goal for triggers.
            [HideInInspector]
            public Color animatedColorValue; //final calculated animatedValue. 
            [HideInInspector]
            public float knobValue; //private - used for caching by active behaviours. 
            // ------------------------------------------- end for additive triggers. 


            public List<SecondaryActiveTrigger> triggers; //secondary triggers. 
            public ActiveType activeType;
            public TriggerProperty triggerProperty;

            public ActiveTrigger()
            {
                //this.startTime = 0.3f; //this messes me up so much. trying without it. 
                this.startDelay2 = -999;
                this.onTime = 0.18f;
                this.onTime2 = -999;
                this.offTime = 3f;
                this.offTime2 = -999;
                this.offDelay2 = -999;
                this.multiplier = 1;
                this.multiplier2 = -999;
                this.childIndex = -1;
                this.startTime2 = -999;
                this.endTime2 = -999;
                this.startEase = Ease.OutSine; //setting ease defaults.
                this.endEase = Ease.InOutSine;
                this.onEase = Ease.InOutSine;
                this.offEase = Ease.InOutSine;
            }

            public ActiveTrigger(SecondaryActiveTrigger sat)
            {
                //conversion for looping. 
                this.id = sat.id;
                this.preset = sat.preset;
                this.target = sat.target;
                this.knob = sat.knob;
                this.knobColor = sat.knobColor;
                this.velocity = sat.velocity;
                this.start = sat.start;
                this.value = sat.value;
                this.end = sat.end;
                this.color = sat.color;
                this.targetColor = sat.targetColor;
                this.startColor = sat.startColor;
                this.endColor = sat.endColor;
                this.disabled = sat.disabled;
                this.startTime = sat.startTime;
                this.startTime2 = sat.startTime2;
                this.startDelay = sat.startDelay;
                this.startDelay2 = sat.startDelay2;
                this.endTime = sat.endTime;
                this.endTime2 = sat.endTime;
                this.endDelay = sat.endDelay;
                this.endDelay2 = sat.endDelay2;
                this.onTime = sat.onTime;
                this.onTime2 = sat.onTime2;
                this.onDelay = sat.onDelay;
                this.onDelay2 = sat.onDelay2;
                this.offTime = sat.offTime;
                this.offTime2 = sat.offTime2;
                this.offDelay = sat.offDelay;
                this.offDelay2 = sat.offDelay2;
                this.trigger = sat.trigger;
                this.propertyIndex = sat.propertyIndex;
                this.propertyName = sat.propertyName;
                this.behaviour = sat.behaviour;
                this.activeType = sat.activeType;
                this.targetString = sat.targetString;
                this.targetMedia = sat.targetMedia;
                this.triggerProperty = sat.triggerProperty;
                this.hideOnComplete = sat.hideOnComplete;
                this.hideOnTarget = sat.hideOnTarget;
                this.multiplier = sat.multiplier;
                this.multiplier2 = sat.multiplier2;
                this.group = sat.group;
                this.childIndex = sat.childIndex;
                this.startEase = sat.startEase;
                this.endEase = sat.endEase;
                this.onEase = sat.onEase;
                this.offEase = sat.offEase;
                this.activeBehaviour = sat.activeBehaviour;
                this.curve = sat.curve;
            }

            public ActiveTrigger(ActiveTrigger at)
            {
                //for copying during instantiation. 
                //leaving some properties out for now. 

                //leaving out id and preset. 
                this.id = at.id;
                this.target = at.target;
                this.start = at.start;
                this.startColor = at.startColor;
                this.targetColor = at.targetColor;
                this.value = at.value;
                this.color = at.color;
                this.knob = at.knob;
                this.knobColor = at.knobColor;
                this.velocity = at.velocity;
                this.end = at.end;
                this.endColor = at.endColor;
                this.disabled = at.disabled;
                this.startTime = at.startTime;
                this.startTime2 = at.startTime2;
                this.startDelay = at.startDelay;
                this.startDelay2 = at.startDelay2;
                this.endTime = at.endTime;
                this.endTime2 = at.endTime2;
                this.endDelay = at.endDelay;
                this.onTime = at.onTime;
                this.onTime2 = at.onTime2;
                this.onDelay = at.onDelay;
                this.onDelay2 = at.onDelay2;
                this.offTime = at.offTime;
                this.offTime2 = at.offTime2;
                this.offDelay = at.offDelay;
                this.offDelay2 = at.offDelay2;
                this.trigger = at.trigger;
                this.propertyIndex = at.propertyIndex;
                this.propertyName = at.propertyName;
                this.behaviour = at.behaviour;
                this.activeType = at.activeType;
                this.triggerProperty = at.triggerProperty;
                this.hideOnComplete = at.hideOnComplete;
                this.hideOnTarget = at.hideOnTarget;
                this.triggerSequence = at.triggerSequence;
                this.startString = at.startString;
                this.valueString = at.valueString;
                this.targetString = at.targetString;
                this.startMedia = at.startMedia;
                this.media = at.media;
                this.endMedia = at.endMedia;
                this.targetMedia = at.targetMedia;
                this.endMedia = at.endMedia;
                this.endString = at.endString;
                this.sequence = at.sequence;
                this.multiplier = at.multiplier;
                this.multiplier2 = at.multiplier2;
                this.group = at.group;
                this.childIndex = at.childIndex;
                this.startEase = at.startEase;
                this.endEase = at.endEase;
                this.onEase = at.onEase;
                this.offEase = at.offEase;
                this.activeBehaviour = at.activeBehaviour;
                this.curve = at.curve;
                //------
                this.triggers = at.triggers;
            }

            public bool hasColor()
            {
                return this.startColor != null ||
                        this.color != null ||
                        this.endColor != null ||
                        this.targetColor != null ||
                        this.knobColor != null;
            }

            public bool isAnimating()
            {
                return this.animating || this.animatingTrigger;
            }

            public float map(float value)
            {
                if (this.triggerProperty != null)
                {
                    return this.triggerProperty.map(value);
                }
                return value;
                //mapping a float value from triggerProperties. 
            }

            public float mapKnobValue()
            {
                return this.map(this.knobValue);
            }

            public override bool Equals(System.Object obj)
            {
                if (obj is null)
                {
                    return false;
                }

                ActiveTrigger a = obj as ActiveTrigger;
                return
                    this.value == a.value &&
                    this.start == a.start &&
                    this.target == a.target &&
                    this.knob == a.target &&
                    this.end == a.end &&
                    this.startColor == a.startColor &&
                    this.color == a.color &&
                    this.endColor == a.endColor &&
                    this.targetColor == a.targetColor &&
                    this.startTime == a.startTime &&
                    this.startTime2 == a.startTime2 &&
                    this.endTime == a.endTime &&
                    this.endTime2 == a.endTime2 &&
                    this.startDelay == a.startDelay &&
                    this.startDelay2 == a.startDelay2 &&
                    this.endDelay == a.endDelay &&
                    this.onTime == a.onTime &&
                    this.onTime2 == a.onTime2 &&
                    this.offTime == a.offTime &&
                    this.offTime2 == a.offTime2 &&
                    this.onDelay == a.onDelay &&
                    this.onDelay2 == a.onDelay2 &&
                    this.offDelay == a.offDelay &&
                    this.offDelay2 == a.offDelay2 &&
                    this.trigger == a.trigger;
            }

            public void overwrite(ActiveTrigger _at)
            {
                this.id = string.IsNullOrEmpty(_at.id) ? this.id : _at.id;  //experimental.
                this.preset = string.IsNullOrEmpty(_at.preset) ? this.preset : _at.preset; //experimental.
                ActiveTrigger defaultAt = new ActiveTrigger();
                this.trigger = string.IsNullOrEmpty(_at.trigger) ? this.trigger : _at.trigger;
                this.target = _at.target == null ? this.target : _at.target;
                this.knob = _at.knob == defaultAt.knob ? this.knob : _at.knob;
                this.start = _at.start == defaultAt.start ? this.start : _at.start;
                this.value = _at.value == defaultAt.value ? this.value : _at.value;
                this.end = _at.end == defaultAt.end ? this.end : _at.end;
                this.color = _at.color == null ? this.color : _at.color;
                this.targetColor = _at.targetColor == null ? this.targetColor : _at.targetColor;
                this.knobColor = _at.knobColor == null ? this.knobColor : _at.knobColor;
                //this.disabled = _at.disabled == false ? this.disabled : _at.disabled;
                this.startTime = _at.startTime == defaultAt.startTime ? this.startTime : _at.startTime;
                this.startTime2 = _at.startTime2 == defaultAt.startTime2 ? this.startTime2 : _at.startTime2;
                this.endTime = _at.endTime == defaultAt.endTime ? this.endTime : _at.endTime;
                this.endTime2 = _at.endTime2 == defaultAt.endTime2 ? this.endTime2 : _at.endTime;
                this.startDelay = _at.startDelay == 0 ? this.startDelay : _at.startDelay;
                this.startDelay2 = _at.startDelay2 == defaultAt.startDelay2 ? this.startDelay2 : _at.startDelay2;
                this.onTime = _at.onTime == defaultAt.onTime ? this.onTime : _at.onTime;
                this.onTime2 = _at.onTime2 == defaultAt.onTime2 ? this.onTime2 : _at.onTime2;
                this.offTime = _at.offTime == defaultAt.offTime ? this.offTime : _at.offTime;
                this.offTime2 = _at.offTime2 == defaultAt.offTime2 ? this.offTime2 : _at.offTime2;
                this.onDelay = _at.onDelay == defaultAt.onDelay ? this.onDelay : _at.onDelay;
                this.onDelay2 = _at.onDelay2 == defaultAt.onDelay2 ? this.onDelay2 : _at.onDelay2;
                this.offDelay = _at.offDelay == defaultAt.offDelay ? this.offDelay : _at.offDelay;
                this.offDelay2 = _at.offDelay2 == defaultAt.offDelay2 ? this.offDelay2 : _at.offDelay2;
                this.multiplier = _at.multiplier == 1 ? this.multiplier : _at.multiplier;
                this.multiplier2 = _at.multiplier2 == -999 ? this.multiplier2 : _at.multiplier2;
                this.propertyIndex = _at.propertyIndex == 0 ? this.propertyIndex : _at.propertyIndex;
                this.propertyName = string.IsNullOrEmpty(_at.propertyName) ? this.propertyName : _at.propertyName;
                this.behaviour = string.IsNullOrEmpty(_at.behaviour) ? this.behaviour : _at.behaviour;
                this.targetString = string.IsNullOrEmpty(_at.targetString) ? this.targetString : _at.targetString;
                this.hideOnComplete = _at.hideOnComplete == false ? this.hideOnComplete : _at.hideOnComplete;
                this.hideOnTarget = _at.hideOnTarget == false ? this.hideOnTarget : _at.hideOnTarget;
                this.targetMedia = _at.targetMedia == null ? this.targetMedia : _at.targetMedia;
                this.activeType = _at.activeType == ActiveType.Default ? this.activeType : _at.activeType;
                this.triggerProperty = _at.triggerProperty == null ? this.triggerProperty : _at.triggerProperty;
                this.group = string.IsNullOrEmpty(_at.group) ? this.group : _at.group;
                this.childIndex = _at.childIndex == -1 ? this.childIndex : _at.childIndex;
                this.startEase = _at.startEase == defaultAt.startEase ? this.startEase : _at.startEase;
                this.endEase = _at.endEase == defaultAt.endEase ? this.endEase : _at.endEase;
                this.onEase = _at.onEase == defaultAt.onEase ? this.onEase : _at.onEase;
                this.offEase = _at.offEase == defaultAt.offEase ? this.offEase : _at.offEase;
                this.activeBehaviour = string.IsNullOrEmpty(_at.activeBehaviour) ? this.activeBehaviour : _at.activeBehaviour;
                this.curve = _at.curve == null ? this.curve : _at.curve;
                this.triggers = _at.triggers == null ? this.triggers : _at.triggers;
            }

            public void overwrite(SecondaryActiveTrigger _sat)
            {
                SecondaryActiveTrigger defaultSat = new SecondaryActiveTrigger();
                this.trigger = string.IsNullOrEmpty(_sat.trigger) ? this.trigger : _sat.trigger;
                this.target = _sat.target == null ? this.target : _sat.target;
                this.knob = _sat.knob == defaultSat.knob ? this.knob : _sat.knob;
                this.start = _sat.start == defaultSat.start ? this.start : _sat.start;
                this.value = _sat.value == defaultSat.value ? this.value : _sat.value;
                this.end = _sat.end == defaultSat.end ? this.end : _sat.end;
                this.color = _sat.color == null ? this.color : _sat.color;
                this.targetColor = _sat.targetColor == null ? this.targetColor : _sat.targetColor;
                this.knobColor = _sat.knobColor == null ? this.knobColor : _sat.knobColor;
                //this.disabled = _sat.disabled == false ? this.disabled : _sat.disabled;
                this.startTime = _sat.startTime == 0 ? this.startTime : _sat.startTime;
                this.startTime2 = _sat.startTime2 == defaultSat.startTime2 ? this.startTime2 : _sat.startTime2;
                this.endTime = _sat.endTime == defaultSat.endTime ? this.endTime : _sat.endTime;
                this.endTime2 = _sat.endTime2 == defaultSat.endTime2 ? this.endTime2 : _sat.endTime;
                this.startDelay = _sat.startDelay == 0 ? this.startDelay : _sat.startDelay;
                this.startDelay2 = _sat.startDelay2 == 0 ? this.startDelay2 : _sat.startDelay2;
                this.onTime = _sat.onTime == defaultSat.onTime ? this.onTime : _sat.onTime;
                this.onTime2 = _sat.onTime2 == defaultSat.onTime2 ? this.onTime2 : _sat.onTime2;
                this.onDelay = _sat.onDelay == defaultSat.onDelay ? this.onDelay : _sat.onDelay;
                this.onDelay2 = _sat.onDelay2 == defaultSat.onDelay2 ? this.onDelay2 : _sat.onDelay2;
                this.offDelay = _sat.offDelay == defaultSat.offDelay ? this.offDelay : _sat.offDelay;
                this.offDelay2 = _sat.offDelay2 == defaultSat.offDelay2 ? this.offDelay2 : _sat.offDelay2;
                this.offTime = _sat.offTime == defaultSat.offTime ? this.offTime : _sat.offTime;
                this.offTime2 = _sat.offTime2 == defaultSat.offTime2 ? this.offTime2 : _sat.offTime2;
                this.multiplier = _sat.multiplier == 1 ? this.multiplier : _sat.multiplier;
                this.multiplier2 = _sat.multiplier2 == -999 ? this.multiplier2 : _sat.multiplier2;
                this.propertyIndex = _sat.propertyIndex == 0 ? this.propertyIndex : _sat.propertyIndex;
                this.propertyName = string.IsNullOrEmpty(_sat.propertyName) ? this.propertyName : _sat.propertyName;
                this.behaviour = string.IsNullOrEmpty(_sat.behaviour) ? this.behaviour : _sat.behaviour;
                this.targetString = string.IsNullOrEmpty(_sat.targetString) ? this.targetString : _sat.targetString;
                this.hideOnComplete = _sat.hideOnComplete == false ? this.hideOnComplete : _sat.hideOnComplete;
                this.hideOnTarget = _sat.hideOnTarget == false ? this.hideOnTarget : _sat.hideOnTarget;
                this.targetMedia = _sat.targetMedia == null ? this.targetMedia : _sat.targetMedia;
                this.activeType = _sat.activeType == ActiveType.Default ? this.activeType : _sat.activeType;
                this.triggerProperty = _sat.triggerProperty == null ? this.triggerProperty : _sat.triggerProperty;
                this.group = string.IsNullOrEmpty(_sat.group) ? this.group : _sat.group;
                this.childIndex = _sat.childIndex == -1 ? this.childIndex : _sat.childIndex;
                this.startEase = _sat.startEase == defaultSat.startEase ? this.startEase : _sat.startEase;
                this.endEase = _sat.endEase == defaultSat.endEase ? this.endEase : _sat.endEase;
                this.onEase = _sat.onEase == defaultSat.onEase ? this.onEase : _sat.onEase;
                this.activeBehaviour = string.IsNullOrEmpty(_sat.activeBehaviour) ? this.activeBehaviour : _sat.activeBehaviour;
                this.curve = _sat.curve == null ? this.curve : _sat.curve;
            }

            public static bool operator ==(ActiveTrigger lhs, ActiveTrigger rhs)
            {
                if (lhs is null)
                {
                    if (rhs is null)
                    {
                        return true;
                    }
                    return false;
                }
                return lhs.Equals(rhs);
            }


            public static bool operator !=(ActiveTrigger lhs, ActiveTrigger rhs) => !(lhs == rhs);

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }
    }
}