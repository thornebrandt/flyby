using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using System.Collections.Generic;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Rendering;
using flyby;
using System;

// notes this is an additive activeTrigger. 
// blending modes might change soon. 


// x -> position of ramp ( left - right ) 
// y -> width of ramp ( 1 is full width, it extends from center )  0.1 is default. 
// z -> softness of ramp ( eventually )  0 is default. 

// there is a bug with keeping the ramp at full. 
// here is how to resolve a full value in activeQuad. 
// "value": {
//     "x": 0.4999,
//     "y": 1,
//     "z": 0,
// }

// background color is put here as well for use with transparency ( for ramp, not displacement )  
// activeColor should work as well. 
// if a volume is assigned, it will use the postProcessing shader instead of the ramp material. 

//rampTriggers[0] is reserved for knobs, and triggered events with the propertyName "constant" ( name subject to change ) 

//why isn't this causing a conflict? ( see displacementRamp.cs )

[Serializable]
public struct Ramp
{
    public float position;
    public float width;
    public float blur;
    public Vector4 color;
}

namespace flyby
{
    namespace Active
    {
        public class ActiveRamp : ActiveBehaviour
        {
            private Renderer rend;
            private Material material;
            private Volume volume;
            private int numRamps = 32;
            private int rampIndex = 0;
            private ActiveTrigger[] rampTriggers; //pool of triggers
            private Ramp[] ramps; //triggers represented in floats for shader. 
            private float[] rampWidths; //animated widths of each ramp. 
            private bool[] rampSustains; //keeping track of which triggers are being sustained. 
            private Vector4[] rampColors; //animated colors for ramp. 
            private float[] rampBlurs; //animated blur. Value shouldn't be bigger than width ( y ) 
            private ComputeBuffer rampBuffer;


            private Color baseBackgroundColor;
            private Color currentBackgroundColor;
            private Color targetBackgroundColor;
            private bool animatingBackground;

            private VHSRamp displacementRamp;
            private bool usingVolume;
            private bool hasComponent;

            public override void setupActiveTriggers(Obj o)
            {
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    // this one will probably use a bunch of duplicates of this script. 
                    // probably need find a way to add an array of them with copies of the component. 
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);
                        this.instance = this.gameObject;
                    }
                }
                setupShader();
            }

            void OnDestroy()
            {
                if (rampBuffer != null)
                {
                    rampBuffer.Release();
                }
            }

            private void setupShader()
            {
                //TODO - refactor.
                //keep track of ramps here but send them to displacementRamp on updateShaderBuffer
                this.volume = GetComponent<Volume>();
                if (this.volume != null)
                {
                    volume.profile.TryGet(out displacementRamp);
                    this.usingVolume = true;
                    this.hasComponent = true;
                }

                setupRampTriggers();

                if (this.displacementRamp == null)
                {
                    this.rend = GetComponent<Renderer>();
                    if (this.rend != null)
                    {
                        this.material = new Material(rend.material); //hoping this fixes the weird double ramps. 
                        this.rend.material = this.material;
                        this.hasComponent = true;
                    }
                    int bufferSize = sizeof(float) * 7; //3 floats, then assuming that vector 4 is equivalent 4 floats. 
                    this.rampBuffer = new ComputeBuffer(numRamps, bufferSize);
                    updateShaderBuffer();
                }
            }

            private void setupRampTriggers()
            {
                this.ramps = new Ramp[numRamps];
                rampTriggers = new ActiveTrigger[numRamps];
                rampSustains = new bool[numRamps];
                for (int i = 0; i < numRamps; i++)
                {
                    this.ramps[i].position = 0;
                    this.ramps[i].color = new Vector4(0, 0, 0, 1);
                    this.ramps[i].width = 0;
                    this.ramps[i].blur = 0;
                    rampTriggers[i] = new ActiveTrigger();
                    rampSustains[i] = false;
                }
            }

            private void introBackgroundColor(ActiveTrigger at, TriggerObj triggerObj)
            {
                //set background color here. 
            }

            private void introRampTrigger(ActiveTrigger at, TriggerObj triggerObj, int i)
            {
                rampIndex = i;
                rampIndex = (int)Mathf.Repeat(rampIndex, numRamps);
                rampIndex = rampIndex == 0 ? 1 : rampIndex; //reserving 0 for knobs. 
                if (at.propertyName == "constant")
                {
                    //can specify constant ramp that will take over knob. 
                    rampIndex = 0;
                }
                ActiveTrigger rt = new ActiveTrigger(at);

                this.rampTriggers[rampIndex] = rt;
                rt.lerpValue = 0;

                if (at.end != null)
                {
                    rt.endValue = at.end.getVector3();
                }
                else
                {
                    rt.end = new Vec(0);
                }

                if (at.value != null)
                {
                    rt.value = new Vec(at.value.getVector3()); //get rid of -999s. 
                    rt.baseValue = rt.value.getVector3();

                }
                else
                {
                    rt.value = new Vec(0);
                }

                if (at.start != null)
                {
                    //do we need both?
                    rt.start = new Vec(at.start.getVector3());
                    rt.startValue = rt.start.getVector3();
                }
                else
                {
                    rt.startValue = Vector3.zero;
                    rt.start = new Vec(0);
                }

                if (rt.startColor != null)
                {
                    rt.startColorValue = rt.startColor.getColor();
                }

                if (rt.color != null)
                {
                    rt.baseColorValue = rt.color.getColor();
                }

                if (at.startTime <= 0)
                {
                    rt.startValue = rt.baseValue;
                }

                rt.currentValue = rt.startValue;
                bool needsSequence = at.startTime > 0 || at.endTime > 0;
                updateRamps();
                if (needsSequence)
                {
                    if (rt.start == rt.value)
                    {
                        rt.startTime = 0;
                    }
                    if (rt.sequence != null)
                    {
                        rt.sequence.Kill();
                    }
                    rt.sequence = DOTween.Sequence();
                    AdditiveIntroSequence(rt);
                    if (!triggerObj.sustain && rt.endTime > 0)
                    {
                        AdditiveEndSequence(rt);
                    }
                }
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                foreach (ActiveTrigger at in this.triggers)
                {
                    TriggerObj t;
                    float mappedValue;
                    bool knobExists = false;
                    if (this.isCorrectBehaviour(at) && at.knob != null)
                    {
                        t = checkForCachedKnob(at);
                        at.knobValue = t != null ? t.value : 0;
                        mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * at.multiplier) : at.knobValue * at.multiplier;
                        Vector3 knobValue = at.knob.lerpVector3(mappedValue);
                        //testing one knob first. 
                        this.ramps[0].position = knobValue.x;
                        this.ramps[0].width = knobValue.y;
                        this.ramps[0].blur = knobValue.z;
                        knobExists = true;
                    }

                    if (at.knobColor == null)
                    {
                        this.ramps[0].color = Color.white;
                    }
                    else
                    {
                        mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * at.multiplier) : at.knobValue * at.multiplier;
                        this.ramps[0].color = at.knobColor.lerpColor(mappedValue);
                        knobExists = true;
                    }
                    if (knobExists)
                    {
                        updateShaderBuffer();
                    }
                }
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                if (Application.isPlaying)
                {
                    if (this.hasComponent && this.triggers != null && this.triggers.Count > 0
                        && this.rampTriggers != null && this.rampTriggers.Length > 0
                    )
                    {
                        Kill();
                        triggerController = base.getTriggerController();
                        this.checkForCachedKnobs(this.triggers[0]);

                        for (int i = 0; i < this.triggers.Count; i++)
                        {
                            ActiveTrigger at = this.triggers[i];
                            if (at.propertyName == "background" || at.propertyName == "backgroundColor")
                            {
                                introBackgroundColor(at, triggerObj);
                            }
                            else
                            {
                                introRampTrigger(at, triggerObj, i);
                            }
                        }

                    }
                }
                base.Intro(triggerObj, o);
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (this.isCorrectBehaviour(at) && at.trigger == knobEvent.eventName)
                    {
                        if (at.knob != null)
                        {
                            at.knobValue = knobEvent.value;
                            at.animatingKnob = true;
                            float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * at.multiplier) : at.knobValue * at.multiplier;
                            Vector3 knobValue = at.knob.lerpVector3(mappedValue);

                            this.ramps[0].position = knobValue.x;
                            this.ramps[0].width = knobValue.y;
                            this.ramps[0].blur = knobValue.z;
                        }

                        if (at.knobColor == null)
                        {
                            this.ramps[0].color = Color.white;
                        }
                        else
                        {
                            float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * at.multiplier) : at.knobValue * at.multiplier;
                            this.ramps[0].color = at.knobColor.lerpColor(mappedValue);
                        }
                    }
                }
            }


            private void updateRamps()
            {
                if (this.hasComponent && this.rampTriggers != null)
                {
                    //rampTriggers should be one less than numRamps. 
                    for (int i = 1; i < this.rampTriggers.Length; i++)
                    {
                        ActiveTrigger rt = this.rampTriggers[i];
                        if (rt.starting)
                        {
                            rt.currentValue = Vector3.Lerp(rt.startValue, rt.baseValue, rt.lerpValue);
                            this.ramps[i].position = rt.currentValue.x;
                            this.ramps[i].width = rt.currentValue.y;
                            this.ramps[i].blur = rt.currentValue.z;
                            rt.animatedColorValue = Color.Lerp(rt.startColorValue, rt.baseColorValue, rt.lerpValue);
                            this.ramps[i].color = rt.animatedColorValue;
                            updateShaderBuffer();
                        }

                        if (rt.ending)
                        {
                            rt.currentValue = Vector3.Lerp(rt.baseValue, rt.endValue, 1.0f - rt.lerpValue);
                            this.ramps[i].position = rt.currentValue.x;
                            this.ramps[i].width = rt.currentValue.y;
                            this.ramps[i].color = Color.Lerp(rt.baseColorValue, rt.endColorValue, rt.lerpValue);
                            this.ramps[i].blur = rt.currentValue.z;
                            updateShaderBuffer();
                        }

                        if (rt.target != null && rt.animatingTrigger)
                        {
                            rt.animatedValue = Vector3.Lerp(rt.baseValue, rt.targetValue, rt.triggerLerpValue);
                            this.ramps[i].position = rt.animatedValue.x;
                            this.ramps[i].width = rt.animatedValue.y;
                            this.ramps[i].blur = rt.animatedValue.z;
                            if (rt.baseColorValue != null)
                            {
                                rt.animatedColorValue = Color.Lerp(rt.baseColorValue, rt.targetColorValue, rt.triggerLerpValue);
                                this.ramps[i].color = rt.animatedColorValue;
                            }

                            updateShaderBuffer();
                        }

                    }
                }
            }

            public override void Update()
            {
                updateRamps();
                updateBackground();
            }

            private void updateBackground()
            {
                if (animatingBackground)
                {

                }
            }

            private void updateShaderBuffer()
            {
                if (this.ramps != null && this.ramps.Length > 0)
                {
                    if (rampBuffer != null)
                    {
                        rampBuffer.SetData(ramps);
                        this.material.SetBuffer("_Ramps", rampBuffer);
                    }

                    if (usingVolume)
                    {
                        this.displacementRamp.setRamps(ramps);
                    }
                }
            }

            public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                ActiveRamp _activeRamp = _instance.GetComponent<ActiveRamp>();
                if (_activeRamp == null)
                {
                    _activeRamp = _instance.AddComponent<ActiveRamp>();
                    _activeRamp.triggers = new List<ActiveTrigger>();
                    _activeRamp.instance = _instance;
                    _activeRamp.root = this.root;
                    _behaviours.Add(_activeRamp);
                }
                _activeRamp.triggers.Add(at);
            }

            public void updateValue(ActiveTrigger at, Vector3 _value, int rampIndex)
            {
                //replace with override when you convert to additive. 
                //although this might need to be DELETED - and we should another extender called ramps. 
                if (this.hasComponent && this.ramps.Length > 0)
                {
                    ActiveTrigger rt = rampTriggers[rampIndex];

                    if (rt.color != null)
                    {
                        rt.baseColorValue = rt.color.getColor();
                    }

                    if (rt.targetColor != null)
                    {
                        rt.targetColorValue = rt.targetColor.getColor();
                    }

                    if (rt.value == null || rt.value.x < 0)
                    {
                        rt.value = new Vec(0);
                    }
                    else
                    {
                        //gets rid of -999
                        //do we need both of these??
                        rt.value = new Vec(rt.value.getVector3());
                        rt.baseValue = rt.value.getVector3();
                    }

                    if (rt.target.x < 0)
                    {
                        rt.target = new Vec(0);
                    }
                    else
                    {
                        rt.target = new Vec(rt.target.getVector3());
                    }
                }
            }


            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (Application.isPlaying && a != null && this.hasComponent && a.target != null)
                {
                    Vector3 newTargetValue = a.target.getVector3();
                    rampIndex = rampIndex + 1;
                    rampIndex = (int)Mathf.Repeat(rampIndex, numRamps);
                    rampIndex = rampIndex == 0 ? 1 : rampIndex; //reserving 0 for knobs/constant 
                    if (a.propertyName == "constant")
                    {
                        //can specify constant ramp that will take over knob. 
                        rampIndex = 0;
                    }
                    rampSustains[rampIndex] = triggerObj.sustain;
                    ActiveTrigger rt = new ActiveTrigger(a);

                    rampTriggers[rampIndex] = rt;
                    rt.targetValue = newTargetValue;
                    rt.triggerSequence = DOTween.Sequence();
                    AdditiveTriggerSequence(rt, triggerObj.sustain);
                    rt.lastTargetValue = newTargetValue;
                    base.Trigger(triggerObj, rt, o);

                    if (rt.color != null)
                    {
                        rt.baseColorValue = rt.color.getColor();
                    }

                    if (rt.targetColor != null)
                    {
                        rt.targetColorValue = rt.targetColor.getColor();
                    }

                    if (rt.value == null || rt.value.x < 0)
                    {
                        rt.value = new Vec(0);
                    }
                    else
                    {
                        //gets rid of -999
                        //do we need both of these??
                        rt.value = new Vec(rt.value.getVector3());
                        rt.baseValue = rt.value.getVector3();
                    }

                    if (rt.target.x < 0)
                    {
                        rt.target = new Vec(0);
                    }
                    else
                    {
                        rt.target = new Vec(rt.target.getVector3());
                    }

                    if (!triggerObj.sustain)
                    {
                        AdditiveTriggerOffSequence(rt);
                    }
                }
            }

            public override void Release()
            {
                //release for the intro. 
                foreach (ActiveTrigger rt in this.rampTriggers)
                {
                    if (rt.value != null && rt.sequence != null && rt.end != null)
                    {
                        rt.sequence.Kill();
                        this.AdditiveEndSequence(rt);
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //handing sustain off for secondary triggers. 
                for (int i = 0; i < this.numRamps; i++)
                {
                    ActiveTrigger rt = this.rampTriggers[i];
                    if (this.rampSustains[i])
                    {
                        if (rt.triggerSequence != null)
                        {
                            rt.triggerSequence.Kill();
                        }
                        rt.triggerSequence = DOTween.Sequence();
                        rt.currentAnimatedValue = Vector3.zero;  //not sure if this should be zero. 
                        AdditiveTriggerOffSequence(rt);       //should this be only if sequence exists.  
                    }
                }
            }
        }
    }
}