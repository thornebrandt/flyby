using DG.Tweening;
using UnityEngine;
using Dreamteck.Splines;
using System.Collections.Generic;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Active
    {
        public class ActiveSpline : AdditiveActiveBehaviour
        {
            public SplinePositioner splinePositioner;
            public float splinePosition;

            public override void setupComponent()
            {
                if (this.splinePositioner == null)
                {
                    this.splinePositioner = GetComponent<SplinePositioner>();
                }
                if (this.splinePositioner != null)
                {
                    this.hasComponent = true;
                    this.splinePositioner.SetPercent(0.5f);
                }
            }

            private void updateSplinePosition()
            {
                splinePositioner.SetPercent(splinePosition);
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent)
                {
                    switch (at.propertyName)
                    {
                        case "position":
                        case "":
                            //HEADS UP - this empty string might cause bugs. 
                            splinePosition = _value.x;
                            break;

                    }
                }
            }
        }
    }
}