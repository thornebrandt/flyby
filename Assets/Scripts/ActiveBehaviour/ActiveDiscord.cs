using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Active
    {
        // TODO - not sure what to name this ( activeSocket? ) but it extends beyond discord now. 
        // this script is a good example of hijacking other activeBehaviours. position and scale of attached gameobject. 

        public class ActiveDiscord : ActiveBehaviour
        {
            public string chatTriggerString = "strum";
            public int collisionMIDIChannel = -1;
            public float minHeight = -4;
            public float maxHeight = 4;
            public float minLength = 5;
            public float maxLength = 90;
            public float minScale = 0.8f;
            public float maxScale = 3;
            public ActivePosition activePosition;
            public ActiveScale activeScale;
            private ActiveTrigger a;
            private float height; //normalized;
            private float length; //normalized;

            public override void setupActiveTriggers(Obj o)
            {
                this.instance = this.gameObject;

                if (this.activeScale == null)
                {
                    this.activeScale = this.GetComponent<ActiveScale>();
                }

                if (this.activeScale == null)
                {
                    Debug.Log("warning! activeScale doesn't exist in this discord object " + this.gameObject.name);
                }

                if (this.activePosition == null)
                {
                    this.activePosition = this.GetComponent<ActivePosition>();
                }

                if (this.activePosition == null)
                {
                    Debug.Log("warning! activePosition doesn't exist in this discord object " + this.gameObject.name);
                }

                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);
                        this.instance = this.gameObject;
                    }
                }

                //do we need these?
                this.activePosition.instance = this.gameObject;
                this.activePosition.o = o;
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                this.o = o;
                Kill();
                this.triggerController = base.getTriggerController();

                //TODO - check velocity
                if (triggerObj.position != null)
                {
                    //did this ever work ? 
                    this.height = 1.0f - triggerObj.position.y;
                    this.length = triggerObj.position.x;
                }
                else
                {
                    this.ParseChatMessage(triggerObj.message, out height, out length);
                }

                sendPosition(this.height);
                sendScale(this.length); //what is triggering this?
                base.Intro(triggerObj, o);  //experimental - might be what breaks discord. 
            }


            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                //this is not actually doing anything - knobs are coming from the position. 
                //I believe we just need "ActiveTypeNext" - but we need to turn off position for this.

                // foreach (ActiveTrigger at in this.triggers)
                // {
                //     TriggerObj t;
                //     float mappedValue;
                //     if (at.knob != null)
                //     {
                //         t = checkForCachedKnob(at);
                //         at.knobValue = t != null ? t.value : 0;
                //         mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * at.multiplier) : at.knobValue * at.multiplier;
                //         switch (at.propertyName)
                //         {
                //             case "length":
                //                 this.height = mappedValue;
                //                 sendScale(this.height);
                //                 break;
                //             case "height":
                //                 this.length = 1.0f - mappedValue;
                //                 sendPosition(this.length);
                //                 break;
                //         }
                //     }
                // }
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                // this is not actually doing anything - knobs are coming from the position. 
                // investigate when we need to do more per knob. 
                // I believe we just need "activeTypeNext" - but we need to turn off position for this. 

                // animatingKnob = true;
                // cacheKnob(knobEvent);
                // foreach (ActiveTrigger at in this.triggers)
                // {
                //     if (at.trigger == knobEvent.eventName)
                //     {
                //         switch (at.propertyName)
                //         {
                //             case "length":
                //                 this.height = at.triggerProperty.map(knobEvent.value);
                //                 sendScale(this.height);
                //                 break;
                //             case "height":
                //                 this.length = at.triggerProperty.map(knobEvent.value);
                //                 sendPosition(this.length);
                //                 break;
                //         }
                //     }
                // }
            }


            public void ParseChatMessage(string command, out float height, out float length)
            {

                Regex heightRegex = new Regex(@"--height (\d+(\.\d+)?)");
                Regex lengthRegex = new Regex(@"--length (\d+(\.\d+)?)");

                height = Random.value;
                length = Random.value;

                //return random if chat message does not exist. 
                if (string.IsNullOrEmpty(command))
                {
                    return;
                }

                if (!command.StartsWith(this.chatTriggerString))
                {
                    return;
                }

                Match heightMatch = heightRegex.Match(command);
                if (heightMatch.Success)
                {
                    float.TryParse(heightMatch.Groups[1].Value, out height);
                }

                Match sizeMatch = lengthRegex.Match(command);
                if (sizeMatch.Success)
                {
                    float.TryParse(sizeMatch.Groups[1].Value, out length);
                }

                return;
            }


            private void sendScale(float length)
            {
                //sends a start trigger to the activeScale.?
                //can it be an onTrigger?
                TriggerObj t = new TriggerObj();
                ActiveTrigger aScale = new ActiveTrigger();
                float scale = Mathf.Lerp(this.minScale, this.maxScale, length);
                aScale.value = new Vec(scale);
                aScale.startTime = 1f;
                aScale.endTime = Mathf.Lerp(this.minLength, this.maxLength, length);
                aScale.endDelay = 1;
                aScale.start = new Vec(0);
                aScale.end = new Vec(0);
                aScale.endEase = DG.Tweening.Ease.InQuad;
                aScale.hideOnComplete = true;
                this.activeScale.triggers = new List<ActiveTrigger>();
                this.activeScale.triggers.Add(aScale);
            }

            private void sendPosition(float height)
            {
                //sends a on trigger to the activePosition.
                TriggerObj t = new TriggerObj();
                ActiveTrigger ap = new ActiveTrigger();
                float yPosition = Mathf.Lerp(this.minHeight, this.maxHeight, height);
                Vec position = new Vec(0, yPosition, 0);
                ap.startTime = -1;
                ap.endTime = -1;
                ap.start = position;
                ap.value = position;
                ap.target = position;
                ap.onTime = 0.01f;
                ap.offTime = -1;
                this.activePosition.triggers = new List<ActiveTrigger>();
                this.activePosition.triggers.Add(ap);
                //this.activePosition.Trigger(t, ap, null);
            }
        }

    }
}


