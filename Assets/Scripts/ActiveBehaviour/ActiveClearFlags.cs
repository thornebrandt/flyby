using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using flyby.Triggers;
using flyby.Controllers;
using flyby.Core;

namespace flyby
{
    namespace Active
    {
        //QUICK GLITCH EFFECT
        //quick script inclusion that will turn off the clear flags. 
        //TODO - allow turning off and on  ( perhaps with value.x ) 
        //TODO - use a feedback post processing volume and attach to this so effect can be faded in. 

        public class ActiveClearFlags : ActiveBehaviour
        {
            private Camera cam;

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                CameraController cameraController = CameraController.instance;
                if (cameraController != null)
                {
                    cam = cameraController.cam;
                }
                else
                {
                    Debug.LogError("CameraController not found.");
                }

                if (cam != null)
                {
                    HDAdditionalCameraData hdCameraData = cam.GetComponent<HDAdditionalCameraData>();
                    if (hdCameraData != null)
                    {
                        hdCameraData.clearColorMode = HDAdditionalCameraData.ClearColorMode.None;
                    }
                    else
                    {
                        Debug.LogError("HDAdditionalCameraData component not found on the camera.");
                    }
                }
                else
                {
                    Debug.LogError("Camera component not found.");
                }
            }
        }
    }
}