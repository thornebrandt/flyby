using DG.Tweening;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActivePosition : ActiveBehaviour
        {
            // this script is used for both triggerPosition and triggerPivot. 
            // hierarchy matters. 


            private Vector3 position; //final calculated localPosition of popup object. 
            private Vector3 basePosition; //value of position, in between animations.
            private Vector3 startPosition;
            private Vector3 endPosition;
            public Vector3 currentPosition; //current position refers to preanimated start/stop/value
            private Vector3 targetPosition;
            private Vector3 lastTargetPosition;
            private Vector3 animatedPosition;
            private Vector3 targetKnobPosition;
            private Vector3 currentKnobPosition; //for lerping knobs. 
            private float knobLerpSpeed = 8; //TODO - consider making knobLerpSpeed part of activeTrigger or triggerProperty.

            public override void setupActiveTriggers(Obj o)
            {
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o != null && o.triggerPosition != null)
                    {
                        this.assignSecondaryTriggers(o.triggerPosition);
                    }
                    this.instance = this.gameObject;
                }
                if (Config.instance.testing)
                {
                    knobLerpSpeed = Mathf.Infinity;  //lots of tests failing because of the lerp speed. 
                }
            }

            public override void Update()
            {
                if (instantiated)
                {
                    if (animating || animatingTrigger || animatingKnob)
                    {
                        updatePosition(); //sometimes animating to zero. 
                        //animatingKnob = false;  //DEBUG. 
                    }
                }
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                Kill();
                triggerController = base.getTriggerController();
                //have not put in parents yet. 
                if (this.triggers == null)
                {
                    this.assignSecondaryTriggers(o.triggerPosition);
                }
                ActiveTrigger a = this.triggers.Count > 0 ? this.triggers[0] : new ActiveTrigger();
                if (a != null && !a.disabled)
                {
                    this.multiplier = getMultiplier(a);

                    lerpValue = 0;

                    if (o.hierarchyType == HierarchyType.None)
                    {
                        //affected by other scripts such as stack. 
                        basePosition = o.origin.getVector3() * multiplier;
                        if (o.origin.multiple != null)
                        {
                            basePosition = Vec.getNearestMultiple(basePosition, o.origin.multiple);
                        }
                    }

                    this.checkForCachedKnobs(a);

                    if (a.value != null)
                    {
                        basePosition = a.value.getVector3() * multiplier;
                        if (a.value.multiple != null)
                        {
                            basePosition = Vec.getNearestMultiple(basePosition, a.value.multiple);
                        }
                    }
                    if (a.start != null)
                    {
                        startPosition = a.start.getVector3() * multiplier;
                        if (a.start.multiple != null)
                        {
                            startPosition = Vec.getNearestMultiple(startPosition, a.start.multiple);
                        }
                        if (a.start.relative)
                        {
                            startPosition = Vec.Add(startPosition, basePosition);
                        }
                    }
                    else
                    {
                        //startPosition = Vector3.zero; //should be base position?
                        startPosition = basePosition;
                    }
                    if (a.end != null)
                    {
                        endPosition = a.end.getVector3() * multiplier;
                        if (a.end.multiple != null)
                        {
                            endPosition = Vec.getNearestMultiple(endPosition, a.end.multiple);
                        }
                    }
                    currentPosition = startPosition;
                    //TODO - what's going on here - why multtiple kills?
                    if (_sequence != null)
                    {
                        _sequence.Kill(true);
                    }
                    this.instance.transform.localPosition = startPosition + currentKnobPosition;
                    // if (_sequence != null) {
                    //     _sequence.Kill();
                    // }
                    _sequence = DOTween.Sequence();
                    if (a.start == a.value)
                    {
                        a.startTime = 0;
                    }
                    base.IntroSequence(_sequence, a);
                    if (!triggerObj.sustain && a.end != null && a.endTime > 0)
                    {
                        base.EndSequence(_sequence, a);
                    }
                }
                updatePosition();
                base.Intro(triggerObj, o);
            }

            public void updatePosition()
            {
                if (starting)
                {
                    currentPosition = Vector3.Lerp(startPosition, basePosition, lerpValue);
                }
                if (ending)
                {
                    currentPosition = Vector3.Lerp(basePosition, endPosition, 1.0f - lerpValue);
                }

                if (animatingTrigger && targetPosition != null)
                {
                    animatedPosition = Vector3.Lerp(currentPosition, targetPosition, triggerLerpValue);
                }
                else
                {
                    animatedPosition = currentPosition;
                }
                //TODO - check for using knobs before calculating this every frame. 
                if (usingKnobs)
                {
                    currentKnobPosition = Vector3.Lerp(currentKnobPosition, targetKnobPosition, Time.deltaTime * knobLerpSpeed);
                }
                else
                {
                    currentKnobPosition = Vector3.zero;
                }
                position = (animatedPosition + currentKnobPosition);
                instance.transform.localPosition = position;
            }

            public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                ActivePosition _activePosition = _instance.GetComponent<ActivePosition>();
                if (_activePosition == null)
                {
                    _activePosition = _instance.AddComponent<ActivePosition>();
                    _activePosition.triggers = new List<ActiveTrigger>();
                    _activePosition.instance = _instance;
                    _activePosition.root = this.root;
                    _behaviours.Add(_activePosition);
                }
                _activePosition.triggers.Add(at);
            }

            public override void updateKnobValue(Vector3 addedKnobsValue)
            {
                //base class calls this after running through cached knobs. 
                targetKnobPosition = addedKnobsValue;
                currentKnobPosition = addedKnobsValue;
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                this.usingKnobs = true;
                animatingKnob = true;
                targetKnobPosition = Vector3.zero;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        float mappedKnobValue = at.mapKnobValue();
                        targetKnobPosition += at.knob.lerpVector3(mappedKnobValue);
                    }
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (a.target != null)
                {
                    this.multiplier = getMultiplier(a);
                    Vector3 newTargetPosition = a.target.getVector3() * multiplier;
                    Vector3 differenceAfterKill = Vector3.zero;

                    if (_triggerSequence != null && o != null)
                    {
                        if (o.stack != null && a.target.relative && o.hierarchyType == HierarchyType.None)
                        {
                            // this is such a hack. position should not have knowledge of stack.  
                            // doing some weird stuff.
                            //o.stack.checkStack(o); //force a change. 
                            Vector3 positionBeforeKill = instance.transform.localPosition; //experimental.
                            _triggerSequence.Kill(true); //true is controversial. 
                            differenceAfterKill = instance.transform.localPosition - positionBeforeKill;
                            instance.transform.localPosition = positionBeforeKill; //experimental return

                        }
                        else
                        {
                            _triggerSequence.Kill();
                        }
                    }

                    if (a.target.relative)
                    {
                        if (o.stack != null && o.hierarchyType == HierarchyType.None)
                        {
                            //need to continue this for all positions. 
                            Vector3 subtractedFullStack = new Vector3();
                            if (differenceAfterKill.x >= o.stack.boundaries.x2)
                            {
                                subtractedFullStack.x = o.stack.fullStack.x;
                            }
                            if (differenceAfterKill.x <= o.stack.boundaries.x)
                            {
                                subtractedFullStack.x = -o.stack.fullStack.x;
                            }
                            if (differenceAfterKill.y >= o.stack.boundaries.y2)
                            {
                                subtractedFullStack.y = o.stack.fullStack.y;
                            }
                            if (differenceAfterKill.y <= o.stack.boundaries.y)
                            {
                                subtractedFullStack.y = -o.stack.fullStack.y;
                            }
                            if (differenceAfterKill.z >= o.stack.boundaries.z2)
                            {
                                subtractedFullStack.z = o.stack.fullStack.z;
                            }
                            if (differenceAfterKill.z <= -o.stack.fullStack.z)
                            {
                                subtractedFullStack.z = -o.stack.fullStack.z;
                            }

                            newTargetPosition = instance.transform.localPosition + newTargetPosition + differenceAfterKill - subtractedFullStack;
                        }
                        else
                        {
                            //newTargetPosition = instance.transform.localPosition + newTargetPosition + differenceAfterKill;
                            newTargetPosition = instance.transform.localPosition + newTargetPosition;
                        }
                    }

                    if (a.target.multiple != null)
                    {
                        //jump to nearest round number. 
                        newTargetPosition = Vec.getNearestMultiple(newTargetPosition, a.target.multiple);
                    }


                    //check for permanent triggers, or new triggers to keep transitions fluid. 
                    if (a.offTime <= 0 || (lastTrigger != null && lastTrigger != a))
                    {
                        triggerLerpValue = 0;
                        currentPosition = instance.transform.localPosition;
                    }

                    //check for self randomized position. 
                    if (animatingTrigger && lastTargetPosition != newTargetPosition)
                    {
                        starting = false;
                        triggerLerpValue = 0;
                        currentPosition = instance.transform.localPosition;
                    }

                    _triggerSequence = DOTween.Sequence();
                    targetPosition = newTargetPosition;
                    TriggerSequence(_triggerSequence, a);
                    lastTargetPosition = newTargetPosition;
                    base.Trigger(triggerObj, a, o);
                    if (!triggerObj.sustain && a.offTime >= 0)
                    {
                        TriggerOffSequence(_triggerSequence, a);
                    }
                    a.timesTriggered++; //experimental. 

                }
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (instance != null)
                {
                    if (a.offTime < 0)
                    {
                        //its not coming back. this is our new home. 
                        currentPosition = instance.transform.localPosition;
                    }
                    else
                    {
                        currentPosition = basePosition;
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //handling sustain off - move currentPosition back to base.
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }

                if (a.offTime > -1)
                {
                    _triggerSequence = DOTween.Sequence();
                    currentPosition = basePosition;
                    TriggerOffSequence(_triggerSequence, a);
                }
                else
                {
                    //its not coming back. this is our new home.
                    triggerLerpValue = 0;
                    currentPosition = instance.transform.localPosition;
                }
            }

            public override void Kill()
            {
                // how has position survived without a kill?
                // might need for knobs. 

                base.Kill();
            }
        }
    }
}