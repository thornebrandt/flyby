using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using UnityEngine.Rendering.HighDefinition;

namespace flyby
{
    namespace Active
    {
        public class ActiveShader : AdditiveActiveBehaviour
        {
            private Renderer rend;
            private Material material;

            public override void setupComponent()
            {
                this.material = this.getMaterial();
                if (this.material != null)
                {
                    this.hasComponent = true; //attempting to bypass material need. 
                    return;
                }
            }

            private Material getMaterial()
            {
                //TODO - figure out how to make less WET - 

                if (this.material != null) return material;
                if (instance != null) instance = this.gameObject;

                if (rend == null)
                {
                    rend = GetComponent<Renderer>();
                }

                //first check if normal renderer and return this.material.
                if (rend != null)
                {
                    if (rend.material.name.EndsWith("(Instance)")) return rend.material; // a hack to prevent double instantiation. 

                    this.material = new Material(rend.material);
                    rend.material = this.material;
                    this.material.name += "(Instance)";
                    return this.material;
                }

                if (rend == null)
                {
                    DecalProjector decalProjector = this.GetComponent<DecalProjector>();
                    if (decalProjector != null)
                    {
                        if (decalProjector.material.name.EndsWith("(Instance)")) return decalProjector.material; //a hack to prevent double instantiation. 
                        this.material = new Material(decalProjector.material);
                        this.material.name += "(Instance)";
                        decalProjector.material = this.material;
                        return this.material;
                    }
                    else
                    {
                        Debug.Log("can't find any material on " + this.gameObject.name);
                        return null;
                    }
                }

                return this.material;
            }

            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                Vector3 value = new Vector3();
                if (this.hasComponent && this.material != null)
                {
                    if (material.HasVector(at.propertyName))
                    {
                        value = material.GetVector(at.propertyName);
                    }
                    else
                    {
                        value.x = material.GetFloat(at.propertyName);
                    }
                }
                return value;
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent && this.material != null)
                {
                    if (material.HasVector(at.propertyName))
                    {
                        //SET VECTOR3
                        this.material.SetVector(at.propertyName, _value);
                    }
                    else
                    {
                        this.material.SetFloat(at.propertyName, _value.x);
                    }
                }
            }
        }
    }
}

