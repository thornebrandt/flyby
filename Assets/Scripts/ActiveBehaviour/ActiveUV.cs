using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;

namespace flyby
{
    namespace Active
    {
        public class ActiveUV : AdditiveActiveBehaviour
        {

            public string textureMap = "_UnlitColorMap";
            //others include "_BaseColorMap"
            private Renderer rend;
            private Material material;
            private Media media;
            private Vector2 zoomOffset; //keep this offset so we zoom in on the center of the texture. 
            private Vector2 currentOffset; //triggered offset. 
            private Vector3 finalOffset; //after calculating zoomOffset and currentOffset. 
            private AssetController assetController;


            public override void setupComponent()
            {
                this.rend = GetComponent<Renderer>();
                if (this.rend != null)
                {
                    this.material = rend.material;
                    if (this.material != null)
                    {

                        if (this.triggers != null && this.triggers.Count > 0)
                        {

                            this.assetController = getAssetController();
                            int i = 0;
                            // in case we want to set media from activUV. 
                            // not really needed. 
                            foreach (ActiveTrigger at in this.triggers)
                            {
                                if (at.media != null)
                                {
                                    media = at.media;
                                    media.loadedTexture = this.assetController.LoadTexture(media.path);
                                    rend.material.SetTexture(this.textureMap, media.loadedTexture);
                                }
                                i++;
                            }
                        }
                        this.zoomOffset = Vector2.zero;
                        this.currentOffset = Vector2.zero;


                        //getting current offset. 
                        currentOffset = this.material.GetTextureOffset(textureMap);
                        Vector2 textureScale = this.material.GetTextureScale(textureMap);
                        this.zoomOffset = -0.5f * textureScale;
                        this.finalOffset = this.zoomOffset + this.currentOffset; //also adjust offset so we zoom into the center.


                        this.hasComponent = true;
                    }
                }
            }


            private AssetController getAssetController()
            {
                if (this.assetController != null)
                {
                    return this.assetController;
                }
                if (AssetController.instance != null)
                {
                    return AssetController.instance;
                }
                return null;
            }

            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                if (this.hasComponent && this.rend != null)
                {
                    switch (at.propertyName)
                    {
                        case "scroll":
                        case "offset":
                        case "":
                            return new Vector3(currentOffset.x, currentOffset.y, 0);
                        case "tile":
                        case "zoom":
                            Vector2 ts = this.material.GetTextureScale(textureMap);
                            return new Vector3(ts.x, ts.y, 0);
                    }
                }
                return base.getCurrentValue(at);
            }

            public override void calculateValue(ActiveTrigger at)
            {
                switch (at.propertyName)
                {
                    case "scroll":
                    case "offset":
                    case "":
                        this.addValues(at);
                        break;
                    case "zoom":
                    case "tile":
                        this.multiplyValues(at);
                        break;
                    default:
                        this.addValues(at);
                        break;

                }
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent && this.rend != null && this.material != null)
                {
                    switch (at.propertyName)
                    {
                        case "scroll":
                        case "offset":
                        case "":
                            this.currentOffset = new Vector2(_value.x, _value.y);
                            this.finalOffset = this.zoomOffset + this.currentOffset; //also adjust offset so we zoom into the center.
                            this.material.SetTextureOffset(textureMap, finalOffset);
                            break;
                        case "zoom":
                        case "tile":
                            this.material.SetTextureScale(textureMap, new Vector2(_value.x, _value.y));
                            this.zoomOffset = -0.5f * _value;
                            this.finalOffset = this.zoomOffset + this.currentOffset; //also adjust offset so we zoom into the center.
                            this.material.SetTextureOffset(textureMap, finalOffset);
                            break;
                    }
                }
            }
        }
    }
}

