using DG.Tweening;
using UnityEngine;
using flyby.Core;
using System.Collections.Generic;
using flyby.Triggers;
using System.IO;
using flyby.Controllers;
using Newtonsoft.Json;

namespace flyby
{
    namespace Active
    {
        public class TimeMapping : ActiveBehaviour
        {
            // meant for making a new triggers file mapped from a live performance. 
            // for recording at high resolution to match a midi file. 
            public string defaultPath = "/timeTriggers/timeTriggers.json";
            private string path = "";
            public List<Trigger> savedTriggers;
            public float timeOffset;


            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                this.assignSecondaryTriggers(o.activeTrigger);
                savedTriggers = new List<Trigger>();
                timeOffset = Time.time;
            }

            public void OnDisable()
            {
                saveTimeMappedTriggers();
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                Trigger t = new Trigger();
                t.time = Time.time - timeOffset;
                t.id = triggerObj.eventName + t.time;
                t.eventName = triggerObj.eventName;
                t.sustain = triggerObj.sustain;
                t.noteOff = triggerObj.noteOff;
                //t.group = triggerObj.group;
                savedTriggers.Add(t);
            }

            private void saveTimeMappedTriggers()
            {
                var settings = new Newtonsoft.Json.JsonSerializerSettings();
                settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                settings.Formatting = Formatting.Indented;
                if (o != null && o.activeTrigger != null && o.activeTrigger.valueString != null)
                {
                    path = o.activeTrigger.valueString;
                }
                else
                {
                    path = defaultPath;
                }
                string savedTriggersJSON = JsonConvert.SerializeObject(savedTriggers, settings);
                string fullSavePath = Config.instance.jsonPath + "/" + path;
                Debug.Log("fullSvaePath: " + fullSavePath);
                File.WriteAllText(fullSavePath, savedTriggersJSON);
                Debug.Log("Saved to : " + Config.instance.jsonPath);
            }

        }
    }
}
