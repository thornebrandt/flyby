using DG.Tweening;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveLayer : ActiveBehaviour
        {
            private string originalLayer = "Default";
            private string currentLayer;
            private string startLayer;
            private string endLayer;

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                this.o = o;
                Kill();
                this.triggerController = base.getTriggerController();
                if (this.triggers == null)
                {
                    this.assignSecondaryTriggers(o.triggerParent);
                }

                ActiveTrigger a = this.triggers[0];

                if (a != null)
                {
                    lerpValue = 0; // we're still using lerp and dotween so we can cancel timers. 
                    this.originalLayer = Tools.Scene.getLayerName(this.instance);
                    if (!string.IsNullOrEmpty(a.valueString))
                    {
                        currentLayer = a.valueString;
                    }
                    else
                    {
                        currentLayer = originalLayer;
                    }

                    if (!string.IsNullOrEmpty(a.startString))
                    {
                        startLayer = a.startString;
                    }
                    else
                    {
                        startLayer = currentLayer;
                    }

                    if (!string.IsNullOrEmpty(a.endString))
                    {
                        endLayer = a.endString;
                    }

                    if (_sequence != null)
                    {
                        _sequence.Kill();
                    }

                    _sequence = DOTween.Sequence();

                    if (startLayer == currentLayer)
                    {
                        a.startTime = 0;
                    }

                    Tools.Scene.setLayer(this.instance, this.startLayer);
                    base.IntroSequence(_sequence, a);
                    if (!triggerObj.sustain && !string.IsNullOrEmpty(endLayer) && a.endTime > 0)
                    {
                        base.EndSequence(_sequence, a);
                    }
                    base.Intro(triggerObj, o);
                }
            }

            public override void OnIntroComplete(ActiveTrigger a)
            {
                if (instance != null)
                {
                    Tools.Scene.setLayer(this.instance, this.currentLayer);
                }
            }

            public override void OnEndComplete(ActiveTrigger a)
            {
                if (instance != null)
                {
                    Tools.Scene.setLayer(this.instance, this.endLayer);
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //TODO - use similar "hasValue" and "getSetValue" as additiveBehaviour

                if (a.targetString != null)
                {
                    //Transform targetParent = getParent(a.targetString);
                    if (_triggerSequence != null)
                    {
                        _triggerSequence.Kill();
                    }

                    _triggerSequence = DOTween.Sequence();
                    TriggerSequence(_triggerSequence, a);

                    if (!triggerObj.sustain && a.offTime >= 0)
                    {
                        TriggerOffSequence(_triggerSequence, a);
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //TODO - this should probably be the generic, probaly just need global methods such as .
                //OnTriggerStart and OnTriggerOff
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                TriggerOffSequence(_triggerSequence, a);
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                //we can't really transition in between frames so this is just a timer. 
                if (!string.IsNullOrEmpty(a.targetString))
                {
                    Tools.Scene.setLayer(this.instance, a.targetString);
                }
            }

            public override void OnTriggerOffComplete(ActiveTrigger a)
            {
                //this value should always exist. 
                Tools.Scene.setLayer(this.instance, this.currentLayer);
            }

        }
    }
}