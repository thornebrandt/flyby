using flyby.Controllers;
using DG.Tweening;
using flyby.Core;
using flyby.Triggers;
using System.Collections.Generic;
using UnityEngine;

namespace flyby
{
    namespace Active
    {
        public class ActiveBehaviour : MonoBehaviour
        {
            public string behaviour_id;  //used to filter mutliple custom behaviours. 
            public Obj o;
            [HideInInspector]
            public bool instantiated; //intro has been fired.
            [HideInInspector]
            public bool fresh; //brief moment before any animation.
            [HideInInspector]
            public bool introduced; //should it be called ended?  
            [HideInInspector]
            public bool ended;
            [HideInInspector]
            public bool triggered; //a trigger has completed. 
            [HideInInspector]
            public bool animating;
            [HideInInspector]
            public bool animatingTrigger;
            [HideInInspector]
            public bool animatingKnob;
            [HideInInspector]
            public bool starting;
            [HideInInspector]
            public bool ending;
            [HideInInspector]
            public float lerpValue;
            [HideInInspector]
            public float triggerLerpValue;
            [HideInInspector]
            public bool on; //active trigger animation is completed
            [HideInInspector]
            public int _i; //instance index (from obj )
            [HideInInspector]
            public ActiveTrigger lastTrigger;
            [HideInInspector]
            public ActiveTrigger lastColorTrigger;
            public List<ActiveTrigger> triggers;  //list of active triggers. 
            [HideInInspector]
            public GameObject instance; //acting instance to apply behaviours to. 
            [HideInInspector]
            public GameObject root; //root gameobject ( base prefab, bottom of hierarchy ) 
            public Sequence _sequence;
            public Sequence _triggerSequence;
            [HideInInspector]
            public TriggerController triggerController;
            public float multiplier = 1;
            private bool _usingColor;
            [HideInInspector]
            public bool usingKnobs;

            //for custom trigger behaviour. can be called a second time in tests. 
            public virtual void setupActiveTriggers(Obj o)
            {
                if (this.triggers != null)
                {
                    return;
                }
                this.assignSecondaryTriggers(o.activeTrigger); //experimental.
            }

            public virtual void Intro(TriggerObj triggerObj, Obj o)
            {
                this.o = o;
                if (this.triggers != null && this.triggers.Count > 0)
                {
                    this.multiplier = getMultiplier(this.triggers[0]);
                }
                instantiated = true;
                fresh = true;
                resetTriggers();
            }

            private void resetTriggers()
            {
                if (this.triggers != null)
                {
                    foreach (ActiveTrigger a in this.triggers)
                    {
                        a.timesTriggered = 0;
                    }
                }
            }

            public virtual float getMultiplier(ActiveTrigger a)
            {
                return Tools.Rand.Between(a.multiplier, a.multiplier2);
            }

            public virtual void TriggerEventHandler(TriggerObj triggerObj, Obj o)
            {
                this.o = o;
                if (this.instance == null && this.gameObject != null)
                {
                    this.instance = this.gameObject;
                }

                if (this.triggers != null && this.triggers.Count > 0)
                {
                    foreach (ActiveTrigger at in triggers)
                    {
                        if (triggerObj.eventName == at.trigger)
                        {
                            if (at.disabled)
                            {
                                continue;
                            }

                            bool meetsFilter = true;

                            if (!string.IsNullOrEmpty(at.group))
                            {
                                int groupIndex = FlyByController.instance.currentPart.getCurrentGroupIndex(at.group);
                                if (at.groupIndex != groupIndex)
                                {
                                    meetsFilter = false;
                                }
                            }

                            if (!string.IsNullOrEmpty(at.behaviour))
                            {
                                if (at.behaviour != this.behaviour_id)
                                {
                                    meetsFilter = false;
                                }
                            }

                            if (meetsFilter)
                            {
                                FlyByController.instance.currentPart.assignIncrementToGroupIndex(at.group);
                                handleTrigger(triggerObj, at);
                            }

                        }
                    }
                }
            }

            public void handleTrigger(TriggerObj triggerObj, ActiveTrigger a)
            {
                if (triggerObj.noteOff && triggerObj.sustain)
                {
                    //if there is a note off in the obj, trigger note off. 
                    //should probably be renamed to "release"
                    TriggerOff(triggerObj, a, o);
                    return;
                };

                if (triggerObj.triggerEnd)
                {
                    EndTrigger(triggerObj);
                }

                switch (triggerObj.source)
                {
                    case "midiKnob":
                    case "audio":
                    case "socketKnob":
                    case "kinectKnob":
                    case "LFO":
                    case "lfo":
                        if (triggerObj.value >= 0)
                        {
                            TriggerKnob(triggerObj, a, o);
                        }
                        break;
                    case "keyUp":
                    case "midiNoteOff":
                    case "socketOff":
                    case "kinectOff":
                        //also handles note off and key up here. 
                        // new conditional. 
                        if (triggerObj.sustain)
                        {
                            TriggerOff(triggerObj, a, o);
                        }
                        break;
                    case "midiNoteOn":
                    case "keyDown":
                    case "time":
                    case "audioHit":
                    case "twitch":
                    case "discord":
                    case "socketOn":
                    case "kinectOn":
                    default:
                        Trigger(triggerObj, a, o);
                        break;

                }
            }

            public virtual void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //this is called after the triggerObj event has passed the filters above ( in TriggerEventHandler.) 
                this.multiplier = getMultiplier(a);
                if (a.target != null)
                {
                    lastTrigger = a;
                }
                if (a.targetColor != null)
                {
                    lastColorTrigger = a;
                }
                fresh = false;
            }

            //release function for secondary triggers 
            public virtual void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o) { }

            public virtual void TriggerKnob(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                fresh = false;
            }

            //callback after trigger off delay. 
            public virtual void OnTriggerOff(ActiveTrigger a) { }

            //call back after end delay. 
            public virtual void OnEnd(ActiveTrigger a) { }

            public virtual void OnEndComplete(ActiveTrigger a) { }

            public virtual void OnTriggerComplete(ActiveTrigger a) { }

            public virtual void OnTriggerOffComplete(ActiveTrigger a) { }

            public virtual void OnIntroComplete(ActiveTrigger a) { }

            public virtual void Release()
            {
                // release is sustain used in main envelope   start -> value -> end
                if (_sequence != null)
                {
                    _sequence.Kill();
                }

                if (this.triggers != null && this.triggers.Count > 0 && this.triggers[0] != null && this.triggers[0].end != null)
                {
                    _sequence = DOTween.Sequence();
                    EndSequence(_sequence, this.triggers[0]);
                }
            }

            public virtual void EndTrigger(TriggerObj triggerObj)
            {
                if (_sequence != null)
                {
                    _sequence.Kill();

                    if (this.triggers[0] != null && this.triggers[0].end != null)
                    {
                        _sequence = DOTween.Sequence();
                        if (this.triggers[0].endTime < 0)
                        {
                            //what is this doing??
                            ActiveTrigger forceEndTrigger = new ActiveTrigger(this.triggers[0]);
                            forceEndTrigger.endTime = -forceEndTrigger.endTime;
                            EndSequence(_sequence, forceEndTrigger);
                        }
                        else
                        {
                            EndSequence(_sequence, this.triggers[0]);
                        }
                    }
                }

            }

            public virtual void Update() { }

            public virtual void IntroSequence(Sequence sequence, ActiveTrigger a)
            {
                if (a.disabled)
                {
                    //attempting to disable most behaviours
                    return;
                }

                float _startTime = Tools.Rand.Between(a.startTime, a.startTime2);
                float _startDelay = Tools.Rand.Between(a.startDelay, a.startDelay2);
                _sequence.Append((
                            DOTween.To(() => lerpValue, x => lerpValue = x, 1.0f, _startTime)
                                .SetDelay(_startDelay)
                                .SetEase(a.startEase)
                                .OnStart(() =>
                                {
                                    animating = true;
                                    starting = true;
                                    fresh = false;
                                })
                                .OnComplete(() =>
                                {
                                    Update();
                                    animating = false;
                                    introduced = true;
                                    starting = false;
                                    fresh = false;
                                    OnIntroComplete(a);
                                })
                       ));
            }

            public virtual TriggerController getTriggerController()
            {
                if (triggerController != null)
                {
                    return triggerController;
                }
                if (TriggerController.instance != null)
                {
                    return TriggerController.instance;
                }

                if (FlyByController.instance != null)
                {
                    return FlyByController.instance.triggerController;
                }

                return null;
            }


            public virtual void EndSequence(Sequence sequence, ActiveTrigger a)
            {

                float _endTime = Tools.Rand.Between(a.endTime, a.endTime2);

                _sequence.Append(
                   DOTween.To(() => lerpValue, x => lerpValue = x, 0.0f, _endTime)
                    .SetDelay(a.endDelay)
                    .OnStart(() =>
                    {
                        animating = true;
                        ending = true;
                        OnEnd(a);
                    })
                    .SetEase(a.endEase)
                    .OnComplete(() =>
                    {
                        Update();
                        animating = false;
                        ending = false;
                        ended = true;
                        OnEndComplete(a);
                        if (a.hideOnComplete)
                        {
                            //removing experimental. works after making obj setActive after intro. 
                            Hide();
                        }
                    })
               );
            }

            public virtual void AdditiveIntroSequence(ActiveTrigger a)
            {
                float _startTime = Tools.Rand.Between(a.startTime, a.startTime2);
                a.sequence.Append((
                    DOTween.To(() => a.lerpValue, x => a.lerpValue = x, 1.0f, _startTime)
                        .SetDelay(a.startDelay)
                        .SetEase(a.startEase)
                        .OnStart(() =>
                        {
                            a.animating = true;
                            a.starting = true;
                            a.fresh = false;
                        })
                        .OnComplete(() =>
                        {
                            Update(); //sketch.
                            a.animating = false;
                            a.introduced = true;
                            a.starting = false;
                            a.fresh = false;
                            OnIntroComplete(a);
                        })
                ));
            }


            public virtual void AdditiveEndSequence(ActiveTrigger a)
            {
                float _endTime = Tools.Rand.Between(a.endTime, a.endTime2);
                a.sequence.Append(
                    DOTween.To(() => a.lerpValue, x => a.lerpValue = x, 0.0f, _endTime)
                    .SetDelay(a.endDelay)
                    .SetEase(a.endEase)
                    .OnStart(() =>
                    {
                        a.animating = true;
                        a.ending = true;
                        OnEnd(a);
                    })
                    .OnComplete(() =>
                    {
                        Update();
                        a.animating = false;
                        a.ending = false;
                        a.ended = true;
                        OnEndComplete(a);  //new, experimental, but seems very much needed ( used by activeKinect ) 
                        if (a.hideOnComplete)
                        {
                            Hide();
                        }
                    })
                );
            }





            public virtual void TriggerSequence(Sequence triggerSequence, ActiveTrigger a)
            {
                TriggerSequence(triggerSequence, a, false);
            }

            public virtual void TriggerSequence(Sequence triggerSequence, ActiveTrigger a, bool sustain)
            {
                _triggerSequence = triggerSequence;
                float _onTime = Tools.Rand.Between(a.onTime, a.onTime2);
                float _onDelay = Tools.Rand.Between(a.onDelay, a.onDelay2);
                _triggerSequence.Append(
                        DOTween.To(() => triggerLerpValue, x => triggerLerpValue = x, 1.0f, _onTime)
                        .SetDelay(_onDelay)
                        .OnStart(() =>
                        {
                            animatingTrigger = true;
                        })
                        .SetEase(a.onEase)
                        .OnComplete(() =>
                        {
                            Update();
                            on = true;
                            if (!sustain)
                            {
                                if (a.offTime <= 0)
                                {
                                    //kind of want to remove this.  and handle individually in onTriggerComplete. 
                                    animatingTrigger = false;
                                    //triggerLerpValue = 0; //removed to make retrigger animator tests work. 
                                }
                                OnTriggerComplete(a);
                                if (a.hideOnTarget)
                                {
                                    Hide();
                                }
                            }
                        })
                    );
            }

            public virtual void TriggerOffSequence(Sequence triggerSequence, ActiveTrigger a)
            {
                _triggerSequence = triggerSequence;
                float _offTime = Tools.Rand.Between(a.offTime, a.offTime2);
                _triggerSequence.Append(
                    DOTween.To(() => triggerLerpValue, x => triggerLerpValue = x, 0.0f, _offTime)
                        .OnStart(() =>
                        {
                            //after ontriggercomplete and trigger delay. 
                            OnTriggerOff(a);
                        })
                        .SetEase(a.offEase)
                        .SetDelay(a.offDelay)
                        .OnComplete(() =>
                        {
                            Update();
                            on = false;
                            animatingTrigger = false;
                            triggered = true;
                            OnTriggerOffComplete(a);
                        })
                );
            }

            public virtual void AdditiveTriggerSequence(ActiveTrigger a, bool sustain)
            {
                float _onTime = Tools.Rand.Between(a.onTime, a.onTime2);
                a.triggerSequence.Append(
                        DOTween.To(() => a.triggerLerpValue, x => a.triggerLerpValue = x, 1.0f, _onTime)
                        .SetEase(a.onEase)
                        .SetDelay(a.onDelay)
                        .OnStart(() =>
                        {
                            a.animatingTrigger = true;
                        })
                        .OnComplete(() =>
                        {
                            Update();
                            a.on = true;
                            if (!sustain)
                            {
                                if (a.offTime <= 0)
                                {
                                    a.animatingTrigger = false;
                                    a.triggerLerpValue = 0;
                                }
                                OnTriggerComplete(a);
                            }
                        })
                    );
            }

            public virtual void AdditiveTriggerOffSequence(ActiveTrigger a)
            {
                float _offTime = Tools.Rand.Between(a.offTime, a.offTime2);
                a.triggerSequence.Append(
                    DOTween.To(() => a.triggerLerpValue, x => a.triggerLerpValue = x, 0.0f, _offTime)
                        .OnStart(() =>
                        {
                            //after ontriggercomplete and trigger delay. 
                            a.animatingTriggerOff = true;
                            OnTriggerOff(a);
                        })
                        .SetEase(a.offEase)
                        .SetDelay(a.offDelay)
                        .OnComplete(() =>
                        {
                            Update();
                            a.on = false;
                            a.animatingTrigger = false;
                            a.animatingTriggerOff = false;
                            a.triggered = true;
                        })
                );
            }

            public virtual TriggerObj getCachedKnob(TriggerObj t)
            {
                if (t == null)
                {
                    return new TriggerObj();
                }
                if (string.IsNullOrEmpty(t.eventName) || this.triggerController == null)
                {
                    return t;
                }
                return triggerController.getCachedKnob(t);
            }

            public virtual TriggerObj checkForCachedKnob(ActiveTrigger a)
            {
                TriggerObj t;

                if (a == null)
                {
                    t = new TriggerObj();
                    t.eventName = a.trigger; //this line doesnt really make sense.
                    t.knob = -2;
                    return t;
                }

                if (string.IsNullOrEmpty(a.trigger) || this.triggerController == null)
                {
                    t = new TriggerObj();
                    t.eventName = a.trigger;
                    t.knob = -3;
                    return t;
                }
                return triggerController.getCachedKnob(a);
            }

            public virtual void cacheKnob(TriggerObj t)
            {
                this.triggerController = getTriggerController();
                if (this.triggers != null)
                {
                    foreach (ActiveTrigger at in this.triggers)
                    {
                        if (at.knob != null || at.knobColor != null)
                        {
                            if (this.isCorrectBehaviour(at) && at.trigger == t.eventName)
                            {
                                at.animatingKnob = true;
                                at.knobValue = t.value;
                                if (this.triggerController != null)
                                {
                                    this.triggerController.cacheKnob(at);
                                }
                            }
                        }

                    }
                }
                else
                {
                    Debug.Log("no triggers yet!");
                }
            }

            public bool isCorrectBehaviour(ActiveTrigger at)
            {
                //filters for correct. behaviour ids. 
                if (string.IsNullOrEmpty(at.behaviour))
                {
                    return true;
                }
                return at.behaviour == this.behaviour_id;
            }

            public virtual void checkForCachedKnobs(ActiveTrigger parentA)
            {
                //this is overridden a lot, but you can use it as a model. 
                this.triggerController = getTriggerController();
                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }

                //TODO - split here between "additive and override"
                Vector3 addedKnobsValue = Vector3.zero;

                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null || at.knobColor != null)
                    {
                        if (this.isCorrectBehaviour(at))
                        {
                            this.usingKnobs = true;
                            TriggerObj t = checkForCachedKnob(at);
                            at.knobValue = t != null ? t.value : at.knobValue;
                            float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * multiplier) : at.knobValue * multiplier;
                            addedKnobsValue = addedKnobsValue + at.knob.lerpVector3(mappedValue);
                        }

                    }
                }

                updateKnobValue(addedKnobsValue);
            }

            public virtual List<ActiveBehaviour> assignChildBehaviours()
            {
                List<ActiveBehaviour> behaviours = new List<ActiveBehaviour>();
                GameObject _instance;
                //we are pulling triggers out of this and assigning to children. 
                List<ActiveTrigger> tempTriggers = new List<ActiveTrigger>(this.triggers);
                int removedItems = 0;
                if (tempTriggers.Count == 0)
                {
                    return behaviours;
                }
                for (int i = 0; i < this.triggers.Count; i++)
                {
                    ActiveTrigger at = this.triggers[i];
                    if (at.childIndex > -1)
                    {
                        _instance = assignInstance(at);
                        if (_instance == null)
                        {
                            Debug.Log("childIndex is out of range for " + this.o.id + ":" + GetType().ToString());
                        }
                        else
                        {
                            this.assignChildBehaviour(_instance, at, behaviours);
                            tempTriggers.RemoveAt(i - removedItems);
                        }
                        removedItems++;
                    }
                }
                this.triggers = tempTriggers;
                return behaviours;
            }

            public virtual void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                // specific to the behaviour. 
                // check if it has a trigger. 
                // assign array of triggers. 
                // assign instance. 
                // assign root.
                // add to behaviours. 
                // add to triggers.  
            }

            public GameObject assignInstance(ActiveTrigger at)
            {
                //note: root is assigned from activeBehaviourController/
                if (this.root == null)
                {
                    return this.instance;
                }

                if (at.childIndex > -1)
                {
                    if (root.transform.childCount > at.childIndex)
                    {
                        return root.transform.GetChild(at.childIndex).gameObject;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return this.instance;
                }
            }

            public void assignSecondaryTriggers(ActiveTrigger _a)
            {
                //this is kind of a constructor.
                if (!string.IsNullOrEmpty(_a.preset))
                {
                    string preset = _a.preset;
                    string id = _a.id;
                    ActiveTrigger presetAt = this.findActiveTriggerPreset(_a.preset);
                    if (presetAt != null)
                    {
                        presetAt.overwrite(_a);
                        _a = presetAt;
                        _a.preset = preset;
                        _a.id = id;
                    }
                }

                if (!string.IsNullOrEmpty(_a.id))
                {
                    this.addToActiveTriggerPresets(_a);
                }

                //makes list of active triggers. primary is the first. secondaries are converted for loops.
                this.triggers = new List<ActiveTrigger>();
                ActiveTrigger newA = new ActiveTrigger(_a);
                if (newA.media != null)
                {
                    newA.media = new Media(newA.media);  //experimental.
                }
                this.triggers.Add(newA);
                newA._i = 0;
                int i = 1;
                if (_a.triggers != null)
                {
                    foreach (SecondaryActiveTrigger sat in _a.triggers)
                    {
                        ActiveTrigger at = new ActiveTrigger(sat);
                        if (at.preset != null)
                        {
                            ActiveTrigger foundPreset = findActiveTriggerPreset(at.preset);
                            if (foundPreset != null)
                            {
                                foundPreset.overwrite(at);
                                at = foundPreset;
                            }
                        }

                        if (!string.IsNullOrEmpty(sat.id))
                        {
                            this.addToActiveTriggerPresets(sat);
                        }
                        at._i = i;
                        this.triggers.Add(at);
                        i++;
                    }
                }
            }

            private void addToActiveTriggerPresets(ActiveTrigger at)
            {
                if (ActiveBehaviourController.instance != null)
                {
                    ActiveBehaviourController.instance.addToActiveTriggerPresets(at);
                }
                //not implemented yet. 
            }

            private void addToActiveTriggerPresets(SecondaryActiveTrigger sat)
            {
                if (ActiveBehaviourController.instance != null)
                {
                    ActiveBehaviourController.instance.addToActiveTriggerPresets(new ActiveTrigger(sat));
                }
            }


            private ActiveTrigger findActiveTriggerPreset(string preset)
            {
                if (ActiveBehaviourController.instance != null)
                {
                    return ActiveBehaviourController.instance.findActiveTriggerPreset(preset);
                }
                return null;
            }

            public virtual void updateKnobValue(Vector3 addedKnobsValue)
            {
                //for example, knobSpeed = addedKnobsValue.
            }

            public virtual void updateKnobRotation(Quaternion addedKnobsRotation)
            {

            }

            public virtual void updateKnobColor(Color addedKnobsColor)
            {

            }

            public virtual float getLerpValue()
            {
                return lerpValue;
            }

            public virtual void Kill()
            {
                introduced = false;
                //temporaririly turning off. 
                instantiated = false;
                ending = false;
                starting = false;
                on = false;
            }

            public virtual void Hide()
            {
                //call to store back in pool.
                if (this.o != null)
                {
                    GameObject instance = this.o.getInstance(this._i);
                    if (instance != null)
                    {
                        instance.transform.position = this.o.nullVector;
                        this.o.resetRigidBody(instance);
                        instance.SetActive(false);
                    }
                }
            }

            public void killTweens()
            {
                // this isn't called from anywhere. 
                // afraid to turn it back on. 
                if (_sequence != null)
                {
                    _sequence.Kill(true);
                }
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill(true);
                }

            }
        }
    }
}