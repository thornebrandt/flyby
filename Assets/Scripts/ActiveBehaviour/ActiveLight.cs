using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using System.Collections.Generic;
using UnityEngine.Rendering.HighDefinition;

namespace flyby
{
    namespace Active
    {
        public class ActiveLight : ActiveBehaviour
        {

            // currently you need a color in activeTrigger to use activeLight. 
            // TODO - fix this. 

            private Light _light;

            private HDAdditionalLightData lightData;

            bool usingColor;

            private Color originalColor; //color on the light when we start. 
            private Color startColor; //color to fade in from. 
            private Color baseColor; //color in rest state.
            private Color endColor; //color to fade out to. 
            private Color targetColor; //color for triggers.
            private Color currentColor; //before targetColor lerps over it. 
            private Color animatedColor; //animating between triggers. 
            private Color knobColor; //addition of all knob colors. 
            private Color color; //final color
            private Color killedColor = Color.black; //color after animation is complete and killed.  
            private Color lastTargetColor; //caching target color for transition.

            bool usingIntensity;
            float defaultCone = 60;

            private Vector3 originalValue;
            private Vector3 value;
            private Vector3 baseValue;
            private Vector3 startValue;
            private Vector3 endValue;
            private Vector3 currentValue;
            private Vector3 targetValue;
            private Vector3 lastTargetValue;
            private Vector3 animatedValue;
            private Vector3 knobValue;

            public override void setupActiveTriggers(Obj o)
            {
                // useful for generic and dynamic but custom behaviours. 
                // to quickly make variations via json. 
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);
                        this.instance = this.gameObject;
                    }
                }
            }

            public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                ActiveLight _activeLight = _instance.GetComponent<ActiveLight>();
                if (_activeLight == null)
                {
                    _activeLight = _instance.AddComponent<ActiveLight>();
                }
                _activeLight.triggers = new List<ActiveTrigger>();
                _activeLight.instance = _instance;
                _activeLight.root = this.root;
                _behaviours.Add(_activeLight);
                _activeLight.triggers.Add(at);
            }


            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                if (Application.isPlaying)
                {
                    _light = gameObject.GetComponent<Light>();
                    lightData = gameObject.GetComponent<HDAdditionalLightData>();

                    if (_light != null && this.triggers != null && this.triggers.Count > 0)
                    {

                        Kill();
                        triggerController = base.getTriggerController();
                        //notice there is no secondary triggers here. 
                        ActiveTrigger a = this.triggers[0];
                        lerpValue = 0;
                        this.checkForCachedKnobs(a);
                        this.animatingKnob = true; //trigger first frame of animation in update.
                        this.usingIntensity = checkUsingIntensity(a);
                        this.usingColor = checkUsingColor(a);
                        //TODO - need a similar test for "usingColor" 

                        if (usingColor)
                        {
                            if (a.endColor != null)
                            {
                                endColor = a.endColor.getColor();
                            }

                            if (a.color != null)
                            {
                                baseColor = a.color.getColor();
                            }
                            else
                            {
                                baseColor = _light.color;
                            }

                            if (a.startColor != null)
                            {
                                startColor = a.startColor.getColor();
                            }
                            else
                            {
                                startColor = Color.black;
                            }

                            if (a.startTime <= 0)
                            {
                                startColor = baseColor;
                            }
                            currentColor = startColor;
                            _light.color = startColor + knobColor;
                        }


                        //intensity portion. 

                        if (usingIntensity)
                        {
                            if (a.value != null)
                            {
                                baseValue = a.value.getVector3();
                                baseValue.y = baseValue.y <= 0 ? this.defaultCone : baseValue.y;
                            }
                            else
                            {
                                baseValue = originalValue;
                            }
                            if (a.start != null)
                            {
                                startValue = a.start.getVector3();
                                startValue.y = startValue.y <= 0 ? this.defaultCone : startValue.y;
                            }
                            else
                            {
                                startValue = baseValue;
                            }
                            if (a.end != null)
                            {
                                endValue = a.end.getVector3();
                                endValue.y = endValue.y <= 0 ? this.defaultCone : endValue.y;
                            }
                            currentValue = startValue;
                            float intensity = currentValue.x;
                            float cone = currentValue.y;

                            lightData.intensity = intensity;

                            if (usingColor)
                            {
                                lightData.color = startColor;
                            }
                            lightData.SetSpotAngle(cone);

                            // _light.intensity = intensity;
                            // _light.color = startColor;
                            // _light.spotAngle = cone;


                            // if (intensity < 1) {
                            //     _light.color = Color.black; //hack fixing flickering. 
                            // }

                        }
                        else
                        {
                            lightData.color = startColor;
                            //_light.color = startColor;
                        }

                        if (a.startColor == a.color && !usingIntensity || (usingIntensity && startValue == value))
                        {
                            a.startTime = 0;
                        }

                        if (_sequence != null)
                        {
                            _sequence.Kill();
                        }
                        _sequence = DOTween.Sequence();
                        base.IntroSequence(_sequence, a);


                        if (!triggerObj.sustain && a.endTime > 0)
                        {
                            if ((usingIntensity && a.end != null) || (usingColor && a.endColor != null))
                            {
                                base.EndSequence(_sequence, a);
                            }
                        }
                    }
                }
                base.Intro(triggerObj, o);
            }


            private bool checkUsingIntensity(ActiveTrigger a)
            {
                //TODO - different tests for intensity/cone etc. 
                if (a.value != null || a.start != null || a.target != null || a.knob != null)
                {
                    if (this.originalValue == null)
                    {
                        Debug.Log("original: " + _light.spotAngle);
                        this.originalValue = new Vector3(_light.intensity, _light.spotAngle, 0);
                    }
                    return true;
                }
                return false;
            }

            private bool checkUsingColor(ActiveTrigger a)
            {
                if (a.color != null || a.startColor != null || a.targetColor != null || a.knobColor != null)
                {
                    if (this.originalColor == null)
                    {
                        Debug.Log("origin color: " + _light.color);
                        this.originalColor = _light.color;
                    }
                    return true;
                }
                return false;
            }


            public override void Update()
            {
                if (animating || animatingTrigger || animatingKnob)
                {
                    updateLight();
                    animatingKnob = false;
                }
            }

            private void updateLight()
            {
                if (_light != null)
                {
                    if (starting)
                    {
                        if (usingColor)
                        {
                            currentColor = Color.Lerp(startColor, baseColor, lerpValue);
                        }
                        if (usingIntensity)
                        {
                            currentValue = Vector3.Lerp(startValue, baseValue, lerpValue);
                        }
                    }
                    if (ending)
                    {
                        if (usingColor)
                        {
                            currentColor = Color.Lerp(baseColor, endColor, 1.0f - lerpValue);
                        }
                        if (usingIntensity)
                        {
                            currentValue = Vector3.Lerp(baseValue, endValue, 1.0f - lerpValue);
                        }
                    }

                    if (usingColor)
                    {
                        animatedColor = currentColor;
                    }

                    if (usingIntensity)
                    {
                        animatedValue = currentValue;
                    }


                    if (animatingTrigger)
                    {
                        if (targetColor != null)
                        {
                            animatedColor = Color.Lerp(currentColor, targetColor, triggerLerpValue);
                        }
                        if (targetValue != null)
                        {
                            animatedValue = Vector3.Lerp(currentValue, targetValue, triggerLerpValue);
                        }
                    }

                    if (usingColor)
                    {
                        color = Col.AddColors(animatedColor, knobColor);
                    }

                    if (usingIntensity)
                    {
                        float intensity = animatedValue.x;
                        float cone = animatedValue.y;
                        //_light.color = color;


                        if (usingColor)
                        {
                            lightData.color = color;
                        }

                        // if (intensity < 1) {
                        //     _light.color = Color.black; //hack - fixing flickering. 
                        // }

                        lightData.intensity = intensity;
                        lightData.SetSpotAngle(cone);

                        //_light.intensity = intensity;
                        //_light.spotAngle = animatedValue.y;

                        //Debug.Log("animated cone: " + cone);
                    }
                    else
                    {
                        if (usingColor)
                        {
                            lightData.color = color;
                        }

                        //_light.color = color;
                    }
                }
            }

            public override void Release()
            {
                //handles endColor on sustain for intro trigger. 
                if (_sequence != null)
                {
                    _sequence.Kill();
                }

                if (this.triggers.Count > 0)
                {
                    ActiveTrigger a = this.triggers[0];
                    if (a != null && a.endColor != null)
                    {
                        _sequence = DOTween.Sequence();
                        base.EndSequence(_sequence, a);
                    }
                }
            }

            public override void updateKnobColor(Color addedKnobsColor)
            {
                //base knob calls this after running through cached knobs.
                knobColor = addedKnobsColor;
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //TODO - change baseColor based on state.
                //test an "Insert" with mixable colors

                if (Application.isPlaying && a != null)
                {
                    //checks that this is not an editorMode test.
                    if (_light != null)
                    {
                        bool hasTarget = false;
                        if (a.targetColor != null)
                        {
                            hasTarget = true;
                            Color newTargetColor = a.targetColor.getColor();
                            if (a.offTime <= 0 || (lastTrigger != null && lastTrigger != a))
                            {
                                triggerLerpValue = 0;
                                currentColor = _light.color;
                            }

                            if (animatingTrigger && lastTargetColor != newTargetColor)
                            {
                                starting = false;
                                triggerLerpValue = 0;
                                currentColor = _light.color;
                            }
                            targetColor = newTargetColor;
                            lastTargetColor = newTargetColor;
                        }
                        else
                        {
                            targetColor = baseColor; //experimental.
                        }


                        if (a.target != null)
                        {
                            hasTarget = true;
                            Vector3 newTargetValue = a.target.getVector3();
                            newTargetValue.y = newTargetValue.y <= 0 ? this.defaultCone : newTargetValue.y;
                            if (a.offTime <= 0 || (lastTrigger != null && lastTrigger != a))
                            {
                                // what is this doing?
                                // can we comment out this block?
                                triggerLerpValue = 0;
                                currentValue.x = lightData.intensity;
                                currentValue.y = _light.spotAngle;
                            }

                            if (animatingTrigger && lastTargetValue != newTargetValue)
                            {
                                starting = false;
                                triggerLerpValue = 0;
                                currentValue.x = lightData.intensity;
                                currentValue.y = _light.spotAngle;
                            }

                            targetValue = newTargetValue;
                            lastTargetValue = newTargetValue;
                        }

                        if (hasTarget)
                        {
                            if (_triggerSequence != null)
                            {
                                _triggerSequence.Kill();
                            }
                            _triggerSequence = DOTween.Sequence();
                            TriggerSequence(_triggerSequence, a);
                            lastTrigger = a;
                            if (!triggerObj.sustain)
                            {
                                TriggerOffSequence(_triggerSequence, a);
                            }
                            a.timesTriggered++;
                        }
                    }
                }
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (instance != null && _light != null)
                {
                    if (a.offTime < 0)
                    {
                        //its not coming back. this is our new home.  
                        if (usingColor)
                        {
                            currentColor = _light.color;
                        }
                        if (usingIntensity)
                        {
                            currentValue.x = _light.intensity;
                            currentValue.y = _light.spotAngle;
                        }
                    }
                    else
                    {
                        if (usingColor)
                        {
                            currentColor = baseColor;
                        }
                        if (usingIntensity)
                        {
                            currentValue = baseValue;
                        }
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //handling sustain off - move currentColor back to baseColor. 
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                if (usingColor)
                {
                    currentColor = baseColor;
                }
                if (usingIntensity)
                {
                    currentValue = baseValue;
                }
                TriggerOffSequence(_triggerSequence, a);
            }


            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true;
                if (usingColor)
                {
                    knobColor = Color.black;
                }
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knobColor != null)
                    {
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue) : at.knobValue;
                        if (usingColor)
                        {
                            knobColor = Col.AddColors(knobColor, at.knobColor.lerpColor(mappedValue));
                        }

                        if (usingIntensity)
                        {
                            knobValue += at.knob.lerpVector3(mappedValue);
                        }
                    }
                }
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                this.triggerController = getTriggerController();
                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }
                Color addedKnobsColor = Color.black;
                foreach (ActiveTrigger at in this.triggers)
                {
                    //shouldn't we also check for at.knob for intensity ? 
                    if (at.knobColor != null)
                    {
                        TriggerObj t = checkForCachedKnob(at);
                        at.knobValue = t != null ? t.value : at.knobValue;
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * multiplier) : at.knobValue * multiplier;
                        addedKnobsColor = Col.AddColors(addedKnobsColor, at.knobColor.lerpColor(mappedValue));
                        Debug.Log("checking: " + mappedValue + " becomes: " + at.knobColor.lerpColor(mappedValue));
                    }
                }
                this.knobColor = addedKnobsColor;
            }


            public override void Kill()
            {
                if (_light != null)
                {
                    if (usingColor)
                    {
                        color = killedColor;
                        _light.color = killedColor;
                    }
                    _light.intensity = 0;
                    _light.spotAngle = this.defaultCone;
                }

                base.Kill();
            }

        }
    }
}