using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using System.Collections.Generic;
using UnityEngine.Rendering.HighDefinition;

namespace flyby
{
    namespace Active
    {
        public class ActiveColor : ActiveBehaviour
        {
            private Renderer rend;
            public Material material;
            private Color originalColor; //if color is not assigned - stash this. 
            private Color originalEmissiveColor; //same as above. 
            private bool originalColorSaved;
            private bool originalEmissiveColorSaved;
            private Color startColor; //color to fade in from. 
            private Color baseColor; //color in rest state.
            private Color endColor; //color to fade out to. 
            private Color targetColor; //color for triggers.
            private Color currentColor; //before targetColor lerps over it. 
            private bool hasEmission; //material has emission property ( needs rework )
            private Color animatedColor; //animating between triggers. 
            private Color knobColor; //addition of all knob colors. 
            private Color color; //final color
            private Color killedColor = Color.clear; //color after animation is complete and killed.  
            private Color lastTargetColor; //caching target color for transition.

            //we will try all the most common default properties. 
            private static readonly string[] defaultColorProperties = { "_BaseColor", "_UnlitColor", "_Color" };
            public string materialProperty = "_UnlitColor"; //can set this. 
            public string emissiveProperty = "_EmissiveColor";
            public string emissionKeyword = "_EMISSION";
            public bool usingEmission = true; //turn this off if you don't want emission.

            private DecalProjector decalProjector;


            public override void setupActiveTriggers(Obj o)
            {
                this.o = o;
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o.triggerColor != null)
                    {
                        this.assignSecondaryTriggers(o.triggerColor);
                        this.instance = this.gameObject;
                    }
                }
                if (Application.isPlaying && o != null)
                {
                    this.material = getMaterial(o.triggerColor);
                }
            }

            public void setBaseColors(Color color)
            {
                //setting externally to be able to use tweens. 
                this.baseColor = color;
                this.startColor = color;
                this.originalColor = color;
                //this.currentColor = color; //this is an animated color.
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                if (Application.isPlaying)
                { //checks it is not editor mode test
                    //Kill(); //just added - messing up default . 
                    triggerController = base.getTriggerController();
                    if (this.triggers == null)
                    {
                        this.assignSecondaryTriggers(o.triggerColor);
                    }
                    ActiveTrigger a = this.triggers.Count > 0 ? this.triggers[0] : new ActiveTrigger();
                    Color introColor = new Color();
                    //TODO - intro emissive color / tests 
                    material = getMaterial(a);
                    this.hasEmission = this.checkHasEmission(material);
                    if (a != null && material != null)
                    {
                        lerpValue = 0;
                        //deprecated - see position. 
                        //this._knobs = base.setupKnobs(a, this._knobs);
                        this.checkForCachedKnobs(a);
                        this.originalColor = getOriginalMaterialColor(a); //cache original color before knob changes it.
                        this.originalEmissiveColor = getOriginalEmissiveColor();
                        this.animatingKnob = true; //trigger first frame of animation in update. 


                        if (a.targetColor != null)
                        {
                            targetColor = a.targetColor.getColor();
                        }
                        if (a.endColor != null)
                        {
                            endColor = a.endColor.getColor();
                        }

                        if (a.color == null && a.startColor == null)
                        {
                            baseColor = getOriginalMaterialColor(a);
                            startColor = baseColor;
                            currentColor = baseColor;
                        }

                        if (a.startColor != null)
                        {
                            startColor = a.startColor.getColor();
                            baseColor = startColor;
                            currentColor = startColor;
                            material.SetColor(materialProperty, baseColor);

                        }
                        if (a.color != null)
                        {
                            baseColor = a.color.getColor();
                            currentColor = baseColor;
                            if (a.startColor == null)
                            {
                                startColor = baseColor;
                                introColor = Col.AddColors(baseColor, knobColor, BlendMode.Colorize);
                                material.SetColor(materialProperty, introColor);
                                if (usingEmission)
                                {
                                    material.SetColor(emissiveProperty, introColor);
                                }
                            }
                            else
                            {
                                introColor = Col.AddColors(startColor, knobColor, BlendMode.Colorize);
                                material.SetColor(materialProperty, introColor);
                                if (usingEmission)
                                {
                                    material.SetColor(emissiveProperty, startColor);
                                }
                                if (_sequence != null)
                                {
                                    _sequence.Kill();
                                }
                            }
                            if (a.startColor == a.color)
                            {
                                a.startTime = 0;
                            }
                            _sequence = DOTween.Sequence();
                            base.IntroSequence(_sequence, a);
                        }

                        if (!triggerObj.sustain && a.endColor != null && a.endTime > 0)
                        {
                            base.EndSequence(_sequence, a);
                        }
                    }

                    this.killedColor = baseColor;

                    //hack - experimental 
                    if (this.decalProjector != null)
                    {
                        this.decalProjector.fadeFactor = baseColor.a;
                    }

                    base.Intro(triggerObj, o);
                }
            }

            public override void Update()
            {
                if (animating || animatingTrigger || animatingKnob)
                {
                    updateColor();
                    animatingKnob = false;
                }
            }

            private bool checkUsingEmission(Material material)
            {
                if (string.IsNullOrEmpty(this.emissiveProperty)) return false;
                if (material.HasProperty(emissiveProperty))
                {
                    return true;
                }
                return false;
            }

            private void updateColor()
            {

                if (material != null)
                {
                    if (starting)
                    {
                        currentColor = Color.Lerp(startColor, baseColor, lerpValue);
                    }
                    if (ending)
                    {
                        currentColor = Color.Lerp(baseColor, endColor, 1.0f - lerpValue);
                    }
                    if (animatingTrigger && targetColor != null)
                    {
                        animatedColor = Color.Lerp(currentColor, targetColor, triggerLerpValue);
                    }
                    else
                    {
                        animatedColor = currentColor;
                    }
                    color = Col.AddColors(animatedColor, knobColor, BlendMode.Colorize);
                    material.SetColor(materialProperty, color);
                    if (usingEmission)
                    {
                        material.SetColor(emissiveProperty, animatedColor);
                    }
                }
            }

            public Material getMaterial(ActiveTrigger a)
            {
                if (material != null) return material; //cache material.
                if (instance == null) instance = this.gameObject;
                if (a == null)
                {
                    return null;
                }


                if (rend == null)
                {
                    rend = GetComponent<Renderer>();
                }

                //first check if normal renderer and return this.material.
                if (rend != null)
                {
                    if (rend.material.name.EndsWith("(Instance)")) return rend.material; // a hack to prevent double instantiation. 
                    this.material = new Material(rend.material);
                    this.material.name += "(Instance)";
                    rend.material = this.material;
                    this.material = this.getPropertyName(a, this.material); //checks if property is set. 
                    this.hasEmission = checkHasEmission(material);
                    return this.material;
                }

                if (rend == null)
                {
                    this.decalProjector = this.GetComponent<DecalProjector>();
                    if (this.decalProjector != null)
                    {
                        if (decalProjector.material.name.EndsWith("(Instance)")) return this.decalProjector.material; //a hack to prevent double instantiation. 

                        this.material = new Material(decalProjector.material);
                        this.material.name += "(Instance)";
                        this.decalProjector.material = this.material;
                        this.material = this.getPropertyName(a, this.material); //checks if property is set. 
                        this.hasEmission = checkHasEmission(material);
                        return this.material;
                    }
                    else
                    {
                        Debug.Log("can't find any material on " + this.gameObject.name);
                        return null;
                    }
                }

                if (material == null)
                {
                    if (a.childIndex < 0)
                    {
                        Debug.Log("Warning: " + gameObject.name + " material " + a.propertyIndex
                        + " does not exist. Defaulting to zero.");
                    }
                    if (rend != null)
                    {
                        material = rend.materials[0];
                    }
                }

                if (material == null)
                {
                    if (a.childIndex < 0)
                    {
                        Debug.Log("Warning: " + gameObject.name + " does not have any materials.");
                    }
                    return null;
                }

                Debug.Log("not sure if we ever make it here.");
                this.material = this.getPropertyName(a, material);
                this.hasEmission = checkHasEmission(material);
                return material;
            }

            public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                ActiveColor _activeColor = _instance.GetComponent<ActiveColor>();
                if (_activeColor == null)
                {
                    _activeColor = _instance.AddComponent<ActiveColor>();
                    _activeColor.triggers = new List<ActiveTrigger>();
                    _activeColor.instance = _instance;
                    _activeColor.root = this.root;
                    _behaviours.Add(_activeColor);
                }
                _activeColor.triggers.Add(at);
            }

            private Material getPropertyName(ActiveTrigger a, Material material)
            {
                if (!string.IsNullOrEmpty(materialProperty) && material.HasProperty(materialProperty))
                {
                    //we have the material already set - no reason to loop. 
                    return material;
                }

                if (string.IsNullOrEmpty(a.propertyName))
                {
                    foreach (string property in defaultColorProperties)
                    {
                        if (material.HasProperty(property))
                        {
                            this.materialProperty = property;
                            break;
                        }
                    }
                }
                else
                {
                    this.materialProperty = a.propertyName;
                }

                if (!material.HasProperty(materialProperty))
                {
                    if (a.childIndex < 0)
                    {
                        Debug.Log("Warning: " + gameObject.name + " material " + a.propertyIndex
                        + " does not have property: " + materialProperty);
                    }
                    return null;
                }


                return material;
            }

            private bool checkHasEmission(Material material)
            {
                if (!material.HasProperty(emissiveProperty))
                {
                    // don't think we need this. 
                    // Debug.Log("Not using emission for " + gameObject.name);
                    return false;
                }

                return true;
            }

            private Color getOriginalMaterialColor(ActiveTrigger a)
            {
                this.material = this.getPropertyName(a, this.material);
                if (this.material != null && !this.originalColorSaved)
                {
                    this.originalColor = this.material.GetColor(materialProperty);
                    this.originalColorSaved = true;
                }
                return this.originalColor;
            }

            public Color getOriginalEmissiveColor()
            {
                if (this.usingEmission && this.hasEmission && this.material != null && !this.originalEmissiveColorSaved)
                {
                    this.originalEmissiveColor = this.material.GetColor(emissiveProperty);
                }
                return this.originalEmissiveColor;
            }

            public override void Release()
            {
                // handles endColor sustain after intro. 
                if (_sequence != null)
                {
                    _sequence.Kill();
                }

                if (this.triggers.Count > 0)
                {
                    ActiveTrigger a = this.triggers[0];
                    if (a != null && a.endColor != null)
                    {
                        _sequence = DOTween.Sequence();
                        base.EndSequence(_sequence, a);
                    }
                }
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                this.triggerController = getTriggerController();
                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }
                Color addedKnobsColor = Color.black;
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knobColor != null)
                    {
                        TriggerObj t = checkForCachedKnob(at);
                        at.knobValue = t != null ? t.value : at.knobValue;
                        addedKnobsColor = Col.AddColors(addedKnobsColor, at.knobColor.lerpColor(at.knobValue), BlendMode.Colorize);
                    }
                }
            }


            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                // TODO - change baseColor based on state.
                // test an "Insert" with mixable colors
                if (Application.isPlaying && a != null)
                {
                    //checks that this is not an editorMode test.

                    if (a.targetColor != null && material != null)
                    {
                        Color newTargetColor = a.targetColor.getColor();
                        Color newTargetEmission = killedColor;
                        if (_triggerSequence != null)
                        {
                            _triggerSequence.Kill();
                        }

                        //check for permanent triggers, or new triggers to keep transitions fluid.
                        if (a.offTime <= 0 || (lastColorTrigger != null && lastColorTrigger != a))
                        {
                            triggerLerpValue = 0;
                            currentColor = material.GetColor(materialProperty);
                        }

                        if (animatingTrigger)
                        {
                            bool reTrigger = false;
                            if (lastTargetColor != newTargetColor)
                            {
                                reTrigger = true;
                            }
                            if (reTrigger)
                            {
                                //smoothly animate from the current state. 
                                starting = false;
                                triggerLerpValue = 0;
                                currentColor = material.GetColor(materialProperty);
                            }
                        }

                        _triggerSequence = DOTween.Sequence();
                        targetColor = newTargetColor;
                        lastTargetColor = newTargetColor;
                        TriggerSequence(_triggerSequence, a);
                        base.Trigger(triggerObj, a, o);
                        if (!triggerObj.sustain)
                        {
                            TriggerOffSequence(_triggerSequence, a);
                        }
                    }
                }
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (instance != null && material != null)
                {
                    if (a.offTime < 0)
                    {
                        //its not coming back. this is our new home.  
                        currentColor = material.GetColor(materialProperty);
                        baseColor = currentColor;
                        starting = false;  //interrups the intro sequence if it's a final sequence.
                    }
                    else
                    {
                        //it is going back, come back to home.
                        currentColor = baseColor;
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //handling sustain off - move currentColor back to baseColor. 
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                currentColor = baseColor;
                TriggerOffSequence(_triggerSequence, a);
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true;
                knobColor = Color.black;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knobColor != null)
                    {
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue) : at.knobValue;
                        knobColor = Col.AddColors(knobColor, at.knobColor.lerpColor(mappedValue), BlendMode.Colorize);
                    }
                }
            }

            public override void Kill()
            {
                if (material != null)
                {

                    if (originalColorSaved)
                    {
                        killedColor = this.originalColor;
                    }

                    if (baseColor != null)
                    {
                        killedColor = baseColor;
                    }

                    if (startColor != null)
                    {
                        killedColor = startColor;
                    }

                    color = killedColor;
                    //obsolete
                    material.SetColor(materialProperty, killedColor);
                    if (this.usingEmission)
                    {
                        material.SetColor(emissiveProperty, killedColor);
                    }
                    if (this.triggers != null && this.triggers.Count > 0)
                    {
                        if (this.triggers[0].hideOnComplete)
                        {
                            //transform.parent.parent.parent.gameObject.SetActive(false);
                        }
                    }
                }
                base.killTweens(); //wondering if this will prevent black. 
                base.Kill();
            }

        }
    }
}
