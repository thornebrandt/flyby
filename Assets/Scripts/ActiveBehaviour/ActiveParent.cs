using DG.Tweening;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveParent : ActiveBehaviour
        {
            private Transform parent; //final caulculated parent;
            private Transform originalParent;
            private Transform startParent;
            private Transform currentParent; //value parent, or base in between target triggers.  
            private Transform endParent;
            private Transform targetParent;
            private Transform lastTargetParent;
            private FlyByController flyByController;
            private CameraController cameraController;

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                this.o = o;
                Kill();
                this.triggerController = base.getTriggerController();
                this.flyByController = getFlyByController();
                if (this.triggers == null)
                {
                    this.assignSecondaryTriggers(o.triggerParent);
                }

                originalParent = transform.parent;

                ActiveTrigger a = this.triggers[0];
                if (a != null)
                {
                    lerpValue = 0; // we're still using lerp and dotween so we can cancel timers. 
                    originalParent = transform.parent;

                    if (!string.IsNullOrEmpty(a.valueString))
                    {
                        this.currentParent = getParent(a.valueString);
                    }
                    else
                    {
                        currentParent = originalParent;
                    }

                    if (!string.IsNullOrEmpty(a.startString))
                    {
                        startParent = getParent(a.startString);
                    }
                    else
                    {
                        startParent = currentParent;
                    }

                    if (!string.IsNullOrEmpty(a.endString))
                    {
                        endParent = getParent(a.endString);
                    }
                    if (_sequence != null)
                    {
                        _sequence.Kill();
                    }

                    _sequence = DOTween.Sequence();

                    if (startParent == currentParent)
                    {
                        //might need a delay for multiple parents. 
                        a.startTime = 0;
                    }

                    this.transform.SetParent(startParent);

                    //resetting origin.
                    this.transform.localPosition = o.origin.getVector3();
                    base.IntroSequence(_sequence, a);
                    if (!triggerObj.sustain && endParent != null && a.endTime > 0)
                    {
                        base.EndSequence(_sequence, a);
                    }
                    base.Intro(triggerObj, o);
                }
            }

            public override void OnIntroComplete(ActiveTrigger a)
            {
                if (instance != null)
                {
                    this.transform.SetParent(this.currentParent);
                    //use start to determine offset.
                }
            }

            public override void OnEndComplete(ActiveTrigger a)
            {
                if (instance != null)
                {
                    this.transform.SetParent(endParent);
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //TODO - use similar "hasValue" and "getSetValue" as additiveBehaviour

                if (a.targetString != null)
                {
                    //Transform targetParent = getParent(a.targetString);
                    if (_triggerSequence != null)
                    {
                        _triggerSequence.Kill();
                    }

                    _triggerSequence = DOTween.Sequence();
                    TriggerSequence(_triggerSequence, a);

                    if (!triggerObj.sustain && a.offTime >= 0)
                    {
                        TriggerOffSequence(_triggerSequence, a);
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //TODO - this should probably be the generic, probaly just need global methods such as .
                //OnTriggerStart and OnTriggerOff
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                TriggerOffSequence(_triggerSequence, a);
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                //we can't really transition in between frames so this is just a timer. 
                if (!string.IsNullOrEmpty(a.targetString))
                {
                    Transform targetParent = getParent(a.targetString);
                    this.transform.SetParent(targetParent);
                }
            }

            public override void OnTriggerOffComplete(ActiveTrigger a)
            {
                //this value should always exist. 
                this.transform.SetParent(currentParent);
            }

            private FlyByController getFlyByController()
            {
                //TODO - move this to base class. 

                if (this.flyByController != null)
                {
                    return flyByController;
                }
                if (FlyByController.instance != null)
                {
                    return FlyByController.instance;
                }
                return null;
            }

            private CameraController getCameraController()
            {
                if (this.cameraController != null)
                {
                    return cameraController;
                }
                if (CameraController.instance != null)
                {
                    return CameraController.instance;
                }
                if (FlyByController.instance != null)
                {
                    return FlyByController.instance.cameraController;
                }
                return null;
            }


            private Transform getParent(string parentName)
            {
                if (this.flyByController != null)
                {

                    if (parentName == "root" || parentName == "scene" || parentName == "null" || parentName == "empty")
                    {
                        return this.originalParent;
                    }

                    if (parentName == "cam")
                    {
                        this.cameraController = getCameraController();
                        return this.cameraController.getCam().transform;
                    }

                    GameObject instance = this.flyByController.findActiveInstanceById(parentName);
                    Obj o = this.flyByController.findObjById(parentName);

                    if (instance != null)
                    {
                        if (o.hierarchyType == HierarchyType.None)
                        {
                            //TODO - make this less hacky. 
                            return instance.transform;
                        }
                        else
                        {
                            return Obj.getPrefabInstance(instance).transform;
                        }
                    }
                    else
                    {
                        Debug.Log("could not find parent " + parentName + " for " + this.gameObject.name);
                    }
                }

                return this.transform;
            }
        }
    }
}