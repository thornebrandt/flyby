using DG.Tweening;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using UnityEngine;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveRotation : ActiveBehaviour
        {


            public Quaternion rotation; //final calculated rotation.
            private Quaternion baseRotation;
            private Quaternion startRotation;
            private Quaternion endRotation;
            private Quaternion currentRotation; //lerped rotation between start, base, and end. 
            private Quaternion targetRotation; //temporary or sustained rotation
            private Quaternion animatedRotation; //lerped rotation between currentRotation and targetRotation
            private Quaternion targetKnobRotation;
            private Quaternion currentKnobRotation;
            private Quaternion lastTargetRotation; //keeping track of the same trigger being fired. 
            private List<TriggerObj> knobs;
            private float knobLerpSpeed = 5;

            public override void Update()
            {
                if (instantiated)
                {
                    if (animating || animatingTrigger || animatingKnob)
                    {
                        updateRotation();
                        animatingKnob = false;
                    }
                }
            }

            public override void setupActiveTriggers(Obj o)
            {
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o != null && o.triggerRotation != null)
                    {
                        this.assignSecondaryTriggers(o.triggerRotation);
                    }
                    this.instance = this.gameObject;
                }

                //might not be the best use case. 
                if (Config.instance.testing)
                {
                    knobLerpSpeed = Mathf.Infinity;  //lots of tests failing because of the lerp speed. 
                }
            }

            public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                ActiveRotation _activeRotation = _instance.GetComponent<ActiveRotation>();
                if (_activeRotation == null)
                {
                    _activeRotation = _instance.AddComponent<ActiveRotation>();
                    _activeRotation.triggers = new List<ActiveTrigger>();
                    _activeRotation.instance = _instance;
                    _activeRotation.root = this.root;
                    _behaviours.Add(_activeRotation);
                }
                _activeRotation.triggers.Add(at);
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                Kill();
                triggerController = base.getTriggerController();
                if (this.triggers == null)
                {
                    this.assignSecondaryTriggers(o.triggerRotation);
                }

                this.currentKnobRotation = Quaternion.identity;

                ActiveTrigger a = this.triggers[0];

                if (a != null)
                {
                    lerpValue = 0;
                    baseRotation = Quaternion.identity;
                    this.checkForCachedKnobs(a);

                    if (a.value != null)
                    {
                        baseRotation = a.value.getQuaternion();
                        currentRotation = baseRotation;
                        if (a.value.multiple != null)
                        {
                            Vector3 newValueEuler = baseRotation.eulerAngles;
                            newValueEuler = Vec.getNearestMultiple(newValueEuler, a.value.multiple);
                            baseRotation = Quaternion.Euler(newValueEuler);
                        }
                    }

                    if (a.start != null)
                    {
                        startRotation = a.start.getQuaternion();
                        currentRotation = startRotation;
                    }
                    else
                    {
                        startRotation = baseRotation;
                        currentRotation = startRotation;
                    }

                    if (a.end != null)
                    {
                        endRotation = a.end.getQuaternion();
                    }

                    if (_sequence != null)
                    {
                        _sequence.Kill(true);
                    }

                    if (a.start == a.value)
                    {
                        a.startTime = 0;
                    }

                    _sequence = DOTween.Sequence();
                    base.IntroSequence(_sequence, a);
                    if (!triggerObj.sustain && a.end != null && a.endTime > 0)
                    {
                        base.EndSequence(_sequence, a);
                    }
                }
                updateRotation(); //prevent first frame popping.
                base.Intro(triggerObj, o);
            }

            public void updateRotation()
            {
                if (starting)
                {
                    currentRotation = Quaternion.Lerp(startRotation, baseRotation, lerpValue);
                }
                if (ending)
                {
                    currentRotation = Quaternion.Lerp(baseRotation, endRotation, 1.0f - lerpValue);
                }
                if (animatingTrigger && targetRotation != null)
                {
                    animatedRotation = Quaternion.Lerp(currentRotation, targetRotation, triggerLerpValue);
                }
                else
                {
                    animatedRotation = currentRotation;
                }
                if (this.usingKnobs && this.targetKnobRotation != null)
                {
                    currentKnobRotation = Quaternion.Lerp(currentKnobRotation, targetKnobRotation, Time.deltaTime * knobLerpSpeed);
                }
                else
                {
                    currentKnobRotation = Quaternion.identity;
                }
                rotation = animatedRotation * currentKnobRotation;
                instance.transform.localRotation = rotation;
                //debug. 
                //getStatus();
            }

            public override void Release()
            {
                // handles endColor sustain after intro. 
                if (_sequence != null)
                {
                    _sequence.Kill();
                }

                if (this.triggers.Count > 0)
                {
                    ActiveTrigger a = this.triggers[0];
                    if (a != null && a.end != null)
                    {
                        _sequence = DOTween.Sequence();
                        base.EndSequence(_sequence, a);
                    }
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (a.target != null)
                {
                    Quaternion newTargetRotation = a.target.getQuaternion();
                    Vector3 differenceAfterKill = Vector3.zero;

                    if (_triggerSequence != null)
                    {
                        _triggerSequence.Kill();
                    }


                    if (a.target.relative)
                    {
                        newTargetRotation = instance.transform.localRotation * newTargetRotation;
                    }

                    if (a.target.multiple != null)
                    {
                        Vector3 newTargetEuler = newTargetRotation.eulerAngles;
                        newTargetEuler = Vec.getNearestMultiple(newTargetEuler, a.target.multiple);
                        newTargetRotation = Quaternion.Euler(newTargetEuler);
                    }

                    //check for permanent triggers, or new triggers to keep transitions fluid. 
                    if (a.offTime <= 0 || (lastTrigger != null && lastTrigger != a))
                    {
                        triggerLerpValue = 0;
                        currentRotation = instance.transform.localRotation;
                    }

                    //check for self randomized rotation;
                    if (animatingTrigger && lastTargetRotation != newTargetRotation)
                    {
                        starting = false;
                        triggerLerpValue = 0;
                        currentRotation = instance.transform.localRotation;
                    }

                    _triggerSequence = DOTween.Sequence();
                    targetRotation = newTargetRotation;
                    TriggerSequence(_triggerSequence, a);
                    lastTargetRotation = newTargetRotation;
                    base.Trigger(triggerObj, a, o);
                    if (!triggerObj.sustain)
                    {
                        TriggerOffSequence(_triggerSequence, a);
                    }
                }
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (instance != null)
                {
                    if (a.offTime < 0)
                    {
                        //its not coming back. this is our new home.  
                        currentRotation = instance.transform.localRotation;
                    }
                    else
                    {
                        currentRotation = baseRotation;
                    }
                }
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true;
                this.usingKnobs = true;
                targetKnobRotation = Quaternion.identity;
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue) : at.knobValue;
                        targetKnobRotation = targetKnobRotation * Quaternion.Euler(at.knob.lerpVector3(mappedValue));
                    }
                }
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                //override for quaternion multiplication. 

                this.triggerController = getTriggerController();
                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }

                this.targetKnobRotation = Quaternion.identity;
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null)
                    {
                        this.usingKnobs = true;
                        TriggerObj t = checkForCachedKnob(at);
                        at.knobValue = t != null ? t.value : at.knobValue;
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue) : at.knobValue;
                        targetKnobRotation = targetKnobRotation * Quaternion.Euler(at.knob.lerpVector3(mappedValue));
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                currentRotation = baseRotation;
                TriggerOffSequence(_triggerSequence, a);
            }

            public override void Kill()
            {
                // can kill be removed?   ( might need for knobs ) 
                // rotation = Quaternion.identity;
                // startRotation = Quaternion.identity;
                // endRotation = Quaternion.identity;
                // currentRotation = Quaternion.identity;
                // targetRotation = Quaternion.identity;
                // animatedRotation = Quaternion.identity;
                // lastTargetRotation = Quaternion.identity;
                base.Kill();
            }

            private void getStatus()
            {
                //debug animating trigger. 
                Debug.Log("animTrigger: " + this.animatingTrigger + ":" + triggerLerpValue + ": " + this.rotation);
            }
        }
    }
}