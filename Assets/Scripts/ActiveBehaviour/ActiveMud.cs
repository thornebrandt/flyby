using DG.Tweening;
using UnityEngine;
using MudBun;
using System.Collections.Generic;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Active
    {
        public class ActiveMud : AdditiveActiveBehaviour
        {
            // note on mud - subtractive brushes only effect what is above them. 
            // it is best to parent objs to two sets of children of the mudRenderer
            // so that the pools render in the correct order.  

            public MudRendererBase mudRenderer;
            public MudParticleSystem mudParticle;
            public MudNoiseVolume mudNoiseVolume;
            public MudNoiseScroller mudScroll;
            public MudNoiseModifier mudNoiseModifier;
            public MudBox mudBox; //to control roundness. 
            public MudCylinder mudCylinder;
            public MudCurveFull mudCurve;
            public ParticleSystem particle;
            // first example of multiple overlapping tweens. 

            //for sine curve
            public int numPoints = 12;
            public float amplitude = 1.0f;
            public float frequency = 1.0f;
            public float waveLength = 2.0f;
            public float speed = 1.0f;
            private float continuousTime = 0.0f;
            private float currentFrequency;
            public float frequencyLerpSpeed = 0.05f;
            private float phaseOffset;

            //for noise offset.  ( used by both volume and modifier brush ) 
            //TODO - make private after debug. 
            private float noiseOffset_x;
            private float noiseOffset_y;
            private float noiseOffset_z;
            private Vector3 currentNoiseOffset = new Vector3();
            private Vector3 lastNoiseOffset = new Vector3();

            //for noise scroller
            //TODO - make private after debug. 
            private float scroll_x;
            private float scroll_y;
            private float scroll_z;
            private Vector3 currentScroll = new Vector3();


            public override void Update()
            {
                base.Update();
                if (this.mudCurve != null)
                {
                    updateSineWave();
                }

                if (this.mudNoiseVolume != null)
                {
                    updateMudNoiseVolume();
                }

                if (this.mudNoiseModifier != null)
                {
                    updateMudNoiseModifier();
                }

                if (this.mudScroll != null)
                {
                    updateMudScroll();
                }
            }

            private void updateMudNoiseModifier()
            {
                //need to update scroll here. 
                currentScroll = new Vector3(
                    scroll_x,
                    scroll_y,
                    scroll_z
                );

                this.mudNoiseModifier.Offset += currentScroll * Time.deltaTime;
            }

            private void updateMudNoiseVolume()
            {
                //TODO - I think we can delete this and only use noise. 

                currentNoiseOffset = new Vector3(
                    noiseOffset_x,
                    noiseOffset_y,
                    noiseOffset_z
                );
                if (lastNoiseOffset != currentNoiseOffset)
                {
                    this.mudNoiseVolume.Offset = currentNoiseOffset;
                }
                lastNoiseOffset = currentNoiseOffset;
            }

            private void updateMudScroll()
            {
                currentScroll = new Vector3(
                    scroll_x,
                    scroll_y,
                    scroll_z
                );

                if (this.mudScroll.Speed != currentScroll)
                {
                    this.mudScroll.Speed = currentScroll;
                }
            }

            public override void setupComponent()
            {
                if (this.mudRenderer == null)
                {
                    this.mudRenderer = GetComponent<MudRendererBase>();
                }

                if (this.mudParticle == null)
                {
                    this.mudParticle = GetComponent<MudParticleSystem>();
                    this.particle = GetComponent<ParticleSystem>();
                }

                if (this.mudBox == null)
                {
                    this.mudBox = GetComponent<MudBox>();
                }


                if (this.mudCylinder == null)
                {
                    this.mudCylinder = GetComponent<MudCylinder>();
                }

                if (this.mudCurve == null)
                {
                    this.mudCurve = GetComponent<MudCurveFull>();
                    if (mudCurve != null)
                    {
                        //TODO - do more specific check for curve related parameters.
                        setupSinePoints();  //temporary
                    }
                }

                if (this.mudNoiseVolume == null)
                {
                    this.mudNoiseVolume = GetComponent<MudNoiseVolume>();
                }

                if (this.mudNoiseModifier == null)
                {
                    this.mudNoiseModifier = GetComponent<MudNoiseModifier>();
                }

                if (this.mudScroll == null)
                {
                    this.mudScroll = GetComponent<MudNoiseScroller>();
                }

                //TODO - why not do this in the reverse??
                if (this.mudRenderer == null &&
                    this.mudParticle == null &&
                    this.mudBox == null &&
                    this.mudCurve == null)
                {
                    Debug.Log("No mud scripts hooked up. ");
                }
                else
                {
                    this.hasComponent = true;
                }
            }

            public override void assignChildBehaviour(GameObject _instance, ActiveTrigger at, List<ActiveBehaviour> _behaviours)
            {
                ActiveMud _activeMud = _instance.GetComponent<ActiveMud>();
                if (_activeMud == null)
                {
                    _activeMud = _instance.AddComponent<ActiveMud>();
                }
                _activeMud.triggers = new List<ActiveTrigger>();
                _activeMud.instance = _instance;
                _activeMud.root = this.root;
                _behaviours.Add(_activeMud);
                _activeMud.triggers.Add(at);
            }

            private void setupSinePoints()
            {
                List<MudBun.MudCurveFull.Point> points = new List<MudBun.MudCurveFull.Point>();
                currentFrequency = frequency;
                for (int i = 0; i < numPoints; i++)
                {
                    GameObject empty = new GameObject();
                    empty.transform.parent = this.gameObject.transform;
                    float x = i * waveLength;
                    float y = amplitude * Mathf.Sin((x + (continuousTime)) * currentFrequency);
                    empty.transform.localPosition = new Vector3(
                        x,
                        y,
                        0
                    );
                    MudBun.MudCurveFull.Point point = new MudBun.MudCurveFull.Point
                    {
                        Transform = empty?.transform,
                        Radius = 2
                    };
                    points.Add(point);
                }
                mudCurve.Points = points;
            }

            private void updateSineWave()
            {
                continuousTime += Time.deltaTime * speed;
                float oldFrequency = currentFrequency;
                currentFrequency = Mathf.Lerp(currentFrequency, frequency, frequencyLerpSpeed);
                phaseOffset += (oldFrequency - currentFrequency) * continuousTime;
                for (int i = 0; i < mudCurve.Points.Count; i++)
                {
                    float x = i * waveLength;
                    float y = amplitude * Mathf.Sin((x + (continuousTime)) * currentFrequency + phaseOffset);
                    mudCurve.Points[i].Transform.localPosition = new Vector3(x, y, 0);
                }
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                //renderer properties. 

                if (this.mudRenderer != null)
                {
                    switch (at.propertyName)
                    {
                        case "voxelDensity":
                            this.mudRenderer.VoxelDensity = _value.x;
                            break;
                        default:
                            break;
                    }
                }

                if (this.mudBox != null)
                {
                    switch (at.propertyName)
                    {
                        case "round":
                            this.mudBox.Round = _value.x;
                            break;
                    }
                }

                if (this.mudCylinder != null)
                {
                    switch (at.propertyName)
                    {
                        case "radius":
                            this.mudCylinder.Radius = _value.x;
                            break;
                        case "topRadius":
                            this.mudCylinder.TopRadiusOffset = _value.x;
                            break;
                    }
                }

                if (this.mudCurve != null)
                {
                    switch (at.propertyName)
                    {
                        case "frequency":
                            this.frequency = _value.x;
                            break;
                        case "amplitude":
                            this.amplitude = _value.x;
                            break;
                        case "waveLength":
                            this.waveLength = _value.x;
                            break;
                        case "speed":
                            this.speed = _value.x;
                            break;
                        case "radius":
                            for (int i = 0; i < mudCurve.Points.Count; i++)
                            {
                                if (at.propertyIndex == i || at.propertyIndex == -1)
                                {
                                    mudCurve.Points[i].Radius = _value.x;
                                }
                            }
                            break;
                    }
                }

                if (this.mudScroll != null)
                {
                    switch (at.propertyName)
                    {
                        case "scroll_x":
                            this.scroll_x = _value.x;
                            break;
                        case "scroll_y":
                            this.scroll_y = _value.x;
                            break;
                        case "scroll_z":
                            this.scroll_z = _value.x;
                            break;
                    }
                }

                if (this.mudNoiseModifier)
                {
                    switch (at.propertyName)
                    {
                        case "scroll_x":
                            this.scroll_x = _value.x;
                            break;
                        case "scroll_y":
                            this.scroll_y = _value.x;
                            break;
                        case "scroll_z":
                            this.scroll_z = _value.x;
                            break;
                        case "noiseThreshold":
                            this.mudNoiseModifier.Threshold = _value.x;
                            break;
                    }
                }



                if (this.mudNoiseVolume != null)
                {
                    switch (at.propertyName)
                    {
                        case "noiseThreshold":
                            this.mudNoiseVolume.Threshold = _value.x;
                            break;
                        case "noiseOffset_x":
                            //TODO - I think these scrolls are obsolete. 
                            this.noiseOffset_x = _value.x;
                            break;
                        case "noiseOffset_y":
                            this.noiseOffset_y = _value.x;
                            break;
                        case "noiseOffset_z":
                            this.noiseOffset_z = _value.x;
                            break;
                    }
                }
            }

        }
    }
}
