using DG.Tweening;
using UnityEngine;
using System.Collections.Generic;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Active
    {
        public class ActiveRigidBody : AdditiveActiveBehaviour
        {
            public Rigidbody rb;
            public Vector3 gravity;
            public Vector3 maxBounds = new Vector3(15, 15, 100);
            public Vector3 minBounds = new Vector3(-15, -15, -15);


            public override void setupComponent()
            {
                if (this.rb == null)
                {
                    this.rb = GetComponent<Rigidbody>();
                }
                if (this.rb != null)
                {
                    this.hasComponent = true;
                }
                this.gravity = Vector3.zero;
            }

            public override void updateComponent()
            {
                base.updateComponent();
                if (this.hasComponent && this.triggers != null && this.instantiated)
                {
                    if (this.gravity != Vector3.zero)
                    {
                        rb.AddForce(this.gravity, ForceMode.Acceleration);
                    }
                }
            }

            public void FixedUpdate()
            {
                if (Time.frameCount % 3 == 0 && this.o != null)
                {
                    {
                        if (transform.position.x < minBounds.x || transform.position.x > maxBounds.x ||
                            transform.position.y < minBounds.y || transform.position.y > maxBounds.y ||
                            transform.position.z < minBounds.z || transform.position.z > maxBounds.z)
                        {
                            this.Hide();
                        }
                    }
                }
                this.updateComponent();
            }


            public override void Update()
            {
                //don't call update. 
                //base.Update();
            }

            private void updateForce(ActiveTrigger at, Vector3 _value)
            {
                rb.AddForce(_value, ForceMode.Force);
            }

            private void updateTorque(ActiveTrigger at, Vector3 _value)
            {
                rb.AddTorque(_value);
            }

            private void sendImpulse(Vector3 _value)
            {
                //note - past compositions might be broken because this is now called on fixedUpdate. 
                rb.AddForce(_value, ForceMode.Impulse);
            }


            public override void Trigger(TriggerObj triggerObj, ActiveTrigger at, Obj o)
            {
                if (this.hasComponent && at.target != null)
                {
                    switch (at.propertyName)
                    {
                        case "impulse":
                            Vector3 impulseValue = Vector3.Scale(at.target.getVector3(), Vec.lerpVector3(at.velocity, triggerObj.velocity));
                            sendImpulse(impulseValue);
                            break;
                        default:
                            base.Trigger(triggerObj, at, o);
                            break;
                    }
                }
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent)
                {
                    switch (at.propertyName)
                    {
                        case "force":
                        case "":
                            //HEADS UP - this empty string might cause bugs. 
                            updateForce(at, _value);
                            break;
                        case "impulse":
                            //dont send this more than one frame
                            sendImpulse(_value);
                            break;
                        case "gravity":
                            gravity = _value;
                            break; ;
                        case "torque":
                            updateTorque(at, _value);
                            break;
                    }
                }
            }
        }
    }
}