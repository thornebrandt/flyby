using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using System;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveBone : ActiveBehaviour
        {
            // meant for expressive abstract controls down chains of bones, from tentacles. 
            //public List<Transform> roots;

            public DynamicBoneColliderBase[] externalColliders;
            private List<DynamicBoneColliderBase> allColliders;
            private List<DynamicBone> dbs;


            //storing list of original rigidBody states. 
            private Rigidbody[] rigidBodies;
            private Quaternion[] originalRigidRotations;
            private Vector3[] originalRigidLocations;


            public List<BoneChain> boneChains;
            public float debugElasticTrigger = 0.2f;
            public override void setupActiveTriggers(Obj o)
            {
                // useful for generic and dynamic but custom behaviours. 
                // to quickly make variations via json. 
                if (this.triggers == null || this.triggers.Count == 0)
                {
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);

                    }
                }
                this.instance = this.gameObject;
                setupBones();
            }

            private void setupBones()
            {
                copyChains(boneChains);
                setupChain(boneChains);
                List<DynamicBoneColliderBase> allColliders = attachDynamicColliders(boneChains);
                assignDynamicBones(allColliders);
                assignRigidBodies();
            }

            public override void Update()
            {
                updateBoneChains();
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                Kill();
                resetRigidBodies();
                if (this.boneChains.Count > 0 && this.triggers != null)
                {
                    this.checkForCachedKnobs(this.triggers[0]);
                    foreach (ActiveTrigger at in this.triggers)
                    {
                        if (boneChains.Count > at.propertyIndex)
                        {
                            BoneChain chain = boneChains[at.propertyIndex];
                            introBoneChain(at, chain);
                        }
                    }
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (a != null && a.target != null && this.boneChains.Count > a.propertyIndex)
                {
                    BoneChain chain = boneChains[a.propertyIndex];
                    bool existingTrigger = false;

                    foreach (ActiveTrigger at in this.triggers)
                    {
                        if (at.propertyIndex == a.propertyIndex)
                        {
                            if (at.propertyName == a.propertyName)
                            {
                                at.triggerSequence.Kill();  //should cover all exiting triggers and current trigger. 
                                if (at.animatingTrigger)
                                {
                                    at.starting = false;
                                    at.triggerLerpValue = 0;
                                    existingTrigger = true;
                                }
                            }
                        }
                    }

                    switch (a.propertyName)
                    {
                        case "rotation":
                        default:

                            if (existingTrigger)
                            {
                                //save currentRotations so that next trigger continues smoothly. 
                                for (int i = 0; i < chain.bones.Length; i++)
                                {
                                    chain.currentRotations[i] = chain.bones[i].localRotation;
                                }
                            }

                            //otherwise we want it to go back to it's default value. 
                            manualRotateBoneChain(a, chain);
                            break;
                    }

                    a.targetValue = a.target.getVector3(); //placeholder?
                    a.triggerSequence = DOTween.Sequence();
                    AdditiveTriggerSequence(a, triggerObj.sustain);
                    a.lastTargetValue = a.targetValue;
                    base.Trigger(triggerObj, a, o);
                    if (!triggerObj.sustain)
                    {
                        AdditiveTriggerOffSequence(a);
                    }
                }
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (a != null && this.boneChains.Count > a.propertyIndex)
                {
                    BoneChain chain = boneChains[a.propertyIndex];
                    for (int i = 0; i < chain.bones.Length; i++)
                    {
                        Transform bone = chain.bones[i];
                        if (a.offTime < 0)
                        {
                            //it aint coming back, change value. 
                            chain.currentRotations[i] = bone.localRotation;
                        }
                        else
                        {
                            //go back to original value.  
                            chain.currentRotations[i] = chain.originalRotations[i];
                        }
                    }
                }
            }





            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //handing target sustain off
                if (a != null && this.boneChains.Count > a.propertyIndex)
                {
                    BoneChain chain = boneChains[a.propertyIndex];
                    foreach (ActiveTrigger at in this.triggers)
                    {
                        if (at.propertyIndex == a.propertyIndex)
                        {
                            at.triggerSequence.Kill();
                        }
                        if (at.animatingTrigger)
                        {
                            at.starting = false;
                            at.animatingTrigger = false;
                            a.triggerLerpValue = 0;
                        }
                    }
                    for (int i = 0; i < chain.bones.Length; i++)
                    {
                        chain.currentRotations[i] = chain.originalRotations[i];
                    }
                }

                a.triggerSequence = DOTween.Sequence();
                a.currentAnimatedValue = Vector3.zero;  //not sure if this should be zero. 
                AdditiveTriggerOffSequence(a);
            }

            private void smoothNoiseRotation(BoneChain chain)
            {
            }

            private void updateBoneChains()
            {
                //switch to chains. 

                int chainI = 0;
                foreach (BoneChain chain in this.boneChains)
                {
                    resetKnobRotations(chain);
                    //smoothNoiseRotation(chain);

                    for (int i = 0; i < this.triggers.Count; i++)
                    {
                        ActiveTrigger at = this.triggers[i];
                        if (at.propertyIndex == chainI)
                        {
                            //handle start and target animations. 
                            if (at.target != null && at.animatingTrigger)
                            {
                                at.animatedValue = Vector3.Lerp(at.currentAnimatedValue, at.targetValue, at.triggerLerpValue);
                                chain.db.m_Elasticity = Mathf.Lerp(debugElasticTrigger, chain.elasticity, triggerLerpValue);
                                chain.db.m_BlendWeight = 1.0f - triggerLerpValue; //experimental. 
                                updateBoneChain(at, chain);
                            }
                        }

                        //handle knobs across all chains.
                        if (at.knob != null)
                        {
                            float mappedValue = Tools.Math.Lerp(at.knob.x, at.knob.x2, at.knobValue);
                            mappedValue = at.triggerProperty != null ? at.triggerProperty.map(mappedValue) : mappedValue;
                            switch (at.propertyName)
                            {
                                case "elasticity":
                                    chain.db.m_Elasticity = mappedValue;
                                    chain.db.UpdateParameters();
                                    break;
                                case "damping":
                                    chain.db.m_Damping = mappedValue;
                                    chain.db.UpdateParameters();
                                    break;
                                case "inert":
                                    chain.db.m_Inert = mappedValue;
                                    chain.db.UpdateParameters();
                                    break;
                                case "stiffness":
                                    chain.db.m_Stiffness = mappedValue;
                                    chain.db.UpdateParameters();
                                    break;
                                case "weight":
                                    chain.db.m_BlendWeight = mappedValue;
                                    chain.db.UpdateParameters();
                                    break;
                                case "updateRate":
                                    chain.db.m_UpdateRate = mappedValue;
                                    chain.db.UpdateParameters();
                                    break;
                                case "rotation":
                                    if (at.propertyIndex == chainI)
                                    {
                                        updateKnobRotations(at, chain);
                                    }
                                    break;
                            }
                            updateBoneChain(at, chain);
                        }
                    }
                    chainI++;
                }
                this.animatingKnob = false;
                //for rigid bodies 
                // RigidBody.velocity  might be interesting as a knob. 
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                this.triggerController = getTriggerController();
                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }

                int chainI = 0;
                foreach (BoneChain chain in this.boneChains)
                {
                    chain.knobRotation = Quaternion.identity;
                    foreach (ActiveTrigger at in this.triggers)
                    {
                        if (at.propertyName == "rotation" && at.propertyIndex == chainI && at.knob != null)
                        {
                            TriggerObj t = checkForCachedKnob(at);
                            at.knobValue = t != null ? t.value : at.knobValue;
                            chain.knobRotation = chain.knobRotation * Quaternion.Euler(at.knob.lerpVector3(at.knobValue));
                        }
                    }
                    chainI++;
                }
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true;
                cacheKnob(knobEvent);
                // foreach (ActiveTrigger at in this.triggers) {
                //     if (at.trigger == knobEvent.eventName) {
                //         at.knobValue = knobEvent.value;
                //         at.animatingKnob = true;
                //     }
                // }
                int chainI = 0;
                foreach (BoneChain chain in this.boneChains)
                {
                    chain.knobRotation = Quaternion.identity;
                    foreach (ActiveTrigger at in this.triggers)
                    {
                        if (at.propertyName == "rotation" && at.propertyIndex == chainI)
                        {
                            TriggerObj t = checkForCachedKnob(at);
                            at.knobValue = t != null ? t.value : at.knobValue;
                            chain.knobRotation = chain.knobRotation * Quaternion.Euler(at.knob.lerpVector3(at.knobValue));
                        }
                    }
                    chainI++;
                }
            }

            private void manualRotateBoneChain(ActiveTrigger at, BoneChain chain)
            {
                //for dance moves. 
                for (int i = 0; i < chain.bones.Length; i++)
                {
                    Transform bone = chain.bones[i];



                    float curveRatio = at.curve == null ? 1 : at.curve.Evaluate((float)i / chain.bones.Length);
                    float smoothRatio = chain.smoothness < 1 ? 1 : Mathf.PingPong(i, chain.smoothness) / chain.smoothness;

                    Quaternion newTargetRotation = Quaternion.Euler(at.target.getVector3() * smoothRatio * curveRatio);
                    Quaternion curveRotation = Quaternion.identity;

                    if (at.target.relative)
                    {
                        newTargetRotation = bone.transform.localRotation * newTargetRotation;
                    }

                    if (at.triggerProperty != null)
                    {
                        if (at.triggerProperty.max > 1)
                        {
                            float max = at.triggerProperty.max;
                            Vector3 rot = newTargetRotation.eulerAngles;
                            rot.x = Mathf.Min(rot.x, max);
                            rot.y = Mathf.Min(rot.y, max);
                            rot.z = Mathf.Min(rot.z, max);
                            rot.x = Mathf.Max(rot.x, -max);
                            rot.y = Mathf.Max(rot.y, -max);
                            rot.z = Mathf.Max(rot.z, -max);
                            newTargetRotation = Quaternion.Euler(rot);
                        }
                    }

                    if (at.animatingTrigger)
                    {
                        at.starting = false; //repeated but that's ok. 
                        at.triggerLerpValue = 0; //repeated but that's ok. 
                        chain.currentRotations[i] = bone.transform.localRotation;
                    }

                    chain.targetRotations[i] = newTargetRotation;
                    //chain.db.m_UpdateMode = DynamicBone.UpdateMode.AnimatePhysics;
                    // chain.db.m_Elasticity = 1;
                    // chain.db.m_ElasticityDistrib = new AnimationCurve();
                    chain.db.UpdateParameters();
                }
            }

            private void introBoneChain(ActiveTrigger at, BoneChain chain)
            {
                //TODO - working on target rotaitons first. 
                if (at.value != null)
                {
                    for (int i = 0; i < chain.bones.Length; i++)
                    {
                        Transform bone = chain.bones[i];
                        chain.originalRotations[i] = at.value.getQuaternion(); //randomizing each bone. 
                        chain.currentRotations[i] = chain.originalRotations[i];
                        bone.transform.localRotation = chain.originalRotations[i];
                    }
                }
            }

            private void resetKnobRotations(BoneChain chain)
            {
                // rotations are multiplied ? 
                for (int i = 0; i < chain.knobRotations.Length; i++)
                {
                    chain.knobRotations[i] = Quaternion.identity;
                }
            }

            public void updateKnobRotations(ActiveTrigger at, BoneChain chain)
            {
                for (int i = 0; i < chain.bones.Length; i++)
                {
                    Transform bone = chain.bones[i];
                    if (bone != null)
                    {
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * multiplier) : at.knobValue * multiplier;
                        //TODO - add some animationCurves. 
                        Vector3 eulerRotation = at.knob.lerpVector3(mappedValue);
                        chain.knobRotations[i] = chain.knobRotations[i] * Quaternion.Euler(eulerRotation);
                    }
                }
            }

            private void updateBoneChain(ActiveTrigger at, BoneChain chain)
            {
                for (int i = 0; i < chain.bones.Length; i++)
                {
                    Transform bone = chain.bones[i];
                    if (bone != null)
                    {
                        Quaternion currentRotation = chain.currentRotations[i];
                        Quaternion targetRotation = chain.targetRotations[i];
                        bone.localRotation = Quaternion.Lerp(currentRotation, targetRotation, at.triggerLerpValue) * chain.knobRotations[i];
                    }
                    //float positionRatio = rotation.distrib == null ? 1 : rotation.distrib.Evaluate ((float) i / chain.bones.Length);
                    //float ping = Mathf.PingPong(i, rotation.smoothness);

                }
            }

            private void copyChains(List<BoneChain> boneChains)
            {
                //in case they need to be copied. 
                for (int i = 0; i < boneChains.Count; i++)
                {
                    BoneChain chain = boneChains[i];
                    if (chain.copyI > -1 && chain.copyI < boneChains.Count)
                    {
                        // has a copy and copy is not itself.
                        // does not copy root. 
                        BoneChain cp = boneChains[chain.copyI];
                        chain.smoothness = cp.smoothness;
                        chain.damping = cp.damping;
                        chain.dampingDistrib = cp.dampingDistrib;
                        chain.elasticity = cp.elasticity;
                        chain.elasticityDistrib = cp.elasticityDistrib;
                        chain.stiffness = cp.stiffness;
                        chain.stiffnessDistrib = cp.stiffnessDistrib;
                        chain.inert = cp.inert;
                        chain.inertDistrib = cp.inertDistrib;
                        chain.force = cp.force;
                        chain.colliderRadius = cp.colliderRadius;
                        chain.colliderSpacing = cp.colliderSpacing;
                    }
                }
            }

            private void setupChain(List<BoneChain> boneChains)
            {
                dbs = new List<DynamicBone>();
                //int length = Mathf.Min(boneChains.Count, roots.Count); //WTF is this doing??
                for (int i = 0; i < boneChains.Count; i++)
                {
                    BoneChain chain = boneChains[i];
                    //chain.root = roots[i]; //needs to be assinged in the editor;
                    Transform[] bones = chain.root.GetComponentsInChildren<Transform>();
                    chain.bones = bones;
                    chain.originalRotations = new Quaternion[chain.bones.Length];
                    chain.currentRotations = new Quaternion[chain.bones.Length];
                    chain.targetRotations = new Quaternion[chain.bones.Length];
                    chain.knobRotations = new Quaternion[chain.bones.Length];
                    chain.noiseRotations = new Quaternion[chain.bones.Length];
                    for (int j = 0; j < chain.bones.Length; j++)
                    {
                        chain.originalRotations[j] = chain.bones[j].localRotation;
                        chain.currentRotations[j] = chain.bones[j].localRotation;
                        chain.targetRotations[j] = Quaternion.identity;
                        chain.knobRotations[i] = Quaternion.identity; //needs to find knobs here. 
                        chain.noiseRotations[i] = Quaternion.identity;
                    }
                }
            }

            private void assignDynamicBones(List<DynamicBoneColliderBase> allColliders)
            {
                foreach (BoneChain chain in boneChains)
                {
                    List<DynamicBoneColliderBase> collidersMasked = getMaskedColliders(chain);
                    DynamicBone new_db = gameObject.AddComponent<DynamicBone>();
                    new_db.m_Root = chain.root; //keep this as original chain. 
                    new_db.m_Colliders = collidersMasked;
                    new_db.m_Damping = chain.damping;
                    new_db.m_DampingDistrib = chain.dampingDistrib;
                    new_db.m_Elasticity = chain.elasticity;
                    new_db.m_ElasticityDistrib = chain.elasticityDistrib;
                    new_db.m_Stiffness = chain.stiffness;
                    new_db.m_StiffnessDistrib = chain.stiffnessDistrib;
                    new_db.m_Inert = chain.inert;
                    new_db.m_InertDistrib = chain.inertDistrib;
                    new_db.m_Force = chain.force;
                    new_db.m_Radius = chain.colliderRadius;
                    new_db.m_UpdateRate = 45;
                    new_db.m_DistantDisable = true;
                    new_db.m_DistanceToObject = 40;
                    new_db.m_ReferenceObject = FlyByController.instance.cameraController.getCam().transform;
                    dbs.Add(new_db);
                    chain.db = new_db; //original chain
                }
            }

            public void addMoreColliders(List<DynamicBoneColliderBase> moreColliders)
            {
                List<DynamicBoneColliderBase> collidersMasked = new List<DynamicBoneColliderBase>(moreColliders);
                foreach (BoneChain chain in boneChains)
                {
                    List<DynamicBoneColliderBase> thisColliders = chain.colliders;
                    foreach (DynamicBoneColliderBase collider in allColliders)
                    {
                        foreach (DynamicBoneColliderBase thisCollider in thisColliders)
                        {
                            if (collider == thisCollider)
                            {
                                collidersMasked.Remove(collider);
                            }
                        }
                    }
                }

                DynamicBone[] dbs = gameObject.GetComponents<DynamicBone>();
                foreach (DynamicBone db in dbs)
                {
                    db.m_Colliders.AddRange(collidersMasked);
                }
            }

            List<DynamicBoneColliderBase> getMaskedColliders(BoneChain chain)
            {
                List<DynamicBoneColliderBase> thisColliders = chain.colliders;
                List<DynamicBoneColliderBase> collidersMasked = new List<DynamicBoneColliderBase>(allColliders);

                foreach (DynamicBoneColliderBase collider in allColliders)
                {
                    foreach (DynamicBoneColliderBase thisCollider in thisColliders)
                    {
                        if (collider == thisCollider)
                        {
                            collidersMasked.Remove(collider);
                        }
                    }
                }
                if (chain.addExternalColliders)
                {
                    collidersMasked.AddRange(externalColliders);

                }
                return collidersMasked;
            }

            List<DynamicBoneColliderBase> attachDynamicColliders(List<BoneChain> boneChains)
            {
                allColliders = new List<DynamicBoneColliderBase>();
                foreach (BoneChain chain in boneChains)
                {
                    chain.colliders = new List<DynamicBoneColliderBase>();
                    for (int i = 0; i < chain.bones.Length; i++)
                    {
                        if (chain.colliderSpacing == 0 || i % chain.colliderSpacing == 0)
                        {
                            Transform bone = chain.bones[i];
                            DynamicBoneCollider collider = bone.gameObject.AddComponent<DynamicBoneCollider>();
                            collider.m_Radius = chain.colliderRadius;
                            chain.colliders.Add(collider);
                            allColliders.Add(collider);
                        }
                    }
                }

                //allColliders.AddRange(externalColliders);
                return allColliders;
            }

            //---------------------
            //traditional physics / ragdoll etc. 

            private void assignRigidBodies()
            {
                this.rigidBodies = GetComponentsInChildren<Rigidbody>();
                this.originalRigidLocations = new Vector3[rigidBodies.Length];
                this.originalRigidRotations = new Quaternion[rigidBodies.Length];
                for (int i = 0; i < this.rigidBodies.Length; i++)
                {
                    this.originalRigidLocations[i] = this.rigidBodies[i].transform.localPosition;
                    this.originalRigidRotations[i] = this.rigidBodies[i].transform.localRotation;
                }
            }

            private void resetRigidBodies()
            {
                for (int i = 0; i < this.rigidBodies.Length; i++)
                {
                    Rigidbody rb = this.rigidBodies[i];
                    rb.gameObject.SetActive(false);
                    rb.velocity = Vector3.zero;
                    rb.angularVelocity = Vector3.zero;
                    rb.transform.localPosition = this.originalRigidLocations[i];
                    rb.transform.localRotation = this.originalRigidRotations[i];
                    rb.gameObject.SetActive(true);
                }
            }

        }
    }

    [Serializable]
    public class BoneChain
    {
        public string id;
        [Tooltip("Put index of chain you want to duplicate. (still add a new root)")]
        public int copyI; //copy integer. 
        public Transform root;
        public float colliderRadius;
        public float colliderSpacing;
        [Tooltip("affects how harsh manual rotations are")]
        public int smoothness; //distributes noise curves over multiple joints, had a note to delete. 
        public float damping = 0.8f;
        public AnimationCurve dampingDistrib;
        public float elasticity;
        public AnimationCurve elasticityDistrib;
        public float stiffness;
        public AnimationCurve stiffnessDistrib;
        public float inert;
        public AnimationCurve inertDistrib;
        [HideInInspector]
        public List<DynamicBoneColliderBase> colliders;
        public bool addExternalColliders;
        [HideInInspector]
        public Transform[] bones;
        public Quaternion[] originalRotations;
        public Quaternion[] currentRotations;
        public Quaternion[] targetRotations;
        public Quaternion[] knobRotations;
        public Quaternion[] noiseRotations; //storing random subtle movement. 
        public Quaternion knobRotation;
        public Vector3 force;
        [HideInInspector]
        public DynamicBone db;
        public Sequence triggerSequence;
        public float triggerLerpValue;

        public BoneChain()
        {
            Keyframe[] ks = new Keyframe[2];
            ks[0] = new Keyframe(0, 0.2f);
            ks[1] = new Keyframe(1, 1);
            this.dampingDistrib = new AnimationCurve(ks);
            this.colliderRadius = 0.003f;
            this.colliderSpacing = 2;
            this.damping = 0.9f;
            this.elasticity = 0.4f;
            this.stiffness = 0.07f;
            this.inert = 0.1f;
            this.smoothness = 1; //bone distribution, should probably delete soon 
            this.colliders = new List<DynamicBoneColliderBase>();
            this.addExternalColliders = true;
            this.copyI = -1; //copy integer. 
        }

        public BoneChain(BoneChain chain)
        {
            this.id = chain.id;
            this.dampingDistrib = chain.dampingDistrib;
            this.colliderRadius = chain.colliderRadius;
            this.colliderSpacing = chain.colliderSpacing;
            this.damping = chain.damping;
            this.elasticity = chain.elasticity;
            this.stiffness = chain.stiffness;
            this.smoothness = chain.smoothness;
            this.inert = chain.inert;
            this.colliders = chain.colliders;
            this.addExternalColliders = chain.addExternalColliders;
        }

    }
}


