using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using System.Collections.Generic;

namespace flyby {
    namespace Active {
        public class ActiveSequentialAnimation : ActiveBehaviour {
            //animation does not have a way to trigger by index. 
            //so you must name the animations sequentially. 
            //stupid, right?

            //NOTICE: model's rig must be set to "Legacy"
            public int _numAnimations; //num animation is the x value of mainTrigger value ( can be randomized ) 
            public float _animationSpeed = 1; //speed is the x of secondaryTrigger target
            public float _fadeTime = 0.2f;
            public string _animationName;
            private int _animationIndex; //animation index i sthe y value of mainTrigger value, so it can be randomized. 
            public int zeroValue = 0;
            private Animation anim;

            public override void setupActiveTriggers(Obj o) {
                if (this.triggers == null || this.triggers.Count == 0) {
                    if (o.activeTrigger != null) {
                        this.assignSecondaryTriggers(o.activeTrigger);
                    }
                }
                this.instance = this.gameObject;
                anim = GetComponent<Animation>();
            }


            public override void Intro(TriggerObj triggerObj, Obj o) {
                if (anim != null) {
                    if (this.triggers != null && this.triggers.Count > 0) {
                        ActiveTrigger a = this.triggers[0];
                        this._animationIndex = a.value == null ? this._animationIndex : (int)a.value.getVector3().y;
                        this._numAnimations = a.value == null ? this._numAnimations : (int)a.value.getVector3().x;
                        this._animationIndex = (int)Mathf.Repeat(this._animationIndex, this._numAnimations);
                        string animationClip = this._animationName + (this._animationIndex + zeroValue);
                        if (anim[animationClip] != null) {
                            anim[animationClip].speed = 0;
                            anim.Play(animationClip); //should pause on first frame of clip. 
                        } else {
                            Debug.Log("no animation clip: " + animationClip);
                        }
                    }
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o) {
                if (anim != null) {
                    string animationClip = _animationName + (_animationIndex + zeroValue);
                    if (anim[animationClip] != null) {
                        anim[animationClip].speed = a.target == null ? _animationSpeed : a.target.x;
                        anim.CrossFade(animationClip, a.onTime);
                        //anim.Play (animationClip);
                    } else {
                        Debug.Log("no animation clip for _animationIndex: " + _animationIndex + " zeroValue: " + zeroValue);
                    }

                    _animationIndex++;
                    this._animationIndex = (int)Mathf.Repeat(this._animationIndex, this._numAnimations);
                }
            }
        }
    }
}