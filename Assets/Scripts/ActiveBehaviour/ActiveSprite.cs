using DG.Tweening;
using UnityEngine;
using flyby.Core;
using System.Collections.Generic;
using flyby.Triggers;
using flyby.Controllers;
using UnityEngine.Rendering.HighDefinition;

// TODO - this is not a sprite this needs to be renamed to ActiveTexture ActiveAnimatedTexture. 
// ActiveSprite needs to  actually use sprites for various positioning and slicing techniques. 

namespace flyby
{
    namespace Active
    {
        public class ActiveSprite : ActiveBehaviour
        {
            public Media sprite; //sprite that is being used, used externally for tests. 
            private Material material;
            private Media startSprite;
            private Media currentSprite;
            private Media endSprite;
            private Media lastSprite;
            private FlyByController flyByController;
            public Renderer rend;
            private MaterialPropertyBlock mpb;
            private AssetController assetController;
            private Texture originalTexture;
            private Texture startTexture;
            private Texture currentTexture;
            private Texture endTexture;
            private Texture targetTexture;
            private Texture lastTargetTexture;
            private Vector2 uv_size;
            private Vector2 uv_index;
            private Vector2 uv_offset;
            private int framesSinceStart; //reset on sprite switch. making sure first frame gets justice.
            private int loopsSinceStart; //number of times it
            public int _endFrame; // 
            private bool playing;
            private float knobValue = -1;
            private int knobFrame = -1;
            private static readonly string[] defaultTextureProperties = { "_UnlitColorMap" };
            private string materialProperty = "_UnlitColorMap"; //set this. 
            private DecalProjector decalProjector;

            public override void setupActiveTriggers(Obj o)
            {
                mpb = new MaterialPropertyBlock();
                this.material = getMaterial();
                this.assetController = getAssetController();
                preloadTextures();
                base.setupActiveTriggers(o);
            }

            private Material getMaterial()
            {
                //TODO - figure out how to make less WET - 

                if (this.material != null) return material;
                if (instance == null) instance = this.gameObject;

                if (rend == null)
                {
                    rend = GetComponent<Renderer>();
                }

                //first check if normal renderer and return this.material.
                if (rend != null)
                {
                    if (rend.material.name.EndsWith("(Instance)")) return rend.material; // a hack to prevent double instantiation. 
                    this.material = new Material(rend.material);
                    this.material.name += "(Instance)";
                    rend.material = this.material;
                    return this.material;
                }

                if (rend == null)
                {
                    this.decalProjector = this.GetComponent<DecalProjector>();
                    if (this.decalProjector != null)
                    {
                        if (decalProjector.material.name.EndsWith("(Instance)")) return this.decalProjector.material; //a hack to prevent double instantiation. 

                        this.material = new Material(decalProjector.material);
                        this.material.name += "(Instance)";
                        this.decalProjector.material = this.material;
                        return this.material;
                    }
                    else
                    {
                        Debug.Log("can't find any material on " + this.gameObject.name);
                        return null;
                    }
                }

                return this.material;
            }

            public Vector3 adjustScale(Media media)
            {
                if (media.loadedTexture == null)
                {
                    return Vector3.zero;
                }
                float ratio;
                float width = (float)media.loadedTexture.width / media.cols;
                float height = (float)media.loadedTexture.height / media.rows;

                if (width >= height)
                {
                    ratio = height / width;
                    return new Vector3(1.0f, ratio, 1.0f);
                }
                else
                {
                    ratio = width / height;
                    return new Vector3(ratio, 1.0f, 1.0f);
                }
            }

            private void setSortingOrder(int nextSortingOrder)
            {
                //new function for decals. 

                if (this.decalProjector != null)
                {
                    decalProjector.material.renderQueue = nextSortingOrder;
                    decalProjector.material.SetFloat("_DrawOrder", nextSortingOrder);
                }
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                //TODO - needs a media controller for storing textures and videos. 
                this.o = o;
                Kill();
                this.triggerController = base.getTriggerController();
                this.flyByController = getFlyByController();

                if (this.triggers == null || this.triggers.Count == 0)
                {
                    Debug.Log("no triggers on intro for " + this.gameObject.name);
                    return;
                }

                ActiveTrigger a = this.triggers[0];
                if (a != null && this.assetController != null && this.material != null)
                {
                    lerpValue = 0;
                    originalTexture = this.material.GetTexture(this.materialProperty);
                    if (a.startMedia != null)
                    {
                        startTexture = getNextTextureVariant(a.startMedia);
                        instance.transform.localScale = adjustScale(a.startMedia);
                        startSprite = a.startMedia;
                        sprite = startSprite;
                    }
                    else
                    {
                        startTexture = originalTexture;
                        a.startMedia = a.media;
                    }

                    this.checkForCachedKnobs(a);

                    if (a.media != null)
                    {
                        currentTexture = getNextTextureVariant(a.media);
                        instance.transform.localScale = adjustScale(a.media);
                        currentSprite = a.media;
                        sprite = a.media;
                        uv_size = new Vector2(
                            1.0f / sprite.cols,
                            1.0f / sprite.rows
                        );
                        if (a.media.alwaysFront)
                        {
                            int nextSortingOrder = FlyByController.instance.currentSong.GetNextSortingOrder();
                            this.setSortingOrder(nextSortingOrder);
                        }
                        else
                        {
                            this.setSortingOrder(a.media.sortingOrder);
                        }
                    }
                    else
                    {
                        currentSprite = a.startMedia;
                        currentTexture = startTexture;
                    }

                    if (a.endMedia != null)
                    {
                        endTexture = getNextTextureVariant(a.endMedia);
                        instance.transform.localScale = adjustScale(a.endMedia);
                    }

                    if (a.startTime <= 0)
                    {
                        startTexture = currentTexture;
                    }

                    if (_sequence != null)
                    {
                        _sequence.Kill();
                    }

                    if (startSprite == null)
                    {
                        startSprite = sprite;
                    }

                    if (startSprite == sprite)
                    {
                        a.startTime = 0;
                    }

                    _sequence = DOTween.Sequence();
                    this.material.SetTexture(this.materialProperty, startTexture);
                    base.IntroSequence(_sequence, a);
                    if (!triggerObj.sustain && endTexture != null && a.endTime > 0)
                    {
                        base.EndSequence(_sequence, a);
                    }

                    setupSprite(startSprite); //startSprite will be sprite if start is null.

                    base.Intro(triggerObj, o);

                }
            }

            private int getSortingOrderFromZ(Media media)
            {
                //depreceated. 
                if (media.sortByZ)
                {
                    float remappedFloat = Tools.Math.Remap(-5, 15, 100, -100, instance.transform.position.z);
                    return (int)remappedFloat;
                }
                return media.sortingOrder;
            }


            public override void Update()
            {
                if (this.material != null && playing)
                {
                    SetSpriteFrame();
                    framesSinceStart++;
                }
            }

            private void setupSprite(Media sprite)
            {
                sprite.oi = _i;
                sprite.frame = (int)Tools.Rand.Between(sprite.startFrame, sprite.startFrame2);
                sprite.totalFrames = sprite.totalFrames == 0 ? sprite.cols * sprite.rows : sprite.totalFrames;
                _endFrame = (int)Tools.Rand.Between(sprite.endFrame, sprite.endFrame2);
                _endFrame = _endFrame == -1 ? (sprite.totalFrames - 1) : _endFrame;
                framesSinceStart = 0;
                loopsSinceStart = 0;
                playing = sprite.totalFrames > 1;
                if (playing)
                {
                    // this is messing up the texture offset. 
                    // I think we can fix it by also making that a material property block. 
                    renderSpriteFrame();
                }
            }

            private void SetSpriteFrame()
            {
                if (sprite.stops > 0 && (framesSinceStart != 0 && framesSinceStart % sprite.stops == 0))
                {
                    sprite.frame = getNextFrameIndex(sprite);
                }
                if (knobFrame > -1 && animatingKnob)
                {
                    sprite.frame = knobFrame;
                }
                renderSpriteFrame();
            }

            private void renderSpriteFrame()
            {
                uv_index = new Vector2(
                    sprite.frame % sprite.cols,
                    sprite.frame / sprite.cols
                );
                uv_offset = new Vector2(
                    uv_index.x * uv_size.x,
                    (1.0f - uv_size.y) - (uv_index.y) * uv_size.y
                );

                //testing if this is the bug - not going to use mpb anymore. 
                // mpb.SetVector("_UnlitColorMap", new Vector4(uv_index.x, uv_index.y, uv_offset.x, uv_offset.y));
                // if (rend != null) rend.SetPropertyBlock(mpb);

                this.material.SetTextureOffset("_UnlitColorMap", uv_offset);
                this.material.SetTextureScale("_UnlitColorMap", uv_size);
                this.material.SetVector("_UVOffset", uv_offset);
                this.material.SetVector("_UVScale", uv_size);
            }


            private int getNextFrameIndex(Media _sprite)
            {
                int i = _sprite.frame;
                i++;
                if (i > _endFrame)
                {
                    if (_sprite.repeat > -1 && loopsSinceStart >= _sprite.repeat)
                    {
                        i = _endFrame;
                        endAnimation(_sprite);
                        return i;
                    }
                    i = _sprite.startFrame;
                    loopsSinceStart++;
                }
                return i;
            }

            private void endAnimation(Media _sprite)
            {
                playing = false;
                if (_sprite.hideOnComplete)
                {
                    // TODO - we might want to override activeBehaviour hide 
                    // so that we don't accidentally put hideOnComplete in trigger instead of sprite.
                    Hide();
                }
            }

            public override void OnIntroComplete(ActiveTrigger a)
            {
                if (instance != null && this.material != null)
                {
                    this.material.SetTexture("_UnlitColorMap", currentTexture);
                    sprite = currentSprite;
                    setupSprite(sprite);
                }
            }

            public override void OnEndComplete(ActiveTrigger a)
            {
                if (instance != null && this.material != null)
                {
                    this.material.SetTexture("_UnlitColorMap", endTexture);
                }
            }

            public override void Release()
            {
                //first time this is being added to parent activeBehaviour.
                //check for doubles in activeColor. 

                if (_sequence != null)
                {
                    _sequence.Kill();
                }

                if (this.triggers[0] != null && this.triggers[0].endMedia != null)
                {
                    _sequence = DOTween.Sequence();
                    EndSequence(_sequence, this.triggers[0]);
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (a.targetMedia != null)
                {
                    a.targetMedia.loadedTexture = AssetController.instance.LoadTexture(a.targetMedia.path);
                    instance.transform.localScale = adjustScale(a.targetMedia);
                    if (_triggerSequence != null)
                    {
                        _triggerSequence.Kill();
                    }

                    _triggerSequence = DOTween.Sequence();
                    TriggerSequence(_triggerSequence, a);

                    if (!triggerObj.sustain && a.offTime >= 0)
                    {
                        TriggerOffSequence(_triggerSequence, a);
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                //TODO - this should probably be the generic, probaly just need global methods such as .
                //OnTriggerStart and OnTriggerOff
                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                TriggerOffSequence(_triggerSequence, a);
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                //we can't really transition in between frames so this is just a timer. 
                if (a.targetMedia != null && a.targetMedia.loadedTexture != null)
                {
                    a.targetMedia.loadedTexture = getNextTextureVariant(a.targetMedia);
                    this.material.SetTexture("_UnlitColorMap", a.targetMedia.loadedTexture);
                }
            }

            public override void OnTriggerOffComplete(ActiveTrigger a)
            {
                //this value should always exist. 
                if (instance != null && this.material != null)
                {
                    this.material.SetTexture("_UnlitColorMap", currentTexture);
                }
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true;
                if (this.sprite != null)
                {
                    knobValue = 0;
                    cacheKnob(knobEvent);
                    float mappedValue = _a.triggerProperty != null ? _a.triggerProperty.map(_a.knobValue) : _a.knobValue;
                    knobValue = mappedValue;
                    //TODO - should this function be part of media class?
                    knobFrame = getNormalizedFrame(sprite, knobValue);
                }
            }

            private int getNormalizedFrame(Media _sprite, float _value)
            {
                return (int)((_sprite.totalFrames - 1) * _value);
            }

            private FlyByController getFlyByController()
            {
                //TODO - move this to base class. 
                if (this.flyByController != null)
                {
                    return this.flyByController;
                }
                if (FlyByController.instance != null)
                {
                    return FlyByController.instance;
                }
                return null;
            }

            private AssetController getAssetController()
            {
                //TODO - put in utility function.
                if (this.assetController != null)
                {
                    return this.assetController;
                }
                if (AssetController.instance != null)
                {
                    return AssetController.instance;
                }
                return null;
            }

            private void preloadVariants(Media media)
            {

                this.assetController = getAssetController();
                if (this.assetController != null)
                {
                    media = this.assetController.PreloadTextureVariants(media);
                }
            }

            private Texture2D getNextTextureVariant(Media media)
            {
                if (media.loadedTextures != null && media.loadedTextures.Count > 0)
                {
                    //TODO - move this logic inward. 
                    media.variant_i = media.initialVariant + (int)Mathf.Repeat(this.o.timesTriggered - 1, media.loadedTextures.Count);
                    media.loadedTexture = media.loadedTextures[media.variant_i];
                }
                return media.loadedTexture;
            }

            private Media getMediaFromString(string mediaString)
            {
                //TODO - can possibly parse the frames from the string. runcycle:3:3.png;
                Media media = new Media();
                media.path = mediaString;
                return media;
            }

            private void preloadTextures()
            {
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.startMedia != null)
                    {
                        preloadVariants(at.startMedia);
                    }
                    if (at.media != null)
                    {
                        preloadVariants(at.media);
                    }
                    if (at.endMedia != null)
                    {
                        preloadVariants(at.endMedia);
                    }
                    if (at.targetMedia != null)
                    {
                        preloadVariants(at.targetMedia);
                    }
                    //quick setup from value string ( experimental )
                    if (at.media == null && at.valueString != null)
                    {
                        at.media = getMediaFromString(at.valueString);
                        preloadVariants(at.media);
                    }
                }
            }
        }
    }
}