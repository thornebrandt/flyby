using DG.Tweening;
using UnityEngine;
using UnityEngine.VFX;
using flyby.Core;
using flyby.Triggers;

namespace flyby
{
    namespace Active
    {
        public class ActiveVFX : AdditiveActiveBehaviour
        {
            public VisualEffect vfx;

            public override void setupComponent()
            {
                if (this.vfx == null)
                {
                    this.vfx = GetComponent<VisualEffect>();
                }

                if (this.vfx != null)
                {
                    this.hasComponent = true;
                }
                else
                {
                    Debug.Log("could not find vfx");

                }
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.vfx.HasVector3(at.propertyName))
                {
                    this.vfx.SetVector3(at.propertyName, _value);
                }
            }
        }
    }
}

