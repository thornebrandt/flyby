using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveBlendShape : AdditiveActiveBehaviour
        {
            private SkinnedMeshRenderer rend;
            //NOTE - values are between 0 and 100.

            public override void setupComponent()
            {
                this.rend = GetComponent<SkinnedMeshRenderer>();
                if (this.rend != null)
                {
                    this.hasComponent = true;
                }
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent && rend != null)
                {
                    rend.SetBlendShapeWeight(at.propertyIndex, _value.x);
                }
            }

            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                Vector3 value = new Vector3();
                if (this.hasComponent && this.rend != null)
                {
                    value.x = rend.GetBlendShapeWeight(at.propertyIndex);
                }
                return value;
            }

        }
    }
}