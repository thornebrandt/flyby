using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using System;


namespace flyby
{
    namespace Active
    {
        [Serializable]
        public struct VirtualCameraAngle
        {
            public string name;
            public CinemachineVirtualCamera camera;
            public int i;
        }

        public class ActiveVirtualCameras : AdditiveActiveBehaviour
        {

            public List<CinemachineVirtualCamera> virtualCameras;
            private CinemachineVirtualCamera currentCamera;

            //add a tooltip
            [Tooltip("define what max shake will look like.")]
            public CinemachineVirtualCamera shakeCamera;

            //add horizontal line. 
            [Header("Camera Angles")]

            private CinemachineBasicMultiChannelPerlin shakePerlin;
            private CinemachineBasicMultiChannelPerlin currentCameraPerlin; //cache the perlin. ? 
            private bool hasShakeCamera = false;
            private float shakeAmount;
            private float cachedAmplitude = 0;
            private float cachedFrequency = 0;
            private NoiseSettings cachedNoiseSettings;

            [HideInInspector]
            public List<VirtualCameraAngle> namedVirtualCameras;

            private int currentCameraIndex = 0;
            private CinemachineBrain cinemachineBrain;

            public override void setupComponent()
            {
                this.instance = this.gameObject;
                cinemachineBrain = FindObjectOfType<CinemachineBrain>();

                if (this.cinemachineBrain != null && this.virtualCameras != null && this.virtualCameras.Count > 0)
                {
                    this.cinemachineBrain.enabled = true;
                    this.hasComponent = true;
                    if (this.shakeCamera != null)
                    {
                        this.shakePerlin = this.shakeCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                        this.hasShakeCamera = true;
                        this.addPerlinToAllCameras(); //if it doesn't exist. 
                        this.namedVirtualCameras = getNamedCameraAngles(this.virtualCameras);
                    }
                }
            }

            private List<VirtualCameraAngle> getNamedCameraAngles(List<CinemachineVirtualCamera> virtualCameras)
            {
                List<VirtualCameraAngle> cameraAngles = new List<VirtualCameraAngle>();
                int i = 0;
                foreach (CinemachineVirtualCamera camera in virtualCameras)
                {
                    VirtualCameraAngle cameraAngle = new VirtualCameraAngle();
                    cameraAngle.camera = camera;
                    cameraAngle.i = i;
                    cameraAngle.name = camera.gameObject.name;
                    i++;
                }
                return cameraAngles;
            }

            private CinemachineBasicMultiChannelPerlin createEmptyPerlin()
            {
                CinemachineBasicMultiChannelPerlin perlin = new CinemachineBasicMultiChannelPerlin();
                perlin.m_AmplitudeGain = 0.0f;
                perlin.m_FrequencyGain = 0.0f;
                return perlin;
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                this.o = o;
                Kill();
                this.triggerController = base.getTriggerController();
                ActiveTrigger a = this.triggers[0];

                hideAllCameras();
                if (virtualCameras.Count > 0)
                {
                    this.CutToCamera(0);

                }
                base.Intro(triggerObj, o);
            }


            private void CutToCamera(int index)
            {
                this.currentCamera = virtualCameras[index];
                this.currentCamera.Priority = 10;
                this.currentCameraPerlin = this.addPerlinToCamera(this.currentCamera);
            }

            private void ShakeCamera()
            {
                //animating a sudden shake, like an impact, not to be confused with slow hand-held motion. 
                if (this.currentCamera == null) return;
                if (this.shakeCamera == null)
                {
                    //no shake camera defined. 
                    //TODO - attempt to use current perlin.
                    Debug.Log("no shake camera to derive shake from.");
                    return;
                }

                this.shakePerlin = this.shakeCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            }

            public override void updateValue(ActiveTrigger at, Vector3 _value)
            {
                if (this.hasComponent && this.hasShakeCamera)
                {
                    switch (at.propertyName)
                    {
                        case "shake":
                        case "punch":
                            if (this.hasShakeCamera && this.currentCameraPerlin != null)
                            {
                                this.shakeAmount = _value.x;
                                this.lerpPerlinForAllCameras(_value);
                            }
                            else
                            {
                                Debug.Log("no current camera perlin");
                            }
                            break;
                        default:
                            break;
                    }

                }
            }

            private void lerpPerlinForAllCameras(Vector3 _value)
            {
                foreach (CinemachineVirtualCamera camera in this.virtualCameras)
                {
                    CinemachineBasicMultiChannelPerlin perlin = camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                    if (perlin != null)
                    {
                        //TODO - putting zero here is a placeholder.
                        //TODO - consider adding y. 
                        perlin.m_AmplitudeGain = Mathf.Lerp(this.cachedAmplitude, this.shakePerlin.m_AmplitudeGain, _value.x);
                        perlin.m_FrequencyGain = Mathf.Lerp(this.cachedFrequency, this.shakePerlin.m_FrequencyGain, _value.x);

                        if (_value.x == 0 && cachedNoiseSettings != null)
                        {
                            perlin.m_NoiseProfile = cachedNoiseSettings;
                        }
                        else
                        {
                            perlin.m_NoiseProfile = shakePerlin.m_NoiseProfile;
                        }
                    }
                }
            }


            public override Vector3 getCurrentValue(ActiveTrigger at)
            {
                Vector3 value = new Vector3();

                if (this.hasComponent)
                {
                    switch (at.propertyName)
                    {
                        case "shake":
                        case "punch":
                            value.x = this.shakeAmount;
                            break;
                        default:
                            break;
                    }
                    value.x = this.shakeAmount;
                }
                return value;
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                // todo - random camera, index cameras, camera by name. 
                // knob shake.  
                if (this.hasComponent)
                {
                    switch (a.propertyName)
                    {
                        case "prev":
                        case "prev_camera_angle":
                        case "prev_camera":
                        case "prevCamera":
                        case "prevCameraAngle":
                            getPreviousCamera(a);
                            break;
                        case "index":
                        case "camera_by_index":
                        case "i":
                        case "camera_by_i":
                            getCamera(a, a.propertyIndex);
                            break;
                        case "shake":
                        case "punch":
                            ShakeCamera();
                            break;
                        case "name":
                        case "camera_by_name":
                            getCameraByName(a, a.propertyName);
                            break;
                        case "next":
                        case "next_camera_angle":
                        case "next_camera":
                        case "nextCamera":
                        case "nextCameraAngle":
                        default:
                            if (!string.IsNullOrEmpty(a.propertyName))
                            {
                                getCameraByName(a, a.propertyName);
                            }
                            else
                            {
                                getNextCamera(a);
                            }
                            break;
                    }
                }

                base.Trigger(triggerObj, a, o);
            }

            private void hideAllCameras()
            {
                //deprecated version
                foreach (CinemachineVirtualCamera virtualCamera in this.virtualCameras)
                {
                    virtualCamera.Priority = 0;
                }

                //real one. 
                // foreach (VirtualCameraAngle cameraAngle in this.cameraAngles)
                // {
                //     cameraAngle.camera.Priority = 0;
                // }
            }

            private void getCameraByName(ActiveTrigger a, string name)
            {
                //not setup yet - lets populate from the camera name - those can be pretty descriptive. 
                foreach (VirtualCameraAngle c in this.namedVirtualCameras)
                {
                    if (c.name == name)
                    {
                        currentCameraIndex = c.i;
                        this.getCamera(a, c.i);
                        break;
                    }
                }
            }

            private void getPreviousCamera(ActiveTrigger a)
            {
                currentCameraIndex = currentCameraIndex - 1;
                currentCameraIndex = (int)Mathf.Repeat(currentCameraIndex, virtualCameras.Count);
                this.getCamera(a, currentCameraIndex);
            }

            private void getNextCamera(ActiveTrigger a)
            {
                currentCameraIndex = currentCameraIndex + 1;
                currentCameraIndex = (int)Mathf.Repeat(currentCameraIndex, virtualCameras.Count);
                this.getCamera(a, currentCameraIndex);
            }

            private void getCamera(ActiveTrigger a, int camIndex)
            {
                if (this.cinemachineBrain != null)
                {
                    var customBlend = new CinemachineBlendDefinition(CinemachineBlendDefinition.Style.EaseInOut, a.onTime);
                    cinemachineBrain.m_DefaultBlend = customBlend;
                }
                hideAllCameras();
                this.currentCamera = virtualCameras[camIndex];
                this.currentCamera.Priority = 10;
                this.currentCameraPerlin = this.addPerlinToCamera(this.currentCamera);
            }

            private void addPerlinToAllCameras()
            {
                foreach (CinemachineVirtualCamera camera in this.virtualCameras)
                {
                    CinemachineBasicMultiChannelPerlin perlin = addPerlinToCamera(camera);
                    this.cachedAmplitude = perlin.m_AmplitudeGain;
                    this.cachedFrequency = perlin.m_FrequencyGain;
                    this.cachedNoiseSettings = perlin.m_NoiseProfile;
                }
            }

            private CinemachineBasicMultiChannelPerlin addPerlinToCamera(CinemachineVirtualCamera camera)
            {
                CinemachineBasicMultiChannelPerlin perlin = camera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                if (perlin == null)
                {
                    perlin = camera.AddCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
                }
                if (perlin.m_NoiseProfile == null && this.shakeCamera != null)
                {
                    //if the shake camera exists, we don't need to add noise to every camera
                    perlin.m_NoiseProfile = this.shakeCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>().m_NoiseProfile;
                }
                return perlin;
            }

            void OnDestroy()
            {
                ResetMainCamera();
            }

            void ResetMainCamera()
            {
                Camera cam = Camera.main;
                if (cam != null)
                {
                    cam.transform.rotation = Quaternion.identity;
                }

                if (CameraController.instance != null && CameraController.instance.camObj != null)
                {
                    CameraController.instance.camObj.transform.rotation = Quaternion.identity;
                }
            }

        }
    }
}
