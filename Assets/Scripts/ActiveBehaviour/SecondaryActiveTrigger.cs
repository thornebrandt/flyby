using System;
using flyby.Core;
using flyby.Triggers;
using DG.Tweening;
using UnityEngine;

//testing to see if this prevents the recursion error. 
//to prevent objs from becoming too monolithic. 

namespace flyby
{
    namespace Active
    {
        [Serializable]
        public class SecondaryActiveTrigger
        {
            //should there be an endtime?

            //secondary active triggers have knobs and targets
            public string trigger; //event name
            public string id;
            public string preset;
            public Vec target; //triggered target position, rotation, etc.   
            public Vec knob; //meant to interpolate between two vectors with a knob.
            public Vec velocity;
            public Vec start; //usually not used. 
            public Vec value;
            public Vec end;
            public Col targetColor; //triggered target color
            public Col startColor; //usually not used. 
            public Col color; //for initial color ( secondary additive ) 
            public Col endColor; //usually not used. 
            public Col knobColor;
            public bool disabled; //toggle off for testing. 
            public float startTime;
            public float startTime2; //to add a range to the start time. 
            public float endTime; //triggered on exit. 
            public float endTime2; //adding range to endTime;
            public float startDelay;
            public float startDelay2; //ading range to start delay. 
            public float endDelay; //formerly fadeout delay
            public float endDelay2; //range for endDelay. 
            public float onTime; //on animation for trigger. 
            public float onTime2; //range for oneTime. 
            public float offTime; //off animation for trigger. 
            public float offTime2; //to add range to the offtime. 
            public float onDelay; //delay for trigger. 
            public float onDelay2; //range for delay. 
            public float offDelay; //sustain without sustain 
            public float offDelay2; //range for offDelay.
            public float multiplier; //multiplies all vectors.
            public float multiplier2;
            public int propertyIndex; //material or blendshape index.
            public string propertyName; //color name.  
            public string behaviour; //id of activeBehaviour. To avoid conflicts. 
            public string targetString; //goal of spritesheet for example. 
            public bool hideOnComplete; //usually hides if true and endTime > 0
            public bool hideOnTarget; //hides obj when trigger is finished, meant for swapping. 
            public Media targetMedia;
            public ActiveType activeType;
            public TriggerProperty triggerProperty;
            public string group;
            public int childIndex;
            public Ease startEase;
            public Ease endEase;
            public Ease onEase;
            public Ease offEase;
            public string activeBehaviour; //experimental.  - not implemented yet. 
            public Curve curve; //experimental - used for bone rotations. 

            public SecondaryActiveTrigger()
            {
                this.startDelay2 = -999;
                this.onTime = 0.18f;
                this.onTime2 = -999;
                this.onDelay2 = -999;
                this.offTime = 3f;
                this.offTime2 = -999;
                this.offDelay2 = -999;
                this.multiplier = 1;
                this.multiplier2 = -999;
                this.childIndex = -1;
                this.startTime2 = -999;
            }

            public SecondaryActiveTrigger(ActiveTrigger at)
            {
                //conversion for looping. 
                this.id = at.id;
                this.preset = at.preset;
                this.target = at.target;
                this.knob = at.knob;
                this.velocity = at.velocity;
                this.start = at.start;
                this.value = at.value;
                this.end = at.end;
                this.startColor = at.startColor;
                this.color = at.color;
                this.endColor = at.endColor;
                this.targetColor = at.targetColor;
                this.knobColor = at.knobColor;
                this.disabled = at.disabled;
                this.startTime = at.startTime;
                this.startTime2 = at.startTime2;
                this.endTime = at.endTime;
                this.endTime2 = at.endTime2;
                this.startDelay = at.startDelay;
                this.startDelay2 = at.startDelay2;
                this.onTime = at.onTime;
                this.onTime2 = at.onTime;
                this.onDelay = at.onDelay;
                this.onDelay2 = at.onDelay2;
                this.offTime = at.offTime;
                this.offTime2 = at.offTime2;
                this.offDelay = at.offDelay;
                this.offDelay2 = at.offDelay2;
                this.trigger = at.trigger;
                this.propertyIndex = at.propertyIndex;
                this.propertyName = at.propertyName;
                this.behaviour = at.behaviour;
                this.activeType = at.activeType;
                this.targetString = at.targetString;
                this.targetMedia = at.targetMedia;
                this.triggerProperty = at.triggerProperty;
                this.hideOnComplete = at.hideOnComplete;
                this.hideOnTarget = at.hideOnTarget;
                this.multiplier = at.multiplier;
                this.multiplier2 = at.multiplier2;
                this.group = at.group;
                this.childIndex = at.childIndex;
                this.startEase = at.startEase;
                this.endEase = at.endEase;
                this.onEase = at.onEase;
                this.offEase = at.offEase;
                this.activeBehaviour = at.activeBehaviour;
                this.curve = at.curve;
            }

            public float map(float value)
            {
                return value;
                //mapping a float value from triggerProperties. 
            }

            public override bool Equals(System.Object obj)
            {
                if (obj is null)
                {
                    return false;
                }

                ActiveTrigger a = obj as ActiveTrigger;
                return
                    this.knob == a.target &&
                    this.targetColor == a.targetColor &&
                    this.onTime == a.onTime &&
                    this.offTime == a.offTime &&
                    this.onDelay == a.onDelay &&
                    this.onDelay2 == a.onDelay2 &&
                    this.offDelay == a.offDelay &&
                    this.offDelay2 == a.offDelay2 &&
                    this.trigger == a.trigger;
            }

            public static bool operator ==(SecondaryActiveTrigger lhs, SecondaryActiveTrigger rhs)
            {
                if (lhs is null)
                {
                    if (rhs is null)
                    {
                        return true;
                    }
                    return false;
                }
                return lhs.Equals(rhs);
            }

            public static bool operator !=(SecondaryActiveTrigger lhs, SecondaryActiveTrigger rhs) => !(lhs == rhs);

            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
        }
    }
}