using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class AdditiveActiveBehaviour : ActiveBehaviour
        {
            //meant for custom activeBehaviours that have lots of properties which can be overlapped. 

            [HideInInspector]
            public bool hasComponent;  //agnostic null check for shared functions.  there should be a set in setupComponent. 

            [HideInInspector]
            public Dictionary<string, ActiveTrigger> propertyAts;


            public override void setupActiveTriggers(Obj o)
            {
                // useful for generic and dynamic but custom behaviours. 
                // to quickly make variations via json. 

                //experimental change from this.triggers.Count <= 0 to 1, 
                //but it fixed multiple child triggers and why wouldn't we want to always add children. 
                propertyAts = new Dictionary<string, ActiveTrigger>();
                if (this.triggers == null || this.triggers.Count <= 1)
                {
                    // this one will probably use a bunch of duplicates of this script. 
                    // probably need find a way to add an array of them with copies of the component. 
                    if (o.activeTrigger != null)
                    {
                        //this is a custom active trigger. 
                        this.assignSecondaryTriggers(o.activeTrigger);
                        this.instance = this.gameObject;
                    }
                }
                this.setupComponent();
            }

            public virtual void setupComponent()
            {
                //where the child classes should set hasComponent and specific references. 
            }

            public virtual void IntroSetup()
            {
                //where the child classes should set up their intro sequences. 
            }

            public override void Update()
            {
                this.updateComponent();
            }

            public virtual void updateComponent()
            {
                if (this.hasComponent && this.triggers != null)
                {
                    bool shouldUpdate = false;
                    bool shouldUpdateColor = false;
                    foreach (ActiveTrigger at in this.triggers)
                    {

                        if (!string.IsNullOrEmpty(at.propertyName) && propertyAts.ContainsKey(at.propertyName))
                        {
                            if (propertyAts[at.propertyName]._i != at._i && propertyAts[at.propertyName].isAnimating())
                            {
                                //sequence for property is already being animated by a trigger. 
                                //newer triggers will reset this priority. 
                                continue;
                            }
                        }

                        shouldUpdate = false;
                        shouldUpdateColor = false;

                        if (at.starting)
                        {
                            if (at.start != null)
                            {
                                at.currentValue = Vector3.Lerp(at.startValue, at.baseValue, at.lerpValue);
                            }
                            at.currentValue = Vector3.Lerp(at.startValue, at.baseValue, at.lerpValue); //does this need similar check. 
                            if (at.startColorValue != null)
                            {
                                at.currentColorValue = Color.Lerp(at.startColorValue, at.baseColorValue, at.lerpValue);
                                shouldUpdateColor = true;
                            }
                            shouldUpdate = true;
                        }

                        if (at.ending)
                        {
                            at.currentValue = Vector3.Lerp(at.baseValue, at.endValue, 1.0f - at.lerpValue);
                            if (at.endColorValue != null)
                            {
                                at.currentColorValue = Color.Lerp(at.baseColorValue, at.endColorValue, 1.0f - at.lerpValue);
                                shouldUpdateColor = true;
                            }
                            shouldUpdate = true;
                        }

                        if (at.target != null && at.animatingTrigger)
                        {
                            //currentValue and targetValue are adjusted during Trigger() function, based on relative,etc. 
                            at.animatedValue = Vector3.Lerp(at.currentValue, at.targetValue, at.triggerLerpValue);
                            shouldUpdate = true;
                        }
                        else
                        {
                            at.animatedValue = at.currentValue;
                        }

                        if (at.targetColor != null && at.animatingTrigger)
                        {

                            at.animatedColorValue = Color.Lerp(at.currentColorValue, at.targetColorValue, at.triggerLerpValue);
                            shouldUpdateColor = true;
                        }
                        else
                        {
                            at.animatedColorValue = at.currentColorValue;
                        }

                        if (at.animatingKnob)
                        {
                            shouldUpdate = true;
                            shouldUpdateColor = true;
                        }


                        if (shouldUpdate)
                        {
                            calculateValue(at);
                        }

                        if (isUsingColors(at) && shouldUpdateColor)
                        {
                            calculateColorValue(at);
                        }
                        at.animatingKnob = false; //experimental. 
                    }
                }
            }

            private bool isUsingColors(ActiveTrigger at)
            {
                return at.targetColor != null || at.startColor != null || at.endColor != null;
            }

            public virtual void calculateValue(ActiveTrigger at)
            {
                //this function calculates how the envelope value plays with the knob values.
                //there are two presets, addValues and multiplyValues.

                //note compilation of knobValues happens on knobTrigger. 

                addValues(at);
                //multiplyValues(at); // turn on in override for calculateValues for scale.  
                // TODO - make this active trigger based?
            }

            public virtual void calculateColorValue(ActiveTrigger at)
            {
                //see above for what calculate value does. 
                addColorValues(at);
            }

            public void multiplyValues(ActiveTrigger at)
            {
                Vector3 knobValue = Vector3.one;

                //TODO - also check for knobColor. 
                if (at.knob != null)
                {
                    float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * multiplier) : at.knobValue * multiplier;
                    knobValue = at.knob.lerpVector3(mappedValue);
                }

                Vector3 value = at.animatedValue;
                value.Scale(knobValue);
                updateValue(at, value);
            }

            public void addValues(ActiveTrigger at)
            {
                Vector3 knobValue = Vector3.zero;
                //TODO - also check for knobColor
                if (at.knob != null)
                {
                    float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * multiplier) : at.knobValue * multiplier;
                    knobValue = at.knob.lerpVector3(mappedValue);
                }
                Vector3 value = Vec.Add(at.animatedValue, knobValue);
                updateValue(at, value);
            }

            public void addColorValues(ActiveTrigger at)
            {
                Color knobColor = Color.black;
                if (at.knobColor != null)
                {
                    knobColor = at.knobColor.getColor();
                }
                Color color = Col.AddColors(at.animatedColorValue, knobColor);
                updateColorValue(at, color);
            }

            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                if (Application.isPlaying)
                {
                    if (this.hasComponent = true && this.triggers != null && this.triggers.Count > 0)
                    {
                        this.Kill();
                        this.IntroSetup();
                        triggerController = base.getTriggerController();
                        this.checkForCachedKnobs(this.triggers[0]);
                        foreach (ActiveTrigger at in this.triggers)
                        {
                            at.lerpValue = 0;
                            at.startValue = Vector3.zero;
                            at.startColorValue = Color.white;
                            at.baseColorValue = Color.white;

                            //end     
                            if (at.end != null)
                            {
                                at.endValue = at.end.getVector3();
                            }
                            if (at.endColor != null)
                            {
                                at.endColorValue = at.endColor.getColor();
                            }

                            //value.
                            if (at.value != null)
                            {
                                //experimental. 
                                at.baseValue = Vector3.Scale(at.value.getVector3(), Vec.lerpVector3(at.velocity, triggerObj.velocity)); //when was velocity added?
                                at.startValue = at.baseValue;
                            }
                            if (at.color != null)
                            {
                                at.baseColorValue = at.color.getColor();
                                at.startColorValue = at.baseColorValue;
                            }


                            if (at.start != null)
                            {
                                at.startValue = at.start.getVector3();
                            }
                            if (at.startColor != null)
                            {
                                at.startColorValue = at.startColor.getColor();
                            }

                            if (at.startTime <= 0)
                            {
                                at.startValue = at.baseValue;
                                at.startColorValue = at.baseColorValue; //might need a nullcheck here.
                            }

                            at.currentValue = at.startValue;
                            at.currentColorValue = at.startColorValue; //might need nuckck

                            //APPLY START STATE. 
                            updateValue(at, at.currentValue);
                            if (isUsingColors(at))
                            {
                                updateColorValue(at, at.currentColorValue);
                            }

                            bool needsSequence = at.startTime > 0 || at.endTime > 0;
                            if (needsSequence)
                            {
                                if (at.start == at.value && at.startColorValue == at.baseColorValue)
                                {
                                    at.startTime = 0;
                                }

                                if (at.sequence != null)
                                {
                                    at.sequence.Kill();
                                }
                                at.sequence = DOTween.Sequence();
                                AdditiveIntroSequence(at);
                                if (!triggerObj.sustain && at.endTime > 0)
                                {
                                    AdditiveEndSequence(at);
                                }
                            }
                        }
                    }
                }
                base.Intro(triggerObj, o);
            }

            public virtual void updateValue(ActiveTrigger at, Vector3 _value)
            {
                //common function for intro, trigger, and outro, and knobs working together. 
                //at.lerpValue stores lerp-value. _value holds the lerped value.
            }

            public virtual void updateColorValue(ActiveTrigger at, Color _color)
            {
                //common function for intro, trigger, and outro, and knobs.
                //for - 
                //at.lerpValue stores lerp-value. at.color holds the lerped value. 
            }

            public Vector3 getTargetValue(ActiveTrigger at, Vector3 _targetValue)
            {
                //prevents values from going above max. 

                if (at.triggerProperty != null)
                {
                    if (at.triggerProperty.clampMax != -999 || at.triggerProperty.clampMin != -999)
                    {
                        return Math.Clamp(_targetValue, at.triggerProperty.clampMin, at.triggerProperty.clampMax);
                    }
                }
                return _targetValue;
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger at, Obj o)
            {
                if (Application.isPlaying && at != null && this.hasComponent && (at.target != null || at.targetColor != null))
                {

                    if (!string.IsNullOrEmpty(at.propertyName))
                    {
                        if (propertyAts.ContainsKey(at.propertyName) && propertyAts[at.propertyName] != at)
                        {
                            if (propertyAts[at.propertyName].triggerSequence != null && propertyAts[at.propertyName].triggerSequence.active)
                            {
                                if (propertyAts[at.propertyName]._i != at._i)
                                {
                                    //another older trigger sequence  is using this property.
                                    //clean up previous trigger. 
                                    propertyAts[at.propertyName].triggerLerpValue = 0;
                                    //it's fine to kill a sequence twice)
                                    propertyAts[at.propertyName].triggerSequence.Kill();
                                    propertyAts[at.propertyName].animatingTriggerOff = false;
                                    propertyAts[at.propertyName].animatingTrigger = false;
                                    at.animatingTriggerOff = false;

                                    if (at.target != null)
                                    {
                                        at.currentValue = getCurrentValue(at);
                                    }

                                    if (at.targetColor != null)
                                    {
                                        at.currentColorValue = getCurrentColor(at);
                                    }

                                    at.triggerLerpValue = 0;
                                }
                            }

                        }
                        propertyAts[at.propertyName] = at;
                    }

                    Vector3 newTargetValue = Vector3.zero;

                    if (at.target != null)
                    {
                        newTargetValue = at.target.getVector3();
                        if (lastTrigger != null && lastTrigger.offTime < 0)
                        {
                            at.currentValue = getCurrentValue(at);
                            at.triggerLerpValue = 0;
                        }

                        if (at.value != null && at.value.relative)
                        {
                            //experimental.  not sure this is the strategy. 
                            at.currentValue = getCurrentValue(at);
                            at.baseValue = at.currentValue;
                            at.triggerLerpValue = 0;
                        }


                        if (at.target.relative)
                        {
                            at.triggerLerpValue = 0;
                            at.currentValue = getCurrentValue(at);
                            if ((at.value != null && at.value.relative) || (lastTrigger != null && lastTrigger.offTime < 0))
                            {
                                at.baseValue = at.currentValue;
                            }
                            else
                            {
                                at.baseValue = at.startValue; //experimental - we need to go back sometimes.
                            }
                            Vector3 addedTargetValue = at.currentValue + newTargetValue; // this needs to follow same rule of additive or multiplitive. 
                            Vector3 clampedTargetValue = this.getTargetValue(at, addedTargetValue); //experimental - clamping values. 
                            newTargetValue = clampedTargetValue;
                        }

                        at.targetValue = newTargetValue; //usually just one.
                    }

                    if (at.targetColor != null)
                    {
                        Color newTargetColor = at.targetColor.getColor();
                        if (lastColorTrigger != null && lastColorTrigger.offTime < 0)
                        {
                            at.currentColorValue = getCurrentColor(at);
                            at.triggerLerpValue = 0;
                        }

                        at.targetColorValue = at.targetColor.getColor();
                        at.targetValue = newTargetValue;
                    }

                    if (at.triggerSequence != null)
                    {
                        at.triggerSequence.Kill();
                    }

                    at.triggerSequence = DOTween.Sequence(); //eventually move this inside of additiveTriggerSequence. 
                    AdditiveTriggerSequence(at, triggerObj.sustain);

                    if (at.target != null)
                    {
                        at.lastTargetValue = newTargetValue;
                    }

                    base.Trigger(triggerObj, at, o);
                    if (!triggerObj.sustain)
                    {
                        AdditiveTriggerOffSequence(at);
                    }
                }
            }


            public override void OnTriggerComplete(ActiveTrigger at)
            {
                if (at.offTime > -1)
                {
                    //perhaps this should be some kind of 
                    at.currentValue = at.baseValue;
                    if (isUsingColors(at))
                    {
                        at.currentColorValue = at.baseColorValue;
                    }
                }
                base.OnTriggerComplete(at);
            }


            public override void Release()
            {
                //TODO: can we get the actual activeTrigger that is being released?
                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.value != null && at.sequence != null && at.end != null)
                    {
                        at.sequence.Kill();
                        this.AdditiveEndSequence(at);
                    }
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {

                if (a.triggerSequence != null)
                {
                    a.triggerSequence.Kill();
                }

                if (a.offTime > -1)
                {
                    a.triggerSequence = DOTween.Sequence();
                    AdditiveTriggerOffSequence(a);
                }
                else
                {
                    a.currentValue = getCurrentValue(a);
                    a.currentColorValue = getCurrentColor(a);
                    a.triggerLerpValue = 0;
                }
            }

            public override void TriggerKnob(TriggerObj knobEvent, ActiveTrigger _a, Obj o)
            {
                animatingKnob = true; //is this needed for base behaviour?
                cacheKnob(knobEvent);
                foreach (ActiveTrigger at in this.triggers)
                {
                    //TODO - add propertyAts reference here. 
                    at.animatingKnob = false;
                    if (at.knob != null && this.isCorrectBehaviour(at) && at.trigger == knobEvent.eventName)
                    {
                        float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(at.knobValue * multiplier) : at.knobValue * multiplier;
                        at.knobValue = mappedValue;
                        at.animatingKnob = true;
                    }

                    //I don't think we need color here because we convert on the update side.   
                }
            }


            public virtual Vector3 getCurrentValue(ActiveTrigger at)
            {
                return at.currentValue;
            }

            public virtual Color getCurrentColor(ActiveTrigger at)
            {
                return at.currentColorValue;
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                this.triggerController = getTriggerController();

                if (this.triggers == null)
                {
                    assignSecondaryTriggers(parentA);
                }

                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.knob != null || at.knobColor != null)
                    {
                        TriggerObj t = checkForCachedKnob(at);
                        if (t != null && t.knob > -1)
                        {
                            float cachedValue = t != null ? t.value : 0;
                            //need to figure out if we want to normalize in the data. it would make this algorithm easier. 
                            float mappedValue = at.triggerProperty != null ? at.triggerProperty.map(cachedValue * multiplier) : cachedValue * multiplier;
                            at.knobValue = t != null ? cachedValue : at.knobValue;
                            //at.animatingKnob was turned off, possibly for tests. This needs to be on 
                            //for ActiveType.Next - Shader. 
                            //it was commented out for the commit "possibly fixed child additive behaviours"

                            //re commenting out at.animatingKnob to fix pixelation between sockets and discord. 

                            //possibly put a addKnobs and addColorKnobs here.  

                            at.animatingKnob = true;
                        }
                    }
                }
            }
        }

    }
}