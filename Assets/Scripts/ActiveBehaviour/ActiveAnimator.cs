using DG.Tweening;
using UnityEngine;
using flyby.Core;
using flyby.Triggers;
using flyby.Controllers;
using System.Collections;
using System.Collections.Generic;

namespace flyby
{
    namespace Active
    {
        public class ActiveAnimator : ActiveBehaviour
        {
            // before LFOS are set up, this animator has access to two knobs. 
            // knobTrigger and envelopeTrigger.
            // steps for getting knobs to work in animator: 
            // 1. name them in the prefab. ( something like "knob_that_animates" )
            // 2. make a trigger that references that same name. 


            private int introLayer = 0; //intro
            private int targetLayer = 1; //secondary trigger on active objs. 
            private int loopLayer = 2; //base loop animation. 
            private int knobLayer = 3; //knob controls position of animation. 
            private int envelopeLayer = 4; //eventually will be LFO, right now it's second knob 
            private int exitLayer = 5; //if exit time. 
            private int sustainLayer = 6; //sustain for trigger/target Layer ( such as throbbing while expanded ) 

            public RuntimeAnimatorController animatorController; //use 
            public string knobTrigger;
            public string envelopeTrigger;
            private ActiveTrigger _knobTrigger;
            private ActiveTrigger _envelopeTrigger;

            public float globalSpeed = 1.0f;
            public float loopSpeed = 1.0f; // y of value of activeTrigger ( loopStart, loopSpeed ) 
            public float loopStart; // x of value of activeTrigger
            public AnimationClip idleAnimation;
            public AnimationClip introAnimation; //intro stays on, so put base properties here. 
            public AnimationClip targetAnimation;
            public AnimationClip loopAnimation;
            public AnimationClip envelopeAnimation;
            public AnimationClip knobAnimation;
            public AnimationClip exitAnimation;
            public AnimationClip sustainAnimation;

            private FlyByController flyByController;

            private Animator animator;
            private bool hasSustain;

            public void Awake()
            {
                if (this.enabled)
                {
                    this.instance = this.gameObject;
                    setupAnimator();
                }
            }

            public override void Update()
            {
                if (starting)
                {
                    if (loopAnimation != null)
                    {
                        animator.SetLayerWeight(loopLayer, lerpValue);
                    }
                }

                if (ending)
                {
                    if (exitAnimation != null && animator != null)
                    {
                        //lerpValue is negative on exit! 
                        animator.SetLayerWeight(exitLayer, 1.0f - lerpValue);
                        animator.SetLayerWeight(loopLayer, lerpValue); //experimental.
                        animator.SetLayerWeight(introLayer, lerpValue); //experimental.
                    }
                }

                if (animatingTrigger && animator != null)
                {
                    if (targetAnimation != null)
                    {
                        //target animation starts, stops at end and returns 
                        animator.SetLayerWeight(targetLayer, triggerLerpValue);
                        if (hasSustain)
                        {
                            //sustain fades in and loops until it receives a noteOff.
                            animator.SetLayerWeight(sustainLayer, triggerLerpValue);
                        }
                    }
                }

                //debug
                //getStatus();
            }

            private float getSpeedFromTime(float time, float clipLength)
            {
                //getting clip speed from animation time. 
                float speed = 1.0f;
                if (time <= 0)
                {
                    speed = 100; //placeholder. what is correct? Infinity?
                }
                else
                {
                    speed = clipLength / time;
                }
                return speed;
            }

            public override void setupActiveTriggers(Obj o)
            {
                //TODO - manage this with primary/secondary.

                //manually setting up triggers for knob caching.
                //eventually replace strings with activeTrigger.
                //no use automatically making this since we need to assign animations manually. 

                //TODO - forenvelope/  animation speed use y value.  


                if (o.activeTrigger == null)
                {
                    Debug.Log("no active trigger on animator: " + o.id);
                    o.activeTrigger = new ActiveTrigger();
                }

                if (this.triggers == null)
                {
                    this.triggers = new List<ActiveTrigger>();
                }
                assignSecondaryTriggers(o.activeTrigger);

                foreach (ActiveTrigger at in this.triggers)
                {
                    if (at.trigger == this.knobTrigger || at.propertyName == "knobTrigger")
                    {
                        _knobTrigger = at;
                        //in case it wasn't set in the object. 
                        this.knobTrigger = _knobTrigger.trigger;

                    }
                    if (at.trigger == this.envelopeTrigger || at.propertyName == "envelopeTrigger")
                    {
                        _envelopeTrigger = at;
                        //in case it wasn't set in the object. 
                        this.envelopeTrigger = _envelopeTrigger.trigger;
                    }
                }

                if (_knobTrigger == null)
                {
                    this.triggers.Add(new ActiveTrigger
                    {
                        knob = new Vec(),
                        trigger = this.knobTrigger
                    });
                }

                if (_envelopeTrigger == null)
                {
                    this.triggers.Add(new ActiveTrigger
                    {
                        knob = new Vec(),
                        trigger = this.envelopeTrigger
                    });
                }

                // if (o != null && o.activeTrigger != null) {
                //     this.triggers[0] = o.activeTrigger;
                // }
            }

            public override void checkForCachedKnobs(ActiveTrigger parentA)
            {
                if (knobAnimation != null || envelopeAnimation != null)
                {
                    this.triggerController = getTriggerController();
                    if (this.triggers == null)
                    {
                        assignSecondaryTriggers(parentA);
                    }

                    foreach (ActiveTrigger at in this.triggers)
                    {
                        if (at.knob != null)
                        {
                            if (at.trigger == knobTrigger)
                            {
                                TriggerObj t = checkForCachedKnob(at);
                                at.knobValue = t != null ? t.value : at.knobValue;
                                float mappedValue = Tools.Math.Lerp(at.knob.x, at.knob.x2, at.knobValue);
                                mappedValue = at.triggerProperty != null ? at.triggerProperty.map(mappedValue) : mappedValue;
                                animator.Play("Knob", knobLayer, mappedValue);
                            }
                            if (at.trigger == envelopeTrigger)
                            {
                                TriggerObj t = checkForCachedKnob(at);
                                at.knobValue = t != null ? t.value : at.knobValue;
                                float mappedValue = Tools.Math.Lerp(at.knob.x, at.knob.x2, at.knobValue);
                                mappedValue = at.triggerProperty != null ? at.triggerProperty.map(mappedValue) : mappedValue;
                                animator.SetLayerWeight(envelopeLayer, mappedValue);

                            }
                        }
                    }
                }
            }

            private IEnumerator startAnimationAfterDelay(TriggerObj triggerObj, ActiveTrigger a)
            {
                animator.SetFloat("IntroSpeed", 0); //pausing animation at zero. 
                animator.Play("Intro", introLayer, 0f);
                yield return new WaitForSeconds(a.startDelay);
                startAnimation(triggerObj, a);
            }

            private void startAnimation(TriggerObj triggerObj, ActiveTrigger a)
            {
                if (a.startTime > 0)
                {
                    float introSpeed = getSpeedFromTime(a.startTime, introAnimation.length);
                    animator.SetFloat("IntroSpeed", introSpeed);
                    animator.Play("Intro", introLayer, 0f);
                }
                else
                {
                    //start at the end of the intro
                    animator.SetFloat("IntroSpeed", 999);
                    animator.Play("Intro", introLayer, 1);
                }

                if (_sequence != null)
                {
                    _sequence.Kill(true);
                }
                _sequence = DOTween.Sequence();
                base.IntroSequence(_sequence, a);

                if (!triggerObj.sustain && a.endTime > 0)
                {
                    base.EndSequence(_sequence, a);
                }
            }


            public override void Intro(TriggerObj triggerObj, Obj o)
            {
                if (animator != null)
                {
                    resetAnimations();
                    if (this.triggers != null && this.triggers.Count > 0)
                    {
                        ActiveTrigger a = this.triggers[0];
                        this.checkForCachedKnobs(a);
                        a.start = new Vec(-1); //hack - needs to use start time. 

                        if (a.value != null)
                        {
                            Vector3 value = a.value.getVector3();
                            this.loopStart = value.x;
                            this.loopSpeed = value.y; // need to figure out a default. 
                        }

                        if (introAnimation != null)
                        {
                            if (a.startDelay > 0)
                            {
                                StartCoroutine(startAnimationAfterDelay(triggerObj, a));
                            }
                            else
                            {
                                startAnimation(triggerObj, a);
                            }
                        }

                        //we're proceededing now with starting loop with intro. 
                        if (loopAnimation != null)
                        {
                            animator.SetFloat("LoopSpeed", this.loopSpeed);
                            animator.Play("Loop", loopLayer, this.loopStart);
                        }

                        if (envelopeAnimation != null)
                        {
                            float envelopeSpeed = 1;
                            if (this._envelopeTrigger != null && this._envelopeTrigger.value != null && this._envelopeTrigger.value.x != 0)
                            {
                                envelopeSpeed = this._envelopeTrigger.value.x;
                            }
                            animator.SetFloat("EnvelopeSpeed", envelopeSpeed);
                            animator.Play("Envelope", envelopeLayer, 0f);
                        }
                    }
                }
                else
                {
                    Debug.Log("no animator!");
                }
            }

            public override void Trigger(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                if (targetAnimation != null && animator != null && instance.activeInHierarchy)
                {
                    startTargetAnimation(a);
                    this.hasSustain = checkForSustain(triggerObj);
                    if (_triggerSequence != null)
                    {
                        _triggerSequence.Kill();
                    }
                    if (a.offTime <= 0)
                    {
                        //why do we ever want it not to animate again??
                        triggerLerpValue = Mathf.Max(0.0f, triggerLerpValue); //don't understand this code. 
                        //meant for a loop.  
                        //triggerLerpValue = 0;
                    }
                    _triggerSequence = DOTween.Sequence();
                    TriggerSequence(_triggerSequence, a);
                    base.Trigger(triggerObj, a, o);
                    if (!triggerObj.sustain && a.offTime >= 0)
                    {
                        TriggerOffSequence(_triggerSequence, a);
                    }
                }
            }

            private void startTargetAnimation(ActiveTrigger a)
            {
                //float targetSpeed = 1.0f; 
                //possibly needs to be different from time. 

                float targetSpeed = getSpeedFromTime(a.onTime, targetAnimation.length);
                animator.SetFloat("TargetSpeed", targetSpeed);
                float targetPosition = 0;
                if (this.on)
                {
                    targetPosition = getTargetAnimationPosition();

                    // commented out an ability to loop to make all tests pass. 
                    // I think we should make a more specific loop command. 

                    // if (a.offTime < 0)
                    // {
                    //     // set offTime to -1 for manual loops. ( default offTime makes pingPong with target ) 
                    //     targetPosition = 0;
                    // }
                }

                if (targetPosition < 0.99f)
                {

                    animator.Play("Target", targetLayer, targetPosition);
                }
            }

            public override void OnTriggerComplete(ActiveTrigger a)
            {
                if (hasSustain)
                {
                    startSustainAnimation();
                }
            }

            public override void OnTriggerOff(ActiveTrigger a)
            {
                if (animator != null && instance.activeInHierarchy)
                {
                    float targetSpeed = getSpeedFromTime(a.offTime, targetAnimation.length);
                    float targetPosition = getTargetAnimationPosition();
                    animator.SetFloat("TargetSpeed", -targetSpeed);
                    animator.Play("Target", targetLayer, targetPosition);
                }
            }

            public override void Release()
            {
                //overriding release, (mostly for tests.) not checking for end.
                if (exitAnimation != null)
                {
                    _sequence.Kill();
                    _sequence = DOTween.Sequence();
                    EndSequence(_sequence, this.triggers[0]);
                }
            }

            public override void OnEnd(ActiveTrigger a)
            {
                if (exitAnimation != null)
                {
                    startEndAnimation(a);
                }
            }

            private void startEndAnimation(ActiveTrigger a)
            {
                float exitSpeed = getSpeedFromTime(a.endTime, exitAnimation.length);
                if (animator != null && instance.activeInHierarchy)
                {
                    animator.SetFloat("ExitSpeed", exitSpeed);
                    animator.Play("Exit", exitLayer, 0.0f);
                }
            }

            public override void TriggerOff(TriggerObj triggerObj, ActiveTrigger a, Obj o)
            {
                // handling sustain release.
                // TODO  I believe we can dry up triggerOFf now that we have onTriggerOff. 

                if (_triggerSequence != null)
                {
                    _triggerSequence.Kill();
                }
                _triggerSequence = DOTween.Sequence();
                TriggerOffSequence(_triggerSequence, a);
            }

            private bool checkForSustain(TriggerObj triggerObj)
            {
                return sustainAnimation != null & triggerObj.sustain;
            }

            private void startSustainAnimation()
            {
                float sustainSpeed = 1.0f; //need to set sustain speed somehow. 
                animator.SetFloat("SustainSpeed", sustainSpeed);
                animator.Play("Sustain", sustainLayer, 0f);
            }

            public override void TriggerEventHandler(TriggerObj triggerObj, Obj o)
            {
                base.TriggerEventHandler(triggerObj, o);
                switch (triggerObj.source)
                {
                    case "midiKnob":
                        knobHandler(triggerObj);
                        break;
                    default:
                        break;
                }
            }

            //deprecated - see position. 

            private void knobHandler(TriggerObj triggerObj)
            {
                //handles both knob layer and envelope ( fade in knob layer. )

                if (triggerObj.eventName == this.knobTrigger)
                {
                    //animator.SetLayerWeight(knobLayer, 1);
                    //if you need an additive blend mode - it needs to be a new layer. 
                    this.cacheKnob(triggerObj);
                    if (this.gameObject.activeInHierarchy)
                    {
                        float mappedValue = Tools.Math.Lerp(this._knobTrigger.knob.x, this._knobTrigger.knob.x2, triggerObj.value);
                        mappedValue = this._knobTrigger.triggerProperty != null ? this._knobTrigger.triggerProperty.map(mappedValue) : mappedValue;
                        animator.Play("Knob", knobLayer, mappedValue);
                    } //if not active, we need to cache. 


                }
                if (triggerObj.eventName == this.envelopeTrigger)
                {
                    this.cacheKnob(triggerObj);
                    if (this.gameObject.activeInHierarchy)
                    {
                        float mappedValue = Tools.Math.Lerp(this._envelopeTrigger.knob.x, this._envelopeTrigger.knob.x2, this._envelopeTrigger.knobValue);
                        mappedValue = this._envelopeTrigger.triggerProperty != null ? this._envelopeTrigger.triggerProperty.map(mappedValue) : mappedValue;
                        animator.SetLayerWeight(envelopeLayer, mappedValue);
                    } //if not active, we need to cache this
                }
            }

            private void setupAnimator()
            {
                animator = gameObject.GetComponent<UnityEngine.Animator>();
                if (animator == null)
                {
                    animator = gameObject.AddComponent<UnityEngine.Animator>();
                }
                if (animatorController == null)
                {
                    this.flyByController = getFlyByController();
                    this.animatorController = this.flyByController.animatorController;
                }
                if (this.animatorController == null)
                {
                    Debug.Log("No runtime animator controller assigned.");
                }
                animator.runtimeAnimatorController = animatorController;
                AnimatorOverrideController aoc = new AnimatorOverrideController(animator.runtimeAnimatorController);
                animator.runtimeAnimatorController = aoc;
                aoc["INTRO_CLIP"] = introAnimation != null ? introAnimation : idleAnimation;
                aoc["TARGET_CLIP"] = targetAnimation; //target
                aoc["LOOP_CLIP"] = loopAnimation != null ? loopAnimation : idleAnimation; //base static animation. 
                aoc["ENVELOPE_CLIP"] = envelopeAnimation;
                aoc["KNOB_CLIP"] = knobAnimation;
                aoc["EXIT_CLIP"] = exitAnimation;
                aoc["SUSTAIN_CLIP"] = sustainAnimation;
                //animator.SetFloat("EnvelopeSpeed", 0); //eventually coming from a sine wave. 
                animator.SetFloat("KnobSpeed", 0); //we do not use the speed for this. 
                                                   // debug 
                                                   // animator.cullingMode = AnimatorCullingMode.CullCompletely;
            }

            private void resetAnimations()
            {
                animator.SetFloat("EnvelopeSpeed", 0);
                animator.SetFloat("KnobSpeed", 0);
                animator.SetFloat("IntroSpeed", 0);
                animator.SetFloat("ExitSpeed", 0);
                animator.Play("Idle", introLayer, 0f);
                animator.Play("Idle", targetLayer, 0f);
                animator.Play("Idle", loopLayer, 0f);
                animator.Play("Idle", envelopeLayer, 0f);
                animator.Play("Idle", knobLayer, 0f);
                animator.Play("Idle", exitLayer, 0f);
                animator.Play("Idle", sustainLayer, 0f);
                animator.SetLayerWeight(envelopeLayer, 0f);
                animator.SetLayerWeight(knobLayer, 1.0f);
                animator.SetLayerWeight(targetLayer, 0f);
                animator.SetLayerWeight(loopLayer, 0f);
                animator.SetLayerWeight(exitLayer, 0f);
                animator.SetLayerWeight(sustainLayer, 0f);
                animator.speed = globalSpeed;
                instance.transform.localPosition = Vector3.zero;
                this.fresh = true;
            }

            public override void Hide()
            {
                // fresh means it has been reintroduced, which 
                // might have triggered the killing. 
                if (!this.fresh)
                {
                    if (this.animator != null)
                    {
                        animator.SetLayerWeight(knobLayer, 0.0f);
                    }
                    //no reason for this be here, right?
                    //resetAnimations();
                    base.Hide();
                    base.Kill();
                }
            }

            private FlyByController getFlyByController()
            {
                //TODO - move this to base class. 

                if (this.flyByController != null)
                {
                    return flyByController;
                }
                if (FlyByController.instance != null)
                {
                    return FlyByController.instance;
                }
                return null;
            }

            public void getStatus()
            {
                if (animator != null)
                {
                    // intro. 
                    // float introPosition = getIntroAnimationPosition();
                    // Debug.Log(lerpValue + ":" + introduced + " , " + transform.localScale);
                    // Debug.Log("intro: " + animator.GetLayerWeight(introLayer) + " : " + introPosition);

                    //target
                    // float targetPosition = getTargetAnimationPosition();
                    // float targetLayerWeight = animator.GetLayerWeight(targetLayer);
                    // float targetSpeed = animator.GetFloat("TargetSpeed");
                    // Debug.Log("target: " + animatingTrigger + ":" + triggerLerpValue + ", position: " + targetPosition +
                    //  ", weight: " + targetLayerWeight + ", speed: " + targetSpeed + ", transform: " + gameObject.transform.localPosition);

                    //loop
                    // float loopPosition = getLoopPosition();
                    // float loopLayerWeight = animator.GetLayerWeight(loopLayer);
                    // float loopSpeed = animator.GetFloat("LoopSpeed");
                    // Debug.Log("loop: position: " + loopPosition + " , weight: " + loopLayerWeight + " , speed: " + loopSpeed);

                    //exit
                    // float exitPosition = getExitAnimationPosition();
                    // float exitLayerWeight = animator.GetLayerWeight(exitLayer);
                    // float exitSpeed = animator.GetFloat("ExitSpeed");
                    // Debug.Log("anim:" + animating + ":" + lerpValue + ", " + "pos: " + exitPosition +
                    //     ", weight: " + exitLayerWeight + " , speed: " + exitSpeed + ", trasform: " + gameObject.transform.localScale);



                }
                else
                {
                    Debug.Log("animator is null.");
                }
            }

            float getIntroAnimationPosition()
            {
                AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(introLayer);
                if (stateInfo.IsName("Intro Layer.Intro"))
                {
                    return stateInfo.normalizedTime;
                }
                return 0;
            }

            float getLoopPosition()
            {
                AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(loopLayer);
                if (stateInfo.IsName("Loop Layer.Loop"))
                {
                    return stateInfo.normalizedTime;
                }
                return 0;
            }

            float getEnvelopeAnimationPosition()
            {
                AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(envelopeLayer);
                if (stateInfo.IsName("Envelope Layer.Envelope"))
                {
                    return stateInfo.normalizedTime;
                }
                return 0;
            }

            float getTargetAnimationPosition()
            {
                AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(targetLayer);
                if (stateInfo.IsName("Target Layer.Target"))
                {
                    //lord knows why unity keeps increasing normalized time for unlooped clip. 
                    return Mathf.Clamp(stateInfo.normalizedTime, 0.0f, 1.0f);
                }
                return 0;
            }

            float getExitAnimationPosition()
            {
                AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(exitLayer);
                if (stateInfo.IsName("Exit Layer.Exit"))
                {
                    //lord knows why unity keeps increasing normalized time for unlooped clip. 
                    return Mathf.Clamp(stateInfo.normalizedTime, 0.0f, 1.0f);
                }
                return 0;
            }
        }
    }
}