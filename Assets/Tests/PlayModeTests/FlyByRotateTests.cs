using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Active;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyByRotateTests : PlayModeTests
    {
        ActiveRotate activeRotate;  //first activeRotation.

        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            this.teardown();
        }

        [UnityTest]
        public IEnumerator Can_Disable_A_Rotate_Trigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotate = new ActiveTrigger
            {
                disabled = true,
                start = new Vec
                {
                    x = 1,
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            yield return 0;
            //does not rotate because it was never assigned because it is disabled.
            Assert.AreEqual(instance.transform.rotation.eulerAngles.x, 0);
            Assert.IsNull(activeRotate);
        }

        [UnityTest]
        public IEnumerator Intro_Works_For_Rotate()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotate = new ActiveTrigger
            {
                start = new Vec
                {
                    x = 1,
                },
                value = new Vec { },
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            //wait two frames. 
            yield return 0;
            yield return 0;
            Assert.Greater(instance.transform.rotation.eulerAngles.x, 0);
            yield return new WaitForSecondsOrTrue(1, () => activeRotate.introduced);
            Quaternion currentRotation = instance.transform.rotation;
            yield return new WaitForSeconds(0.2f);
            //check that rotation does not change after intro. 
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, currentRotation));
        }

        [UnityTest]
        public IEnumerator Value_Works_For_Rotate()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotate = new ActiveTrigger
            {
                value = new Vec
                {
                    x = 1,
                }
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSeconds(0.2f);
            Assert.Greater(instance.transform.rotation.eulerAngles.x, 0);
        }

        [UnityTest]
        public IEnumerator ActiveTrigger_Does_Not_Trigger_Rotate()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotate = new ActiveTrigger();
            obj1.activeTrigger = new ActiveTrigger
            {
                value = new Vec
                {
                    x = 1,
                }
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSeconds(0.2f);
            Assert.AreEqual(instance.transform.rotation.eulerAngles.x, 0);
        }

        [UnityTest]
        public IEnumerator Trigger_Sustain_Works_For_Rotate()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotate = new ActiveTrigger
            {
                trigger = "test_sustain_trigger",
                target = new Vec
                {
                    x = 1
                }
            };
            Trigger sustain_trigger = new Trigger
            {
                eventName = "test_sustain_trigger",
                sustain = true,
                note = 1,
                channel = 0,
            };
            addTrigger(sustain_trigger);
            registerTriggers();
            initializeObjs();
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            triggerEvent("intro");
            // wait three frames. 
            yield return 0;
            yield return 0;
            yield return 0;
            Assert.AreEqual(instance.transform.rotation, Quaternion.identity);
            midiController.OnMidiNoteOn(0, 1, 1);
            yield return new WaitForSecondsOrTrue(1, () => activeRotate.on);
            Assert.Greater(instance.transform.rotation.eulerAngles.x, 0);
            Quaternion onRotation = instance.transform.rotation;
            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(activeRotate.rotate, new Vector3(1, 0, 0));
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Rotate()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotate = new ActiveTrigger
            {
                trigger = "test_rotate_trigger",
                target = new Vec
                {
                    x = 1,
                },
                offDelay = 0.01f,
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            yield return 0;
            yield return 0;
            Assert.AreEqual(instance.transform.rotation, Quaternion.identity);
            triggerEvent("test_rotate_trigger");
            yield return 0;
            yield return 0;
            yield return 0;
            Assert.Greater(instance.transform.rotation.eulerAngles.x, 0);
        }


        [UnityTest]
        public IEnumerator Trigger_Rotate_Keeps_Rotating_If_OffTime_Is_Negative_One()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotate = new ActiveTrigger
            {
                trigger = "test_continuous_rotation",
                target = new Vec
                {
                    x = 0.1f,
                },
                onTime = 0.1f,
                offTime = -1,
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("test_continuous_rotation");
            yield return 0;
            yield return 0;
            Assert.Greater(instance.transform.rotation.eulerAngles.x, 0);
            yield return new WaitForSecondsOrTrue(0.3f, () => activeRotate.on);
            yield return 0; // wait for one more frame before collecting, to be safe. ( remove eventually ) 
            float currentEulerAngles = instance.transform.rotation.eulerAngles.x;
            //wait a few few more frames. 
            yield return 0;
            yield return 0;
            yield return 0;
            Assert.Greater(instance.transform.rotation.eulerAngles.x, currentEulerAngles);
        }


        [UnityTest]
        public IEnumerator End_Time_Works_With_Sustain_For_Rotate()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro_sustain";
            obj1.triggerRotate = new ActiveTrigger
            {
                startTime = 0.1f,
                endTime = 0.1f,
                start = new Vec
                {
                    x = 1.0f,
                },
                value = new Vec
                {
                    x = 0,
                },
                end = new Vec
                {
                    x = -0.1f,
                }
            };
            TriggerObj trigger_intro_sustain = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
            };
            TriggerObj trigger_intro_sustain_off = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
                noteOff = true
            };
            initializeObjs();
            triggerEvent(trigger_intro_sustain);
            yield return new WaitForSecondsOrTrue(1, () => a.instantiated);
            float current_x = instance.transform.rotation.eulerAngles.x;
            Assert.Greater(current_x, 0);

            //stay sustained.
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(instance.transform.rotation.eulerAngles.x, current_x);
            triggerEvent(trigger_intro_sustain_off);
            yield return new WaitForSeconds(0.1f);
            Assert.Less(instance.transform.rotation.eulerAngles.x, current_x);
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            instance = getFirstRotateScaleInstance();
            activeRotate = instance.GetComponent<ActiveRotate>();
            a = obj1.triggerRotate;
        }
    }
}

