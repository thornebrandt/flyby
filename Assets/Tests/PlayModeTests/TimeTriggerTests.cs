using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Active;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests {
    public class TimeTriggerTests : PlayModeTests {

        [SetUp]
        public void Setup() {
            this.setup();
        }

        [TearDown]
        public void Teardown() {
            flybyController.killAll();
        }

        [UnityTest]
        public IEnumerator Can_Trigger_An_Instance_With_Time() {
            obj1 = createBlankObj();
            obj1.trigger = "timed_trigger";
            Trigger timed_trigger = new Trigger();
            timed_trigger.time = 0.1f;
            timed_trigger.eventName = "timed_trigger";
            addTrigger(timed_trigger);
            registerTriggers();
            initializeObjs();
            yield return new WaitForSeconds(0.2f);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            // wait seconds for time - 
        }

        [UnityTest]
        public IEnumerator Timed_Trigger_Will_Retrigger() {
            obj1 = createBlankObj();
            obj1.trigger = "timed_interval_trigger";
            obj1.numInstances = 2;
            Trigger timed_trigger = new Trigger();
            timed_trigger.time = 0.0f;
            timed_trigger.repeatInterval = 0.1f;
            timed_trigger.eventName = "timed_interval_trigger";
            addTrigger(timed_trigger);
            registerTriggers();
            initializeObjs();
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            GameObject instance2 = getPopupInstanceByIndex(1);
            Assert.AreEqual(instance2.transform.position, o.nullVector);
            yield return new WaitForSeconds(0.15f);
            Assert.AreEqual(instance2.transform.position, Vector3.zero);
        }


        [UnityTest]
        public IEnumerator Timed_Trigger_Will_Retrigger_Until_Num_Repeats() {
            obj1 = createBlankObj();
            obj1.trigger = "timed_interval_trigger";
            obj1.numInstances = 5;
            Trigger timed_trigger = new Trigger();
            timed_trigger.time = 0.0f;
            timed_trigger.repeatInterval = 0.1f;
            timed_trigger.numRepeats = 3;
            timed_trigger.eventName = "timed_interval_trigger";
            addTrigger(timed_trigger);
            registerTriggers();
            initializeObjs();
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            GameObject instance2 = getPopupInstanceByIndex(1);
            GameObject instance3 = getPopupInstanceByIndex(2);
            GameObject instance4 = getPopupInstanceByIndex(3);
            GameObject instance5 = getPopupInstanceByIndex(4);
            yield return new WaitForSeconds(0.11f);
            Assert.AreEqual(instance2.transform.position, Vector3.zero);
            yield return new WaitForSeconds(0.11f);
            Assert.AreEqual(instance3.transform.position, Vector3.zero);
            yield return new WaitForSeconds(0.11f);
            Assert.AreEqual(instance4.transform.position, o.nullVector);
        }


        [UnityTest]
        public IEnumerator Can_Trigger_Secondary_Behaviours_With_Time() {
            Trigger timed_trigger = new Trigger();
            timed_trigger.time = 0f;
            timed_trigger.eventName = "timed_trigger";
            Trigger timed_trigger2 = new Trigger();
            timed_trigger2.time = 0.1f;
            timed_trigger2.eventName = "timed_trigger2";
            Trigger timed_trigger3 = new Trigger();
            timed_trigger3.time = 0.3f;
            timed_trigger3.eventName = "timed_trigger3";
            addTrigger(timed_trigger);
            addTrigger(timed_trigger2);
            addTrigger(timed_trigger3);
            obj1 = createBlankObj();
            obj1.trigger = "timed_trigger";
            obj1.origin = new Vec(0);
            obj1.triggerPosition = new ActiveTrigger {
                value = new Vec {
                    a = 0
                },
                target = new Vec {
                    y = 1,
                },
                trigger = "timed_trigger2",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            x = -1,
                        },
                        trigger = "timed_trigger3",
                        onTime = 0.1f,
                        offTime = -1,
                    }
                },
                startTime = 0,
                onTime = 0.1f,
                offTime = -1,
            };
            registerTriggers();
            initializeObjs();
            yield return new WaitForSeconds(0.01f);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            yield return new WaitForSecondsOrTrue(0.3f, () => instance.transform.position.y == 1);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position.y, 1));
            yield return new WaitForSecondsOrTrue(0.3f, () => instance.transform.position == new Vector3(-1, 0, 0));
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(-1, 0, 0)));
        }

        //TODO - test for "timeOff"

    }
}