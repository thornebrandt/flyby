using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;


namespace Tests {
    public class InstanceArrayTests : PlayModeTests {
        private List<Obj> _objs; //objs of currentPart. 
        private GameObject instance1;
        private GameObject instance2;
        private GameObject instance3;
        private GameObject prefab1;
        private GameObject prefab2;
        private GameObject prefab3;

        [SetUp]
        public void Setup() {
            this.setup();
        }

        [TearDown]
        public void Teardown() {
            flybyController.killAll();
        }

        [Test]
        public void Makes_Copies_From_Num_Instances() {
            obj1 = createBlankObj();
            obj1.array = new InstanceArray {
                numInstances = 3
            };
            obj1.trigger = "intro";
            initializeObjs();
            Assert.AreEqual(_objs.Count, 3);
        }

        [Test]
        public void Array_With_Single_Obj_Property_Will_Only_Create_One_Obj() {
            obj1 = createBlankObj();
            obj1.array = new InstanceArray {
                singleObj = true,
                numInstances = 3
            };
            obj1.trigger = "intro";
            initializeObjs();
            Assert.AreEqual(_objs.Count, 1);
        }

        [UnityTest]
        public IEnumerator Array_With_Single_Obj_And_Children_Will_Affect_ChildIndex() {
            //the more intuitive test case after array_will_change_childIndex.  
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.asset = "ParentWithChildren";
            obj1.bundle = "TestPrefabs";
            obj1.hierarchyType = HierarchyType.None;
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    y = 1
                },
                childIndex = 0,
                trigger = "trigger_child_position",
                onTime = 0.1f,
                offTime = -1
            };
            //maye some property in array for "same object"
            obj1.array = new InstanceArray {
                numInstances = 3,
                singleObj = true,
                triggerPosition = new ActiveTrigger {
                    childIndex = 1,
                    onTime = 0
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("trigger_child_position");
            yield return new WaitForSeconds(0.2f);
            Transform child1 = flybyController.currentPart.objs[0].instances[0].transform.GetChild(0);
            Assert.AreEqual(child1.position.y, 1);
            Transform child2 = flybyController.currentPart.objs[0].instances[0].transform.GetChild(1);
            Assert.AreEqual(child2.position.y, 1);
            Transform child3 = flybyController.currentPart.objs[0].instances[0].transform.GetChild(2);
            Assert.AreEqual(child3.position.y, 1);
        }

        [UnityTest]
        public IEnumerator Array_With_Single_Obj_And_Children_Will_Affect_ChildIndex_With_Different_Triggers() {
            //the more intuitive test case after array_will_change_childIndex.  
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.asset = "ParentWithChildren";
            obj1.bundle = "TestPrefabs";
            obj1.hierarchyType = HierarchyType.None;
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    y = 1
                },
                childIndex = 0,
                trigger = "trigger",
                onTime = 0.1f,
                offTime = -1
            };
            //maye some property in array for "same object"
            obj1.array = new InstanceArray {
                numInstances = 3,
                singleObj = true,
                triggerPosition = new ActiveTrigger {
                    childIndex = 1,
                    onTime = 0,
                    trigger = "1"
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;

            Transform child1 = flybyController.currentPart.objs[0].instances[0].transform.GetChild(0);
            Transform child2 = flybyController.currentPart.objs[0].instances[0].transform.GetChild(1);
            Transform child3 = flybyController.currentPart.objs[0].instances[0].transform.GetChild(2);
            triggerEvent("trigger1");
            yield return new WaitForSeconds(0.2f);
            Assert.AreEqual(child1.position.y, 1);
            Assert.AreEqual(child2.position.y, 0);
            triggerEvent("trigger2");
            yield return new WaitForSeconds(0.2f);
            Assert.AreEqual(child2.position.y, 1);
            Assert.AreEqual(child3.position.y, 0);
            triggerEvent("trigger3");
            yield return new WaitForSeconds(0.2f);
            Assert.AreEqual(child3.position.y, 1);
        }

        [Test]
        public void Increments_Asset_Name_If_Array_Asset_Is_Present_And_Number() {
            obj1 = createBlankObj();
            obj1.asset = "test_asset";
            obj1.array = new InstanceArray {
                asset = "1",
                numInstances = 3
            };
            List<Obj> objs = obj1.array.makeArray(new Obj(obj1));
            Assert.AreEqual(objs.Count, 3);
            Assert.AreEqual(objs[0].asset, "test_asset1");
            Assert.AreEqual(objs[1].asset, "test_asset2");
            Assert.AreEqual(objs[2].asset, "test_asset3");
        }


        [Test]
        public void Increments_Property_Name_If_Array_Asset_Is_Present_And_Number() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                propertyName = "test_property"
            };
            obj1.array = new InstanceArray {
                triggerPosition = new ActiveTrigger {
                    propertyName = "0",
                },
                numInstances = 3
            };
            List<Obj> objs = obj1.array.makeArray(new Obj(obj1));
            Assert.AreEqual(objs.Count, 3);
            Assert.AreEqual(objs[0].triggerPosition.propertyName, "test_property0");
            Assert.AreEqual(objs[1].triggerPosition.propertyName, "test_property1");
            Assert.AreEqual(objs[2].triggerPosition.propertyName, "test_property2");
        }

        [UnityTest]
        public IEnumerator Increments_Trigger_Name_If_Array_Trigger_Is_Present_And_Number() {
            obj1 = createBlankObj();
            obj1.array = new InstanceArray {
                numInstances = 3,
                trigger = "1",
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro1");
            yield return 0;
            Assert.AreEqual(instance1.transform.position, Vector3.zero);
            Assert.AreEqual(instance2.transform.position, o.nullVector);
            triggerEvent("intro2");
            Assert.AreEqual(instance2.transform.position, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Makes_Three_Rotated_Copies_Based_On_Value_With_No_Obj_TriggerRotation() {
            obj1 = createBlankObj();
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerRotation = new ActiveTrigger {
                    value = new Vec {
                        x = 5
                    }
                }
            };
            obj1.hierarchyType = HierarchyType.None;
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(Math.RoughEquals(instance1.transform.rotation, Quaternion.Euler(0, 0, 0)));
            Assert.IsTrue(Math.RoughEquals(instance2.transform.rotation, Quaternion.Euler(5, 0, 0)));
            Assert.IsTrue(Math.RoughEquals(instance3.transform.rotation, Quaternion.Euler(10, 0, 0)));
        }

        [UnityTest]
        public IEnumerator Makes_Three_Rotated_Copies_Based_On_Value_With_TriggerRotation() {
            obj1 = createBlankObj();
            obj1.triggerRotation = new ActiveTrigger {
                startTime = 0,
                value = new Vec {
                    x = 0
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerRotation = new ActiveTrigger {
                    value = new Vec {
                        x = 5
                    }
                }
            };
            obj1.hierarchyType = HierarchyType.None;
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance1.transform.rotation, Quaternion.Euler(0, 0, 0)));
            Assert.IsTrue(Math.RoughEquals(instance2.transform.rotation, Quaternion.Euler(5, 0, 0)));
            Assert.IsTrue(Math.RoughEquals(instance3.transform.rotation, Quaternion.Euler(10, 0, 0)));
        }

        [UnityTest]
        public IEnumerator Makes_Three_Rotating_Copies_Based_On_Value() {
            //this is the first new property we are testing. 
            obj1 = createBlankObj();
            obj1.triggerRotate = new ActiveTrigger {
                startTime = 0,
                value = new Vec {
                    x = 0
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerRotate = new ActiveTrigger {
                    value = new Vec {
                        x = 0.1f
                    }
                }
            };
            obj1.hierarchyType = HierarchyType.None;
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRotate");
            yield return new WaitForSeconds(0.2f);
            Assert.IsTrue(Math.RoughEquals(instance1.transform.rotation, Quaternion.Euler(0, 0, 0)));
            Assert.Greater(instance2.transform.rotation.eulerAngles.x, instance1.transform.rotation.eulerAngles.x);
            //Assert.IsTrue(Math.RoughEquals(instance3.transform.rotation, Quaternion.Euler(10, 0, 0)));
        }


        [UnityTest]
        public IEnumerator Array_Will_Change_Target_Rotation() {
            obj1 = createBlankObj();
            obj1.triggerRotation = new ActiveTrigger {
                target = new Vec {
                    x = 0
                },
                startTime = 0,
                trigger = "triggerRotation",
                onTime = 0.1f,
                offTime = -1
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerRotation = new ActiveTrigger {
                    target = new Vec {
                        x = 5
                    },
                    onTime = 0,
                    offTime = -1
                },
            };
            obj1.hierarchyType = HierarchyType.None;
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRotation");
            yield return new WaitForSeconds(0.2f);
            Assert.IsTrue(Math.RoughEquals(instance1.transform.rotation, Quaternion.Euler(0, 0, 0)));
            Assert.IsTrue(Math.RoughEquals(instance2.transform.rotation, Quaternion.Euler(5, 0, 0)));
            Assert.IsTrue(Math.RoughEquals(instance3.transform.rotation, Quaternion.Euler(10, 0, 0)));
        }

        [UnityTest]
        public IEnumerator Makes_Three_Stacked_Copies_Based_On_Value_With_TriggerPosition() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                startTime = 0,
                value = new Vec {
                    x = 0
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    value = new Vec {
                        x = 1
                    }
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(prefab1.transform.position, Vector3.zero);
            Assert.AreEqual(prefab2.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(prefab3.transform.position, new Vector3(2, 0, 0));
        }

        [UnityTest]
        public IEnumerator Makes_Three_Stacked_Copies_With_A_Manipulated_Value() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                startTime = 0,
                value = new Vec {
                    x = 1
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    value = new Vec {
                        x = 1
                    }
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(prefab1.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(prefab2.transform.position, new Vector3(2, 0, 0));
            Assert.AreEqual(prefab3.transform.position, new Vector3(3, 0, 0));
        }

        [Test]
        public void Array_Will_Extend_Start_Value() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                startTime = 0,
                start = new Vec { },
                value = new Vec {
                    x = 1
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    start = new Vec {
                        x = -1
                    }
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(prefab1.transform.position, new Vector3(0, 0, 0));
            Assert.AreEqual(prefab2.transform.position, new Vector3(-1, 0, 0));
            Assert.AreEqual(prefab3.transform.position, new Vector3(-2, 0, 0));
        }



        [UnityTest]
        public IEnumerator Array_Will_Extend_Start_Delay_Of_Positions() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                startTime = 0.1f,
                start = new Vec {
                    x = 0
                },
                value = new Vec {
                    x = 1
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    startDelay = 0.5f,
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.2f, () => prefab1.transform.position == new Vector3(1, 0, 0));
            Assert.AreEqual(prefab1.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(prefab2.transform.position, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Array_Will_Extend_Start_Time_Of_Positions() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                startTime = 0.1f,
                start = new Vec {
                    x = 0
                },
                value = new Vec {
                    x = 1
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    startTime = 1.0f,
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.2f, () => prefab1.transform.position == new Vector3(1, 0, 0));
            Assert.Less(prefab2.transform.position.x, 1);
            Assert.Greater(prefab2.transform.position.x, 0);
        }

        [UnityTest]
        public IEnumerator Array_Will_Add_Colors() {
            obj1 = createBlankObj();
            obj1.triggerColor = new ActiveTrigger {
                color = new Col {
                    gray = 0
                },
                startTime = 0
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerColor = new ActiveTrigger {
                    color = new Col {
                        r = 0.5f
                    }
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Color color = prefab1.GetComponent<Renderer>().material.color;
            Assert.AreEqual(color, Color.black);
            Color color2 = prefab2.GetComponent<Renderer>().material.color;
            Assert.IsTrue(Math.RoughEquals(color2, new Color(0.5f, 0, 0)));
            Color color3 = prefab3.GetComponent<Renderer>().material.color;
            Assert.IsTrue(Math.RoughEquals(color3, new Color(1.0f, 0, 0)));
        }

        [UnityTest]
        public IEnumerator Array_Will_Subtract_Colors() {
            obj1 = createBlankObj();
            obj1.triggerColor = new ActiveTrigger {
                color = new Col {
                    gray = 1
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerColor = new ActiveTrigger {
                    color = new Col {
                        r = -0.5f
                    }
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Color color = prefab1.GetComponent<Renderer>().material.color;
            Assert.AreEqual(color, Color.white);
            Color color2 = prefab2.GetComponent<Renderer>().material.color;
            Assert.IsTrue(Math.RoughEquals(color2, new Color(0.5f, 1, 1)));
            Color color3 = prefab3.GetComponent<Renderer>().material.color;
            Assert.IsTrue(Math.RoughEquals(color3, new Color(0, 1, 1)));
        }

        [UnityTest]
        public IEnumerator Array_Will_Change_Target_Color() {
            obj1 = createBlankObj();
            obj1.triggerColor = new ActiveTrigger {
                color = new Col { },
                targetColor = new Col { },
                onTime = 0.2f,
                offTime = -1,
                trigger = "triggerColor"
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerColor = new ActiveTrigger {
                    targetColor = new Col {
                        g = 0.3f
                    },
                    onTime = 0
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            triggerEvent("triggerColor");
            yield return new WaitForSeconds(0.3f);
            Color color = prefab1.GetComponent<Renderer>().material.color;
            Assert.AreEqual(color, Color.black);
            Color color2 = prefab2.GetComponent<Renderer>().material.color;
            Assert.IsTrue(Math.RoughEquals(color2, new Color(0, 0.3f, 0)));
        }

        [UnityTest]
        public IEnumerator Array_Will_Change_Target_Position() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    x = 1
                },
                trigger = "triggerPosition",
                onTime = 0.1f,
                offTime = -1
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    target = new Vec {
                        x = 1
                    },
                    onTime = 0,
                    offTime = -1
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(0.2f, () => prefab1.transform.position == new Vector3(1, 0, 0));
            Assert.AreEqual(prefab1.transform.position.x, 1);
            Assert.AreEqual(prefab2.transform.position.x, 2);
            Assert.AreEqual(prefab3.transform.position.x, 3);
        }

        [UnityTest]
        public IEnumerator Array_Will_Change_Target_Trigger_String() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    x = 1
                },
                trigger = "triggerPosition",
                onTime = 0.1f,
                offTime = -1
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    trigger = "1",
                    onTime = 0
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerPosition1");
            yield return new WaitForSecondsOrTrue(0.2f, () => prefab1.transform.position == new Vector3(1, 0, 0));
            Assert.AreEqual(prefab1.transform.position.x, 1);
            Assert.AreEqual(prefab2.transform.position.x, 0);
            triggerEvent("triggerPosition2");
            yield return new WaitForSecondsOrTrue(0.2f, () => prefab2.transform.position == new Vector3(1, 0, 0));
            Assert.AreEqual(prefab2.transform.position.x, 1);
            Assert.AreEqual(prefab3.transform.position.x, 0);
        }


        [UnityTest]
        public IEnumerator Array_Will_Change_ChildIndex() {
            // this one should do a less than intuitive use case. 
            // three duplicate objs with different childIndexes. 
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.asset = "ParentWithChildren";
            obj1.bundle = "TestPrefabs";
            obj1.hierarchyType = HierarchyType.None;
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    y = 1
                },
                childIndex = 0,
                id = "trigger_child_position",
                trigger = "trigger_child_position",
                onTime = 0.1f,
                offTime = -1
            };
            //maye some property in array for "same object"
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    childIndex = 1,
                    onTime = 0
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("trigger_child_position");
            yield return new WaitForSeconds(0.2f);
            Transform child1 = flybyController.currentPart.objs[0].instances[0].transform.GetChild(0);
            Assert.AreEqual(child1.position.y, 1);
            Transform child2 = flybyController.currentPart.objs[1].instances[0].transform.GetChild(1);
            Assert.AreEqual(child2.position.y, 1);
            //notice the obj index as well as the child index are incremental
            Transform child3 = flybyController.currentPart.objs[2].instances[0].transform.GetChild(2);
            Assert.AreEqual(child3.position.y, 1);
        }

        [UnityTest]
        public IEnumerator Array_Will_Change_Knob_Color() {
            obj1 = createBlankObj();
            obj1.triggerColor = new ActiveTrigger {
                knobColor = new Col {
                    r = 0,
                    r2 = 0.1f
                },
                activeType = ActiveType.Next,
                trigger = "test_knob_array",
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerColor = new ActiveTrigger {
                    knobColor = new Col {
                        r = 0,
                        r2 = 0.1f
                    }
                }
            };
            Trigger t = new Trigger {
                eventName = "test_knob_array",
                channel = 0,
                knob = 1
            };
            obj1.trigger = "intro";
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerKnob(t, 1.0f);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Color color = prefab1.GetComponent<Renderer>().material.color;
            Assert.IsTrue(Math.RoughEquals(color, new Color(0.1f, 0, 0)));
            Color color2 = prefab2.GetComponent<Renderer>().material.color;
            Assert.IsTrue(Math.RoughEquals(color2, new Color(0.2f, 0, 0)));
        }


        [UnityTest]
        public IEnumerator Array_Will_Change_Knob_Position() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                knob = new Vec {
                    x = 0,
                    x2 = 1,
                    y = 0,
                    y2 = 1,
                    z = 0,
                    z2 = 1
                },
                activeType = ActiveType.Next,
                trigger = "test_knob_array",
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    knob = new Vec {
                        x = 0,
                        x2 = 1,
                        y = 0,
                        y2 = 1,
                        z = 0,
                        z2 = 1
                    },
                }
            };
            Trigger t = new Trigger {
                eventName = "test_knob_array",
                channel = 0,
                knob = 1
            };
            obj1.trigger = "intro";
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerKnob(t, 1.0f);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(prefab1.transform.position.x, 1);
            Assert.AreEqual(prefab2.transform.position.x, 2);
            Assert.AreEqual(prefab3.transform.position.x, 3);
        }


        [UnityTest]
        public IEnumerator Array_Will_Change_Nested_Target_Positions() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        onTime = 0,
                        offTime = -1,
                        trigger = "secondary_trigger",
                        target = new Vec {
                            x = 1
                        }
                    }
                }
            };

            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    triggers = new List<SecondaryActiveTrigger> {
                        new SecondaryActiveTrigger {
                            onTime = 0,
                            offTime = -1,
                            target = new Vec {
                                x = 1
                            }
                        }
                    }
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("secondary_trigger");
            yield return 0;
            Assert.AreEqual(prefab1.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(prefab2.transform.position, new Vector3(2, 0, 0));

        }

        [UnityTest]
        public IEnumerator Array_Will_Change_On_Delay() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    x = 1
                },
                trigger = "triggerPosition",
                onTime = 0.1f,
                offTime = -1
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    onDelay = 0.3f
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(0.2f, () => prefab1.transform.position == new Vector3(1, 0, 0));
            Assert.AreEqual(prefab1.transform.position.x, 1);
            Assert.AreEqual(prefab2.transform.position.x, 0);
            Assert.AreEqual(prefab3.transform.position.x, 0);
        }

        [UnityTest]
        public IEnumerator Array_Will_Change_On_Time() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    x = 1
                },
                trigger = "triggerPosition",
                onTime = 0.2f,
                offTime = -1
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    onTime = 0.2f
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSeconds(0.25f);
            Assert.AreEqual(prefab1.transform.position.x, 1);
            //rest of prefabs here.
            Assert.Less(prefab2.transform.position.x, prefab1.transform.position.x);
            Assert.Less(prefab3.transform.position.x, prefab2.transform.position.x);
        }

        [UnityTest]
        public IEnumerator Array_Will_Change_Off_Time() {
            //need to also check off delay.
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    x = 1
                },
                trigger = "triggerPosition",
                onTime = 0.2f,
                offDelay = 0.1f,
                offTime = 0.1f
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    onTime = 0,
                    offTime = 0.1f
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSeconds(0.25f);
            Assert.AreEqual(prefab1.transform.position.x, 1);
            Assert.AreEqual(prefab2.transform.position.x, 1);
            Assert.AreEqual(prefab3.transform.position.x, 1);
            yield return new WaitForSecondsOrTrue(1.0f, () => prefab1.transform.position.x < 0.8f);
            //the other ones take longer to come back. 
            Assert.Greater(prefab2.transform.position.x, prefab1.transform.position.x);
            Assert.Greater(prefab3.transform.position.x, prefab2.transform.position.x);
        }

        [UnityTest]
        public IEnumerator Array_Will_Change_Off_Delay() {
            //need to also check off delay.
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                target = new Vec {
                    x = 1
                },
                trigger = "triggerPosition",
                onTime = 0,
                offDelay = 0,
                offTime = 1.0f
            };
            obj1.array = new InstanceArray {
                numInstances = 3,
                triggerPosition = new ActiveTrigger {
                    onTime = 0,
                    offDelay = 0.1f
                }
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSeconds(0.5f);
            //the other ones take longer to start coming back. 
            Assert.Greater(prefab2.transform.position.x, prefab1.transform.position.x);
            Assert.Greater(prefab3.transform.position.x, prefab2.transform.position.x);
        }

        [Test]
        public void Array_Will_Copy_Properties_That_It_Is_Not_Altering() {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger {
                startTime = 0,
                value = new Vec {
                    x = 1
                }
            };
            obj1.array = new InstanceArray {
                numInstances = 3
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            Assert.IsTrue(Math.RoughEquals(prefab1.transform.position, new Vector3(1, 0, 0)));
            Assert.IsTrue(Math.RoughEquals(prefab1.transform.position, new Vector3(1, 0, 0)));
        }


        public override void initializeObjs() {
            base.initializeObjs();
            _objs = flybyController.currentPart.objs;
            //o = flybyController.currentPart.objs[0];
            instance1 = _objs[0].instances[0];
            prefab1 = Obj.getPrefabInstance(instance1);
            if (_objs.Count > 1) {
                instance2 = _objs[1].instances[0];
                instance3 = _objs[2].instances[0];
                prefab2 = Obj.getPrefabInstance(instance2);
                prefab3 = Obj.getPrefabInstance(instance3);
            }
        }
    }
}
