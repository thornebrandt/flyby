using System.Collections;
using System.Collections.Generic;
using flyby.Active;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyBySpriteTests : PlayModeTests
    {
        ActiveSprite activeSprite;
        Material material;

        [SetUp]
        public void Setup()
        {
            this.colorProperty = "_UnlitColor";
            go = GameObject.Find("FlyByController");
            if (go == null)
            {
                go = new GameObject("FlyByController");
            }
            flybyController = go.GetComponent<FlyByController>();
            if (flybyController == null)
            {
                flybyController = go.AddComponent<FlyByController>();
            }

            flybyController.configPath = "config.sample.json"; //empty
            if (assetController == null)
            {
                AssetController assetController = new AssetController();
                assetController.setup();
                //assetController.prefabCache = new Dictionary<string, GameObject>();
                camObj = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
            }

            flybyController.camObj = camObj;
            KeyboardController keyboardController = go.GetComponent<KeyboardController>();
            if (keyboardController == null)
            {
                go.AddComponent<KeyboardController>();
                go.AddComponent<MidiController>();
                go.AddComponent<ActiveBehaviourController>();
            }
            flybyController.initialize();
            objs = new List<NullObj>();
            NullObj obj1 = new NullObj();
            triggers = new List<Trigger>();
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            instance = getFirstPrefabInstance();
            activeSprite = instance.GetComponent<ActiveSprite>();
            material = instance.gameObject.GetComponent<Renderer>().material;
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
        }

        [Test]
        public void ActiveSprite_Is_Not_Null()
        {
            obj1 = createBlankObj();
            obj1.id = "new_obj";
            obj1.triggerSprite = new ActiveTrigger { };
            initializeObjs();
            Assert.IsNotNull(activeSprite);
        }

        [UnityTest]
        public IEnumerator Intro_Sprite_Is_Loaded()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startMedia = new Media
                {
                    path = "3x3.png"
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "3x3.png");
        }

        [UnityTest]
        //might as well setup media with value string. 
        public IEnumerator Simple_Setup_With_ValueString()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                valueString = "3x3.png"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "3x3.png");
        }

        [UnityTest]
        public IEnumerator Value_Media_Sprite_Is_Loaded_Immediately_If_No_Start()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                media = new Media
                {
                    path = "3x3.png"
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "3x3.png");
        }

        [UnityTest]
        public IEnumerator Image_Loads_Relative_To_Size_By_Default()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startMedia = new Media
                {
                    path = "2x1.png"
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "2x1.png");
            Assert.AreEqual(instance.transform.localScale.x, 1);
            Assert.AreEqual(instance.transform.localScale.y, 0.5f);
        }

        [UnityTest]
        public IEnumerator Image_Size_Makes_Bigger_Axis_Into_One()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startMedia = new Media
                {
                    path = "1x2.png"
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "1x2.png");
            Assert.AreEqual(instance.transform.localScale.y, 1);
            Assert.AreEqual(instance.transform.localScale.x, 0.5f);
        }

        [UnityTest]
        public IEnumerator Image_Ratio_Takes_Cells_Into_Account()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startMedia = new Media
                {
                    path = "2x1.png",
                    rows = 10,
                    cols = 20
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "2x1.png");
            Assert.AreEqual(instance.transform.localScale.x, 1);
            Assert.AreEqual(instance.transform.localScale.y, 1);
        }


        [UnityTest]
        public IEnumerator Can_Load_Sprite_Variants()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startMedia = new Media
                {
                    path = "rings/ring_.png",
                    numVariants = 2
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "rings/ring_0.png");
            triggerEvent("intro");
            // actual test.
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "rings/ring_1.png");
        }

        [UnityTest]
        public IEnumerator Can_Load_Sprite_Variants_In_Normal_Media()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0, //also need test for leaving this out. 
                media = new Media
                {
                    path = "rings/ring_.png",
                    numVariants = 2
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "rings/ring_0.png");
            triggerEvent("intro");
            // actual test.
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "rings/ring_1.png");
        }

        [UnityTest]
        public IEnumerator Sprite_Variants_Take_Instance_Index_Into_Account()
        {
            //first instance takes ring_0, second instance takes ring_1.
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.numInstances = 2;
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0, //also need test for leaving this out. 
                media = new Media
                {
                    path = "rings/ring_.png",
                    numVariants = 2
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "rings/ring_0.png");
            triggerEvent("intro");
            // actual test.
            yield return 0;
            GameObject instance2 = getPrefabInstanceByIndex(1);
            Material material2 = instance2.gameObject.GetComponent<Renderer>().material;
            Assert.AreEqual(material2.GetTexture("_UnlitColorMap").name, "rings/ring_1.png");
        }

        [UnityTest]
        public IEnumerator Intro_Current_And_End_Sprites_Are_Loaded()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0.1f,
                endTime = 0.1f,
                startMedia = new Media
                {
                    path = "3x3.png"
                },
                media = new Media
                {
                    path = "uv.jpg"
                },
                endMedia = new Media
                {
                    path = "1x1.png"
                }
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "3x3.png");
            yield return new WaitForSecondsOrTrue(1, () => activeSprite.introduced);
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "uv.jpg");
            yield return new WaitForSecondsOrTrue(1, () => activeSprite.ended);
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "1x1.png");
        }

        [UnityTest]
        public IEnumerator Sustain_Intro_Holds_Sprite_Until_Exit()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "introSustain";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0.1f,
                endTime = 0.1f,
                startMedia = new Media
                {
                    path = "3x3.png"
                },
                media = new Media
                {
                    path = "uv.jpg"
                },
                endMedia = new Media
                {
                    path = "1x1.png"
                }
            };

            TriggerObj sustainTriggerObj = new TriggerObj
            {
                eventName = "introSustain",
                sustain = true
            };
            TriggerObj noteOffTriggerObj = new TriggerObj
            {
                eventName = "introSustain",
                sustain = true,
                noteOff = true
            };

            initializeObjs();
            triggerEvent(sustainTriggerObj);
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "3x3.png");
            yield return new WaitForSecondsOrTrue(1, () => activeSprite.introduced);
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "uv.jpg");
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "uv.jpg");
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => activeSprite.ended);
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "1x1.png");
        }

        [UnityTest]
        public IEnumerator Can_Trigger_New_Sprite()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png"
                },
                targetMedia = new Media
                {
                    path = "uv.jpg"
                },
                trigger = "triggerSprite",
                onTime = 0f,
                offTime = -1,
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "3x3.png");
            triggerEvent("triggerSprite");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "uv.jpg");
        }

        [UnityTest]
        public IEnumerator Knob_Can_Trigger_Sprite()
        {
            obj1 = createBlankObj();
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    stops = 1,
                    totalFrames = 9
                },
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                },
                trigger = "test_knob",
                onTime = 0f,
                offTime = -1,
            };
            Trigger knobTrigger = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(knobTrigger);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(activeSprite.sprite.frame, 0);
            triggerKnob(knobTrigger, 1.0f);
            yield return 0;
            Assert.AreEqual(activeSprite.sprite.frame, 8);
        }


        [UnityTest]
        public IEnumerator Can_Trigger_Multiple_Secondary_Sprites()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png"
                },
                targetMedia = new Media
                {
                    path = "uv.jpg"
                },
                trigger = "triggerSprite",
                onTime = 0f,
                offTime = -1,
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger{
                        targetMedia = new Media {
                            path = "1x1.png",
                        },
                        trigger = "triggerSprite2",
                        onTime = 0,
                        offTime = -1
                    }
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "3x3.png");
            triggerEvent("triggerSprite");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "uv.jpg");
            triggerEvent("triggerSprite2");
            yield return 0;
            Assert.AreEqual(material.GetTexture("_UnlitColorMap").name, "1x1.png");
        }

        [UnityTest]
        public IEnumerator Can_Animate_Sprite()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    stops = 1,
                    totalFrames = 9
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.frame, 0);
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.sprite.frame == 1);
            if (activeSprite.sprite.frame == 0)
            {
                //unexplainable behaviour between single tests and bulk tests that changes how time works. 
                yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.sprite.frame == 1);
            }
            Assert.AreEqual(activeSprite.sprite.frame, 1);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 2);
        }


        [UnityTest]
        public IEnumerator Can_Start_On_A_Specific_Frame()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "specific_frame";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    stops = 1,
                    startFrame = 3,
                    totalFrames = 9
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.frame, 3);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 4);
        }

        [UnityTest]
        public IEnumerator Plays_All_Frames_In_A_3_x_3_Animation()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "9x9_sanity_check";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    stops = 1
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.frame, 0);
            yield return new WaitForSecondsOrTrue(0.3f, () => activeSprite.sprite.frame == 1);
            Assert.AreEqual(activeSprite.sprite.frame, 1);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 2);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 3);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 4);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 5);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 6);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 7);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 8);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 0);
        }


        [UnityTest]
        public IEnumerator Plays_All_Frames_In_A_3_x_4_Animation()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "9x9_sanity_check";
            obj1.triggerSprite = new ActiveTrigger
            {
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 4,
                    stops = 1
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.2f, () => activeSprite.sprite.frame == 0);
            Assert.AreEqual(activeSprite.sprite.frame, 0);
            yield return new WaitForSecondsOrTrue(0.2f, () => activeSprite.sprite.frame == 1);
            Assert.AreEqual(activeSprite.sprite.frame, 1);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 2);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 3);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 4);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 5);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 6);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 7);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 8);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 9);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 10);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 11);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 0);
        }


        [UnityTest]
        public IEnumerator Negative_Stops_Freeze_Animation()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "specific_frame";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    stops = -1,
                    startFrame = 3,
                    totalFrames = 9
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.frame, 3);
            yield return 0;
            yield return 0;
            yield return 0;
            //3 frames later, still on frame 3. 
            Assert.AreEqual(activeSprite.sprite.frame, 3);
        }

        [UnityTest]
        public IEnumerator Calculates_Total_Frames_Based_On_Rows_X_Columns()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "specific_frame";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "1x1.png",
                    cols = 10,
                    rows = 10,
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.totalFrames, 100);
            //TODO - need to redesign endFrame to use
            Assert.AreEqual(activeSprite._endFrame, 99); //recently changes for endFrame to be base-0
        }

        [UnityTest]
        public IEnumerator Can_Start_On_A_Random_Start_Frame()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "random_frame";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "1x1.png",
                    cols = 100,
                    rows = 100,
                    startFrame = 0,
                    startFrame2 = 99
                }
            };
            initializeObjs();
            bool matching = true;
            int lastStartingFrame = 0;
            for (int i = 0; i < 10; i++)
            {
                triggerEvent("intro");
                yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
                if (activeSprite.sprite.frame != lastStartingFrame)
                {
                    matching = false;
                    break;
                }
            }
            Assert.IsFalse(matching);
        }

        [UnityTest]
        public IEnumerator If_Repeat_Is_One_It_Stays_On_End_Frame()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "specific_frame";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    stops = 4,
                    totalFrames = 9,
                    repeat = 1
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.frame, 0);

            //there was a problem with this code where it was assumed base 1. 
            yield return new WaitForSecondsOrTrue(0.3f, () => activeSprite.sprite.frame == 8);
            // if (activeSprite.sprite.frame == 8)
            // {
            //     //unexplainable behaviour between single tests and bulk tests that changes how time works. 
            //     Debug.Log("and then we change some code.. adn voila");
            //     yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.sprite.frame == 9);
            // }
            Assert.AreEqual(activeSprite.sprite.frame, 8);
            float randomTime = Random.Range(0.3f, 1.0f);
            yield return new WaitForSeconds(randomTime);
            Assert.AreEqual(activeSprite.sprite.frame, 8);
        }

        [UnityTest]
        public IEnumerator Can_Pause_On_First_Frame_If_End_Frame_Is_Set_As_Zero()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "specific_frame";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    repeat = 0,
                    endFrame = 0
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.frame, 0);
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(activeSprite.sprite.frame, 0);
        }

        [UnityTest]
        public IEnumerator Can_Pause_On_First_Frame_For_Start_Media()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "specific_frame";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 5,
                startMedia = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    repeat = 0,
                    endFrame = 0
                },
                media = new Media  //from real-world bug. should never arrive her during the test. 
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    repeat = -1
                }
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(activeSprite.sprite.frame, 0);
            yield return new WaitForSeconds(1.0f);
            Assert.AreEqual(activeSprite.sprite.frame, 0);
        }

        [UnityTest]
        public IEnumerator If_Hide_On_Complete_It_Hides_After_Last_Frame()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.id = "specific_frame";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    stops = 1,
                    startFrame = 7,
                    totalFrames = 9,
                    repeat = 1,
                    hideOnComplete = true

                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.frame, 7);
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.sprite.frame == 8);
            // if (activeSprite.sprite.frame == 8)
            // {
            //     //unexplainable behaviour between single tests and bulk tests that changes how time works. 
            //     yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.sprite.frame == 9);
            // }
            Assert.AreEqual(activeSprite.sprite.frame, 8);
            yield return new WaitForSeconds(0.3f);
            Assert.IsFalse(instance.activeInHierarchy);
        }


        //TODO - check for startFrame being higher than endframe,(so we do need. totalFrames different than endFrame)


        [UnityTest]
        public IEnumerator Can_End_Loop_On_A_Specific_Frame()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "Sprite";
            obj1.trigger = "intro";
            obj1.triggerSprite = new ActiveTrigger
            {
                startTime = 0,
                media = new Media
                {
                    path = "3x3.png",
                    cols = 3,
                    rows = 3,
                    stops = 1,
                    endFrame = 3,
                    totalFrames = 9
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeSprite.introduced);
            Assert.AreEqual(activeSprite.sprite.frame, 0);
            yield return new WaitForSecondsOrTrue(1.0f, () => activeSprite.sprite.frame == 3);
            Assert.AreEqual(activeSprite.sprite.frame, 3);
            yield return null;
            Assert.AreEqual(activeSprite.sprite.frame, 0);
        }

    }

}