using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.TestTools;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using NUnit.Framework;

namespace Tests
{
    public class PresetTests : PlayModeTests
    {

        [SetUp]
        public void Setup()
        {
            go = new GameObject("FlyByController");
            flybyController = go.AddComponent<FlyByController>();
            flybyController.configPath = "Assets/Tests/PlayModeTests/json/test_config.json";
            if (assetController == null)
            {
                AssetController assetController = new AssetController();
                assetController.prefabCache = new Dictionary<string, GameObject>();
                camObj = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
            }
            flybyController.camObj = camObj;
            go.AddComponent<KeyboardController>();
            go.AddComponent<MidiController>();
            go.AddComponent<ActiveBehaviourController>();
            flybyController.initialize();
            triggerEvent("load_test_song2");
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
        }

        [Test]
        public void Can_Copy_Objs_In_Same_JSON_File()
        {
            Obj presetO = flybyController.currentPart.objs[0];
            Obj copiedO = flybyController.currentPart.objs[1];
            Obj copiedO2 = flybyController.currentPart.objs[2];
            triggerEvent("trigger_original1");
            GameObject presetInstance = Obj.getPrefabInstance(presetO.instances[0]);
            GameObject copiedInstance = Obj.getPrefabInstance(copiedO.instances[0]);
            GameObject identicalInstance = Obj.getPrefabInstance(copiedO2.instances[0]);
            //original origin. 
            Assert.AreEqual(presetInstance.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(identicalInstance.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(copiedInstance.transform.position, presetO.nullVector);
            triggerEvent("trigger_copy1");
            //copied instance overrides the first one/ 
            Assert.AreEqual(copiedInstance.transform.position, new Vector3(0, 0, 0));
        }

        [Test]
        public void Can_Copy_A_Copy()
        {
            Obj preset = flybyController.currentPart.objs[0];
            Obj copy = flybyController.currentPart.objs[2];
            Obj copyOfCopy = flybyController.currentPart.objs[3];
            triggerEvent("trigger_original1");
            GameObject presetInstance = Obj.getPrefabInstance(preset.instances[0]);
            GameObject copiedInstance = Obj.getPrefabInstance(copy.instances[0]);
            GameObject copyOfCopiedInstance = Obj.getPrefabInstance(copyOfCopy.instances[0]);
            GameObject copiedRotateScaleInstance = Obj.getRotateScaleInstance(copy.instances[0]);
            GameObject copyOfCopiedRotateScaleInstance = Obj.getRotateScaleInstance(copy.instances[0]);
            //all origins are inherited. 
            Assert.AreEqual(presetInstance.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(copiedInstance.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(copyOfCopiedInstance.transform.position, new Vector3(1, 0, 0));
            //scale is inherited. 
            Assert.AreEqual(copiedRotateScaleInstance.transform.localScale, new Vector3(2, 2, 2));
            Assert.AreEqual(copyOfCopiedRotateScaleInstance.transform.localScale, new Vector3(2, 2, 2));
        }

        [Test]
        public void Can_Use_ActiveTrigger_Preset()
        {
            Obj preset = flybyController.currentPart.objs[1];
            Obj copy = flybyController.currentPart.objs[2];
            triggerEvent("trigger_copy1"); //copy needs this trigger.
            triggerEvent("trigger_original1"); //copy of copy needs this trigger.
            GameObject presetInstance = Obj.getPrefabInstance(preset.instances[0]);
            GameObject copiedInstance = Obj.getPrefabInstance(copy.instances[0]);
            Assert.AreEqual(getColor(presetInstance), Color.red);
            Assert.AreEqual(getColor(copiedInstance), Color.red);
        }

        [UnityTest]
        public IEnumerator Copies_ActiveTrigger_Preset_Child_SecondaryActiveTriggers()
        {
            Obj preset = flybyController.currentPart.objs[1];
            Obj copy = flybyController.currentPart.objs[2];
            triggerEvent("trigger_copy1"); //copy needs this trigger.
            triggerEvent("trigger_original1"); //copy of copy needs this trigger.
            GameObject presetInstance = Obj.getPrefabInstance(preset.instances[0]);
            GameObject copiedInstance = Obj.getPrefabInstance(copy.instances[0]);
            Assert.AreEqual(getColor(presetInstance), Color.red);
            Assert.AreEqual(getColor(copiedInstance), Color.red);
            triggerEvent("triggerGreen");
            yield return new WaitForSeconds(0.2f);
            Assert.IsTrue(Math.RoughEquals(getColor(presetInstance), Color.green));
            Assert.IsTrue(Math.RoughEquals(getColor(copiedInstance), Color.green));
        }

        [UnityTest]
        public IEnumerator Copies_ActiveTrigger_From_Other_SecondaryActiveTriggers()
        {
            Obj preset = flybyController.currentPart.objs[1];
            Obj copy = flybyController.currentPart.objs[4];
            triggerEvent("trigger_copy1"); //copy needs this trigger.
            triggerEvent("trigger_preset1"); //copy of copy needs this trigger.
            GameObject presetInstance = Obj.getPrefabInstance(preset.instances[0]);
            GameObject copiedInstance = Obj.getPrefabInstance(copy.instances[0]);
            triggerEvent("triggerGreen");
            yield return new WaitForSeconds(0.2f);
            Assert.IsTrue(Math.RoughEquals(getColor(presetInstance), Color.green));
            Assert.IsTrue(Math.RoughEquals(getColor(copiedInstance), Color.green));
        }

        [UnityTest]
        public IEnumerator Multiple_ActiveTriggers_Can_Copy_The_Same_ActiveTrigger_Preset()
        {
            //this works when there are TWO children, but breaks on 3. 
            Obj preset = flybyController.currentPart.objs[7];
            triggerEvent("trigger1");
            yield return 0;
            GameObject parentPrefabInstance = Obj.getPrefabInstance(preset.instances[0]);
            GameObject child0 = parentPrefabInstance.transform.GetChild(0).gameObject;
            Assert.AreEqual(child0.name, "originalChild");
            GameObject child1 = parentPrefabInstance.transform.GetChild(1).gameObject;
            Assert.AreEqual(child1.name, "secondChild");
            GameObject child2 = parentPrefabInstance.transform.GetChild(2).gameObject;
            Assert.AreEqual(child2.name, "thirdChild");
            Assert.AreEqual(child0.transform.localScale, Vector3.one);
            triggerEvent("trigger2");
            yield return new WaitForSecondsOrTrue(1.0f, () => child0.transform.localScale.x == 2);
            yield return 0;
            Assert.AreEqual(child0.transform.localScale, new Vector3(2, 2, 2));
            Assert.AreEqual(child1.transform.localScale, new Vector3(2, 2, 2));
            Assert.AreEqual(child2.transform.localScale, new Vector3(2, 2, 2));
        }

        [Test]
        public void Can_Copy_Built_In_Set_Presets()
        {
            Obj copiedO = flybyController.currentPart.objs[4];
            triggerEvent("trigger_preset1");
            GameObject copiedInstance = Obj.getPrefabInstance(copiedO.instances[0]);
            Assert.AreEqual(copiedInstance.transform.position, new Vector3(1, 1, 1));
        }

        // [UnityTest]
        // public IEnumerator Presets_Can_Replace_Groups()
        // {
        //     Obj preset = flybyController.currentPart.objs[8];
        //     TriggerObj groupTrigger1 = new TriggerObj();
        //     groupTrigger1.group = "group1";
        //     groupTrigger1.eventName = "trigger_group1";
        //     Obj copy = flybyController.currentPart.objs[9];
        //     TriggerObj groupTrigger2 = new TriggerObj();
        //     groupTrigger2.group = "group2";
        //     groupTrigger2.eventName = "trigger_group2";
        //     triggerEvent(groupTrigger1);
        //     yield return 0;
        //     triggerEvent(groupTrigger2);
        //     GameObject presetInstance = Obj.getPrefabInstance(preset.instances[0]);
        //     GameObject copiedInstance = Obj.getPrefabInstance(copy.instances[0]);
        //     Assert.AreEqual(presetInstance.transform.position, Vector3.zero);
        //     Assert.AreEqual(copiedInstance.transform.position, Vector3.zero);
        // }

        //TODO - test that presets can nullify groups. 

        [Test]
        public void Uniform_False_Is_Copied()
        {
            Obj preset = flybyController.currentPart.objs[10];
            Obj copy = flybyController.currentPart.objs[11];
            GameObject presetInstance = Obj.getPrefabInstance(preset.instances[0]);
            GameObject copiedInstance = Obj.getPrefabInstance(copy.instances[0]);
            Assert.AreNotEqual(presetInstance.transform.position.x, presetInstance.transform.position.y);
            Assert.AreNotEqual(copiedInstance.transform.position.x, copiedInstance.transform.position.y);
            Assert.AreNotEqual(this.getColor(presetInstance).r, this.getColor(presetInstance).g);
            Assert.AreNotEqual(this.getColor(copiedInstance).r, this.getColor(copiedInstance).g);
        }

        [Test]
        public void Origin_Is_Copied()
        {
            Obj preset = flybyController.currentPart.objs[12];
            Obj copy = flybyController.currentPart.objs[13];
            GameObject presetInstance = Obj.getPrefabInstance(preset.instances[0]);
            GameObject copiedInstance = Obj.getPrefabInstance(copy.instances[0]);
            triggerEvent("trigger_preset_12");
            Assert.AreEqual(presetInstance.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(copiedInstance.transform.position, new Vector3(1, 0, 0));
        }

        [Test]
        public void Scale_Is_Copied()
        {
            Obj preset = flybyController.currentPart.objs[12];
            Obj copy = flybyController.currentPart.objs[13];
            GameObject presetInstance = Obj.getRotateScaleInstance(preset.instances[0]);
            GameObject copiedInstance = Obj.getRotateScaleInstance(copy.instances[0]);
            triggerEvent("trigger_preset_12");
            Assert.AreEqual(presetInstance.transform.localScale, new Vector3(2, 2, 2));
            Assert.AreEqual(copiedInstance.transform.localScale, new Vector3(2, 2, 2));
        }

        [Test]
        public void Scale_Is_Overriden()
        {
            Obj preset = flybyController.currentPart.objs[12];
            Obj copy = flybyController.currentPart.objs[14];
            GameObject presetInstance = Obj.getRotateScaleInstance(preset.instances[0]);
            GameObject copiedInstance = Obj.getRotateScaleInstance(copy.instances[0]);
            triggerEvent("trigger_preset_12");
            Assert.AreEqual(presetInstance.transform.localScale, new Vector3(2, 2, 2));
            Assert.AreEqual(copiedInstance.transform.localScale, new Vector3(3, 3, 3));
        }

        //TODO - check that copied scale can override preset scale. 


        private Color getColor(GameObject instance)
        {
            return instance.GetComponent<Renderer>().material.GetColor(this.colorProperty);
        }
    }
}