using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests {
    public class FlyByControllerTests : PlayModeTests {

        [SetUp]
        public void Setup() {
            // have tried and failed several times to optimise this setup and the singletons don't like it. 
            this.configPath = "Assets/Tests/PlayModeTests/json/test_config.json"; //test json files. 
            setup();
        }

        [TearDown]
        public void Teardown() {
            flybyController.killAll();
            instance = null;
        }

        [Test]
        public void Sets_Up_Controller() {
            Assert.IsNotNull(flybyController);
            Assert.IsNotNull(flybyController.currentSong.currentPart);
            Assert.AreEqual(flybyController.currentSong.currentPart.objs[0].id, "test_obj");
        }

        [Test]
        public void Song_Resets_And_Reloads_Json() {
            Obj o = flybyController.currentPart.objs[0];
            GameObject instance = Obj.getPrefabInstance(o.instances[0]);
            Assert.AreEqual(instance.transform.position, o.nullVector);
            triggerEvent("test_trigger");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("reset");
            o = flybyController.currentPart.objs[0];
            instance = Obj.getPrefabInstance(o.instances[0]);
            Assert.AreEqual(instance.transform.position, o.nullVector);
        }

        [Test]
        public void Can_Load_Second_Song_With_Trigger() {
            Assert.AreEqual(flybyController.currentSong.id, "test_song1");
            triggerEvent("load_test_song2");
            Assert.AreEqual(flybyController.currentSong.id, "test_song2");
        }

        [Test]
        public void Second_Song_Removes_First_Song_Instances() {
            Assert.AreEqual(flybyController.currentSong.id, "test_song1");
            triggerEvent("load_test_song2");
            Assert.IsNull(instance);
        }

        [Test]
        public void Can_Load_Second_Part_With_Trigger() {
            Assert.AreEqual(flybyController.currentPart.id, "test_part1");
            triggerEvent("load_test_part2");
            Assert.AreEqual(flybyController.currentPart.id, "test_part2");
        }

        [Test]
        public void Can_Trigger_Next_Part_With_Event() {
            Assert.AreEqual(flybyController.currentPart, flybyController.currentSong.loadedParts[0]);
            triggerEvent("next_part");
            Assert.AreEqual(flybyController.currentSong.currentPart, flybyController.currentSong.loadedParts[1]);
        }

        [Test]
        public void Can_Trigger_Previous_Part_With_Event() {
            Assert.AreEqual(flybyController.currentPart, flybyController.currentSong.loadedParts[0]);
            triggerEvent("prev_part");
            Assert.AreEqual(flybyController.currentSong.currentPart, flybyController.currentSong.loadedParts[2]);
        }

        [Test]
        public void Loads_Second_Part_Instances_And_Hides_First() {
            Assert.AreEqual(flybyController.currentPart.id, "test_part1");
            triggerEvent("test_trigger");
            Obj o = flybyController.currentPart.objs[0];
            GameObject instance = Obj.getPrefabInstance(o.instances[0]);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("load_test_part2");
            Obj o2 = flybyController.currentPart.objs[0];
            GameObject instance2 = Obj.getPrefabInstance(o2.instances[0]);
            //Assert.AreEqual(instance2.transform.position, o.nullVector);
            triggerEvent("test_trigger");
            Assert.AreEqual(instance2.transform.position, new Vector3(0, 0, 2));
            Assert.AreEqual(instance.transform.position, o.nullVector); //kills first instance. 
        }

        [Test]
        public void Triggers_Only_Fires_Instances_On_Current_Part() {
            Obj o = flybyController.currentPart.objs[0];
            Obj o2 = flybyController.currentSong.loadedParts[1].objs[0];
            GameObject part1Instance = Obj.getPrefabInstance(o.instances[0]);
            GameObject part1Instance2 = Obj.getPrefabInstance(o.instances[1]);
            GameObject part2Instance = Obj.getPrefabInstance(o2.instances[0]);
            triggerEvent("test_trigger");
            Assert.AreEqual(part1Instance.transform.position, Vector3.zero);
            Assert.AreEqual(part2Instance.transform.position, o.nullVector);
            triggerEvent("load_test_part2");
            Assert.AreEqual(part2Instance.transform.position, o.nullVector);
            triggerEvent("test_trigger");
            Assert.AreEqual(part2Instance.transform.position, new Vector3(0, 0, 2));
            Assert.AreEqual(part1Instance2.transform.position, o.nullVector);
        }


        [Test]
        public void Active_Behaviour_Fires_Intro_Behaviour() {
            Obj o = flybyController.currentPart.objs[0];
            GameObject instance1 = Obj.getPrefabInstance(o.instances[0]);
            instance1.GetComponent<Renderer>().material.color = Color.white;
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = "test_trigger";
            flybyController.triggerController.triggerSources[0].trigger(triggerObj);
            Assert.AreEqual(instance1.GetComponent<Renderer>().material.color, Color.red);
        }

        [UnityTest]
        public IEnumerator Active_Trigger_Only_Fires_For_Active_Instance() {
            Obj o = flybyController.currentPart.objs[0];
            GameObject instance1 = Obj.getPrefabInstance(o.instances[0]);
            GameObject instance2 = Obj.getPrefabInstance(o.instances[1]);
            instance1.GetComponent<Renderer>().material.color = Color.white;
            TriggerObj instantiateTrigger = new TriggerObj();
            instantiateTrigger.eventName = "test_trigger";
            flybyController.triggerController.triggerSources[0].trigger(instantiateTrigger);
            flybyController.triggerController.triggerSources[0].trigger(instantiateTrigger);
            yield return new WaitForSecondsOrTrue(1, () => instance2.GetComponent<Renderer>().material.color == Color.green);
            TriggerObj activeTrigger = new TriggerObj();
            activeTrigger.eventName = "test_active_trigger";
            flybyController.triggerController.triggerSources[0].trigger(activeTrigger);
            //should only trigger the second instance. 
            yield return new WaitForSecondsOrTrue(1, () => instance2.GetComponent<Renderer>().material.color == Color.blue);
            Assert.AreEqual(instance2.GetComponent<Renderer>().material.color, Color.blue);
            Assert.IsTrue(Math.RoughEquals(instance2.GetComponent<Renderer>().material.color, Color.blue));
            Assert.IsTrue(Math.RoughEquals(instance1.GetComponent<Renderer>().material.color, Color.green)); //original color. 
        }

        [UnityTest]
        public IEnumerator Sustain_Works_For_Instantiation() {
            Obj o = flybyController.currentPart.objs[3];
            Assert.AreEqual(o.id, "test_sustain");
            GameObject instance = Obj.getPrefabInstance(o.instances[0]);
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = "test_sustain";
            triggerObj.sustain = true;
            triggerObj.note = 1;
            triggerEvent(triggerObj);
            yield return new WaitForSecondsOrTrue(0.5f, () => getColor(instance) == Color.green);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.green));
            yield return new WaitForSecondsOrTrue(0.5f, () => getColor(instance) == Color.red);
            //stays sustained until release event. 
            Assert.AreEqual(getColor(instance), Color.green);
            TriggerObj releaseTrigger = new TriggerObj();
            releaseTrigger.note = 1;
            releaseTrigger.eventName = "test_sustain";
            releaseTrigger.noteOff = true;
            triggerEvent(releaseTrigger);
            yield return new WaitForSecondsOrTrue(0.5f, () => getColor(instance) == Color.red);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Sustain_Works_For_Active_Triggers() {
            Obj o = flybyController.currentPart.objs[4];
            Assert.AreEqual(o.id, "test_sustain_trigger");
            GameObject instance = Obj.getPrefabInstance(o.instances[0]);
            yield return new WaitForSeconds(0.1f);
            TriggerObj instantiateTrigger = new TriggerObj();
            instantiateTrigger.eventName = "test_sustain_trigger_intro";
            triggerEvent(instantiateTrigger);
            TriggerObj sustainTrigger = new TriggerObj();
            sustainTrigger.eventName = "test_sustain_trigger";
            sustainTrigger.sustain = true;
            sustainTrigger.note = 1;
            triggerEvent(sustainTrigger);
            Assert.AreEqual(getColor(instance), Color.black);
            yield return new WaitForSecondsOrTrue(0.5f, () => getColor(instance) == Color.green);
            Assert.AreEqual(getColor(instance), Color.green);
            yield return new WaitForSecondsOrTrue(1f, () => getColor(instance) == Color.black);
            Assert.AreEqual(getColor(instance), Color.green);
            //does not return to black if sustained until note off. 
            TriggerObj noteOffTrigger = new TriggerObj();
            noteOffTrigger.eventName = "test_sustain_trigger";
            noteOffTrigger.noteOff = true;
            noteOffTrigger.note = 1;
            triggerEvent(noteOffTrigger);
            yield return new WaitForSecondsOrTrue(1f, () => getColor(instance) == Color.black);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.black));
        }

        [UnityTest]
        public IEnumerator Primary_And_Secondary_Triggers_Work_For_Position() {
            Obj o = flybyController.currentPart.objs[5];
            GameObject instance = o.instances[0];
            GameObject popup_go = Obj.getPopupInstance(instance);
            TriggerObj introTrigger = new TriggerObj();
            introTrigger.eventName = "test_position";
            flybyController.triggerController.triggerSources[0].trigger(introTrigger);
            Assert.AreEqual(popup_go.transform.position, Vector3.zero);
            TriggerObj trigger1 = new TriggerObj();
            trigger1.eventName = "test_position_trigger1";
            flybyController.triggerController.triggerSources[0].trigger(trigger1);
            yield return new WaitForSecondsOrTrue(1, () => popup_go.transform.position.x == 1);
            Assert.AreEqual(popup_go.transform.position, new Vector3(1, 0, 0));
            TriggerObj trigger2 = new TriggerObj();
            trigger2.eventName = "test_position_trigger2";
            flybyController.triggerController.triggerSources[0].trigger(trigger2);
            yield return new WaitForSecondsOrTrue(1, () => popup_go.transform.position.y == 1);
            Assert.AreEqual(popup_go.transform.position, new Vector3(0, 1, 0));
        }

        [UnityTest]
        public IEnumerator Knob_Trigger_Works() {
            Obj o = flybyController.currentPart.objs[6];
            GameObject instance = o.instances[0];
            GameObject instance_child = Obj.getPrefabInstance(instance);
            triggerEvent("test_knob_intro");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, 1);
            yield return new WaitForSecondsOrTrue(1, () => instance_child.transform.position.x == 2);
            Assert.AreEqual(instance_child.transform.position, new Vector3(2, 0, 0));
        }

        [UnityTest]
        public IEnumerator Trigger_Obj_Can_Override_ActiveType() {
            Obj o = flybyController.currentPart.objs[0];
            GameObject instance1 = Obj.getPrefabInstance(o.instances[0]);
            GameObject instance2 = Obj.getPrefabInstance(o.instances[1]);
            GameObject instance3 = Obj.getPrefabInstance(o.instances[2]);
            triggerEvent("test_trigger");
            yield return new WaitForSecondsOrTrue(1, () => getColor(instance1) == Color.green);
            //should only affect one instance. 
            Assert.IsTrue(Math.RoughEquals(getColor(instance1), Color.green));
            Assert.IsFalse(Math.RoughEquals(getColor(instance3), Color.green));
            triggerEvent("test_trigger");
            triggerEvent("test_trigger");
            //after two more triggers it affects the third instance. 
            yield return new WaitForSecondsOrTrue(1, () => getColor(instance3) == Color.green);
            Assert.IsTrue(Math.RoughEquals(getColor(instance3), Color.green));
            Assert.AreEqual(instance3.transform.position, Vector3.zero);
            triggerEvent("test_active_type_trigger");
            //triggerPosition has activeType.All, should affect all instances on one trigger.
            yield return new WaitForSeconds(1);
            Assert.AreEqual(instance3.transform.position, new Vector3(0, 1, 0));
            Assert.AreEqual(instance1.transform.position, new Vector3(0, 1, 0));
        }

        [Test]
        public void TriggerController_Triggered_List_Starts_Empty() {
            Assert.AreEqual(TriggerController.instance.triggered.Count, 0);
        }

        [Test]
        public void TriggerController_Triggered_Stores_Triggered_Knobs() {
            Assert.AreEqual(flybyController.triggerController.triggered.Count, 0);
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = "test_knob";
            triggerObj.knob = 0;
            triggerObj.value = 0.5f;
            triggerObj.channel = 0;
            triggerObj.source = "midiKnob";
            triggerObj.created = Time.time;
            triggerObj.changed = Time.time;
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(triggerObj);
            Assert.AreEqual(flybyController.triggerController.triggered.Count, 1);
            Assert.AreEqual(TriggerController.instance.triggered["test_knob"].value, 0.5f);
        }

        [UnityTest]
        public IEnumerator Does_Not_Fire_Empty_Trigger_Events() {
            obj1 = createBlankObj();
            initializeObjs();
            TriggerObj emptyTriggerObj = new TriggerObj();
            triggerEvent(emptyTriggerObj);
            yield return 0;
            //should not have brought the instance to zero. 
            Assert.AreEqual(instance.transform.position, o.nullVector);
        }

        [UnityTest]
        public IEnumerator ActiveType_All_Works_On_Colors_From_Knob_For_First_Instance() {
            triggerEvent("load_test_part2");
            Obj o = flybyController.currentPart.objs[4];
            Assert.AreEqual(o.id, "active_type_next_test");
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = "testNextColorKnob";
            triggerObj.knob = 0;
            triggerObj.value = 1;
            triggerObj.channel = 0;
            triggerObj.source = "midiKnob";
            triggerObj.created = Time.time;
            triggerObj.changed = Time.time;
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(triggerObj);
            //r2 is 1 and activeType is Next, this should make the next instance red before instantiation. 
            triggerEvent("active_type_next_intro");
            yield return new WaitForSeconds(0.1f);
            GameObject instance = Obj.getPrefabInstance(o.instances[0]);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator ActiveType_All_Works_On_Colors_From_Knob_For_All_Future_Instances() {
            triggerEvent("load_test_part2");
            Obj o = flybyController.currentPart.objs[4];
            Assert.AreEqual(o.id, "active_type_next_test");
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = "testNextColorKnob";
            triggerObj.knob = 0;
            triggerObj.value = 1;
            triggerObj.channel = 0;
            triggerObj.source = "midiKnob";
            triggerObj.created = Time.time;
            triggerObj.changed = Time.time;
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(triggerObj);
            Assert.AreEqual(flybyController.triggerController.triggered.Count, 1);
            triggerEvent("active_type_next_intro");
            yield return new WaitForSeconds(0.1f);
            GameObject instance = Obj.getPrefabInstance(o.instances[0]);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
            //the knob should be retained for all next instances.
            triggerEvent("active_type_next_intro");
            yield return new WaitForSeconds(0.2f);
            GameObject instance2 = Obj.getPrefabInstance(o.instances[1]);
            Assert.IsTrue(Math.RoughEquals(getColor(instance2), Color.red));
        }

        [UnityTest]
        public IEnumerator Knob_Clamping_Works() {
            Obj o = flybyController.currentPart.objs[7];
            GameObject instance = o.instances[0];
            GameObject instance_child = Obj.getPrefabInstance(instance);
            triggerEvent("test_knob_clamp_intro");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = "test_knob_clamp";
            triggerObj.knob = 0;
            triggerObj.value = 0;
            triggerObj.channel = 0;
            triggerObj.source = "midiKnob";
            triggerObj.created = Time.time;
            triggerObj.changed = Time.time;
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(triggerObj);
            yield return new WaitForSecondsOrTrue(1, () => instance_child.transform.position.x == 0.5f);
            Assert.AreEqual(instance_child.transform.position, new Vector3(0.5f, 0, 0));
        }

        [UnityTest]
        public IEnumerator Can_Trigger_A_Camera_Angle() {
            //TODO - cameras not deconstructing properly in multiple tests. 
            GameObject cam = flybyController.cameraController.camObj.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
            yield return 0;
            //starts off as default position. 
            Vector3 defaultPosition = new Vector3(0, 0, -5);
            Assert.AreEqual(cam.transform.position, defaultPosition);
            triggerEvent("next_camera_angle");
            yield return new WaitForSecondsOrTrue(1, () => cam.transform.position.y == 10);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 10, 0));
        }

        [UnityTest]
        public IEnumerator Note_Off_Trigger_Does_Not_Trigger_A_Camera_Angle() {
            GameObject cam = flybyController.cameraController.camObj.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
            yield return 0;
            //starts off as default position. 
            Vector3 defaultPosition = new Vector3(0, 0, -5);
            Assert.AreEqual(cam.transform.position, defaultPosition);
            TriggerObj t = new TriggerObj {
                eventName = "next_camera_cangle",
                noteOff = true
            };
            triggerEvent(t);
            yield return new WaitForSecondsOrTrue(1, () => cam.transform.position.y == 10);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -5));
        }


        [UnityTest]
        public IEnumerator A_PreRendered_Child_Can_Use_ActiveParent() {
            triggerEvent("load_test_song2");
            Obj parentO = flybyController.currentPart.objs[5];
            Obj childO = flybyController.currentPart.objs[6];
            GameObject parentInstance = Obj.getPrefabInstance(parentO.instances[0]);
            GameObject childInstance = childO.instances[0];
            yield return new WaitForSeconds(0.15f);
            Assert.AreEqual(childInstance.name, "test_child_prerender_0");
            Assert.AreEqual(parentInstance.name, "test_parent_prerender_0_prefab");
            Assert.AreEqual(childInstance.transform.parent, parentInstance.transform);
        }

        [UnityTest]
        public IEnumerator Can_ReTrigger_Camera_Angles_After_Reset() {
            GameObject cam = flybyController.cameraController.camObj.transform.GetChild(0).GetChild(0).GetChild(0).gameObject;
            yield return 0;
            //starts off as default position. 
            Vector3 defaultPosition = new Vector3(0, 0, -5);
            Assert.AreEqual(cam.transform.position, defaultPosition);
            triggerEvent("next_camera_angle");
            yield return new WaitForSecondsOrTrue(1, () => cam.transform.position.y == 10);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 10, 0));
            triggerEvent("next_camera_angle");
            yield return new WaitForSecondsOrTrue(1, () => cam.transform.position.z == -5);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -5));
            triggerEvent("reset");
            yield return 0;
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -5));
            triggerEvent("next_camera_angle");
            //responding to a bug in which camera angles appear to be double triggered. 
            yield return new WaitForSecondsOrTrue(1, () => cam.transform.position.y == 10);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 10, 0));
        }

        [Test]
        public void Can_PreRender_An_Obj() {
            triggerEvent("load_test_part2");
            Obj o = flybyController.currentPart.objs[1];
            GameObject instance = o.instances[0];
            Assert.AreEqual(instance.transform.position.y, -1);
        }

        [Test]
        public void Can_Render_An_Asset_From_A_Bundle() {
            Obj o = flybyController.currentPart.objs[8];
            GameObject instance = o.instances[0];
            Assert.AreEqual(instance.name, "TestSphere_0");
            GameObject instance_child = Obj.getPrefabInstance(instance);
            Assert.IsNotNull(instance_child.GetComponent<SphereCollider>());
        }

        [Test]
        public void Obj_Inherits_Bundle_Name_From_Part() {
            triggerEvent("load_test_part2");
            Obj o = flybyController.currentPart.objs[0];
            GameObject instance = o.instances[0];
            Assert.AreEqual(instance.name, "second_part_sphere_0");
            GameObject instance_child = Obj.getPrefabInstance(instance);
            Assert.IsNotNull(instance_child.GetComponent<SphereCollider>());
        }

        [Test]
        public void Sets_Up_CameraController() {
            Assert.IsNotNull(flybyController.cameraController);
            Assert.IsNotNull(flybyController.cameraController.camObj);
        }

        [Test]
        public void Loads_CameraAngles_From_Part_Json() {
            //first test part has two camera angles
            Assert.AreEqual(flybyController.cameraController.cameraAngles.Count, 2);
        }

        [Test]
        public void Does_Not_Load_Disabled_Obj() {
            triggerEvent("load_test_part2");
            Obj o = flybyController.currentPart.objs[2];
            Assert.AreEqual(o.id, "disabled_obj");
            triggerEvent("trigger_disabled_obj");
            Assert.AreEqual(o.instances.Count, 0);
        }

        [Test]
        public void Quick_Setup_With_Manual_Objs() {
            List<NullObj> objs = new List<NullObj>();
            objs.Add(new NullObj());
            objs[0].id = "new_obj";
            flybyController.quickSetup(objs);
            Assert.AreEqual(flybyController.currentPart.objs.Count, 1);
            Assert.AreEqual(flybyController.currentPart.objs[0].id, "new_obj");
        }

        [Test]
        public void Can_Trigger_A_Manually_Loaded_Obj() {
            List<NullObj> objs = new List<NullObj>();
            objs.Add(new NullObj());
            objs[0].id = "manual_obj";
            objs[0].trigger = "manual_trigger";
            flybyController.quickSetup(objs);
            triggerEvent("manual_trigger");
            Assert.AreEqual(flybyController.currentPart.objs.Count, 1);
            Assert.AreEqual(flybyController.currentPart.objs[0].instances.Count, 1);
            GameObject instance = flybyController.currentPart.objs[0].instances[0];
            Assert.AreEqual(instance.transform.position, Vector3.zero);
        }

        [Test]
        public void Can_Manually_PreRender_An_Obj() {
            List<NullObj> objs = new List<NullObj>();
            objs.Add(new NullObj());
            objs[0].id = "manual_prerendered_obj";
            objs[0].preRender = "true";
            flybyController.quickSetup(objs);
            Assert.AreEqual(flybyController.currentPart.objs.Count, 1);
            GameObject instance = flybyController.currentPart.objs[0].instances[0];
            Assert.AreEqual(instance.transform.position, Vector3.zero);
        }

        private Color getColor(GameObject instance) {
            return instance.GetComponent<Renderer>().material.GetColor(colorProperty);
        }

    }
}