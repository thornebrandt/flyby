using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyByBlendShapeTests : PlayModeTests
    {
        private ActiveBlendShape activeBlendShape;
        private SkinnedMeshRenderer rend;

        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
            // why do we need the below ones? 
            // investigate during compilation. 
            activeBlendShape.Kill();
        }

        [UnityTest]
        public IEnumerator BlendShape_Is_Not_Null()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            yield return 0;
            Assert.IsNotNull(activeBlendShape);
        }

        [UnityTest]
        public IEnumerator Intro_Works_On_Blendshape()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.start = new Vec(0, 0, 0);
            a.value = new Vec(100, 0, 0);
            a.startTime = 0.1f;
            a.endTime = -1;
            triggerEvent("intro");
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            yield return new WaitForSecondsOrTrue(1, () => a.introduced);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
        }

        [UnityTest]
        public IEnumerator End_Time_Works_With_Sustain_For_Blendshapes()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro_sustain";
            initializeObjs();
            a.startTime = 0.1f;
            a.endTime = 0.1f;
            a.start = new Vec(0, 0, 0);
            a.value = new Vec(100, 0, 0);
            a.end = new Vec(0, 0, 0);
            TriggerObj trigger_intro_sustain = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
            };
            TriggerObj trigger_intro_sustain_off = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
                noteOff = true
            };
            triggerEvent(trigger_intro_sustain);
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            yield return new WaitForSecondsOrTrue(1, () => a.introduced);
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            triggerEvent(trigger_intro_sustain_off);
            yield return new WaitForSecondsOrTrue(0.3f, () => a.ended);
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
        }

        [UnityTest]
        public IEnumerator Target_Animation_Called_From_ActiveTrigger_And_Returns_To_Zero()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_blendshape";
            a.target = new Vec(100, 0, 0);
            a.onTime = 0.1f;
            a.offDelay = 0.1f;
            a.offTime = 0.1f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            triggerEvent("trigger_blendshape");
            yield return new WaitForSeconds(0.1f);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            yield return new WaitForSeconds(0.2f);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
        }

        [UnityTest]
        public IEnumerator Two_Animations_Can_Be_Triggered_Simultaneously()
        {
            //testing chords. 
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_blendshape_1";
            a.target = new Vec(100, 0, 0);
            a.onTime = 0.1f;
            a.offDelay = 0.1f;
            a.offTime = 0.1f;
            a.triggers = new List<SecondaryActiveTrigger>{
                new SecondaryActiveTrigger{
                    trigger="trigger_blendshape_2",
                    target = new Vec(100, 0, 0),
                    onTime = 0.1f,
                    offDelay = 0.1f,
                    propertyIndex = 1,
                },
            };
            activeBlendShape.assignSecondaryTriggers(a);
            a = activeBlendShape.triggers[0]; //have to redefine bc it's been copied. 
            ActiveTrigger a2 = activeBlendShape.triggers[1];
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            triggerEvent("trigger_blendshape_1");
            yield return new WaitForSeconds(0.1f);
            triggerEvent("trigger_blendshape_2");
            yield return new WaitForSecondsOrTrue(1, () => rend.GetBlendShapeWeight(0) == 100);
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            //assert that second blendshape was also fired. 
            yield return new WaitForSecondsOrTrue(1, () => rend.GetBlendShapeWeight(1) == 100);
            Assert.AreEqual(rend.GetBlendShapeWeight(1), 100);
        }

        [UnityTest]
        public IEnumerator Target_Animation_Can_Be_ReTriggered_And_Returns_To_Zero()
        {
            //target animation should retrigger from where it left off. 
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            obj1.preRender = "true";
            initializeObjs();
            a.trigger = "trigger_blendshape";
            a.target = new Vec(100, 0, 0);
            a.onTime = 0.1f;
            a.offDelay = 0.1f;
            a.offTime = 0.3f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            triggerEvent("trigger_blendshape");
            yield return new WaitForSeconds(0.1f);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            yield return new WaitForSeconds(0.2f); //offDelay plus some offTime
            Assert.Less(rend.GetBlendShapeWeight(0), 100);
            Assert.Greater(rend.GetBlendShapeWeight(0), 0);
            //retrigger here. 
            triggerEvent("trigger_blendshape");
            yield return new WaitForSecondsOrTrue(0.2f, () => rend.GetBlendShapeWeight(0) == 100);
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            yield return new WaitForSecondsOrTrue(1, () => rend.GetBlendShapeWeight(0) == 0);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
        }

        [UnityTest]
        public IEnumerator Target_Blendhape_Can_Sustain_And_Return()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_sustain";
            a.target = new Vec(100, 0, 0);
            a.onTime = 0.1f;
            a.offTime = 0.1f;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            TriggerObj trigger_sustain = new TriggerObj
            {
                eventName = "trigger_sustain",
                sustain = true,
            };
            TriggerObj trigger_sustain_off = new TriggerObj
            {
                eventName = "trigger_sustain",
                sustain = true,
                noteOff = true
            };
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            triggerEvent(trigger_sustain);
            yield return new WaitForSecondsOrTrue(1, () => a.on);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            //trigger stays on . 
            yield return new WaitForSeconds(0.5f);
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            triggerEvent(trigger_sustain_off);
            yield return new WaitForSecondsOrTrue(1, () => rend.GetBlendShapeWeight(0) == 0);
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
        }

        [UnityTest]
        public IEnumerator Target_Blendhape_With_Sustain_Can_Be_ReTriggered_Without_Popping_Lower()
        {
            //this is testing a specific bug. 
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_sustain";
            a.target = new Vec(100, 0, 0);
            a.onTime = 1.0f; //seems to only happen on long animations
            a.offTime = 1.0f;
            TriggerObj trigger_sustain = new TriggerObj
            {
                eventName = "trigger_sustain",
                sustain = true,
            };
            TriggerObj trigger_sustain_off = new TriggerObj
            {
                eventName = "trigger_sustain",
                sustain = true,
                noteOff = true
            };
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            triggerEvent(trigger_sustain);
            yield return new WaitForSeconds(0.3f);
            //trigger stays on . 
            float blendShapeWeight = rend.GetBlendShapeWeight(0);
            triggerEvent(trigger_sustain_off);
            yield return new WaitForSeconds(0.3f);
            Assert.Less(rend.GetBlendShapeWeight(0), blendShapeWeight);
            triggerEvent(trigger_sustain);

            yield return new WaitForSeconds(0.01f); //seems to only happen on a quick trigger off. 
            //give time to get even higher. 
            triggerEvent(trigger_sustain_off);
            blendShapeWeight = rend.GetBlendShapeWeight(0);
            yield return new WaitForSecondsOrTrue(0.3f, () => rend.GetBlendShapeWeight(0) > blendShapeWeight / 2);
            // wait three frames. 
            //should continue from where it left off, not less than halfway.  ( observed popping bug on retriggering sustain )
            Assert.Greater(rend.GetBlendShapeWeight(0), blendShapeWeight / 2);
        }

        [UnityTest]
        public IEnumerator Target_Blendshape_Can_Be_ReTriggered_Without_Starting_At_Zero()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_blendshape";
            a.target = new Vec(100, 0, 0);
            a.onTime = 0.1f;
            a.offDelay = 0.1f;
            a.offTime = 0.2f;
            a.onTime = 0.2f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            triggerEvent("trigger_blendshape");
            yield return new WaitForSecondsOrTrue(0.3f, () => rend.GetBlendShapeWeight(0) == 100);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            //wait a few frames. 
            yield return new WaitForSecondsOrTrue(1, () => rend.GetBlendShapeWeight(0) < 90);
            Assert.Less(rend.GetBlendShapeWeight(0), 90);
            yield return 0;
            yield return 0;
            float blendShapeWeight = rend.GetBlendShapeWeight(0);
            triggerEvent("trigger_blendshape");
            yield return new WaitForSeconds(0.1f);
            Assert.Greater(rend.GetBlendShapeWeight(0), blendShapeWeight);
        }

        [UnityTest]
        public IEnumerator Secondary_Trigger_Can_Target_A_Different_Blendshape()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.triggers = new List<SecondaryActiveTrigger>{
                new SecondaryActiveTrigger {
                    trigger = "trigger_secondary_blendshape",
                    target = new Vec(100, 0, 0),
                    onTime = 0.1f,
                    offDelay = 0.1f,
                    propertyIndex = 1,
                }
            };
            activeBlendShape.assignSecondaryTriggers(a);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(1), 0);
            triggerEvent("trigger_secondary_blendshape");
            yield return new WaitForSeconds(0.1f);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(1), 100);
        }


        [UnityTest]
        public IEnumerator Multiple_Secondary_Blendshapes_Can_Be_Triggered()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.triggers = new List<SecondaryActiveTrigger>{
                new SecondaryActiveTrigger {
                    trigger = "trigger_secondary_blendshape_1",
                    target = new Vec(100, 0, 0),
                    onTime = 0.1f,
                    offDelay = 0.1f,
                    propertyIndex = 0,
                },
                new SecondaryActiveTrigger {
                    trigger = "trigger_secondary_blendshape_2",
                    target = new Vec(100, 0, 0),
                    onTime = 0.1f,
                    offDelay = 0.1f,
                    propertyIndex = 1,
                }
            };
            activeBlendShape.assignSecondaryTriggers(a);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            triggerEvent("trigger_secondary_blendshape_1");
            yield return new WaitForSeconds(0.1f);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
            triggerEvent("trigger_secondary_blendshape_2");
            yield return new WaitForSeconds(0.1f);
            yield return 0;
            Assert.Less(rend.GetBlendShapeWeight(0), 100);
            Assert.Greater(rend.GetBlendShapeWeight(1), 0);
        }


        [UnityTest]
        public IEnumerator Knob_Trigger_Works_For_BlendShape()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0;
            a.knob = new Vec
            {
                x = 0,
                x2 = 100
            };
            a.trigger = "blendshape_knob";
            Trigger t = new Trigger
            {
                eventName = "blendshape_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t, 1);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 100);
        }

        [UnityTest]
        public IEnumerator Start_Trigger_Property_Works_For_Knob()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0;
            a.knob = new Vec
            {
                x = 0,
                x2 = 100
            };
            a.triggerProperty = new TriggerProperty
            {
                start = 0.5f
            };
            a.trigger = "blendshape_knob";
            Trigger t = new Trigger
            {
                eventName = "blendshape_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 50);
        }

        [UnityTest]
        public IEnumerator Can_Cache_Blendshape_Knob()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            obj1.numInstances = 2;
            obj1.activeType = ActiveType.Next;
            Trigger t = new Trigger
            {
                eventName = "test_cached_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            a.knob = new Vec
            {
                x = 0,
                x2 = 100
            };
            a.startTime = 0;
            a.trigger = "test_cached_knob";

            GameObject instance2 = getPrefabInstanceByIndex(1);
            SkinnedMeshRenderer rend2 = instance2.GetComponent<SkinnedMeshRenderer>();
            //---- have to manually assign the triggers. 
            ActiveBlendShape activeBlendShape2 = instance2.GetComponent<ActiveBlendShape>();
            activeBlendShape2.triggers = new List<ActiveTrigger>();
            ActiveTrigger a2 = new ActiveTrigger();
            a2.knob = new Vec
            {
                x = 0,
                x2 = 100
            };
            a2.startTime = 0;
            a2.trigger = "test_cached_knob";
            activeBlendShape2.triggers.Add(a2);
            activeBlendShape2.setupActiveTriggers(o);

            triggerKnob(t, 0.5f);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 50);

            //----cache remains 
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend2.GetBlendShapeWeight(0), 50);
        }

        [UnityTest]
        public IEnumerator Blendshape_Knob_Value_Is_Affected_By_Trigger_Property_Lerp_Speed()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0;
            a.knob = new Vec
            {
                x = 0,
                x2 = 100
            };
            a.trigger = "blendshape_knob";
            Trigger t = new Trigger
            {
                eventName = "blendshape_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            a.triggerProperty = new TriggerProperty
            {
                start = 0,
                lerpSpeed = 1.0f
            };
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);
            triggerKnob(t, 1);
            yield return new WaitForSecondsOrTrue(1, () => rend.GetBlendShapeWeight(0) > 0);
            Assert.Less(rend.GetBlendShapeWeight(0), 100);
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            flybyController.quickSetup(objs);
            instance = getFirstPrefabInstance();
            activeBlendShape = instance.GetComponent<ActiveBlendShape>();
            rend = instance.GetComponent<SkinnedMeshRenderer>();
            activeBlendShape.triggers = new List<ActiveTrigger>();
            a = new ActiveTrigger();
            activeBlendShape.triggers.Add(a);
            o = flybyController.currentPart.objs[0];
            activeBlendShape.setupActiveTriggers(o);

        }
    }
}
