using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests {
    public class FlyByParentTests : PlayModeTests {
        NullObj parentObj;
        NullObj childObj;
        private GameObject parentInstance;
        private GameObject childInstance;
        private ActiveParent activeParent;
        private ActivePosition activePosition;

        [SetUp]
        public void Setup() {
            this.setup();
            parentObj = new NullObj();
            childObj = new NullObj();
            triggers = new List<Trigger>();
        }

        [TearDown]
        public void Teardown() {
            flybyController.killAll();
        }

        [Test]
        public void ActiveParent_Is_Not_Null() {
            parentObj = createBlankObj();
            parentObj.trigger = "intro"; //event for initialize.
            parentObj.triggerParent = new ActiveTrigger { };
            childObj = createBlankObj(); //for child test. 
            childObj.triggerParent = new ActiveTrigger { };
            initializeObjs();
            Assert.IsNotNull(childObj.triggerParent);
            Assert.IsNotNull(activeParent);
        }

        [UnityTest]
        public IEnumerator Can_Parent_To_Obj_By_Name() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                valueString = "parentObj",
                startTime = 0
            };
            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            triggerEvent("intro_child");
            yield return new WaitForSecondsOrTrue(0.3f, () => activeParent.introduced);
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
        }


        [UnityTest]
        public IEnumerator Can_Parent_Immediately_Without_StartTime() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                valueString = "parentObj",
            };
            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            triggerEvent("intro_child");
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
        }

        [UnityTest]
        public IEnumerator Two_PreRendered_Objs_Can_Be_Parented() {
            //make sure to use a startTime as a "triggerDelay"
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.preRender = "true";
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.triggerParent = new ActiveTrigger {
                valueString = "parentObj",
                startTime = 0,
            };
            childObj.preRender = "true";
            initializeObjs();
            yield return 0;
            Assert.AreEqual(childInstance.transform.position, Vector3.zero);
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
        }

        [UnityTest]
        public IEnumerator Child_Resets_Properly() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            parentObj.triggerSpeed = new ActiveTrigger {
                value = new Vec {
                    x = 1
                }
            };
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startTime = 0.1f,
                startString = "root",
                valueString = "parentObj"
            };
            initializeObjs();
            triggerEvent("intro_parent");
            //let it move for a while. 
            yield return new WaitForSeconds(0.2f);
            Assert.Greater(parentInstance.transform.position.x, 0);
            triggerEvent("intro_child");
            Assert.IsTrue(Math.RoughEquals(childInstance.transform.position, Vector3.zero));
            float savedX = childInstance.transform.position.x;
            yield return new WaitForSeconds(0.2f);
            //make sure child is following parent.
            Assert.Greater(childInstance.transform.position.x, savedX);
            //retrigger the child. 
            triggerEvent("intro_child");
            Assert.AreEqual(childInstance.transform.position.x, 0);
            //it should still be in the center.
        }

        [UnityTest]
        public IEnumerator Parent_Does_Not_Have_Child_On_Respawn() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startTime = 0,
                valueString = "parentObj"
            };
            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            triggerEvent("intro_child");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeParent.introduced);
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
            yield return 0;
            triggerEvent("intro_parent");
            Assert.AreEqual(childInstance.transform.parent, null);
            Assert.AreEqual(childInstance.transform.position, flybyController.currentPart.objs[0].nullVector);
        }

        [UnityTest]
        public IEnumerator Parent_Retains_Prefab_Children_But_Not_Dynamic_Children_On_Respawn() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            parentObj.bundle = "TestPrefabs";
            parentObj.asset = "ParentWithChildren";
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startTime = 0,
                valueString = "parentObj"
            };
            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            yield return new WaitForSecondsOrTrue(0.1f, () => activeParent.introduced);
            GameObject parentPrefabInstance = Obj.getPrefabInstance(parentInstance);
            Assert.AreEqual(parentPrefabInstance.transform.GetChild(0).name, "originalChild");
            triggerEvent("intro_child");
            yield return 0;
            Assert.AreEqual(parentPrefabInstance.transform.GetChild(2).name, "thirdChild");
            triggerEvent("intro_parent");
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent, null);
            Assert.AreEqual(childInstance.transform.position, flybyController.currentPart.objs[0].nullVector);
            Assert.AreEqual(parentPrefabInstance.transform.childCount, 3); //original child and secondChild
            Assert.AreEqual(parentPrefabInstance.transform.GetChild(0).name, "originalChild");
        }

        [UnityTest]
        public IEnumerator Child_Starts_Relative_To_Parent_If_Start_Value() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            parentObj.origin = new Vec(1, 0, 0);
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startString = "parentObj",
            };
            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            Assert.AreEqual(parentInstance.transform.position, new Vector3(1, 0, 0));
            triggerEvent("intro_child");
            yield return 0;
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
            Assert.AreEqual(childInstance.transform.position, new Vector3(1, 0, 0));
        }

        [UnityTest]
        public IEnumerator If_Value_Parent_Is_Empty_It_Parents_To_Scene() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            parentObj.origin = new Vec(1, 0, 0);
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startString = "parentObj",
                startTime = 0.1f,
                startDelay = 0.1f,
                valueString = ""
            };
            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            triggerEvent("intro_child");
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
            yield return new WaitForSecondsOrTrue(0.3f, () => childInstance.transform.parent == null);
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent, null);
        }

        [UnityTest]
        public IEnumerator Child_Can_Have_An_End_Parent() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            parentObj.origin = new Vec(1, 0, 0);
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startTime = 0,
                endTime = 0.1f,
                endString = "parentObj",
            };
            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            triggerEvent("intro_child");
            Assert.AreEqual(childInstance.transform.parent, null);
            yield return new WaitForSecondsOrTrue(0.3f, () => activeParent.ended);
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
        }

        [UnityTest]
        public IEnumerator Sustain_Intro_Holds_Value_Until_Exit() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";

            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "introSustain";
            childObj.triggerParent = new ActiveTrigger {
                startTime = 0.3f,
                endDelay = 0.1f,
                endTime = 0.05f,
                startString = "parentObj",
                endString = "parentObj2"
            };

            //DO NOT MOVE UP -getChildInstance is getting objs[1]
            NullObj parentObj2 = createBlankObj();
            parentObj2.id = "parentObj2";
            parentObj2.trigger = "intro_parent2";


            TriggerObj sustainTriggerObj = new TriggerObj {
                eventName = "introSustain",
                sustain = true
            };
            TriggerObj noteOffTriggerObj = new TriggerObj {
                eventName = "introSustain",
                sustain = true,
                noteOff = true
            };

            initializeObjs();
            triggerEvent("intro_parent");
            triggerEvent("intro_parent2");
            yield return 0;
            triggerEvent(sustainTriggerObj);
            yield return 0;
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
            yield return new WaitForSecondsOrTrue(0.3f, () => activeParent.introduced);
            childInstance = getChildInstance();
            Assert.IsNull(childInstance.transform.parent);
            yield return new WaitForSeconds(0.3f);
            //sustains and stays at root. 
            childInstance = getChildInstance();
            Assert.AreEqual(childInstance.transform.parent, null);
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSeconds(0.4f);
            //yield return new WaitForSecondsOrTrue(0.3f, () => childInstance.transform.parent.name == "parentObj2_0_prefab");
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj2_0_prefab");
        }

        [UnityTest]
        public IEnumerator Can_Trigger_New_Parent() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            parentObj.origin = new Vec(1, 0, 0);
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startTime = 0,
                targetString = "parentObj",
                trigger = "triggerNewParent",
                onTime = 0f,
                offTime = -1
            };
            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            triggerEvent("intro_child");
            Assert.AreEqual(childInstance.transform.parent, null);
            triggerEvent("triggerNewParent");
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Multiple_Secondary_Parents() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            parentObj.origin = new Vec(1, 0, 0);
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startTime = 0,
                targetString = "parentObj",
                trigger = "triggerNewParent",
                onTime = 0f,
                offTime = -1,
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        targetString = "parentObj2",
                        trigger = "triggerNewParent2",
                        onTime = 0f,
                        offTime = -1,
                    }
                },
            };

            NullObj parentObj2 = createBlankObj();
            parentObj2.id = "parentObj2";
            parentObj2.trigger = "intro_parent";

            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            triggerEvent("intro_child");
            Assert.AreEqual(childInstance.transform.parent, null);
            triggerEvent("triggerNewParent");
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
            triggerEvent("triggerNewParent2");
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj2_0_prefab");
        }

        [UnityTest]
        public IEnumerator Can_Parent_To_Camera() {
            parentObj = createBlankObj();
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                valueString = "cam",
                startTime = 0
            };

            initializeObjs();
            triggerEvent("intro_child");
            yield return 0;
            yield return 0;
            Assert.AreEqual(childInstance.transform.parent.name, "3D1_Camera");
        }

        [UnityTest]
        public IEnumerator Can_Trigger_New_Parent_With_Sustain() {
            parentObj = createBlankObj();
            parentObj.id = "parentObj";
            parentObj.trigger = "intro_parent";
            parentObj.origin = new Vec(1, 0, 0);
            childObj = createBlankObj();
            childObj.id = "childObj";
            childObj.trigger = "intro_child";
            childObj.triggerParent = new ActiveTrigger {
                startTime = 0,
                targetString = "parentObj",
                trigger = "triggerParentSustain",
                onTime = 0f,
                offTime = 0f,
            };

            TriggerObj sustainTriggerObj = new TriggerObj {
                eventName = "triggerParentSustain",
                sustain = true
            };
            TriggerObj noteOffTriggerObj = new TriggerObj {
                eventName = "triggerParentSustain",
                sustain = true,
                noteOff = true
            };

            initializeObjs();
            triggerEvent("intro_parent");
            yield return 0;
            triggerEvent("intro_child");
            Assert.AreEqual(childInstance.transform.parent, null);
            triggerEvent(sustainTriggerObj);
            yield return new WaitForSecondsOrTrue(0.1f, () => activeParent.on);
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
            yield return new WaitForSeconds(0.3f);
            //still has parent. 
            Assert.AreEqual(childInstance.transform.parent.name, "parentObj_0_prefab");
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(0.1f, () => !activeParent.on);
            Assert.AreEqual(childInstance.transform.parent, null);
        }


        [UnityTest]
        public IEnumerator Can_Reference_Child_Instances_With_Child_Index() {
            parentObj = createBlankObj();
            childObj = createBlankObj(); // not actually needed - to keep test from erroring. 
            parentObj.bundle = "TestPrefabs";
            parentObj.id = "wtf";
            parentObj.asset = "ParentWithChildren"; //has a child cube at (0, 1, 0)
            parentObj.trigger = "intro";
            parentObj.triggerPosition = new ActiveTrigger {
                startTime = 0,
                trigger = "trigger_child_position",
                childIndex = 0,
                onTime = 0.1f,
                offTime = -1,
                target = new Vec(1, 0, 0)
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            GameObject parentPrefabInstance = Obj.getPrefabInstance(parentInstance);
            GameObject childInstance = parentPrefabInstance.transform.GetChild(0).gameObject;
            Assert.AreEqual(childInstance.name, "originalChild");
            Assert.AreEqual(childInstance.transform.position, new Vector3(0, 0, 0)); //it affects the original position.
            triggerEvent("trigger_child_position");
            //yield return new WaitForSecondsOrTrue(0.3f, () => parentObj.triggerPosition.on == true);
            yield return new WaitForSeconds(0.3f);
            yield return 0;
            Assert.AreEqual(childInstance.transform.position, new Vector3(1, 0, 0));
        }


        [UnityTest]
        public IEnumerator Child_Instances_Referenced_With_Child_Index_Can_Use_Presets() {
            parentObj = createBlankObj();
            childObj = createBlankObj(); // not actually needed - to keep test from erroring. 
            parentObj.bundle = "TestPrefabs";
            parentObj.id = "wtf";
            parentObj.asset = "ParentWithChildren"; //has a child cube at (0, 1, 0)
            parentObj.trigger = "intro";
            //we are also testing if parent trigger can be empty. 
            parentObj.triggerPosition = new ActiveTrigger {
                triggers = new List<SecondaryActiveTrigger> {
                    new SecondaryActiveTrigger{
                        id = "trigger_child_position",
                        trigger = "trigger_child_position",
                        startTime = 0,
                        childIndex = 0,
                        onTime = 0.1f,
                        offTime = -1,
                        target = new Vec(1, 0, 0)
                    },
                    new SecondaryActiveTrigger{
                        id = "second_trigger",
                        preset = "trigger_child_position",
                        childIndex = 1
                    }
                },
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            GameObject parentPrefabInstance = Obj.getPrefabInstance(parentInstance);
            GameObject childInstance = parentPrefabInstance.transform.GetChild(0).gameObject;
            Assert.AreEqual(childInstance.name, "originalChild");
            Assert.AreEqual(childInstance.transform.position, new Vector3(0, 0, 0)); //it affects the original position.
            triggerEvent("trigger_child_position");
            //yield return new WaitForSecondsOrTrue(0.3f, () => parentObj.triggerPosition.on == true);
            yield return new WaitForSeconds(0.3f);
            yield return 0;
            Assert.AreEqual(childInstance.transform.position, new Vector3(1, 0, 0));
            //and the other one has moved as well. 
            GameObject childInstance2 = parentPrefabInstance.transform.GetChild(1).gameObject;
            Assert.AreEqual(childInstance2.transform.position, new Vector3(1, 0, 0));
        }




        public override void initializeObjs() {
            base.initializeObjs();
            childInstance = getChildInstance();
            parentInstance = getParentInstance();
            activeParent = getChildInstance().GetComponent<ActiveParent>();
        }

        private GameObject getParentInstance() {
            return flybyController.currentPart.objs[0].instances[0];
        }

        private GameObject getChildInstance() {
            return flybyController.currentPart.objs[1].instances[0];
        }

    }

}

