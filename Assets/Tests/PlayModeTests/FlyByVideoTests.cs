using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Video;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests {
    public class FlyByVideoTests {
        FlyByController flybyController;
        GameObject go;
        GameObject camObj;
        AssetController assetController;
        List<NullObj> objs;
        List<Trigger> triggers;
        NullObj obj1;
        private GameObject instance;
        private ActiveVideo activeVideo;
        private VideoPlayer videoPlayer;
        ActiveTrigger a;
        Obj o;

        [SetUp]
        public void Setup() {
            go = GameObject.Find("FlyByController");
            if (go == null) {
                go = new GameObject("FlyByController");
            }
            flybyController = go.GetComponent<FlyByController>();
            if (flybyController == null) {
                flybyController = go.AddComponent<FlyByController>();
            }

            flybyController.configPath = "config.sample.json"; //empty
            if (assetController == null) {
                AssetController assetController = new AssetController();
                assetController.prefabCache = new Dictionary<string, GameObject>();
                camObj = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");

            }

            flybyController.camObj = camObj;
            KeyboardController keyboardController = go.GetComponent<KeyboardController>();
            if (keyboardController == null) {
                go.AddComponent<KeyboardController>();
                go.AddComponent<MidiController>();
                go.AddComponent<ActiveBehaviourController>();
            }
            flybyController.initialize();
            objs = new List<NullObj>();
            NullObj obj1 = new NullObj();
            triggers = new List<Trigger>();
        }


        [TearDown]
        public void Teardown() {
            flybyController.killAll();
            // why do we need the below ones? 
            // investigate during compilation. 
            activeVideo.Kill();
            activeVideo.killTweens();
        }

        [UnityTest]
        public IEnumerator Animator_Is_Not_Null() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VideoCube";
            obj1.trigger = "intro";
            initializeObjs();
            yield return 0;
            Assert.IsNotNull(activeVideo);
            Assert.IsNotNull(videoPlayer);
        }

        [UnityTest]
        public IEnumerator Can_Read_Clip_Name() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VideoCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.1f;
            activeVideo.setupActiveTriggers(o);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(videoPlayer.clip.name, "countdown");
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Intro_And_Current_clip() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VideoCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.2f;
            activeVideo.setupActiveTriggers(o);
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => videoPlayer.clip.name == "countdown");
            Assert.AreEqual(videoPlayer.clip.name, "countdown");
            yield return new WaitForSeconds(0.25f);
            Assert.AreEqual(videoPlayer.clip.name, "swing");
        }

        [UnityTest]
        public IEnumerator If_StartTime_Is_Zero_Video_Starts_With_ValueVideo() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VideoCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0;
            activeVideo.setupActiveTriggers(o);
            yield return 0;
            triggerEvent("intro");
            Assert.AreEqual(videoPlayer.clip.name, "swing");
        }

        [UnityTest]
        public IEnumerator Triggers_End_Video() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VideoCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0;
            a.endTime = 0.1f;
            activeVideo.setupActiveTriggers(o);
            yield return 0;
            triggerEvent("intro");
            Assert.AreEqual(videoPlayer.clip.name, "swing");
            yield return new WaitForSeconds(0.2f);
            Assert.AreEqual(videoPlayer.clip.name, "countdown");
        }

        [UnityTest]
        public IEnumerator Can_Trigger_TargetVideo() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VideoCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0;
            a.trigger = "triggerVideo";
            //a.trigger = "triggerClip"; not used yet. 
            activeVideo.setupActiveTriggers(o);
            yield return 0;
            triggerEvent("intro");
            Assert.AreEqual(videoPlayer.clip.name, "swing");
            yield return 0;
            triggerEvent("triggerVideo");
            yield return 0;
            Assert.AreEqual(videoPlayer.clip.name, "Unitylogo");
        }

        private void initializeObjs() {
            flybyController.quickSetup(objs);
            instance = getFirstPrefabInstance();
            activeVideo = instance.GetComponent<ActiveVideo>();
            videoPlayer = instance.GetComponent<VideoPlayer>();
            activeVideo.triggers = new List<ActiveTrigger>();
            a = new ActiveTrigger();
            activeVideo.triggers.Add(a);
            o = flybyController.currentPart.objs[0];
        }

        private NullObj createBlankObj() {
            objs.Add(new NullObj());
            return objs[objs.Count - 1];
        }

        private void triggerEvent(TriggerObj triggerObj) {
            flybyController.triggerController.triggerSources[0].trigger(triggerObj);
        }

        private void triggerEvent(string eventName) {
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = eventName;
            flybyController.triggerController.triggerSources[0].trigger(triggerObj);
        }

        private void triggerKnob(float value) {
            //triggers knob 0, channel 0. TODO - extend if you need more channels for these tests. 
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, value);
        }

        private void triggerKnob(Trigger t, float value) {
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(t.channel, t.knob, value);
        }

        private void addTrigger(Trigger t) {
            triggers.Add(t);
        }

        private void registerTriggers() {
            flybyController.registerTriggers(triggers);
        }


        private GameObject getFirstPrefabInstance() {
            return Obj.getPrefabInstance(getFirstInstance());
        }

        private GameObject getPrefabInstanceByIndex(int i) {
            return Obj.getPrefabInstance(getInstanceByIndex(i));
        }

        private GameObject getFirstInstance() {
            return flybyController.currentPart.objs[0].instances[0];
        }

        private GameObject getInstanceByIndex(int i) {
            return flybyController.currentPart.objs[0].instances[i];
        }

    }
}