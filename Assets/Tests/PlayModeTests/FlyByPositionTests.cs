using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Active;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyByPositionTests : PlayModeTests
    {
        private ActivePosition activePosition;

        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            activePosition = getFirstPopupInstance().GetComponent<ActivePosition>();
        }

        [TearDown]
        public void Teardown()
        {
            this.teardown();
        }

        [Test]
        public void Trigger_Position_Has_Start_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                start = new Vec(-1, 0, 0)
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position.x, -1);
        }

        [UnityTest]
        public IEnumerator End_Position_Acts_As_PopTo_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                end = new Vec(-1, 0, 0),
                endTime = 0.3f,
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position.x, 0);
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.x == -1);
            Assert.AreEqual(instance.transform.position.x, -1);
        }

        [UnityTest]
        public IEnumerator Trigger_Position_Updates_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger();
            obj1.triggerPosition.trigger = "triggerPosition";
            obj1.triggerPosition.target = new Vec(-1, 0, 0);
            obj1.triggerPosition.startTime = 0;
            obj1.triggerPosition.onTime = 0.1f;
            obj1.triggerPosition.offDelay = 0.1f;
            obj1.triggerPosition.offTime = 0.1f;
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position.x, 0);
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position.x, -1);
            yield return new WaitForSecondsOrTrue(1, () => !activePosition.on);
            Assert.AreEqual(instance.transform.position.x, 0);
        }

        [UnityTest]
        public IEnumerator Target_Position_Is_Affected_By_TriggerProperty_Max()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            Vec targetVec = new Vec(10, 10, 10);
            targetVec.max = new Vector3(0, -999, 1);
            obj1.triggerPosition = new ActiveTrigger
            {
                trigger = "triggerPosition",
                target = targetVec,
                onTime = 0.1f,
                offTime = -1
            };
            initializeObjs();
            triggerEvent("intro");
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 10, 1));
            yield return 0;
        }


        [UnityTest]
        public IEnumerator Two_Trigger_Position_Updates_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger();
            obj1.triggerPosition.triggers = new List<SecondaryActiveTrigger>();
            obj1.triggerPosition.triggers.Add(
                new SecondaryActiveTrigger
                {
                    trigger = "triggerPosition1",
                    target = new Vec(-1, 0, 0),
                    startTime = 0,
                    onTime = 0.1f,
                    offTime = -1
                }
            );
            obj1.triggerPosition.triggers.Add(
                new SecondaryActiveTrigger
                {
                    trigger = "triggerPosition2",
                    target = new Vec(0, 1, 0),
                    startTime = 0,
                    onTime = 0.1f,
                    offTime = -1
                }
            );
            initializeObjs();
            triggerEvent("intro");
            triggerEvent("triggerPosition1");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.x == -1);
            Assert.AreEqual(instance.transform.position.x, -1);
            triggerEvent("triggerPosition2");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.y == 1);
            Assert.AreEqual(instance.transform.position.y, 1);
        }


        [UnityTest]
        public IEnumerator Sustain_Works_For_Trigger_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger();
            obj1.triggerPosition.trigger = "triggerPosition";
            obj1.triggerPosition.target = new Vec(0, 1, 0);
            obj1.triggerPosition.startTime = 0;
            obj1.triggerPosition.onTime = 0.1f;
            obj1.triggerPosition.offDelay = 0.1f;
            obj1.triggerPosition.offTime = 0.1f;
            TriggerObj t = new TriggerObj
            {
                sustain = true,
                eventName = "triggerPosition"
            };
            initializeObjs();
            triggerEvent("intro");
            triggerEvent(t);
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.y == 1);
            Assert.AreEqual(instance.transform.position.y, 1);
            yield return new WaitForSecondsOrTrue(0.5f, () => instance.transform.position.y < 1); //should not fire with sustain on. 
            Assert.AreEqual(instance.transform.position.y, 1);
            t.noteOff = true;
            activePosition.TriggerOff(t, obj1.triggerPosition, o);
            yield return new WaitForSecondsOrTrue(1, () => !activePosition.on);
            Assert.AreEqual(go.transform.position.y, 0);
        }

        [UnityTest]
        public IEnumerator Obj_Returns_To_Origin_After_Several_Triggers_If_Offtime()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger();
            obj1.triggerPosition.trigger = "triggerPosition";
            obj1.triggerPosition.value = new Vec();
            obj1.triggerPosition.target = new Vec(-1, 0, 0);
            obj1.triggerPosition.startTime = 0;
            obj1.triggerPosition.onTime = 1;
            obj1.triggerPosition.offTime = -1;
            obj1.triggerPosition.triggers = new List<SecondaryActiveTrigger>();
            SecondaryActiveTrigger secondaryTrigger = new SecondaryActiveTrigger();
            secondaryTrigger.target = new Vec(0, 1, 0);
            //secondaryTrigger.target.relative = true;
            secondaryTrigger.onTime = 0.1f;
            secondaryTrigger.offDelay = 0.1f;
            secondaryTrigger.offTime = 0.111f;
            secondaryTrigger.trigger = "triggerPosition2";
            obj1.triggerPosition.triggers.Add(secondaryTrigger);
            initializeObjs();
            triggerEvent("intro");
            triggerEvent("triggerPosition");
            yield return new WaitForSeconds(0.5f);
            triggerEvent("triggerPosition2");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.y == 1);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.y == 0);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
        }

        [Test]
        public void Relative_Start_Is_Relative_To_Value()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger();
            obj1.triggerPosition.trigger = "triggerPosition";
            obj1.triggerPosition.start = new Vec(-1, 0, 0);
            obj1.triggerPosition.start.relative = true;
            obj1.triggerPosition.startTime = 0.1f;
            obj1.triggerPosition.value = new Vec(2, 2, 2);
            initializeObjs();
            triggerEvent("intro");
            triggerEvent("triggerPosition");
            Assert.AreEqual(instance.transform.position, new Vector3(1, 2, 2));
        }

        [UnityTest]
        public IEnumerator Trigger_Position_With_Negative_OffTime_Remains_In_Triggered_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger();
            obj1.triggerPosition.trigger = "triggerPosition";
            obj1.triggerPosition.target = new Vec(-1, 0, 0);
            obj1.triggerPosition.startTime = 0;
            obj1.triggerPosition.onTime = 0.1f;
            obj1.triggerPosition.offTime = -1;
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position.x, 0);
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position.x, -1);
            yield return new WaitForSecondsOrTrue(1, () => !activePosition.on);
            Assert.AreEqual(instance.transform.position.x, -1);
        }


        [UnityTest]
        public IEnumerator Cache_Knob_Trigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro"; //event for initialize. 
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                },
                activeType = ActiveType.Next,
                trigger = "test_knob"
            };
            Trigger t = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerKnob(1);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
        }

        [UnityTest]
        public IEnumerator Can_Cache_Multiple_Knobs()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro"; //event for initialize. 
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                },
                activeType = ActiveType.Next,
                trigger = "test_knob",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        knob = new Vec {
                            y = 0,
                            y2 = 1
                        },
                        trigger = "test_knob2"
                    },
                    new SecondaryActiveTrigger {
                        knob = new Vec {
                            z = 0,
                            z2 = 1
                        },
                        trigger = "test_knob3"
                    }
                }
            };
            Trigger t = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            Trigger t2 = new Trigger
            {
                eventName = "test_knob2",
                channel = 0,
                knob = 2
            };
            Trigger t3 = new Trigger
            {
                eventName = "test_knob3",
                channel = 0,
                knob = 3
            };
            addTrigger(t);
            addTrigger(t2);
            addTrigger(t3);
            registerTriggers();
            initializeObjs();
            triggerKnob(1);
            yield return 0;
            triggerKnob(t2, 1);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t3, 1);
            yield return 0;
            Assert.AreEqual(instance.transform.position, new Vector3(1, 1, 1));
        }

        [UnityTest]
        public IEnumerator ActiveType_All_Affects_All_Knobs()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.numInstances = 3;
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    y = 0,
                    y2 = 1
                },
                trigger = "test_knob",
                activeType = ActiveType.All
            };
            Trigger t = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            triggerKnob(1);
            yield return 0;
            GameObject instance2 = getPopupInstanceByIndex(1);
            Assert.AreEqual(instance.transform.position.y, 1);
            Assert.AreEqual(instance2.transform.position.y, 1);
        }

        [UnityTest]
        public IEnumerator Knob_Trigger_Works()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro"; //event for initialize. 
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 2
                },
                trigger = "test_knob"
            };
            Trigger t = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, 1);
            yield return 0;
            Assert.AreEqual(instance.transform.position, new Vector3(2, 0, 0));
        }

        [UnityTest]
        public IEnumerator Knob_Trigger_Works_With_Triggered_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro"; //event for initialize. 
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 2
                },
                target = new Vec
                {
                    y = 1
                },
                onTime = 0.1f,
                offTime = -1,
                startTime = 0,
                trigger = "test_knob"
            };
            Trigger t = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            triggerKnob(0.5f);
            triggerEvent("test_knob");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.y == 1);
            Assert.AreEqual(instance.transform.position, new Vector3(2, 1, 0));
        }

        [UnityTest]
        public IEnumerator Start_Knob_Value_Affects_Spawn_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                },
                startTime = 0,
                triggerProperty = new TriggerProperty
                {
                    start = 1
                },
                trigger = "test_knob"
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
        }

        [UnityTest]
        public IEnumerator Knob_Value_Is_Affected_By_Trigger_Property_Lerp_Speed()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                },
                startTime = 0,
                triggerProperty = new TriggerProperty
                {
                    start = 0,
                    lerpSpeed = 1 //just some small value. 
                },
                trigger = "test_knob"
            };
            Trigger t = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, new Vector3(0, 0, 0));
            triggerKnob(1.0f);
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.x > 0);
            Assert.Greater(instance.transform.position.x, 0);
            Assert.Less(instance.transform.position.x, 1.0);

            // Assert.AreEqual(instance.transform.position, new Vector3(0.5f, 0, 0));
        }

        [UnityTest]
        public IEnumerator Secondary_Knob_Trigger_Works_With_Knob_Trigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 2
                },
                startTime = 0,
                trigger = "primary_knob",
                triggers = new List<SecondaryActiveTrigger>{
                    {
                        new SecondaryActiveTrigger {
                            knob = new Vec {
                                y = 0,
                                y2 = 2
                            },
                            trigger = "secondary_knob"
                        }
                    }
                }
            };
            Trigger t = new Trigger
            {
                eventName = "primary_knob",
                channel = 0,
                knob = 1
            };
            Trigger t2 = new Trigger
            {
                eventName = "secondary_knob",
                channel = 0,
                knob = 2
            };
            addTrigger(t);
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t, 0.5f);
            triggerKnob(t2, 1);
            yield return 0;
            Assert.AreEqual(instance.transform.position, new Vector3(1, 2, 0));
        }

        [UnityTest]
        public IEnumerator Secondary_Knob_Trigger_Works_If_Triggered_First_And_Next_ActiveType()
        {
            // found a bug where second round in a pool was not caching values properly. 
            // needs a Next activeType to reproduce.
            // seems to be the last value. 
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.activeType = ActiveType.Next;
            obj1.numInstances = 2;
            obj1.triggerPosition = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                },
                startTime = 0,
                trigger = "primary_knob",
                triggers = new List<SecondaryActiveTrigger>{
                    {
                        new SecondaryActiveTrigger {
                            knob = new Vec {
                                y = 0,
                                y2 = 1
                            },
                            trigger = "secondary_knob"
                        }
                    }
                }
            };
            Trigger t = new Trigger
            {
                eventName = "primary_knob",
                channel = 0,
                knob = 1
            };
            Trigger t2 = new Trigger
            {
                eventName = "secondary_knob",
                channel = 0,
                knob = 2
            };
            addTrigger(t);
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t2, 1);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            GameObject instance2 = getPopupInstanceByIndex(1);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            //second instance should be the same because we have not changed the knobs.  
            Assert.AreEqual(instance2.transform.position, new Vector3(0, 1, 0));
        }

        [UnityTest]
        public IEnumerator Starts_At_Value_If_No_Start()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                value = new Vec
                {
                    x = -3
                }
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, new Vector3(-3, 0, 0));
            yield return new WaitForSecondsOrTrue(1, () => activePosition.introduced);
            Assert.AreEqual(instance.transform.position, new Vector3(-3, 0, 0));
        }

        [Test]
        public void Origin_Property_Quickly_Assigns_Position_Without_ActiveTrigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.origin = new Vec
            {
                x = 1
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
        }


        [Test]
        public void Origin_Does_Not_Affect_Position_In_Default_Hierarchy()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger { };
            obj1.origin = new Vec
            {
                x = 1
            };
            initializeObjs();
            triggerEvent("intro");
            //localPosition is not affected by origin. 
            Assert.AreEqual(instance.transform.localPosition, new Vector3(0, 0, 0));
        }

        [Test]
        public void Origin_Affects_BasePosition_In_Hierarchy_None()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            obj1.triggerPosition = new ActiveTrigger { };
            obj1.origin = new Vec
            {
                x = 1
            };
            initializeObjs();
            triggerEvent("intro");
            //localPosition is not affected by origin. 
            Assert.AreEqual(instance.transform.localPosition, new Vector3(1, 0, 0));
        }

        [Test]
        public void Value_Overrides_Origin_In_Hierarchy_None()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            obj1.triggerPosition = new ActiveTrigger
            {
                value = new Vec
                {
                    y = 1
                }
            };
            obj1.origin = new Vec
            {
                x = 1
            };

            initializeObjs();
            triggerEvent("intro");
            //localPosition is not affected by origin. 
            Assert.AreEqual(instance.transform.localPosition, new Vector3(0, 1, 0));
        }

        [UnityTest]
        public IEnumerator Start_Position_Acts_As_PopupPosition()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                start = new Vec
                {
                    x = -1
                },
                startTime = 0.1f
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, new Vector3(-1, 0, 0));
            yield return new WaitForSecondsOrTrue(1, () => activePosition.introduced);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Start_Position_Is_Relative_To_Origin()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.origin = new Vec(1, 0, 0);
            obj1.triggerPosition = new ActiveTrigger
            {
                start = new Vec
                {
                    y = 1,
                }
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, new Vector3(1, 1, 0));
            Assert.AreEqual(instance.transform.localPosition, new Vector3(0, 1, 0));
            yield return 0;
        }

        [UnityTest]
        public IEnumerator Popup_Works_With_Any_Value()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                start = new Vec
                {
                    x = -1
                },
                value = new Vec
                {
                    x = 1,
                },
                startTime = 0.1f
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, new Vector3(-1, 0, 0));
            yield return new WaitForSecondsOrTrue(1, () => activePosition.introduced);
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
        }


        [UnityTest]
        public IEnumerator Start_Delay_Delays_Value_Even_If_Start_Time_Is_Zero()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                start = new Vec
                {
                    x = 0
                },
                value = new Vec
                {
                    x = 1
                },
                startTime = 0,
                startDelay = 0.1f
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Position_Once_From_Zero()
        {
            obj1 = createBlankObj();
            obj1.id = "obj1";
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    y = 1,
                },
                trigger = "triggerPosition",
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            yield return new WaitForSecondsOrTrue(1, () => !activePosition.on);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Multiplier_Affects_All_Vectors()
        {
            obj1 = createBlankObj();
            obj1.id = "obj1";
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                value = new Vec
                {
                    x = 0.5f,
                },
                multiplier = 2,
                target = new Vec
                {
                    y = 1,
                },
                trigger = "triggerPosition",
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0)); //double 0.5
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 2, 0)); //double 1.
        }

        [UnityTest]
        public IEnumerator Secondary_Position_Triggers_From_Zero()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    y = 1,
                },
                trigger = "triggerPosition",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            x = -1,
                        },
                        trigger = "triggerSecondaryPosition",
                        onTime = 0.1f,
                        offTime = 0.1f,
                        offDelay = 0.1f,
                    }
                },
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            triggerEvent("triggerSecondaryPosition");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.x == -1);
            Assert.AreEqual(instance.transform.position, new Vector3(-1, 0, 0));
        }

        [UnityTest]
        public IEnumerator Second_Trigger_Continues_From_First_Trigger_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    y = 1,
                },
                trigger = "triggerPosition",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            x = -1,
                        },
                        trigger = "triggerSecondaryPosition",
                        onTime = 0.5f,
                        offTime = -1
                    }
                },
                onTime = 0.1f,
                offTime = -1
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            yield return 0;
            triggerEvent("triggerPosition"); //uh..
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position == new Vector3(0, 1, 0));
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            triggerEvent("triggerSecondaryPosition");
            //now should be transitioning in a diagonal line from (0, 1, 0) to (1, 0, 0)
            yield return new WaitForSecondsOrTrue(0.3f, () => instance.transform.position.y > 0);
            //second trigger. should have started from first trigger.  - this will be false if it waits too long.
            Assert.Greater(instance.transform.position.y, 0.0); //should not have gone back to zero. 
        }


        [UnityTest]
        public IEnumerator Can_Trigger_Position_Once_From_Any_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                value = new Vec
                {
                    x = 1,
                },
                start = new Vec
                {
                    x = 1
                },
                target = new Vec
                {
                    y = 1,
                },
                trigger = "triggerPosition",
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            yield return new WaitForSecondsOrTrue(1, () => !activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Position_With_Sustain_From_Zero()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    y = 1,
                },
                trigger = "triggerPositionSustain",
                onTime = 0.1f,
                offDelay = 0.001f,
            };
            Trigger sustain_trigger = new Trigger
            {
                eventName = "triggerPositionSustain",
                sustain = true,
                note = 1,
                channel = 0,
            };
            addTrigger(sustain_trigger);
            registerTriggers();
            initializeObjs();
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            yield return 0;
            midiController.OnMidiNoteOn(0, 1, 1);
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            yield return new WaitForSeconds(0.5f);
            //still in same position without off note.
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
        }

        [UnityTest]
        public IEnumerator Secondary_Sustain_Position_Returns_To_Base_Position_Before_Finishing()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    y = 1,
                },
                trigger = "triggerPosition",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            x = -1,
                        },
                        trigger = "secondarySustainPosition",
                        onTime = 2.0f,
                        offTime = 0.1f,
                    }
                },
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            initializeObjs();
            TriggerObj sustainTriggerObj = new TriggerObj
            {
                eventName = "secondarySustainPosition",
                sustain = true

            };
            TriggerObj noteOffObj = new TriggerObj
            {
                eventName = "secondarySustainPosition",
                sustain = true,
                noteOff = true
            };
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            yield return 0;
            triggerEvent("triggerPosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            triggerEvent(sustainTriggerObj);
            yield return new WaitForSeconds(0.3f);
            //cut it short and then trigger off. 
            triggerEvent(noteOffObj);
            yield return new WaitForSecondsOrTrue(1, () => !activePosition.on);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
        }


        [UnityTest]
        public IEnumerator Sustain_Trigger_Returns_To_Original_Position_Of_Zero()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    y = 1,
                },
                trigger = "triggerPositionSustain",
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.001f,
            };
            TriggerObj sustainTriggerObj = new TriggerObj
            {
                eventName = "triggerPositionSustain",
                sustain = true
            };
            TriggerObj noteOffTriggerObj = new TriggerObj
            {
                eventName = "triggerPositionSustain",
                sustain = true,
                noteOff = true
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            yield return 0;
            triggerEvent(sustainTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            yield return new WaitForSeconds(0.5f);
            //still in same position without off note.
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => !activePosition.on);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Sustain_Trigger_Returns_To_Any_Original_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    y = 1,
                },
                value = new Vec
                {
                    y = -1
                },
                trigger = "triggerPositionSustain",
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.001f,
            };
            TriggerObj sustainTriggerObj = new TriggerObj
            {
                eventName = "triggerPositionSustain",
                sustain = true
            };
            TriggerObj noteOffTriggerObj = new TriggerObj
            {
                eventName = "triggerPositionSustain",
                sustain = true,
                noteOff = true
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.introduced);
            Assert.AreEqual(instance.transform.position, new Vector3(0, -1, 0));
            triggerEvent(sustainTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            yield return new WaitForSeconds(0.5f);
            //still in same position without off note.
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => !activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(0, -1, 0));
        }

        [UnityTest]
        public IEnumerator Sustain_Intro_Holds_On_Value_Before_Exit()
        {
            obj1 = createBlankObj();
            obj1.trigger = "introSustain";
            obj1.triggerPosition = new ActiveTrigger
            {
                start = new Vec
                {
                    y = -1,
                },
                value = new Vec { },
                end = new Vec
                {
                    y = 1,
                },
                onTime = 0.1f,
                endDelay = 0.01f,
                endTime = 0.1f,
            };
            TriggerObj sustainTriggerObj = new TriggerObj
            {
                eventName = "introSustain",
                sustain = true
            };
            TriggerObj noteOffTriggerObj = new TriggerObj
            {
                eventName = "introSustain",
                sustain = true,
                noteOff = true
            };
            registerTriggers();
            initializeObjs();
            triggerEvent(sustainTriggerObj);
            Assert.AreEqual(instance.transform.position, new Vector3(0, -1, 0));
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            yield return new WaitForSeconds(0.5f);
            //still in same position without note off. 
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => activePosition.ended);
            Assert.AreEqual(instance.transform.position, new Vector3(0, 1, 0));
        }

        [UnityTest]
        public IEnumerator Debugging_Relative_Position_Trigger_Tweens()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 1,
                    relative = true,
                },
                onTime = 0.2f,
                trigger = "triggerRelativePosition"
            };
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            instance.transform.position = new Vector3(1, 0, 0);
            yield return 0;
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
            triggerEvent("triggerRelativePosition");
            yield return 0;
            yield return 0;
            yield return new WaitForSeconds(0.1f);
            Assert.Greater(instance.transform.position.x, 1);
        }

        [UnityTest]
        public IEnumerator Relative_Vec_Triggers_Additive_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 1,
                    relative = true,
                },
                onTime = 0.1f,
                offTime = -1,
                trigger = "triggerRelativePosition"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("triggerRelativePosition");
            yield return new WaitForSecondsOrTrue(1, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
        }

        [UnityTest]
        public IEnumerator Relative_Vec_Triggers_Additive_Position_If_ReTriggered()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 1,
                    relative = true,
                },
                onTime = 1.0f,
                offTime = -1,
                trigger = "triggerRelativePosition"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("triggerRelativePosition");
            yield return new WaitForSeconds(0.3f);
            Vector3 savedPosition = instance.transform.position;
            triggerEvent("triggerRelativePosition");
            yield return new WaitForSeconds(1.1f);
            Assert.AreEqual(instance.transform.position, new Vector3(savedPosition.x + 1.0f, 0, 0));
        }

        [UnityTest]
        public IEnumerator Relative_Vec_Triggers_Exact_Position_If_ReTriggered_With_Multiple()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 1,
                    relative = true,
                    multiple = new Vector3(1, 1, 1)
                },
                onTime = 0.3f,
                offTime = -1,
                trigger = "triggerRelativePosition"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("triggerRelativePosition");
            yield return new WaitForSeconds(0.2f);
            triggerEvent("triggerRelativePosition");
            Vector3 savedPosition = instance.transform.position;
            yield return new WaitForSecondsOrTrue(0.5f, () => activePosition.on);
            Assert.AreEqual(instance.transform.position, new Vector3(2.0f, 0, 0));
        }

        [UnityTest]
        public IEnumerator AlwaysFront_Proprety_Hijacks_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.alwaysFront = "true";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            CameraController cameraController = flybyController.cameraController;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position.z, 99.99f));
        }

        [UnityTest]
        public IEnumerator AlwaysBack_Proprety_Hijacks_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.alwaysBack = "true";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            CameraController cameraController = flybyController.cameraController;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position.z, 100.01f));
        }
    }

}

