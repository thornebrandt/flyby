using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;


namespace Tests {
    public class StackControllerTests : PlayModeTests {
        private List<GameObject> instances;
        private ActivePosition activePosition;
        private ActiveSpeed activeSpeed;
        private StackController stackController;

        [SetUp]
        public void Setup() {
            this.setup();
            stackController = go.GetComponent<StackController>();
        }

        [TearDown]
        public void Teardown() {
            flybyController.killAll();
        }

        [Test]
        public void StackController_Is_Not_Null() {
            Assert.IsNotNull(stackController);
        }

        [Test]
        public void Gets_A_List_Of_Stacked_Objs() {
            obj1 = createBlankObj();
            obj1.numInstances = 3;
            obj1.preRender = "true";
            obj1.stack = new StackArray {
                normalized = new Vector3(1, 0, 0)
            };
            initializeObjs();
            Assert.AreEqual(stackController.stackedObjs.Count, 1);
        }

        [Test]
        public void Obj_Caluclates_Scale_Based_On_Asset_And_TriggerScale() {
            obj1 = createBlankObj();
            obj1.triggerScale = new ActiveTrigger {
                value = new Vec(2)
            };
            initializeObjs();
            Vector3 objScale = o.calculateScale();
            Assert.AreEqual(objScale, new Vector3(2, 2, 2));
        }


        [Test]
        public void Defaults_To_ActiveType_All() {
            obj1 = createBlankObj();
            obj1.stack = new StackArray();
            initializeObjs();
            Assert.AreEqual(flybyController.currentPart.objs[0].activeType, ActiveType.All);
        }

        [Test]
        public void Defaults_To_X5_Stack() {
            obj1 = createBlankObj();
            obj1.stack = new StackArray();
            initializeObjs();
            Assert.AreEqual(flybyController.currentPart.objs[0].numInstances, 5);
        }

        [UnityTest]
        public IEnumerator Can_Edit_Origin_Of_Obj() {
            //TODO - add stack controller to this. 
            obj1 = createBlankObj();
            obj1.numInstances = 3;
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instances[0].transform.position, Vector3.zero);
            o.origin = new Vec(0, 3, 0);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instances[1].transform.position, new Vector3(0, 3, 0));
        }

        [UnityTest]
        public IEnumerator Makes_Centered_Stack_Of_Objs_Odd_Number() {
            obj1 = createBlankObj();
            // add centered property if this breaks at some point. 
            obj1.stack = new StackArray {
                instanceDimensions = new Vector3(3, 3, 3),
                normalized = new Vector3(1, 1, 1),
                boundaries = new Vec {
                    x = -5,
                    x2 = 5,
                    y = -5,
                    y2 = 5,
                    z = -5,
                    z2 = 5
                }
            };
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            triggerEvent("intro");
            initializeObjs();
            yield return 0;
            Assert.AreEqual(instances[0].transform.position, new Vector3(1.0f, 1.0f, 1.0f));
            Assert.AreEqual(instances[1].transform.position, new Vector3(1.0f, 1.0f, 0.0f));
            Assert.AreEqual(instances[2].transform.position, new Vector3(1.0f, 1.0f, -1.0f));
            Assert.AreEqual(instances[3].transform.position, new Vector3(1.0f, 0.0f, 1.0f));
            Assert.AreEqual(instances[4].transform.position, new Vector3(1.0f, 0.0f, 0.0f));
        }

        [UnityTest]
        public IEnumerator Can_Repeat_After_Extending_Beyond_Bounds_In_Flat_Hierarchy() {
            obj1 = createBlankObj();
            obj1.stack = new StackArray {
                instanceDimensions = new Vector3(1, 0, 0),
                boundaries = new Vec {
                    x = -3,
                    x2 = 3,
                    y = -3,
                    y2 = 3,
                    z = -3,
                    z2 = 3
                }
            };
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            triggerEvent("intro");
            initializeObjs();
            yield return 0;
            Assert.AreEqual(instances[0].transform.position, new Vector3(0.0f, 0.0f, 0.0f));

            //+x 
            instances[0].transform.position = new Vector3(3.5f, 0, 0);
            yield return 0;
            Assert.AreEqual(instances[0].transform.position, new Vector3(2.5f, 0.0f, 0.0f));

            //-x
            instances[0].transform.position = Vector3.zero;
            yield return 0;
            instances[0].transform.position = new Vector3(-3.5f, 0, 0);
            yield return 0;
            Assert.AreEqual(instances[0].transform.position, new Vector3(-2.5f, 0.0f, 0.0f));

            //+y
            instances[0].transform.localPosition = Vector3.zero;
            yield return 0;
            instances[0].transform.localPosition = new Vector3(0, 3.5f, 0);
            yield return 0;
            Assert.AreEqual(instances[0].transform.localPosition, new Vector3(0, 2.5f, 0));

            //-y
            instances[0].transform.localPosition = Vector3.zero;
            yield return 0;
            instances[0].transform.localPosition = new Vector3(0, -3.5f, 0);
            yield return 0;
            Assert.AreEqual(instances[0].transform.localPosition, new Vector3(0, -2.5f, 0));

            //z
            instances[0].transform.localPosition = Vector3.zero;
            yield return 0;
            instances[0].transform.localPosition = new Vector3(0, 0, 3.5f);
            yield return 0;
            Assert.AreEqual(instances[0].transform.localPosition, new Vector3(0, 0, 2.5f));

            //z
            instances[0].transform.localPosition = Vector3.zero;
            yield return 0;
            instances[0].transform.localPosition = new Vector3(0, 0, -3.5f);
            yield return 0;
            Assert.AreEqual(instances[0].transform.localPosition, new Vector3(0, 0, -2.5f));
        }

        [UnityTest]
        public IEnumerator Can_Repeat_After_Extending_Beyond_Bounds_In_Default_Hierarchy() {
            obj1 = createBlankObj();
            obj1.stack = new StackArray {
                instanceDimensions = new Vector3(1, 0, 0),
                boundaries = new Vec {
                    x = -3,
                    x2 = 3,
                    y = -3,
                    y2 = 3,
                    z = -3,
                    z2 = 3
                }
            };
            obj1.trigger = "intro";
            triggerEvent("intro");
            initializeObjs();
            yield return 0;
            GameObject popupObj = Obj.getPopupInstance(instances[0]);
            Assert.AreEqual(instances[0].transform.position, new Vector3(0.0f, 0.0f, 0.0f));
            Assert.AreEqual(popupObj.transform.localPosition, new Vector3(0.0f, 0.0f, 0.0f));

            //+x
            popupObj.transform.localPosition = new Vector3(3.5f, 0, 0);
            yield return 0;
            Assert.AreEqual(popupObj.transform.localPosition, new Vector3(2.5f, 0.0f, 0.0f));

            //-x
            popupObj.transform.localPosition = Vector3.zero;
            yield return 0;
            popupObj.transform.localPosition = new Vector3(-3.5f, 0, 0);
            yield return 0;
            Assert.AreEqual(popupObj.transform.localPosition, new Vector3(-2.5f, 0.0f, 0.0f));

            //+y
            popupObj.transform.localPosition = Vector3.zero;
            yield return 0;
            popupObj.transform.localPosition = new Vector3(0, 3.5f, 0);
            yield return 0;
            Assert.AreEqual(popupObj.transform.localPosition, new Vector3(0, 2.5f, 0));

            //-y
            popupObj.transform.localPosition = Vector3.zero;
            yield return 0;
            popupObj.transform.localPosition = new Vector3(0, -3.5f, 0);
            yield return 0;
            Assert.AreEqual(popupObj.transform.localPosition, new Vector3(0, -2.5f, 0));

            //z
            popupObj.transform.localPosition = Vector3.zero;
            yield return 0;
            popupObj.transform.localPosition = new Vector3(0, 0, 3.5f);
            yield return 0;
            Assert.AreEqual(popupObj.transform.localPosition, new Vector3(0, 0, 2.5f));

            //z
            popupObj.transform.localPosition = Vector3.zero;
            yield return 0;
            popupObj.transform.localPosition = new Vector3(0, 0, -3.5f);
            yield return 0;
            Assert.AreEqual(popupObj.transform.localPosition, new Vector3(0, 0, -2.5f));
        }

        [UnityTest]
        public IEnumerator Makes_Centered_Stack_Of_Objs_Even_Number() {
            obj1 = createBlankObj();
            obj1.stack = new StackArray {
                instanceDimensions = new Vector3(2, 2, 2),
                normalized = new Vector3(1, 1, 1),
                boundaries = new Vec {
                    x = -5,
                    x2 = 5,
                    y = -5,
                    y2 = 5,
                    z = -5,
                    z2 = 5
                }
            };
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            triggerEvent("intro");
            initializeObjs();
            yield return 0;
            Assert.AreEqual(instances[0].transform.position, new Vector3(0.5f, 0.5f, 0.5f));
            Assert.AreEqual(instances[1].transform.position, new Vector3(0.5f, 0.5f, -0.5f));
            Assert.AreEqual(instances[2].transform.position, new Vector3(0.5f, -0.5f, 0.5f));
            Assert.AreEqual(instances[3].transform.position, new Vector3(0.5f, -0.5f, -0.5f));
        }


        [UnityTest]
        public IEnumerator Makes_2D_Stack_If_Z_Is_Empty() {
            obj1 = createBlankObj();
            obj1.stack = new StackArray {
                instanceDimensions = new Vector3(2, 2, 0),
                normalized = new Vector3(1, 1, 1),
                boundaries = new Vec {
                    x = -5,
                    x2 = 5,
                    y = -5,
                    y2 = 5,
                    z = -5,
                    z2 = 5
                }
            };
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            triggerEvent("intro");
            initializeObjs();
            yield return 0;
            Assert.AreEqual(instances[0].transform.position, new Vector3(0.5f, 0.5f, 0.0f));
            Assert.AreEqual(instances[1].transform.position, new Vector3(0.5f, -0.5f, 0.0f));
            Assert.AreEqual(instances[2].transform.position, new Vector3(-0.5f, 0.5f, 0.0f));
            Assert.AreEqual(instances[3].transform.position, new Vector3(-0.5f, -0.5f, 0.0f));
        }

        [UnityTest]
        public IEnumerator Makes_Centered_Stack_Offset_By_Original_Origin() {
            obj1 = createBlankObj();
            obj1.stack = new StackArray {
                instanceDimensions = new Vector3(2, 2, 2),
                normalized = new Vector3(1, 1, 1),
                boundaries = new Vec {
                    x = -5,
                    x2 = 5,
                    y = -5,
                    y2 = 5,
                    z = -5,
                    z2 = 5
                }
            };
            obj1.origin = new Vec(3, 4, 5);
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            triggerEvent("intro");
            initializeObjs();
            yield return 0;
            Assert.AreEqual(instances[0].transform.position, new Vector3(3.5f, 4.5f, 5.5f));
            Assert.AreEqual(instances[1].transform.position, new Vector3(3.5f, 4.5f, 4.5f));
            Assert.AreEqual(instances[2].transform.position, new Vector3(3.5f, 3.5f, 5.5f));
            Assert.AreEqual(instances[3].transform.position, new Vector3(3.5f, 3.5f, 4.5f));
        }

        public override void initializeObjs() {
            base.initializeObjs();
            instances = flybyController.currentPart.objs[0].instances;
            o = flybyController.currentPart.objs[0];
            activePosition = getFirstInstance().GetComponent<ActivePosition>();
            activeSpeed = getFirstInstance().GetComponent<ActiveSpeed>();
        }
    }
}
