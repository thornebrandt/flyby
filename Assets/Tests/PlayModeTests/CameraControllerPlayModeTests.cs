using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Active;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;


namespace Tests
{
    public class CameraControllerPlayModeTests
    {
        GameObject go;
        CameraController cameraController;
        AssetController assetController;
        GameObject camObj;
        GameObject camTarget; //cam will always be looking at this target. used to keep track of y rotation.  
        GameObject camPivot; //used to keep track of x rotation. 
        GameObject camOffset; //used to keep track of offset. 
        GameObject cam; //used to keep track of triggered position
        Obj cameraO;

        [SetUp]
        public void Setup()
        {
            go = new GameObject("CameraController");
            if (cameraController == null)
            {
                cameraController = go.AddComponent<CameraController>();
                cameraO = new Obj();
            }
            if (assetController == null)
            {
                assetController = new AssetController();
                assetController.prefabCache = new Dictionary<string, GameObject>();
                GameObject cam = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
                this.camObj = Object.Instantiate(cam) as GameObject;
                cameraController.camObj = this.camObj;
            }
            cameraController.camObj = camObj;
            cameraController.setup(null, cameraO);
            camTarget = camObj.transform.GetChild(0).gameObject;
            camPivot = camTarget.transform.GetChild(0).gameObject;
            camOffset = camPivot.transform.GetChild(0).gameObject;
            cam = camOffset.transform.GetChild(0).gameObject;
        }

        [TearDown]
        public void TearDown()
        {
            cameraController.reset();
        }

        [Test]
        public void Sets_Up_Controller()
        {
            Assert.IsNotNull(cameraController);
            Assert.IsNotNull(cameraController.camObj);
        }

        [UnityTest]
        public IEnumerator Can_Transition_To_Specific_Angle_By_Name()
        {
            cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                };
            CameraAngle c = cameraController.cameraAngles[0];
            c.animationTime = 0.2f;
            c.offset = new Vec(1, 0, 0);
            c.trigger = "thisAngle";
            TriggerObj t = new TriggerObj
            {
                eventName = "thisAngle"
            };
            cameraController.eventHandler(t);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -5));
            yield return new WaitForSeconds(0.1f);
            Assert.AreNotEqual(cam.transform.position, new Vector3(0, 0, -5));
            yield return new WaitForSecondsOrTrue(1.0f, () => cam.transform.position == new Vector3(1, 0, 0));
            Assert.IsTrue(Math.RoughEquals(cam.transform.position, new Vector3(1, 0, 0)));
        }


        [UnityTest]
        public IEnumerator Rotation_Interrupts_Camera_Angle_Sequence()
        {
            cameraController.cameraAngles = new List<CameraAngle>{
                new CameraAngle(),
                new CameraAngle()
            };
            CameraAngle c = cameraController.cameraAngles[1];
            c.animationTime = 1;
            c.rotation = new Vec(0, -90, 0);
            c.trigger = "triggerCameraAngle";

            cameraO.triggerRotate = new ActiveTrigger
            {
                trigger = "triggerConstantRotation",
                target = new Vec
                {
                    y = 1
                }
            };
            TriggerObj constantRotationTrigger = new TriggerObj
            {
                eventName = "triggerConstantRotation"
            };
            TriggerObj cameraAngleTrigger = new TriggerObj
            {
                eventName = "triggerCameraAngle"
            };
            cameraController.setupCameraTriggers(cameraO);
            cameraController.eventHandler(cameraAngleTrigger);
            Assert.IsTrue(camTarget.transform.localRotation == Quaternion.Euler(new Vector3(0, 0, 0)));
            yield return new WaitForSeconds(0.2f);
            Assert.Less(camTarget.transform.localRotation.y, 0);
            // this event should interrupt the tween.  
            cameraController.eventHandler(constantRotationTrigger);
            //save current rotation for comparison.
            float currentRotation = camTarget.transform.localRotation.y;
            yield return new WaitForSeconds(0.2f);
            Assert.Greater(camTarget.transform.localRotation.y, currentRotation);
        }


        [UnityTest]
        public IEnumerator Can_Animate_Rotation_To_Next_Angle()
        {
            cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                    new CameraAngle(),
                };
            CameraAngle c2 = cameraController.cameraAngles[1];
            c2.animationTime = 0.2f;
            c2.rotation = new Vec(
                45,
                90,
                0
            );
            TriggerObj t = new TriggerObj
            {
                eventName = "next_camera_angle"
            };
            cameraController.setupCameraTriggers(cameraO);
            cameraController.eventHandler(t);
            Assert.AreEqual(camPivot.transform.localRotation, Quaternion.Euler(new Vector3(0, 0, 0)));
            yield return new WaitForSeconds(0.1f);
            Assert.AreNotEqual(camPivot.transform.localRotation, Quaternion.Euler(new Vector3(0, 0, 0)));
            yield return new WaitForSecondsOrTrue(1.0f, () => cam.transform.position == new Vector3(1, 0, 0));
            Assert.AreEqual(camPivot.transform.localRotation, Quaternion.Euler(new Vector3(45, 0, 0)));
            Assert.IsTrue(camTarget.transform.localRotation == Quaternion.Euler(new Vector3(0, 90, 0)));
        }

        [UnityTest]
        public IEnumerator Can_Rotate_Based_On_CameraTriggers()
        {
            cameraO.triggerRotate = new ActiveTrigger
            {
                trigger = "triggerRotatePivot",
                target = new Vec
                {
                    x = 1
                },
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger{
                        trigger = "triggerRotateTarget",
                        target = new Vec {
                            y = 1
                        }
                    }
                }
            };
            cameraController.setupCameraTriggers(cameraO);
            TriggerObj t = new TriggerObj
            {
                eventName = "triggerRotatePivot"
            };
            cameraController.eventHandler(t);
            Assert.AreEqual(camPivot.transform.localRotation, Quaternion.Euler(new Vector3(0, 0, 0)));
            yield return new WaitForSeconds(0.2f);
            Assert.Greater(camPivot.transform.localRotation.eulerAngles.x, 0);
            TriggerObj t2 = new TriggerObj
            {
                eventName = "triggerRotateTarget"
            };
            cameraController.eventHandler(t2);
            Assert.AreEqual(camTarget.transform.localRotation, Quaternion.Euler(new Vector3(0, 0, 0)));
            yield return new WaitForSeconds(0.2f);
            Assert.Greater(camTarget.transform.localRotation.y, 0);
        }


        [UnityTest]
        public IEnumerator Can_Trigger_Position_From_CameraTriggers()
        {
            cameraO.triggerPosition = new ActiveTrigger
            {
                onTime = 0.2f,
                offDelay = 0.1f,
                trigger = "triggerCameraPosition",
                target = new Vec
                {
                    z = 1,
                    relative = true
                }
            };
            cameraController.cutToCameraAngle(cameraController.cameraAngles[0]);
            cameraController.setupCameraTriggers(cameraO);
            TriggerObj t = new TriggerObj
            {
                eventName = "triggerCameraPosition"
            };
            cameraController.eventHandler(t);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -5));
            yield return new WaitForSeconds(0.2f);
            yield return 0;
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -4));
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Position_From_CameraTriggers_And_Return_To_Origin()
        {
            cameraO.triggerPosition = new ActiveTrigger
            {
                onTime = 0.1f,
                offDelay = 0.1f,
                offTime = 0.1f,
                trigger = "triggerCameraPosition",
                target = new Vec
                {
                    z = 1,
                    relative = true
                }
            };
            cameraController.cutToCameraAngle(cameraController.cameraAngles[0]);
            cameraController.setupCameraTriggers(cameraO);
            TriggerObj t = new TriggerObj
            {
                eventName = "triggerCameraPosition"
            };
            cameraController.eventHandler(t);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -5));
            yield return new WaitForSecondsOrTrue(0.5f, () => cam.transform.position.z == -4);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -4));
            yield return new WaitForSecondsOrTrue(0.5f, () => cam.transform.position.z == -5);
            Assert.AreEqual(cam.transform.position, new Vector3(0, 0, -5));
        }

        [UnityTest]
        public IEnumerator Spout_Child_Camera_Starts_With_Camera_Disabled_And_Toggles_On()
        {
            GameObject spoutCamObj = cam.transform.GetChild(0).gameObject;
            Assert.IsNotNull(spoutCamObj);
            Camera camCam = cam.GetComponent<Camera>();
            Camera spoutCam = spoutCamObj.GetComponent<Camera>();
            Assert.IsNotNull(camCam);
            Assert.IsNotNull(spoutCam);
            Assert.IsTrue(camCam.enabled);
            Assert.IsFalse(spoutCam.enabled);
            cameraController.setupCameraTriggers(cameraO);
            TriggerObj t = new TriggerObj
            {
                eventName = "toggle_spout"
            };
            cameraController.eventHandler(t);
            yield return 0;
            Assert.IsFalse(camCam.enabled);
            Assert.IsTrue(spoutCam.enabled);
        }

        [UnityTest]
        public IEnumerator Continues_To_Next_Angle_After_A_Position_Punch()
        {
            cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                    new CameraAngle(),
                };
            CameraAngle c2 = cameraController.cameraAngles[1];
            c2.animationTime = 0.2f;
            c2.rotation = new Vec(
                45,
                90,
                0
            );
            cameraO.triggerPosition = new ActiveTrigger
            {
                onTime = 0.1f,
                offDelay = 0.1f,
                offTime = 0.1f,
                trigger = "triggerCameraPosition",
                target = new Vec
                {
                    z = 1,
                    relative = true
                }
            };
            cameraController.cutToCameraAngle(cameraController.cameraAngles[0]);
            cameraController.setupCameraTriggers(cameraO);
            TriggerObj t = new TriggerObj
            {
                eventName = "next_camera_angle"
            };
            cameraController.eventHandler(t);
            Assert.AreEqual(camPivot.transform.localRotation, Quaternion.Euler(new Vector3(0, 0, 0)));
            yield return new WaitForSeconds(0.1f);
            Assert.AreNotEqual(camPivot.transform.localRotation, Quaternion.Euler(new Vector3(0, 0, 0)));
            yield return new WaitForSecondsOrTrue(1.0f, () => cam.transform.position == new Vector3(1, 0, 0));
            Assert.AreEqual(camPivot.transform.localRotation, Quaternion.Euler(new Vector3(45, 0, 0)));
            Assert.IsTrue(camTarget.transform.localRotation == Quaternion.Euler(new Vector3(0, 90, 0)));
        }


    }
}