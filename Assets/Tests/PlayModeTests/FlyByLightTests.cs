using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyByLightTests : PlayModeTests
    {
        private ActiveLight activeLight;
        private Light light;

        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
        }

        [UnityTest]
        public IEnumerator Active_Light_Is_Not_Null()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            triggerEvent("intro");
            yield return 0;
            Assert.IsNotNull(instance);
            Assert.IsNotNull(light);
        }

        [UnityTest]
        public IEnumerator Intro_Works_For_Red_Light_From_Black()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0.1f;
            a.endTime = -1;
            a.startColor = new Col(0);
            a.color = new Col(1, 0, 0);
            triggerEvent("intro");
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
            yield return new WaitForSecondsOrTrue(1, () => activeLight.introduced);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.red));
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Child_Lights_With_Normal_Hierarchy()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "ParentWithChildrenLights";
            obj1.trigger = "intro";
            obj1.activeTrigger = new ActiveTrigger
            {
                childIndex = 0,
                color = new Col
                {
                    color = "red"
                }
            };
            initializeObjs();
            light = instance.transform.GetChild(0).GetComponent<Light>();
            activeLight = light.GetComponent<ActiveLight>();
            base.initializeObjs();
            Assert.IsNotNull(light);
            Assert.IsNotNull(activeLight);
            triggerEvent("intro");
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.red));
        }


        [UnityTest]
        public IEnumerator Intensity_Stays_As_Prefabs_Intensity_If_No_Value()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(light.intensity, 5000);
        }

        [UnityTest]
        public IEnumerator Value_X_Controls_Intensity_For_Start_And_End()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0.1f;
            a.start = new Vec(0, 0, 0);
            a.end = new Vec(0, 0, 0);
            a.value = new Vec(5000, 0, 0);
            a.endDelay = 0.1f;
            a.endTime = 0.1f;
            triggerEvent("intro");
            Assert.AreEqual(light.intensity, 0);
            yield return new WaitForSecondsOrTrue(0.2f, () => light.intensity == 5000);
            Assert.AreEqual(light.intensity, 5000);
        }

        [UnityTest]
        public IEnumerator Value_Y_Defaults_To_60()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0.1f;
            a.start = new Vec(0, 0, 0);
            triggerEvent("intro");
            Assert.AreEqual(light.intensity, 0);
            Assert.AreEqual(light.spotAngle, 60);
            yield return 0;
        }

        [UnityTest]
        public IEnumerator Intensity_Starts_At_Value_If_No_Start_Value()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0.1f;
            a.value = new Vec(5000, 0, 0);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(light.intensity, 5000);
        }

        [UnityTest]
        public IEnumerator Intensity_Starts_As_Zero_If_Value_Is_Zero()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0.1f;
            a.startColor = new Col(0, 0, 0);
            a.color = new Col(1, 0, 0);
            a.targetColor = new Col(0, 1, 0);
            a.value = new Vec(0, 0, 0);
            a.target = new Vec(5000, 0, 0);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(light.intensity, 0);
            yield return new WaitForSeconds(1.0f);
            Assert.AreEqual(light.intensity, 0);
        }


        [UnityTest]
        public IEnumerator Target_Light_Has_A_Cone_Of_60_If_Zero()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.color = new Col(1, 0, 0);
            a.startTime = 0;
            a.targetColor = new Col(0, 1, 0);
            a.value = new Vec(0, 30, 0);
            a.trigger = "triggerLight";
            a.onTime = 0.1f;
            a.offTime = -1;
            a.target = new Vec(5000, 0, 0);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(light.spotAngle, 30);
            triggerEvent("triggerLight");
            yield return new WaitForSecondsOrTrue(0.5f, () => light.intensity == 5000);
            Assert.AreEqual(light.spotAngle, 60);
        }


        [UnityTest]
        public IEnumerator End_Time_Works_For_Lights()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0.1f;
            a.endTime = 0.1f;
            a.endDelay = 0.1f;
            a.startColor = new Col(0);
            a.color = new Col(1, 0, 0);
            a.endColor = new Col(0);
            triggerEvent("intro");
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
            yield return new WaitForSecondsOrTrue(1, () => activeLight.introduced);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.red));
            yield return new WaitForSecondsOrTrue(1, () => activeLight.ended);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
        }

        [UnityTest]
        public IEnumerator End_Time_Works_With_Sustain_For_Lights()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro_sustain";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0.1f;
            a.endTime = 0.1f;
            TriggerObj trigger_intro_sustain = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
            };
            TriggerObj trigger_intro_sustain_off = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
                noteOff = true
            };
            a.startColor = new Col(0);
            a.color = new Col(1, 0, 0);
            a.endColor = new Col(0);
            triggerEvent(trigger_intro_sustain);
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
            yield return new WaitForSecondsOrTrue(1, () => activeLight.introduced);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.red));
            yield return new WaitForSeconds(0.3f);
            Assert.IsTrue(Math.RoughEquals(light.color, Color.red)); //still true. 
            triggerEvent(trigger_intro_sustain_off);
            yield return new WaitForSecondsOrTrue(1, () => activeLight.ended);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
        }


        [UnityTest]
        public IEnumerator Trigger_Works_For_Red_From_Black()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0f;
            a.endTime = -1;
            a.startColor = new Col(0);
            a.color = new Col(0);
            a.trigger = "trigger_light";
            a.targetColor = new Col(1, 0, 0);
            a.offDelay = 0.1f;
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(1, () => activeLight.introduced);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
            triggerEvent("trigger_light");
            yield return new WaitForSecondsOrTrue(1, () => light.color == Color.red);
            Assert.IsTrue(Math.RoughEquals(light.color, Color.red));
        }

        [UnityTest]
        public IEnumerator Trigger_Off_Works_For_Green_To_Black()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0f;
            a.endTime = -1;
            a.startColor = new Col(0);
            a.color = new Col(0);
            a.trigger = "trigger_light";
            a.targetColor = new Col(0, 1, 0);
            a.offDelay = 0.1f;
            a.offTime = 0.1f;
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(1, () => activeLight.introduced);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
            triggerEvent("trigger_light");
            yield return new WaitForSecondsOrTrue(1, () => light.color == Color.green);
            Assert.IsTrue(Math.RoughEquals(light.color, Color.green));
            yield return new WaitForSecondsOrTrue(1, () => light.color == Color.black);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
        }


        [UnityTest]
        public IEnumerator Secondary_Trigger_Works_For_Red_And_Blue_From_Custom_ActiveTrigger()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            obj1.activeTrigger = new ActiveTrigger
            {
                startTime = 0,
                endTime = -1,
                startColor = new Col(0),
                color = new Col(0),
                trigger = "trigger_red",
                targetColor = new Col(1, 0, 0),
                offDelay = 0.1f,
                triggers = new List<SecondaryActiveTrigger> {
                    new SecondaryActiveTrigger {
                        targetColor = new Col(0, 0, 1),
                        trigger = "trigger_blue",
                        offDelay = 0.1f
                    }
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(1, () => activeLight.introduced);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
            triggerEvent("trigger_red");
            yield return new WaitForSecondsOrTrue(1, () => light.color == Color.red);
            Assert.IsTrue(Math.RoughEquals(light.color, Color.red));
            //TODO - do blue now. 
        }


        [UnityTest]
        public IEnumerator Knob_Trigger_Works_For_Color()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0;
            a.color = new Col
            {
                color = "black"
            };
            a.knobColor = new Col
            {
                r = 0,
                r2 = 1
            };
            a.trigger = "light_knob";
            Trigger t = new Trigger
            {
                eventName = "light_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t, 1);
            yield return 0;
            Assert.AreEqual(light.color, Color.red);
        }

        [UnityTest]
        public IEnumerator Start_Knob_Value_Affects_Spawn_Color()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            initializeObjs();
            assignActiveTrigger();
            a.startTime = 0;
            a.color = new Col
            {
                color = "black"
            };
            a.knobColor = new Col
            {
                gray = 0,
                gray2 = 1
            };
            a.trigger = "light_knob";
            a.triggerProperty = new TriggerProperty
            {
                start = 0.5f,
            };
            Trigger t = new Trigger
            {
                eventName = "light_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(light.color, Color.gray);
        }

        private void assignActiveTrigger()
        {
            //older way of setting up the data for the tests 
            activeLight.triggers = new List<ActiveTrigger>();
            a = new ActiveTrigger();
            activeLight.triggers.Add(a);
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            instance = getFirstPrefabInstance();
            activeLight = instance.GetComponent<ActiveLight>();
            light = instance.GetComponent<Light>();
        }
    }
}