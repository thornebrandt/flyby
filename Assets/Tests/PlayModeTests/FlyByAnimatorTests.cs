using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;


namespace Tests
{
    public class FlyByAnimatorTests : PlayModeTests
    {
        private ActiveAnimator activeAnimator;
        private Animator animator;

        //from activeAnimator. 
        //private int introLayer = 0; //intro
        //private int targetLayer = 1; //secondary trigger on active objs. 
        private int loopLayer = 2; //base loop animation. 
        // private int knobLayer = 3; //knob controls position of animation. 
        private int envelopeLayer = 4; //sine wave.
        //private int exitLayer = 5; //if exit time. 
        private int sustainLayer = 6; //sustain for trigger/target Layer.


        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
            // why do we need the below ones? 
            // investigate during compilation. 
            activeAnimator.Kill();
            activeAnimator.killTweens();
        }

        [UnityTest]
        public IEnumerator Animator_Is_Not_Null()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            yield return 0;
            Assert.IsNotNull(activeAnimator);
        }

        [UnityTest]
        public IEnumerator Intro_Animation_Clip_Is_Called()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.3f;
            activeAnimator.loopAnimation = null;
            triggerEvent("intro");
            yield return 0;
            //intro clip is growing from zero. 
            Assert.Less(instance.transform.localScale.x, 1.0);
            yield return new WaitForSecondsOrTrue(0.5f, () => activeAnimator.introduced);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, Vector3.one));
        }

        [UnityTest]
        public IEnumerator Intro_Animation_Inherits_Default_Controller_From_FlyByController_If_Null()
        {
            //have to manually assign the animatorController.
            GameObject animatorCube = null;

            if (assetController == null)
            {
                AssetController assetController = new AssetController();
                assetController.prefabCache = new Dictionary<string, GameObject>();
                animatorCube = assetController.loadAssetFromResources("TestPrefabs", "AnimatorCube");
            }
            flybyController.animatorController = Obj.getPrefabInstance(animatorCube).GetComponent<ActiveAnimator>().animatorController;

            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube2"; //prefab that specifically does not have the animator controller assigned.
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.3f;
            activeAnimator.loopAnimation = null;
            triggerEvent("intro");
            yield return 0;
            //intro clip is growing from zero. 
            Assert.Less(instance.transform.localScale.x, 1.0);
            yield return new WaitForSecondsOrTrue(0.5f, () => activeAnimator.introduced);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, Vector3.one));

            yield return 0;
        }

        [UnityTest]
        public IEnumerator Intro_Animation_Speed_Is_Controlled_By_StartTime()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            activeAnimator.loopAnimation = null;
            a.startTime = 0.1f;
            triggerEvent("intro");
            yield return 0;
            //intro clip is growing from zero. 
            Assert.Less(instance.transform.localScale.x, 1.0);
            Assert.AreEqual(animator.GetFloat("IntroSpeed"), 10);
            //three times the on time should be ample. 
            yield return new WaitForSecondsOrTrue(0.3f, () => instance.transform.localScale.x == 1.0);
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, Vector3.one));
        }

        [UnityTest]
        public IEnumerator Intro_Animation_Waits_For_StartDelay()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            activeAnimator.loopAnimation = null;
            a.startTime = 0.1f;
            a.startDelay = 0.2f;
            triggerEvent("intro");
            yield return new WaitForSeconds(0.2f);
            //intro clip hasn't started yet, growing from zero. 
            Assert.Less(instance.transform.localScale.x, 0.3);
            yield return new WaitForSecondsOrTrue(0.3f, () => instance.transform.localScale.x == 1.0);
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, Vector3.one));
        }

        [UnityTest]
        public IEnumerator Intro_Animation_Fires_Introduced()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            activeAnimator.triggers[0].startTime = 0.1f;
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.3f, () => activeAnimator.introduced);
            Assert.IsTrue(activeAnimator.introduced);
        }

        [UnityTest]
        public IEnumerator Loop_Animation_Also_Starts_With_Intro()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.1f;
            //activeAnimator.loopAnimation = null;
            triggerEvent("intro");
            yield return 0;
            Assert.Less(instance.transform.localScale.x, 1.0);
            yield return new WaitForSecondsOrTrue(0.3f, () => activeAnimator.introduced);
            yield return 0; //one more frame. 
            Assert.AreEqual(animator.GetLayerWeight(loopLayer), 1.0);
            Assert.Greater(instance.transform.localScale.x, 1.0);
        }

        [UnityTest]
        public IEnumerator Loop_Animation_Starts_On_Normalized_X_Value_Frame()
        {
            // this is to help with random start times.
            // animations can also store a lot of mesh diversity information without making a lot of assets. 
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0;
            a.value = new Vec(0.5f, 0, 0); //this means the loop should be finished. 
            triggerEvent("intro");
            yield return 0;
            yield return 0;
            Assert.AreEqual(instance.transform.localScale.x, 2.0);
        }

        [UnityTest]
        public IEnumerator Global_Speed_Affects_Intro_Speed()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.1f;
            activeAnimator.globalSpeed = 10.0f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(animator.speed, 10.0);
            //Assert.Less(instance.transform.localScale.x, 1.0);
        }

        [UnityTest]
        public IEnumerator Knob_Trigger_Works_With_Animator()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            Trigger t = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            obj1.activeTrigger = new ActiveTrigger
            {
                startTime = 0,
                trigger = "test_knob",
                propertyName = "knobTrigger",
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                }
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            activeAnimator.triggers = new List<ActiveTrigger>();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.localRotation, Quaternion.identity);
            triggerKnob(t, 1);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.localRotation, Quaternion.Euler(0, 90, 0)));
        }

        [UnityTest]
        public IEnumerator Envelope_Knob_Trigger_Works_With_Animator()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            Trigger t = new Trigger
            {
                eventName = "test_envelope_knob",
                channel = 0,
                knob = 1
            };
            obj1.activeTrigger = new ActiveTrigger
            {
                startTime = 0,
                trigger = "test_envelope_knob",
                propertyName = "envelopeTrigger",
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                }
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            activeAnimator.setupActiveTriggers(new Obj(obj1));
            triggerEvent("intro");
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(getColor(instance), new Color(1, 1, 1)));
            yield return 0;
            triggerKnob(t, 1.0f);
            yield return 0;
            yield return 0;
            yield return 0;
            Assert.AreEqual(animator.GetLayerWeight(envelopeLayer), 1.0f);
            //defaults to one. 
            Assert.AreEqual(animator.GetFloat("EnvelopeSpeed"), 1.0f);
            Color envelopeColor = getColor(instance);
            Assert.AreEqual(envelopeColor.b, 1.0f);
            Assert.Less(envelopeColor.r, 1.0f); //kinda a short in the dark. 
        }

        [UnityTest]
        public IEnumerator Can_Cache_Envelope_Knob()
        {
            // this test was created before thiere was an active trigger. 
            // knobs need to be added. 
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            obj1.numInstances = 2;
            obj1.activeType = ActiveType.Next;
            Trigger t = new Trigger
            {
                eventName = "test_cached_envelope_knob",
                channel = 0,
                knob = 1
            };
            obj1.activeTrigger = new ActiveTrigger
            {
                startTime = 0,
                trigger = "test_cached_envelope_knob",
                propertyName = "envelopeTrigger",
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                }
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            activeAnimator.setupActiveTriggers(new Obj(obj1));
            GameObject instance2 = getPrefabInstanceByIndex(1);
            ActiveAnimator activeAnimator2 = instance2.GetComponent<ActiveAnimator>();
            Animator animator2 = instance2.GetComponent<Animator>();
            activeAnimator2.envelopeTrigger = "test_cached_envelope_knob";
            activeAnimator2._i = 1;
            activeAnimator2.setupActiveTriggers(new Obj(obj1));
            triggerKnob(t, 1.0f);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(animator.GetLayerWeight(envelopeLayer), 1.0);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(animator2.GetLayerWeight(envelopeLayer), 1.0);
            //TODO -- call a second intro on a second entrance. 
        }


        [UnityTest]
        public IEnumerator Can_Cache_Knob_For_Animator()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            obj1.numInstances = 2;
            obj1.activeType = ActiveType.Next;
            Trigger t = new Trigger
            {
                eventName = "test_cached_knob",
                channel = 0,
                knob = 1
            };
            obj1.activeTrigger = new ActiveTrigger
            {
                startTime = 0,
                trigger = "test_cached_knob",
                propertyName = "knobTrigger",
                knob = new Vec
                {
                    x = 0,
                    x2 = 1
                }
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            activeAnimator.setupActiveTriggers(new Obj(obj1));
            GameObject instance2 = getPrefabInstanceByIndex(1);
            ActiveAnimator activeAnimator2 = instance2.GetComponent<ActiveAnimator>();
            activeAnimator2.knobTrigger = "test_cached_knob";
            activeAnimator2._i = 1;
            activeAnimator2.setupActiveTriggers(new Obj(obj1));
            triggerKnob(t, 1.0f);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.localRotation, Quaternion.Euler(0, 90, 0)));
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance2.transform.localRotation, Quaternion.Euler(0, 90, 0)));
            //TODO -- call a second intro on a second entrance. 
        }


        [UnityTest]
        public IEnumerator Target_Animation_Called_From_ActiveTrigger_And_Returns_To_Zero()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_animator";
            a.onTime = 0.1f;
            a.offDelay = 0.1f;
            a.offTime = 0.1f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("trigger_animator");
            yield return new WaitForSecondsOrTrue(1, () => activeAnimator.on);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
            yield return new WaitForSecondsOrTrue(1, () => !activeAnimator.on);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, Vector3.zero));
        }

        [UnityTest]
        public IEnumerator Target_Animation_Can_Be_ReTriggered()
        {
            //target animation should retrigger from where it left off. 
            //tested manually and this works. 
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0;
            a.endTime = -1;
            a.trigger = "trigger_animator";
            a.onTime = 0.2f;
            a.offDelay = 0.1f;
            a.offTime = 0.3f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("trigger_animator");
            yield return new WaitForSecondsOrTrue(0.5f, () => instance.transform.position == new Vector3(0, 1, 0));
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
            yield return new WaitForSeconds(0.2f);
            //should come back but not all the way. 
            Assert.Less(instance.transform.position.y, 1.0f);
            //retrigger here. 
            float retriggeredPosition = instance.transform.position.y;
            triggerEvent("trigger_animator");
            yield return new WaitForSecondsOrTrue(0.3f, () => instance.transform.position.y > retriggeredPosition);
            Assert.Greater(instance.transform.position.y, retriggeredPosition);
        }


        [UnityTest]
        public IEnumerator Target_Animation_Remains_If_OffTime_Is_Negative()
        {
            //target animation should retrigger from where it left off. 
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_animator";
            a.onTime = 0.1f;
            a.offTime = -1;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("trigger_animator");
            yield return new WaitForSecondsOrTrue(1, () => activeAnimator.on);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
            yield return new WaitForSeconds(0.1f);
            //should not have moved since offTime is -1;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
        }


        //TODO - another retrigger without offTime -1  ( fixing the -1 broke it. ) 


        [UnityTest]
        public IEnumerator Retrigger_With_Negative_Offtime_Does_Nothing()
        {
            //target animation should retrigger from where it left off.
            //I have a note that changing offTime to -1 should loop the animation. 


            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_animator";
            a.onTime = 0.1f;
            a.offTime = -1;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent("trigger_animator");
            yield return new WaitForSecondsOrTrue(1, () => activeAnimator.on);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
            yield return new WaitForSeconds(0.1f);
            //should not have moved since offTime is -1;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
            triggerEvent("trigger_animator");
            yield return 0;
            yield return 0;
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
        }

        [UnityTest]
        public IEnumerator Target_Animation_Can_Call_Sustain_And_Return()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_sustain_animator";
            a.offTime = 0.1f;
            TriggerObj trigger_sustain = new TriggerObj
            {
                eventName = "trigger_sustain_animator",
                sustain = true,
            };
            TriggerObj trigger_sustain_off = new TriggerObj
            {
                eventName = "trigger_sustain_animator",
                sustain = true,
                noteOff = true
            };
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.white));
            triggerEvent(trigger_sustain);
            yield return new WaitForSecondsOrTrue(1, () => activeAnimator.on);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.white));
            yield return new WaitForSecondsOrTrue(0.5f, () => animator.GetLayerWeight(sustainLayer) == 1.0f);
            //sustain is throbbing red. 
            Assert.Less(getColor(instance).g, 1.0f);
            triggerEvent(trigger_sustain_off);
            yield return new WaitForSecondsOrTrue(1.0f, () => !activeAnimator.on);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, Vector3.zero));
        }

        [UnityTest]
        public IEnumerator End_Animation_Clip_Is_Called()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.3f;
            a.endDelay = 0.0f;
            a.endTime = 0.3f;
            triggerEvent("intro");
            yield return 0;

            Debug.Log("animator cube is active: " + instance.activeInHierarchy + " name: " + instance.gameObject.name);
            //intro clip is growing from zero. 
            Assert.Less(instance.transform.localScale.x, 1.0f);
            // yield return new WaitForSecondsOrTrue(1.0f, () => activeAnimator.ended);
            // Assert.Less(instance.transform.localScale.x, 0.5f);
        }

        [UnityTest]
        public IEnumerator Hide_Hides_Parent_And_But_Instantiate_Shows_It()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.1f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            Assert.IsTrue(instance.activeInHierarchy);
            activeAnimator.Hide();
            yield return 0;
            Assert.AreEqual(instance.transform.position, activeAnimator.o.nullVector);
            Assert.IsFalse(instance.activeInHierarchy);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            Assert.IsTrue(instance.activeInHierarchy);
        }

        [UnityTest]
        public IEnumerator End_Animation_Is_Called_After_Sustain_Is_Released()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro_sustain";
            initializeObjs();
            a.startTime = 0.1f;
            a.endTime = 0.1f;
            TriggerObj trigger_intro_sustain = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
            };
            TriggerObj trigger_intro_sustain_off = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
                noteOff = true
            };
            triggerEvent(trigger_intro_sustain);
            yield return 0;
            //intro clip is growing from zero. 
            Assert.Less(instance.transform.localScale.x, 1.0);
            yield return new WaitForSecondsOrTrue(0.3f, () => instance.transform.localScale.x == 1.0);
            Assert.Greater(instance.transform.localScale.x, 1.0); //there is a sustain animation that animates the scale between 1 and 2.
            //stays sustained until release event. 
            yield return new WaitForSeconds(0.5f);
            Assert.Greater(instance.transform.localScale.x, 1.0);
            triggerEvent(trigger_intro_sustain_off);
            yield return new WaitForSeconds(0.2f);
            Assert.Less(instance.transform.localScale.x, 1.0f);
        }

        [UnityTest]
        public IEnumerator Intro_Is_Called_If_ReTriggered_While_Ending_Not_Killed()
        {
            //responding to a bug in which it killed itself upon reinstantiate.
            //edge case. ( usually many instances would take care of this. )  
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.startTime = 0.3f;
            a.endDelay = 0.0f;
            a.endTime = 1.0f;
            a.hideOnComplete = true; //vital to reproducing the bugg. 
            activeAnimator.loopAnimation = null;
            triggerEvent("intro");
            yield return 0;
            //intro clip is growing from zero. 
            Assert.Less(instance.transform.localScale.x, 1.0f);
            yield return new WaitForSecondsOrTrue(1.0f, () => activeAnimator.ending);
            yield return 0;
            yield return 0;
            yield return 0;
            //check if actuall ending. 
            Assert.IsTrue(activeAnimator.ending);
            Assert.Less(instance.transform.localScale.x, 1.0f);
            triggerEvent("intro");
            yield return 0;
            yield return 0;
            Assert.IsTrue(instance.activeInHierarchy);
        }

        private Color getColor(GameObject instance)
        {
            return instance.GetComponent<Renderer>().material.GetColor(colorProperty);
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            flybyController.quickSetup(objs);
            instance = getFirstPrefabInstance();
            activeAnimator = instance.GetComponent<ActiveAnimator>();
            animator = instance.GetComponent<Animator>();
            activeAnimator.triggers = new List<ActiveTrigger>();
            a = new ActiveTrigger();
            activeAnimator.triggers.Add(a);
        }
    }
}
