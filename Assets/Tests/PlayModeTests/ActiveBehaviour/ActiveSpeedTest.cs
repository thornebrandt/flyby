using System.Collections;
using flyby.Active;
using flyby.Core;
using flyby.Controllers;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;

using UnityEngine.TestTools;

namespace Tests
{
    public class ActiveSpeedTest : PlayModeTests
    {
        private GameObject go;
        private GameObject go2;
        private ActiveSpeed activeSpeed;
        private ActiveSpeed activeSpeed2;
        private Obj o;
        private ActiveTrigger a;
        private TriggerObj t;
        private TriggerSourceController triggerSourceController = new TriggerSourceController();


        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            this.activeSpeed = getFirstInstance().GetComponent<ActiveSpeed>();
            instance = getFirstInstance();
        }

        [TearDown]
        public void Teardown()
        {
            this.teardown();
        }

        [Test]
        public void ActiveSpeed_Exists()
        {
            obj1 = createBlankObj();
            obj1.triggerSpeed = new ActiveTrigger();
            initializeObjs();
            Assert.IsNotNull(activeSpeed);
        }

        [UnityTest]
        public IEnumerator Two_Identical_Objs_Move_At_Same_Speed()
        {
            obj1 = createBlankObj();
            obj1.triggerSpeed = new ActiveTrigger();
            obj1.triggerSpeed.value = new Vec(1, 0, 0);
            obj1.trigger = "intro";

            NullObj obj2 = createBlankObj();
            obj2.triggerSpeed = new ActiveTrigger();
            obj2.triggerSpeed.value = new Vec(1, 0, 0);
            obj2.trigger = "intro";

            initializeObjs();
            triggerEvent("intro");
            GameObject instance2 = getInstanceByObjIndex(1);
            Assert.AreEqual(instance.transform.position.x, 0);
            Assert.AreEqual(instance2.transform.position.x, 0);
            yield return new WaitForSeconds(0.1f);
            Assert.Greater(instance.transform.position.x, 0);
            Assert.Greater(instance2.transform.position.x, 0);
            Assert.AreEqual(instance.transform.position.x, instance2.transform.position.x);
        }

        [UnityTest]
        public IEnumerator Resets_Value_On_Second_Intro()
        {
            obj1 = createBlankObj();
            obj1.triggerSpeed = new ActiveTrigger();
            obj1.triggerSpeed.start = new Vec(0, 0, 0);
            obj1.triggerSpeed.value = new Vec(1, 0, 0);
            obj1.triggerSpeed.startDelay = 0.2f;
            obj1.triggerSpeed.startTime = 2;
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(instance.transform.position.x, 0);
            yield return new WaitForSeconds(1.0f);
            Assert.Greater(instance.transform.position.x, 0);
            instance.transform.position = Vector3.zero;
            triggerEvent("intro");
            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(instance.transform.position.x, 0);
        }

        [UnityTest]
        public IEnumerator Active_Trigger_Does_Not_Trigger_Speed()
        {
            obj1 = createBlankObj();
            obj1.triggerSpeed = new ActiveTrigger(); //creating triggerSpeed but not settting anyy values. 
            obj1.activeTrigger = new ActiveTrigger();
            obj1.activeTrigger.value = new Vec(1, 0, 0);
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSeconds(1.0f);
            //speed should not have been triggered.
            Assert.AreEqual(instance.transform.position.x, 0);
        }

        [UnityTest]
        public IEnumerator Rotation_Does_Not_Affect_Speed()
        {
            obj1 = createBlankObj();
            obj1.triggerRotation = new ActiveTrigger();
            obj1.triggerRotation.value = new Vec(0, 0.1f, 0);
            obj1.triggerSpeed = new ActiveTrigger();
            obj1.triggerSpeed.value = new Vec(1, 0, 0);
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.position.x, 0);
            Assert.AreEqual(instance.transform.position.z, 0);
            yield return new WaitForSeconds(1.0f);
            Assert.Greater(instance.transform.position.x, 0);
            Assert.AreEqual(instance.transform.position.z, 0);
        }
    }
}