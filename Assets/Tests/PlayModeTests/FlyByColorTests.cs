using System.Collections;
using System.Collections.Generic;
using flyby.Active;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyByColorTests : PlayModeTests
    {
        ActiveColor activeColor;
        private string emissiveProperty = "_EmissiveColor";
        private Material material;

        [SetUp]
        public void Setup()
        {
            this.setup();

        }

        [TearDown]
        public void Teardown()
        {
            //TODO - need to investiaget why the tests can contaminate each other with false positives for preset names. 
            obj1 = null;
            flybyController.killAll();
            flybyController.activeBehaviourController = null; //seems to be causing issues with the next test.
        }

        [Test]
        public void Quick_Setup_With_Manual_Objs()
        {
            obj1 = createBlankObj();
            obj1.id = "new_obj";
            initializeObjs();
            Assert.AreEqual(flybyController.currentPart.objs.Count, 1);
            Assert.AreEqual(flybyController.currentPart.objs[0].id, "new_obj");
        }

        [Test]
        public void Changes_Color_To_Start_On_Intro()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                startColor = new Col(1, 0, 0),
                color = new Col(0, 0, 1),
                trigger = "triggerColor"
            };
            initializeObjs();
            triggerEvent("intro");
            triggerEvent("triggerColor");
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.red));
        }

        [Test]
        public void Changes_Color_To_Color_On_Intro_If_No_StartColor()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col(0, 0, 1),
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(this.getColor(instance), Color.blue);
        }


        [UnityTest]
        public IEnumerator Trigger_Active_Color_Blends_With_Fading_In_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                startColor = new Col(0, 0, 0),
                color = new Col(1, 0, 0),
                targetColor = new Col(0, 0, 1),
                trigger = "triggerColor",
                onTime = 0.1f,
                offTime = -1,
                startTime = 0.3f
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(this.getColor(instance), Color.black);
            triggerEvent("triggerColor");
            yield return new WaitForSecondsOrTrue(0.2f, () => this.getColor(instance) == Color.blue);
            Assert.AreEqual(this.getColor(instance), Color.blue);
            yield return new WaitForSeconds(0.2f);
            Assert.AreEqual(this.getColor(instance), Color.blue);
        }

        [UnityTest]
        public IEnumerator Trigger_Active_Color_With_No_OffTime_Remains_TriggerColor()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col(0, 0, 0),
                targetColor = new Col(0, 0, 1),
                trigger = "triggerColor",
                onTime = 0.01f,
                offTime = -1,
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(this.getColor(instance), Color.black);
            triggerEvent("triggerColor");
            yield return new WaitForSecondsOrTrue(0.03f, () => this.getColor(instance) == Color.blue);
            Assert.AreEqual(this.getColor(instance), Color.blue);
            yield return new WaitForSecondsOrTrue(1.0f, () => this.getColor(instance) != Color.blue);
            Assert.AreEqual(this.getColor(instance), Color.blue);
        }


        [UnityTest]
        public IEnumerator Fade_Out_Color_Works()
        {

            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col(1, 0, 0),
                endColor = new Col(0, 0, 1),
                startTime = 0,
                endTime = 0.1f,
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(this.getColor(instance), Color.red);
            yield return new WaitForSecondsOrTrue(0.2f, () => this.getColor(instance) == Color.blue);
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.blue));
        }


        [UnityTest]
        public IEnumerator Fades_In_Color_If_StartColor_And_Color_Are_Present()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                startColor = new Col(1, 0, 0),
                color = new Col(0, 0, 1),
                trigger = "triggerColor"
            };
            initializeObjs();
            triggerEvent("intro");
            triggerEvent("triggerColor");
            Assert.AreEqual(this.getColor(instance), Color.red);
            yield return new WaitForSeconds(0.6f);
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.blue));
        }

        [UnityTest]
        public IEnumerator Color_Becomes_Base_Color_On_Kill()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col(1, 0, 0),
                endColor = new Col(0, 0, 1),
                startTime = 0,
                endTime = 0.1f,
                trigger = "triggerColor"
            };
            initializeObjs();
            triggerEvent("intro");
            triggerEvent("triggerColor");
            yield return new WaitForSeconds(0.1f);
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.blue));
            activeColor.Kill();
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Triggers_Active_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col(1, 0, 0),
                targetColor = new Col(0, 0, 1),
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
                trigger = "triggerColor"
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(this.getColor(instance), Color.red);
            triggerEvent("triggerColor");
            yield return new WaitForSecondsOrTrue(1, () => this.getColor(instance) == Color.blue);
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.blue));
            yield return new WaitForSecondsOrTrue(1, () => this.getColor(instance) == Color.red);
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Retains_Original_Material_Color_After_Trigger_If_No_Color_Is_Present()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlackSphere";
            obj1.triggerColor = new ActiveTrigger
            {
                targetColor = new Col(0, 1, 0),
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
                trigger = "triggerColor"
            };

            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(this.getColor(instance), Color.black); //this might be flip-flopping between clear and black. 
            triggerEvent("triggerColor");
            yield return new WaitForSecondsOrTrue(1, () => this.getColor(instance) == Color.green);
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.green));
            yield return new WaitForSecondsOrTrue(1, () => this.getColor(instance) == Color.clear);
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.black)); //this might be flip-flopping between clearn and black.
        }

        // need to register triggers. 
        [UnityTest]
        public IEnumerator Knob_Trigger_Works_For_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                startColor = new Col("black"),
                knobColor = new Col
                {
                    r = 0,
                    r2 = 1
                },
                trigger = "test_knob"
            };
            Trigger t2 = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, 1);
            yield return 0;
            GameObject instance = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[0]);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Same_Knob_Trigger_Can_Trigger_Multiple_Colors()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                trigger = "knob_trigger_event",
                color = new Col(),
                knobColor = new Col
                {
                    r = 0,
                    r2 = 1,
                },
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        trigger = "knob_trigger_event",
                        knobColor = new Col{
                            g = 0,
                            g2 = 1
                        }
                    },
                    new SecondaryActiveTrigger {
                        trigger = "knob_trigger_event",
                        knobColor = new Col{
                            b = 0,
                            b2 = 1
                        }
                    },
                }
            };
            Trigger knobTrigger = new Trigger
            {
                eventName = "knob_trigger_event",
                channel = 0,
                knob = 1
            };
            addTrigger(knobTrigger);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.black));
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, 0.5f);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(this.getColor(instance), Color.gray));
        }

        [UnityTest]
        public IEnumerator Knob_Caching_Works_With_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                startColor = new Col("black"),
                knobColor = new Col
                {
                    r = 0,
                    r2 = 1
                },
                trigger = "test_knob"
            };
            obj1.numInstances = 3;
            obj1.activeType = ActiveType.All;
            Trigger t2 = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, 1);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            // GameObject instance = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[0]);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Knob_Caching_Works_With_Color_Without_Base_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                knobColor = new Col
                {
                    r = 0,
                    r2 = 1
                },
                trigger = "test_knob"
            };
            obj1.asset = "BlackCube";
            obj1.bundle = "TestPrefabs";
            obj1.activeType = ActiveType.All;
            Trigger t2 = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, 1);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            // GameObject instance = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[0]);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Knob_Trigger_Without_Base_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                knobColor = new Col
                {
                    r = 0,
                    r2 = 1
                },
                trigger = "test_knob"
            };
            obj1.asset = "BlackCube";
            obj1.bundle = "TestPrefabs";
            obj1.activeType = ActiveType.All;
            Trigger t2 = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            triggerEvent("intro");
            yield return 0;
            midiController.OnMidiKnob(0, 1, 1);
            yield return 0;
            // GameObject instance = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[0]);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator ActiveType_All_Affects_All_Knobs()
        {
            obj1 = createBlankObj();
            obj1.id = "activeType_all_test_obj";
            obj1.trigger = "intro";
            obj1.numInstances = 3;
            obj1.triggerColor = new ActiveTrigger
            {
                knobColor = new Col
                {
                    r = 0,
                    r2 = 1
                },
                trigger = "test_knob"
            };
            obj1.activeType = ActiveType.All;
            Trigger t = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            triggerEvent("intro");
            triggerEvent("intro");
            yield return 0;
            midiController.OnMidiKnob(0, 1, 1);
            yield return 0;
            GameObject instance1 = getFirstPrefabInstance();
            GameObject instance2 = getPrefabInstanceByIndex(1);
            Assert.AreEqual(getColor(instance1), Color.red);
            Assert.AreEqual(getColor(instance2), Color.red);
        }

        [UnityTest]
        public IEnumerator Blank_Trigger_Color_Does_Not_Turn_Material_Black()
        {
            //bugfix. 
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger { };
            obj1.asset = "WhiteCube";
            obj1.bundle = "TestPrefabs";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            // GameObject instance = getFirstPrefabInstance();
            //prefab is a white cube, nothing has been affected, it should remain white. 
            Assert.AreEqual(getColor(instance), Color.white);
        }

        [UnityTest]
        public IEnumerator Knob_Trigger_Works_With_Multiple_Instances_Without_Base_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                knobColor = new Col
                {
                    r = 0,
                    r2 = 1
                },
                trigger = "test_knob",
                activeType = ActiveType.All
            };
            obj1.numInstances = 3;
            obj1.asset = "BlackCube";
            obj1.bundle = "TestPrefabs";
            obj1.activeType = ActiveType.All;
            Trigger t2 = new Trigger
            {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            triggerEvent("intro");
            yield return 0;
            midiController.OnMidiKnob(0, 1, 1); //sending a "1" knob value. 
            yield return 0;
            GameObject instance1 = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[0]);
            //first instance is red after knob. 
            Assert.IsTrue(Math.RoughEquals(getColor(instance1), Color.red));
            triggerEvent("intro");
            yield return 0;
            GameObject instance2 = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[1]);
            //second instance is red from first knob event. 
            Assert.IsTrue(Math.RoughEquals(getColor(instance2), Color.red));
            midiController.OnMidiKnob(0, 1, 0); //sending a "0" knob value. 
            yield return 0;
            GameObject instance3 = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[2]);
            //third instance should go back down to black. 
            Assert.IsTrue(Math.RoughEquals(getColor(instance3), Color.black));
        }

        [UnityTest]
        public IEnumerator Multiple_Knob_Triggers_Work()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                knobColor = new Col
                {
                    r = 0,
                    r2 = 1
                },
                trigger = "test_knob_1"
            };
            obj1.triggerColor.triggers = new List<SecondaryActiveTrigger>();
            obj1.triggerColor.triggers.Add(
                new SecondaryActiveTrigger
                {
                    knobColor = new Col
                    {
                        g = 0,
                        g2 = 1
                    },
                    trigger = "test_knob_2"
                }
            );
            Trigger t1 = new Trigger
            {
                eventName = "test_knob_1",
                channel = 0,
                knob = 1
            };
            Trigger t2 = new Trigger
            {
                eventName = "test_knob_2",
                channel = 0,
                knob = 2,
            };
            addTrigger(t1);
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            triggerEvent("intro");
            yield return 0;
            midiController.OnMidiKnob(0, 1, 1); //sending 1 to knob assigned to red.
            yield return 0;
            midiController.OnMidiKnob(0, 2, 1); //sending 1 to know assigned to green.
            yield return 0;
            // GameObject instance = getFirstPrefabInstance();
            Assert.IsTrue(Math.RoughEquals(getColor(instance), new Color(1, 1, 0)));
        }

        [UnityTest]
        public IEnumerator Trigger_Works_For_Color_From_Black()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col { },
                targetColor = new Col
                {
                    r = 1,
                },
                offDelay = 0.1f,
                trigger = "triggerRed",
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRed");
            yield return new WaitForSecondsOrTrue(1, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Trigger_Works_For_Emissive_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                targetColor = new Col
                {
                    r = 1
                },
                offTime = -1,
                onTime = 0.2f,
                trigger = "triggerRedGlow"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRedGlow");
            yield return new WaitForSeconds(0.3f);
            Assert.IsTrue(Math.RoughEquals(getEmissiveColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Emissive_Color_Should_Start_As_Black()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col(),
                startTime = 0
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(getEmissiveColor(instance), Color.black));
        }

        [UnityTest]
        public IEnumerator Emmissive_Color_Can_Be_ReTriggered_And_Returns_To_Black()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                trigger = "triggerColor",
                onTime = 0.2f,
                offTime = 0.3f,
                color = new Col
                {
                    gray = 0,
                },
                targetColor = new Col
                {
                    r = 1,
                }
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.IsTrue(Math.RoughEquals(getEmissiveColor(instance), Color.black));
            yield return 0;
            triggerEvent("triggerColor");
            yield return new WaitForSeconds(0.1f);
            triggerEvent("triggerColor");
            Assert.Greater(getColor(instance).r, 0);
            Assert.Greater(getEmissiveColor(instance).r, 0);
            //testing if color returns to black as well. 
            yield return new WaitForSecondsOrTrue(1, () => getColor(instance) == Color.black);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.black));
            Assert.IsTrue(Math.RoughEquals(getEmissiveColor(instance), Color.black));
        }

        [UnityTest]
        public IEnumerator Emissive_Color_Returns_To_Black_After_Trigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col(),
                trigger = "triggerGlow",
                onTime = 0.2f,
                offDelay = 0.2f,
                offTime = 0.2f,
                targetColor = new Col
                {
                    r = 1
                }
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.IsTrue(Math.RoughEquals(getEmissiveColor(instance), Color.black) || Math.RoughEquals(getEmissiveColor(instance), Color.clear));
            triggerEvent("triggerGlow");
            yield return new WaitForSecondsOrTrue(0.3f, () => getEmissiveColor(instance) == Color.red);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(getEmissiveColor(instance), Color.red, true));
            yield return new WaitForSeconds(0.5f);
            Assert.IsTrue(Math.RoughEquals(getEmissiveColor(instance), Color.black));
        }

        [UnityTest]
        public IEnumerator Preset_Colors_Work_With_Triggers()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col { },
                targetColor = new Col
                {
                    color = "red"
                },
                offDelay = 0.1f,
                trigger = "triggerRed",
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRed");
            yield return new WaitForSecondsOrTrue(1, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
        }

        [UnityTest]
        public IEnumerator Active_Trigger_Presets_Work()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                id = "triggerRedPreset",
                color = new Col { },
                targetColor = new Col
                {
                    color = "red"
                },
                offDelay = 0.1f,
                trigger = "triggerRed",
            };

            NullObj obj2 = createBlankObj();
            obj2.trigger = "intro";
            obj2.id = "second_obj";
            obj2.triggerColor = new ActiveTrigger
            {
                preset = "triggerRedPreset"
            };

            NullObj obj3 = createBlankObj();
            obj3.trigger = "intro";
            obj3.id = "third_obj";
            obj3.triggerColor = new ActiveTrigger
            {
                preset = "triggerRedPreset",
                targetColor = new Col
                {
                    color = "green"
                }
            };

            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRed");
            GameObject instance2 = Obj.getPrefabInstance(flybyController.currentPart.objs[1].instances[0]);
            GameObject instance3 = Obj.getPrefabInstance(flybyController.currentPart.objs[2].instances[0]);
            yield return new WaitForSecondsOrTrue(1, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
            Assert.IsTrue(Math.RoughEquals(getColor(instance2), Color.red));
            Assert.IsTrue(Math.RoughEquals(getColor(instance3), Color.green)); //testing that the preset is overridden.
        }

        [UnityTest]
        public IEnumerator Trigger_Color_Works_With_A_Custom_Shader()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "ColorCube";
            obj1.trigger = "intro";
            obj1.id = "custom_shader";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col { },
                propertyName = "_Color",
                targetColor = new Col
                {
                    r = 1
                },
                offTime = -1,
                onTime = 0.2f,
                offDelay = 0.1f,
                trigger = "triggerRed",
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(getColorProperty(instance, "_Color"), Color.black));
            triggerEvent("triggerRed");
            yield return new WaitForSecondsOrTrue(1, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColorProperty(instance, "_Color"), Color.red));
        }

        [UnityTest]
        public IEnumerator Presets_Work_With_Custom_Shader()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "ColorCube";
            obj1.trigger = "intro";
            obj1.id = "custom_shader";
            obj1.triggerColor = new ActiveTrigger
            {
                id = "triggerRedPreset",
                color = new Col { },
                propertyName = "_Color",
                targetColor = new Col
                {
                    color = "red"
                },
                offTime = -1,
                onTime = 0.2f,
                offDelay = 0.1f,
                trigger = "triggerRed",
            };

            //adding activeTrigger in case this causes complications.
            obj1.activeTrigger = new ActiveTrigger
            {
                value = new Vec
                {
                    x = 1
                },
                target = new Vec
                {
                    x = 1
                },
                propertyName = "_Red"
            };

            NullObj obj2 = createBlankObj();
            obj2.trigger = "intro";
            obj2.id = "second_obj";
            obj2.triggerColor = new ActiveTrigger
            {
                preset = "triggerRedPreset",
                id = "triggerGreenPreset",
                targetColor = new Col
                {
                    color = "green"
                }
            };

            NullObj obj3 = createBlankObj();
            obj3.trigger = "intro";
            obj3.id = "third_obj";
            obj3.triggerColor = new ActiveTrigger
            {
                preset = "triggerGreenPreset",
                targetColor = new Col
                {
                    color = "blue"
                }
            };

            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(getColorProperty(instance, "_Color"), Color.black));
            triggerEvent("triggerRed");
            GameObject instance2 = Obj.getPrefabInstance(flybyController.currentPart.objs[1].instances[0]);
            GameObject instance3 = Obj.getPrefabInstance(flybyController.currentPart.objs[2].instances[0]);
            yield return new WaitForSecondsOrTrue(1, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColorProperty(instance, "_Color"), Color.red));
            Assert.IsTrue(Math.RoughEquals(getColorProperty(instance2, "_Color"), Color.green));
            Assert.IsTrue(Math.RoughEquals(getColorProperty(instance3, "_Color"), Color.blue));
        }

        [UnityTest]
        public IEnumerator Secondary_Active_Trigger_Presets_Work()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger{
                        id = "trigger_red_preset2",
                        color = new Col { },
                        targetColor = new Col {
                            color = "red",
                        },
                        offDelay = 0.1f,
                        trigger = "triggerRed"
                    }
                }
            };

            NullObj obj2 = createBlankObj();
            obj2.trigger = "intro";
            obj2.triggerColor = new ActiveTrigger
            {
                preset = "trigger_red_preset2"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRed");
            GameObject instance2 = Obj.getPrefabInstance(flybyController.currentPart.objs[1].instances[0]);
            ActiveColor activeColor2 = instance2.GetComponent<ActiveColor>();
            yield return new WaitForSecondsOrTrue(1, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColor(instance2), Color.red));
        }

        [UnityTest]
        public IEnumerator Secondary_Trigger_Works_For_Color_From_Black()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                trigger = "triggerRed",
                color = new Col { },
                targetColor = new Col
                {
                    r = 1,
                },
                startTime = 0,
                onTime = 0.1f,
                offDelay = 0.1f,
                triggers = new List<SecondaryActiveTrigger> {
                new SecondaryActiveTrigger {
                    trigger = "triggerGreen",
                    targetColor = new Col {
                    g = 1
                    },
                    onTime = 0.1f,
                    offDelay = 0.25f,
                    }
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRed");
            yield return new WaitForSecondsOrTrue(1, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
            triggerEvent("triggerGreen");
            yield return new WaitForSecondsOrTrue(1, () => Math.RoughEquals(getColor(instance), Color.green, false));
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.green));
        }

        [UnityTest]
        public IEnumerator Secondary_Trigger_Transitions_From_Primary_Trigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.id = "help_me";
            obj1.triggerColor = new ActiveTrigger
            {
                trigger = "triggerRed",
                color = new Col { },
                targetColor = new Col
                {
                    r = 1,
                },
                offTime = -1,
                onTime = 0.2f,
                startTime = 0,
                triggers = new List<SecondaryActiveTrigger> {
                    new SecondaryActiveTrigger {
                        trigger = "triggerGreen",
                        targetColor = new Col {
                            g = 1
                        },
                        onTime = 0.5f,
                        offDelay = 0.1f,
                    }
                }
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("triggerRed");
            yield return new WaitForSecondsOrTrue(0.3f, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
            triggerEvent("triggerGreen");
            yield return new WaitForSeconds(0.2f);
            yield return new WaitForSecondsOrTrue(0.1f, () => getColor(instance).r > 0.0f);
            //halfway into the transition should be between green and red should still have red values. 
            Assert.Greater(getColor(instance).r, 0.0f);
        }

        [UnityTest]
        public IEnumerator Secondary_Sustain_Trigger_Returns_To_Base_Color_Before_Finishing()
        {
            //observes bug where a sustain 
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                targetColor = new Col
                {
                    r = 1,
                },
                trigger = "triggerColor",
                triggers = new List<SecondaryActiveTrigger> {
                new SecondaryActiveTrigger {
                targetColor = new Col {
                g = 1,
                },
                trigger = "secondarySustainColor",
                onTime = 2.0f,
                offTime = 0.1f,
                }
                },
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            initializeObjs();
            TriggerObj sustainTriggerObj = new TriggerObj
            {
                eventName = "secondarySustainColor",
                sustain = true

            };
            TriggerObj noteOffObj = new TriggerObj
            {
                eventName = "secondarySustainColor",
                sustain = true,
                noteOff = true
            };
            triggerEvent("intro");
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.white));
            triggerEvent("triggerColor");
            yield return new WaitForSecondsOrTrue(1, () => activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
            triggerEvent(sustainTriggerObj);
            yield return new WaitForSeconds(0.3f);
            //cut it short and then trigger off. 
            triggerEvent(noteOffObj);
            yield return new WaitForSecondsOrTrue(1, () => !activeColor.on);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.white));
        }

        [UnityTest]
        public IEnumerator Sustain_Intro_Holds_On_BaseColor_Before_Exit()
        {
            obj1 = createBlankObj();
            obj1.trigger = "introSustain";
            obj1.triggerColor = new ActiveTrigger
            {
                startColor = new Col
                {
                    r = 1
                },
                color = new Col
                {
                    g = 1,
                },
                endColor = new Col
                {
                    b = 1,
                },
                startTime = 0.1f,
                endTime = 0.1f,
            };
            TriggerObj sustainTriggerObj = new TriggerObj
            {
                eventName = "introSustain",
                sustain = true
            };
            TriggerObj noteOffTriggerObj = new TriggerObj
            {
                eventName = "introSustain",
                sustain = true,
                noteOff = true
            };
            initializeObjs();
            triggerEvent(sustainTriggerObj);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.red));
            yield return new WaitForSecondsOrTrue(1, () => activeColor.introduced);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.green));
            yield return new WaitForSeconds(0.3f);
            //still green. 
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.green));
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => activeColor.ended);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.blue));
        }

        [UnityTest]
        public IEnumerator Instantiation_Works_Without_Start()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                color = new Col
                {
                    g = 1
                },
                endColor = new Col
                {
                    b = 1,
                },
                startTime = 0.1f,
                endTime = 0.1f,
                endDelay = 0.1f,
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(1, () => activeColor.introduced);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.green));
        }

        [UnityTest]
        public IEnumerator Hide_On_Complete_Hides_Object()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger
            {
                startColor = new Col
                {
                    r = 1,
                },
                color = new Col
                {
                    g = 1
                },
                endColor = new Col
                {
                    b = 1,
                },
                startTime = 0.1f,
                endTime = 0.1f,
                endDelay = 0.1f,
                hideOnComplete = true
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(1, () => activeColor.introduced);
            Assert.IsTrue(Math.RoughEquals(getColor(instance), Color.green, true));
            yield return new WaitForSecondsOrTrue(1, () => activeColor.ended);
            Assert.IsFalse(instance.activeInHierarchy);
        }

        private Color getColor(GameObject instance)
        {
            return instance.GetComponent<Renderer>().material.GetColor(colorProperty);
        }

        private Color getColorProperty(GameObject instance, string property)
        {
            return instance.GetComponent<Renderer>().material.GetColor(property);
        }

        private Color getEmissiveColor(GameObject instance)
        {
            return instance.GetComponent<Renderer>().material.GetColor(emissiveProperty);
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            instance = getFirstPrefabInstance();
            activeColor = instance.GetComponent<ActiveColor>();
        }
    }

}