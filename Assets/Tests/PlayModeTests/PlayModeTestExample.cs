using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    public class PlayModeTestExample {
        [Test]
        public void GameObject_Created_With_Given_Will_Have_The_Name () {
            var go = new GameObject ("MyGameObject");
            Assert.AreEqual ("MyGameObject", go.name);
        }

        [UnityTest]
        public IEnumerator GameObject_With_RigidBody_Will_Be_Affected_By_Physics () {
            Physics.autoSimulation = true; //turned off
            var go = new GameObject ();
            go.AddComponent<Rigidbody> ();
            var originalPosition = go.transform.position.y;

            yield return new WaitForFixedUpdate ();

            Assert.AreNotEqual (originalPosition, go.transform.position.y);
        }
    }
}