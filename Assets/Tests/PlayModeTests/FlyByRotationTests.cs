using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Active;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyByRotationTests : PlayModeTests
    {

        ActiveRotation activeRotation;  //first activeRotation.

        [SetUp]
        public void Setup()
        {
            go = GameObject.Find("FlyByController");
            if (go == null)
            {
                go = new GameObject("FlyByController");
            }
            flybyController = go.GetComponent<FlyByController>();
            if (flybyController == null)
            {
                flybyController = go.AddComponent<FlyByController>();
            }

            flybyController.configPath = "config.sample.json"; //empty
            if (assetController == null)
            {
                AssetController assetController = new AssetController();
                assetController.prefabCache = new Dictionary<string, GameObject>();
                camObj = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
            }
            flybyController.camObj = camObj;
            KeyboardController keyboardController = go.GetComponent<KeyboardController>();
            if (keyboardController == null)
            {
                go.AddComponent<KeyboardController>();
                go.AddComponent<MidiController>();
                go.AddComponent<ActiveBehaviourController>();
            }
            flybyController.initialize();
            objs = new List<NullObj>();
            NullObj obj1 = new NullObj();
            triggers = new List<Trigger>();
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
        }

        [UnityTest]
        public IEnumerator Intro_Works_For_Rotation()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                start = new Vec
                {
                    x = 90,
                },
                startTime = 0.01f,
                value = new Vec { },
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(0.1f, () => activeRotation.introduced);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.identity));
        }

        [UnityTest]
        public IEnumerator Value_Works_For_Rotation()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                value = new Vec
                {
                    x = 90
                },
                startTime = 0,
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(90, 0, 0)));
            yield return new WaitForSecondsOrTrue(0.1f, () => activeRotation.introduced);
        }

        [UnityTest]
        public IEnumerator If_No_Start_Rotate_Starts_As_Value()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                value = new Vec
                {
                    x = 90
                },
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(90, 0, 0)));
        }

        [UnityTest]
        public IEnumerator Start_Delay_Delays_Value_Even_If_Start_Time_Is_Zero()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                start = new Vec
                {
                    x = 0
                },
                value = new Vec
                {
                    x = 90
                },
                startTime = 0,
                startDelay = 0.2f
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.rotation, Quaternion.identity);
        }


        [UnityTest]
        public IEnumerator Value_Works_For_ReInstanced_Instance()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                value = new Vec
                {
                    x = 45
                },
                startTime = 0,
            };
            obj1.numInstances = 1; //redudant, but part of the tests. 
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("intro"); //triggering the same instance a second time. 
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(45, 0, 0)));
            yield return new WaitForSecondsOrTrue(0.1f, () => activeRotation.introduced);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(45, 0, 0)));
        }


        [UnityTest]
        public IEnumerator Trigger_Works_For_Rotation_From_Zero()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                value = new Vec { },
                target = new Vec
                {
                    y = 90
                },
                startTime = 0,
                onTime = 0.1f,
                offDelay = 0.1f,
                trigger = "trigger_rotation"
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("trigger_rotation");
            yield return 0;
            Quaternion targetRotation = Quaternion.Euler(0, 90, 0);
            //GameObject instance = getFirstRotateScaleInstance();
            //ActiveRotation activeRotation = instance.GetComponent<ActiveRotation>();
            yield return new WaitForSecondsOrTrue(0.15f, () => activeRotation.on);
            Assert.IsTrue(activeRotation.on);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 90, 0)));
        }

        [UnityTest]
        public IEnumerator Relative_Rotation_Adds_To_Previous_Rotation_Exactly()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                value = new Vec { },
                target = new Vec
                {
                    y = 15,
                    relative = true
                },
                startTime = 0,
                onTime = 0.1f,
                offTime = -1,
                trigger = "trigger_rotation"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("trigger_rotation");
            Quaternion targetRotation = Quaternion.Euler(0, 15, 0);
            yield return new WaitForSecondsOrTrue(0.15f, () => activeRotation.on);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 15, 0)));
            yield return new WaitForSeconds(0.15f);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 15, 0)));
            //still in same spot. 
            triggerEvent("trigger_rotation");
            yield return new WaitForSeconds(0.15f);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 30, 0)));
        }


        [UnityTest]
        public IEnumerator Relative_Rotation_With_Multiple_Rounds_To_Exact_Number()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                value = new Vec { },
                target = new Vec
                {
                    y = 15,
                    relative = true,
                    multiple = new Vector3(
                        0,
                        15,
                        0
                    )
                },
                startTime = 0,
                onTime = 0.15f,
                offTime = -1,
                trigger = "trigger_rotation"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("trigger_rotation");
            yield return new WaitForSeconds(0.1f);
            // should be in between 0 and 15. 
            Assert.IsFalse(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 0, 0)));
            Assert.IsFalse(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 15, 0)));
            triggerEvent("trigger_rotation");
            // even though it did not start at zero, and is relative, it stops at the second multiple of 15.   
            yield return new WaitForSeconds(0.2f);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 30, 0)));
            // triggerEvent("trigger_rotation");

        }

        [UnityTest]
        public IEnumerator End_Time_Works_With_Sustain_For_Rotation()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro_sustain";
            obj1.triggerRotation = new ActiveTrigger
            {
                start = new Vec
                {
                    x = 15
                },
                value = new Vec { },
                end = new Vec
                {
                    x = -15
                },
                startTime = 0.1f,
                endTime = 0.1f,
            };
            TriggerObj trigger_intro_sustain = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
            };
            TriggerObj trigger_intro_sustain_off = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
                noteOff = true
            };
            initializeObjs();
            triggerEvent(trigger_intro_sustain);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(15, 0, 0)));
            yield return new WaitForSecondsOrTrue(1, () => a.instantiated);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.identity));
            yield return new WaitForSeconds(0.3f);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.identity));
            triggerEvent(trigger_intro_sustain_off);
            yield return new WaitForSecondsOrTrue(0.3f, () => a.ended);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(-15, 0, 0)));
        }

        [UnityTest]
        public IEnumerator Trigger_Works_For_Rotation_From_Any_Position()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                start = new Vec
                {
                    x = 90
                },
                value = new Vec
                {
                    x = 90
                },
                target = new Vec
                {
                    y = 90
                },
                startTime = 0,
                onTime = 0.1f,
                offDelay = 0.1f,
                trigger = "trigger_rotation"
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("trigger_rotation");
            yield return 0;
            Quaternion targetRotation = Quaternion.Euler(0, 90, 0);
            GameObject instance = getFirstRotateScaleInstance();
            ActiveRotation activeRotation = instance.GetComponent<ActiveRotation>();
            yield return new WaitForSecondsOrTrue(0.15f, () => activeRotation.on);
            Assert.IsTrue(activeRotation.on);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 90, 0)));
        }

        [UnityTest]
        public IEnumerator Secondary_Rotation_Triggers_From_Zero()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 90,
                },
                trigger = "triggerRotation",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            y = 90
                        },
                        trigger = "triggerSecondaryRotation",
                        onTime = 0.1f,
                        offTime = 0.1f,
                        offDelay = 0.1f,
                    }
                },
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.rotation, Quaternion.identity);
            yield return 0;
            triggerEvent("triggerRotation");
            yield return new WaitForSecondsOrTrue(1, () => activeRotation.on);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(90, 0, 0)));
            yield return new WaitForSecondsOrTrue(1, () => !activeRotation.animatingTrigger);
            triggerEvent("triggerSecondaryRotation");
            yield return new WaitForSecondsOrTrue(1, () =>
                Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 90, 0), false)
            );
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 90, 0)));
        }

        [UnityTest]
        public IEnumerator Second_Trigger_Continues_From_First_Trigger_Rotation()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 10,
                },
                trigger = "triggerRotation",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            y = 10
                        },
                        trigger = "triggerSecondaryRotation",
                        onTime = 0.2f,
                        offTime = 0.1f,
                        offDelay = 0.1f,
                    }
                },
                onTime = 0.2f,
                offTime = -1,
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.rotation, Quaternion.identity);
            yield return 0;
            triggerEvent("triggerRotation");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.rotation == Quaternion.Euler(10, 0, 0));
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(10, 0, 0)));
            triggerEvent("triggerSecondaryRotation");
            yield return new WaitForSecondsOrTrue(0.06f, () => instance.transform.eulerAngles.x > 0);
            //waiting a third of the secondary triggeronTime. 
            //we should be transitioning from the first trigger rotation ( not zero, as observed in bug ) 
            Assert.Greater(instance.transform.rotation.eulerAngles.x, 0.0);
        }

        [UnityTest]
        public IEnumerator Secondary_Sustain_Trigger_Returns_To_Base_Rotation_Before_Finishing()
        {
            //observes bug where a sustain 
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 10,
                },
                trigger = "triggerRotation",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            y = 10,
                        },
                        trigger = "secondarySustainRotation",
                        onTime = 2.0f,
                        offTime = 0.1f,
                    }
                },
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            initializeObjs();
            TriggerObj sustainTriggerObj = new TriggerObj
            {
                eventName = "secondarySustainRotation",
                sustain = true

            };
            TriggerObj noteOffObj = new TriggerObj
            {
                eventName = "secondarySustainRotation",
                sustain = true,
                noteOff = true
            };
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.rotation, Quaternion.identity);
            yield return 0;
            triggerEvent("triggerRotation");
            yield return new WaitForSecondsOrTrue(1, () => activeRotation.on);
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(10, 0, 0)));
            triggerEvent(sustainTriggerObj);
            yield return new WaitForSeconds(0.3f);
            //cut it short and then trigger off. 
            triggerEvent(noteOffObj);
            yield return new WaitForSecondsOrTrue(1, () => !activeRotation.on);
            Assert.AreEqual(instance.transform.rotation, Quaternion.identity);
        }

        [UnityTest]
        public IEnumerator Can_Cache_Knob_Trigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 90
                },
                trigger = "knob_rotation"
            };
            Trigger t = new Trigger
            {
                eventName = "knob_rotation",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerKnob(t, 1);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            //GameObject instance = getFirstRotateScaleInstance();
            Assert.AreEqual(instance.transform.rotation.eulerAngles, Quaternion.Euler(90, 0, 0).eulerAngles);
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Knob()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                knob = new Vec
                {
                    x = 0,
                    x2 = 90
                },
                trigger = "knob_rotation"
            };
            Trigger t = new Trigger
            {
                eventName = "knob_rotation",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t, 1);
            yield return 0;
            //GameObject instance = getFirstRotateScaleInstance();
            Assert.AreEqual(instance.transform.rotation.eulerAngles, Quaternion.Euler(90, 0, 0).eulerAngles);
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Knob_Using_Default_A_Values()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                knob = new Vec
                {
                    a = 0,
                    x2 = 90
                },
                trigger = "knob_rotation"
            };
            Trigger t = new Trigger
            {
                eventName = "knob_rotation",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t, 1);
            yield return 0;
            //GameObject instance = getFirstRotateScaleInstance();
            Assert.AreEqual(instance.transform.rotation.eulerAngles, Quaternion.Euler(90, 0, 0).eulerAngles);
        }


        [UnityTest]
        public IEnumerator Rotations_Consistent_Among_Caches()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.numInstances = 2;
            obj1.activeType = ActiveType.All;
            obj1.triggerRotation = new ActiveTrigger
            {
                knob = new Vec
                {
                    z = 0,
                    z2 = 180
                },
                value = new Vec { },
                startTime = 0,
                trigger = "primary_knob",
                triggers = new List<SecondaryActiveTrigger>{
                    {
                        new SecondaryActiveTrigger {
                            knob = new Vec {
                                x = 0,
                                x2 = 180
                            },
                            trigger = "secondary_knob"
                        }
                    }
                }
            };
            Trigger t = new Trigger
            {
                eventName = "primary_knob",
                channel = 0,
                knob = 1
            };
            Trigger t2 = new Trigger
            {
                eventName = "secondary_knob",
                channel = 0,
                knob = 2
            };
            addTrigger(t);
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t, 1.0f);
            triggerKnob(t2, 1.0f);
            yield return 0;
            triggerEvent("intro");
            //GameObject instance1 = getFirstRotateScaleInstance();
            GameObject instance2 = getRotateScaleInstanceByIndex(1);
            Assert.AreEqual(instance.transform.rotation, instance2.transform.rotation);
            Assert.AreEqual(instance2.transform.rotation, instance2.transform.rotation);
        }

        [UnityTest]
        public IEnumerator Secondary_Knob_Trigger_Works()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                triggers = new List<SecondaryActiveTrigger>{
                    {
                        new SecondaryActiveTrigger {
                            knob = new Vec {
                                y = 0,
                                y2 = 10
                            },
                            trigger="knob_trigger"
                        }
                    }
                }
            };
            Trigger t = new Trigger
            {
                eventName = "knob_trigger",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t, 1);
            yield return 0;
            yield return 0;
            yield return 0;
            GameObject instance = getFirstRotateScaleInstance();
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(0, 10, 0)));
        }


        [UnityTest]
        public IEnumerator Secondary_Knob_Trigger_Works_With_Primary_Knob_Trigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerRotation = new ActiveTrigger
            {
                knob = new Vec
                {
                    y = 0,
                    y2 = 10
                },
                value = new Vec { },
                startTime = 0,
                trigger = "primary_knob",
                triggers = new List<SecondaryActiveTrigger>{
                    {
                        new SecondaryActiveTrigger {
                            knob = new Vec {
                                x = 0,
                                x2 = 10
                            },
                            trigger = "secondary_knob"
                        }
                    }
                }
            };
            Trigger t = new Trigger
            {
                eventName = "primary_knob",
                channel = 0,
                knob = 1
            };
            Trigger t2 = new Trigger
            {
                eventName = "secondary_knob",
                channel = 0,
                knob = 2
            };
            addTrigger(t);
            addTrigger(t2);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerKnob(t, 1);
            triggerKnob(t2, 1);
            yield return 0;
            yield return 0;
            yield return 0;
            GameObject instance = getFirstRotateScaleInstance();
            Assert.IsTrue(Math.RoughEquals(instance.transform.rotation, Quaternion.Euler(10, 10, 0)));
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            instance = getFirstRotateScaleInstance();
            activeRotation = instance.GetComponent<ActiveRotation>();
            a = obj1.triggerRotation;

        }
    }
}

