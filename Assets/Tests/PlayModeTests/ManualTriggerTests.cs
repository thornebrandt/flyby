using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class ManualTriggerTests : PlayModeTests
    {
        //this test class if for testing when physics hits another object. 
        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            this.teardown();
        }

        [UnityTest]
        public IEnumerator Can_Manually_Trigger_Obj_With_Vec()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            ActivePosition ap = instance.GetComponentInChildren<ActivePosition>();
            //setup manual trigger. 
            ActiveTrigger a = new ActiveTrigger();
            a.target = new Vec
            {
                x = 10
            };
            ap.Trigger(new TriggerObj(), a, null);
            yield return new WaitForSeconds(0.1f);
            Assert.Greater(instance.transform.position.x, 0);
        }

        [UnityTest]
        public IEnumerator Can_Manually_Trigger_Obj_With_Color()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerColor = new ActiveTrigger();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            ActiveColor ac = instance.GetComponentInChildren<ActiveColor>();
            //setup manual trigger. 
            ActiveTrigger a = new ActiveTrigger();
            a.targetColor = new Col
            {
                r = 2
            };
            ac.Trigger(new TriggerObj(), a, null);
            yield return new WaitForSeconds(0.1f);
            Assert.Greater(this.getColor(instance).r, 0);
        }

        [UnityTest]
        public IEnumerator Can_Manually_Trigger_An_Animation()
        {
            obj1 = createBlankObj();
            obj1.asset = "AnimatorCube";
            obj1.trigger = "intro";
            obj1.activeTrigger = new ActiveTrigger();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            instance = getFirstPrefabInstance();
            ActiveAnimator aa = instance.GetComponentInChildren<ActiveAnimator>();
            ActiveTrigger a = new ActiveTrigger();
            a.onTime = 0.1f;
            a.offTime = -1;
            aa.Trigger(new TriggerObj(), a, null);
            yield return new WaitForSecondsOrTrue(1, () => aa.on);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, new Vector3(0, 1, 0)));
        }

        [UnityTest]
        public IEnumerator Can_Manually_Trigger_Specific_Behaviour()
        {
            obj1 = createBlankObj();
            obj1.asset = "HelloWorldCube";
            obj1.trigger = "intro";
            obj1.activeTrigger = new ActiveTrigger();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            HelloWorld[] hws = instance.GetComponentsInChildren<HelloWorld>(); //should return 2.
            Assert.AreEqual(2, hws.Length);
            ActiveTrigger a = new ActiveTrigger();
            hws[0].Trigger(new TriggerObj(), a, null);
            Assert.AreEqual(1, hws[0].numTimesTriggered);
            Assert.AreEqual(0, hws[1].numTimesTriggered);
            hws[1].Trigger(new TriggerObj(), a, null);
            Assert.AreEqual(1, hws[0].numTimesTriggered);
            Assert.AreEqual(1, hws[1].numTimesTriggered);
        }

        [UnityTest]
        public IEnumerator Can_Manually_Trigger_Lights()
        {
            obj1 = createBlankObj();
            obj1.asset = "PointLight";
            obj1.trigger = "intro";
            obj1.activeTrigger = new ActiveTrigger();
            obj1.activeTrigger.startTime = 0;
            obj1.activeTrigger.startColor = new Col(0);
            obj1.activeTrigger.color = new Col(0); //why is this defaulting to red ? investigatge. 
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            instance = getFirstPrefabInstance();
            ActiveLight al = instance.GetComponent<ActiveLight>();
            Light light = instance.GetComponent<Light>();
            ActiveTrigger a = new ActiveTrigger();
            a.onTime = 0.1f;
            a.offTime = -1;
            a.targetColor = new Col
            {
                r = 1
            };
            Assert.IsTrue(Math.RoughEquals(light.color, Color.black));
            al.Trigger(new TriggerObj(), a, null);
            yield return new WaitForSecondsOrTrue(1, () => al.on);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(light.color, Color.red));
        }

        private Color getColor(GameObject instance)
        {
            GameObject colorInstance = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[0]);
            return colorInstance.GetComponent<Renderer>().material.GetColor(colorProperty);
        }

    }
}