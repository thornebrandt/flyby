using System.Collections;
using NUnit.Framework;
using UnityEngine;
using flyby.Core;
using System.Collections.Generic;
using UnityEngine.TestTools;
using flyby.Active;


namespace Tests
{
    public class FlyByParticleTests : PlayModeTests
    {
        private ActiveParticles activeParticles;
        private ParticleSystem particles;
        private ParticleSystemRenderer rend;
        private ParticleSystem.MainModule main;

        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
            activeParticles.Kill();
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            instance = getFirstInstance();
            this.activeParticles = instance.GetComponent<ActiveParticles>();
            this.particles = this.activeParticles.particles;
            this.rend = this.particles.GetComponent<ParticleSystemRenderer>();
            this.main = this.particles.main;
            a = new ActiveTrigger();
            activeParticles.triggers.Add(a);
            o = flybyController.currentPart.objs[0];
            this.activeParticles.setupActiveTriggers(o);
        }

        [UnityTest]
        public IEnumerator ActiveParticles_Is_Not_Null()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "ParticleCube";
            obj1.trigger = "intro";
            obj1.preRender = "true";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            yield return 0;
            Assert.IsNotNull(activeParticles);
            Assert.IsNotNull(this.particles);
        }

        [UnityTest]
        public IEnumerator ActiveParticles_Can_Be_Triggered()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "ParticleCube";
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            a.startTime = 0.1f;
            a.startDelay = 0.1f;
            a.start = new Vec(0, 0, 0);
            a.value = new Vec(1, 0, 0);
            a.propertyName = "emission";
            Assert.False(this.particles.isEmitting);
            triggerEvent("intro");
            yield return new WaitForSeconds(0.3f);
            Assert.True(this.particles.isEmitting);
            Assert.AreEqual(this.particles.emission.rateOverTime.constant, 1);
        }

        [UnityTest]
        public IEnumerator ActiveParticle_Scale_Is_Zero_On_Intro()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "ParticleCube";
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            a.startTime = 0.1f;
            a.startDelay = 0.1f;
            a.start = new Vec(0, 0, 0);
            a.value = new Vec(2, 2, 2);
            a.propertyName = "scale";
            //Assert.AreEqual(this.main.startSize.constant, 0); //not sure what that would be. 
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(this.main.startSize.constant, 0);
        }
    }
}