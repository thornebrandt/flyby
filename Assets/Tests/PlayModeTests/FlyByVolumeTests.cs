using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests {
    public class FlyByVolumeTests : PlayModeTests {
        private ActiveVolume activeVolume;
        private Volume volume;
        private Bloom bloom;

        [SetUp]
        public void Setup() {
            this.setup();
        }

        [TearDown]
        public void Teardown() {
            flybyController.killAll();
            // why do we need the below ones? 
            // investigate during compilation. 
            activeVolume.Kill();
        }


        [UnityTest]
        public IEnumerator ActiveVolume_Is_Not_Null() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "GlobalVolume";
            obj1.trigger = "intro";
            obj1.preRender = "true";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            yield return 0;
            Assert.IsNotNull(activeVolume);
            Assert.IsNotNull(volume);
        }

        [UnityTest]
        public IEnumerator ActiveVolume_Can_Fade_In_Bloom_With_Start() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "GlobalVolume";
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            a.startTime = 0.1f;
            a.startDelay = 0.1f;
            a.start = new Vec(0, 0, 0);
            a.value = new Vec(1, 0, 0);
            a.propertyName = "bloom";
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(bloom.intensity.value, 0);
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(bloom.intensity.value, 1);
        }

        [UnityTest]
        public IEnumerator ActiveVolume_Can_Fade_In_Secondary_Bloom_With_Start() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "GlobalVolume";
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            activeVolume.triggers.Add(
                new ActiveTrigger {
                    startTime = 0.1f,
                    startDelay = 0.1f,
                    start = new Vec(0, 0, 0),
                    value = new Vec(1, 0, 0),
                    propertyName = "bloom"
                }
            );
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(bloom.intensity.value, 0);
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(bloom.intensity.value, 1);
        }

        [UnityTest]
        public IEnumerator ActiveVolume_Can_Start_With_Secondary_Bloom() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "GlobalVolume";
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            activeVolume.triggers.Add(
                new ActiveTrigger {
                    startTime = 0,
                    value = new Vec(1, 0, 0),
                    propertyName = "bloom"
                }
            );
            triggerEvent("intro");
            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(bloom.intensity.value, 1);
        }

        public override void initializeObjs() {
            base.initializeObjs();
            instance = getFirstInstance();
            volume = instance.GetComponent<Volume>();
            volume.profile.TryGet(out bloom);
            if (bloom == null) {
                Debug.Log("could not set up bloom");
            }
            activeVolume = instance.GetComponent<ActiveVolume>();
            activeVolume.triggers = new List<ActiveTrigger>();
            a = new ActiveTrigger();
            activeVolume.triggers.Add(a);
            o = flybyController.currentPart.objs[0];
            activeVolume.setupActiveTriggers(o);
        }
    }
}