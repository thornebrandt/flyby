using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyByGroupTests : PlayModeTests
    {
        [SetUp]
        public void Setup()
        {
            //this.configPath = "Assets/Tests/PlayModeTests/json/test_config.json";
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            this.teardown();
            // flybyController.killAll();
            // this.configPath = "config.sample.json";
        }

        [Test]
        public void TriggerController_Gets_Length_Of_Group()
        {
            obj1 = createBlankObj();
            obj1.group = "test_group";
            obj1.groupIndex = 0;
            NullObj obj2 = createBlankObj();
            obj2.group = "test_group";
            obj2.groupIndex = 1;
            initializeObjs();
            Assert.AreEqual(flybyController.currentPart.getGroupLength("test_group"), 2);
        }

        [Test]
        public void Setup_Assigns_Appropriate_Group_Indexes()
        {
            obj1 = createBlankObj();
            obj1.group = "test_group";
            NullObj obj2 = createBlankObj();
            obj2.group = "test_group";
            initializeObjs();
            Assert.AreEqual(flybyController.currentPart.objs[0].groupIndex, 0);
            Assert.AreEqual(flybyController.currentPart.objs[1].groupIndex, 1);
        }

        [UnityTest]
        public IEnumerator Trigger_Gets_Appropriate_Index()
        {
            //TODO - this should not be working bc there is no trigger??
            //not sure what this comment means?
            flybyController.currentPart.id = "test_part_from_test";
            obj1 = createBlankObj();
            obj1.id = "first group obj";
            obj1.group = "test_group";
            obj1.trigger = "intro";
            NullObj obj2 = createBlankObj();
            obj2.group = "test_group";
            obj2.id = "second group obj";
            obj2.trigger = "intro";
            initializeObjs();
            GameObject instance2 = getInstanceByObjIndex(1);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            Assert.AreEqual(instance2.transform.position, o.nullVector);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance2.transform.position, Vector3.zero); //should have transfered. 
        }

        [UnityTest]
        public IEnumerator Group_Makes_Active_Trigger_Sequential_On_Same_Obj()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = -1
                },
                onTime = 0.1f,
                offTime = -1,
                group = "position_group",
                trigger = "trigger_position",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            x = 1
                        },
                        group = "position_group",
                        onTime = 0.1f,
                        offTime = -1,
                        trigger = "trigger_position"
                    }
                }
            };
            initializeObjs();
            TriggerObj groupTrigger = new TriggerObj();
            //groupTrigger.group = "position_group";
            groupTrigger.eventName = "trigger_position";
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent(groupTrigger);
            yield return new WaitForSeconds(0.2f);
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.x == -1);
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.x == 1);
            Assert.AreEqual(instance.transform.position.x, 1);
        }

        [UnityTest]
        public IEnumerator Group_Trigger_Works_For_Child_Behaviours()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.asset = "ParentWithChildren";
            obj1.bundle = "TestPrefabs";
            obj1.hierarchyType = HierarchyType.None;
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = -1
                },
                onTime = 0.1f,
                offTime = -1,
                group = "child_position_group",
                trigger = "trigger_child_position",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            y = 1
                        },
                        childIndex = 0,
                        group = "child_position_group",
                        trigger = "trigger_child_position",
                        onTime = 0.1f,
                        offTime = -1
                    }
                }
            };
            initializeObjs();
            TriggerObj groupTrigger = new TriggerObj
            {
                eventName = "trigger_child_position"
            };
            triggerEvent("intro");
            yield return 0;
            GameObject instance1 = flybyController.currentPart.objs[0].instances[0];
            Assert.AreEqual(instance1.transform.position, Vector3.zero);
            triggerEvent(groupTrigger);
            GameObject child1 = instance1.transform.GetChild(0).gameObject;
            Assert.IsNotNull(child1);
            yield return new WaitForSecondsOrTrue(1, () => instance1.transform.position.x == -1);
            Assert.AreEqual(instance1.transform.position, new Vector3(-1, 0, 0));
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1, () => child1.transform.position.y == 1);
            Assert.AreEqual(child1.transform.position, new Vector3(-1, 1, 0));
        }


        [UnityTest]
        public IEnumerator Group_Makes_Active_Trigger_Sequential_On_Second_Instance()
        {
            obj1 = createBlankObj();
            obj1.id = "TwoInstanceGroup";
            obj1.trigger = "intro";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = -1
                },
                onTime = 0.1f,
                offTime = -1,
                group = "position_group",
                trigger = "trigger_position",
            };
            obj1.numInstances = 3;
            initializeObjs();
            GameObject instance2 = getPopupInstanceByIndex(1);
            TriggerObj groupTrigger = new TriggerObj();
            groupTrigger.eventName = "trigger_position";
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.x == -1);
            Assert.AreEqual(instance.transform.position.x, -1);
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance2.transform.position.x, 0);
            ActivePosition instance2_position = instance2.GetComponent<ActivePosition>();
            instance.transform.position = Vector3.zero; //debugging that the group was not carried over.
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1, () => instance2.transform.position.x == -1);
            Assert.AreEqual(instance2.transform.position.x, -1);
        }

        [UnityTest]
        public IEnumerator Group_Makes_Active_Trigger_Sequential_On_Second_Instance_For_ActiveTypeAll()
        {
            obj1 = createBlankObj();
            obj1.id = "TwoInstanceGroup";
            obj1.trigger = "intro";
            obj1.activeType = ActiveType.All;
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = -1
                },
                onTime = 0.1f,
                offTime = -1,
                group = "position_group",
                trigger = "trigger_position",
            };
            obj1.numInstances = 2;
            initializeObjs();
            GameObject instance2 = getPopupInstanceByIndex(1);
            TriggerObj groupTrigger = new TriggerObj();
            //groupTrigger.group = "position_group";
            groupTrigger.eventName = "trigger_position";
            triggerEvent("intro");
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent(groupTrigger);
            yield return new WaitForSeconds(0.12f);
            Assert.AreEqual(instance.transform.position.x, -1);
            Assert.AreEqual(instance2.transform.position.x, -1);
        }

        [UnityTest]
        public IEnumerator Group_Triggers_Both_Instance_And_ActiveTrigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "group_trigger";
            obj1.group = "test_group";
            obj1.numInstances = 2;
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 1
                },
                onTime = 0.1f,
                offTime = -1,
                trigger = "group_trigger",
                group = "test_group"
            };
            initializeObjs();
            flybyController.currentPart.id = "group_test_part";
            TriggerObj groupTrigger = new TriggerObj();
            groupTrigger.eventName = "group_trigger";
            triggerEvent(groupTrigger);
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            triggerEvent(groupTrigger);
            yield return new WaitForSeconds(0.15f);
            Assert.AreEqual(instance.transform.position, new Vector3(1, 0, 0));
        }


        [UnityTest]
        public IEnumerator Group_Makes_Active_Trigger_Sequential_Between_Objs()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            NullObj obj2 = createBlankObj();
            obj2.trigger = "intro";
            obj2.id = "instance2";
            obj1.triggerPosition = new ActiveTrigger
            {
                id = "obj1 position[0]",
                target = new Vec
                {
                    x = 1
                },
                onTime = 0.1f,
                offTime = -1,
                trigger = "trigger_position",
                group = "position_group"
            };
            obj2.triggerPosition = new ActiveTrigger
            {
                id = "obj2 position[1]",
                target = new Vec
                {
                    x = -1
                },
                onTime = 0.1f,
                offTime = -1,
                trigger = "trigger_position",
                group = "position_group"
            };
            initializeObjs();
            GameObject instance2 = Obj.getPopupInstance(getInstanceByObjIndex(1));
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            Assert.AreEqual(instance2.transform.position, Vector3.zero);
            triggerEvent("trigger_position");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.position.x == 1);
            //the group trigger only affects the first instance.
            Assert.AreEqual(instance.transform.position.x, 1);
            Assert.AreEqual(instance2.transform.position.x, 0);
            triggerEvent("trigger_position");
            yield return new WaitForSecondsOrTrue(1, () => instance2.transform.position.x == -1);
            //now second one is triggered. 
            Assert.AreEqual(instance.transform.position.x, 1);
            Assert.AreEqual(instance2.transform.position.x, -1);
        }

        [UnityTest]
        public IEnumerator Can_Load_Groups_From_Json()
        {
            //seeing if I can reset - from test
            this.setupTestJSON();
            triggerEvent("load_group_song");
            yield return 0;
            Obj o = flybyController.currentPart.objs[0];
            GameObject groupCube = o.instances[0];
            Assert.AreEqual(groupCube.transform.position, o.nullVector);
            TriggerObj groupTrigger = new TriggerObj();
            //groupTrigger.group = "test_group1";
            groupTrigger.eventName = "test_group_trigger1";
            triggerEvent(groupTrigger);
            yield return 0;
            Assert.AreEqual(groupCube.transform.position, Vector3.zero);
            Obj o2 = flybyController.currentPart.objs[1];
            GameObject groupSphere = o2.instances[0];
            Assert.AreEqual(groupSphere.transform.position, o.nullVector);
            triggerEvent(groupTrigger);
            yield return 0;
            Assert.AreEqual(groupSphere.transform.position, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Group_Trigger_Does_Not_Fire_Second_Obj_ActiveBehaviour()
        {
            this.setupTestJSON();
            triggerEvent("load_group_song");
            yield return 0;
            Obj groupCube_o = flybyController.currentPart.objs[2];
            Obj groupSphere_o = flybyController.currentPart.objs[3];
            GameObject groupCube1 = Obj.getPopupInstance(groupCube_o.instances[0]);
            GameObject groupCube2 = Obj.getPopupInstance(groupCube_o.instances[1]);
            GameObject groupSphere1 = Obj.getPopupInstance(groupSphere_o.instances[0]);
            GameObject groupSphere2 = Obj.getPopupInstance(groupSphere_o.instances[1]);
            TriggerObj groupTrigger = new TriggerObj();
            groupTrigger.eventName = "test_group_trigger2";
            //groupTrigger.group = "test_group2";
            triggerEvent("intro_cube");
            triggerEvent("intro_sphere");
            yield return 0;
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1.0f, () => groupCube2.transform.position.x == 1);
            yield return 0;
            Assert.AreEqual(groupCube1.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(groupSphere1.transform.position, Vector3.zero);
        }


        [UnityTest]
        public IEnumerator Group_Trigger_Does_Not_Fire_Second_Instance()
        {
            this.setupTestJSON();
            triggerEvent("load_group_song");
            yield return 0;
            Obj groupCube_o = flybyController.currentPart.objs[2];
            Obj groupSphere_o = flybyController.currentPart.objs[3];
            GameObject groupCube1 = Obj.getPopupInstance(groupCube_o.instances[0]);
            GameObject groupCube2 = Obj.getPopupInstance(groupCube_o.instances[1]);
            GameObject groupSphere1 = Obj.getPopupInstance(groupSphere_o.instances[0]);
            GameObject groupSphere2 = Obj.getPopupInstance(groupSphere_o.instances[1]);
            TriggerObj groupTrigger = new TriggerObj();
            groupTrigger.eventName = "test_group_trigger2";
            //groupTrigger.group = "test_group2";
            triggerEvent("intro_cube");
            triggerEvent("intro_cube");
            triggerEvent("intro_sphere");
            triggerEvent("intro_sphere");
            yield return 0;
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1.0f, () => groupCube2.transform.position.x == 1);
            yield return 0;
            Assert.AreEqual(groupCube2.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(groupCube1.transform.position, Vector3.zero);
            Assert.AreEqual(groupSphere1.transform.position, Vector3.zero);
            //this one is failing. 
            Assert.AreEqual(groupSphere2.transform.position, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Can_Instantiate_Multiple_Instances_With_Groups()
        {
            this.setupTestJSON();
            triggerEvent("load_group_song");
            yield return 0;
            Obj groupCube_o = flybyController.currentPart.objs[2];
            Obj groupSphere_o = flybyController.currentPart.objs[3];
            GameObject groupCube1 = Obj.getPopupInstance(groupCube_o.instances[0]);
            GameObject groupCube2 = Obj.getPopupInstance(groupCube_o.instances[1]);
            GameObject groupSphere1 = Obj.getPopupInstance(groupSphere_o.instances[0]);
            GameObject groupSphere2 = Obj.getPopupInstance(groupSphere_o.instances[1]);
            Assert.AreEqual(groupCube1.transform.position, o.nullVector);
            TriggerObj groupTrigger = new TriggerObj();
            groupTrigger.eventName = "test_group_trigger2";
            //groupTrigger.group = "test_group2";
            triggerEvent("intro_cube");
            yield return 0;
            Assert.AreEqual(groupCube1.transform.position, Vector3.zero);
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1.0f, () => groupCube1.transform.position.x == 1);
            yield return 0;
            Assert.AreEqual(groupCube1.transform.position, new Vector3(1, 0, 0));
            triggerEvent("intro_sphere");
            yield return 0;
            Assert.AreEqual(groupSphere1.transform.position, Vector3.zero);
            //group trigger should be on second instance. 
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1.0f, () => groupSphere1.transform.position.x == 1);

            yield return 0;
            Assert.AreEqual(groupSphere1.transform.position, new Vector3(1, 0, 0));
            triggerEvent("intro_cube");
            triggerEvent("intro_sphere");
            yield return 0;
            yield return 0;
            Assert.AreEqual(groupCube2.transform.position, Vector3.zero);
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1.0f, () => groupCube2.transform.position.x == 1);
            yield return 0;
            Assert.AreEqual(groupCube2.transform.position, new Vector3(1, 0, 0));
            Assert.AreEqual(groupSphere2.transform.position, Vector3.zero);
            triggerEvent(groupTrigger);
            yield return new WaitForSecondsOrTrue(1.0f, () => groupSphere2.transform.position.x == 1);
            yield return 0;
            Assert.AreEqual(groupSphere2.transform.position, new Vector3(1, 0, 0));
        }
        //todo - check copies. 

        private void setupTestJSON()
        {
            this.teardown();
            flybyController.killAll();
            this.configPath = "Assets/Tests/PlayModeTests/json/test_config.json";
            this.setup();
        }
    }
}