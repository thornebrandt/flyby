using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Active;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests {
    public class FlyByScaleTests : PlayModeTests {
        ActiveScale activeScale; //first activeScale;

        [SetUp]
        public void Setup() {
            this.setup();
        }

        [TearDown]
        public void Teardown() {
            flybyController.killAll();
        }

        [UnityTest]
        public IEnumerator Defaults_As_Scale_One() {
            obj1 = createBlankObj();
            obj1.id = "default_obj";
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {

            };
            registerTriggers();
            initializeObjs();
            yield return 0;
            triggerEvent("intro");
            GameObject instance = getFirstRotateScaleInstance();
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
        }

        //TODO - translate to editModeTest as soon as it works. 
        [UnityTest]
        public IEnumerator Can_Quickly_Assign_Scale_With_Scale_Property() {
            obj1 = createBlankObj();
            obj1.id = "scaled_obj";
            obj1.trigger = "intro";
            obj1.scale = new Vec(3);
            initializeObjs();
            yield return 0;
            triggerEvent("intro");
            GameObject instance = getFirstRotateScaleInstance();
            Assert.AreEqual(instance.transform.localScale, new Vector3(3, 3, 3));
        }


        [UnityTest]
        public IEnumerator Intro_For_Scale_Works() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                startTime = 0.1f,
                start = new Vec {
                    a = 0
                },
                value = new Vec {
                    a = 1
                }
            };
            registerTriggers();
            initializeObjs();
            yield return 0;
            triggerEvent("intro");
            GameObject instance = getFirstRotateScaleInstance();
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
            yield return new WaitForSecondsOrTrue(1, () => activeScale.introduced);
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
        }


        [UnityTest]
        public IEnumerator Start_Delay_Delays_Value_Even_If_Start_Time_Is_Zero() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                start = new Vec {
                    a = 0
                },
                value = new Vec {
                    a = 1
                },
                startTime = 0,
                startDelay = 0.2f
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
        }




        [UnityTest]
        public IEnumerator Intro_And_Exit_For_Scale_Works() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.id = "testing_obj";
            obj1.triggerScale = new ActiveTrigger {
                startTime = 0.1f,
                endTime = 0.1f,
                endDelay = 0.15f, //need to add endDelay for tests to behave properly.
                start = new Vec {
                    a = 0
                },
                value = new Vec {
                    a = 1
                },
                end = new Vec {
                    a = 0.1f
                }
            };
            registerTriggers();
            initializeObjs();
            yield return 0;
            triggerEvent("intro");
            GameObject instance = getFirstRotateScaleInstance();
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.localScale == Vector3.one);
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.localScale == Vector3.zero);
            Assert.AreEqual(instance.transform.localScale, new Vector3(0.1f, 0.1f, 0.1f));
        }

        [UnityTest]
        public IEnumerator End_Time_Works_With_Sustain_For_Scale_Instantiation() {
            obj1 = createBlankObj();
            obj1.trigger = "sustain_intro";
            obj1.triggerScale = new ActiveTrigger {
                startTime = 0.1f,
                endTime = 0.1f,
                endDelay = 0.01f, //probably leftover. 
                start = new Vec {
                    a = 0
                },
                value = new Vec {
                    a = 1
                },
                end = new Vec {
                    a = 0
                }
            };
            registerTriggers();
            initializeObjs();
            yield return 0;
            TriggerObj sustainTriggerObj = new TriggerObj {
                eventName = "sustain_intro",
                sustain = true
            };
            triggerEvent(sustainTriggerObj);
            GameObject instance = getFirstRotateScaleInstance();
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
            yield return new WaitForSecondsOrTrue(1, () => activeScale.introduced);
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            yield return new WaitForSeconds(1);
            //we have not sent a noteOff command, so this should remain at the value, which is one.
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            //create the noteOff command. 
            TriggerObj noteOffTriggerObj = new TriggerObj {
                eventName = "sustain_intro",
                sustain = true,
                noteOff = true
            };
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => activeScale.ended);
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Trigger_End_Can_End_Trigger_Manually() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                startTime = 0.1f,
                endTime = -0.1f, //this is default "hold", but forced off if given a triggerEnd".
                start = new Vec {
                    a = 0
                },
                value = new Vec {
                    a = 1
                },
                end = new Vec {
                    a = 0
                },
            };
            initializeObjs();
            yield return 0;
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
            yield return new WaitForSecondsOrTrue(1, () => activeScale.introduced);
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            yield return new WaitForSeconds(1);
            //we have not sent a noteOff command, so this should remain at the value, which is one.
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            TriggerObj noteOffTriggerObj = new TriggerObj {
                triggerEnd = true,
                eventName = "intro",
            };
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => activeScale.ended);
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Trigger_End_Can_End_Trigger_Manually_From_Active_Trigger() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                startTime = 0.1f,
                endTime = -0.1f, //this is default "hold", but forced off if given a triggerEnd".
                start = new Vec {
                    a = 0
                },
                value = new Vec {
                    a = 1
                },
                end = new Vec {
                    a = 0
                },
                trigger = "endMe",
            };
            initializeObjs();
            yield return 0;
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
            yield return new WaitForSecondsOrTrue(1, () => activeScale.introduced);
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            yield return new WaitForSeconds(1);
            //we have not sent a noteOff command, so this should remain at the value, which is one.
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            TriggerObj noteOffTriggerObj = new TriggerObj {
                triggerEnd = true,
                eventName = "endMe",
            };
            triggerEvent(noteOffTriggerObj);
            yield return new WaitForSecondsOrTrue(1, () => activeScale.ended);
            Assert.AreEqual(instance.transform.localScale, Vector3.zero);
        }

        [UnityTest]
        public IEnumerator Trigger_Events_Work_For_Scale() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                onTime = 0.1f,
                offDelay = 0.1f,
                target = new Vec(2),
                trigger = "trigger_scale",
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            triggerEvent("trigger_scale");
            yield return new WaitForSecondsOrTrue(1f, () => activeScale.on);
            //TODO - change decimal points to 2, and add verbose nonequality message. 
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, new Vector3(2, 2, 2)));
        }

        [UnityTest]
        public IEnumerator Trigger_Off_Events_Work_For_Scale() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
                target = new Vec(2),
                trigger = "trigger_scale",
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            triggerEvent("trigger_scale");
            yield return new WaitForSecondsOrTrue(1f, () => instance.transform.localScale == Vector3.one);
            yield return 0;
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, new Vector3(2, 2, 2)));
            yield return new WaitForSecondsOrTrue(1f, () => activeScale.triggered);
            yield return 0;
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
        }

        [UnityTest]
        public IEnumerator Trigger_Off_Delay_Keeps_Scale_The_Same() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 2,
                target = new Vec(2),
                trigger = "trigger_scale",
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("trigger_scale");
            yield return new WaitForSecondsOrTrue(1f, () => activeScale.on);
            Assert.AreEqual(instance.transform.localScale, new Vector3(2, 2, 2));
            yield return new WaitForSeconds(0.5f);
            //should remain at the target scale with a long delay.
            Assert.AreEqual(instance.transform.localScale, new Vector3(2, 2, 2));
        }

        [UnityTest]
        public IEnumerator Trigger_Retains_Scale_If_OffTime_Is_Negative() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                onTime = 0.1f,
                offTime = -1,
                target = new Vec(2, 0.1f, 0.1f),
                trigger = "trigger_scale",
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            triggerEvent("trigger_scale");
            yield return new WaitForSecondsOrTrue(1f, () => activeScale.on);
            Assert.AreEqual(instance.transform.localScale, new Vector3(2, 0.1f, 0.1f));
            yield return new WaitForSeconds(0.1f);
            //should remain in the position.
            Assert.AreEqual(instance.transform.localScale, new Vector3(2, 0.1f, 0.1f));
        }

        [UnityTest]
        public IEnumerator Secondary_Trigger_Overrides_Primary_Trigger() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                target = new Vec(1, 2, 1),
                trigger = "triggerScale",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            a = 1,
                            x = 2,
                        },
                        trigger = "triggerSecondaryScale",
                        onTime = 0.1f,
                        offTime = 0.1f,
                        offDelay = 0.1f,
                    }
                },
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            yield return 0;
            triggerEvent("triggerScale");
            yield return new WaitForSecondsOrTrue(1, () => activeScale.on);
            Assert.AreEqual(instance.transform.localScale, new Vector3(1, 2, 1));
            triggerEvent("triggerSecondaryScale");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.localScale.x == 2.0f);
            Assert.AreEqual(instance.transform.localScale, new Vector3(2, 1, 1));
        }

        [UnityTest]
        public IEnumerator Second_Trigger_Continues_From_First_Trigger_Scale() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                target = new Vec {
                    a = 1,
                    x = 2,
                },
                trigger = "triggerScale",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            a = 1,
                            y = 2
                        },
                        trigger = "triggerSecondaryScale",
                        onTime = 0.2f,
                        offTime = -1
                    }
                },
                onTime = 0.2f,
                offTime = -1
            };
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            yield return 0;
            triggerEvent("triggerScale");
            yield return new WaitForSecondsOrTrue(1, () => instance.transform.localScale == new Vector3(2, 1, 1));
            Assert.AreEqual(instance.transform.localScale, new Vector3(2, 1, 1));
            triggerEvent("triggerSecondaryScale");
            yield return new WaitForSeconds(0.1f);
            //second trigger. should have started from first trigger. 
            Assert.Greater(instance.transform.localScale.x, 1); // the target is one, default is one, but it should not have gone back to one yet. 
        }

        [UnityTest]
        public IEnumerator Knob_Trigger_Works() {
            obj1 = createBlankObj();
            obj1.trigger = "intro"; //event for initialize. 
            obj1.triggerScale = new ActiveTrigger {
                knob = new Vec {
                    a = 1,
                    a2 = 2
                },
                trigger = "test_knob"
            };
            Trigger t = new Trigger {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            yield return 0;
            triggerKnob(t, 1);
            yield return 0;
            Assert.AreEqual(instance.transform.localScale, new Vector3(2, 2, 2));
        }

        [UnityTest]
        public IEnumerator Cache_Knob_Trigger() {
            obj1 = createBlankObj();
            obj1.trigger = "intro"; //event for initialize. 
            obj1.triggerScale = new ActiveTrigger {
                knob = new Vec {
                    a = 1,
                    a2 = 2
                },
                activeType = ActiveType.Next,
                trigger = "test_knob"
            };
            Trigger t = new Trigger {
                eventName = "test_knob",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerKnob(1);
            yield return 0;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(instance.transform.localScale, new Vector3(2, 2, 2));
        }


        [UnityTest]
        public IEnumerator Secondary_Sustain_Scale_Returns_To_Base_Scale_Before_Finishing() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                target = new Vec {
                    a = 0.5f,
                    y = 2,
                },
                trigger = "triggerScale",
                triggers = new List<SecondaryActiveTrigger>{
                    new SecondaryActiveTrigger {
                        target = new Vec {
                            a = 0.5f,
                            x = 2,
                        },
                        trigger = "secondarySustainScale",
                        onTime = 2.0f,
                        offTime = 0.1f,
                    }
                },
                onTime = 0.1f,
                offTime = 0.1f,
                offDelay = 0.1f,
            };
            initializeObjs();
            TriggerObj sustainTriggerObj = new TriggerObj {
                eventName = "secondarySustainScale",
                sustain = true

            };
            TriggerObj noteOffObj = new TriggerObj {
                eventName = "secondarySustainScale",
                sustain = true,
                noteOff = true
            };
            triggerEvent("intro");
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            yield return 0;
            triggerEvent("triggerScale");
            yield return new WaitForSecondsOrTrue(1, () => activeScale.on);
            Assert.AreEqual(instance.transform.localScale, new Vector3(0.5f, 2, 0.5f));
            triggerEvent(sustainTriggerObj);
            yield return new WaitForSeconds(0.3f);
            //cut it short and then trigger off. 
            triggerEvent(noteOffObj);
            yield return new WaitForSecondsOrTrue(1, () => !activeScale.on);
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
        }

        [UnityTest]
        public IEnumerator Hide_On_Complete_Hides_Object() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                start = new Vec(),
                value = new Vec(1),
                end = new Vec(0.01f),
                startTime = 0.1f,
                endTime = 0.1f,
                endDelay = 0.1f,
                hideOnComplete = true
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(1, () => activeScale.introduced);
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, Vector3.one, true));
            yield return new WaitForSecondsOrTrue(1, () => activeScale.ended);
            Assert.IsFalse(instance.activeInHierarchy);
        }


        [UnityTest]
        public IEnumerator Hide_On_Target_Hides_Object() {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.triggerScale = new ActiveTrigger {
                value = new Vec(1),
                target = new Vec(2),
                onTime = 0.1f,
                hideOnTarget = true,
                trigger = "hideMe"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSecondsOrTrue(1, () => activeScale.introduced);
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, Vector3.one, true));
            triggerEvent("hideMe");
            yield return new WaitForSeconds(0.15f);
            Assert.IsFalse(instance.activeInHierarchy);
        }

        [UnityTest]
        public IEnumerator Trigger_Events_Only_Work_For_Assigned_Objs() {
            //debugging. 
            obj1 = createBlankObj();
            obj1.trigger = "intro1";
            obj1.id = "obj1";
            obj1.triggerScale = new ActiveTrigger {
                startTime = 0,
                onTime = 0.2f,
                offTime = -1,
                target = new Vec(2),
                trigger = "trigger_scale1"
            };
            //make second obj but it has different scale triggers. 
            NullObj obj2 = createBlankObj();
            obj2.trigger = "intro2";
            obj2.id = "obj2";
            obj2.triggerScale = new ActiveTrigger {
                startTime = 0,
                onTime = 0.2f,
                offTime = -1,
                target = new Vec(2),
                trigger = "trigger_scale2"
            };
            initializeObjs();
            GameObject instance2 = Obj.getRotateScaleInstance(flybyController.currentPart.objs[1].instances[0]);
            triggerEvent("intro1");
            yield return 0;
            triggerEvent("intro2");
            yield return 0;
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            //triggering second instance first. 
            triggerEvent("trigger_scale2");
            yield return new WaitForSeconds(0.3f);
            Assert.IsTrue(Math.RoughEquals(instance2.transform.localScale, new Vector3(2, 2, 2)));
            Assert.AreEqual(instance.transform.localScale, Vector3.one);
            //now first instance.
            triggerEvent("trigger_scale1");
            yield return new WaitForSeconds(0.3f);
            Assert.IsTrue(Math.RoughEquals(instance.transform.localScale, new Vector3(2, 2, 2)));
        }


        public override void initializeObjs() {
            base.initializeObjs();
            instance = getFirstRotateScaleInstance();
            activeScale = instance.GetComponent<ActiveScale>();
        }

    }
}