using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Active;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;
using UnityEngine.SceneManagement;

namespace Tests
{
    public class FlyByCustomTests : PlayModeTests
    {
        //tests for adding customized scripts that aren't part of the json. 

        FlyByController customFlybyController;
        HelloWorld helloWorld;

        [SetUp]
        public void Setup()
        {
            this.setup();
            go = GameObject.Find("FlyByController");
            if (go == null)
            {
                go = new GameObject("FlyByController");
            }
            customFlybyController = go.GetComponent<FlyByController>();
            if (customFlybyController == null)
            {
                customFlybyController = go.AddComponent<FlyByController>();
            }

            customFlybyController.configPath = "config.sample.json"; //empty
            if (assetController == null)
            {
                AssetController assetController = new AssetController();
                assetController.prefabCache = new Dictionary<string, GameObject>();
                camObj = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
            }
            customFlybyController.camObj = camObj;
            KeyboardController keyboardController = go.GetComponent<KeyboardController>();
            if (keyboardController == null)
            {
                go.AddComponent<KeyboardController>();
                go.AddComponent<MidiController>();
                go.AddComponent<ActiveBehaviourController>();
            }
            customFlybyController.initialize();
            objs = new List<NullObj>();
            NullObj obj1 = new NullObj();
            triggers = new List<Trigger>();
        }

        [TearDown]
        public void Teardown()
        {
            customFlybyController.killAll();
        }

        [UnityTest]
        public IEnumerator Adding_Test_To_Registry()
        {
            yield return 0;
            Assert.AreEqual(1.0f, 1.0f);
        }

        [UnityTest]
        public IEnumerator Can_Add_Custom_Hello_Script_To_Instance()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "HelloWorldCube";
            registerTriggers();
            initializeObjs();
            yield return 0;
            helloWorld = Obj.getPrefabInstance(instance).GetComponent<HelloWorld>();
            Assert.IsNotNull(helloWorld);
        }

        [UnityTest]
        public IEnumerator Can_Move_A_Nested_Child_Via_A_Position_Remap()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "TestPositionRemap";
            obj1.triggerPosition = new ActiveTrigger
            {
                target = new Vec
                {
                    x = 1,
                    y = 1,
                    z = 1
                },
                childIndex = 0,
                onTime = 0.1f,
                offTime = -1,
                trigger = "moveChild"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            GameObject instance = getFirstPrefabInstance();
            GameObject remap_child = instance.transform.GetChild(0).gameObject;
            GameObject nested_child = instance.transform.GetChild(1).GetChild(0).GetChild(0).gameObject;
            Assert.AreEqual(remap_child.transform.localPosition, Vector3.zero);
            Assert.AreEqual(nested_child.transform.localPosition, Vector3.zero);
            triggerEvent("moveChild");
            yield return new WaitForSeconds(0.2f);
            Assert.AreEqual(remap_child.transform.localPosition, new Vector3(1, 1, 1));
            Assert.AreEqual(nested_child.transform.localPosition, new Vector3(1, 1, 1));
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Custom_Script_Intro_From_Instance()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "HelloWorldCube";
            obj1.trigger = "intro";
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            helloWorld = Obj.getPrefabInstance(instance).GetComponent<HelloWorld>();
            Assert.AreEqual(1, helloWorld.numTimesIntroduced);
        }


        [UnityTest]
        public IEnumerator Can_Trigger_Custom_Script_Trigger_From_Instance()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "HelloWorldCube";
            obj1.trigger = "intro";
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            helloWorld = Obj.getPrefabInstance(instance).GetComponent<HelloWorld>();
            Assert.AreEqual(0, helloWorld.numTimesTriggered);
            helloWorld.triggers = new List<ActiveTrigger>{
                new ActiveTrigger{
                    trigger = "customTrigger",
                }
            };
            triggerEvent("customTrigger");
            yield return 0;
            Assert.AreEqual(1, helloWorld.numTimesTriggered);
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Custom_Script_With_A_Knob()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "HelloWorldCube";
            obj1.trigger = "intro";
            Trigger t = new Trigger
            {
                eventName = "customKnobTrigger",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            helloWorld = Obj.getPrefabInstance(instance).GetComponent<HelloWorld>();
            Assert.AreEqual(0, helloWorld.numTimesTriggered);
            helloWorld.triggers = new List<ActiveTrigger>{
                new ActiveTrigger{
                    trigger = "customKnobTrigger",
                    knob = new Vec
                    {
                        x = 0,
                        x2 = 10
                    },
                    behaviour = "hello1" //the important part of this test - the behaviour id. 
                }
            };

            Assert.AreEqual(helloWorld.knobValue, 0);
            triggerKnob(0.5f);
            yield return 0;
            Assert.AreEqual(helloWorld.knobValue, 5f);
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Specific_Behaviour_For_Instance_From_Knob()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "HelloWorldCube";
            obj1.trigger = "intro";
            Trigger t = new Trigger
            {
                eventName = "customKnobTrigger",
                channel = 0,
                knob = 1
            };
            addTrigger(t);
            registerTriggers();
            addTrigger(t);
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            helloWorld = Obj.getPrefabInstance(instance).GetComponent<HelloWorld>();
            Assert.AreEqual(0, helloWorld.numTimesTriggered);
            helloWorld.triggers = new List<ActiveTrigger>{
                new ActiveTrigger{
                    trigger = "customKnobTrigger",
                    knob = new Vec
                    {
                        x = 0,
                        x2 = 10
                    },
                    behaviour = "bogus" //the important part of this test - the behaviour id. 
                }
            };

            Assert.AreEqual(helloWorld.knobValue, 0);
            triggerKnob(0.5f);
            yield return 0;
            Assert.AreEqual(helloWorld.knobValue, 0); //was not triggered because the behavior id was bogus.

            helloWorld.triggers[0].behaviour = "hello1";

            triggerKnob(0.5f);
            yield return 0;
            Assert.AreEqual(helloWorld.knobValue, 5f);

        }


        [UnityTest]
        public IEnumerator Can_Trigger_Specific_Behaviour_For_Instance()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "HelloWorldCube";
            obj1.trigger = "intro";
            registerTriggers();
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            helloWorld = Obj.getPrefabInstance(instance).GetComponent<HelloWorld>();
            Assert.AreEqual(0, helloWorld.numTimesTriggered);
            helloWorld.triggers = new List<ActiveTrigger>{
                new ActiveTrigger{
                    trigger = "customTrigger",
                    behaviour = "bogus" //the important part of this test - the behaviour id. 
                }
            };
            triggerEvent("customTrigger");
            yield return 0;
            Assert.AreEqual(0, helloWorld.numTimesTriggered);
            helloWorld.triggers[0].behaviour = "hello1";
            triggerEvent("customTrigger");
            yield return 0;
            Assert.AreEqual(1, helloWorld.numTimesTriggered);
        }

        [UnityTest]
        public IEnumerator Can_Trigger_Custom_Script_From_Data_With_ActiveTrigger()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "HelloWorldCube";
            obj1.trigger = "intro";
            obj1.activeTrigger = new ActiveTrigger
            {
                trigger = "customTrigger",
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            helloWorld = Obj.getPrefabInstance(instance).GetComponent<HelloWorld>();
            triggerEvent("customTrigger");
            yield return 0;
            Assert.AreEqual(1, helloWorld.numTimesTriggered);
        }

        [UnityTest]
        public IEnumerator Can_Copy_A_Custom_ActiveTrigger()
        {
            obj1 = createBlankObj();
            obj1.id = "customObj";
            obj1.trigger = "intro";
            obj1.bundle = "TestPrefabs";
            obj1.asset = "HelloWorldCube";
            obj1.trigger = "intro";
            obj1.activeTrigger = new ActiveTrigger
            {
                trigger = "customTrigger",
            };
            NullObj obj2 = createBlankObj();
            obj2.preset = "customObj";
            obj2.id = "copied nstance";
            obj2.activeTrigger = new ActiveTrigger
            {
                trigger = "customTrigger2"
            };
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            GameObject instance = Obj.getPrefabInstance(customFlybyController.currentPart.objs[1].instances[0]);
            HelloWorld helloWorldCopied = instance.GetComponent<HelloWorld>();
            Assert.AreEqual(instance.transform.position, Vector3.zero);
            Assert.AreEqual(0, helloWorldCopied.numTimesTriggered);
            triggerEvent("customTrigger2");
            Assert.AreEqual(1, helloWorldCopied.numTimesTriggered);
        }

        [UnityTest]
        public IEnumerator Can_Load_Custom_Obj_With_No_Hierarchy()
        {
            obj1 = createBlankObj();
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(0, instance.transform.childCount);
        }

        [UnityTest]
        public IEnumerator Can_Load_Speed_And_Position_With_No_Hierarchy()
        {
            obj1 = createBlankObj();
            obj1.id = "flat_hierarchy";
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            obj1.triggerSpeed = new ActiveTrigger
            {
                startTime = 0,
                startDelay = 0.1f,
                endTime = -1,
                value = new Vec
                {
                    x = 1,
                },
                trigger = "triggerZero",
                target = new Vec
                {
                    a = 0,
                },
                onTime = 0,
                offDelay = 0.2f,
                offTime = 0.1f,
            };
            obj1.triggerPosition = new ActiveTrigger
            {
                startTime = -1,
                endTime = -1,
                target = new Vec
                {
                    a = 0,
                },
                onTime = 0,
                offDelay = 0.15f,
                trigger = "triggerZero",
                offTime = -1,
            };
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return new WaitForSeconds(0.2f); //let it drift away at first. 
            Assert.Greater(instance.transform.position.x, 0);
            triggerEvent("triggerZero");
            instance = getFirstInstance(); //trying to fix test conflict.
            ActivePosition activePosition = instance.GetComponent<ActivePosition>();
            yield return new WaitForSecondsOrTrue(0.1f, () => instance.transform.position == Vector3.zero);
            Assert.IsTrue(Math.RoughEquals(instance.transform.position, Vector3.zero));
            yield return new WaitForSecondsOrTrue(0.5f, () => instance.transform.position.x > 0);
            yield return 0;
            Assert.Greater(instance.transform.position.x, 0);
        }

        [UnityTest]
        public IEnumerator Can_Load_Scene_Additively_In_Tests()
        {
            //SceneManager.LoadScene("TestAdditiveScene", LoadSceneMode.Additive);
            AsyncOperation async = SceneManager.LoadSceneAsync("TestAdditiveScene", LoadSceneMode.Additive);
            yield return async;
            GameObject helloWorldGO = GameObject.Find("HelloWorldCube");
            Assert.IsNotNull(helloWorldGO);
        }

        public override void initializeObjs()
        {
            customFlybyController.quickSetup(objs);
            instance = getFirstInstance();
        }

        public override void triggerEvent(TriggerObj triggerObj)
        {
            customFlybyController.triggerController.triggerSources[0].trigger(triggerObj);
        }

        public override void triggerEvent(string eventName)
        {
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = eventName;
            customFlybyController.triggerController.triggerSources[0].trigger(triggerObj);
        }

        public override void registerTriggers()
        {
            customFlybyController.registerTriggers(triggers);
        }

        public override GameObject getFirstInstance()
        {
            return customFlybyController.currentPart.objs[0].instances[0];
        }
    }
}

