using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.VFX;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests {
    public class FlyByVFXTests {
        FlyByController flybyController;
        GameObject go;
        GameObject camObj;
        AssetController assetController;
        List<NullObj> objs;
        List<Trigger> triggers;
        NullObj obj1;
        private GameObject instance;
        private ActiveVFX activeVFX;
        private VisualEffect vfx;

        ActiveTrigger a;
        Obj o;

        [SetUp]
        public void Setup() {
            go = GameObject.Find("FlyByController");
            if (go == null) {
                go = new GameObject("FlyByController");
            }
            flybyController = go.GetComponent<FlyByController>();
            if (flybyController == null) {
                flybyController = go.AddComponent<FlyByController>();
            }

            flybyController.configPath = "config.sample.json"; //empty
            if (assetController == null) {
                AssetController assetController = new AssetController();
                assetController.prefabCache = new Dictionary<string, GameObject>();
                camObj = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
            }
            flybyController.camObj = camObj;
            KeyboardController keyboardController = go.GetComponent<KeyboardController>();
            if (keyboardController == null) {
                go.AddComponent<KeyboardController>();
                go.AddComponent<MidiController>();
                go.AddComponent<ActiveBehaviourController>();
            }
            flybyController.initialize();
            objs = new List<NullObj>();
            NullObj obj1 = new NullObj();
            triggers = new List<Trigger>();
        }

        [TearDown]
        public void Teardown() {
            flybyController.killAll();
            // why do we need the below ones? 
            // investigate during compilation. 
            activeVFX.Kill();
        }

        [UnityTest]
        public IEnumerator ActiveVFX_Is_Not_Null() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VFXCube";
            obj1.trigger = "intro";
            initializeObjs();
            yield return 0;
            Assert.IsNotNull(activeVFX);
        }


        [UnityTest]
        public IEnumerator ActiveVFX_Can_Be_Triggered() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VFXCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.propertyName = "spawnRate";
            a.target = new Vec(1, 0, 0);
            a.startTime = 0;
            a.endTime = 0;
            a.trigger = "trigger_vfx";
            a.value = new Vec(0, 0, 0);
            a.onTime = 0.1f;
            a.offDelay = 0.1f;
            a.offTime = 0.1f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(vfx.GetFloat(a.propertyName), 0);
            triggerEvent("trigger_vfx");
            yield return new WaitForSecondsOrTrue(1, () => a.on);
            yield return 0;
            Assert.AreEqual(vfx.GetFloat(a.propertyName), 1);
            yield return new WaitForSecondsOrTrue(1, () => !a.on);
            yield return 0;
            Assert.AreEqual(vfx.GetFloat(a.propertyName), 0);
        }

        [UnityTest]
        public IEnumerator End_Time_Works_With_Sustain_For_VFX() {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "VFXCube";
            obj1.trigger = "intro_sustain";
            initializeObjs();
            a.startTime = 0.1f;
            a.endTime = 0.1f;
            a.start = new Vec(0, 0, 0);
            a.value = new Vec(1, 0, 0);
            a.end = new Vec(0, 0, 0);
            a.propertyName = "spawnRate";
            TriggerObj trigger_intro_sustain = new TriggerObj {
                eventName = "intro_sustain",
                sustain = true,
            };
            TriggerObj trigger_intro_sustain_off = new TriggerObj {
                eventName = "intro_sustain",
                sustain = true,
                noteOff = true
            };
            triggerEvent(trigger_intro_sustain);
            Assert.AreEqual(vfx.GetFloat(a.propertyName), 0);
            yield return new WaitForSecondsOrTrue(1, () => a.introduced);
            Assert.AreEqual(vfx.GetFloat(a.propertyName), 1);
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(vfx.GetFloat(a.propertyName), 1);
            triggerEvent(trigger_intro_sustain_off);
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(vfx.GetFloat(a.propertyName), 0);
        }


        private NullObj createBlankObj() {
            objs.Add(new NullObj());
            return objs[objs.Count - 1];
        }

        private void initializeObjs() {
            flybyController.quickSetup(objs);
            instance = getFirstPrefabInstance();
            activeVFX = instance.GetComponent<ActiveVFX>();
            vfx = instance.GetComponent<VisualEffect>();
            activeVFX.triggers = new List<ActiveTrigger>();
            a = new ActiveTrigger();
            activeVFX.triggers.Add(a);
            o = flybyController.currentPart.objs[0];
            activeVFX.setupActiveTriggers(o);
        }

        private void triggerEvent(TriggerObj triggerObj) {
            flybyController.triggerController.triggerSources[0].trigger(triggerObj);
        }

        private void triggerEvent(string eventName) {
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = eventName;
            flybyController.triggerController.triggerSources[0].trigger(triggerObj);
        }

        private void triggerKnob(float value) {
            //triggers knob 0, channel 0. TODO - extend if you need more channels for these tests. 
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, value);
        }

        private void triggerKnob(Trigger t, float value) {
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(t.channel, t.knob, value);
        }

        private void addTrigger(Trigger t) {
            triggers.Add(t);
        }

        private void registerTriggers() {
            flybyController.registerTriggers(triggers);
        }


        private GameObject getFirstPrefabInstance() {
            return Obj.getPrefabInstance(getFirstInstance());
        }

        private GameObject getPrefabInstanceByIndex(int i) {
            return Obj.getPrefabInstance(getInstanceByIndex(i));
        }

        private GameObject getFirstInstance() {
            return flybyController.currentPart.objs[0].instances[0];
        }

        private GameObject getInstanceByIndex(int i) {
            return flybyController.currentPart.objs[0].instances[i];
        }

    }
}
