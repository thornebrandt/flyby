using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Tools;
using flyby.Triggers;
using flyby.Active;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class FlyByShaderTests : PlayModeTests
    {
        private ActiveShader activeShader;
        private Renderer rend;
        private Material material;

        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
            // why do we need the below ones? 
            // investigate during compilation. 
            activeShader.Kill();
        }

        [UnityTest]
        public IEnumerator ActiveShader_Is_Not_Null()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "NoiseCube";
            obj1.trigger = "intro";
            initializeObjs();
            yield return 0;
            Assert.IsNotNull(activeShader);
        }

        [UnityTest]
        public IEnumerator ActiveShader_Can_Start_With_Secondary_Value()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "NoiseCube";
            obj1.trigger = "intro";
            obj1.hierarchyType = HierarchyType.None;
            initializeObjs();
            activeShader.triggers.Add(
                new ActiveTrigger
                {
                    startTime = 0,
                    value = new Vec(1, 0, 0),
                    propertyName = "noiseAmount"
                }
            );
            triggerEvent("intro");
            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(material.GetFloat("noiseAmount"), 1);
        }

        [UnityTest]
        public IEnumerator ActiveShader_Can_Be_Triggered()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "NoiseCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_shader";
            a.propertyName = "noiseAmount";
            a.target = new Vec(1, 0, 0);
            a.value = new Vec(0, 0, 0);
            a.startTime = 0;
            a.endTime = 0;
            a.onTime = 0.1f;
            a.offDelay = 0.2f;
            a.offTime = 0.1f;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetFloat(a.propertyName), 0);
            triggerEvent("trigger_shader");
            yield return new WaitForSecondsOrTrue(1, () => a.on);
            yield return 0;
            Assert.AreEqual(material.GetFloat(a.propertyName), 1);
            yield return new WaitForSecondsOrTrue(1, () => !a.on);
            yield return 0;
            Assert.AreEqual(material.GetFloat(a.propertyName), 0);
        }

        // [UnityTest]
        // public IEnumerator Active_Shader_Can_Trigger_Color_Value()
        // {
        //     //incorrect label - should use activeColor for this. 
        //     obj1 = createBlankObj();
        //     obj1.bundle = "TestPrefabs";
        //     obj1.asset = "ColorCube";
        //     obj1.trigger = "intro";
        //     initializeObjs();
        //     a.trigger = "trigger_shader";
        //     a.propertyName = "_ColorR";
        //     a.target = new Vec(1, 0, 0);
        //     a.value = new Vec(0, 0, 0);
        //     a.startTime = 0;
        //     a.endTime = 0;
        //     a.onTime = 0.1f;
        //     a.offTime = -1;
        //     triggerEvent("intro");
        //     yield return 0;
        //     Assert.AreEqual(material.GetFloat(a.propertyName), 0);
        // }

        [UnityTest]
        public IEnumerator Active_Shader_Can_Use_Vector3_As_Value()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "ColorCube";
            obj1.trigger = "intro";
            initializeObjs();
            a.trigger = "trigger_shader";
            a.propertyName = "_VectorColor";
            a.target = new Vec(0, 1, 1);
            a.value = new Vec(0, 0, 0);
            a.startTime = 0;
            a.endTime = 0;
            a.onTime = 0.1f;
            a.offTime = -1;
            triggerEvent("intro");
            yield return 0;
            Assert.AreEqual(material.GetVector(a.propertyName), new Vector4(0, 0, 0, 0));
            triggerEvent("trigger_shader");
            yield return new WaitForSecondsOrTrue(1, () => a.on);
            yield return 0;
            Assert.AreEqual(material.GetVector(a.propertyName), new Vector4(0, 1, 1, 0));

        }

        [UnityTest]
        public IEnumerator End_Time_Works_With_Sustain_For_Shaders()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "NoiseCube";
            obj1.trigger = "intro_sustain";
            initializeObjs();
            a.startTime = 0.1f;
            a.endTime = 0.1f;
            a.start = new Vec(0, 0, 0);
            a.value = new Vec(1, 0, 0);
            a.end = new Vec(0, 0, 0);
            a.propertyName = "noiseAmount";
            TriggerObj trigger_intro_sustain = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
            };
            TriggerObj trigger_intro_sustain_off = new TriggerObj
            {
                eventName = "intro_sustain",
                sustain = true,
                noteOff = true
            };
            triggerEvent(trigger_intro_sustain);
            Assert.AreEqual(material.GetFloat(a.propertyName), 0);
            yield return new WaitForSecondsOrTrue(1, () => a.introduced);
            Assert.AreEqual(material.GetFloat(a.propertyName), 1);
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(material.GetFloat(a.propertyName), 1);
            triggerEvent(trigger_intro_sustain_off);
            yield return new WaitForSeconds(0.3f);
            Assert.AreEqual(material.GetFloat(a.propertyName), 0);
        }

        public override void initializeObjs()
        {
            base.initializeObjs();
            flybyController.quickSetup(objs);
            instance = getFirstPrefabInstance();
            activeShader = instance.GetComponent<ActiveShader>();
            rend = instance.GetComponent<Renderer>();
            material = rend.material;
            activeShader.triggers = new List<ActiveTrigger>();
            a = new ActiveTrigger();
            activeShader.triggers.Add(a);
            o = flybyController.currentPart.objs[0];
            activeShader.setupActiveTriggers(o);
        }
    }
}
