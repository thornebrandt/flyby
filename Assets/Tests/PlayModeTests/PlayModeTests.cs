using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Active;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    //parent class 
    public class PlayModeTests
    {
        public FlyByController flybyController;
        public GameObject go;
        public GameObject camObj;
        public AssetController assetController;
        public List<NullObj> objs;
        public List<Trigger> triggers;
        public NullObj obj1;
        public GameObject instance;
        public ActiveTrigger a;
        public Obj o;
        public string configPath = "config.sample.json"; //emty by default. 
        public string colorProperty = "_BaseColor"; //move list of possible material properties into config.  


        public NullObj createBlankObj()
        {
            objs.Add(new NullObj());
            return objs[objs.Count - 1];
        }


        public void setup()
        {
            go = GameObject.Find("FlyByController");
            if (go == null)
            {
                go = new GameObject("FlyByController");
            }
            flybyController = go.GetComponent<FlyByController>();
            if (flybyController == null)
            {
                flybyController = go.AddComponent<FlyByController>();
            }

            flybyController.configPath = configPath;
            if (assetController == null)
            {
                AssetController assetController = new AssetController();
                assetController.prefabCache = new Dictionary<string, GameObject>();
                GameObject cam = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
                this.camObj = Object.Instantiate(cam) as GameObject;

            }

            flybyController.camObj = camObj;
            KeyboardController keyboardController = go.GetComponent<KeyboardController>();
            if (keyboardController == null)
            {
                go.AddComponent<KeyboardController>();
                go.AddComponent<MidiController>();
                go.AddComponent<TimeTriggerController>();
                go.AddComponent<ActiveBehaviourController>();

            }
            flybyController.initialize();
            objs = new List<NullObj>();
            NullObj obj1 = new NullObj();
            triggers = new List<Trigger>();
            o = flybyController.currentPart.objs[0]; //experimental. 
        }

        public void teardown()
        {
            flybyController.killAll();
        }


        public virtual void initializeObjs()
        {
            flybyController.quickSetup(objs);
            instance = getFirstPopupInstance();
        }

        public virtual void triggerEvent(TriggerObj triggerObj)
        {
            flybyController.triggerController.triggerSources[0].trigger(triggerObj);
        }

        public virtual void triggerEvent(string eventName)
        {
            TriggerObj triggerObj = new TriggerObj();
            triggerObj.eventName = eventName;
            flybyController.triggerController.triggerSources[0].trigger(triggerObj);
        }

        public void triggerKnob(float value)
        {
            //triggers knob 0, channel 0. TODO - extend if you need more channels for these tests. 
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(0, 1, value);
        }

        public void triggerKnob(Trigger t, float value)
        {
            MidiController midiController = flybyController.triggerSources[1] as MidiController;
            midiController.OnMidiKnob(t.channel, t.knob, value);
        }

        public void addTrigger(Trigger t)
        {
            triggers.Add(t);
        }

        public virtual void registerTriggers()
        {
            flybyController.registerTriggers(triggers);
        }

        public GameObject getFirstPrefabInstance()
        {
            return Obj.getPrefabInstance(getFirstInstance());
        }

        public GameObject getPrefabInstanceByIndex(int i)
        {
            return Obj.getPrefabInstance(getInstanceByIndex(i));
        }

        public GameObject getFirstPopupInstance()
        {
            return Obj.getPopupInstance(getFirstInstance());
        }

        public GameObject getPopupInstanceByIndex(int i)
        {
            return Obj.getPopupInstance(getInstanceByIndex(i));
        }

        public virtual GameObject getFirstInstance()
        {
            return flybyController.currentPart.objs[0].instances[0];
        }

        public GameObject getInstanceByIndex(int i)
        {
            return flybyController.currentPart.objs[0].instances[i];
        }

        public GameObject getInstanceByObjIndex(int i)
        {
            return flybyController.currentPart.objs[i].instances[0];
        }

        public GameObject getFirstRotateScaleInstance()
        {
            return Obj.getRotateScaleInstance(flybyController.currentPart.objs[0].instances[0]);
        }

        public GameObject getRotateScaleInstanceByIndex(int i)
        {
            return Obj.getRotateScaleInstance(flybyController.currentPart.objs[0].instances[1]);
        }
    }
}