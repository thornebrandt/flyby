using System.Collections;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Active;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests
{
    public class DisabledTests : PlayModeTests
    {

        [SetUp]
        public void Setup()
        {
            this.setup();
        }

        [TearDown]
        public void Teardown()
        {
            flybyController.killAll();
        }

        [Test]
        public void Disabled_Does_Not_Show()
        {
            obj1 = createBlankObj();
            obj1.disabled = "true";
            initializeObjs();
            Assert.AreEqual(flybyController.currentPart.objs[0].instances.Count, 0);
            //Assert.AreEqual(instance.transform.position, o.nullVector);
        }

        [Test]
        public void Solo_Disabled_All_Other_Objs()
        {
            obj1 = createBlankObj();
            obj1.id = "obj1";
            NullObj obj2 = createBlankObj();
            obj2.id = "obj2";
            NullObj obj3 = createBlankObj();
            obj3.id = "obj3";
            obj3.solo = "true";
            initializeObjs();
            Assert.AreEqual(flybyController.currentPart.objs[0].instances.Count, 0);
            Assert.AreEqual(flybyController.currentPart.objs[1].instances.Count, 0);
            Assert.AreEqual(flybyController.currentPart.objs[2].instances.Count, 1);
        }

        [UnityTest]
        public IEnumerator Disabled_ActivePosition_Not_Fired()
        {
            obj1 = createBlankObj();
            obj1.triggerPosition = new ActiveTrigger
            {
                disabled = true,
                value = new Vec
                {
                    x = 1
                }
            };
            obj1.numInstances = 1;
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            GameObject instance = flybyController.currentPart.objs[0].instances[0];
            Assert.AreEqual(instance.transform.position.x, 0);
        }

        [UnityTest]
        public IEnumerator Disabled_ActiveRotation_Not_Fired()
        {
            obj1 = createBlankObj();
            obj1.triggerRotation = new ActiveTrigger
            {
                disabled = true,
                value = new Vec
                {
                    x = 90
                }
            };
            obj1.numInstances = 1;
            obj1.trigger = "intro";
            initializeObjs();
            triggerEvent("intro");
            yield return 0;
            GameObject instance = flybyController.currentPart.objs[0].instances[0];
            Assert.AreEqual(instance.transform.localRotation.eulerAngles.x, 0);
        }

        [UnityTest]
        public IEnumerator Disabled_Blendshape_Not_Fired()
        {
            obj1 = createBlankObj();
            obj1.bundle = "TestPrefabs";
            obj1.asset = "BlendCube";
            obj1.trigger = "intro";
            initializeObjs();
            GameObject instance = Obj.getPrefabInstance(flybyController.currentPart.objs[0].instances[0]);
            ActiveBlendShape activeBlendShape = instance.GetComponent<ActiveBlendShape>();
            SkinnedMeshRenderer rend = instance.GetComponent<SkinnedMeshRenderer>();
            activeBlendShape.triggers = new List<ActiveTrigger>();
            a = new ActiveTrigger();
            activeBlendShape.triggers.Add(a);
            activeBlendShape.setupActiveTriggers(o);
            a.trigger = "trigger_blendshape";
            a.target = new Vec(100, 0, 0);
            a.onTime = 0.1f;
            a.offDelay = 0.1f;
            a.offTime = 0.1f;
            a.disabled = true; //what we are testing. 
            triggerEvent("intro");
            triggerEvent("trigger_blendshape");
            yield return new WaitForSeconds(0.1f);
            yield return 0;
            Assert.AreEqual(rend.GetBlendShapeWeight(0), 0);

        }

        public override void initializeObjs()
        {
            flybyController.quickSetup(objs);
            //do not get first instance bc it doesn't exist. 
        }
    }
}