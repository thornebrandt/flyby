using System.Collections;
using System.Collections.Generic;
using flyby;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class EditorTestScript {
    // A Test behaves as an ordinary method
    [Test]
    public void LoadsAnExample () {
        Example example = new Example ();
        Assert.IsNotNull (example);
        // Use the Assert class to test conditions
    }

    [Test]
    public void CallsAnExampleFunction () {
        Example example = new Example ();
        //Assert.IsTrue(example.returnTrue());
    }
}