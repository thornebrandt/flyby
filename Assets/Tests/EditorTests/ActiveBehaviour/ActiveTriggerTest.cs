using flyby.Active;
using flyby.Core;
using flyby.Triggers;
using Newtonsoft.Json;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    namespace Active
    {
        public class ActiveTriggerTest
        {
            [Test]
            public void Constructs_A_Default_ActiveTrigger()
            {
                ActiveTrigger a = new ActiveTrigger();
                Assert.IsNotNull(a);
                Assert.AreEqual(a.onTime, 0.18f);
            }

            [Test]
            public void Double_Equal_Comparisons_Return_True()
            {
                ActiveTrigger a1 = new ActiveTrigger();
                ActiveTrigger a2 = new ActiveTrigger();
                Assert.IsTrue(a1 == a2);
            }

            [Test]
            public void Not_Equal_Comparisons_Returns_True_For_Different_Triggers()
            {
                ActiveTrigger a1 = new ActiveTrigger();
                ActiveTrigger a2 = new ActiveTrigger();
                a1.onTime = 300;
                Assert.IsTrue(a1 != a2);
            }

            [Test]
            public void Not_Equal_Comparisons_Returns_False_For_Identical_Triggers()
            {
                ActiveTrigger a1 = new ActiveTrigger();
                ActiveTrigger a2 = new ActiveTrigger();
                Assert.IsFalse(a1 != a2);
            }

            [Test]
            public void Not_Equal_Comparisons_Return_False_For_Triggers_With_Same_Trigger()
            {
                ActiveTrigger a1 = new ActiveTrigger();
                ActiveTrigger a2 = new ActiveTrigger();
                a1.trigger = "trigger";
                a2.trigger = "trigger";
                Assert.IsFalse(a1 != a2);
            }

            [Test]
            public void Double_Equal_Comparisons_Return_False_If_One_Null()
            {
                ActiveTrigger a1 = new ActiveTrigger();
                ActiveTrigger a2 = null;
                Assert.IsFalse(a1 == a2);
                ActiveTrigger a3 = null;
                ActiveTrigger a4 = new ActiveTrigger();
                Assert.IsFalse(a3 == a4);
            }

            [Test]
            public void Double_Equal_Comparisons_Return_True_If_Both_Null()
            {
                ActiveTrigger a1 = null;
                ActiveTrigger a2 = null;
                Assert.IsTrue(a1 == a2);
            }

            [Test]
            public void Maps_Same_Value_If_No_TriggerProperty()
            {
                ActiveTrigger a = new ActiveTrigger();
                float value = 0.1f;
                Assert.AreEqual(a.map(value), value);
            }

            [Test]
            public void Maps_A_TriggerProperty()
            {
                //checks for existence of trigger propety and maps to correct value if it exists. otherwise, default. 
                ActiveTrigger a = new ActiveTrigger
                {
                    triggerProperty = new TriggerProperty
                    {
                        start = 0.5f,
                    }
                };
                Assert.AreEqual(a.map(0), 0.5f);
            }

            [Test]
            public void Clamps_Min_Max_From_TriggerProperty()
            {
                ActiveTrigger a = new ActiveTrigger
                {
                    triggerProperty = new TriggerProperty
                    {
                        clampMin = 0.4f,
                        clampMax = 0.6f
                    }
                };
                Assert.AreEqual(a.map(0), 0.4f);
                Assert.AreEqual(a.map(1), 0.6f);
            }

            [Test]
            public void Converts_Secondary_Trigger_To_ActiveTrigger()
            {
                SecondaryActiveTrigger sat = new SecondaryActiveTrigger
                {
                    target = new Vec(1, 0, 0),
                    onTime = 0.11f,
                    offTime = 0.22f,
                };
                ActiveTrigger a = new ActiveTrigger(sat);
                Assert.AreEqual(a.target, sat.target);
                Assert.AreEqual(a.onTime, 0.11f);
                Assert.AreEqual(a.offTime, 0.22f);
            }
        }
    }
}