using System.Collections.Generic;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    namespace Triggers
    {
        public class TriggerPropertyTest
        {
            TriggerProperty tp;

            [SetUp]
            public void Setup()
            {
                tp = new TriggerProperty();
            }


            [Test]
            public void Can_Create_A_TriggerProperty()
            {
                Assert.IsNotNull(tp);
            }

            [Test]
            public void Clamps_A_Minimum_Value()
            {
                tp.clampMin = 0.5f;
                Assert.AreEqual(tp.map(0), 0.5f);
            }

            [Test]
            public void Clamps_A_Maximum_Value()
            {
                tp.clampMax = 0.5f;
                Assert.AreEqual(tp.map(1), 0.5f);
            }

            [Test]
            public void Clamps_Both_Min_And_Max_Value()
            {
                tp.clampMax = 0.6f;
                tp.clampMin = 0.5f;
                Assert.AreEqual(tp.map(1), 0.6f);
                Assert.AreEqual(tp.map(0), 0.5f);
            }

            [Test]
            public void Scales_A_Value()
            {
                tp.max = 2;
                Assert.AreEqual(tp.map(1), 2);
            }

            [Test]
            public void Scales_A_Range()
            {
                tp.rangeMax = 0.5f;
                Assert.AreEqual(tp.map(0.25f), 0.5f);
            }

            [Test]
            public void Scales_A_Range_At_Max()
            {
                tp.rangeMax = 0.5f;
                Assert.AreEqual(tp.map(0.5f), 1);
            }

            [Test]
            public void Scales_A_Range_At_Min()
            {
                tp.rangeMin = 0.5f;
                Assert.AreEqual(tp.map(0.5f), 0);
            }

            [Test]
            public void Scales_A_Range_Below_Min()
            {
                tp.rangeMin = 0.5f;
                Assert.AreEqual(tp.map(0.4f), 0);
            }

            [Test]
            public void Scales_A_Range_Above_Max()
            {
                tp.rangeMax = 0.5f;
                Assert.AreEqual(tp.map(0.5f), 1);
            }

            [Test]
            public void Inverts_A_Value()
            {
                tp.invert = true;
                Assert.AreEqual(tp.map(0.75f), 0.25f);
            }

            [Test]
            public void Inverts_A_Scaled_Value()
            {
                tp.invert = true;
                tp.max = 2;
                Assert.AreEqual(tp.map(0), 2);
            }

            [Test]
            public void Shows_Start_Value()
            {
                tp.start = 0.5f;
                Assert.AreEqual(tp.map(0), 0.5f);
            }

            [Test]
            public void Only_Shows_Start_Value_Once()
            {
                tp.start = 0.5f;
                Assert.AreEqual(tp.map(0), 0.5f);
                Assert.AreEqual(tp.map(0.1f), 0.1f);
                Assert.AreEqual(tp.map(0), 0);
            }
        }
    }
}