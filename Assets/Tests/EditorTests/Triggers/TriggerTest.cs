using System.Collections.Generic;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    namespace Triggers {
        public class TriggerTest {
            [Test]
            public void Can_Create_A_Trigger () {
                Trigger trigger = new Trigger ();
                Assert.IsNotNull (trigger);
            }
        }
    }
}