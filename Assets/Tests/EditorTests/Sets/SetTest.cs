using System.Collections.Generic;
using flyby.Core;
using flyby.Sets;
using flyby.Tools;
using flyby.Controllers;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    namespace Sets {
        public class SetTest {
            [Test]
            public void Can_Create_A_Set() {
                Set set = new Set();
                Assert.IsNotNull(set);
            }

            [Test]
            public void Can_Load_Set_From_JSON() {
                Set set = new Set();
                set.jsonPath = "Assets/Tests/PlayModeTests/json/";
                set = set.getSetFromJSON("sets/default_set.json");
                Assert.AreEqual(set.songs[0], "songs/default_song.json");
                Assert.AreEqual(set.triggers[0], "triggers/default_triggers.json");
            }

            [Test]
            public void Can_Load_Song_From_Set() {
                Set set = new Set();
                set.jsonPath = "Assets/Tests/PlayModeTests/json/";
                Set.OnSongChanged += SongChangedHandler;
                set.songs = new List<string>();
                set.songs.Add("songs/default_song.json");
                Song preloadedSong = Song.fromJSON(set.jsonPath + "songs/default_song.json");
                set.preloadedSongs.Add(preloadedSong);
                set.loadSong(0);
                Assert.AreEqual(set.currentSong.currentPart.objs[0], new Obj());
            }

            [Test]
            public void Can_Load_Triggers_From_Set() {
                Set set = new Set();
                set.jsonPath = "Assets/Tests/PlayModeTests/json/";
                set.triggers.Add("triggers/default_triggers.json");
                set.loadedTriggers = set.loadTriggers();
                Assert.AreEqual(set.loadedTriggers[0], new Trigger());

            }


            public void SongChangedHandler(Song song) {
                //mock.
            }
        }
    }
}