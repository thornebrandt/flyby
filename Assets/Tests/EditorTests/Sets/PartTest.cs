using System.Collections;
using System.Collections.Generic;
using flyby.Controllers;
using flyby.Core;
using flyby.Sets;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    namespace Sets
    {
        public class PartTest
        {
            [Test]
            public void Can_Create_A_Part()
            {
                Part part = new Part();
                Assert.IsNotNull(part);
            }

            [Test]
            public void Part_Has_One_Camera_Angle_By_Default()
            {
                Part part = new Part();
                Assert.AreEqual(part.cameraAngles.Count, 1);
            }

            [Test]
            public void Part_Has_Next_Trigger_By_Default()
            {
                Part part = new Part();
                Assert.AreEqual(part.nextTrigger, "next_part");
            }

            [Test]
            public void Part_Has_Prev_Trigger_By_Default()
            {
                Part part = new Part();
                Assert.AreEqual(part.prevTrigger, "prev_part");
            }

            // [Test]
            // public void Part_Has_A_Current_Camera_Angle_By_Default()
            // {
            //     Part part = new Part();
            //     part.jsonPath = "Assets/Tests/PlayModeTests/json/";
            //     part = part.getPartFromJSON(part.jsonPath + "parts/default_part.json");
            //     Assert.AreEqual(part.currentCameraAngle, part.cameraAngles[0]);
            // }

            [Test]
            public void Can_Load_Components_From_JSON()
            {
                Part part = new Part();
                List<NullObj> nullObjs = part.getNullObjsFromJSON("Assets/Tests/PlayModeTests/json/components/default.json");
                Assert.AreEqual(nullObjs[0], new NullObj());
            }

            [Test]
            public void Can_Load_Parts_From_JSON()
            {
                Part part = new Part();
                part.jsonPath = "Assets/Tests/PlayModeTests/json/";
                part = part.getPartFromJSON(part.jsonPath + "parts/default_part.json");
                Assert.AreEqual(part.objs[0], new Obj());
            }

        }
    }
}