using flyby.Core;
using flyby.Sets;
using flyby.Controllers;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Tests {
    namespace Sets {
        public class SongTest {
            [Test]
            public void Can_Create_A_Song() {
                Song song = new Song();
                Assert.IsNotNull(song);
            }

            [Test]
            public void Can_Load_Songs_From_JSON() {
                Song song = new Song();
                song.jsonPath = "Assets/Tests/PlayModeTests/json/";
                song = song.getSongWithPartsFromJSON(song.jsonPath + "songs/default_song.json");
                Assert.AreEqual(song.loadedParts[0].objs[0], new Obj());
            }

            [Test]
            public void Currrent_Part_Starts_As_Zero() {
                Song song = new Song();
                song.jsonPath = "Assets/Tests/PlayModeTests/json/";
                song = song.getSongWithPartsFromJSON(song.jsonPath + "songs/default_song.json");
                Assert.AreEqual(song.currentPart, song.loadedParts[0]);
            }

            [Test]
            public void Part_Inherits_Bundle_From_Song() {
                Song song = new Song();
                song.jsonPath = "Assets/Tests/PlayModeTests/json/";
                song = song.getSongWithPartsFromJSON(song.jsonPath + "songs/test_different_bundle_song.json");
                Assert.AreEqual("SongDefinedBundle", song.loadedParts[0].bundle);
            }

            [Test]
            public void Song_Overwrites_Song_Bundle() {
                Song song = new Song();
                song.jsonPath = "Assets/Tests/PlayModeTests/json/";
                song = song.getSongWithPartsFromJSON(song.jsonPath + "songs/test_different_bundle_song.json");
                Assert.AreEqual("PartDefinedBundle", song.loadedParts[1].bundle);
            }
        }
    }
}