using flyby.Core;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    namespace Core {
        public class CameraAngleTest {
            [Test]
            public void Constructs_A_Camera_Angle() {
                CameraAngle c = new CameraAngle();
                Assert.IsNotNull(c);
            }

            [Test]
            public void Default_Camera_Angle_Has_A_Z_Of_Negative_5_For_Offset() {
                CameraAngle c = new CameraAngle();
                Assert.AreEqual(c.offset.z, -5);
            }

            [Test]
            public void Default_Camera_Angle_Has_A_Default_Next_Trigger() {
                CameraAngle c = new CameraAngle();
                Assert.AreEqual(c.nextTrigger, "next_camera_angle");
            }

            [Test]
            public void Default_Camera_Angle_Has_A_Default_Prev_Trigger() {
                CameraAngle c = new CameraAngle();
                Assert.AreEqual(c.prevTrigger, "prev_camera_angle");
            }
        }
    }
}