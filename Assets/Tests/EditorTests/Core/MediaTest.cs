using System;
using flyby.Core;
using flyby.Tools;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    namespace Core {
        public class MediaTest {
            [Test]
            public void Equality_Comparisons_Return_Equal() {
                Media media1 = new Media();
                Media media2 = new Media();
                Assert.AreEqual(media1, media2);
            }

            [Test]
            public void Non_Equality_Comparisons_Do_Not_Return_Equal() {
                Media media1 = new Media();
                media1.id = "different";
                Media media2 = new Media();
                Assert.AreNotEqual(media1, media2);
            }


            [Test]
            public void Double_Equal_Comparisons_Return_True() {
                Media media1 = new Media();
                Media media2 = new Media();
                Assert.IsTrue(media1 == media2);
            }

            [Test]
            public void Double_Equal_Comparisons_Returns_False_For_Different_Medias() {
                Media media1 = new Media();
                Media media2 = new Media();
                media1.id = "different";
                Assert.IsFalse(media1 == media2);
            }

            [Test]
            public void Not_Equal_Comparisons_Returns_True_For_Different_Medias() {
                Media media1 = new Media();
                media1.id = "different";
                Media media2 = new Media();
                Assert.IsTrue(media1 != media2);
            }

            [Test]
            public void Two_Different_Medias_With_Same_Id_Are_Equal() {
                Media media1 = new Media();
                media1.id = "same_media";
                Media media2 = new Media();
                media2.id = "same_media";
                Assert.IsTrue(media1 == media2);
            }

        }
    }
}