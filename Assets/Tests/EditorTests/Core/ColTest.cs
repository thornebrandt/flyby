using System;
using flyby.Core;
using flyby.Tools;
using Newtonsoft.Json;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    namespace Core
    {
        public class ColTest
        {
            Color black = new Color(0, 0, 0, 1);
            Color white = new Color(1, 1, 1);
            Color red = new Color(1, 0, 0);
            Color green = new Color(0, 1, 0);
            Color blue = new Color(0, 0, 1);
            Color yellow = new Color(1, 1, 0);

            [Test]
            public void Constructs_Blank_Col_As_Black()
            {
                Col col = new Col();
                Assert.AreEqual(black, col.getColor());
            }

            [Test]
            public void Constructs_Col_With_Float_1_As_White()
            {
                Col col = new Col(1);
                Assert.AreEqual(white, col.getColor());
            }

            [Test]
            public void Constructs_Red_Col()
            {
                Col col = new Col(1, 0, 0);
                Assert.AreEqual(red, col.getColor());
            }

            [Test]
            public void Constructs_Red_Col_From_Preset()
            {
                Col col = new Col("red");
                Assert.AreEqual(red, col.getColor());
            }

            [Test]
            public void Constructs_Red_Col_From_Color_Red()
            {
                Col col = new Col(new Color(1, 0, 0));
                Assert.AreEqual(red, col.getColor());
            }

            [Test]
            public void Contructs_Blue_Col()
            {
                Col col = new Col(0, 0, 1);
                Color blue = Color.blue;
                Assert.AreEqual(blue, col.getColor());
            }

            [Test]
            public void Assign_Yellow_Col_Manually()
            {
                Col col = new Col();
                col.r = 1;
                col.g = 1;
                Assert.AreEqual(yellow, col.getColor());
            }

            [Test]
            public void Parse_Black_Col_From_JSON()
            {
                Col col = getColFromJSON("blank.json");
                Assert.AreEqual(black, col.getColor());
            }

            [Test]
            public void Parse_Red_Col_From_JSON()
            {
                Col col = getColFromJSON("red.json");
                Assert.AreEqual(red, col.getColor());
            }

            [Test]
            public void Random_Colors_Do_Not_Match()
            {
                Col col = getColFromJSON("random.json");
                Color randomColor1 = col.getColor();
                Color randomColor2 = col.getColor();
                Assert.AreNotEqual(randomColor1, randomColor2);
            }

            [Test]
            public void Creates_Random_Gray()
            {
                Col col = getColFromJSON("random_gray.json");
                Color randomGray1 = col.getColor();
                Color randomGray2 = col.getColor();
                //assert randomness of gray. `
                Assert.AreNotEqual(randomGray1.r, randomGray2.r);
                //assert that r and b are the same.
                Assert.AreEqual(randomGray1.r, randomGray1.b);
            }

            [Test]
            public void Red_Overrides_Black()
            {
                Col col = new Col(0);
                col.r = 1;
                Assert.AreEqual(red, col.getColor());
            }

            [Test]
            public void R1_Becomes_Gray_Value_If_R2_Is_Present()
            {
                Col col = new Col(0.1f);
                col.r2 = 2;
                Color color;
                for (int i = 0; i < 10; i++)
                {
                    color = col.getColor();
                    if (color.r > 0.1f)
                    {
                        break;
                    }
                }
                Assert.IsTrue(col.getColor().r > 0.1f);
            }

            [Test]
            public void R1_Becomes_Zero_If_R2_Is_Present_And_No_Gray()
            {
                Col col = new Col(0);
                col.r2 = 2;
                Color color;
                for (int i = 0; i < 10; i++)
                {
                    color = col.getColor();
                    if (color.r > 0)
                    {
                        break;
                    }
                }
                Assert.IsTrue(col.getColor().r > 0);
            }

            [Test]
            public void Zeroes_Override_White()
            {
                Col col = new Col(1);
                col.b = 0;
                Assert.AreEqual(yellow, col.getColor());
            }

            [Test]
            public void Uniform_Is_Default_And_Keeps_Colors_Gray()
            {
                Col col = new Col();
                Assert.IsTrue(col.uniform);
                Col randomGray = getColFromJSON("random_gray.json");
                Color randomGrayColor = randomGray.getColor();
                Assert.AreEqual(randomGrayColor.r, randomGrayColor.b);
            }

            [Test]
            public void Uniform_Off_Creates_Random_Colors()
            {
                Col col = new Col();
                col.uniform = false;
                col.gray = 0;
                col.gray2 = 1;
                Color randomColor = col.getColor();
                Assert.AreNotEqual(randomColor.r, randomColor.b);
            }

            [Test]
            public void Uniform_False_Creates_Colors_Other_Than_Red()
            {
                Col col = new Col();
                col.uniform = false;
                col.gray = 0;
                col.gray2 = 1;
                Color randomColor = col.getColor();
                bool redNotGreater = false;
                for (int i = 0; i < 10; i++)
                {
                    randomColor = col.getColor();
                    if (randomColor.r < randomColor.b || randomColor.r < randomColor.g)
                    {
                        redNotGreater = true;
                        break;
                    }
                }
                Assert.IsTrue(redNotGreater);
            }

            [Test]
            public void Saturation_Makes_White_Red()
            {
                Col col = new Col(1);
                col.sat = 1;
                Assert.AreEqual(red, col.getColor());
            }

            [Test]
            public void Saturation_Navy_Blue_Turns_Blue()
            {
                Col col = new Col(0.9f);
                col.b = 1;
                col.sat = 1;
                Assert.AreEqual(blue, col.getColor());
            }

            [Test]
            public void Zero_Saturation_Turns_Red_White()
            {
                Col col = new Col(1, 0, 0);
                col.sat = 0;
                Assert.AreEqual(white, col.getColor());
            }

            [Test]
            public void Brightness_Turns_Gray_White()
            {
                Col col = new Col(0.5f);
                col.brightness = 1;
                Assert.AreEqual(white, col.getColor());
            }

            [Test]
            public void Zero_Hue_And_Full_Sat_Turns_White_Red()
            {
                Col col = new Col(1f);
                col.hue = 0;
                col.sat = 1;
                Assert.AreEqual(red, col.getColor());
            }

            [Test]
            public void One_Third_Hue_And_Full_Sat_Turns_Color_Green()
            {
                Col col = new Col(1f);
                col.hue = 1.0f / 3.0f;
                col.sat = 1;
                Assert.AreEqual(green, col.getColor());
            }

            [Test]
            public void Two_Thirds_Hue__And_Full_Sat_Turns_Color_Green()
            {
                Col col = new Col(1f);
                col.hue = 2.0f / 3.0f;
                col.sat = 1;
                Assert.AreEqual(blue, col.getColor());
            }

            [Test]
            public void Col_Can_Copy_Grays()
            {
                Col col1 = new Col(0.5f);
                Col col2 = new Col(col1);
                Assert.AreEqual(0.5f, col2.getColor().r);
            }

            [Test]
            public void Col_Can_Copy_Colors()
            {
                Col col1 = new Col(0.5f, 0.5f, 0.5f);
                Col col2 = new Col(col1);
                Assert.AreEqual(0.5f, col2.getColor().r);
            }

            [Test]
            public void White_Overrides_Copy()
            {
                Col col1 = new Col(0.5f);
                Col col2 = new Col(col1);
                overWriteColFromJSON(col2, "white.json");
                Assert.AreEqual(1, col2.getColor().r);
            }

            [Test]
            public void Black_Overrides_Copy()
            {
                Col col1 = new Col(0.5f);
                Col col2 = new Col(col1);
                overWriteColFromJSON(col2, "black.json");
                Assert.AreEqual(0, col2.getColor().r);
            }

            [Test]
            public void Color_Overrides_Copy()
            {
                Col col1 = new Col(0.5f);
                Col col2 = new Col(col1);
                overWriteColFromJSON(col2, "red.json");
                Color color2 = col2.getColor();
                Assert.AreEqual(1, color2.r);
                Assert.AreEqual(0.5f, color2.b);
            }

            [Test]
            public void Uniform_False_Is_Copied()
            {
                Col col1 = new Col();
                col1.gray = 0;
                col1.gray2 = 1;
                col1.uniform = false;
                Color color1 = col1.getColor();
                Assert.AreNotEqual(color1.r, color1.g);
                Col col2 = new Col(col1);
                Assert.IsFalse(col1.uniform);
                Color color2 = col2.getColor();
                Assert.AreNotEqual(color2.r, color2.g);
            }

            [Test]
            public void Uniform_True_Overrides_Copy()
            {
                Col col1 = getColFromJSON("random_gray.json");
                col1.uniform = false;
                Col col2 = new Col(col1);
                overWriteColFromJSON(col2, "uniform_true.json");
                Assert.IsTrue(col2.uniform);
            }

            [Test]
            public void Uniform_False_Overrides_Copy()
            {
                Col col1 = getColFromJSON("random_gray.json");
                col1.uniform = true;
                Col col2 = new Col(col1);
                overWriteColFromJSON(col2, "uniform_false.json");
                Assert.IsFalse(col2.uniform);
            }

            [Test]
            public void Equality_Comparisons_Return_Equal()
            {
                Col col1 = new Col();
                Col col2 = new Col();
                Assert.AreEqual(col1, col2);
            }

            [Test]
            public void Non_Equality_Comparisons_Do_Not_Return_Equal()
            {
                Col col1 = new Col(0.5f);
                Col col2 = new Col(1f);
                Assert.AreNotEqual(col1, col2);
            }

            [Test]
            public void Double_Equal_Comparisons_Return_True()
            {
                Col col1 = new Col();
                Col col2 = new Col();
                Assert.IsTrue(col1 == col2);
            }

            [Test]
            public void Double_Equal_Comparisons_Returns_False_For_Different_Cols()
            {
                Col col1 = new Col(0.1f);
                Col col2 = new Col(0.8f);
                Assert.IsFalse(col1 == col2);
            }

            [Test]
            public void Not_Equal_Comparisons_Returns_True_For_Different_Cols()
            {
                Col col1 = new Col(0.1f);
                Col col2 = new Col();
                Assert.IsTrue(col1 != col2);
            }

            [Test]
            public void Not_Equal_Comparisons_Returns_False_For_Identical_Cols()
            {
                Col col1 = new Col();
                Col col2 = new Col();
                Assert.IsFalse(col1 != col2);
            }

            [Test]
            public void Can_Make_A_Clear_Color()
            {
                Col col = new Col();
                col.alpha = 0;
                Assert.AreEqual(col.getColor(), Color.clear);
            }

            [Test]
            public void Presets_Work()
            {
                Col col = new Col();
                col.color = "white";
                Assert.AreEqual(col.getColor(), Color.white);
                col = new Col();
                col.color = "clear";
                Assert.AreEqual(col.getColor(), Color.clear);
                col = new Col();
                col.color = "gray";
                Assert.AreEqual(col.getColor(), Color.gray);
                col = new Col();
                col.color = "red";
                Assert.AreEqual(col.getColor(), Color.red);
                col = new Col();
                col.color = "green";
                Assert.AreEqual(col.getColor(), Color.green);
                col = new Col();
                col.color = "blue";
                Assert.AreEqual(col.getColor(), Color.blue);
                col = new Col();
                col.color = "random";
                Color randomColor1 = col.getColor();
                Color randomColor2 = col.getColor();
                Assert.AreNotEqual(randomColor1, randomColor2);
            }

            [Test]
            public void Can_Add_Two_Colors()
            {
                Color addedColors = Col.AddColors(Color.black, Color.white);
                Assert.AreEqual(addedColors, Color.white);
            }

            [Test]
            public void Col_Can_Lerp_Two_Values()
            {
                Col col = new Col();
                col.r = 0;
                col.r2 = 1;
                Color color = col.lerpColor(0.5f);
                Assert.AreEqual(color, new Color(0.5f, 0, 0));
            }

            [Test]
            public void Col_Can_Lerp_Between_White_And_Red()
            {
                Col col = new Col();
                col.r = 1;
                col.r2 = 1;
                col.g = 1;
                col.g2 = 0;
                col.b = 1;
                col.b2 = 0;
                Color lerpRed = col.lerpColor(1.0f);
                Color lerpWhite = col.lerpColor(0.0f);
                Assert.AreEqual(lerpRed, Color.red);
                Assert.AreEqual(lerpWhite, Color.white);
            }

            [Test]
            public void Col_Can_Lerp_Two_Grays()
            {
                Col col = new Col();
                col.gray = 0;
                col.gray2 = 1;
                Color color = col.lerpColor(0.5f);
                Assert.AreEqual(color, Color.gray);
            }

            [Test]
            public void R_Is_Gray_If_R2_Is_Present_While_Lerping()
            {
                Col col = new Col();
                col.gray = 0.5f;
                col.r2 = 1;
                Color color = col.lerpColor(0.5f);
                Assert.AreEqual(color.r, 0.75f);
            }


            [Test]
            public void R_Is_Zero_If_R2_Is_Present_While_Lerping_And_There_Is_No_Gray()
            {
                Col col = new Col();
                col.r2 = 1;
                Color color = col.lerpColor(0.5f);
                Assert.AreEqual(color.r, 0.5f);
            }

            [Test]
            public void Add_Colors_Works()
            {
                Col col = new Col();
                Col col2 = new Col(1, 0, 0);
                Color addedColor = Col.AddColors(col.getColor(), col2.getColor());
                Assert.AreEqual(addedColor, red);
            }

            [Test]
            public void Add_Colors_Larger_Than_One_Works()
            {
                Col col1 = new Col(1, 1, 1);
                Col col2 = new Col(1, 1, 1);
                Color addedColor = Col.AddColors(col1.getColor(), col2.getColor());
                Assert.AreEqual(addedColor, new Color(2, 2, 2));
            }

            [Test]
            public void Colorize_Blend_Mode_Works()
            {
                Col baseColor = new Col(1, 1, 1);
                Col blendColor = new Col(1, 0, 0);
                Color addedColor = Col.AddColors(baseColor.getColor(), blendColor.getColor(), BlendMode.Colorize);
                Assert.AreEqual(addedColor, red);
            }

            [Test]
            public void Colorize_Blend_Mode_Works_Mid_Transition()
            {
                Col baseColor = new Col(1, 1, 1);
                Col blendColor = new Col(0.5f, 0, 0);
                Color addedColor = Col.AddColors(baseColor.getColor(), blendColor.getColor(), BlendMode.Colorize);
                Assert.AreEqual(addedColor, new Color(1, 0.5f, 0.5f));
            }

            [Test]
            public void Colorize_Blend_Mode_Does_Not_Break_Dim_Colors()
            {
                Col baseColor = new Col(0, 0, 0);
                Col blendColor = new Col(0.5f, 0, 0);
                Color addedColor = Col.AddColors(baseColor.getColor(), blendColor.getColor(), BlendMode.Colorize);
                Assert.AreEqual(addedColor, new Color(0.5f, 0, 0));
            }

            [Test]
            public void Col_Add_Can_Subtract_Colors()
            {
                Col baseColor = new Col(0, 0, 0);
                Col blendColor = new Col(-0.5f, -0.5f, -0.5f);
                Color addedColor = Col.AddColors(baseColor.getColor(), blendColor.getColor(), BlendMode.Default);
                Assert.AreEqual(addedColor, new Color(-0.5f, -0.5f, -0.5f));
            }

            [Test]
            public void Col_Presets_Can_Be_Sequential()
            {
                Col col = new Col();
                col.color = "cyan_red";
                Assert.AreEqual(col.getColor(), new Color(0, 1, 1));
                Assert.AreEqual(col.getColor(), new Color(1, 0, 0));
                //loops. 
                Assert.AreEqual(col.getColor(), new Color(0, 1, 1));
            }

            [Test]
            public void RGB_Preset_Sequences_Red_Green_And_Blue()
            {
                Col col = new Col();
                col.color = "rgb";
                Assert.AreEqual(col.getColor(), new Color(1, 0, 0));
                Assert.AreEqual(col.getColor(), new Color(0, 1, 0));
                Assert.AreEqual(col.getColor(), new Color(0, 0, 1));
                Assert.AreEqual(col.getColor(), new Color(1, 0, 0));
            }

            [Test]
            public void Col_Can_Add_Another_Col_From_Itself()
            {
                Col baseColor = new Col(0, 0, 0);
                Col newColor = new Col(0.5f, 0.5f, 0.5f);
                baseColor.Add(newColor);
                Assert.AreEqual(baseColor.getColor(), new Color(0.5f, 0.5f, 0.5f));
                baseColor = new Col();
                baseColor.gray = 1.0f;
                newColor = new Col();
                newColor.gray = -0.5f;
                baseColor.Add(newColor);
                Assert.AreEqual(baseColor.getColor(), new Color(0.5f, 0.5f, 0.5f));
                baseColor = new Col();
                baseColor.gray = 1.0f;
                newColor = new Col();
                newColor.r = -0.5f;
                baseColor.Add(newColor);
                Assert.AreEqual(baseColor.getColor(), new Color(0.5f, 1.0f, 1.0f));
            }

            [Test]
            public void Can_Multiply_Col_By_A_Numer()
            {
                Col col = new Col(1, 0, 0);
                col.Multiply(2);
                Assert.AreEqual(col.getColor(), new Color(2, 0, 0));
                col = new Col();
                col.gray = 0.1f;
                Assert.AreEqual(col.getColor(), new Color(0.1f, 0.1f, 0.1f));
                col.Multiply(2);
                Assert.AreEqual(col.getColor(), new Color(0.2f, 0.2f, 0.2f));
                col = new Col();
                col.alpha = 0.1f;
                col.Multiply(2);
                Assert.AreEqual(col.getColor(), new Color(0, 0, 0, 0.2f));
                col = new Col();
                col.gray = 0.1f;
                col.g = 0.3f;
                col.Multiply(2);
                Assert.AreEqual(col.getColor(), new Color(0.2f, 0.6f, 0.2f));
                col = new Col();
                col.gray = 1.0f;
                col.Multiply(5);
                Assert.AreEqual(col.getColor(), new Color(5, 5, 5));
                col = new Col();
                col.Multiply(0);
                Assert.AreEqual(col.getColor(), Color.black);
            }

            [Test]
            public void Col_Defaults_Seed_To_Negative_999()
            {
                Col col = new Col();
                Assert.AreEqual(col.seed, -999);
            }

            [Test]
            public void Two_Random_Cols_With_A_Seed_Produce_The_Same_Result()
            {
                Col col = new Col(0, 0, 0);
                col.r2 = 1;
                col.g2 = 1;
                col.b2 = 1;
                col.seed = 1;
                Col col2 = new Col(0, 0, 0);
                col2.r2 = 1;
                col2.g2 = 1;
                col2.b2 = 1;
                col2.seed = 1;
                Assert.AreEqual(col.getColor(), col2.getColor());
            }

            [Test]
            public void Col_Produces_Different_Value_For_Each_Channel_When_Seeded()
            {
                Col col = new Col(0, 0, 0);
                col.r2 = 1;
                col.g2 = 1;
                col.b2 = 1;
                col.seed = 1;
                Assert.AreNotEqual(col.getColor().r, col.getColor().g);
            }

            private Col getColFromJSON(string JSONPath)
            {
                string json = Json.getJSON("Assets/Tests/EditorTests/json/col/" + JSONPath);
                return JsonConvert.DeserializeObject<Col>(json);
            }

            private void overWriteColFromJSON(Col col, string JSONPath)
            {
                string json = Json.getJSON("Assets/Tests/EditorTests/json/col/" + JSONPath);
                try
                {
                    JsonUtility.FromJsonOverwrite(json, col);
                }
                catch (Exception e)
                {
                    Debug.Log("error overwriting: " + e);
                }
            }

        }

    }
}