using System;
using System.Collections.Generic;
using flyby.Controllers;
using flyby.Core;
using flyby.Triggers;
using flyby.Tools;
using flyby.Active;
using Newtonsoft.Json;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    namespace Core
    {
        public class ObjTest
        {

            Obj defaultObj = new Obj();
            TriggerObj t = new TriggerObj();
            GameObject go;


            [SetUp]
            public void Setup()
            {
                go = new GameObject();
                ActiveBehaviourController activeBehaviourController = go.AddComponent<ActiveBehaviourController>();
                activeBehaviourController.setup();
            }

            [TearDown]
            public void Teardown()
            {
                GameObject.DestroyImmediate(go);
            }

            [Test]
            public void Constructs_Default_Obj()
            {
                Obj obj = new Obj();
                //assert all default values. 
                Assert.AreEqual(new Vec(), obj.origin);
            }

            [Test]
            public void Parse_Obj_From_JSON()
            {
                Obj obj = getObjFromJSON("default.json");
                Assert.AreEqual(defaultObj, obj);
            }

            [Test]
            public void Equality_Comparisons_Return_Equal()
            {
                Obj obj1 = new Obj();
                Obj obj2 = new Obj();
                Assert.AreEqual(obj1, obj2);
            }

            [Test]
            public void Can_Parse_Blank_Obj_From_JSON()
            {
                Obj obj1 = getObjFromJSON("default.json");
                Obj obj2 = new Obj();
                Assert.AreEqual(obj1, obj2);
            }

            [Test]
            public void Can_Load_Enum_From_JSON()
            {
                Obj enumObj = getObjFromJSON("enum.json");
                Assert.AreEqual(enumObj.activeType, ActiveType.Random);
            }

            [Test]
            public void ActiveInstanceIndexes_Defaults_To_Last_Obj()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 1);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
            }

            [Test]
            public void Obj_With_Group_Does_Note_Instantiate_Without_Group_In_Trigger()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 1;
                obj.trigger = "testTrigger";
                obj.group = "testGroup";
                TriggerObj triggerObj = new TriggerObj
                {
                    eventName = "testTrigger"
                };
                obj.current = true;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.TriggerEventHandler(triggerObj);
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
            }

            // [Test]
            // public void Obj_With_Group_Does_Not_Instantiate_Without_Correct_Group_Index()
            // {
            //     Obj obj = new Obj();
            //     obj.prefab = new GameObject();
            //     obj.numInstances = 1;
            //     obj.trigger = "testTrigger";
            //     obj.group = "testGroup";
            //     obj.groupIndex = 1;
            //     TriggerObj triggerObj = new TriggerObj
            //     {
            //         eventName = "testTrigger",
            //         group = "testGroup"
            //     };
            //     obj.current = true;
            //     obj.setupInstances();
            //     Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
            //     obj.TriggerEventHandler(triggerObj);
            //     Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
            // }

            // [Test]
            // public void Obj_With_Group_Does_Instantiate_With_Correct_Group_And_GroupIndex()
            // {
            //     Obj obj = new Obj();
            //     obj.prefab = new GameObject();
            //     obj.numInstances = 1;
            //     obj.trigger = "testTrigger";
            //     obj.group = "testGroup";
            //     obj.groupIndex = 1;
            //     TriggerObj triggerObj = new TriggerObj
            //     {
            //         eventName = "testTrigger",
            //         group = "testGroup",
            //         groupIndex = 1
            //     };
            //     obj.current = true;
            //     obj.setupInstances();
            //     Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
            //     obj.TriggerEventHandler(triggerObj);
            //     Assert.AreEqual(obj.activeInstanceIndexes.Count, 1);
            // }

            [Test]
            public void First_Instance_Is_Active_If_ActiveType_Is_Next_Before_Trigger()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Next;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 1);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
            }

            [Test]
            public void ActiveType_Next_Loops_Around_Correctly()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Next;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 1);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
            }

            [Test]
            public void ActiveType_Second_Behaves_Correctly()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Second;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 1);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
            }

            [Test]
            public void ActiveType_All_Affects_All_Instances()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.All;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 3);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 3);
            }

            [Test]
            public void ActiveType_Random_Affects_Random_Visible_Instance()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 10;
                obj.activeType = ActiveType.Random;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                int lastIndex = 0;
                bool allMatching = true;
                for (int i = 0; i < obj.numInstances * 2; i++)
                {
                    int max = (int)Mathf.Max(i, obj.numInstances);
                    obj.instantiate(t);
                    int randomIndex = obj.activeInstanceIndexes[0];
                    Assert.Less(randomIndex, max);
                    if (randomIndex != lastIndex)
                    {
                        allMatching = false;
                    }
                    lastIndex = randomIndex;
                }
                Assert.False(allMatching);
            }


            [Test]
            public void ActiveType_Closest_Affects_Closest_Instance_To_Origin()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Closest;
                obj.setupInstances();
                obj.instances[0].transform.position = new Vector3(1, 0, 0);
                obj.instances[1].transform.position = new Vector3(0.1f, 0, 0);
                obj.instances[2].transform.position = new Vector3(2, 0, 0);
                List<int> activeInstanceIndexes = obj.getActiveInstanceIndexes(2, obj.activeType, obj.instances);
                Assert.AreEqual(activeInstanceIndexes[0], 1);
            }

            [Test]
            public void ActiveType_Closest_Affects_Most_Recent_Instance_If_Distances_Are_Same()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Closest;
                obj.setupInstances();
                obj.instances[0].transform.position = new Vector3(1, -1, 1);
                obj.instances[1].transform.position = new Vector3(1, 1, -1);
                obj.instances[2].transform.position = new Vector3(-1, 1, 1);
                List<int> activeInstanceIndexes = obj.getActiveInstanceIndexes(2, obj.activeType, obj.instances);
                Assert.AreEqual(activeInstanceIndexes[0], 2);
            }

            [Test]
            public void ActiveType_Closest_Behaves_Correctly_When_Looped()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Closest;
                obj.origin = new Vec(0);
                obj.setupInstances();
                obj.instantiate(t);
                Assert.AreEqual(obj.instances[0].transform.position, Vector3.zero);
                Assert.AreEqual(obj.instances[1].transform.position, obj.nullVector);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.instances[0].transform.position, Vector3.zero);
                Assert.AreEqual(obj.instances[1].transform.position, Vector3.zero);
                Assert.AreEqual(obj.instances[2].transform.position, obj.nullVector);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 1);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                obj.origin = new Vec(1, 1, 1);
                obj.instantiate(t);
                //making obj farther away. 
                Assert.AreEqual(obj.instances[1].transform.position, new Vector3(1, 1, 1));
                //should be last trigger called?
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                //return origin back to zero.
                obj.origin = new Vec(0);
                obj.instantiate(t);
                //should now be 2 since it is most recent called and also closest. 
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
            }

            [Test]
            public void ActiveType_Middle_Returns_Middle_Obj_Odd()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Middle;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                obj.instantiate(t);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 1);
            }

            [Test]
            public void ActiveType_Middle_Returns_Only_Active()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Middle;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 1);
            }

            [Test]
            public void ActiveType_Middle_Returns_Middle_Obj_Even()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 4;
                obj.activeType = ActiveType.Middle;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                obj.instantiate(t);
                obj.instantiate(t);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
            }

            [Test]
            public void ActiveType_Middle_Returns_Middle_Obj_Single()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 1;
                obj.activeType = ActiveType.Middle;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
            }


            [Test]
            public void ActiveType_Middle_Returns_Middle_Obj_Odd_Looped()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 3;
                obj.activeType = ActiveType.Middle;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                obj.instantiate(t);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 1);
                //the number should increase with each instantiation as the objs loop. 
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 1);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
            }

            [Test]
            public void ActiveType_Middle_Returns_Middle_Obj_Even_Looped()
            {
                Obj obj = new Obj();
                obj.prefab = new GameObject();
                obj.numInstances = 4;
                obj.activeType = ActiveType.Middle;
                obj.setupInstances();
                Assert.AreEqual(obj.activeInstanceIndexes.Count, 0);
                obj.instantiate(t);
                obj.instantiate(t);
                obj.instantiate(t);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 3);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 1);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 2);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 3);
                obj.instantiate(t);
                Assert.AreEqual(obj.activeInstanceIndexes[0], 0);
            }

            [Test]
            public void Obj_Can_Overwrite_Set_Values_From_A_NullObj()
            {
                NullObj nullObj = new NullObj();
                nullObj.origin = new Vec();
                Obj obj = new Obj();
                obj.origin = new Vec(1, 1, 1);
                obj.overwrite(nullObj);
                Assert.AreEqual(obj.origin, new Vec());
            }

            [Test]
            public void Can_Instantiate_From_A_NullObj()
            {
                NullObj nullObj = new NullObj();
                Obj obj = new Obj(nullObj);
                Assert.AreEqual(obj.numInstances, 1);
            }

            [Test]
            public void Obj_Can_Overwrite_Boolean_Values_From_A_NullObj()
            {
                NullObj nullObj = new NullObj();
                nullObj.preRender = "false";
                Obj obj = new Obj();
                obj.preRender = true;
                obj.overwrite(nullObj);
                Assert.AreEqual(obj.preRender, false);
            }

            private Obj overWriteObjFromJSON(Obj obj, string JSONPath)
            {
                string json = Json.getJSON("Assets/Tests/EditorTests/json/obj/" + JSONPath);
                try
                {
                    JsonConvert.PopulateObject(json, obj);
                }
                catch (Exception e)
                {
                    Debug.Log("error overwriting: " + e);
                }
                return obj;
            }

            private Obj getObjFromJSON(string JSONPath)
            {
                string json = Json.getJSON("Assets/Tests/EditorTests/json/obj/" + JSONPath);
                return JsonConvert.DeserializeObject<Obj>(json);
            }
        }
    }
}