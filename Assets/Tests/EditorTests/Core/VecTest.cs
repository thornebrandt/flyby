using flyby.Core;
using flyby.Tools;
using Newtonsoft.Json;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    namespace Core
    {
        public class VecTest
        {
            Vector3 zero = new Vector3();
            Vector3 one = new Vector3(1, 1, 1);
            Vector3 testVector123 = new Vector3(1, 2, 3);

            [Test]
            public void Constructs_Blank_Vec_As_Zero()
            {
                Vec vec = new Vec();
                Assert.AreEqual(zero, vec.getVector3());
            }

            [Test]
            public void Constructs_Zero_Vec_With_Zero()
            {
                Vec vec = new Vec(0);
                Assert.AreEqual(vec.x, 0);
            }


            [Test]
            public void Constructs_Vec_With_One_Argument()
            {
                Vec vec = new Vec(1);
                Assert.AreEqual(one, vec.getVector3());
            }

            [Test]
            public void Construct_Vec_With_Negative_Values()
            {
                Vec vec = new Vec(-900);
                Assert.AreEqual(new Vector3(-900, -900, -900), vec.getVector3());
            }

            [Test]
            public void Constructs_Vec_With_Three_Arguments()
            {
                Vec vec = new Vec(1, 2, 3);
                Assert.AreEqual(testVector123, vec.getVector3());
            }

            [Test]
            public void Constructs_Vec_With_Six_Arguments()
            {
                Vec vec = new Vec(1, 1, 2, 2, 3, 3);
                Assert.AreEqual(testVector123, vec.getVector3());
            }

            [Test]
            public void Parses_Blank_Vec_From_JSON()
            {
                Vec vec = getVecFromJSON("blank.json");
                Assert.AreEqual(zero, vec.getVector3());
            }

            [Test]
            public void Parses_Vec_From_JSON()
            {
                Vec vec = getVecFromJSON("vec123.json");
                Assert.AreEqual(testVector123, vec.getVector3());
            }

            [Test]
            public void Constructs_Random_Vec()
            {
                Vec vec1 = new Vec(0, 1, 0, 1, 0, 1);
                Vec vec2 = new Vec(0, 1, 0, 1, 0, 1);
                Assert.AreNotEqual(vec1.getVector3(), vec2.getVector3());
            }

            [Test]
            public void Vec_Defaults_Seed_To_Negative_999()
            {
                Vec vec1 = new Vec();
                Assert.AreEqual(vec1.seed, -999);
            }

            [Test]
            public void Two_Random_Vecs_With_A_Seed_Produce_The_Same_Result()
            {
                Vec vec1 = new Vec(0, 1, 0, 1, 0, 1);
                vec1.seed = 1;
                Vec vec2 = new Vec(0, 1, 0, 1, 0, 1);
                vec2.seed = 1;
                Assert.AreEqual(vec1.getVector3(), vec2.getVector3());
            }

            [Test]
            public void Vecs_Produce_Different_Values_For_Each_Axis_When_Seeded()
            {
                Vec vec1 = new Vec(0, 1, 0, 1, 0, 1);
                vec1.seed = 1;
                Assert.AreNotEqual(vec1.getVector3().x, vec1.getVector3().y);
            }

            [Test]
            public void Can_Construct_Vec_From_Another_Vec()
            {
                Vec vec1 = new Vec(1, 1, 1);
                vec1.uniform = true;
                vec1.relative = true;
                vec1.absolute = true;
                vec1.multiple = new Vector3(1, 1, 1);
                Vec vec2 = new Vec(vec1);
                Assert.AreEqual(vec2.getVector3(), new Vector3(1, 1, 1));
                Assert.IsTrue(vec2.uniform);
                Assert.IsTrue(vec2.relative);
                Assert.IsTrue(vec2.absolute);
                Assert.AreEqual(vec2.multiple, new Vector3(1, 1, 1));
            }

            [Test]
            public void Uniform_Random_Vec_Creates_Uniform_Random_Values()
            {
                Vec vec = getVecFromJSON("uniform_true.json");
                Vector3 vector3 = vec.getVector3();
                Assert.AreEqual(vector3.x, vector3.y);
            }

            [Test]
            public void Non_Uniform_Vec_Creates_Non_Uniform_Random_Values()
            {
                Vec vec = getVecFromJSON("uniform_false.json");
                Vector3 vector3 = vec.getVector3();
                Assert.AreNotEqual(vector3.x, vector3.y);
            }

            [Test]
            public void Vec_Creates_Uniform_Positive_Values_From_A()
            {
                Vec vec = new Vec();
                vec.a = 1;
                Vector3 vector3 = vec.getVector3();
                Assert.AreEqual(vector3.x, 1);
            }

            [Test]
            public void Vec_Creates_Uniform_Zero_Values_From_A()
            {
                Vec vec = new Vec();
                vec.a = 0;
                Vector3 vector3 = vec.getVector3();
                Assert.AreEqual(vector3.x, 0);
            }

            [Test]
            public void Random_Vec_Creates_Non_Uniform_Values_By_Default()
            {
                Vec vec = new Vec();
                vec.a = -1;
                vec.a2 = 1;
                Vector3 vector3 = vec.getVector3();
                Assert.AreNotEqual(vector3.x, vector3.y);
            }

            [Test]
            public void Equality_Comparisons_Return_Equal()
            {
                Vec vec1 = new Vec();
                Vec vec2 = new Vec();
                Assert.AreEqual(vec1, vec2);
            }

            [Test]
            public void Non_Equality_Comparisons_Do_Not_Return_Equal()
            {
                Vec vec1 = new Vec(1);
                Vec vec2 = new Vec(2);
                Assert.AreNotEqual(vec1, vec2);
            }

            [Test]
            public void Double_Equal_Comparisons_Return_False_For_Different_Vecs()
            {
                Vec vec1 = new Vec(1);
                Vec vec2 = new Vec(2);
                Assert.IsFalse(vec1 == vec2);
            }

            [Test]
            public void Not_Equal_Comparisons_Return_False()
            {
                Vec vec1 = new Vec();
                Vec vec2 = new Vec();
                Assert.IsFalse(vec1 != vec2);
            }

            [Test]
            public void Not_Equal_Comparisons_Return_True()
            {
                Vec vec1 = new Vec();
                vec1.uniform = false;
                Vec vec2 = new Vec();
                vec2.uniform = true;
                Assert.IsTrue(vec1 != vec2);
            }

            [Test]
            public void Returns_Quaternion_Identity()
            {
                Vec rotationVec = new Vec();
                Assert.AreEqual(rotationVec.getQuaternion(), Quaternion.identity);
            }

            [Test]
            public void Returns_Rotated_Quaternion()
            {
                Vec rotationVec = new Vec(1, 0, 0);
                Assert.AreEqual(rotationVec.getQuaternion().eulerAngles.x, 1);
            }

            [Test]
            public void Returns_Rotated_Quaternion2()
            {
                Vec targetRotation = new Vec(0, 90, 0);
                Assert.AreEqual(targetRotation.getQuaternion().eulerAngles, new Vector3(0, 90, 0));
            }

            [Test]
            public void Quaternions_Multiply_Correctly_1()
            {
                //TODO - can change this for you daily quaternion test needs. 
                Quaternion rotation1 = Quaternion.Euler(0, 10, 0);
                Quaternion rotation2 = Quaternion.Euler(10, 0, 0);
                Assert.IsTrue(Math.RoughEquals(rotation1 * rotation2, Quaternion.Euler(10, 10, 0)));
                //Assert.IsTrue(Math.RoughEquals(rotation2 * rotation1, Quaternion.Euler(10, 10, 0)));
            }

            [Test]
            public void Quaternions_Multiply_Correctly_2()
            {
                Quaternion rotation1 = Quaternion.Euler(0, 90, 0);
                Quaternion rotation2 = Quaternion.identity;
                Quaternion targetRotation = rotation1 * rotation2;
                Vector3 eulerGoal = rotation1.eulerAngles;
                Assert.AreEqual(targetRotation.eulerAngles, eulerGoal);
            }

            [Test]
            public void Can_Lerp_Two_Vectors()
            {
                Vec vec = new Vec();
                vec.x = 1;
                vec.x2 = -1;
                vec.y = 0;
                vec.y2 = 2;
                vec.z = 2;
                vec.z2 = -2;
                Assert.AreEqual(vec.lerpVector3(0.5f), new Vector3(0, 1, 0));
            }

            [Test]
            public void Can_Add_Two_Vectors()
            {
                Vec vec1 = new Vec();
                vec1.x = 1;
                vec1.y = 1;
                Vec vec2 = new Vec();
                vec2.y = 1;
                vec2.z = 3;
                Assert.AreEqual(Vec.Add(vec1.getVector3(), vec2.getVector3()), new Vector3(1, 2, 3));
            }

            [Test]
            public void Can_Lerp_Two_Vectors_With_A_Vector3()
            {
                Vec vec = new Vec();
                vec.x = 1;
                vec.x2 = -1;
                vec.y = 0;
                vec.y2 = 2;
                vec.z = 2;
                vec.z2 = -2;
                Vector3 lerpVector = new Vector3(0.5f, 0.5f, 0.5f);
                Assert.AreEqual(vec.lerpVector3(lerpVector), new Vector3(0, 1, 0));
            }

            [Test]
            public void Can_Lerp_Two_Vectors_With_Only_A_Values()
            {
                Vec vec = new Vec();
                vec.a = 1;
                vec.a2 = 2;
                Assert.AreEqual(vec.lerpVector3(0.5f), new Vector3(1.5f, 1.5f, 1.5f));
            }

            [Test]
            public void Can_Lerp_Between_An_A_Value_And_A_Regular_Value()
            {
                Vec vec = new Vec();
                vec.a = 1;
                vec.x2 = 2;
                Assert.AreEqual(vec.lerpVector3(0.5f), new Vector3(1.5f, 1.0f, 1.0f));
            }

            [Test]
            public void Can_Lerp_Two_Vectors_With_With_A_And_Override()
            {
                Vec vec = new Vec();
                vec.x = 1;
                vec.y = -1;
                vec.y2 = 1;
                vec.a = 1;
                vec.a2 = 2;
                //x: 1, 2 ( 1.5 ) 
                //y: -1, 1 ( 0 )
                //z: 1, 2 ( 1.5 )
                Assert.AreEqual(vec.lerpVector3(0.5f), new Vector3(1.5f, 0f, 1.5f));
            }

            [Test]
            public void Can_Get_Nearest_Multiple_Of_Two_Vectors()
            {
                Vector3 valueVector = new Vector3(7, 6, 8);
                Vector3 multipleVector = new Vector3(3, 7, 0);
                Assert.AreEqual(Vec.getNearestMultiple(valueVector, multipleVector), new Vector3(6, 7, 8));
            }

            [Test]
            public void Can_Multiply_A_Vec_By_A_Number()
            {
                Vec vec = new Vec(1, 0, 0);
                vec.Multiply(2);
                Assert.AreEqual(vec.getVector3(), new Vector3(2, 0, 0));
                vec = new Vec(3, 3, 3);
                vec.Multiply(-1);
                Assert.AreEqual(vec.getVector3(), new Vector3(-3, -3, -3));
                vec = new Vec();
                vec.a = 5;
                vec.Multiply(100);
                Assert.AreEqual(vec.getVector3(), new Vector3(500, 500, 500));
                vec = new Vec();
                vec.Multiply(0);
                Assert.AreEqual(vec.getVector3(), new Vector3(0, 0, 0));
            }

            [Test]
            public void Can_Construct_A_Vec_From_Two_Vector3s()
            {
                Vec vec = new Vec(testVector123, testVector123);
                Assert.AreEqual(vec.getVector3(), testVector123);

                Vector3 vector1 = new Vector3(1, 1, 1);
                Vector3 vector2 = new Vector3(2, 2, 2);
                vec = new Vec(vector1, vector2);
                Vector3 lerpedVector = vec.lerpVector3(0.5f);
                Assert.AreEqual(lerpedVector, new Vector3(1.5f, 1.5f, 1.5f));
                //get knob value. 

            }

            [Test]
            public void Can_Clamp_A_Vec()
            {
                Vec vec1 = new Vec(1, 10, -10);
                Assert.AreEqual(Math.Clamp(vec1.getVector3(), 0, 1), new Vector3(1, 1, 0));
                vec1 = new Vec(0.5f, 0.5f, 0.5f);
            }

            [Test]
            public void Max_Works_For_Vec()
            {
                Vec vec = new Vec();
                Vector3 maxVector = new Vector3(5, 5, 5);
                vec.a = 10;
                vec.max = maxVector;
                Vector3 vector1 = vec.getVector3();
                Assert.AreEqual(vector1, maxVector);
            }

            [Test]
            public void Min_Works_For_Vec()
            {
                Vec vec = new Vec();
                Vector3 minVector = new Vector3(5, 5, 5);
                vec.min = minVector;
                Vector3 vector1 = vec.getVector3();
                Assert.AreEqual(vector1, minVector);
            }

            [Test]
            public void Can_Clamp_A_Vec_Internally()
            {
                Vec vec = new Vec(0, 100, 0);
                Vector3 minVector = new Vector3(5, 5, 5);
                Vector3 maxVector = new Vector3(6, 6, 6);
                vec.min = minVector;
                vec.max = maxVector;
                Vector3 vector1 = vec.getVector3();
                Assert.AreEqual(vector1, new Vector3(5, 6, 5));
            }

            [Test]
            public void Can_Only_Max_Out_Some_Axis()
            {
                Vec vec = new Vec(10, 10, 10);
                vec.max = new Vector3(-999, 5, -999);
                Vector3 vector1 = vec.getVector3();
                Assert.AreEqual(vector1, new Vector3(10, 5, 10));
            }

            [Test]
            public void Can_Add_Two_Vecs()
            {
                Vec vec1 = new Vec();
                Vec vec2 = new Vec();
                vec1.x = 1;
                vec2.y = 1;
                vec1.Add(vec2);
                Assert.AreEqual(vec1.getVector3(), new Vector3(1, 1, 0));
                vec1 = new Vec(1, 1, 1);
                vec2 = new Vec(2, 2, 2);
                vec1.Add(vec2);
                Assert.AreEqual(vec1.getVector3(), new Vector3(3, 3, 3));
                vec1 = new Vec(10, 10, 10, 10, 10, 10);
                vec2 = new Vec(-20, -20, -20, -20, -20, -20);
                vec1.Add(vec2);
                Assert.AreEqual(vec1.getVector3(), new Vector3(-10, -10, -10));
                Assert.AreEqual(vec1.x2, -10);
                vec1 = new Vec();
                vec2 = new Vec();
                vec1.a = 3;
                vec2.a = 2;
                vec1.Add(vec2);
                Assert.AreEqual(vec1.getVector3(), new Vector3(5, 5, 5));
            }

            [Test]
            public void Can_Sanitize_A_Vec()
            {
                //used for passing randomness down to another component.
                //the negative -999s were used to quickly write notation. 
                //turns second axis to first axis. 
                //turns first axis to zero. 
                Vec vec = new Vec();
                vec.x2 = 10;
                vec.y = 20;
                vec.z = 30;
                vec.z2 = 31;
                Vec sanitizedVec = vec.getSanitized();
                Assert.AreEqual(sanitizedVec.x, 0);
                Assert.AreEqual(sanitizedVec.x2, 10);
                Assert.AreEqual(sanitizedVec.y, 20);
                Assert.AreEqual(sanitizedVec.y2, 20);
                Assert.AreEqual(sanitizedVec.z, 30);
                Assert.AreEqual(sanitizedVec.z2, 31);
            }

            private Vec getVecFromJSON(string JSONPath)
            {
                string json = Json.getJSON("Assets/Tests/EditorTests/json/vec/" + JSONPath);
                return JsonConvert.DeserializeObject<Vec>(json);
            }
        }
    }
}