using flyby.Tools;
using flyby.Core;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;

namespace Tests
{
    namespace Tools
    {
        public class ToolsTest
        {
            [Test]
            public void Can_Read_JSON()
            {
                string json = Json.getJSON("Assets/Tests/EditorTests/json/json.json");
                Assert.IsNotNull(json);
            }

            [Test]
            public void Get_Random_Between_Returns_First_Number_If_Neg999()
            {
                Assert.AreEqual(1, Rand.Between(1, -999));
            }

            [Test]
            public void Get_Random_Between_Returns_Zero_If_Both_Neg999()
            {
                Assert.AreEqual(0, Rand.Between(-999, -999));
            }

            [Test]
            public void Get_Random_Between_Two_Identical_Numbers_Returns_Number()
            {
                Assert.AreEqual(3, Rand.Between(3, 3));
            }




            // Meant to be broken. 
            // [Test]
            // public void Bogus_Random_Range () {
            //     found error in math.
            //     float numToCompare = 6.0f;
            //     float newNum;
            //     bool matches = true;
            //     for (int i = 0; i < 100; i++) {
            //         newNum = UnityEngine.Random.Range (numToCompare, numToCompare);
            //         Debug.Log (newNum);
            //         Assert.IsTrue ((newNum - Mathf.Epsilon) <= numToCompare);
            //     }
            // }

            [Test]
            public void Get_Random_Between()
            {
                bool match = false;
                int numAttempts = 10; //very miniscule percent chance of failing. 
                int attempts = 0;
                float num1 = Rand.Between(0, 1);
                float num2 = Rand.Between(0, 1);
                match = num1 == num2;
                while (match == true && attempts < numAttempts)
                {
                    Debug.Log("random numbers matching: " + num1 + ":" + num2 + " attempt: " + attempts);
                    num2 = Rand.Between(0, 1);
                    match = num1 == num2;
                    attempts++;
                }
                Assert.IsFalse(match);
            }

            [Test]
            public void Can_Sort_A_List_Of_Vecs_By_X()
            {
                List<Vec> vecs = new List<Vec>();
                vecs.Add(new Vec(0, 0, 0));
                vecs.Add(new Vec(1, 0, 0));
                vecs.Add(new Vec(-1, 0, 0));
                List<Vec> sortedVecs = Math.SortVecsByX(vecs);
                Assert.AreEqual(vecs[0].x, -1);
            }

            [Test]
            public void Can_Round_Floats()
            {
                float float1 = 1.00f;
                float float2 = 1.001f;
                Assert.IsTrue(Math.Round(float1, 2) == Math.Round(float2, 2));
            }

            [Test]
            public void Round_Does_Not_Round_Too_Much()
            {
                float float1 = 1.00f;
                float float2 = 1.001f;
                Assert.IsFalse(Math.Round(float1, 3) == Math.Round(float2, 3));
            }


            [Test]
            public void Math_Rough_Equals_Compares_Approximate_Colors()
            {
                Color color1 = new Color(
                    1.0001f,
                    0,
                    0
                );
                Color color2 = new Color(
                    1,
                    0,
                    0
                );
                Assert.IsTrue(Math.RoughEquals(color1, color2));
            }

            [Test]
            public void Math_Rough_Equals_Does_Not_Think_Rend_And_Blue_Are_Similar()
            {
                Assert.IsFalse(flyby.Tools.Math.RoughEquals(Color.blue, Color.red));
            }

            [Test]
            public void Math_Rough_Equals_Compares_Approximate_Rotations()
            {
                Quaternion r1 = Quaternion.Euler(
                    90,
                    0,
                    0
                );
                Quaternion r2 = Quaternion.Euler(
                    90.1f,
                    0,
                    0
                );
                Assert.IsTrue(Math.RoughEquals(r1, r2));
            }

            [Test]
            public void Math_Rough_Equals_Returns_False_For_Far_Rotations()
            {
                Quaternion r1 = Quaternion.Euler(
                    90,
                    0,
                    0
                );
                Quaternion r2 = Quaternion.Euler(
                    91f,
                    0,
                    0
                );
                Assert.IsFalse(Math.RoughEquals(r1, r2));
            }

            // [Ignore("Meant to be broken.")]
            // [Test]
            // public void Weird_Quaternions_Versus_Each_Other() {
            //     Vector3 v1 = new Vector3(180, 0, 180);
            //     Quaternion q1 = Quaternion.identity * Quaternion.Euler(v1);
            //     Quaternion q2 = Quaternion.Euler(0, 90, 0);
            //     Vector3 e1 = q1.eulerAngles;
            //     Assert.AreEqual(v1, e1);
            // }

            [Test]
            public void Math_Rough_Equals_Compares_Approximate_Vectors()
            {
                Vector3 v1 = new Vector3(1, 1, 1);
                Vector3 v2 = new Vector3(1.001f, 1, 1);
                Assert.IsTrue(Math.RoughEquals(v1, v2));
            }

            [Test]
            public void Lerp_Interpolates_Between_Two_Numbers()
            {
                float float1 = 1;
                float float2 = 2f;
                Assert.AreEqual(Math.Lerp(float1, float2, 0.5f), 1.5f);
            }

            [Test]
            public void Lerp_Interpolates_Between_Two_Floats()
            {
                float float1 = 0.5f;
                float float2 = 1.0f;
                Assert.AreEqual(Math.Lerp(float1, float2, 0.5f), 0.75f);
            }

            [Test]
            public void Lerp_Interpolates_First_Value_If_Second_Value_Is_NullValue()
            {
                float float1 = 1;
                float float2 = -999;
                Assert.AreEqual(Math.Lerp(float1, float2, 0.5f), 1f);
            }

            [Test]
            public void Lerp_Returns_Zero_If_Both_Values_Are_NullValues()
            {
                float float1 = -999;
                float float2 = -999;
                Assert.AreEqual(Math.Lerp(float1, float2, 0.5f), 0);
            }

            [Test]
            public void Lerp_Interpolates_First_Value_If_Values_Are_Same()
            {
                float float1 = 1;
                float float2 = 1;
                Assert.AreEqual(Math.Lerp(float1, float2, 0.5f), 1f);
            }

            [Test]
            public void Remap_Can_Interpolate_Between_Two_Values()
            {
                Assert.AreEqual(Math.Remap(0, 100, 100, 200, 50), 150);
                Assert.AreEqual(Math.Remap(-100, 100, 0, 100, 0), 50);
            }

            [Test]
            public void Lerp_Plays_Well_With_Negative_Numbers()
            {
                Assert.AreEqual(Math.Lerp(1, -2, 0), 1);
            }

            [Test]
            public void Can_Get_Nearest_Multiple()
            {
                Assert.AreEqual(Math.getNearestMultiple(10, 3), 9);
                Assert.AreEqual(Math.getNearestMultiple(8, 3), 9);
                Assert.AreEqual(Math.getNearestMultiple(11, 2), 12); //goes to higher value. 
            }

            [Test]
            public void Can_Get_Nearest_Multiple_With_Floats()
            {
                Assert.AreEqual(Math.getNearestMultiple(6, 2.5f), 5);
                Assert.AreEqual(Math.getNearestMultiple(6, 3.5f), 7);
                Assert.AreEqual(Math.getNearestMultiple(11.5f, 5.5f), 11);
                Assert.AreEqual(Math.getNearestMultiple(5, 0), 5);
            }

            [Test]
            public void Can_Test_Equality_Of_Matrix4x4()
            {
                Matrix4x4 m1 = new Matrix4x4();
                Matrix4x4 m2 = new Matrix4x4();
                Assert.AreEqual(m1, m2);
            }

            [Test]
            public void Can_Normalize_A_Vector3()
            {
                //should return 
                Vector3 min = new Vector3(-0.5f, -0.5f, -0.5f);
                Vector3 max = new Vector3(0.5f, 0.5f, 0.5f);
                Vector3 vector = Vector3.zero;
                Assert.AreEqual(Math.NormalizeVector3(vector, min, max), new Vector3(0.5f, 0.5f, 0.5f));

                Vector3 handMin = new Vector3(-0.7f, -0.4f, -0.5f);
                Vector3 handMax = new Vector3(0.7f, 1.3f, 0.5f);
            }

            [Test]
            public void Can_Get_Angle_From_Two_Vectors()
            {
                Vector2 v1 = new Vector2(-1, 1);
                Vector2 v2 = new Vector2(1, 1);
                Assert.AreEqual(Math.GetAngle(v1, v2), 0);

                v1 = new Vector2(-10, 1);
                v2 = new Vector2(20, 1);
                Assert.AreEqual(Math.GetAngle(v1, v2), 0);

                v1 = new Vector2(0, 1);
                v2 = new Vector2(0, -3);
                Assert.AreEqual(Math.GetAngle(v1, v2), 270);

                v1 = new Vector2(0, 0);
                v2 = new Vector2(1, 1);
                Assert.AreEqual(Math.GetAngle(v1, v2), 45);
            }

            [Test]
            public void Can_Normalize_Angles_Around_360_Degrees()
            {
                //illustrative use case. 
                Assert.AreEqual(Math.NormalizeAngle(360, 270, 90), 0.5f);
                Assert.AreEqual(Math.NormalizeAngle(0, 270, 90), 0.5f);
                Assert.AreEqual(Math.NormalizeAngle(90, 270, 90), 1);
                Assert.AreEqual(Math.NormalizeAngle(270, 270, 90), 0);
                Assert.AreEqual(Math.NormalizeAngle(-90, 270, 90), 0);
                Assert.AreEqual(Math.NormalizeAngle(0, 260, 100), 0.5f);
                //negative values. 
                Assert.AreEqual(Math.NormalizeAngle(0, -100, 100), 0.5f);
                //complete circle. 
                Assert.AreEqual(Math.NormalizeAngle(0, 180, 180), 0.5f);
                Assert.AreEqual(Math.NormalizeAngle(-90, 180, 180), 0.25f);
                Assert.AreEqual(Math.NormalizeAngle(-90, 90, 90), 0.5f);
                Assert.AreEqual(Math.NormalizeAngle(90, 90, 90), 0);
                Assert.IsTrue(Math.RoughEquals(Math.NormalizeAngle(89.9f, 90, 90), 1));
                Assert.AreEqual(Math.NormalizeAngle(0, 0, 360), 0);
                //clamping:
                Assert.AreEqual(Math.NormalizeAngle(-19, 20, -20), 1);
                Assert.AreEqual(Math.NormalizeAngle(19, 20, -20), 0);
            }
        }
    }
}