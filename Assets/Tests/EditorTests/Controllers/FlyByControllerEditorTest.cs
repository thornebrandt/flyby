using System.Collections.Generic;
using flyby.Controllers;
using flyby.Core;
using flyby.Sets;
using flyby.Triggers;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests {
    namespace Controllers {
        public class FlyByControllerEditorTest {
            //most of an integration test of flybycontroller can be handled here. 

            FlyByController flybyController;
            GameObject go;
            GameObject camObj;
            AssetController assetController;

            [SetUp]
            public void Setup() {
                go = new GameObject("FlyByController");
                ActiveBehaviourController activeBehaviourController = go.AddComponent<ActiveBehaviourController>();
                go.AddComponent<KeyboardController>();
                go.AddComponent<MidiController>();
                if (assetController == null) {
                    AssetController assetController = new AssetController();
                    assetController.prefabCache = new Dictionary<string, GameObject>();
                    camObj = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
                }
                activeBehaviourController.setup();
                flybyController = go.AddComponent<FlyByController>();
                flybyController.configPath = "Assets/Tests/PlayModeTests/json/test_config.json";
                flybyController.camObj = camObj;
                flybyController.initialize();
            }

            [TearDown]
            public void Teardown() {
                flybyController.killAll();
            }

            [Test]
            public void Can_Set_Up_A_FlyByController() {
                Assert.IsNotNull(flybyController.config);
                Obj o = flybyController.currentPart.objs[0];
                Assert.AreEqual(o.id, "test_obj");
                GameObject instance = o.instances[0];
                Assert.AreEqual(instance.name, "test_obj_0");
                Assert.AreEqual(instance.transform.position, o.nullVector);
                TriggerObj triggerObj = new TriggerObj();
                triggerObj.eventName = "test_trigger";
                flybyController.triggerSources[0].trigger(triggerObj);
                Assert.AreEqual(instance.transform.position, Vector3.zero);
            }

            [Test]
            public void Can_Trigger_An_Instance() {
                Obj o = flybyController.currentPart.objs[0];
                GameObject instance = o.instances[0];
                Assert.AreEqual(instance.transform.position, o.nullVector);
                TriggerObj triggerObj = new TriggerObj();
                triggerObj.eventName = "test_trigger";
                flybyController.triggerController.triggerSources[0].trigger(triggerObj);
                Assert.AreEqual(instance.transform.position, Vector3.zero);
            }

            [Test]
            public void Trigger_Only_Affects_Objs_Assigned_By_EventName() {
                Obj o = flybyController.currentPart.objs[0];
                Obj o2 = flybyController.currentPart.objs[1];
                GameObject instance = o.instances[0];
                GameObject instance2 = o2.instances[0];
                TriggerObj triggerObj = new TriggerObj();
                triggerObj.eventName = "test_trigger";
                flybyController.triggerController.triggerSources[0].trigger(triggerObj);
                Assert.AreEqual(instance.transform.position, Vector3.zero);
                Assert.AreEqual(instance2.transform.position, o2.nullVector);
            }

            [Test]
            public void Can_Trigger_Instances_Separately() {
                Obj o = flybyController.currentPart.objs[0];
                GameObject instance1 = o.instances[0];
                GameObject instance2 = o.instances[1];
                GameObject instance3 = o.instances[2];
                TriggerObj triggerObj = new TriggerObj();
                triggerObj.eventName = "test_trigger";
                flybyController.triggerSources[0].trigger(triggerObj);
                Assert.AreEqual(o._i, 1);
                Assert.AreEqual(instance2.transform.position, o.nullVector);
                flybyController.triggerSources[0].trigger(triggerObj);
                Assert.AreEqual(o._i, 2);
                Assert.AreEqual(instance2.transform.position, Vector3.zero);
                Assert.AreEqual(instance3.transform.position, o.nullVector);
                flybyController.triggerSources[0].trigger(triggerObj);
                Assert.AreEqual(o._i, 0);
                Assert.AreEqual(instance3.transform.position, Vector3.zero);
            }

            [Test]
            public void Can_Trigger_With_A_Keypress() {
                Obj o = flybyController.currentPart.objs[0];
                KeyboardController keyboardController = flybyController.triggerSources[0] as KeyboardController;
                keyboardController.OnKeyPress("1");
                Assert.AreEqual(o._i, 1);
            }

            [Test]
            public void Can_Trigger_With_Midi_NoteOn() {
                Obj o = flybyController.currentPart.objs[0];
                MidiController midiController = flybyController.triggerSources[1] as MidiController;
                midiController.OnMidiNoteOn(0, 1, 1);
                Assert.AreEqual(o._i, 1);
            }

            [Test]
            public void Wrong_Notes_Do_Not_Trigger_Midi_NoteOn() {
                Obj o = flybyController.currentPart.objs[0];
                MidiController midiController = flybyController.triggerSources[1] as MidiController;
                midiController.OnMidiNoteOn(0, 99, 1);
                Assert.AreEqual(o._i, 0);
            }

            [Test]
            public void Multiple_Midi_Notes_Work_For_Triggers() {
                Obj o = flybyController.currentPart.objs[0];
                MidiController midiController = flybyController.triggerSources[1] as MidiController;
                midiController.OnMidiNoteOn(0, 1, 1);
                Assert.AreEqual(o._i, 1);
                midiController.OnMidiNoteOn(0, 2, 1);
                Assert.AreEqual(o._i, 2);
            }

        }
    }
}