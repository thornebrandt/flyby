using flyby.Controllers;
using flyby.Core;
using NUnit.Framework;
using UnityEngine;
using System.Collections.Generic;
using flyby.Triggers;
using UnityEngine.TestTools;



namespace Tests {
    namespace Controllers {

        public class CameraControllerTest {
            CameraController cameraController;
            AssetController assetController;
            GameObject go;
            GameObject camObj;
            GameObject camTarget; //cam will always be looking at this target. used to keep track of y rotation.  
            GameObject camPivot; //used to keep track of x rotation. 
            GameObject cam; //used to keep track of offset from rotation. 

            [SetUp]
            public void Setup() {
                go = new GameObject("CameraController");
                cameraController = go.AddComponent<CameraController>();
                if (assetController == null) {
                    assetController = new AssetController();
                    assetController.prefabCache = new Dictionary<string, GameObject>();
                    camObj = assetController.loadAssetFromResources("TestPrefabs", "CamPrefab");
                }
                cameraController.camObj = camObj;
                cameraController.setup(null, null);
                camTarget = camObj.transform.GetChild(0).gameObject;
                camPivot = camTarget.transform.GetChild(0).gameObject;
                cam = camPivot.transform.GetChild(0).gameObject;
            }

            [Test]
            public void Sets_Up_Controller() {
                Assert.IsNotNull(cameraController);
                Assert.IsNotNull(cameraController.camObj);
            }


            [Test]
            public void Instantiates_The_Cam_Children() {
                Assert.IsNotNull(camTarget);
                Assert.IsNotNull(camPivot);
                Assert.IsNotNull(cam);
            }

            [Test]
            public void Cam_Is_Positioned_Correctly() {
                Assert.AreEqual(cam.transform.localPosition, new Vector3(0, 0, -5));
            }

            [Test]
            public void Camera_Angle_Has_Default_Animation_Time_Of_Four_Seconds() {
                Assert.AreEqual(cameraController.currentCameraAngle.animationTime, 4);
            }

            [Test]
            public void Cam_Animates_To_Direct_CameraAngle() {
                CameraAngle c = new CameraAngle();
                c.offset.z = -10;
                c.origin.x = -1;
                c.rotation = new Vec(
                    45,
                    90,
                    0
                );
                c.animationTime = 0;
                cameraController.transitionToCameraAngle(c);
                Assert.AreEqual(camObj.transform.position, new Vector3(-1, 0, 0));
                Assert.AreEqual(cam.transform.localPosition, new Vector3(0, 0, -10));
                Assert.AreEqual(camPivot.transform.localRotation, Quaternion.Euler(new Vector3(45, 0, 0)));
                Assert.IsTrue(camTarget.transform.localRotation == Quaternion.Euler(new Vector3(0, 90, 0)));
                //not sure why below fails and above passes. 
                //Assert.AreEqual(camTarget.transform.localRotation, Quaternion.Euler(new Vector3(0, 90, 0)));
            }

            [Test]
            public void Can_Trigger_A_Second_Camera_Angle() {
                cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                    new CameraAngle()
                };
                TriggerObj t = new TriggerObj {
                    eventName = "second_camera"
                };
                cameraController.currentCameraAngle = cameraController.cameraAngles[0];
                cameraController.cameraAngles[1].trigger = "second_camera";
                cameraController.eventHandler(t);
                Assert.AreEqual(cameraController.currentCameraAngle, cameraController.cameraAngles[1]);
            }


            [Test]
            public void Can_Trigger_Next_Camera_Angle() {
                cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                    new CameraAngle()
                };
                TriggerObj t = new TriggerObj {
                    eventName = "next_camera_angle"
                };
                cameraController.currentCameraAngle = cameraController.cameraAngles[0];
                cameraController.eventHandler(t);
                Assert.AreEqual(cameraController.currentCameraAngle, cameraController.cameraAngles[1]);
            }


            [Test]
            public void Can_Trigger_Previous_Camera_Angle() {
                cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                    new CameraAngle(),
                    new CameraAngle(),
                };
                TriggerObj t = new TriggerObj {
                    eventName = "prev_camera_angle"
                };
                cameraController.currentCameraAngle = cameraController.cameraAngles[0];
                cameraController.eventHandler(t);
                Assert.AreEqual(cameraController.currentCameraAngle, cameraController.cameraAngles[2]);
            }

            [Test]
            public void Can_Transition_To_Next_Camera_Angle() {
                cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                    new CameraAngle(),
                };
                CameraAngle c1 = cameraController.cameraAngles[0];
                CameraAngle c2 = cameraController.cameraAngles[1];
                c1.animationTime = 0;
                c2.animationTime = 0;
                c1.offset = new Vec(1, 0, 0);
                c2.offset = new Vec(-1, 0, 0);
                TriggerObj t = new TriggerObj {
                    eventName = "next_camera_angle"
                };
                cameraController.eventHandler(t);
                Assert.AreEqual(cam.transform.localPosition, new Vector3(-1, 0, 0));
            }

            [Test]
            public void Can_Transition_To_Previous_Camera_Angle() {
                cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                    new CameraAngle(),
                    new CameraAngle(),
                };
                CameraAngle c1 = cameraController.cameraAngles[0];
                CameraAngle c2 = cameraController.cameraAngles[1];
                CameraAngle c3 = cameraController.cameraAngles[2];
                c1.animationTime = 0;
                c2.animationTime = 0;
                c3.animationTime = 0;
                c1.offset = new Vec(1, 0, 0);
                c2.offset = new Vec(-1, 0, 0);
                c3.offset = new Vec(0, -1, 0);
                TriggerObj t = new TriggerObj {
                    eventName = "prev_camera_angle"
                };
                cameraController.eventHandler(t);
                Assert.AreEqual(cam.transform.localPosition, new Vector3(0, -1, 0));
            }

            [Test]
            public void Can_Transition_To_Specific_Angle_By_Name() {
                cameraController.cameraAngles = new List<CameraAngle>{
                    new CameraAngle(),
                };
                CameraAngle c = cameraController.cameraAngles[0];
                c.animationTime = 0;
                c.offset = new Vec(1, 0, 0);
                c.trigger = "thisAngle";
                TriggerObj t = new TriggerObj {
                    eventName = "thisAngle"
                };
                cameraController.eventHandler(t);
                Assert.AreEqual(cam.transform.localPosition, new Vector3(1, 0, 0));
            }
        }
    }
}