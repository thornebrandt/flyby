
struct Ramp {
    float position;
    float width;
    float blur;
    float4 color;
};

StructuredBuffer<Ramp> _Ramps;


float rampMask(float uv_x, float pad){
    //sends back white/black where there is a ramp. 
    //does not take ramp alpha into effect. 
    float color = 0;
    for (uint i = 0; i < 32; i++) {
        float position = _Ramps[i].position;
        float width = max(0.001, _Ramps[i].width) + pad;
        float blur = _Ramps[i].blur;
        float x = uv_x * (1.0 - width);
        float leftSide = smoothstep(position - width, position - width + blur, x - (width * 0.5));
        float rightSide = 1.0 - smoothstep(position - blur, position, x);
        float ramp = leftSide * rightSide;
        color += ramp;
        color = clamp(color, 0.0, 1.0);
    }
    return color;
}

float addRamp(float uv_x, Ramp _ramp){
    float position = _ramp.position;
    float width = max(0.001, _ramp.width);
    float blur = _ramp.blur;
    float x = uv_x * (1.0 - width);
    float leftSide = smoothstep(position - width, position - width + blur, x - (width * 0.5));
    float rightSide = 1.0 - smoothstep(position - blur, position, x);
    float ramp = leftSide * rightSide;
    return ramp;
}


float4 rampFunction_float4_white(float uv_x, float4 _BackgroundColor, out float4 rampsColor) {
    //sends back the backgroundColor
    float4 color = float4(0.0, 0.0, 0.0, 0.0); // Start with transparent color
    rampsColor = float4(0.0, 0.0, 0.0, 0.0); //this one is being returned. 
    float4 col = color;
    float alpha = 0;
    //attempting starting at one. 
    for (uint i = 0; i < 32; i++) {
        float ramp = addRamp(uv_x, _Ramps[i]);
        rampsColor.rgb += ramp * _Ramps[i].color;
        color.rgb += ramp;
        alpha += ramp;
    }

    alpha = clamp(alpha, 0.0, 1.0);
    color.a = alpha;

    if (_BackgroundColor.a > 0) {
        col.rgb = lerp(_BackgroundColor.rgb, color.rgb, color.a);
        col.a = lerp(_BackgroundColor.a, color.a, color.a);
    } else {
        col = color;
    }

    return col;
}

float4 rampFunction_float4(float uv_x, float4 _BackgroundColor) {
    //sends back the backgroundColor
    float4 color = float4(0, 0, 0, 0); // Start with transparent color
    float4 col = color;
    float alpha = 0;
    //attem pting starting at one. 
    for (uint i = 0; i < 32; i++) {
        float position = _Ramps[i].position;
        float width = max(0.001, _Ramps[i].width);
        float blur = _Ramps[i].blur;
        float x = uv_x * (1.0 - width);
        float leftSide = smoothstep(position - width, position - width + blur, x - (width * 0.5));
        float rightSide = 1.0 - smoothstep(position - blur, position, x);
        float ramp = leftSide * rightSide;
        float4 rampColor = _Ramps[i].color;
        color.rgb += ramp * rampColor.rgb;
        alpha += ramp * rampColor.a;
    }

    alpha = clamp(alpha, 0.0, 1.0);
    color.a = alpha;

    if (_BackgroundColor.a > 0) {
        col.rgb = lerp(_BackgroundColor.rgb, color.rgb, color.a);
        col.a = lerp(_BackgroundColor.a, color.a, color.a);
    } else {
        col = color;
    }

    return col;
}