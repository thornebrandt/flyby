Shader "Custom/Stencil/MaskStencil"
{

    // Masking in HDRP
    // 1.Add the "MaskPass" to the scene. 
    // 2. Target a "Mask" Layer. 
    // 3. On the "masking object"  use an HDRP material with "Surface Type: Transparent" and "Depth Test: Never"
    // 4. On the "masking object" set layer to "Mask"
    // 5. On the "Masked Object"  Set "Surface Type: Transparent"  and "Depth Test: "Less Equal" ( Default )
    

    SubShader {
        // Render the mask after regular geometry, but before masked geometry and
        // transparent things.
 
        Tags {"Queue" = "Geometry+10" }
 
        // Don't draw in the RGBA channels; just the depth buffer
 
        ColorMask 0
        ZWrite On
 
        // Do nothing specific in the pass:
 
        Pass {}
    }
}