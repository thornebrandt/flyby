struct Ramp {
    float position;
    float width;
    float blur;
    float4 color;
};

StructuredBuffer<Ramp> _Ramps;

void rampFunction_float(float uv_x, float4 _BackgroundColor, out float4 col) {
    float4 color = float4(0, 0, 0, 0); // Start with transparent color
    float alpha = 0;
    for (uint i = 0; i < 32; i++) {
        float position = _Ramps[i].position;
        float width = max(0.001, _Ramps[i].width);
        float blur = _Ramps[i].blur;
        float x = uv_x * (1.0 - width);
        float leftSide = smoothstep(position - width, position - width + blur, x - (width * 0.5));
        float rightSide = 1.0 - smoothstep(position - blur, position, x);
        float ramp = leftSide * rightSide;
        float4 rampColor = _Ramps[i].color;
        color.rgb += ramp * rampColor.rgb;
        alpha += ramp * rampColor.a;
    }

    alpha = clamp(alpha, 0.0, 1.0);
    color.a = alpha;

    if (_BackgroundColor.a > 0) {
        col.rgb = lerp(_BackgroundColor.rgb, color.rgb, color.a);
        col.a = lerp(_BackgroundColor.a, color.a, color.a);
    } else {
        col = color;
    }
}